﻿Imports System.Net.Mail

Namespace BizSoft.Utilities

    Public Module LoginUtility

        '   User Info
        Public mUserId As String = ""
        Public mUserName As String = ""
        Public mUserDepartment As String = ""
        Public mIsPowerUsers As Boolean
        Public mStatusID As String = ""        'GL_userStatus 

        ' General Info 
        Public gGlobalDate As Date = Date.Now
        Public mLocationID As String = "0"
        Public mLocationName As String = "0"
        Public mWorkingDate As String = ""
        Public mWorkStationID As String = "0"
        Public mSegmentId As String = "0"
        '   Financial Year Info
        Public GL_FinancialID As String = ""
        Public GL_FinancialYear As String = ""
        Public GL_FinancialDateF As String = ""
        Public GL_FinancialDateT As String = ""
        ' Company Info
        Public mCompanyId As String = "0"
        Public mCompanyName As String = "BizSoft Solution (Pvt) Ltd"
        Public sSystemTitle As String
        Public sDefaultCashAccount As String
        Public sDefaultBankAccount As String
        ' Chart OF Account Form Veriables
        Public ParentAccountCode As String = ""
        Public newParentAccountCode As String = ""
        Public AccountGroupID As String = ""
        Public OpeningBalancelevel As String = ""
        Public newAccountCode As String = ""
        Public ExistParent As String = ""

        Public GL_holdValue As String = ""

        Public GLV_accountCode As String = ""
        Public GLV_parentCode As String = ""
        Public GLV_levels As Integer = 0
        Public GLF_NewRecoerd As Boolean = False
        Public GLF_EditRecord As Boolean = False
        Public GL_Flag As Boolean
        Public GL_CancelFlag As Boolean

        Public AccessControl As String = ""
        Public ChaildSub As String = ""
        Public gFromDate As Date = Date.Now
        Public gToDate As Date = Date.Now

        Public gAccommodationType As String = ""
        Public gAccommodation As String = ""
        Public gRoomID As String = ""
        Public gBedID As String = ""


        Sub SetUserSession(ByVal UserRow As DataRow)
            HttpContext.Current.Session("LoginUserInfo") = UserRow

        End Sub
        Function ValidateLogin() As Boolean

            Dim dr As DataRow = CType(HttpContext.Current.Session("LoginUserInfo"), DataRow)
            If dr IsNot Nothing Then
                Return True
            End If
            Return False
        End Function

        Public Sub SendEmail()
            Dim mString As String
            Dim mStrEmailInfo As String = ""
            mStrEmailInfo = "Select * from tblEmail where IsEmailSent = 0"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(mStrEmailInfo)



            For i = 0 To ds.Tables(0).Rows.Count - 1
                Try

                    Dim SmtpServer As New SmtpClient()
                    Dim mail As New MailMessage()

                    'SmtpServer.Credentials = New Net.NetworkCredential("asim_jcp@yahoo.com", "p")
                    'SmtpServer.Port = 25
                    'SmtpServer.Host = "smtp.mail.yahoo.com"
                    'mail = New MailMessage()
                    'mail.From = New MailAddress("asim_jcp@yahoo.com")
                    'mail.To.Add("asim_jcp@yahoo.com")
                    'mail.CC.Add("asim@bizsoft.pk")
                    'mail.Bcc.Add("adnan@bizsoft.pk")
                    'mail.Subject = ds.Tables(0).Rows(i)("Subject").ToString()
                    'mail.Body = ds.Tables(0).Rows(i)("MailMessage").ToString()
                    'SmtpServer.EnableSsl = True
                    'SmtpServer.Send(mail)
                    'mString = "mail send"



                    SmtpServer.Credentials = New Net.NetworkCredential(ds.Tables(0).Rows(i)("SenderEmailAddress").ToString(), ds.Tables(0).Rows(i)("SenderEmailPassword").ToString())
                    SmtpServer.Port = ds.Tables(0).Rows(i)("SenderHostPort").ToString()
                    SmtpServer.Host = ds.Tables(0).Rows(i)("SenderHostName").ToString()
                    mail = New MailMessage()
                    mail.From = New MailAddress(ds.Tables(0).Rows(i)("SenderEmailAddress").ToString())
                    mail.To.Add(ds.Tables(0).Rows(i)("Email").ToString())
                    mail.CC.Add(ds.Tables(0).Rows(i)("Cc").ToString())
                    mail.Bcc.Add(ds.Tables(0).Rows(i)("Bcc").ToString())
                    mail.Subject = ds.Tables(0).Rows(i)("Subject").ToString()
                    mail.Body = ds.Tables(0).Rows(i)("MailMessage").ToString()
                    SmtpServer.EnableSsl = False
                    SmtpServer.Send(mail)
                    mString = "mail send"

                Catch ex As Exception
                    mString = "error for sending"
                End Try
            Next

        End Sub
        Function ApplicationName() As String
            Return "Bridge ERP System (FMIS)"
        End Function
        Function GetDateFormat() As String
            Return System.Configuration.ConfigurationManager.AppSettings("DATEFORMAT")

        End Function
        Function GetDateFormatForJava() As String
            Return System.Configuration.ConfigurationManager.AppSettings("DATEFORMATFORJAVA")
        End Function

        Public Function HasRights(ByVal strRight As String, ByVal strAction As String, ByVal strEntity As String) As Boolean
            Dim flag As Boolean = False
            Dim dt As DataSet = CType(HttpContext.Current.Session("AccessRights"), DataSet)
            Dim dv As New DataView(dt.Tables(0))
            dv.RowFilter = "MenuName='" & strRight.Replace("'", "''") & "' and " & strAction & "=" & strEntity
            If dv.ToTable().Rows.Count > 0 Then
                flag = True
            Else
                flag = False
            End If
            Return flag
        End Function

        Public Sub InsertLogs(ByVal UserID As String, ByVal iDate As String, ByVal iAction As String, ByVal iKey As String, ByVal CompanyID As String, ByVal iTime As String, ByVal DocName As String, ByVal TableName As String, ByVal TableID As String, ByVal PCID As String)
            PCID = Environment.MachineName
            Dim QrtStr As String = ""
            QrtStr = "insert into GL_TransactionLog (UserID, iDate, iAction, iKey, CompanyID, iTime,DocName ,TableName,TableID ,PCID) Values " &
                    "(" & UserID & ", '" & DateTime.Parse(DateTime.Now).ToString("dd MMMM yyyy") & "' ,'" & iAction & "','" & iKey & "','" & CompanyID & "','" & DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt") & "','" & DocName & "','" & TableName & "','" & TableID & "','" & PCID & "')"
            BizSoft.DBManager.ExecuteQry(QrtStr)

        End Sub

        Public Sub InsertErrorLogs(ByVal UserID As String, ByVal iDate As String, ByVal iAction As String, ByVal iKey As String, ByVal CompanyID As String, ByVal iTime As String, ByVal DocName As String, ByVal TableName As String, ByVal TableID As String, ByVal PCID As String, ByVal AppVariable As String)
            PCID = Environment.MachineName
            Dim QrtStr As String = ""
            QrtStr = "insert into GL_ErrorLog (UserID, iDate, iAction, iKey, CompanyID, iTime,DocName ,TableName,TableID ,PCID, AppVer) Values " &
                    "(" & UserID & ",'" & DateTime.Parse(DateTime.Now).ToString("dddd, dd MMMM yyyy") & "','" & Regex.Replace(iAction, "[^a-zA-Z 0-9-/-]", "") & "','" & Regex.Replace(iKey, "[^a-zA-Z 0-9-/-]", "") & "','" & CompanyID & "','" & DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt") & "','" & DocName & "','" & TableName & "','" & TableID & "','" & PCID & "','" & AppVariable & "')"
            BizSoft.DBManager.ExecuteQry(QrtStr)

        End Sub

        Public Sub BtnEnableDisble(ByVal btnadd As System.Web.UI.WebControls.ImageButton, ByVal btnSave As System.Web.UI.WebControls.ImageButton, ByVal btnView As System.Web.UI.WebControls.ImageButton, ByVal btnDelete As System.Web.UI.WebControls.ImageButton, ByVal btnPrint As System.Web.UI.WebControls.ImageButton, ByVal Mtype As String)

            If Mtype = "LOAD" Then
                btnadd.Enabled = True
                btnSave.Enabled = True
                btnView.Enabled = True
                btnDelete.Enabled = True
                btnPrint.Enabled = True

            ElseIf Mtype = "ADD" Then
                btnadd.Enabled = True
                btnSave.Enabled = True
                btnView.Enabled = True
                btnDelete.Enabled = True
                btnPrint.Enabled = True
            ElseIf Mtype = "SAVE" Then
                btnadd.Enabled = True
                btnSave.Enabled = True
                btnView.Enabled = True
                btnDelete.Enabled = True
                btnPrint.Enabled = True
            ElseIf Mtype = "DEL" Then
                btnadd.Enabled = True
                btnSave.Enabled = True
                btnView.Enabled = True
                btnDelete.Enabled = True
                btnPrint.Enabled = True
            End If

        End Sub
    End Module
End Namespace
