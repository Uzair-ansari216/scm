﻿Imports System.Web.Optimization
Imports System.Web.Routing

Public Class Global_asax
    Inherits HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
        BundleConfig.RegisterBundles(BundleTable.Bundles)
        AuthConfig.RegisterOpenAuth()
        RegisterRoutes(RouteTable.Routes)
    End Sub

    Sub RegisterRoutes(routes As RouteCollection)
        ' AppRoutes.Main(routes)
        AppRoutes.Setups(routes)
        'AppRoutes.Transaction(routes)

    End Sub


    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
        'Dim result = IsSessionExpired()
        Dim Context As HttpContext = HttpContext.Current

        'If result Then
        'Context.Response.Redirect("default.aspx")
        'End If

    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub
    'fired whenever a single user Session ends or times out
    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        'Context.Response.Redirect("default.aspx")
    End Sub
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
    End Sub
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub
    Public Shared Function IsSessionExpired() As Boolean

        If HttpContext.Current.Session Is Nothing Then
            'If (HttpContext.Current.Session.IsNewSession) Then

            Dim CookieHeaders As String = HttpContext.Current.Request.Headers("Cookie")

            If IsNothing(CookieHeaders) And (CookieHeaders.IndexOf("ASP.NET_SessionId") >= 0) Then
                ' IsNewSession is true and session cookie exists,
                ' so, ASP.NET session is expired
                Return True
            End If
            'End If
        End If

        ' Session is not expired and function will return False,
        ' it could be new session, or existing active session
        Return False
    End Function

End Class