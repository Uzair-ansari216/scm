﻿Imports System.Windows.Forms
Imports System.Web.Script.Services

Public Class _Default
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Session.Abandon()
            'FillCompany()
            'FillLocation()
            FillFinancialYear()
            'txtpasswd.Text = "123"
        End If

    End Sub


    Private Sub FillLocation()
        'Dim objLoc As New BizSoft.Bridge_SCM.clsLogin
        'Dim dtlocation As DataSet = objLoc.GetLocation

        'cmbLocation.DataSource = dtlocation.Tables(0)
        'Dim dr As DataRow = dtlocation.Tables(0).NewRow
        'dr("ID") = -1
        'dr("Name") = "Please Select"
        'dtlocation.Tables(0).Rows.Add(dr)
        'cmbLocation.DataTextField = "Name"
        'cmbLocation.DataValueField = "ID"
        'cmbLocation.SelectedValue = -1
        'cmbLocation.DataBind()

    End Sub
    Private Sub FillCompany()
        Dim objLoc As New BizSoft.Bridge_SCM.clsLogin
        'Dim dtlocation As DataSet = objLoc.GetCompany
        'cmbCompany.DataSource = dtlocation.Tables(0)
        'Dim dr As DataRow = dtlocation.Tables(0).NewRow
        'dr("companyID") = -1
        'dr("companyName") = "Please Select"
        'dtlocation.Tables(0).Rows.Add(dr)
        'cmbCompany.DataTextField = "companyName"
        'cmbCompany.DataValueField = "companyID"
        'cmbCompany.SelectedValue = -1
        'cmbCompany.DataBind()

        'Dim ds4 As DataSet = obj.FillAccomodation()
        'ddlAccommodationType.DataSource = ds4
        'Dim dr4 As DataRow = ds4.Tables(0).NewRow
        'dr4("ID") = -1
        'dr4("Description") = "Please Select"
        'ds4.Tables(0).Rows.Add(dr4)
        'ddlAccommodationType.DataValueField = "ID"
        'ddlAccommodationType.DataTextField = "Description"
        'ddlAccommodationType.DataBind()

    End Sub
    Private Sub FillFinancialYear()
        Dim objLoc As New BizSoft.Bridge_SCM.clsLogin
        Dim dtlocation As DataSet = objLoc.GetFinancialYear
        cmbFinancial.DataSource = dtlocation.Tables(0)

        Dim dr As DataRow = dtlocation.Tables(0).NewRow
        dr("yearID") = -1
        dr("yearTitle") = "Please Select"
        dtlocation.Tables(0).Rows.Add(dr)
        cmbFinancial.DataTextField = "yearTitle"
        cmbFinancial.DataValueField = "yearID"
        cmbFinancial.SelectedIndex = 0
        cmbFinancial.DataBind()

    End Sub


    'Protected Sub btnLogin_Click1(sender As Object, e As EventArgs) Handles btnLogin.Click
    'Dim objLogin As New BizSoft.Bridge_SCM.clsLogin()
    'Dim blnStatus As Boolean = 0
    'Dim strMsg As String = ""

    'If cmbLocation.SelectedValue = -1 Then
    '    lblAllError.Text = "Location must be Selected..."
    '    Exit Sub
    'End If

    'If cmbCompany.SelectedValue = -1 Then
    '    lblAllError.Text = "Company must be Selected..."
    '    Exit Sub
    'End If

    'Dim dr As DataSet = objLogin.Login(txtCode.Text, txtpasswd.Text, blnStatus, strMsg)

    'If blnStatus = False Then
    '    blnStatus = False
    '    lblAllError.Text = "Invalid UserName or Password"
    '    Exit Sub
    'End If
    'If dr.Tables(0).Rows.Count > 0 Then
    '    lblAllError.Text = "Welcome"
    '    blnStatus = True
    '    Session("UserID") = Convert.ToString(dr.Tables(0).Rows(0)("UserID"))
    '    Session("UserName") = Convert.ToString(dr.Tables(0).Rows(0)("UserName"))
    '    Session("CompanyID") = Convert.ToString(dr.Tables(0).Rows(0)("CompanyID"))
    '    Session("isPowerUser") = Convert.ToString(dr.Tables(0).Rows(0)("isPowerUser"))
    '    Session("StatusID") = Convert.ToString(dr.Tables(0).Rows(0)("StatusID"))
    '    Session("LocationId") = Convert.ToString(dr.Tables(0).Rows(0)("LocationId"))

    '    If Convert.ToBoolean(Session("isPowerUser")) = True Then
    '        Session("LocationId") = cmbLocation.SelectedValue
    '        Session("LocationName") = cmbLocation.SelectedItem.Text.ToString()
    '        Session("CompanyID") = cmbCompany.SelectedValue
    '    Else
    '        Session("LocationId") = Convert.ToString(dr.Tables(0).Rows(0)("LocationID"))
    '        'Session("LocationName") = Convert.ToString(dr.Tables(0).Rows(0)("Location"))
    '        Session("CompanyID") = Convert.ToString(dr.Tables(0).Rows(0)("CompanyID"))
    '    End If
    '    'drNew = objLogin.LoginNew(txtCode.Text, txtpasswd.Text, blnStatus, strMsg)
    'Else
    '    blnStatus = False
    '    lblAllError.Text = "Invalid UserName or Password"
    'End If
    'If blnStatus Then
    '    BizSoft.Utilities.SetUserSession(dr.Tables(0).Rows(0))
    '    Dim objSetAccessRight As New BizSoft.Bridge_SCM.AccessRight()
    '    Dim ODSrght As New DataSet

    '    '  objSetAccessRight.GetAllaccessbyUser(Session("UserID"), ODSrght)
    '    ' Session("AccessRights") = ODSrght.Tables(0)

    '    Response.Redirect("Forms/Admin/Home.aspx")
    'Else
    '    lblAllError.Text = strMsg
    'End If
    ' End Sub
End Class