﻿
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Bridge_SCM


Partial Public Class ReportViewer
    Inherits System.Web.UI.Page

    Dim crParameterFields As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinitions  ' ParameterFields
    Dim crParameterField As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition
    Dim crParameterDiscreteValue As CrystalDecisions.Shared.ParameterDiscreteValue
    Dim crParameterValues As CrystalDecisions.Shared.ParameterValues
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button
    Dim obj As ReportDocument
    Dim htparam As Hashtable
    Private ogsReport As New BizSoft.DBManager.clsReport.oReport
    Private objReport As New BizSoft.DBManager.clsReport
    Private reportCredentials

    ' Dim objLogin As New BizSoft.Bridge_FMIS.clsLogin()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim rptTitle As TextObject
        Dim rptCompantName As TextObject
        Dim PrintedBy As TextObject
        Dim isGeneratedByBuilder = Convert.ToBoolean(HttpContext.Current.Request.QueryString("isGeneratedByBuilder"))
        ogsReport = Session("ogsReport")
         If Not isGeneratedByBuilder Then
            setConnectionForOtherReports()

            htparam = CType(ogsReport.ht, Hashtable)
            If Not IsNothing(htparam) Then
                SetupReportParametersForOtherForms(htparam)
            End If
        Else
            reportCredentials = Session("credentials")
            SetConnection()
            htparam = CType(ogsReport.ht, Hashtable)

            If Not IsNothing(htparam) Then
                SetupReportParameters(htparam)
            End If
        End If





        Me.CrystalReportViewer.DisplayToolbar = True
        Try
            If ogsReport.rptCreteria.Length > 0 Then
                obj.RecordSelectionFormula = IIf(ogsReport.rptCreteria.Length > 17, ogsReport.rptCreteria, "")
            End If
        Catch
        End Try
        Try
            rptTitle = obj.ReportDefinition.ReportObjects.Item("txtTitle")
        Catch
        End Try
        Try
            rptCompantName = obj.ReportDefinition.ReportObjects.Item("txtCompanyName")
        Catch
        End Try
        Try
            ' Dim rptProduct As CrystalDecisions.CrystalReports.Engine.TextObject = obj.ReportDefinition.ReportObjects.Item("txtProductName")
            '  Dim rptCriteria As CrystalDecisions.CrystalReports.Engine.TextObject = obj.ReportDefinition.ReportObjects.Item("txtCriteria")
        Catch
        End Try
        Try
            PrintedBy = obj.ReportDefinition.ReportObjects.Item("PrintedBy")
        Catch
        End Try
        Try

            rptTitle.Text = ogsReport.rptTitle
        Catch
        End Try
        Try
            rptCompantName.Text = ogsReport.strCompanyName
        Catch
        End Try
        Try
            ' rptProduct.Text = ogsReport.rptProduct
            '  rptCriteria.Text = ogsReport.rptCreteria
        Catch
        End Try
        Try
            PrintedBy.Text = Session("UserName")

            Me.Title = ogsReport.rptTitle
        Catch
        End Try

        'CrystalReportViewer.ToolPanelView = "None" 'CrystalDecisions.Web.ToolPanelViewType.None

        'Dim screenWidth As Integer = Screen.PrimaryScreen.Bounds.Width()
        'Dim screenHeight As Integer = Screen.PrimaryScreen.Bounds.Height
        'Me.CrystalReportViewer.Width = screenWidth
        'Me.CrystalReportViewer.Height = screenHeight


        Me.CrystalReportViewer.ReportSource = obj
        '  Me.CrystalReportViewer.RefreshReport()
        Me.CrystalReportViewer.DataBind()

        '----------------------
        Dim mStrm As System.IO.MemoryStream


        If Session("reportFormat") IsNot Nothing And Session("reportFormat").ToString() = "Excel" Then
            'mStrm = obj.ExportToStream(ExportFormatType.Excel)
            ' the code below will create excel in memory and stream them to the browser
            'Session("reportFormat") = Nothing
            ' instead of creating files on disk.
            Dim s As System.IO.Stream = obj.ExportToStream(ExportFormatType.Excel)
            Dim b(s.Length) As Byte
            s.Read(b, 0, CInt(s.Length))
            With HttpContext.Current.Response
                .ClearContent()
                .ClearHeaders()
                .ContentType = "application/excel"
                .AddHeader("Content-Disposition", "inline; filename=" + ogsReport.strFileName.Split(".")(0) + ".xls")
                .BinaryWrite(b)
                .Flush()
                .SuppressContent = True
                HttpContext.Current.ApplicationInstance.CompleteRequest()
                .End()
                '---------------------------
            End With

        ElseIf Session("reportFormat") IsNot Nothing And Session("reportFormat").ToString() = "PDF" Then

            ' the code below will create pdfs in memory and stream them to the browser
            'mStrm = obj.ExportToStream(ExportFormatType.PortableDocFormat)
            ' instead of creating files on disk.
            Dim s As System.IO.Stream = obj.ExportToStream(ExportFormatType.PortableDocFormat)
            Dim b(s.Length) As Byte
            s.Read(b, 0, CInt(s.Length))
            With HttpContext.Current.Response
                .ClearContent()
                .ClearHeaders()
                .ContentType = "application/pdf"
                .AddHeader("Content-Disposition", "inline; filename=" + ogsReport.strFileName.Split(".")(0) + ".pdf")
                .BinaryWrite(b)
                .Flush()
                .SuppressContent = True
                HttpContext.Current.ApplicationInstance.CompleteRequest()
                .End()
                '---------------------------
            End With
        End If







        ' the code below will create pdfs in memory and stream them to the browser

        ' instead of creating files on disk.
        'With HttpContext.Current.Response
        '    .ClearContent()
        '    .ClearHeaders()
        '    .ContentType = "application/pdf"
        '    .AddHeader("Content-Disposition", "inline; filename=Report.pdf")
        '    .BinaryWrite(mStrm.ToArray)
        '    ' .End()
        '    .Flush()
        '    '---------------------------
        'End With

    End Sub


    Private Sub SetConnection()


        Dim crDatabase As Database
        Dim crTables As Tables
        Dim crTable As Table
        Dim crTableLogOnInfo As TableLogOnInfo
        'Dim crConnectionInfo As ConnectionInfo
        Dim crConnectionInfo As ReportCredentials
        Dim item As String


        obj = LoadReport(ogsReport.strFileName)

        'crConnectionInfo = New ConnectionInfo
        'With crConnectionInfo
        'objReport.GetConnection(reportCredentials.ReportServer, reportCredentials.ReportDbUserId, reportCredentials.ReportDbPassord, reportCredentials.ReportDatabase, True)
        'objReport.GetConnection(.ServerName, .UserID, .Password, .DatabaseName, True)
        'End With
        If reportCredentials.ReportConnectionType = "odbc" Then
            crConnectionInfo = objReport.GetConnection(reportCredentials.ReportServer, reportCredentials.ConnectionUserId, reportCredentials.ConnectionPassword, reportCredentials.ConnectionName, True)
        Else
            crConnectionInfo = objReport.GetConnection(reportCredentials.ReportServer, reportCredentials.ReportDbUserId, reportCredentials.ReportDbPassord, reportCredentials.ReportDatabase, True)
        End If
        crDatabase = obj.Database
        crTables = crDatabase.Tables
        ''Apply the logon information to each table in the collection
        For Each crTable In crTables
            crTableLogOnInfo = crTable.LogOnInfo
            If reportCredentials.ReportConnectionType = "odbc" Then
                crTableLogOnInfo.ConnectionInfo.ServerName = crConnectionInfo.Database
            Else
                crTableLogOnInfo.ConnectionInfo.ServerName = crConnectionInfo.Server
            End If
            crTableLogOnInfo.ConnectionInfo.UserID = crConnectionInfo.UserId
            crTableLogOnInfo.ConnectionInfo.Password = crConnectionInfo.Password
            crTableLogOnInfo.ConnectionInfo.DatabaseName = crConnectionInfo.Database
            crTableLogOnInfo.ConnectionInfo.IntegratedSecurity = crConnectionInfo.lIntegratedSecurity
            '  crConnectionInfo.IntegratedSecurity = True
            crTable.ApplyLogOnInfo(crTableLogOnInfo)
        Next
        If ogsReport.rptIsSubLoad = True Then
            For Each item In ogsReport.rptSubReportCollection
                'obj.Subreports(item).SetDatabaseLogon(crConnectionInfo.UserID, crConnectionInfo.Password, crConnectionInfo.ServerName, crConnectionInfo.DatabaseName)
            Next
        End If

        If Session("QueryString") = True Then
            ' adoOleDbDataAdapter.Fill(dataSet, "Authors")
            obj.SetDataSource(Session("rptMainTable"))
            Session("QueryString") = False
            Session("rptMainTable") = Nothing
        End If
    End Sub

    Private Function LoadReport(ByVal sReportName As String) As ReportDocument
        Dim crReportDocument As ReportDocument
        crReportDocument = New ReportDocument
        Dim strPath As String
        Dim strNewPath As String

        strPath = Server.MapPath("")
        strNewPath = strPath.Replace("\Forms\Ctrls", "")
        If strNewPath.IndexOf("\Forms\Ctrls") > 0 Then
            strNewPath = strNewPath.Replace("\Forms\Ctrls", "")
        Else
            strNewPath = strNewPath.Replace("\Forms\Ctrls", "")
        End If
        Dim path = reportCredentials.ReportPath
        crReportDocument.Load(strNewPath & path & "\" & sReportName)
        'crReportDocument.Load(strNewPath & "\P-Reports" & "\" & sReportName)


        crReportDocument.Refresh()


        Return crReportDocument
    End Function

    Private Sub SetupReportParameters(ByVal reportParms As Hashtable)

        Dim myE As IDictionaryEnumerator
        Dim pFields As ParameterFieldDefinitions
        '  If Not isLoaded Then
        pFields = obj.DataDefinition.ParameterFields
        ' isLoaded = True
        ' End If
        If pFields.Count > 0 Then 'if report contain parameter 
            Dim pValues As ParameterValues
            Dim dValue As ParameterDiscreteValue
            'Setup Report Parameters
            myE = reportParms.GetEnumerator()
            While myE.MoveNext()
                Dim flag As Boolean = True
                Dim paramValType = myE.Value.GetType()
                pValues = pFields(myE.Key.ToString()).CurrentValues

                If (paramValType.ToString() = "System.Collections.ArrayList") Then
                    Dim myALvalues As ArrayList
                    myALvalues = myE.Value
                    Dim strValue As String
                    For Each strValue In myALvalues
                        dValue = New ParameterDiscreteValue
                        dValue.Value = strValue.ToString()
                        pValues.Add(dValue)
                        dValue = Nothing
                    Next
                Else
                    For Each item As ParameterFieldDefinition In obj.DataDefinition.ParameterFields
                        If item.Name = myE.Key.ToString() And item.EnableAllowMultipleValue Then
                            Dim words As String() = myE.Value.Split(New Char() {","c})
                            For Each values As String In words
                                dValue = New ParameterDiscreteValue
                                dValue.Value = values
                                pValues.Add(dValue)
                                obj.DataDefinition.ParameterFields(myE.Key.ToString()).ApplyCurrentValues(pValues)
                            Next
                            flag = False
                        End If

                    Next
                    If flag <> False Then
                        dValue = New ParameterDiscreteValue
                        dValue.Value = myE.Value
                        pValues.Add(dValue)
                        obj.DataDefinition.ParameterFields(myE.Key.ToString()).ApplyCurrentValues(pValues)
                    End If
                End If
            End While
        End If
    End Sub

    Private Sub setConnectionForOtherReports()

        Dim crDatabase As Database
        Dim crTables As Tables
        Dim crTable As Table
        Dim crTableLogOnInfo As TableLogOnInfo
        'Dim crConnectionInfo As ConnectionInfo
        Dim crConnectionInfo As ReportCredentials
        Dim item As String


        obj = LoadReportForOtherForms(ogsReport.strFileName)

        'crConnectionInfo = New ConnectionInfo
        'With crConnectionInfo
        '    objReport.GetConnection(ConfigurationManager.AppSettings("DBSERVER").ToString(), ConfigurationManager.AppSettings("DBUSERID").ToString(), ConfigurationManager.AppSettings("DBPASSWORD").ToString(), ConfigurationManager.AppSettings("DBDATABASE").ToString(), True)
        'End With
        crConnectionInfo = objReport.GetConnection(ConfigurationManager.AppSettings("DBSERVER").ToString(), ConfigurationManager.AppSettings("DBUSERID").ToString(), ConfigurationManager.AppSettings("DBPASSWORD").ToString(), ConfigurationManager.AppSettings("DBDATABASE").ToString(), True)

        crDatabase = obj.Database
        crTables = crDatabase.Tables
        ''Apply the logon information to each table in the collection
        For Each crTable In crTables
            crTableLogOnInfo = crTable.LogOnInfo
            crTableLogOnInfo.ConnectionInfo.ServerName = crConnectionInfo.Server
            crTableLogOnInfo.ConnectionInfo.UserID = crConnectionInfo.UserId
            crTableLogOnInfo.ConnectionInfo.Password = crConnectionInfo.Password
            crTableLogOnInfo.ConnectionInfo.DatabaseName = crConnectionInfo.Database
            crTableLogOnInfo.ConnectionInfo.IntegratedSecurity = crConnectionInfo.lIntegratedSecurity
            '  crConnectionInfo.IntegratedSecurity = True
            crTable.ApplyLogOnInfo(crTableLogOnInfo)
        Next
        If ogsReport.rptIsSubLoad = True Then
            For Each item In ogsReport.rptSubReportCollection
                'obj.Subreports(item).SetDatabaseLogon(crConnectionInfo.UserID, crConnectionInfo.Password, crConnectionInfo.ServerName, crConnectionInfo.DatabaseName)
            Next
        End If

        If Session("QueryString") = True Then
            ' adoOleDbDataAdapter.Fill(dataSet, "Authors")
            obj.SetDataSource(Session("rptMainTable"))
            Session("QueryString") = False
            Session("rptMainTable") = Nothing
        End If
    End Sub
    Private Function LoadReportForOtherForms(ByVal sReportName As String) As ReportDocument
        Dim crReportDocument As ReportDocument
        crReportDocument = New ReportDocument
        Dim strPath As String
        Dim strNewPath As String

        strPath = Server.MapPath("")
        strNewPath = strPath.Replace("\Forms\Ctrls", "")
        If strNewPath.IndexOf("\Forms\Ctrls") > 0 Then
            strNewPath = strNewPath.Replace("\Forms\Ctrls", "")
        Else
            strNewPath = strNewPath.Replace("\Forms\Ctrls", "")
        End If
        crReportDocument.Load(strNewPath & "\P-Reports" & "\" & sReportName)


        crReportDocument.Refresh()


        Return crReportDocument
    End Function

    Private Sub SetupReportParametersForOtherForms(ByVal reportParms As Hashtable)
        Dim myE As IDictionaryEnumerator
        Dim pFields As ParameterFieldDefinitions
        '  If Not isLoaded Then
        pFields = obj.DataDefinition.ParameterFields
        ' isLoaded = True
        ' End If
        If pFields.Count > 0 Then 'if report contain parameter 
            Dim pValues As ParameterValues
            Dim dValue As ParameterDiscreteValue
            'Setup Report Parameters
            myE = reportParms.GetEnumerator()
            While myE.MoveNext()
                pValues = pFields(myE.Key.ToString()).CurrentValues
                Dim paramValType = myE.Value.GetType()

                If (paramValType.ToString() = "System.Collections.ArrayList") Then
                    Dim myALvalues As ArrayList
                    myALvalues = myE.Value
                    Dim strValue As String
                    For Each strValue In myALvalues
                        dValue = New ParameterDiscreteValue
                        dValue.Value = strValue.ToString()
                        pValues.Add(dValue)
                        dValue = Nothing
                    Next
                Else
                    dValue = New ParameterDiscreteValue
                    dValue.Value = myE.Value
                    pValues.Add(dValue)
                    obj.DataDefinition.ParameterFields(myE.Key.ToString()).ApplyCurrentValues(pValues)
                End If
            End While
        End If
    End Sub
    Protected Sub btnPDF_Click(sender As Object, e As EventArgs)
        obj.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, True, "PurchaseOrderReport " & Date.Now.ToShortDateString)
    End Sub


End Class