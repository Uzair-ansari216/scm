﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="SalesOrderNew.aspx.vb" Inherits="Bridge_SCM.SalesOrderNew" %>

<%@ Import Namespace="System.Web.Optimization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/SalesOrderNewScript")%>
    <div id="SaleOrderForm">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <%--<form method="get" action="/" class="form-horizontal">--%>
                    <div class="card-header row">
                        <div class="col-md-3 col-sm-12">
                            <h4 class="card-title" style="color: #000000">Sales Order</h4>
                        </div>
                        <div class="col-md-9">
                            <button type="button" class="btn btn-primary pull-right" onclick="AddNew()" title="Add New Sale Order"><i class="fas fa-sync-alt"></i></button>
                            <button type="button" class="btn btn-primary pull-right" onclick="bindPrintClick()" title="Print Sales Order"><i class="fas fa-print"></i></button>
                            <button type="button" class="btn btn-primary pull-right" onclick="Show()" title="View Sale Order"><i class="fas fa-eye"></i></button>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-9">
                                <span id="Validation" class="new badge"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-9">
                                <h4 class="modal-title title-cl" id="SalesOrder"></h4>
                            </div>
                        </div>
                        <input type="hidden" id="txtID" value="" />
                        <div class="row">
                            <div class="col-sm-12">
                                <span id="ValidationSummary" class="new badge"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group label-floating">

                                    <label class="control-label">Doc Date</label>
                                    <input type="text" class="form-control" id="txtDate" name="txtDate" placeholder="" />
                                    <span id="rDate" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <label class="control-label">Location</label>
                                    <select id="ddlLocation" class="form-control"></select>
                                    <span id="rLocation" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <label class="control-label">Sales Person</label>
                                    <select id="ddlSalesPerson" class="form-control"></select>
                                    <span id="rSalesPerson" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <label class="control-label">Customer</label>
                                    <select id="ddlCustomer" class="form-control"></select>
                                    <span id="rCustomer" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Ship To</label>
                                    <input type="text" class="form-control" id="txtShipTo" name="txtShipTo" placeholder="" />
                                    <span id="rShipTo" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Bill To</label>
                                    <input type="text" class="form-control" id="txtBillTo" name="txtBillTo" placeholder="" />
                                    <span id="rBillTo" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <label class="control-label">Currency</label>
                                    <select id="ddlCurrency" class="form-control"></select>
                                    <span id="rCurrency" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <label class="control-label">Rate</label>
                                    <input type="text" class="form-control" value="1" id="txtCurrency" name="txtCurrency" placeholder="" />
                                    <span id="rtxtCurrency" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <label class="control-label">SO Type</label>
                                    <select id="ddlSOType" class="form-control"></select>
                                    <span id="rSOType" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <label class="control-label">Terms</label>
                                    <select id="ddlTerms" class="form-control"></select>
                                    <span id="rTerms" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                        </div>


                    </div>
                    <%--</form> --%>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-6"></div>
                            <div class="col-sm-3"></div>
                            <div class="col-sm-3">
                                <button type="button" class="btn btn-primary pull-right" onclick="OnAdd()"><i class="fas fa-plus"></i></button>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-content table-responsive">
                                    <table id='tableDetail' class="table table-striped table-bordered display" style="width: 100%">
                                        <thead class="header bg-primary">
                                            <tr class="">
                                                <th></th>
                                                <th>ID</th>
                                                <th>Model#</th>
                                                <th>PartNumber</th>
                                                <th>Description</th>
                                                <th>Amendment</th>
                                                <th>Qty</th>
                                                <th>Unit</th>
                                                <th>U.Price</th>
                                                <th>Remarks</th>
                                                <th>B.D Price</th>
                                                <th>Disc</th>
                                                <th>A.D Price</th>
                                                <th>Amount</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <label class="control-label">Amount</label>
                                    <input type="text" class="form-control" id="txtIAmount" name="txtIAmount" placeholder="" />
                                    <span id="rAmount" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="form-group label-floating">
                                    <label class="control-label">GST %</label>
                                    <input type="text" class="form-control" id="txtIGST" name="txtIGST" placeholder="" />
                                    <span id="rGTS" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <label class="control-label">GST Amount</label>
                                    <input type="text" class="form-control" id="txtIGSTAmount" name="txtIGSTAmount" placeholder="" />
                                    <span id="rGSTAmount" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group label-floating">
                                    <label class="control-label">Charges</label>
                                    <input type="text" class="form-control" id="txtICharges" name="txtICharges" placeholder="" />
                                    <span id="rCharges" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <label class="control-label">Total Amount</label>
                                    <input type="text" class="form-control" id="txtITotalAmount" name="txtITotalAmount" placeholder="" />
                                    <span id="rTotalAmount" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Remarks</label>
                                    <input type="text" class="form-control" id="txtRemarks" name="txtRemarks" placeholder="" />
                                    <span id="rRemarks" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Billing Remarks</label>
                                    <input type="text" class="form-control" id="txtBillings" name="txtBillings" placeholder="" />
                                    <span id="rBillings" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <label class="control-label">Status</label>
                                    <select id="ddlStatus" class="form-control"></select>
                                    <span id="rStatus" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <label class="control-label">Status Date</label>
                                    <input type="text" class="form-control" id="txtStatusDate" name="txtStatusDate" placeholder="" />
                                    <span id="rStatusDate" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Remarks</label>
                                    <input type="text" class="form-control" id="txtStatusRemarks" name="txtStatusRemarks" placeholder="" />
                                    <span id="rStatusRemarks" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                            </div>
                            <div class="col-sm-6"></div>
                            <div class="col-sm-3">
                                <button type="button" class="btn btn-primary pull-right" onclick="OnSave()">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="width: 55%">
            <div class="modal-content" id="SaleOroderDetailForm">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="txtMID" value="" />
                    <input type="hidden" id="Hidden2" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="Span2" class="new badge"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">Model #</label>
                                <input type="text" class="form-control" id="txtModel" readonly="readonly" name="txtModel" placeholder="" />
                                <span id="rModel" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary btn-sm pull-right" onclick="OnSearch()" style="margin: 25px 65px 0px 0px"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Part #</label>
                                <input type="text" class="form-control" id="txtPart" name="txtPart" placeholder="" />
                                <span id="rPart" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Default Description</label>
                                <input type="text" class="form-control" id="txtDefaultDescription" name="txtDefaultDescription" placeholder="" />
                                <span id="rDefaultDescription" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Amendments Description</label>
                                <input type="text" class="form-control" id="txtAmendmentsDescription" name="txtAmendmentsDescription" placeholder="" />
                                <span id="rAmendmentsDescription" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Unit</label>
                                <input type="text" class="form-control" id="txtUnit" name="txtUnit" placeholder="" />
                                <span id="rUnit" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Stock</label>
                                <input type="text" class="form-control" id="txtStock" name="txtStock" placeholder="" />
                                <span id="rStock" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Quantity</label>
                                <input type="text" class="form-control" id="txtQuantity" name="txtQuantity" placeholder="" />
                                <span id="rQuantity" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Before Discount Price</label>
                                <input type="text" class="form-control" id="txtBDPrice" name="txtBDPrice" placeholder="" />
                                <span id="rBDPrice" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Discount</label>
                                <input type="text" class="form-control" id="txtDiscount" name="txtDiscount" placeholder="" />
                                <span id="rDiscount" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Unit Price</label>
                                <input type="text" class="form-control" id="txtUnitPrice" name="txtUnitPrice" placeholder="" />
                                <span id="rUnitPrice" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">PKR Amount</label>
                                <input type="text" class="form-control" id="txtAmount" name="txtAmount" placeholder="" />
                                <span id="rAmount" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Remarks</label>
                                <input type="text" class="form-control" id="txtItemRemarks" name="txtItemRemarks" placeholder="" />
                                <span id="rItmeRemarks" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnSaveDetail()">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDelete2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H3">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDeleteDetail()">Confirm</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalView2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="OnClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H4"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <table id='tblItemList' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>Select</th>
                                        <th>ID</th>
                                        <th>Part#</th>
                                        <th>Model#</th>
                                        <th>Product</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalShow" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="width: 50%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="OnClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-content table-responsive">
                                <table id='tableData' class="table table-striped table-bordered display" style="width: 100%">
                                    <thead class="header bg-primary">
                                        <tr class="">
                                            <th>ID</th>
                                            <th>SalesOrder</th>
                                            <th>Customer</th>
                                            <th>Date</th>
                                            <th>Number</th>
                                            <th>Remarks</th>
                                            <th>Select</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function bindPrintClick() {
            var reportPath = '<%= System.Configuration.ConfigurationManager.AppSettings("ReportPath").ToString() %>'
            let salesOrderId = $(_ID).val();
            if (salesOrderId == "") {
                alert("ID is required")
            }
            else {

                $.ajax({
                    type: "POST",
                    url: "" + serviceurl + "/generateSalesOrderReport",
                    data: JSON.stringify({ salesOrderId: salesOrderId }),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (res) {
                        var data = JSON.parse(res.d);
                        if (data.Status) {
                            window.open(reportPath + '?isGeneratedByBuilder=' + false);
                            $("#FormError").css('display', 'none');
                            $("#Error").empty();
                        } else {
                            $("#Validation").fadeIn(1000);
                            $("#Validation").text(data.Message);
                            $("#Validation").fadeOut(5000);
                        }
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        function OnSelect() {
            $(tblname).on('click', '.select', function () {
                var row = $(this).closest('tr');
                var rowIndex = table.row(row).index();
                var rowData = table.row(rowIndex).data();
                $.ajax({
                    type: "POST",
                    url: "" + serviceurl + "/getSaleOrderById",
                    data: JSON.stringify({ ID: rowData.ID }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (responce) {
                        let salesOrder = JSON.parse(responce.d)
                        $(_ID).val(salesOrder.ID);
                        $("#SalesOrder").text(salesOrder.SalesOrder);
                        var javascriptDate = new Date(salesOrder.SalesDate);
                        javascriptDate = javascriptDate.getMonth() + 1 + "/" + javascriptDate.getDate() + "/" + javascriptDate.getFullYear();
                        $(_Date).val(javascriptDate);

                        $(_Customer).val(salesOrder.FK_CustomerID);
                        $(_BillTo).val(salesOrder.BillTo);
                        isValueExist(_BillTo, $(_BillTo).val())
                        $(_ShipTo).val(salesOrder.ShipTo);
                        isValueExist(_ShipTo, $(_ShipTo).val())
                        $(_SOType).val(salesOrder.OrderType);
                        $(_SalesPerson).val(salesOrder.FK_EmployeeID);
                        $(_Location).val(salesOrder.FK_LocationID);
                        $(_Remarks).val(salesOrder.Remarks);
                        $(_Billings).val(salesOrder.Billing);
                        $(_Terms).val(salesOrder.FK_Terms);
                        $(_Currency).val(salesOrder.FK_CurrencyID);
                        $(_txtCurrency).val(salesOrder.ExchangeRate);
                        $(_Status).val(salesOrder.FK_StatusID);
                        $(_StatusRemarks).val(salesOrder.StatusRemarks);

                        $(_IGST).val(salesOrder.Tax);
                        $(_ICharges).val(salesOrder.Charges);
                        OnLoadGrid3(salesOrder.ID);
                        isValueExist(_Remarks, $(_Remarks).val())
                        isValueExist(_Billings, $(_Billings).val())
                        isValueExist(_Status, $(_Status).val())
                        isValueExist(_StatusDate, $(_StatusDate).val())
                        isValueExist(_StatusRemarks, $(_StatusRemarks).val())
                        $("#modalShow").modal('hide');
                    },
                    error: OnErrorCall
                });

                //var javascriptDate2 = new Date(rowData.StatusDate);
                //javascriptDate2 = javascriptDate2.getMonth() + 1 + "/" + javascriptDate2.getDate() + "/" + javascriptDate2.getFullYear();
                //$(_StatusDate).val(javascriptDate2);

            });
        }
    </script>
</asp:Content>
