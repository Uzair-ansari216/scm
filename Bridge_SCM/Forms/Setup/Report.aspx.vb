﻿Public Class Report
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GC.Collect()
        GC.WaitForPendingFinalizers()
        GC.Collect()
        If Not IsPostBack Then

        End If



    End Sub
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function Save() As String

        Dim IDS As String = HttpContext.Current.Request.QueryString("id")
        Dim id() As String

        Dim ht As New Hashtable

        Dim report As String = HttpContext.Current.Request.QueryString("ddlReport")
        Dim ddlVoucherType As String = HttpContext.Current.Request.QueryString("ddlVoucherType")
        Dim ddlVoucherStatus As String = HttpContext.Current.Request.QueryString("ddlVoucherStatus")
        Dim ddlLocation As String = HttpContext.Current.Request.QueryString("ddlLocation")
        Dim ddlSegment As String = HttpContext.Current.Request.QueryString("ddlSegment")
        Dim ddlDepartment As String = HttpContext.Current.Request.QueryString("ddlDepartment")
        Dim ddlProject As String = HttpContext.Current.Request.QueryString("ddlProject")
        Dim ddlEmployee As String = HttpContext.Current.Request.QueryString("ddlEmployee")
        Dim ddlCustomer As String = HttpContext.Current.Request.QueryString("ddlCustomer")
        Dim ddlSupplier As String = HttpContext.Current.Request.QueryString("ddlSupplier")
        Dim ddlAccountCode As String = HttpContext.Current.Request.QueryString("ddlAccountCode")
        Dim ToDateWithTime As String = HttpContext.Current.Request.QueryString("ToDateWithTime")
        Dim FromDateWithTime As String = HttpContext.Current.Request.QueryString("FromDateWithTime")

        Dim Todates As String = HttpContext.Current.Request.QueryString("Todate")
        'Dim ToDateWithTime As DateTime = HttpContext.Current.Request.QueryString("ToDateWithTime")
        Dim timee As String = " 23:59:59"
        Dim Todate As String = Todates + timee

        Dim result As String = ""
        Dim mAccountCode As String = ""
        'Dim ObjACt As New BizSoft.Bridge_SCM.Task

        Dim objReport As New BizSoft.DBManager.clsReport.oReport
        Dim StrList As New Generic.List(Of String)

        Dim oCulture = New System.Globalization.CultureInfo("en-US", True)

        Dim selectionsql As String = ""
        Dim selectionsqlCode As String = ""
        objReport.Formula = ""

        If report = "-1" Then
            Return "Please Select Any Reports"
        End If

        If IDS = "" Then
            'Return "Please Select ChartofAccount"
        Else
            id = IDS.Split(",")
        End If

        Dim obj As New BizSoft.Bridge_SCM.Report()

        Dim dr As DataSet = obj.GetReportData(report)

        If dr.Tables(0).Rows.Count > 0 Then
            If dr.Tables(0).Rows(0)("AutoCalling").ToString() = "1" Then

                'If report = "RptMemberCountnewHourwise.rpt" Then
                Dim ds As DataSet = obj.GetReportParameter(report)
                If dr.Tables(0).Rows(0)("IsSubReport").ToString() = "1" Then
                    objReport.rptIsSubLoad = True
                    objReport.rptSubReportCollection.Add("1")
                End If
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim ObjectValue As String = System.Net.WebUtility.HtmlDecode(HttpContext.Current.Request.QueryString(ds.Tables(0).Rows(i)("ObjectName")))
                    Dim ParameterName As String = ds.Tables(0).Rows(i)("ParameterName")

                    If ObjectValue = "-1" Or ObjectValue = "undefined" Or ObjectValue = "" Or ObjectValue = " " Then
                        Return ds.Tables(0).Rows(i)("Msg")
                    End If

                    ht.Add(ParameterName, ObjectValue)
                Next

                'ht.Add("FromDate", ToDateWithTime)
                'ht.Add("Todate", ToDateWithTime)
                'End If
                'Dim ds As DataSet = obj.GetReportParameter(report)
                'For i = 0 To ds.Tables(0).Rows.Count - 1
                '    Dim ObjectValue As String = HttpContext.Current.Request.QueryString(ds.Tables(0).Rows(i)("ObjectName"))
                '    Dim ParameterName As String = ds.Tables(0).Rows(i)("ParameterName")

                '    ht.Add(ParameterName, ObjectValue)
                'Next

            End If

            'Dim iAction As String = ""
            'iAction = "Report Generated" & " " & dr.Tables(0).Rows(0)("ReportName").ToString()
            'BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Report", "Report", "", Environment.MachineName)

        End If

        objReport.strFileName = report
        HttpContext.Current.Session("ReportFileName") = report


        'ht.Add("ToDate", ToDate)
        'ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
        'ht.Add("User", HttpContext.Current.Session("UserID"))

        If dr.Tables(0).Rows(0)("IsSelectionFormula").ToString() = "1" Then
            selectionsql = SetCritria(report)
            selectionsql = selectionsqlCode & " " & selectionsql

            objReport.Formula = selectionsql
            objReport.rptCreteria = selectionsql
        Else

        End If
        'selectionsqlCode = SetCritriaCode(id)


        HttpContext.Current.Session("ogsReport") = objReport



        objReport.ht = ht
        objReport.StrList = StrList

        Return "1"

    End Function

    Public Shared Function SetCritria(ByVal reportsName As String) As String

        Dim _NA As String = HttpContext.Current.Request.QueryString("ddlNA")
        Dim _PA As String = HttpContext.Current.Request.QueryString("ddlPA")
        Dim _UC As String = HttpContext.Current.Request.QueryString("ddlUC")
        Dim _Ward As String = HttpContext.Current.Request.QueryString("ddlWard")
        Dim _Block As String = HttpContext.Current.Request.QueryString("ddlBlock")
        Dim _Area As String = HttpContext.Current.Request.QueryString("ddlArea")
        Dim _Street As String = HttpContext.Current.Request.QueryString("ddlStreet")

        Dim selectionsql As String = ""
        Dim AndFlag As Boolean = False

        If reportsName = "RptBlockCodeAS.rpt" Then
            selectionsql = "{VNSetup.PK_NA} <> -400"
            If _NA <> "-1" And _NA <> "" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_NA} = " & _NA
            End If

            If _PA <> "-1" And _PA <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_PA} = " & _PA
            End If

            If _UC <> "-1" And _UC <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PKUCID} = " & _UC
            End If

            If _Ward <> "-1" And _Ward <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PKWardID} = " & _Ward
            End If

            If _Block <> "-1" And _Block <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_Block} = " & _Block
            End If

            If _Area <> "" And _Area <> "undefined" Then
                selectionsql = selectionsql & " AND {GS_Area.PK_Area} >= " & _Area
            End If

            If _Street <> "" And _Street <> "undefined" Then
                selectionsql = selectionsql & " AND {GS_BlockCodeDetails.Fk_Street} = " & _Street
            End If

        ElseIf reportsName = "RptSetup.rpt" Then

            selectionsql = "{VNSetup.PK_NA} <> -400"

            If _NA <> "-1" And _NA <> "" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_NA} = " & _NA
            End If

            If _PA <> "-1" And _PA <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_PA} = " & _PA
            End If

            If _UC <> "-1" And _UC <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PKUCID} = " & _UC
            End If

            If _Ward <> "-1" And _Ward <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PKWardID} = " & _Ward
            End If

            If _Block <> "-1" And _Block <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_Block} = " & _Block
            End If

        ElseIf reportsName = "RptBlockDetail.rpt" Then
            selectionsql = "{VNSetup.PK_NA} <> -400"

            If _NA <> "-1" And _NA <> "" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_NA} = " & _NA
            End If

            If _PA <> "-1" And _PA <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_PA} = " & _PA
            End If

            If _UC <> "-1" And _UC <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PKUCID} = " & _UC
            End If

            If _Ward <> "-1" And _Ward <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PKWardID} = " & _Ward
            End If

            If _Block <> "-1" And _Block <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_Block} = " & _Block
            End If

        ElseIf reportsName = "RPPSAnalysis.rpt" Then

            selectionsql = "{VNSetup.PK_NA} <> -400"

            If _NA <> "-1" And _NA <> "" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_NA} = " & _NA
            End If

            If _PA <> "-1" And _PA <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_PA} = " & _PA
            End If

            If _UC <> "-1" And _UC <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PKUCID} = " & _UC
            End If

            If _Ward <> "-1" And _Ward <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PKWardID} = " & _Ward
            End If

            If _Block <> "-1" And _Block <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_Block} = " & _Block
            End If


        ElseIf reportsName = "RptProvinceAssemblyAnalysis.rpt" Then

            selectionsql = "{VNSetup.PK_NA} <> -400"

            If _NA <> "-1" And _NA <> "" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_NA} = " & _NA
            End If

            If _PA <> "-1" And _PA <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_PA} = " & _PA
            End If

            If _UC <> "-1" And _UC <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PKUCID} = " & _UC
            End If

            If _Ward <> "-1" And _Ward <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PKWardID} = " & _Ward
            End If

            If _Block <> "-1" And _Block <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_Block} = " & _Block
            End If

        ElseIf reportsName = "RptUCAnalysis.rpt" Then

            selectionsql = "{VNSetup.PK_NA} <> -400"

            If _NA <> "-1" And _NA <> "" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_NA} = " & _NA
            End If

            If _PA <> "-1" And _PA <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_PA} = " & _PA
            End If

            If _UC <> "-1" And _UC <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PKUCID} = " & _UC
            End If

            If _Ward <> "-1" And _Ward <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PKWardID} = " & _Ward
            End If

            If _Block <> "-1" And _Block <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_Block} = " & _Block
            End If

        ElseIf reportsName = "RptWardAnalysis.rpt" Then

            selectionsql = "{VNSetup.PK_NA} <> -400"

            If _NA <> "-1" And _NA <> "" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_NA} = " & _NA
            End If

            If _PA <> "-1" And _PA <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_PA} = " & _PA
            End If

            If _UC <> "-1" And _UC <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PKUCID} = " & _UC
            End If

            If _Ward <> "-1" And _Ward <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PKWardID} = " & _Ward
            End If

            If _Block <> "-1" And _Block <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_Block} = " & _Block
            End If

        ElseIf reportsName = "rptMemberList.rpt" Then

            selectionsql = "{VNSetup.PK_NA} <> -400"

            If _NA <> "-1" And _NA <> "" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_NA} = " & _NA
            End If

            If _PA <> "-1" And _PA <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_PA} = " & _PA
            End If

            If _UC <> "-1" And _UC <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PKUCID} = " & _UC
            End If

            If _Ward <> "-1" And _Ward <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PKWardID} = " & _Ward
            End If

            If _Block <> "-1" And _Block <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_Block} = " & _Block
            End If

        Else

            selectionsql = "{VNSetup.PK_NA} <> -400"

            If _NA <> "-1" And _NA <> "" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_NA} = " & _NA
            End If

            If _PA <> "-1" And _PA <> "undefined" Then
                selectionsql = selectionsql & " AND {VNSetup.PK_PA} = " & _PA
            End If

        End If





        Return selectionsql

    End Function

End Class