﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="Financial.aspx.vb" Inherits="Bridge_SCM.Financial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header card-header-text">
                    <h4 class="card-title" style="color: #000000">Financial</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-primary pull-right" id="btnAdddetail" _recordid="0" title="Add New Financial"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">Theme</label>
                                <select class="form-control" id="theme" title="Single Select"></select>
                            </div>
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-1" style="margin-top: 1.3em !important;">
                            <button type="button" class="btn btn-primary btn-sm pull-right" id="btnadd" title="Add New Theme"><i class="fas fa-plus"></i></button>
                        </div>
                        <div class="col-lg-3 col-md-2 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Document</label>
                                <select class="form-control" id="document" title="Single Select">
                                    <option value="0" disabled selected>-- Select Document --</option>
                                    <option value="1">P & L</option>
                                    <option value="2">BL</option>
                                    <option value="3">Note</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">From Date</label>
                                <input type="text" class="form-control datepicker" id="txtFromDate" name="txtFromDate" placeholder="" />
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">To Date</label>
                                <input type="text" class="form-control datepicker" id="txtToDate" name="txtToDate" placeholder="" />
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary pull-left" id="upmappedchartofaccount">Un-Mapped Chart Of Account</button>
                            <div class="col-sm-4 checkbox-radios">
                                <div class="checkbox">
                                    <label>
                                        <input id="chkconfiguration" type="checkbox" name="activeCheckboxes" />
                                        Configuration
                                    </label>
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary pull-right" id="generatetrailbalance">Generate Trail Balance</button>
                            <i id="refresh_financial_details" class="fas fa-sync-alt btn btn-primary pull-right" title="Refresh" style="padding-right: 15px; padding-left: 15px; padding-bottom: 18px;"></i>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">

                            <table id='tablefinancial' class="table table-striped table-bordered display">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th></th>
                                        <th>Code</th>
                                        <th>Parent Code</th>
                                        <th>Heading</th>
                                        <th>Debit</th>
                                        <th>Credit</th>
                                        <th>Net Amount</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>
                <%--</form> --%>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalAdd" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel">Financial Theme</h4>
                </div>
                <div class="modal-body" id="themeForm">

                    <input type="hidden" id="txtID" value="" />
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Name</label>
                                <input type="text" class="form-control" id="txttheme" name="txttheme" placeholder="" />
                                <span id="rTheme" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-fill btn-default" id="savetheme">Save changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalAddDetail" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="">Financial Detail</h4>
                </div>
                <div class="modal-body" id="financialDetailForm">

                    <input type="hidden" id="txtDetailID" value="0" />
                    <br />
                    <div class="row">
                        <div class="col-md-3">
                            <span>Parent Code:</span>
                        </div>
                        <div class="col-md-1" style="margin-left: -50px;">
                            <b><span id="spcode"></span></b>
                        </div>
                        <div class="col-md-3">
                            <span>Parent Name:</span>
                        </div>
                        <div class="col-md-6" style="margin-left: -50px;">
                            <b><span id="spname"></span></b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Code</label>
                                <input type="text" class="form-control" id="txtcode" name="txtcode" placeholder="" readonly="readonly" />
                                <span id="rCode" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Parent Code</label>
                                <input type="text" class="form-control" id="txtpcode" name="txtpcode" placeholder="" readonly="readonly" />
                                <span id="rPCode" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Heading</label>
                                <input type="text" class="form-control" id="txtheading" name="txtheading" placeholder="" />
                                <span id="rHeading" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Fixed Value</label>
                                <input type="text" class="form-control" id="fixedvalue" name="fixedvalue" placeholder="" />
                                <span id="rFixedValue" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Notes Code</label>
                                <input type="text" class="form-control" id="notescode" name="notescode" placeholder="" />
                                <span id="rNotesCode" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Add Code Value</label>
                                <select class="form-control" id="addtocode"></select>
                                <span id="rAddToCode" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="row">
                                <div id="demo-component" class="file-field">
                                    <div class="btn col-md-3"></div>
                                    <div class="file-path-wrapper col-md-8">
                                        <input type="text" id="bgcolor" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3 checkbox-radios" style="margin-top: 10px; width: auto">
                                <div class="togglebutton">
                                    <label>
                                        Impact
                                    <input type="checkbox" checked="" id="impact" />
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <input type="text" placeholder="" disabled="" class="form-control" id="impactvalue" value="1" />

                            </div>
                            <div class="col-sm-4 checkbox-radios">
                                <div class="checkbox">
                                    <label>
                                        <input id="chkshowinreport" type="checkbox" name="activeCheckboxes" />
                                        Show In Report
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4 checkbox-radios">
                                <div class="checkbox">
                                    <label>
                                        <input id="chkbold" type="checkbox" name="activeCheckboxes" />
                                        Bold
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-fill btn-default" id="savefinancialdetail">Save changes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="trailbalancemodal" data-backdrop="static" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document" style="margin-right: 700px;">
                <div class="modal-content modal-lg" style="width: 1168px">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="formModalLabel">Trail Balance</h4>
                    </div>
                    <div class="modal-body" id="trialBalanceForm">
                        <div class="row">
                            <div class="col-lg-12">
                                <table id='tabletrialBalance' class="table table-striped table-bordered display tabledataresponseview">
                                    <thead class="header bg-primary">
                                        <tr class="">
                                            <th></th>
                                            <th></th>
                                            <th>Head</th>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Debit</th>
                                            <th>Credit</th>
                                            <th>Debit</th>
                                            <th>Credit</th>
                                            <th>Debit</th>
                                            <th>Credit</th>
                                            <th>Parent Code</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-fill btn-default" id="maptrailbalance">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="unmappmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="H1">Delete</h4>
                    </div>
                    <div class="modal-body">

                        <h5><i class="fa fa-warning  text-danger"></i>Are you sure you want to un-mapped this record ?</h5>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-fill btn-default" onclick="ConfirmUnMapped()">Confirm</button>
                    </div>
                </div>
            </div>
        </div>



        <div class="modal fade" id="headingdetailmodal" data-backdrop="static" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="">Heading Details</h4>
                    </div>
                    <div class="modal-body" id="">
                        <div class="row">
                            <div class="col-lg-12">
                                <table id='tableheadingdetail' class="table table-striped table-bordered display w-100">
                                    <thead class="header bg-primary">
                                        <tr class="">
                                            <th>Account Code</th>
                                            <th>Account Name</th>
                                            <th>Debit</th>
                                            <th>Credit</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="un_mapped_coa_modal" data-backdrop="static" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document" style="margin-right: 700px;">
                <div class="modal-content modal-lg" style="width: 1168px">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="">Un-Mapped Chart Of Account</h4>
                    </div>
                    <div class="modal-body" id="">
                        <div class="row">
                            <div class="col-lg-12">
                                <table id='tableunmappedcoa' class="table table-striped table-bordered display">
                                    <thead class="header bg-primary">
                                        <tr class="">
                                            <th>Account Code</th>
                                            <th>Account Name</th>
                                            <th>Opening Debit</th>
                                            <th>Opening Credit</th>
                                            <th>Current Debit</th>
                                            <th>Current Credit</th>
                                            <th>Closing Debit</th>
                                            <th>Closing Credit</th>
                                            <th>Heading</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-fill btn-default" id="map_unmappped_coa">Map</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <script>
            let serviceUrl = "../../appServices/FinancialService.asmx"
            let financialMapping = new Array();
            var table, source;
            $(function () {
                bindGetFinancialThemes();
                bindAddFinancialTheme();
                bindSaveTheme();
                bindAddEditFinancialDetail();
                bindSaveFinancialDetail();
                bindGetFinancialDetails();
                bindDeleteFinancialDetail();
                bindGenerateTrialBalance();
                SaveFinancialMapping();
                bindUnMappTrialBalance();
                bindGetHeadingDetail();
                bindRefreshFinancialDetails();
                bindGetUnMappedChartOfAccount();

                demo.initFormExtendedDatetimepickers();

                isValueExist("#document", $("#document").val())
                $("#txtFromDate").val('<%= HttpContext.Current.Session("Datefrom").ToString().Split(" ")(0) %>')
                $("#txtToDate").val('<%= HttpContext.Current.Session("dateTo").ToString().Split(" ")(0) %>')

                $(document).on('change', "#tablefinancial tbody tr input[name='format']", function () {
                    if ($("#chkconfiguration:checked").length > 0) {
                        $.ajax({
                            url: serviceUrl + "/getFinancialMapping",
                            type: "post",
                            data: JSON.stringify({ financialtheme: $("#theme").val() }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                var data = JSON.parse(response.d);
                                var data = JSON.parse(response.d);
                                MakeDataGridForTrialBalance(data, $("#document").find("option:selected").text());
                                $("#trailbalancemodal").modal();
                            }
                        })
                    } else {

                    }
                })


                $("body").on("change", ".financialheading", function () {
                    let flag = true;
                    var mappingData = {
                        AccountCode: $(this).closest('tr').find('td:eq(0)').text(),
                        DetailID: $(this).val()
                    }
                    $.grep(financialMapping, function (key, value) {
                        if (key.AccountCode == mappingData.AccountCode) {
                            key.DetailID = mappingData.DetailID
                            flag = false;
                        }
                    })
                    if (flag) {
                        financialMapping.push(mappingData)
                        flag = true;
                        console.log(financialMapping)
                    }
                })

                $(document).on("click", "#map_unmappped_coa", function () {
                    //$("#tableunmappedcoa tbody tr").each(function (key, value) {
                    //    var mappingData = {
                    //        AccountCode: $(value).find("td:eq(0)").text(),
                    //        DetailID: $(value).find("td:eq(8) select").val()
                    //    }

                    //    financialMapping.push(mappingData)
                    //})
                    console.log(financialMapping)
                    $.ajax({
                        url: serviceUrl + "/financialHeadingMapping",
                        type: "post",
                        data: JSON.stringify({ model: financialMapping, financialtheme: $("#theme").val(), document: $("#document").val() }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var data = JSON.parse(response.d);
                            if (data.Status) {
                                MakeDataGridForFinancialDetail(data.GenericList)
                                $("#un_mapped_coa_modal").modal('hide');
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text(data.Message);
                                $("#Validation").fadeOut(5000);
                            }
                        }
                    })
                })

                $("body").on("change", "#impact", function () {
                    let val = $("#impact:checked").length
                    if (val > 0) {
                        $("#impactvalue").val('1')
                    } else {
                        $("#impactvalue").val('-1')
                    }
                })

                $('#demo-component').colorpicker({
                    component: '.btn'
                });
            })

            function bindGetFinancialThemes() {
                $.ajax({
                    url: serviceUrl + "/getFinancialThemes",
                    type: "post",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var data = JSON.parse(response.d);
                        $("#theme").empty().append("<option value='0' disabled selected>-- Select Theme --</option>");
                        $.each(data, function (index, item) {
                            $("#theme").append("<option value=" + item.Value + "> " + item.Text + "</option>");
                        });
                    }, complete: function () {
                        isValueExist("#theme", $("#theme").val())
                    }
                })
            }

            function bindAddFinancialTheme() {
                $("body").on("click", "#btnadd", function () {
                    $("#modalAdd").modal();
                })
            }

            function bindSaveTheme() {
                $("body").on("click", "#savetheme", function () {
                    if ($("#txttheme").val() != "") {
                        $.ajax({
                            url: serviceUrl + "/saveFinancialThemes",
                            data: JSON.stringify({ name: $("#txttheme").val() }),
                            type: "post",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                var data = JSON.parse(response.d);
                                $("#theme").empty().append("<option value='0' disabled selected>-- Select Theme --</option>");
                                $.each(data, function (index, item) {
                                    $("#theme").append("<option value=" + item.Value + "> " + item.Text + " </option>");
                                });
                                $("#modalAdd").modal('hide');
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text("Theme Save Successfully");
                                $("#Validation").fadeOut(5000);
                            }, complete: function () {
                                isValueExist("#theme", $("#theme").val())
                            }
                        })
                    } else {
                        alert("Name is requried")
                    }
                })
            }
            function bindAddEditFinancialDetail() {
                $("body").on("click", "#btnAdddetail , .edit", function () {
                    let id = $(this).attr("_recordid")
                    if ($("#theme").val() != null && $("#document").val() != null) {
                        let code = '';
                        let parentCode = '';
                        let selectedRow = $("#tablefinancial tbody tr").find('input[type="radio"]:checked').closest('tr')
                        if (selectedRow.length > 0) {
                            code = $(selectedRow).find('td:eq(1)').text()
                            parentCode = $(selectedRow).find('td:eq(2)').text()
                        }
                        $.ajax({
                            url: serviceUrl + "/addEditFinancialDetail",
                            data: JSON.stringify({ id: id, theme: $("#theme").val(), code: code, parentCode: parentCode }),
                            type: "post",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                var data = JSON.parse(response.d);
                                if (parseInt(id) > 0) {
                                    $("#txtDetailID").val(data.ID)
                                    $("#spcode").text(data.ParentCode)
                                    $("#spname").text(data.Heading)
                                    $("#txtcode").val(data.code)
                                    $("#txtpcode").val(data.ParentCode == "" ? data.ParentCode : data.ParentCode)
                                    $("#txtheading").val(data.Heading)
                                    $("#fixedvalue").val(data.FixedValue)
                                    $("#notescode").val(data.NotesCode)
                                    $("#bgcolor").val(data.RowColour)
                                    if (data.Impact == 1) {
                                        $("#impact").prop("checked", true)
                                        $("#impactvalue").val("1")
                                    } else {
                                        $("#impact").prop("checked", false)
                                        $("#impactvalue").val("-1")
                                    }
                                    if (data.IsShowInReport == 1) {
                                        $("#chkshowinreport").prop("checked", true)
                                    } else {
                                        $("#chkshowinreport").prop("checked", false)
                                    }
                                    if (data.IsBold == 1) {
                                        $("#chkbold").prop("checked", true)
                                    } else {
                                        $("#chkbold").prop("checked", false)
                                    }
                                } else {
                                    $("#txtDetailID").val(0)
                                    $("#spcode").text($(selectedRow).find('td:eq(1)').text())
                                    $("#spname").text($(selectedRow).find('td:eq(3)').text())
                                    $("#txtcode").val(data.code)
                                    $("#txtpcode").val($(selectedRow).find('td:eq(1)').text())
                                    $("#txtheading , #fixedvalue , #notescode").val("")
                                }
                                $("#addtocode").empty().append("<option value='0' disabled selected>-- Select Heading --</option>");
                                $.each(data.headingList, function (index, item) {
                                    $("#addtocode").append("<option value=" + item.Value + "> " + item.Text + "</option>");
                                });
                                $("#addtocode").val(data.AddToCode)
                                $("#impactvalue").parent().removeClass('form-group')
                                $("#modalAddDetail").modal();
                            }, complete: function () {
                                isValueExist("#txtcode", $("#txtcode").val())
                                isValueExist("#txtpcode", $("#txtpcode").val())
                                isValueExist("#txtheading", $("#txtheading").val())
                                isValueExist("#fixedvalue", $("#fixedvalue").val())
                                isValueExist("#notescode", $("#notescode").val())
                            }
                        })

                    } else {
                        alert("Please select theme and document")
                    }

                })
            }

            function bindSaveFinancialDetail() {
                $("body").on("click", "#savefinancialdetail", function () {
                    var formData = {
                        ID: $("#txtDetailID").val(),
                        Financial: $("#theme").val(),
                        Document: $("#document").val(),
                        code: $("#txtcode").val(),
                        ParentCode: $("#txtpcode").val(),
                        Heading: $("#txtheading").val(),
                        FixedValue: $("#fixedvalue").val(),
                        NotesCode: $("#notescode").val(),
                        AddToCode: $("#addtocode").val(),
                        Impact: $("#impact:checked").length,
                        IsShowInReport: $("#chkshowinreport:checked").length
                    }
                    $.ajax({
                        url: serviceUrl + "/saveFinancialDetail",
                        data: JSON.stringify({ model: formData, IsBold: $("#chkbold:checked").length, RowColour: $("#bgcolor").val() }),
                        type: "post",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var data = JSON.parse(response.d);
                            MakeDataGridForFinancialDetail(data)
                            $("#modalAddDetail").modal('hide');
                            $("#Validation").fadeIn(1000);
                            $("#Validation").text("Financial details Save Successfully");
                            $("#Validation").fadeOut(5000);
                        }
                    })

                })
            }

            function bindGetFinancialDetails() {
                $("body").on("change", "#theme , #document", function () {
                    let theme = $("#theme").val()
                    let document = $("#document").val()
                    if (theme != null && document != null) {
                        $.ajax({
                            url: serviceUrl + "/getFinancialDetail",
                            data: JSON.stringify({ theme: theme, document: document }),
                            type: "post",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                var data = JSON.parse(response.d);
                                MakeDataGridForFinancialDetail(data)
                            }
                        })
                    }
                })
            }

            function bindDeleteFinancialDetail() {
                $("body").on("click", ".del", function () {
                    let currentEle = $(this)
                    let id = currentEle.attr("_recordId");
                    $.ajax({
                        url: serviceUrl + "/deleteFinancialDetail",
                        data: JSON.stringify({ id: id }),
                        type: "post",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var data = JSON.parse(response.d);
                            if (data.Item2) {
                                currentEle.closest("tr").remove()
                            }
                            $("#Validation").fadeIn(1000);
                            $("#Validation").text(data.Item1);
                            $("#Validation").fadeOut(5000);
                        }
                    })
                })
            }

            function bindGenerateTrialBalance() {
                $("body").on("click", "#generatetrailbalance", function () {
                    let theme = parseInt($("#theme").val())
                    let document = parseInt($("#document").val())
                    let fromDate = $("#txtFromDate").val()
                    let toDate = $("#txtToDate").val()
                    //let code = '';
                    //let selectedRow = $("#tablefinancial tbody tr").find('input[type="radio"]:checked').closest('tr')
                    //if (selectedRow.length > 0) {
                    //    code = $(selectedRow).find('td:eq(1)').text()
                    //}
                    //if (selectedRow.length > 0) {
                    if (fromDate != "" && toDate != "" && theme > 0 && document > 0) {
                        $.ajax({
                            url: serviceUrl + "/generateTrialBalance",
                            data: JSON.stringify({ fromDate: fromDate, toDate: toDate, financialTheme: theme, document: document }),
                            type: "post",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                var data = JSON.parse(response.d);
                                if (data.Status) {
                                    MakeDataGridForFinancialDetail(data.GenericList)
                                    $("#Validation").fadeIn(1000);
                                    $("#Validation").text(data.Message);
                                    $("#Validation").fadeOut(5000);
                                }
                            }
                        })
                    } else {
                        alert("All above fields must be selected")
                    }
                    //} else {
                    //    alert("Financial detail must be selected")
                    //}
                })

            }

            function SaveFinancialMapping() {
                $("body").on("click", "#maptrailbalance", function () {
                    debugger
                    let documentType = $('#document').find('option:selected').text()
                    let financialMappingArray = new Array()
                    // $("#tabletrialBalance tbody tr").each(function (index, row) {
                    //if ($(row).find("input[type='checkbox']:checked").length > 0) {
                    $("#tabletrialBalance tbody tr").find("input[type='checkbox']:checked").each(function (innerIndex, innerRow) {
                        let formDetail = {
                            DetailID: $("#tablefinancial tbody input[type='radio']:checked").attr('id'),
                            AccountCode: $(innerRow).closest('tr').find("td:eq(3)").text(),
                            DebitAccount: documentType == 'P & L' ? $(innerRow).closest('tr').find("td:eq(7)").text() : $(innerRow).closest('tr').find("td:eq(9)").text(),
                            CreditAccount: documentType == 'P & L' ? $(innerRow).closest('tr').find("td:eq(8)").text() : $(innerRow).closest('tr').find("td:eq(10)").text()
                        }
                        financialMappingArray.push(formDetail)
                    })
                    //}
                    //})
                    console.log(financialMappingArray)
                    $.ajax({
                        url: "" + serviceUrl + "/MapTrialBalance",  //?control=" + JSON.stringify(controlsArray),
                        type: "POST",
                        data: JSON.stringify({ model: financialMappingArray, financialtheme: $("#theme").val(), document: $("#document").val() }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var data = JSON.parse(response.d);
                            MakeDataGridForTrialBalance(data.TrialBalanceList, $("#document").find("option:selected").text());
                            MakeDataGridForFinancialDetail(data.FinancialDetailList)
                        }, error: function (responce) {
                            //alert(responce)
                        }
                    })
                })
            }
            let accountCode;
            let currentState
            function bindUnMappTrialBalance() {
                $("body").on("click", ".unmapped", function () {
                    accountCode = $(this).closest('tr').find('td:eq(3)').text()
                    //currentState = $(this)//currentRow.find('a').attr('_doctype')
                    $("#unmappmodal").modal();
                })
            }

            function ConfirmUnMapped() {

                $.ajax({
                    url: serviceUrl + "/FinancialUnMapping",
                    type: "post",
                    data: JSON.stringify({ code: accountCode, detailId: $("#tablefinancial tbody input[type='radio']:checked").attr('id'), financialtheme: $("#theme").val(), document: $("#document").val() }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var data = JSON.parse(response.d);

                        //MakeDataGridForTrialBalance(data.TrialBalanceList, $("#document").find("option:selected").text());
                        MakeDataGridForFinancialDetail(data.FinancialDetailList)
                        $("#unmappmodal").modal('hide');
                    }
                })
            }

            function bindGetHeadingDetail() {
                $("body").on("click", ".detail", function () {
                    let id = $(this).attr("_recordId")
                    $.ajax({
                        url: serviceUrl + "/getHeadingDetail",
                        data: JSON.stringify({ id: id, theme: $("#theme").val() }),
                        type: "post",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var data = JSON.parse(response.d);
                            MakeDataGridForHeadingDetail(data)
                            //$("#headingdetailmodal .dt-buttons").prepend($("#tableheadingdetail_length").html()).find(">:first-child").css({"margin-right":"30px"})
                            //$("#headingdetailmodal .dt-buttons").append($("#tableheadingdetail_filter").html()).find(">:last-child").css({ "margin-left": "30px" })
                            //$("#tableheadingdetail_length , #tableheadingdetail_filter").html("")
                            $("#headingdetailmodal").modal();
                        }
                    })
                })
            }

            function bindRefreshFinancialDetails() {
                $("body").on("click", "#refresh_financial_details", function () {
                    let theme = parseInt($("#theme").val())
                    let document = parseInt($("#document").val())
                    if (theme > 0 && document > 0) {
                        $.ajax({
                            url: serviceUrl + "/refreshFinancialDetails",
                            data: JSON.stringify({ financialTheme: theme, document: document }),
                            type: "post",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                var data = JSON.parse(response.d);
                                MakeDataGridForFinancialDetail(data)
                            }
                        })
                    } else {
                        alert("Please select theme and document")
                    }
                })
            }

            function bindGetUnMappedChartOfAccount() {
                $("body").on("click", "#upmappedchartofaccount", function () {
                    let theme = parseInt($("#theme").val())
                    let document = parseInt($("#document").val())
                    if (theme > 0 && document > 0) {
                        $.ajax({
                            url: serviceUrl + "/getUnMappedChartOfAccount",
                            data: JSON.stringify({ financialTheme: theme, document: document }),
                            type: "post",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                var data = JSON.parse(response.d);
                                MakeDataGridForUnMappedChartOfAccount(data)
                                $("#un_mapped_coa_modal").modal();

                            }
                        })
                    } else {
                        alert("Please select theme and document")
                    }
                })
            }

            function MakeDataGridForFinancialDetail(datasource) {
                $("#tablefinancial").DataTable().destroy();
                $("#tablefinancial").DataTable({
                    "searching": true,
                    "bLengthChange": true,
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    "pageLength": 50,
                    "sSort": true,
                    "order": [0, 'desc'],
                    "bInfo": true,
                    dom: 'Blfrtip',
                    buttons: [
                         {
                             extend: 'print',
                             footer: true,
                             exportOptions: {
                                 columns: [1, 2, 3, 4, 5, 6]
                             },
                             title: 'Financial',
                             text: '<i class="fa fa-print"></i>  Print'

                         },
                         {
                             extend: 'excelHtml5',
                             footer: true,
                             exportOptions: {
                                 columns: [1, 2, 3, 4, 5, 6]
                             },
                             title: 'Financial',
                             text: '<i class="fa fa-file-excel"></i>  Excel',
                         },
                        {
                            extend: 'pdf',
                            footer: true,
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5, 6]
                            },
                            title: 'Financial',
                            text: '<i class="fa fa-file-pdf"></i>  PDF'
                        },
                    ],
                    "aaData": datasource,
                    "aoColumns": [

                        {
                            data: null,
                            width: "4%",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {

                                    let radio = '<div class="checkbox"><label><input type="radio" name="format" id="' + data.ID + '" /></label></div>'
                                    var html = radio;
                                    return html;
                                }
                                return data;
                            }
                        },
                                    { data: "code", width: "7%" },
                                    { data: "ParentCode", width: "10%" },
                                    { data: "Heading", width: "45%" },
                                    { data: "DebitAmount", width: "10%", className: "text-right" },
                                    { data: "CreditAmount", width: "10%", className: "text-right" },
                                    { data: "NetAmount", width: "10%", className: "text-right" },
                                    {
                                        data: null,
                                        sDefaultContent: "",
                                        width: "7%",
                                        "orderable": false,
                                        render: function (data, type, row) {
                                            if (type === 'display') {
                                                var detial = ' <a href="#" _recordId = ' + data.ID + ' class="detail" title="Detail"><i class="fas fa-align-center"></i></a>';
                                                var del = ' <a href="#" _recordId = ' + data.ID + ' class="del" title="Delete"><i class="fa fa-trash" ></i></a>';
                                                var edit = ' <a href="#" _recordId = ' + data.ID + ' class="edit" title="Edit"><i class="fas fa-pencil-alt" ></i></a>';
                                                var html = detial + edit + del;
                                                return html;
                                            }
                                            return data;
                                        }
                                    },
                    ], rowCallback: function (row, data) {
                        if (data.IsBold) {
                            $(row).addClass("text-bold");
                        }
                        if (data.RowColour != "") {
                            $(row).css("background", data.RowColour);
                        }
                    }

                });
            }

            //Multi level or herarchical Datatable datagrid
            function MakeDataGridForTrialBalance(dataSource, document) {
                $("#tabletrialBalance").DataTable().destroy();
                source = dataSource
                dataSource.forEach(value => checkChildren(dataSource, value))

                table = $("#tabletrialBalance")
                   .DataTable({
                       "searching": true,
                       "sSort": true,
                       "order": [0, 'desc'],
                       "fixedHeader": true,
                       dom: 'Bfrtip',
                       oLanguage: {
                           sEmptyTable: "No results found"
                       },
                       "lengthMenu": [10, 20, 50],
                       buttons: [
                            {
                                extend: 'print',
                                footer: true,
                                exportOptions: {
                                    columns: [0, 1, 3]
                                },
                                title: 'Trial Balance',
                                text: '<i class="fa fa-lg fa-print"></i>  Print'

                            },
                            {
                                extend: 'excelHtml5',
                                footer: true,
                                exportOptions: {
                                    columns: [0, 1, 3]
                                },
                                title: 'Trial Balance',
                                text: '<i class="fa fa-lg fa-file-excel"></i>  Excel',
                            },
                           {
                               extend: 'pdf',
                               footer: true,
                               exportOptions: {
                                   columns: [0, 1, 3]
                               },
                               title: 'Trial Balance',
                               text: '<i class="fa fa-lg fa-file-pdf"></i>  PDF'
                           },
                       ],
                       "aaData": dataSource,
                       "columns": [

                                       {
                                           "className": 'details-control',
                                           "orderable": false,
                                           "data": null,
                                           "defaultContent": '',
                                           "width": "3%",
                                           render: function (data, type, row) {
                                               if (type === 'display') {
                                                   var expandable = '<a href="#" class="" title="" _docType = "' + document + '"><i class="fas fa-plus-circle"></i></a>';
                                                   var html = expandable;
                                                   return html;
                                               }
                                               return data;
                                           }
                                       },
                                            {
                                                data: null,
                                                sDefaultContent: "",
                                                //"width": "3%",
                                                "orderable": false,
                                                render: function (data, type, row) {
                                                    if (type === 'display') {
                                                        let checkbox = '<div class="col-sm-3 checkbox-radios"><div class="checkbox"><label><input id="' + data.AcountCode + '" type="checkbox" name="selector" _docType = "' + document + '"><span class="checkbox-material"><span class="check"></span></span></label></div></div>'
                                                        var html = checkbox;
                                                        //if (document == "P & L" && (data.AcountCode == 4 || data.AcountCode == 5)) {
                                                        //    return html;
                                                        //} else if (document == "BL" && (data.AcountCode == 1 || data.AcountCode == 2 || data.AcountCode == 3)) {
                                                        //    return html;
                                                        //} else {
                                                        //    return "";
                                                        //}
                                                        //if ((document == "P & L" || document == "BL") && (data.AcountCode == 5)) {
                                                        //    return html;
                                                        //} else {
                                                        //    return "";
                                                        //}
                                                    }
                                                    return data;
                                                }
                                            },
                                       { data: "Heading" },
                                       { data: "AcountCode", title: "Code" },
                                       { data: "AccountName", title: "Name" },
                                       { data: "ODabit", title: "Opening Debit", className: "text-right" },
                                       { data: "OCredit", title: "Opening Credit", className: "text-right" },
                                       { data: "Debit", title: "Current Debit", className: "text-right" },
                                       { data: "Credit", title: "Current Credit", className: "text-right" },
                                       { data: "CDebit", title: "Closing Debit", className: "text-right" },
                                       { data: "CCredit", title: "Closing Credit", className: "text-right" },
                                       { data: "ParentCode", title: "Parent Code" },
                       ],
                       "columnDefs": [
                           { "targets": [11], "visible": false },
                           { "targets": [3], "width": "110px" },
                       ], rowCallback: function (row, data) {
                           $(row).attr('id', "Parent");
                       }, "createdRow": function (row, data, dataIndex) {
                           if (data.SAccount == 1) {
                               $(row).addClass('firstlevel');
                           } else if (data.SAccount == 2) {
                               $(row).addClass('secondlevel');
                           }
                       }
                   });
            }

            // Add event listener for opening and closing details
            $(document).on('click', '.details-control', function () {
                var tr = $(this).closest('tr');
                var row = $.fn.dataTable.Api(tr.closest('table')).row(tr); // table

                switch (row.data().SAccount) {
                    case "1":
                        if (tr.hasClass('secondlevel')) {
                            tr.removeClass('secondlevel').addClass('firstlevel')
                        } else {
                            tr.removeClass('firstlevel').addClass('secondlevel')
                        }
                        break;
                    case "2":
                        if (tr.hasClass('thirdlevel')) {
                            tr.removeClass('thirdlevel').addClass('secondlevel')
                        } else {
                            tr.removeClass('secondlevel').addClass('thirdlevel')
                        }
                        break;
                    case "3":
                        if (tr.hasClass('forthlevel')) {
                            tr.removeClass('forthlevel').addClass('thirdlevel')
                        } else {
                            tr.removeClass('thirdlevel').addClass('forthlevel')
                        }
                        break;
                    case "4":
                        if (tr.hasClass('fifthlevel')) {
                            tr.removeClass('fifthlevel').addClass('forthlevel')
                        } else {
                            tr.removeClass('forthlevel').addClass('fifthlevel')
                        } break;
                    case "5":
                        tr.removeClass('firstlevel').addClass('second')
                        break;
                    default:

                }

                if (row.child.isShown()) {  //tr.hasClass("shown")
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    row.child("").show();
                    row.child().find('td').append("<table id='etc" + row.data().AcountCode + "' class='table table-striped table-bordered display no-footer dataTable'></table>")
                    childTable(row.data(), "etc" + row.data().AcountCode, tr);
                    tr.addClass('shown');
                }

                if ($(this).closest('tr').hasClass('shown')) {
                    $(this).find('i').removeClass("fas fa-plus-circle").addClass("fas fa-minus-circle")
                } else {
                    $(this).find('i').removeClass("fas fa-minus-circle").addClass("fas fa-plus-circle")
                }

            });

            /* Child datatable */
            function childTable(dataSouce, id, currentRow) {
                return $('#' + id)
                    .on('init.dt', function (e, settings, json) {
                        $('#' + id + ' thead tr').remove();
                    })
                    .DataTable({
                        "searching": false,
                        "bLengthChange": false,
                        "sSort": true,
                        "order": [0, 'desc'],
                        "bPaginate": false,
                        oLanguage: {
                            sEmptyTable: "No results found"
                        },
                        "lengthMenu": [5, 10, 25],
                        "bInfo": false,
                        "aaData": dataSouce.children,
                        columns: [
                                        {
                                            "className": 'details-control',
                                            "orderable": false,
                                            "data": null,
                                            "defaultContent": '',
                                            render: function (data, type, row) {
                                                if (type === 'display') {
                                                    if (data.SAccount == 4) {
                                                        var expende = '<a href="#" class="" title="" _docType = "' + currentRow.find('a').attr('_doctype') + '" style="display:inline-block; vertical-align: middle;"><i class="fas fa-plus-circle"></i></a>';
                                                        if (currentRow.find('a').attr('_doctype') == "P & L" && (data.AcountCode.slice(0, 1) == 4 || data.AcountCode.slice(0, 1) == 5)) {
                                                            expende += '<div class="col-sm-3 checkbox-radios c-style"><div class="checkbox"><label><input id="' + data.AcountCode + '" type="checkbox" name="selector" ><span class="checkbox-material"><span class="check"></span></span></label></div></div>'
                                                        } else if (currentRow.find('a').attr('_doctype') == "BL" && (data.AcountCode.slice(0, 1) == 1 || data.AcountCode.slice(0, 1) == 2 || data.AcountCode.slice(0, 1) == 3)) {
                                                            expende += '<div class="col-sm-3 checkbox-radios c-style"><div class="checkbox"><label><input id="' + data.AcountCode + '" type="checkbox" name="selector" ><span class="checkbox-material"><span class="check"></span></span></label></div></div>'
                                                        } else {
                                                            expende += "";
                                                        }
                                                    } else {
                                                        var expende = '<a href="#" class="" title="" _docType = "' + currentRow.find('a').attr('_doctype') + '"><i class="fas fa-plus-circle"></i></a>';
                                                    }
                                                    var html = expende;
                                                    if (data.SAccount == 5) {
                                                        return "";
                                                    } else {
                                                        return html;
                                                    }
                                                }
                                                return data;
                                            }
                                        },
                                                   {
                                                       data: null,
                                                       sDefaultContent: "",
                                                       "orderable": false,
                                                       render: function (data, type, row) {
                                                           let documentType = currentRow.find('a').attr('_doctype')
                                                           if (type === 'display') {
                                                               let check = ""
                                                               if (currentRow.find('input[type="checkbox"]:checked').length > 0) {
                                                                   check = "checked"
                                                               }
                                                               let checkbox = '<div class="col-sm-3 checkbox-radios"><div class="checkbox"><label><input id="' + data.AcountCode + '" type="checkbox" ' + check + ' name="selector" _docType = "' + documentType + '"><span class="checkbox-material"><span class="check"></span></span></label></div></div>'
                                                               var html;
                                                               if (documentType == "P & L" && (data.AcountCode.slice(0, 1) == 4 || data.AcountCode.slice(0, 1) == 5)) {
                                                                   html = checkbox;
                                                               } else if (documentType == "BL" && (data.AcountCode.slice(0, 1) == 1 || data.AcountCode.slice(0, 1) == 2 || data.AcountCode.slice(0, 1) == 3)) {
                                                                   html = checkbox;
                                                               } else {
                                                                   html = "";
                                                               }
                                                               if (data.Heading == "" && data.SAccount == 5) {
                                                                   return html;
                                                               } else if (data.Heading != "" && data.SAccount == 5) {
                                                                   if (documentType == "P & L" && (data.AcountCode.slice(0, 1) == 4 || data.AcountCode.slice(0, 1) == 5)) {
                                                                       html = "<a href='#' id='" + data.AcountCode + "' class='unmapped' title='Un-Mapped' style='color: #ec0909'><i class='fa fa-times' style='margin-left: 14px'></i></a>";
                                                                   } else if (documentType == "BL" && (data.AcountCode.slice(0, 1) == 1 || data.AcountCode.slice(0, 1) == 2 || data.AcountCode.slice(0, 1) == 3)) {
                                                                       html = "<a href='#' id='" + data.AcountCode + "' class='unmapped' title='Un-Mapped' style='color: #ec0909'><i class='fa fa-times' style='margin-left: 14px'></i></a>";
                                                                   } else {
                                                                       html = "";
                                                                   }
                                                                   return html;
                                                               } else {
                                                                   return ""
                                                               }

                                                           }
                                                           return data;
                                                       }
                                                   },
                                        { data: "Heading" },
                                        { data: "AcountCode", },
                                        { data: "AccountName" },
                                        { data: "ODabit", className: "text-right" },
                                        { data: "OCredit", className: "text-right" },
                                        { data: "Debit", className: "text-right" },
                                        { data: "Credit", className: "text-right" },
                                        { data: "CDebit", className: "text-right" },
                                        { data: "CCredit", className: "text-right" },
                                        { data: "ParentCode", title: "Parent Code" },

                        ],
                        "columnDefs": [
                            { "targets": [0], "width": "3%" },
                            { "targets": [11], "visible": false },
                            { "targets": [3], "width": "110px" },
                        ], rowCallback: function (row, data) {
                            $(row).attr('id', "Child");
                        }, "createdRow": function (row, data, dataIndex) {
                            if (data.SAccount == 2) {
                                $(row).addClass('secondlevel');
                            } else if (data.SAccount == 3) {
                                $(row).addClass('thirdlevel');
                            } else if (data.SAccount == 4) {
                                $(row).addClass('forthlevel');
                            } else if (data.SAccount == 5) {
                                $(row).addClass('fifthlevel');
                            }
                        }
                    })

            }


            function MakeDataGridForHeadingDetail(datasource) {
                $("#tableheadingdetail").DataTable().destroy();
                $("#tableheadingdetail").DataTable({
                    "searching": true,
                    "bLengthChange": true,
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    "pageLength": 10,
                    "sSort": true,
                    "order": [0, 'desc'],
                    "bInfo": true,
                    dom: 'Blfrtip',
                    buttons: [
                         {
                             extend: 'print',
                             footer: true,
                             exportOptions: {
                                 columns: [1, 2, 3, 4]
                             },
                             title: 'Financial Heading Details',
                             text: '<i class="fa fa-print"></i>  Print'

                         },
                         {
                             extend: 'excelHtml5',
                             footer: true,
                             exportOptions: {
                                 columns: [1, 2, 3, 4]
                             },
                             title: 'Financial Heading Details',
                             text: '<i class="fa fa-file-excel"></i>  Excel',
                         },
                        {
                            extend: 'pdf',
                            footer: true,
                            exportOptions: {
                                columns: [1, 2, 3, 4]
                            },
                            title: 'Financial Heading Details',
                            text: '<i class="fa fa-file-pdf"></i>  PDF'
                        },
                    ],
                    "aaData": datasource,
                    "aoColumns": [
                                    { data: "code", width: "7%" },
                                    { data: "Heading", width: "30%" },
                                    { data: "DebitAmount", width: "10%", className: "text-right" },
                                    { data: "CreditAmount", width: "10%", className: "text-right" },
                    ],
                });
            }

            function MakeDataGridForUnMappedChartOfAccount(datasource) {

                let selectbox = '<select class="form-control financialheading">'
                selectbox += '<option value="0" disabled="" selected="">-- Select Heading --</option>'
                $(datasource.FinancialHeadingList).each(function (key, value) {
                    selectbox += '<option value="' + value.Value + '" >' + value.Text + '</option>'
                })
                selectbox += '</select>'

                $("#tableunmappedcoa").DataTable().destroy();
                $("#tableunmappedcoa").DataTable({
                    "searching": true,
                    "bLengthChange": true,
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    "pageLength": 10,
                    "sSort": true,
                    "order": [0, 'desc'],
                    "bInfo": false,
                    "aaData": datasource.TrialBalanceList,
                    "aoColumns": [
                                    { data: "AcountCode", title: "Acount Code" },
                                    { data: "AccountName", title: "Account Name", width: "30%" },
                                    { data: "ODabit", title: "O-Debit", width: "3%", className: "text-right" },
                                    { data: "OCredit", title: "O-Credit", width: "3%", className: "text-right" },
                                    { data: "Debit", title: "C-Debit", width: "5%", className: "text-right" },
                                    { data: "Credit", title: "C-Credit", width: "5%", className: "text-right" },
                                    { data: "CDebit", title: "C-Debit", width: "5%", className: "text-right" },
                                    { data: "CCredit", title: "C-Credit", width: "5%", className: "text-right" },
                                     {
                                         data: null,
                                         sDefaultContent: "",
                                         "width": "5%",
                                         "orderable": false,
                                         render: function (data, type, row) {
                                             return selectbox;
                                         }
                                     },
                    ],

                });
            }

            checkChildren = (data, value) => {

                if (data.filter(data_val => data_val.ParentCode == value.AcountCode).length > 0) {
                    value.children = data.filter(data_val => data_val.ParentCode == value.AcountCode)
                    value.children.forEach(child => {
                        source.forEach((ind, key) => ind.AcountCode === child.AcountCode ? source.splice(key, 1) : null);
                        checkChildren(data, child)

                    });

                } else {
                    value.children = [];
                }

            }
        </script>
</asp:Content>
