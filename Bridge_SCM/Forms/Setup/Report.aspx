﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="Report.aspx.vb" Inherits="Bridge_SCM.Report" %>

<%@ Import Namespace="System.Web.Optimization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../../Layout/multiplelist/select2/css/select2-bootstrap.min.css" rel="stylesheet" />
    <link href="../../Layout/multiplelist/select2/css/select2.css" rel="stylesheet" />
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/ReportScript")%>
    <script type="text/javascript">
        function Save() {
            var IDS = [];

            jQuery.each(data, function (index, item) {
                IDS.push(item.id);
            });

            var ddlVoucherType = $("#ddlVoucherType").val();
            var ddlVoucherStatus = $("#ddlVoucherStatus :selected").val();
            var ddlLocation = $("#ddlLocation :selected").val();
            var ddlSegment = $("#ddlSegment :selected").val();
            var ddlDepartment = $("#ddlDepartment :selected").val();
            var ddlProject = $("#ddlProject :selected").val();
            var ddlEmployee = $("#ddlEmployee :selected").val();
            var ddlCustomer = $("#ddlCustomer :selected").val();
            var ddlSupplier = $("#ddlSupplier :selected").val();
            var ddlReport = $("#ddlReports :selected").val();
            var ddlAccountCode = [];
            $("#ddlAccountCode option:selected").each(function () {
                var $this = $(this);
                if ($this.length) {
                    var selText = $this.val();
                    ddlAccountCode.push(selText);
                }
            });


            $(_FromTime).val("00:00:00")
            $(_ToTime).val("23:59:59")
            var Todate = $("#txtToDate").val();
            var Totime = "23:59:59";
            var Fromdate = $("#txtFromDate").val();
            var Fromtime = "00:00:00";
            var ToDateWithTime = Todate + ' ' + Totime;
            var FromDateWithTime = Fromdate + ' ' + Fromtime;



            urlname = "Save";
            //if (cmb1 == "-1") {
            //    alert("Please Select")
            //}
            //else if (cmb2 == "-1") {
            //    alert("Please Select")
            //}
            //else if (cmb3 == "-1") {
            //    alert("Please Select")
            //}
            //else if (cmb4 == "-1") {
            //    alert("Please Select")
            //}
            //else {
            $.ajax({
                type: "POST",
                url: '' + pagename + '/' + urlname + '?id=' + IDS + '&ddlVoucherType=' + encodeURIComponent(ddlVoucherType) + '&ddlVoucherStatus=' + encodeURIComponent(ddlVoucherStatus) +
                    '&ddlLocation=' + encodeURIComponent(ddlLocation) + '&ddlSegment=' + encodeURIComponent(ddlSegment) + '&ddlDepartment=' + encodeURIComponent(ddlDepartment) +
                    '&ddlProject=' + encodeURIComponent(ddlProject) + '&ddlEmployee=' + encodeURIComponent(ddlEmployee) + '&ddlCustomer=' + encodeURIComponent(ddlCustomer) + '&ddlSupplier=' + encodeURIComponent(ddlSupplier) + '&ddlReport=' + encodeURIComponent(ddlReport) +
                '&ddlAccountCode=' + ddlAccountCode + '&ToDateWithTime=' + encodeURIComponent(ToDateWithTime) + '&FromDateWithTime=' + encodeURIComponent(FromDateWithTime),
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    var data = res.d;
                    if (data == "1") {
                        window.open("../Ctrls/ReportViewer.aspx");
                        $("#FormError").css('display', 'none');
                        $("#Error").empty();

                    } else {
                        alert(data);
                    }
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        }
        //}

    </script>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header card-header-text">
                    <h4 class="card-title">Report</h4>
                </div>
                <input type="hidden" id="ReportType" />
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Voucher Type</label>
                                <select id="ddlVoucherType" class="form-control">
                                </select>
                                <span id="rVoucherType" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Voucher Status</label>
                                <select id="ddlVoucherStatus" class="form-control">
                                </select>
                                <span id="rVoucherStatus" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Location</label>
                                <select id="ddlLocation" class="form-control">
                                </select>
                                <span id="rLocation" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Segment</label>
                                <select id="ddlSegment" class="form-control">
                                </select>
                                <span id="rSegment" class="new badge  hide">required</span>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Department</label>
                                <select id="ddlDepartment" class="form-control">
                                </select>
                                <span id="rDepartment" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Project</label>
                                <select id="ddlProject" class="form-control">
                                </select>
                                <span id="rProject" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Employee</label>
                                <select id="ddlEmployee" class="form-control">
                                </select>
                                <span id="rEmployee" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Customer</label>
                                <select id="ddlCustomer" class="form-control">
                                </select>
                                <span id="rCustomer" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Supplier</label>
                                <select id="ddlSupplier" class="form-control">
                                </select>
                                <span id="rSupplier" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Reports Category</label>
                                <select id="ddlReportCategory" class="form-control">
                                </select>
                                <span id="rReportCategory" class="new badge  hide">required</span>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Reports</label>
                                <select id="ddlReports" class="form-control">
                                </select>
                                <span id="rReports" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group label-floating">
                                <label class="control-label">From Date</label>
                                <input type="text" class="form-control" id="txtFromDate" name="txtFromDate" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group label-floating">
                                <label class="control-label">To Date</label>
                                <input type="text" class="form-control" id="txtToDate" name="txtToDate" placeholder="" />
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-fill btn-default" onclick="selectAll('ProductDropdown',true)">Select All</button>
                            <button type="button" class="btn btn-fill btn-default" onclick="selectAll(document.getElementById('ProductDropdown'),false)">UnSelect All</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8" id="ddlAccountCode">
                            <fieldset class="form-group">
                                <legend></legend>
                            </fieldset>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-fill btn-default" onclick="Save()">Print</button>
                        </div>
                    </div>
                    <div class="col-sm-6 cmb1">
                        <div class="form-group label-floating">
                            <label class="control-label lblcmb1"></label>
                            <select id="cmb1" class="form-control">
                            </select>
                            <span id="Span1" class="new badge  hide">required</span>
                        </div>
                    </div>
                    <div class="col-sm-6 cmb2">
                        <div class="form-group label-floating">
                            <label class="control-label lblcmb2"></label>
                            <select id="cmb2" class="form-control">
                            </select>
                            <span id="Span2" class="new badge  hide">required</span>
                        </div>
                    </div>
                    <div class="col-sm-6 cmb3">
                        <div class="form-group label-floating">
                            <label class="control-label lblcmb3"></label>
                            <select id="cmb3" class="form-control">
                            </select>
                            <span id="Span3" class="new badge  hide">required</span>
                        </div>
                    </div>
                    <div class="col-sm-6 cmb4">
                        <div class="form-group label-floating">
                            <label class="control-label lblcmb3"></label>
                            <select id="cmb4" class="form-control">
                            </select>
                            <span id="Span4" class="new badge  hide">required</span>
                        </div>
                    </div>

                    <div class="col-sm-6 txt1">
                        <div class="form-group label-floating">
                            <label class="control-label lbltxt1"></label>
                            <input type="text" class="form-control" id="txt1" name="txt1" placeholder="" />
                        </div>
                    </div>
                    <div class="col-sm-6 txt2">
                        <div class="form-group label-floating">
                            <label class="control-label lbltxt2"></label>
                            <input type="text" class="form-control" id="txt2" name="txt2" placeholder="" />
                        </div>
                    </div>
                    <div class="col-sm-6 txt3">
                        <div class="form-group label-floating">
                            <label class="control-label lbltxt3"></label>
                            <input type="text" class="form-control" id="txt3" name="txt3" placeholder="" />
                        </div>
                    </div>
                    <div class="col-sm-6 txt4">
                        <div class="form-group label-floating">
                            <label class="control-label lbltxt4"></label>
                            <input type="text" class="form-control" id="txt4" name="txt4" placeholder="" />
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</asp:Content>
