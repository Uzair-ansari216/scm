﻿Imports System.Reflection

Public Class PurchaseOrder
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FillDropDowns()
    End Sub

    Public Sub FillDropDowns()
        Dim objPurchaseOrder As New BizSoft.Bridge_SCM.PurchaseOrder


        Dim dtFromOrBillTo As DataSet = objPurchaseOrder.GetFromToOrBillTo()
        cboFromBillTo.DataSource = dtFromOrBillTo
        cboFromBillTo.DataBind()
        cboFromBillTo.DataTextField = "CompanyName"
        cboFromBillTo.DataValueField = "CompanyID"
        cboFromBillTo.DataBind()



        Dim dtSupplier As DataSet = objPurchaseOrder.GetSuppliers()
        cboBillTo.DataSource = dtSupplier
        cboBillTo.DataBind()
        cboBillTo.DataTextField = "SupplierName"
        cboBillTo.DataValueField = "SupplierID"
        cboBillTo.Items.Insert(0, New ListItem("Please Select", "0"))
        cboBillTo.DataBind()

        Dim dtCustomer As DataSet = objPurchaseOrder.GetCustomer()
        cboShipTo.DataSource = dtCustomer
        cboShipTo.DataBind()
        cboShipTo.DataTextField = "CompanyName"
        cboShipTo.DataValueField = "CompanyID"
        cboShipTo.Items.Insert(0, New ListItem("Please Select", "0"))
        cboShipTo.DataBind()


        Dim dtCurrency As DataSet = objPurchaseOrder.GetCurrencies()
        cboCurrency.DataSource = dtCurrency
        cboCurrency.DataBind()
        cboCurrency.DataTextField = "Currency"
        cboCurrency.DataValueField = "PK_CurrencyID"
        cboCurrency.Items.Insert(0, New ListItem("Please Select", "0"))
        cboCurrency.DataBind()



        Dim dtStatus As DataSet = objPurchaseOrder.GetPOStatus()
        cboPOStatus.DataSource = dtStatus
        cboPOStatus.DataBind()
        cboPOStatus.DataTextField = "StatusName"
        cboPOStatus.DataValueField = "StatusID"
        cboPOStatus.Items.Insert(0, New ListItem("Please Select", "0"))
        cboPOStatus.DataBind()


        Dim dtRegion As DataSet = objPurchaseOrder.GetReason()
        cboRegion.DataSource = dtRegion
        cboRegion.DataBind()
        cboRegion.DataTextField = "StatusName"
        cboRegion.DataValueField = "StatusID"
        cboRegion.Items.Insert(0, New ListItem("Please Select", "0"))
        cboRegion.DataBind()

        Dim dtSignature As DataSet = objPurchaseOrder.GetSignatures()
        cboSignature.DataSource = dtSignature
        cboSignature.DataBind()
        cboSignature.DataTextField = "PersonName"
        cboSignature.DataValueField = "PK_Signature"
        cboSignature.Items.Insert(0, New ListItem("Please Select", "0"))
        cboSignature.DataBind()


        Dim dtLocation As DataSet = objPurchaseOrder.GetLocations()
        cboLocation.DataSource = dtLocation
        cboLocation.DataBind()
        cboLocation.DataTextField = "Name"
        cboLocation.DataValueField = "ID"
        cboLocation.DataBind()

    End Sub

End Class