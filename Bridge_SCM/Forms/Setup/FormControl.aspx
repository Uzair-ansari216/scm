﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="FormControl.aspx.vb" Inherits="Bridge_SCM.FormControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%--<%: Scripts.Render("~/bundles/BrandScript")%>--%>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header card-header-text">
                    <h4 class="card-title" style="color: #000000">Form Control</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                        <%--  <div class="col-sm-3">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnAdd()"><i class="fas fa-plus"></i></button>
                        </div>--%>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <%--<label for="txtAccountCode" class="control-label">Menu</label>--%>
                                <select id="ddlcontrol" class="form-control"></select>
                                <span id="rcontrol" class="new hide"></span>

                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-lg-12">

                            <table id='tableCity' class="table ">
                                <thead class="bg-primary">
                                    <tr class="table-bordered">
                                        <th>Object Name</th>
                                        <th>Label</th>
                                        <th>ToolTip</th>
                                        <th>Mandatory</th>
                                        <th>Mandatory Msg</th>
                                        <th>Field Type</th>
                                        <th>Max Length</th>
                                        <th>Type validation Msg</th>
                                    </tr>
                                </thead>
                                <tbody id="controlBody"></tbody>
                            </table>


                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <button type="button" class="btn btn-primary pull-right" id="btnupdate" title="Update"><i class="fas fa-save"></i></button>
                        </div>
                    </div>
                </div>
                <%--</form> --%>
            </div>
        </div>
    </div>

    <script>
        var serviceUrl = "../../appServices/CommonService.asmx";
        $(function () {
            bindMenues()
            bindGetControlsByMenu()
            bindUpdateControlsContent()

            $("body #controlBody .check").on("change", function () {
                alert("dasds")
            })
        })
        function bindMenues() {
            $.ajax({
                url: "" + serviceUrl + "/getMenuList",
                type: "POST",
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    $("#ddlcontrol").empty();
                    $("#ddlcontrol").append("<option value='0' selected>-- Select Menu --</option>");
                    jQuery.each(JSON.parse(res.d), function (index, item) {
                        $("#ddlcontrol").append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });
                    $("#ddlcontrol option:selected").text("-- Select Menu --").trigger("focus")
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            })
        }

        function bindGetControlsByMenu() {
            $("#ddlcontrol").on("change", function () {
                var form = $(this).find("option:selected").text().trim()
                $.ajax({
                    url: "" + serviceUrl + "/getFormControls",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify({ form: form }),
                    dataType: "json",
                    success: function (res) {
                        $("#controlBody").html("")
                        var i = 1
                        jQuery.each(JSON.parse(res.d), function (index, item) {
                            var row = " <tr>" +
            "<td><input type='text' class='form-control' id='txtObjectName' name='txtObjectName' placeholder='' disabled='disabled' Value='" + item.ObjectName + "' /></td>" +
            "<td><input type='text' class='form-control' id='txtLabel' name='txtLabel' placeholder='' Value='" + item.Label + "' /></td>" +
            "<td><input type='text' class='form-control' id='txtToolTip' name='txtToolTip' placeholder='' Value='" + item.ToolTip + "'/></td>" +
            "<td><div class='checkbox' style='margin-left: 35px;'><label><input type='checkbox' name='mandatory" + i + "' class='mandatory'><span class='checkbox-material'><span class='check'></span></span></label></div></td>" +
            "<td><input type='text' class='form-control' id='txtMandatoryMsg' name='txtMandatoryMsg' placeholder='' Value='" + item.MendatoryMessage + "'/></td>" +
            "<td><select id='fieldType' class='form-control fieldType'><option value='0'>-- Select --</option><option value='text'>Text</option><option value='number'>Number</option><option value='email'>Email</option></select></td>" +
            "<td><input type='text' class='form-control' id='txtMaxLength' name='txtMaxLength' placeholder='' Value='" + item.MaximumLength + "'/></td>" +
            "<td><input type='text' class='form-control' id='txtTypeValidMsg' name='txtTypeValidMsg' placeholder='' Value='" + item.FieldTypeValidMessage + "'/></td>" +
           "</tr>"
                            $("#controlBody").append(row)
                            if (item.IsMendatory) {
                                $("#controlBody").find("tr").eq(index).find("td").eq(3).find('input').attr('checked', 'checked')
                            }
                            if ($("#controlBody").find("tr").eq(index).find("td").eq(5).find('select option:nth-child(2)').val() == item.FieldType) {
                                $("#controlBody").find("tr").eq(index).find("td").eq(5).find('select option:nth-child(2)').attr("selected", "selected")
                            }
                            if ($("#controlBody").find("tr").eq(index).find("td").eq(5).find('select option:nth-child(3)').val() == item.FieldType) {
                                $("#controlBody").find("tr").eq(index).find("td").eq(5).find('select option:nth-child(3)').attr("selected", "selected")
                            }
                            if ($("#controlBody").find("tr").eq(index).find("td").eq(5).find('select option:nth-child(4)').val() == item.FieldType) {
                                $("#controlBody").find("tr").eq(index).find("td").eq(5).find('select option:nth-child(4)').attr("selected", "selected")
                            }
                            i++
                        });

                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                })
            })
        }

        function bindUpdateControlsContent() {
            $("#btnupdate").click(function () {
                if ($("#controlBody tr").length > 0) {
                    var controlsArray = new Array()
                    var i = 1
                    $("#controlBody tr").each(function (index, row) {
                        if ($(row).attr('type') != 'hidden') {
                            var formDetail = {
                                ObjectName: $(row).find("td").eq(0).find('input').val(),
                                Label: $(row).find("td").eq(1).find('input').val(),
                                ToolTip: $(row).find("td").eq(2).find('input').val(),
                                IsMendatory: $(row).find("td").eq(3).find('input[name=mandatory' + i + ']:checked').length,
                                MendatoryMessage: $(row).find("td").eq(4).find('input').val(),
                                FieldType: $(row).find("td").eq(5).find('select').val(),
                                MaximumLength: $(row).find("td").eq(6).find('input').val(),
                                FieldTypeValidMessage: $(row).find("td").eq(7).find('input').val(),
                                MenuName: $("#ddlcontrol").find("option:selected").text()
                            }
                            i++
                            if (formDetail.IsMendatory == 1) {
                                formDetail.IsMendatory = true
                            } else {
                                formDetail.IsMendatory = false
                            }
                            controlsArray.push(formDetail)

                        }
                    })

                    $.ajax({
                        url: "" + serviceUrl + "/UpdateControlContent",  //control=" + JSON.stringify(controlsArray),
                        type: "post",
                        data: JSON.stringify({ model: controlsArray }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (responce) {
                            var data = JSON.parse(responce.d);
                            if (!data.Status) {
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text(data.Message);
                                $("#Validation").fadeOut(5000);
                                $("#modalAdd").modal('hide');
                            } else {
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text("Record has been updated successfully");
                                $("#Validation").fadeOut(5000);
                            }

                        }, error: function (responce) {
                            //alert(responce)
                        }
                    })
                } else {
                    alert("Select menu to continue")
                }

            })
        }
    </script>
</asp:Content>

