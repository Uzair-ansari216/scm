﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="ReportSetup.aspx.vb" Inherits="Bridge_SCM.ReportSetup" %>

<%@ Import Namespace="System.Web.Optimization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/ReportSetupScript")%>
    <script src="../../FormScript/ValidationScript.js"></script>


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header card-header-text">
                    <h4 class="">Report Setup</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnAdd()"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">

                            <table id='tableCity' class="table table-striped table-bordered display">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>Report Module</th>
                                        <th>Report Category</th>
                                        <th>Report Name</th>
                                        <th>IsActive</th>
                                        <th>Report ID</th>
                                        <th>AutoCalling</th>
                                        <th>IsSubReport</th>
                                        <th>FKReportCategory</th>
                                        <th>IsSelectionFormula</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>
                <%--</form> --%>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body" id="modal-body-Setup">

                    <input type="hidden" id="txtID" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="ValidationSummary" class="new badge"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Report Module</label>
                                <select id="ddlReportModule" class="form-control">
                                </select>
                                <span id="rReportModule" class="new badge  hide">required</span>
                                <input type="hidden" id="hdnreportpath" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Report Category</label>
                                <select id="ddlReportCategory" class="form-control">
                                </select>
                                <span id="rReportCategory" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Report Name</label>
                                <input type="text" class="form-control" id="txtName" name="txtName" placeholder="" />
                                <span id="rName" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Report ID</label>
                                <input type="text" readonly="readonly" class="form-control" id="txtReportID" name="txtReportID" placeholder="" />
                                <span id="rReportID" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <input type="file" name="imgfile" accept=".rpt" onchange="showname()" id="imgfile" />
                            <span id="imgLabel"></span>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-sm-2 checkbox-radios">
                            <div class="checkbox">
                                <label>
                                    <input id="chkActive" type="checkbox" name="activeCheckboxes" />
                                    Active
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-3 checkbox-radios">
                            <div class="checkbox">
                                <label>
                                    <input id="chkAutoCalling" checked="checked" type="checkbox" name="activeCheckboxes" />
                                    Auto Calling
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-3 checkbox-radios">
                            <div class="checkbox">
                                <label>
                                    <input id="chkSubReport" type="checkbox" name="activeCheckboxes" />
                                    Sub Report
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-4 checkbox-radios">
                            <div class="checkbox">
                                <label>
                                    <input id="chkSelectionFormula" type="checkbox" name="activeCheckboxes" />
                                    Selection Formula
                                </label>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Report Connection Type</label>
                        </div>

                        <div class="col-sm-3 checkbox-radios">
                            <input type="hidden" id="hndConnectionType" />
                            <div class="">
                                <label>
                                    <input id="chkRportConType" class="chkRportConType" type="radio" name="activeCheckboxes" _type="odbc" />
                                    ODBC
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-3 checkbox-radios">
                            <div class="">
                                <label>
                                    <input id="chkRportConType" class="chkRportConType" type="radio" name="activeCheckboxes" _type="oledb" />
                                    OLEDB
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnSave()">Save changes</button>
                </div>
            </div>
        </div>
    </div>


    <%--Setup Parameter Modal--%>
    <div class="modal fade" id="modalParameter" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H2"></h4>
                </div>
                <div class="modal-body" id="modal-body-Param">

                    <input type="hidden" id="txtMasterID" value="" />
                    <input type="hidden" id="txtDetailID" value="" />
                    <input type="hidden" id="txtMode" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="Validation2" class="new badge"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Parameter Name</label>
                                <%--     <select id="ddlParameterName" class="form-control"></select>
                                <input type="hidden" class="form-control" id="txtParameterName" name="txtObjectType" placeholder="" />--%>
                                <input list="ddlParameterName" name="browser" class="form-control ddlParameterName" />
                                <datalist id="ddlParameterName" class="form-control" style="display: none">
                                    <%-- <option value="Internet Explorer"></option>
                                    <option value="Firefox"></option>
                                    <option value="Chrome"></option>
                                    <option value="Opera"></option>
                                    <option value="Safari"></option>--%>
                                </datalist>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Object Type</label>
                                <select id="ddlParameterObjectType" class="form-control ddlObjectType"></select>
                                <%--<input type="text" class="form-control" id="txtObjectType" name="txtObjectType" placeholder="" />--%>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Object Name</label>
                                <select id="ddlParameterObjectName" class="form-control ddlObjectName"></select>
                                <%--<input type="text" class="form-control" id="txtObjectType" name="txtObjectType" placeholder="" />--%>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <div class="form-group label-floating">
                                    <label class="control-label">Data Type</label>
                                    <input type="text" class="form-control" id="txtDataType" name="txtDataType" placeholder="" />
                                </div>
                                <span id="rDataType" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <div class="form-group label-floating">
                                    <label class="control-label">Message</label>
                                    <input type="text" class="form-control" id="txtMsg" name="txtMsg" placeholder="" />
                                </div>
                                <span id="rMsg" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnSaveDetail()">Save changes</button>
                </div>
                <hr />
                <div class="row">
                    <div class="modal-body">
                        <div class="col-lg-12">

                            <table id='tableDetail' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>ReportID</th>
                                        <th>Data Type</th>
                                        <th>Parameter</th>
                                        <th>Object Name</th>
                                        <th>Msg</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--Setup Parameter Modal End--%>

    <%-- Selection Formula Pop Up Modal Section--%>

    <div class="modal fade" id="modalSelectionFormula" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="title"></h4>
                </div>
                <div class="modal-body" id="modal-body-SF">

                    <input type="hidden" id="txtMasterID" value="" />
                    <input type="hidden" id="txtDetailID" value="" />
                    <input type="hidden" id="txtMode" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="Validation2" class="new badge"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <div class="form-group label-floating">
                                    <label class="control-label">Report</label>
                                    <input type="text" class="form-control" id="txtReport" name="txtReport" placeholder="" readonly="readonly" />
                                </div>
                                <span id="rReport" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Object Type</label>
                                <select id="ddlObjectType" class="form-control ddlObjectType">
                                    <%--<option value="">-- Select Object Type--</option>
                                    <option value="Dropdown">ComboBox Control</option>--%>
                                    <%-- <option value="Textbox">TextBox Control</option>
                                    <option value="Date">Date/Time Control</option>--%>
                                </select>
                                <%--<input type="text" class="form-control" id="txtObjectType" name="txtObjectType" placeholder="" />--%>
                            </div>
                        </div>
                        <div class="col-sm-4">

                            <div class="form-group label-floating">
                                <label class="control-label">Object Name</label>
                                <select id="ddlObjectName" class="form-control ddlObjectName"></select>
                                <%--<input type="text" class="form-control" id="txtObjectName" name="txtObjectName" placeholder="" />--%>
                            </div>
                            <span id="rObjectName" class="new badge  hide">required</span>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <div class="form-group label-floating">
                                    <label class="control-label">Selection Formula</label>
                                    <input type="text" class="form-control" id="txtSelectionFormula" name="txtSelectionFormula" placeholder="" />
                                </div>
                                <span id="rSelectionFormula" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" id="saveSelectionFormula">Save changes</button>
                </div>

                <hr />
                <div class="row">
                    <div class="modal-body">
                        <div class="col-lg-12">

                            <table id='tableselectionformula' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>ID</th>
                                        <th>Report</th>
                                        <th>Object</th>
                                        <th>Selection Formula</th>
                                        <th></th>

                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDeleteSF" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H3">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDeleteSF()">Confirm</button>
                </div>
            </div>
        </div>
    </div>
    <%-- End Selection Formula Pop Up Modal Section --%>
    <div class="modal fade" id="modalDelete2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H3">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDeleteDetail()">Confirm</button>
                </div>
            </div>
        </div>
    </div>

    <%--Setup Detail Modal--%>
    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H4"></h4>
                </div>
                <div class="modal-body" id="modal-body-Detail">

                    <input type="hidden" id="txtMaster" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="Span1" class="new badge"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">

                            <div class="form-group label-floating">
                                <label class="control-label">Object Type</label>
                                <select id="detailddlObjectType" class="form-control ddlObjectType" _type="objectType"></select>
                                <%--<input type="text" class="form-control" id="txtObjectType" name="txtObjectType" placeholder="" />--%>
                            </div>
                            <span id="rObjectType" class="new badge  hide">required</span>

                        </div>
                        <div class="col-sm-4">

                            <div class="form-group label-floating">
                                <label class="control-label">Object Name</label>
                                <select id="detailddlObjectName" class="form-control ddlObjectName" _type="objectName"></select>
                                <%--<input type="text" class="form-control" id="txtObjectName2" name="txtObjectName2" placeholder="" />--%>
                            </div>
                            <span id="rObjectName2" class="new badge  hide">required</span>

                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <div class="form-group label-floating">
                                    <label class="control-label">Label</label>
                                    <input type="text" class="form-control" id="txtLabel" name="txtLabel" placeholder="" />
                                </div>
                                <span id="rLabel" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="DefaultValue">
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <div class="form-group label-floating">
                                    <label class="control-label">Default Value</label>
                                    <input type="text" class="form-control" id="txtDefaultValue" name="txtDefaultValue" placeholder="" />
                                </div>
                                <span id="rDefaultValue" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <div class="form-group label-floating">
                                    <label class="control-label">Query</label>
                                    <input type="text" class="form-control" id="txtQuery" name="txtQuery" placeholder="" />
                                </div>
                                <span id="rQuery" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <div class="form-group label-floating">
                                    <label class="control-label">Session Variable</label>
                                    <select id="ddlSessionVariable" class="form-control  multi-select" name="ddlSessionVariable[]" style="width: 100%;"></select>
                                </div>
                                <span id="rsessionvariable" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnSaveDetail2()">Save changes</button>
                </div>
                <hr />
                <div class="row">
                    <div class="modal-body">
                        <div class="col-lg-12">

                            <table id='tblDetail' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>ReportID</th>
                                        <th>Object Name</th>
                                        <th>ObjectType</th>
                                        <th>Query</th>
                                        <th>Label</th>
                                        <th>Default Value</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDelete()">Confirm</button>
                </div>
            </div>
        </div>
    </div>
    <%--Setupt Detail Modals--%>

    <div class="modal fade" id="modalDelete3" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H5">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDeleteDetail2()">Confirm</button>
                </div>
            </div>
        </div>
    </div>

    <%-- Store Procedure Modal --%>
    <div class="modal fade" id="modalStoreProcedure" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="titleSP"></h4>
                </div>
                <div class="modal-body" id="modal-body-SP">


                    <input type="hidden" id="txtMode" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="Validation2" class="new badge"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <div class="form-group label-floating">
                                    <label class="control-label">Report</label>
                                    <input type="text" class="form-control" id="txtReportsp" name="txtReportsp" placeholder="" readonly="readonly" />
                                </div>
                                <span id="rReport" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <div class="form-group label-floating">
                                    <label class="control-label">Store Procedure</label>
                                    <input type="text" class="form-control" id="txtSP" name="txtSP" placeholder="" />
                                </div>
                                <span id="rtxtSP" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <div class="form-group label-floating">
                                    <label class="control-label">Execution Order</label>
                                    <input type="number" class="form-control" id="txtsort" name="txtsort" placeholder="" />
                                </div>
                                <span id="rSort" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" id="saveStoreProcedure">Save changes</button>
                </div>

                <hr />
                <div class="row">
                    <div class="modal-body">
                        <div class="col-lg-12">

                            <table id='tablestoreprocedure' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>ID</th>
                                        <th>Report</th>
                                        <th>Store Procedure</th>
                                        <th>Exec Order</th>
                                        <th></th>

                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalDeleteSp" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H3">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDeleteSP()">Confirm</button>
                </div>
            </div>
        </div>
    </div>

    <%-- Store Procedure Modal --%>
    <script>
        $(function () {
            $("body").on("change", "#ddlObjectName", function () {
                var selectedObject = $(this).val()
                $("#tableselectionformula tr").each(function (index, row) {
                    if ($(row).find('td:nth-child(3)').text() == selectedObject) {
                        alert("This object is already selected")
                        $('select[id="ddlObjectName"]').find('option[value="0"]').attr("selected", true);
                    }
                })
            })

        })

        //changes
        function bindSaveSelectionFormula() {
            $("body").on("click", "#saveSelectionFormula", function () {
                debugger
                var spFound;
                var formdata = $("#frmselectionformula").serialize();
                //  $.getScript('../../FormScript/ValidationScript.js', function () {
                spFound = valid("modal-body-SF");

                SelectionFormulaFormValidaton();

                var dataSend = {
                    ReportID: $("#txtReport").val(),
                    ObjectName: $("#ddlObjectName").val(),
                    SelectionFormula: $("#txtSelectionFormula").val(),

                }

                if (msg == "") {
                    if (spFound == false) {
                        $.ajax({
                            type: "POST",
                            url: "" + serviceurl + "/SaveSelectionFormula",
                            data: JSON.stringify({ model: dataSend }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                if (response.d == "sessionNotFound") {
                                    alert("sessionNotFound");
                                    window.location.href = loginUrl;
                                } else {
                                    var data = JSON.parse(response.d);
                                    console.log(data);
                                    if (data.Message == "success") {

                                        ClearFields();
                                        $("#Validation").fadeIn(1000);
                                        $("#Validation").text(success);
                                        $("#Validation").fadeOut(5000);
                                        //OnLoadGrid();
                                        $("#modalSelectionFormula").modal('hide');
                                    } else if (data.Message == "exist") {
                                        $("#Validation").fadeIn(1000);
                                        $("#Validation").text(exist);
                                        $("#Validation").fadeOut(5000);
                                        // OnLoadGrid();
                                        $("#modalAdd").modal('hide');
                                    } else if (data.Message == "update") {
                                        $("#Validation").fadeIn(1000);
                                        $("#Validation").text(update);
                                        $("#Validation").fadeOut(5000);
                                        // OnLoadGrid();
                                        $("#modalSelectionFormula").modal('hide');
                                    }
                                    else if (data.Message == "failed") {
                                        $("#Validation").fadeIn(1000);
                                        $("#Validation").text(failedmsg);
                                        $("#Validation").fadeOut(5000);
                                        // OnLoadGrid();
                                        $("#modalSelectionFormula").modal('hide');
                                    }
                                }
                            },
                            error: OnErrorCall
                        });
                    }
                    else {
                        alert("Illegal Characters Detected!")
                    }
                } else {

                }
                //});
            })
        }
        var spFound;
        var valid = function (id) {
            spFound = false;
            var inputTags = $("#" + id).find('input')
            for (var i = 0, length = inputTags.length; i < length; i++) {
                if (inputTags[i].type == 'text') {
                    if (inputTags[i].value == "") {
                        spFound = true;
                    }
                }
            }

            var input = $(".modal-body").find('textarea')
            for (var i = 0, length = input.length; i < length; i++) {
                if (input[i].value == "") {
                    spFound = true;
                }
            }
            return spFound;
        }




        //delete report detail changes

        var Del2 = ""
        var ObjectNamee = "";
        function OnDeleteDetail2() {
            $(tblDetail2).on('click', '.del', function () {
                var row = $(this).closest('tr');
                var rowIndex = table3.row(row).index();
                var rowData = table3.row(rowIndex).data();
                Del2 = rowData.ReportID;
                ObjectNamee = rowData.ObjectName;

                if (Del2 != "" && ObjectNamee != "") {
                    $("#modalDelete3").modal();
                }

            });
        }

        function OnConfirmDeleteDetail2() {

            DeleteDetail2(Del2, ObjectNamee);
        }

        function DeleteDetail2(id, ObjectName) {
            if (id != "") {
                debugger
                $.ajax({
                    type: "POST",
                    url: "" + serviceurl + "/OnDeleteDetail2",
                    data: JSON.stringify({ dataID: id, ObjectName: ObjectName }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        if (response.d == "sessionNotFound") {
                            alert("sessionNotFound");
                            window.location.href = loginUrl;
                        } else {
                            var data = JSON.parse(response.d);
                            console.log(data);
                            if (data.Message == "success") {
                                $("#Span1").fadeIn(1000);
                                $("#Span1").text(delt);
                                $("#Span1").fadeOut(5000);
                                OnLoadGrid3(MasterID2);
                                $("#modalDelete3").modal('hide');
                            } else if (data.Message == "failed") {
                                $("#Span1").fadeIn(1000);
                                $("#Span1").text(failedmsg);
                                $("#Span1").fadeOut(5000);
                            } else if (data.Message == "use") {
                                $("#Span1").fadeIn(1000);
                                $("#Span1").text("It is used by another process, Please try again!");
                                $("#Span1").fadeOut(5000);
                            }

                        }
                    },
                    error: OnErrorCall
                });
            } else {
                alert("Please select a record to delete!")
            }
        }


        function OnSave() {
            var spFound;
            $.getScript('../../FormScript/ValidationScript.js', function () {
                spFound = valid();

                FormValidaton();
                var isActive, autoCalling, isSubReport, isSelectionFormula;
                if ($(_chkActive).is(':checked')) {
                    isActive = 1;
                }
                else {
                    isActive = 0
                }

                if ($(_chkAutoCalling).is(':checked')) {
                    autoCalling = 1;
                }
                else {
                    autoCalling = 0
                }

                if ($(_chkSubReport).is(':checked')) {
                    isSubReport = 1;
                }
                else {
                    isSubReport = 0
                }

                if ($(_chkSelectionFormula).is(':checked')) {
                    isSelectionFormula = 1;
                }
                else {
                    isSelectionFormula = 0
                }

                var dataSend = {
                    ID: $(_ID).val(),
                    ReportName: $(_Name).val(),
                    IsActive: isActive,
                    ReportID: $(_ReportID).val(),
                    AutoCalling: autoCalling,
                    IsSubReport: isSubReport,
                    IsSelectionFormula: isSelectionFormula,
                    FKReportCategory: $(_ReportCategory).val(),
                    FKReportModule: $(_ReportModule).val(),
                    ReportConnectionType: $(_ConnectionType).val(),
                    ReportPath: $(_ReportPath).val()
                }

                if (msg == "") {
                    if (spFound == false) {
                        $.ajax({
                            type: "POST",
                            url: "" + serviceurl + "/OnSave",
                            data: JSON.stringify({ model: dataSend }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {

                                if (response.d == "sessionNotFound") {
                                    alert("sessionNotFound");
                                    window.location.href = loginUrl;
                                } else {
                                    var data = JSON.parse(response.d);
                                    console.log(data);
                                    if (data.Message == "success") {
                                        var data = new FormData()
                                        var files = $("#imgfile").get(0).files;
                                        if (files.length > 0) {
                                            data.append("UploadedImage", files[0])
                                        }

                                        sendFile(data);

                                        ClearFields();
                                        $("#Validation").fadeIn(1000);
                                        $("#Validation").text(success);
                                        $("#Validation").fadeOut(5000);
                                        OnLoadGrid();
                                        $("#modalAdd").modal('hide');
                                    } else if (data.Message == "exist") {
                                        $("#Validation").fadeIn(1000);
                                        $("#Validation").text(exist);
                                        $("#Validation").fadeOut(5000);
                                        OnLoadGrid();
                                        $("#modalAdd").modal('hide');
                                    } else if (data.Message == "update") {
                                        $("#Validation").fadeIn(1000);
                                        $("#Validation").text(update);
                                        $("#Validation").fadeOut(5000);
                                        OnLoadGrid();
                                        $("#modalAdd").modal('hide');
                                    }
                                    else if (data.Message == "failed") {
                                        $("#Validation").fadeIn(1000);
                                        $("#Validation").text(failedmsg);
                                        $("#Validation").fadeOut(5000);
                                        OnLoadGrid();
                                        $("#modalAdd").modal('hide');
                                    }
                                }
                            },
                            error: OnErrorCall
                        });
                    }
                    else {
                        alert("Illegal Characters Detected!")
                    }
                } else {

                }
            });
        }

        function OnDeleteDetail2() {
            $(tblDetail4).on('click', '.delSF', function () {
                debugger
                var row = $(this).closest('tr');
                var rowIndex = table4.row(row).index();
                var rowData = table4.row(rowIndex).data();
                Del2 = rowData.ID;
                ObjectNamee = rowData.ObjectName;

                if (Del2 != "" && ObjectNamee != "") {
                    $("#modalDeleteSF").modal();
                }

            });
        }

        function DeleteSelectionFormula(id) {
            if (id != "") {
                $.ajax({
                    type: "POST",
                    url: "" + serviceurl + "/OnDeleteSelectionFormula",
                    data: JSON.stringify({ dataID: id }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        if (response.d == "sessionNotFound") {
                            alert("sessionNotFound");
                            window.location.href = loginUrl;
                        } else {
                            var data = JSON.parse(response.d);
                            console.log(data);
                            if (data.Message == "success") {
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text(delt);
                                $("#Validation").fadeOut(5000);
                                $("#modalDeleteSF").modal('hide');
                                $("#modalSelectionFormula").modal('hide');

                            } else if (data.Message == "failed") {
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text(failedmsg);
                                $("#Validation").fadeOut(5000);
                            } else if (data.Message == "use") {
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text("It is used by another process, Please try again!");
                                $("#Validation").fadeOut(5000);
                            }
                        }
                    },
                    error: OnErrorCall
                });
            } else {
                alert("Please select a record to delete!")
            }
        }




        //setup Detail 
        function OnDetail() {
            $(tblname).on('click', '.view2', function () {
                debugger
                var row = $(this).closest('tr');
                var rowIndex = table.row(row).index();
                var rowData = table.row(rowIndex).data();
                //console.log(rowData);
                $(_Master).val(rowData.ReportID);
                MasterID2 = rowData.ReportID;
                $("#txtLabel, #txtQuery").val("")
                $("#modal-body-Detail #detailddlObjectType").find('option').remove().end().append("<option value ='0'>-- Select --</option>")
                .append("<option value='Dropdown'>ComboBox Control</option>")
                .append("<option value='Textbox'>TextBox Control</option>")
                .append("<option value='Date'>Date/Time Control</option>")
                $("#modal-body-Detail #detailddlObjectName").find('option').remove().end().append("<option value ='0'>-- Select --</option>")

                $.ajax({
                    type: "POST",
                    url: "" + serviceurl + "/getSessionVariables",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (responce) {
                        //$("#ddlSessionVariable").append("<option value='0'>-- Select Variable --</option>")
                        $.each(JSON.parse(responce.d), function (index, row) {
                            $("#ddlSessionVariable").append("<option value=" + row.Text + ">" + row.Text + "</option>")
                        })
                    },
                    error: function (responce) {

                    }
                })

                OnLoadGrid3(rowData.ReportID);

                ClearDetailFields2();
                $("#H4").text("Detail");
                $("#DefaultValue").hide()
                isValueExist("#detailddlObjectType", $("#detailddlObjectType").val())
                isValueExist("#detailddlObjectName", $("#detailddlObjectName").val())
                $("#modalDetail").modal();
            });
        }


        function OnSaveDetail2() {
            var spFound;
            $.getScript('../../FormScript/ValidationScript.js', function () {
                spFound = valid("modal-body-Detail");

                DetailFormValidaton2();
                debugger
                var dataSend = {
                    ReportID: $(_Master).val(),
                    ObjectName: $("#detailddlObjectName").val(),
                    ObjectType: $("#detailddlObjectType").val(),
                    DefaultValue: $("#txtDefaultValue").val(),
                    Query: $(_Query).val(),
                    Label: $(_Label).val(),
                    SessionVariable: $("#ddlSessionVariable").val()
                }
                if (msg == "") {
                    if (spFound == false) {
                        $.ajax({
                            type: "POST",
                            url: "" + serviceurl + "/OnSaveDetail2",
                            data: JSON.stringify({ model: dataSend }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                if (response.d == "sessionNotFound") {
                                    alert("sessionNotFound");
                                    window.location.href = loginUrl;
                                } else {
                                    var data = JSON.parse(response.d);
                                    console.log(data);
                                    if (data.Message == "success") {
                                        ClearDetailFields2();
                                        OnLoadGrid3(MasterID2);
                                        $("#Span1").fadeIn(1000);
                                        $("#Span1").text(success);
                                        $("#Span1").fadeOut(5000);
                                    } else if (data.Message == "exist") {
                                        OnLoadGrid3(MasterID2);
                                        $("#Span1").fadeIn(1000);
                                        $("#Span1").text(exist);
                                        $("#Span1").fadeOut(5000);
                                    } else if (data.Message == "update") {
                                        OnLoadGrid3(MasterID2);
                                        ClearDetailFields2();
                                        $("#Span1").fadeIn(1000);
                                        $("#Span1").text(update);
                                        $("#Span1").fadeOut(5000);
                                    }
                                }
                            },
                            error: OnErrorCall
                        });
                    }
                    else {
                        alert("Illegal Characters Detected!")
                    }
                } else {

                }
            });
        }

        var DeleteID = "";
        function OnDelete() {
            $("body #tblDetail").on('click', '.del', function () {
                debugger

                DeleteID = $(this).attr("_recordId")
                ObjectNamee = $(this).closest('tr').find('td:nth-child(1)').text()
                if (DeleteID != "") {
                    $("#modalDelete").modal();
                }

            });
        }

        function OnConfirmDelete() {
            Delete(DeleteID, ObjectNamee);
        }

        function Delete(id, ObjectNamee) {
            if (id != "") {
                $.ajax({
                    type: "POST",
                    url: "" + serviceurl + "/DeleteReportDetail",
                    data: JSON.stringify({ dataID: id, objectName: ObjectNamee }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        if (response.d == "sessionNotFound") {
                            alert("sessionNotFound");
                            window.location.href = loginUrl;
                        } else {
                            var data = JSON.parse(response.d);
                            console.log(data);
                            if (data.Message == "success") {
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text(delt);
                                $("#Validation").fadeOut(5000);
                                OnLoadGrid3(data.ID);
                                $("#modalDelete").modal('hide');

                            } else if (data.Message == "failed") {
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text(failedmsg);
                                $("#Validation").fadeOut(5000);
                            } else if (data.Message == "use") {
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text("It is used by another process, Please try again!");
                                $("#Validation").fadeOut(5000);
                            }
                        }
                    },
                    error: OnErrorCall
                });
            } else {
                alert("Please select a record to delete!")
            }
        }


        //Setup Detail End

        //Setup Parameter 
        function OnParameter() {
            $(tblname).on('click', '.view', function () {
                debugger
                var row = $(this).closest('tr');
                var rowIndex = table.row(row).index();
                var rowData = table.row(rowIndex).data();
                //console.log(rowData);
                $(_MasterID).val(rowData.ReportID);
                MasterID = rowData.ReportID;
                GetParameterByReport(rowData.ReportID)
                OnLoadGrid2(rowData.ReportID);
                $(_Mode).val("Add");
                $("#txtDataType, #txtMsg").val("")
                $("#modal-body-Param #ddlParameterObjectType").find('option').remove().end().append("<option value ='0'>-- Select --</option>")
                .append("<option value='Dropdown'>ComboBox Control</option>")
                .append("<option value='Textbox'>TextBox Control</option>")
                .append("<option value='Date'>Date/Time Control</option>")
                .append("<option value='Session'>Session Variables</option>")
                $("#modal-body-Param #ddlParameterObjectName").find('option').remove().end().append("<option value ='0'>-- Select --</option>")
                $("#H2").text("Parameters");

                //$("#ddlParameterObjectType").find("option:selected").trigger("change")
                $("#modalParameter").modal();
            });
        }

        function OnSaveDetail() {
            var spFound;
            $.getScript('../../FormScript/ValidationScript.js', function () {
                spFound = valid();

                DetailFormValidaton();
                debugger
                var dataSend = {
                    ReportID: $(_MasterID).val(),
                    DataType: $(_DataType).val(),
                    ParameterName: $(".ddlParameterName").val(),//$(_ParameterName).val(),
                    ObjectName: $("#ddlParameterObjectName").val(),
                    Msg: $(_Msg).val(),
                    Mode: $(_Mode).val()
                }
                if (msg == "") {
                    //if (spFound == false) {
                    $.ajax({
                        type: "POST",
                        url: "" + serviceurl + "/OnSaveDetail",
                        data: JSON.stringify({ model: dataSend }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            if (response.d == "sessionNotFound") {
                                alert("sessionNotFound");
                                window.location.href = loginUrl;
                            } else {
                                var data = JSON.parse(response.d);
                                console.log(data);
                                if (data.Message == "success") {
                                    ClearDetailFields();
                                    OnLoadGrid2(MasterID);
                                    $("#Validation2").fadeIn(1000);
                                    $("#Validation2").text(success);
                                    $("#Validation2").fadeOut(5000);
                                } else if (data.Message == "exist") {
                                    OnLoadGrid2(MasterID);
                                    $("#Validation2").fadeIn(1000);
                                    $("#Validation2").text(exist);
                                    $("#Validation2").fadeOut(5000);
                                } else if (data.Message == "update") {
                                    OnLoadGrid2(MasterID);
                                    ClearDetailFields();
                                    $("#Validation2").fadeIn(1000);
                                    $("#Validation2").text(update);
                                    $("#Validation2").fadeOut(5000);
                                }
                            }
                        },
                        error: OnErrorCall
                    });
                    //}
                    //else {
                    //    alert("Illegal Characters Detected!")
                    //}
                } else {

                }
            });
        }


        function DetailFormValidaton() {
            msg = "";

            $("#rDataType").addClass("hide");

            if ($(_DataType).val() == "") {
                msg += "Data Type is required";
                $("#rDataType").removeClass("hide");
            } else {
                $("#rDataType").addClass("hide");
            }

            $("#rParameterName").addClass("hide");

            if ($(_ParameterName).val() == "") {
                msg += "Parameter Name is required";
                $("#rParameterName").removeClass("hide");
            } else {
                $("#rParameterName").addClass("hide");
            }

            $("#rObjectName").addClass("hide");

            if ($(_ObjectName).val() == "") {
                msg += "Object Name is required";
                $("#rObjectName").removeClass("hide");
            } else {
                $("#rObjectName").addClass("hide");
            }

            $("#rMsg").addClass("hide");

            if ($(_Msg).val() == "") {
                msg += "Msg is required";
                $("#rMsg").removeClass("hide");
            } else {
                $("#rMsg").addClass("hide");
            }

            if (msg == "") {
                $("#ValidationSummary2").text("");
            } else {
                $("#ValidationSummary2").text("Please fill required values.");
                alert("Please fill required values.");
            }
        }
        //Setup Parameter End 

        function bindOnChangeEvent() {
            $("body").on("change", ".ddlObjectType", function () {
                var type = $(this).val();
                var modalBody = $(this).parents().eq(3).attr('id')
                var i = 1;
                if (type == "Dropdown") {
                    $("#" + modalBody + " .ddlObjectName").find("option").remove().end().append("<option value ='0'>-- Select --</option>");
                    while (i < 5) {
                        $("#" + modalBody + " .ddlObjectName").append("<option value ='cmb" + i + "'>ComboBox " + i + "</option>");
                        i++;
                    }
                    $("#DefaultValue").hide()
                } else if (type == "Textbox") {
                    $("#" + modalBody + " .ddlObjectName").find("option").remove().end().append("<option value ='0'>-- Select --</option>");
                    while (i < 5) {
                        $("#" + modalBody + " .ddlObjectName").append("<option value ='txt" + i + "'>TextBox " + i + "</option>");
                        i++;
                    }
                    $("#DefaultValue").show()
                }
                else if (type == "Session") {
                    $("#" + modalBody + " .ddlObjectName").find("option").remove().end().append("<option value ='0'>-- Select --</option>");
                    $("#" + modalBody + " .ddlObjectName").append("<option value ='txtUserId'>User ID</option>");
                    $("#" + modalBody + " .ddlObjectName").append("<option value ='txtUserName'>User Name</option>");
                    $("#" + modalBody + " .ddlObjectName").append("<option value ='txtCompanyId'>Company ID</option>");
                    $("#" + modalBody + " .ddlObjectName").append("<option value ='txtCompanyName'>Company Name</option>");
                    $("#" + modalBody + " .ddlObjectName").append("<option value ='txtLocationId'>Location ID</option>");
                    $("#" + modalBody + " .ddlObjectName").append("<option value ='txtIsPowereUser'>Is Power User</option>");
                    $("#" + modalBody + " .ddlObjectName").append("<option value ='txtDate'>Date</option>");
                    $("#" + modalBody + " .ddlObjectName").append("<option value ='txtFinancialYearId'>Financial Year Id</option>");
                    $("#" + modalBody + " .ddlObjectName").append("<option value ='txtFinancialYear'>Financial Year Title</option>");
                    $("#" + modalBody + " .ddlObjectName").append("<option value ='txtFromDate'>From Date</option>");
                    $("#" + modalBody + " .ddlObjectName").append("<option value ='txtToDate'>To Date</option>");
                    $("#DefaultValue").hide()
                }
                else {
                    $("#" + modalBody + " .ddlObjectName").find("option").remove().end().append("<option value ='0'>-- Select --</option>");
                    $("#" + modalBody + " .ddlObjectName").append("<option value ='FromDate'>From Date</option>");
                    $("#" + modalBody + " .ddlObjectName").append("<option value ='ToDate'>To Date</option>");
                    $("#" + modalBody + " .ddlObjectName").append("<option value ='FromTime'>From Time</option>");
                    $("#" + modalBody + " .ddlObjectName").append("<option value ='ToTime'>To Time</option>");
                    $("#DefaultValue").hide()
                }
                isValueExist("#detailddlObjectName", $("#detailddlObjectName").val())
            })

            //$("body").on("change", "#ParameterName", function () {
            //    var flag = true
            //    var selectedName = $(this).find("option:selected").text();
            //    $("#parametergrid .dx-datagrid-table tbody tr").each(function (index, row) {
            //        if (index > 1) {
            //            var alreadySelected = $(row).find('td:nth-child(3)').text()
            //            if (selectedName == alreadySelected) {
            //                $("#paramerror").text("This name is already selected")
            //                flag = false;
            //            }
            //        }
            //    })
            //    if (flag) {
            //        $("#SelectedParameterName").val(selectedName)
            //        $("#paramerror").text("")
            //    } else {
            //        $("#SelectedParameterName").val("wrong")
            //    }

            //})
        }

        function bindSaveStoreProcedure() {
            $("body").on("click", "#saveStoreProcedure", function () {
                debugger


                var dataSend = {
                    ReportID: $("#txtReportsp").val(),
                    StoreProcedure: $("#txtSP").val(),
                    ExecutionOrder: $("#txtsort").val(),

                }

                if ($("#txtSP").val() != "") {
                    $.ajax({
                        type: "POST",
                        url: "" + serviceurl + "/SaveStoreProcedure",
                        data: JSON.stringify({ model: dataSend }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            if (response.d == "sessionNotFound") {
                                alert("sessionNotFound");
                                window.location.href = loginUrl;
                            } else {
                                var data = JSON.parse(response.d);
                                console.log(data);
                                if (data.Message == "success") {

                                    $("#Validation").fadeIn(1000);
                                    $("#Validation").text("Store Procedure save successfully");
                                    $("#Validation").fadeOut(5000);
                                    $("#txtSP , #txtsort").val("")
                                    $("#modalStoreProcedure").modal('hide');
                                } else if (data.Message == "exist") {
                                    $("#Validation").fadeIn(1000);
                                    $("#Validation").text(exist);
                                    $("#Validation").fadeOut(5000);
                                    // OnLoadGrid();
                                    $("#modalStoreProcedure").modal('hide');
                                } else if (data.Message == "update") {
                                    $("#Validation").fadeIn(1000);
                                    $("#Validation").text(update);
                                    $("#Validation").fadeOut(5000);
                                    // OnLoadGrid();
                                    $("#modalStoreProcedure").modal('hide');
                                }
                                else if (data.Message == "failed") {
                                    $("#Validation").fadeIn(1000);
                                    $("#Validation").text(failedmsg);
                                    $("#Validation").fadeOut(5000);
                                    // OnLoadGrid();
                                    $("#modalStoreProcedure").modal('hide');
                                }
                            }
                        },
                        error: OnErrorCall
                    });

                } else {
                    alert("Store Procedure name is required")
                }
                //});
            })
        }


        function OnDeleteStoreProcedure() {
            $("body").on('click', '.delSP', function () {
                debugger
                var row = $(this).closest('tr');
                var rowIndex = table4.row(row).index();
                var rowData = table4.row(rowIndex).data();
                Del2 = $(this).attr("_recordId");
                ObjectNamee = rowData.StoreProcedure;

                if (Del2 != "" && ObjectNamee != "") {
                    $("#modalDeleteSp").modal();
                }

            });
        }

        function OnConfirmDeleteSP() {
            DeleteStoreProcedure(Del2);
        }
        function DeleteStoreProcedure(id) {
            if (id != "") {
                $.ajax({
                    type: "POST",
                    url: "" + serviceurl + "/OnDeleteStoreProcedure",
                    data: JSON.stringify({ dataID: id }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        if (response.d == "sessionNotFound") {
                            alert("sessionNotFound");
                            window.location.href = loginUrl;
                        } else {
                            var data = JSON.parse(response.d);
                            console.log(data);
                            if (data.Message == "success") {
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text("Store Procedure deleted successfully");
                                $("#Validation").fadeOut(5000);
                                $("#modalDeleteSp").modal('hide');
                                $("#modalStoreProcedure").modal('hide');

                            } else if (data.Message == "failed") {
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text(failedmsg);
                                $("#Validation").fadeOut(5000);
                            } else if (data.Message == "use") {
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text("It is used by another process, Please try again!");
                                $("#Validation").fadeOut(5000);
                            }
                        }
                    },
                    error: OnErrorCall
                });
            } else {
                alert("Please select a record to delete!")
            }
        }

    </script>
</asp:Content>
