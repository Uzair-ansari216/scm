﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="ReportBuilder.aspx.vb" Inherits="Bridge_SCM.ReportBuilder" %>

<%@ Import Namespace="System.Web.Optimization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/ReportBuilderScript")%>

    <script src="../../Layout/assets/js/demo.js"></script>

    <script type="text/javascript">
        $(function () {
            let reportFormat = '<%= System.Configuration.ConfigurationManager.AppSettings("DefaultReportFormat").ToString() %>'
            switch (reportFormat) {
                case 'CrystalReport':
                    $("#CrystalReport").attr("checked", true)
                    break;
                case 'Excel':
                    $("#Excel").attr("checked", true)
                    break;
                case 'PDF':
                    $("#PDF").attr("checked", true)
                    break;
            }
            $("#ddlReportFormat").val(reportFormat)

        })
        function Save() {
            var IDS = [];

            jQuery.each(data, function (index, item) {
                IDS.push(item.id);
            });

            var ddlNA = $("#ddlNA").val();
            var ddlPA = $("#ddlPA :selected").val();
            var ddlUC = $("#ddlUC :selected").val();
            var ddlWard = $("#ddlWard :selected").val();
            var ddlBlock = $("#ddlBlock :selected").val();
            var ddlArea = $("#ddlArea :selected").val();
            var ddlStreet = $("#ddlStreet :selected").val();
            var ddlReports = $("#ddlReports :selected").val();
            var Todate = $("#txtToDate").val();
            var Totime = $("#txtToTime").val();
            var Fromdate = $("#txtFromDate").val();
            var Fromtime = $("#txtFromTime").val();
            var ToDateWithTime = Todate + ' ' + Totime;
            var FromDateWithTime = Fromdate + ' ' + Fromtime;
            var cmb1 = $("#cmb1").val();
            var cmb2 = $("#cmb2").val();
            var cmb3 = $("#cmb3").val();
            var cmb4 = $("#cmb4").val();
            var txt1 = $("#txt1").val();
            var txt2 = $("#txt2").val();
            var txt3 = $("#txt3").val();
            var txt4 = $("#txt4").val();
            var userId = $(".userid").val();
            var userName = $(".username").val();
            var companyId = $(".companyid").val();
            var locationId = $(".locationid").val();
            var companyName = $(".companyname").val();
            var locationName = $(".locationname").val();
            var isPowerUser = $(".ispoweruser").val()
            var workingDate = $(".workingdate").val();
            var yearId = $(".yearid").val()
            var yearTitle = $(".yeartitle").val()
            var fromDate = $(".datefrom").val()
            var toDate = $(".dateto").val()
            var ReportType = $("#ReportType").val();
            var reportModule = $("#ddlReportModule").val();
            var reportFormat = $("input[type='radio']:checked").attr("id");

            debugger
            var validateData = true;
            var actionName = "checkReportParameter";
            $.ajax({
                type: "POST",
                url: '' + ServiceUrl + '/' + actionName + '?id=' + ddlReports,
                contentType: "application/json",
                dataType: "json",
                success: function (responce) {
                    var data = JSON.parse(responce.d)
                    if (responce.d != 0) {
                        $(data).each(function (index, row) {
                            // checking for select box
                            $("#optionalObjects").find('select').each(function (index, objectRow) {
                                if ($(objectRow).attr('id') == row.ObjectName && $(objectRow).val() == "-1") {
                                    validateData = false
                                }
                            })
                            // checking for text box
                            $("#optionalObjects").find('input').each(function (index, objectRow) {
                                if ($(objectRow).attr('id') == row.ObjectName && $(objectRow).val() == "") {
                                    validateData = false
                                    $(objectRow).parent().find('span').text('Required')
                                }
                            })
                        })
                    }

                    $(".card-content input").each(function (index, row) {
                        if ($(row).attr('_spparameter') && $(row).val() == "") {
                            validateData = false
                            $(row).parent().find('span').text('Required')
                        }
                    })
                    $(".card-content select").each(function (index, row) {
                        if ($(row).attr('_spparameter') && $(row).val() == "-1") {
                            validateData = false
                            $(row).parent().find('span').text('Required')
                        }
                    })

                    if (validateData) {
                        urlname = "Save";
                        $.ajax({
                            type: "POST",
                            url: '' + ServiceUrl + '/' + urlname + '?id=' + IDS + '&ddlNA=' + encodeURIComponent(ddlNA) + '&ddlPA=' + encodeURIComponent(ddlPA) +
                                '&ddlUC=' + encodeURIComponent(ddlUC) + '&ddlWard=' + encodeURIComponent(ddlWard) + '&ddlBlock=' + encodeURIComponent(ddlBlock) +
                                '&ddlArea=' + encodeURIComponent(ddlArea) + '&ddlStreet=' + encodeURIComponent(ddlStreet) + '&ddlReports=' + encodeURIComponent(ddlReports) + '&Todate=' + encodeURIComponent(Todate) +
                                '&Totime=' + encodeURIComponent(Totime) + '&Fromdate=' + encodeURIComponent(Fromdate) + '&Fromtime=' + encodeURIComponent(Fromtime) + '&ToDateWithTime=' + encodeURIComponent(ToDateWithTime) + '&FromDateWithTime=' + encodeURIComponent(FromDateWithTime) +
                                '&cmb1=' + encodeURIComponent(cmb1) + '&cmb2=' + encodeURIComponent(cmb2) + '&cmb3=' + encodeURIComponent(cmb3) + '&cmb4=' + encodeURIComponent(cmb4) + '&txt1=' + encodeURIComponent(txt1) + '&txt2=' + encodeURIComponent(txt2) + '&txt3=' + encodeURIComponent(txt3) + '&txt4=' + encodeURIComponent(txt4) + '&ReportType=' + encodeURIComponent(ReportType) + '&reportModule=' + encodeURIComponent(reportModule) + '&txtUserId=' + encodeURIComponent(userId) + '&txtUserName=' + encodeURIComponent(userName) + '&txtCompanyId=' + encodeURIComponent(companyId) + '&txtLocationId=' + encodeURIComponent(locationId) + '&txtLocationName=' + encodeURIComponent(locationName) + '&txtCompanyName=' + encodeURIComponent(companyName) + '&txtIsPowereUser=' + encodeURIComponent(isPowerUser) + '&txtDate=' + encodeURIComponent(workingDate) + '&txtFinancialYearId=' + encodeURIComponent(yearId) + '&txtFinancialYear=' + encodeURIComponent(yearTitle) + '&txtFromDate=' + encodeURIComponent(fromDate) + '&txtToDate=' + encodeURIComponent(toDate) + '&reportFormat=' + encodeURIComponent(reportFormat),
                            //url: '' + pagename + '/' + urlname + '?id=' + IDS + '&ddlNA=' + ddlNA + '&ddlPA=' + ddlPA +
                            //    '&ddlUC=' + ddlUC + '&ddlWard=' + ddlWard + '&ddlBlock=' + ddlBlock +
                            //    '&ddlArea=' + ddlArea + '&ddlStreet=' + ddlStreet + '&ddlReports=' + ddlReports,
                            contentType: "application/json",
                            dataType: "json",
                            success: function (res) {
                                var data = res.d;
                                if (data == "1") {
                                    window.open("../Ctrls/ReportViewer.aspx" + '?isGeneratedByBuilder=' + true);
                                    $("#FormError").css('display', 'none');
                                    $("#Error").empty();

                                } else {
                                    alert(data)
                                    //$("#FormError").css('display', '');
                                    //$("#Error").empty();
                                    //$("#Error").text(data);
                                }
                            },
                            failure: function (errMsg) {
                                alert(errMsg);
                            }
                        });
                    } else {
                        //alert("Please Select")
                    }

                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            })
        }

        function Save2() {
            //alert('22');
            //var data = $(".select").select2('data');
            var IDS = [];

            jQuery.each(data, function (index, item) {
                IDS.push(item.id);
            });

            //alert('2');
            var ddlNA = $("#ddlNA").val();
            //alert('3');
            var ddlPA = $("#ddlPA :selected").val();
            //alert('4');
            var ddlUC = $("#ddlUC :selected").val();
            var ddlWard = $("#ddlWard :selected").val();
            var ddlBlock = $("#ddlBlock :selected").val();
            var ddlArea = $("#ddlArea :selected").val();
            var ddlStreet = $("#ddlStreet :selected").val();
            var ddlReports = $("#ddlReports :selected").val();
            var Todate = $("#txtToDate").val();
            var Totime = $("#txtToTime").val();
            var Fromdate = $("#txtFromDate").val();
            var Fromtime = $("#txtFromTime").val();
            var ToDateWithTime = Todate + Totime;
            var FromDateWithTime = Fromdate + Fromtime;
            var cmb1 = $("#cmb1 :selected").val();
            var cmb2 = $("#cmb2 :selected").val();
            var cmb3 = $("#cmb3 :selected").val();
            var cmb4 = $("#cmb4 :selected").val();
            var txt1 = $("#txt1").val();
            var txt2 = $("#txt2").val();
            var txt3 = $("#txt3").val();
            var txt4 = $("#txt4").val();


            urlname = "Save";
            if (cmb1 == "-1") {
                alert("Please Select")
            }
            else if (cmb2 == "-1") {
                alert("Please Select")
            }
            else if (cmb3 == "-1") {
                alert("Please Select")
            }
            else if (cmb4 == "-1") {
                alert("Please Select")
            }
            else {
                $.ajax({
                    type: "POST",
                    url: '' + pagename + '/' + urlname + '?id=' + IDS + '&ddlNA=' + ddlNA + '&ddlPA=' + ddlPA +
                        '&ddlUC=' + ddlUC + '&ddlWard=' + ddlWard + '&ddlBlock=' + ddlBlock +
                        '&ddlArea=' + ddlArea + '&ddlStreet=' + ddlStreet + '&ddlReports=' + ddlReports + '&Todate=' + Todate +
                        '&Totime=' + Totime + '&Fromdate=' + Fromdate + '&Fromtime=' + Fromtime + '&ToDateWithTime=' + ToDateWithTime + '&FromDateWithTime=' + FromDateWithTime +
                        '&cmb1=' + cmb1 + '&cmb2=' + cmb2 + '&cmb3=' + cmb3 + '&cmb4=' + cmb4 + '&txt1=' + txt1 + '&txt2=' + txt2 + '&txt3=' + txt3 + '&txt4=' + txt4,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (res) {
                        var data = res.d;
                        if (data == "1") {
                            window.open("../Ctrls/ReportViewer.aspx");
                            $("#FormError").css('display', 'none');
                            $("#Error").empty();

                        } else {
                            //$("#FormError").css('display', '');
                            //$("#Error").empty();
                            alert(data)
                            $("#Error").text(data);
                        }
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        function ExportToExcel() {
            var pagename = "Report.aspx"
            urlname = "ExportToExcel";
            $.ajax({
                type: "POST",
                url: '' + pagename + '/' + urlname,
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    $("#xlsx").css({ "display": "Block" })
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        }

        function Find() {
            $(".cmb1").hide()
            $(".cmb2").hide()
            $(".cmb3").hide()
            $(".cmb4").hide()

            $(".txt1").hide()
            $(".txt2").hide()
            $(".txt3").hide()
            $(".txt4").hide()
            $(_ddlReports).on('change', function () {
                debugger
                var id = $(_ddlReports).val();
                if (id != "-1") {
                    $.ajax({
                        type: "POST",
                        url: ServiceUrl + '/Find',
                        data: JSON.stringify({ ID: id }),
                        contentType: "application/json",
                        dataType: "json",
                        success: function (response) {

                            $("#optionalObjects div").each(function (index, row) {
                                if ($(row).attr('style') == "") {
                                    $(row).css({ "display": "none" })
                                }
                            })
                            var data = JSON.parse(response.d);
                            console.log(data)
                            if (data.length > 0) {
                                for (var i = 0; i < data.length; i++) {
                                    if (data[i].ObjectType == "Dropdown") {
                                        $(".lbl" + data[i].ObjectName).text(data[i].Label);
                                        FindControls(data[i].ObjectName, id)
                                    }
                                    else if (data[i].ObjectType == "Textbox") {
                                        $(".lbl" + data[i].ObjectName).text(data[i].Label);
                                        $("." + data[i].ObjectName).find("input").val(data[i].DefaultValue)
                                        $("." + data[i].ObjectName).show();
                                        isValueExist("#"+$("." + data[i].ObjectName).find("input").attr("id"), $("." + data[i].ObjectName).find("input").val())
                                    }
                                }
                            }
                        }
                    });
                } else {
                    $(".cmb1").hide()
                    $(".cmb2").hide()
                    $(".cmb3").hide()
                    $(".cmb4").hide()

                    $(".txt1").hide()
                    $(".txt2").hide()
                    $(".txt3").hide()
                    $(".txt4").hide()

                    $("#cmb1").empty();
                    $("#cmb2").empty();
                    $("#cmb3").empty();
                    $("#cmb4").empty();

                    $("#txt1").val("")
                    $("#txt2").val("")
                    $("#txt3").val("")
                    $("#txt4").val("")
                }

                if (id != "-1") {
                    $.ajax({
                        type: "POST",
                        url: ServiceUrl + '/findStoreProcedure',
                        data: JSON.stringify({ ID: id }),
                        contentType: "application/json",
                        dataType: "json",
                        success: function (response) {

                            //$("#optionalObjects div").each(function (index, row) {
                            //    if ($(row).attr('style') == "") {
                            //        $(row).css({ "display": "none" })
                            //    }
                            //})

                            $(".card-content input").each(function (index, row) {
                                if ($(row).attr('_spparameter')) {
                                    $(row).removeAttr("_spparameter")
                                }
                            })
                            var data = JSON.parse(response.d);
                            console.log(data)
                            if (data.length > 0) {

                                $.each(data, function (index, row) {
                                    $(".card-content input").each(function (second_index, second_row) {
                                        if ($(second_row).attr('class').split(' ')[1] == row.ObjectName) {
                                            $(second_row).attr("_SpParameter", row.ParameterName)
                                        }
                                        if ($(second_row).attr('id') == row.ObjectName) {
                                            $(second_row).attr("_SpParameter", row.ParameterName)
                                        }
                                    })
                                    $(".card-content select").each(function (second_index, second_row) {
                                        if ($(second_row).attr('id') == row.ObjectName) {
                                            $(second_row).attr("_SpParameter", row.ParameterName)
                                        }
                                    })
                                })

                                //for (var i = 0; i < data.length; i++) {
                                //    if (data[i].ObjectType == "Dropdown") {
                                //        $(".lbl" + data[i].ObjectName).text(data[i].Label);
                                //        FindControls(data[i].ObjectName, id)
                                //    }
                                //    else if (data[i].ObjectType == "Textbox") {
                                //        $(".lbl" + data[i].ObjectName).text(data[i].Label);
                                //        $("." + data[i].ObjectName).show();
                                //    }
                                //}
                            }
                        }
                    });
                }
            });
        }

        function FindControls(CmbName, id) {
            debugger
            $.ajax({
                type: "POST",
                url: ServiceUrl + '/FindControls',
                data: JSON.stringify({ CmbName: CmbName, ID: id, moduleId: $("#ddlReportModule").val() }),
                contentType: "application/json",
                dataType: "json",
                success: function (response) {
                    var data = JSON.parse(response.d);
                    console.log(data)
                    if (data.length > 0) {
                        $("#" + CmbName).empty();
                        $("#" + CmbName).append("<option value='-1'>--Please Select--</option>");
                        jQuery.each(data, function (index, item) {
                            $("#" + CmbName).append("<option value=" + item.Value + "> " + item.Text + " </option>");
                        });

                        $("." + CmbName).show();
                    }
                }
            });
        }

    </script>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header card-header-text">
                    <h4 class="card-title">Report</h4>
                </div>
                <input type="hidden" id="ReportType" />
                <input type="hidden" id="hdnmoduleName" />

                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Report Module</label>
                                <select id="ddlReportModule" class="form-control">
                                </select>
                                <span id="rNA" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <%--   <div class="col-sm-4">
                        <div class="form-group label-floating">
                            <label class="control-label">National Assembly</label>
                            <select id="ddlNA" class="form-control">
                            </select>
                            <span id="rNA" class="new badge  hide">required</span>
                        </div>
                    </div>
                   <div class="col-sm-6">
                        <div class="form-group label-floating">
                            <label class="control-label">Provincial Assembly</label>
                            <select id="ddlPA" class="form-control">
                            </select>
                            <span id="rPA" class="new badge  hide">required</span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group label-floating">
                            <label class="control-label">Union Council</label>
                            <select id="ddlUC" class="form-control">
                            </select>
                            <span id="rUC" class="new badge  hide">required</span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group label-floating">
                            <label class="control-label">Ward</label>
                            <select id="ddlWard" class="form-control">
                            </select>
                            <span id="rWard" class="new badge  hide">required</span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group label-floating">
                            <label class="control-label">Block</label>
                            <select id="ddlBlock" class="form-control">
                            </select>
                            <span id="rBlock" class="new badge  hide">required</span>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group label-floating">
                            <label class="control-label">Area</label>
                            <select id="ddlArea" class="form-control">
                            </select>
                            <span id="rArea" class="new badge  hide">required</span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group label-floating">
                            <label class="control-label">Street</label>
                            <select id="ddlStreet" class="form-control">
                            </select>
                            <span id="rStreet" class="new badge  hide">required</span>
                        </div>
                    </div>--%>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Reports Category</label>
                                <select id="ddlReportCategory" class="form-control">
                                </select>
                                <span id="rReportCategory" class="new badge  hide">required</span>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Reports</label>
                                <select id="ddlReports" class="form-control">
                                </select>
                                <span id="rReports" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">From Date</label>
                                <input type="text" class="form-control FromDate" id="txtFromDate" name="txtFromDate" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">From Time</label>
                                <input type="text" class="form-control FromTime" id="txtFromTime" name="txtFromTime" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">To Date</label>
                                <input type="text" class="form-control ToDate" id="txtToDate" name="txtToDate" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">To Time</label>
                                <input type="text" class="form-control ToTime" id="txtToTime" name="txtToTime" placeholder="" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1" style="margin-right: -70px;">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="format" id="CrystalReport" />
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-1" style="margin-right: -50px; color: green">
                            <i class="fas fa-file-invoice fa-2x"></i>
                        </div>
                        <div class="col-sm-1" style="margin-right: -70px;">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="format" id="PDF" />
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-1" style="margin-right: -50px; color: red">
                            <i class="far fa-file-pdf fa-2x"></i>
                        </div>
                        <div class="col-sm-1" style="margin-right: -70px;">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="format" id="Excel" />
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-1" style="margin-right: -70px; color: green">
                            <i class="far fa-file-excel fa-2x"></i>
                        </div>
                        <div class="col-sm-2 pull-right">
                            <button type="button" class="btn btn-fill btn-default" onclick="Save()">Print</button>
                        </div>
                        <div class="col-sm-2">
                            <%--<button type="button" class="btn btn-fill btn-default" onclick="ExportToExcel()">Export To Excel</button>
                            
                            <a id="xlsx" href="~/Files/text.xlsx"> Download Report</a>--%>
                        </div>
                    </div>
                    <div id="optionalObjects">
                        <div class="col-sm-6 cmb1">
                            <div class="form-group label-floating">
                                <label class="control-label lblcmb1"></label>
                                <select id="cmb1" class="form-control multi-select" name="cmd1[]" style="width: 100%;">
                                </select>
                                <span id="Span1" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-6 cmb2">
                            <div class="form-group label-floating">
                                <label class="control-label lblcmb2"></label>
                                <select id="cmb2" class="form-control multi-select" name="cmd2[]" style="width: 100%;">
                                </select>
                                <span id="Span2" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-6 cmb3">
                            <div class="form-group label-floating">
                                <label class="control-label lblcmb3"></label>
                                <select id="cmb3" class="form-control multi-select" name="cmd3[]" style="width: 100%;">
                                </select>
                                <span id="Span3" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-6 cmb4">
                            <div class="form-group label-floating">
                                <label class="control-label lblcmb3"></label>
                                <select id="cmb4" class="form-control multi-select" name="cmd4[]" style="width: 100%;">
                                </select>
                                <span id="Span4" class="new badge  hide">required</span>
                            </div>
                        </div>

                        <div class="col-sm-6 txt1">
                            <div class="form-group label-floating">
                                <label class="control-label lbltxt1"></label>
                                <input type="text" class="form-control" id="txt1" name="txt1" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-6 txt2">
                            <div class="form-group label-floating">
                                <label class="control-label lbltxt2"></label>
                                <input type="text" class="form-control" id="txt2" name="txt2" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-6 txt3">
                            <div class="form-group label-floating">
                                <label class="control-label lbltxt3"></label>
                                <input type="text" class="form-control" id="txt3" name="txt3" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-6 txt4">
                            <div class="form-group label-floating">
                                <label class="control-label lbltxt4"></label>
                                <input type="text" class="form-control" id="txt4" name="txt4" placeholder="" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-content">
                   <%-- <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group label-floating">
                                <label class="control-label">Report Format</label>
                                <select id="ddlReportFormat" class="form-control">
                                    <option value="CrystalReport">Crystal Report</option>
                                    <option value="PDF">PDF</option>
                                    <option value="Excel">Excel</option>
                                </select>
                                <span id="rReportFormat" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>--%>
                    <div class="col-sm-3">
                        <div class="form-group label-floating">
                            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control userid"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group label-floating">
                            <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control username"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group label-floating">
                            <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control companyid"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group label-floating">
                            <asp:TextBox ID="TextBox4" runat="server" CssClass="form-control locationid"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group label-floating">
                            <asp:TextBox ID="TextBox5" runat="server" CssClass="form-control ispoweruser"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group label-floating">
                            <asp:TextBox ID="TextBox6" runat="server" CssClass="form-control workingdate"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group label-floating">
                            <asp:TextBox ID="TextBox7" runat="server" CssClass="form-control yearid"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group label-floating">
                            <asp:TextBox ID="TextBox8" runat="server" CssClass="form-control yeartitle"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group label-floating">
                            <asp:TextBox ID="TextBox9" runat="server" CssClass="form-control datefrom"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group label-floating">
                            <asp:TextBox ID="TextBox10" runat="server" CssClass="form-control dateto"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group label-floating">
                            <asp:TextBox ID="TextBox11" runat="server" CssClass="form-control companyname"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group label-floating">
                            <asp:TextBox ID="TextBox12" runat="server" CssClass="form-control locationname"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

