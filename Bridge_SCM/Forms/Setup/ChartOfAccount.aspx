﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="ChartOfAccount.aspx.vb" Inherits="Bridge_SCM.ChartOfAccount" %>

<%@ Import Namespace="System.Web.Optimization" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<script type="text/javascript" src="../../js/treeview/bootstrap-treeview.min.js"></script>--%>
    <%--<script src="../../js/treeview/bootstrap-treeview.min.js"></script>--%>
    <%--<script src="../../FormScript/dataTableMethods.js"></script>--%>
    <script src="../../FormScript/ChartOfAccountScript.js"></script>

    <%--  <script src="../../jstree/jstree.min.js"></script>
    <script src="../../jstree/jquery.tree.js"></script>
    <script src="../../js/treeview/bootstrap-treeview.min.js"></script>--%>

    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/Select2Script")%>

    <style>
        .list-group li:nth-child(odd) {
            /*color: #fff;*/
            /*background: #35c3ef;*/
        }

        .list-group li:nth-child(even) {
            color: black;
            /*background: rgba(53, 195, 239, 0.15);*/
        }


        .marginbottom {
            margin-bottom: 5px;
        }
    </style>
    <input type="hidden" id="action" value="add" />
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header card-header-text">
                    <h4 class="card-title" style="color: #000000">Chart Of Account</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="validation" class="new badge"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <%--<label class="control-label">Account Type</label>--%>
                                <select id="ddlMain" class="form-control select2"></select>
                                <span id="rMain" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-4 m-t-20 m-l-15">
                            <i class="fas fa-folder-plus custome-style" id="btn-expand-all" title="Expand"></i>
                            <%--<button type="button" id="btn-expand-all" class="btn btn-primary"></button>--%>
                            <i class="fas fa-folder-minus custome-style" id="btn-collapse-all" title="Collapse"></i>
                            <%--<button type="button" id="btn-collapse-all" class="btn btn-primary"></button>--%>
                        </div>
                        <div class="col-sm-4 pull-right">
                            <button type="button" id="btnAdd" class="btn btn-primary pull-right"><i class="fas fa-plus"></i></button>
                            <button type="button" id="btnEdit" class="btn btn-primary pull-right"><i class="fas fa-pencil-alt"></i></button>
                        </div>
                    </div>
                    
                    <%-- <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="card-box">

                                <h4 class="header-title m-t-0 m-b-30">Basic Tree</h4>

                                <div id="basicTree">
                                    <ul>
                                        <li>Admin
                                               
                            <ul>
                                <li data-jstree='{"opened":true}'>Assets
                                                       
                                    <ul>
                                        <li data-jstree='{"type":"file"}'>Css</li>
                                        <li data-jstree='{"opened":true}'>Plugins
                                                               
                                            <ul>
                                                <li data-jstree='{"selected":true,"type":"file"}'>Plugin one</li>
                                                <li data-jstree='{"type":"file"}'>Plugin two</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li data-jstree='{"opened":true}'>Email Template
                                                       
                                    <ul>
                                        <li data-jstree='{"type":"file"}'>Email one</li>
                                        <li data-jstree='{"type":"file"}'>Email two</li>
                                    </ul>
                                </li>
                                <li data-jstree='{"icon":"zmdi zmdi-view-dashboard"}'>Dashboard</li>
                                <li data-jstree='{"icon":"zmdi zmdi-format-underlined"}'>Typography</li>
                                <li data-jstree='{"opened":true}'>User Interface
                                                       
                                    <ul>
                                        <li data-jstree='{"type":"file"}'>Buttons</li>
                                        <li data-jstree='{"type":"file"}'>Cards</li>
                                    </ul>
                                </li>
                                <li data-jstree='{"icon":"zmdi zmdi-collection-text"}'>Forms</li>
                                <li data-jstree='{"icon":"zmdi zmdi-view-list"}'>Tables</li>
                            </ul>
                                        </li>
                                        <li data-jstree='{"type":"file"}'>Frontend</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                </div>
                <%--</form> --%>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div>
                                <div class="card-content table-responsive" id="scrolling">
                                    <table id='treeview' class="">
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






    <div class="modal fade" id="modalAdd" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body" id="chartOfAccountForm">

                    <input type="hidden" id="txtID" value="" />
                    <input type="hidden" id="hParentAccountCode" value="" />
                    <input type="hidden" id="hAccountGroupID" value="" />
                    <input type="hidden" id="hAccountLevel" value="" />
                    <input type="hidden" id="hBalanceSheetLevel" value="" />
                    <input type="hidden" id="hBalanceSheetAccountHead" value="" />

                    <div class="row">
                        <div class="col-sm-12">
                            <span id="ValidationSummary" class="new badge"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label for="txtAccountCode" class="control-label">Account Code</label>
                                <input type="text" class="form-control validate" id="txtAccountCode" name="txtAccountCode" readonly="true" placeholder="" />
                                <span id="rAccountCode" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                         <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label for="ddlAccounType" class="control-label">Account Type</label>
                                <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlAccounType" CssClass="form-control"></asp:DropDownList>
                                <span id="rddlAccounType" class="new hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label for="txtAccountName" class="control-label">Account Name</label>
                                <input type="text" class="form-control validate" id="txtAccountName" name="txtAccountName" placeholder="" />
                                <span id="rAccountName" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label for="txtDescription" class="control-label">Description</label>
                                <input type="text" class="form-control validate" id="txtDescription" name="txtDescription" placeholder="" />
                                <span id="rDescription" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label for="ddlAccounType" class="control-label">P and L Heading</label>
                                <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlPNLHeading" CssClass="form-control accountcode"></asp:DropDownList>
                                <span id="rddlPNLHeading" class="new hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label for="txtOpeningBalance" class="control-label">Location</label>
                                <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlLocation" CssClass="form-control"></asp:DropDownList>
                                <span id="rddlLocation" class="new hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>

                        <%--<div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label for="chk" class="control-label">Pastable Account</label>
                                <input id="chk" type="checkbox" />
                            </div>
                        </div>--%>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label for="txtOpeningDate" class="control-label">Opening Date</label>
                                <input type="text" class="form-control" id="txtOpeningDate" name="txtOpeningDate" placeholder="" />
                                <span id="rOpeningDate" class="new hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label for="txtOpeningBalance" class="control-label">Opening Balance</label>
                                <input type="text" class="form-control validate" id="txtOpeningBalance" name="txtOpeningBalance" placeholder="" />
                                <span id="rOpeningBalance" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <%--  <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label for="ddlOldAccountCode" class="control-label">Old Account Code</label>
                                <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlOldAccountCode" CssClass="form-control"></asp:DropDownList>
                                <span id="rddlOldAccountCode" class="new hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>--%>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label for="txtOpeningDate" class="control-label">Old Account Code</label>
                                <input type="text" class="form-control" id="txtOldAccountCode" name="txtOldAccountCode" placeholder="" />
                                <span id="rtxtOldAccountCode" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-7 checkbox-radios">
                                    <div class="checkbox">
                                        <label>
                                            <input id="chk" type="checkbox" name="chk" />
                                            Pastable Account
                                        </label>

                                    </div>
                                </div>
                                <div class="col-sm-7 checkbox-radios">
                                    <div class="checkbox">
                                        <label>
                                            <input id="ChkMustSubLedger" type="checkbox" name="ChkMustSubLedger" />
                                            Must Sub Ledger
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="OnSave()">Save changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="Delete()">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        function OnSave() {

            var isValid = true

            $("body #chartOfAccountForm input").each(function (index, row) {
                if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
                    if ($(row).val() == "") {
                        $(row).parent().find("span").eq(2).removeClass("hide")
                        isValid = false
                    } else {
                        $(row).parent().find("span").eq(2).addClass("hide")
                    }
                }
            })

            $("body #chartOfAccountForm select").each(function (index, row) {
                if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
                    if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                        $(row).parent().find("span").eq(2).removeClass("hide")
                        isValid = false
                    } else {
                        $(row).parent().find("span").eq(2).addClass("hide")
                    }
                }
            })

            //formvalidaton();
            var bsid = "1";

            if ($("#chk").is(":checked")) {
                var ptba = "1";
            } else {
                var ptba = "0";
            }

            if ($("#chkmustsubledger").is(":checked")) {
                var msb = "1";
            } else {
                var msb = "0";
            }

            var datasend = {

                AccountCode: $(_txtAccountCode).val(),
                AccountName: $(_txtAccountName).val(),
                Description: $(_txtDescription).val(),
                ParentAccountCode: 0,
                AccountGroupID: 0, // $(haccountgroupid).val(),
                OpeningBalance: $(_txtOpeningBalance).val(),
                AccountTypeID: $(_ddlAccounType).val(),
                AccountLevel: 0,
                AccountCodeOld: $(_txtOldAccountCode).val(),
                LocationID: $(_ddlLocation).val(),
                pastableAccount: ptba,
                ISSubLedger: msb,
                //BalanceSheetAccountHead: $(BalanceSheetAccountHead).val(),
                FK_BSID: $(".accountcode").val()   //bsid
            }
            console.log(datasend);
            debugger


            if (isValid) {   // msg == ""  (old validation)
                $.ajax({
                    type: "post",
                    url: "" + serviceurl + "/onSaveChart",
                    data: JSON.stringify({ model: datasend }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d == "sessionnotfound") {
                            alert("sessionnotfound");
                            window.location.href = loginUrl;
                        } else {
                            var data = JSON.parse(response.d);
                            console.log(data);
                            console.log(data.message);
                            if (!data.Status) {
                                $("#validation").fadeIn(1000);
                                $("#validation").text(data.Message);
                                $("#validation").fadeOut(5000);
                                $("#modalAdd").modal('hide');
                            }
                            if (data.Message == "success") {
                                getTreeView(TID)
                                ClearDialogFields();
                                $("#validation").fadeIn(1000);
                                $("#validation").text(data.Message);
                                $("#validation").fadeOut(5000);
                                //OnLoad();
                                $("#modalAdd").modal('hide');
                            } else if (data.Message == "exist") {
                                $("#validation").fadeIn(1000);
                                $("#validation").text(data.Message);
                                $("#validation").fadeOut(5000);
                                //OnLoad();
                                $("#modalAdd").modal('hide');
                            } else if (data.Message == "update") {
                                getTreeView(TID)
                                //OnLoad();
                                $("#validation").fadeIn(1000);
                                $("#validation").text(data.Message);
                                $("#validation").fadeOut(5000);

                                $("#modalAdd").modal('hide');
                            }
                        }
                    },
                    error: OnErrorCall
                });
            } else {

            }
        }

        <%--  $(function () {
            var dataa = [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Revenue ? 4 ?", "id": "4", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Sale", "id": "41", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Equipment Sale", "id": "411", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Dealer", "id": "41101", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Head Office - Dealer - Equipment Sale", "id": "41101001", "nodes": [] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Islamabad Office - Dealer - Equipment Sale", "id": "41101002", "nodes": [] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Lahore Office - Dealer - Equipment Sale", "id": "41101003", "nodes": [] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Hyderabad Office - Dealer - Equipment Sale", "id": "41101004", "nodes": [] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Peshawar Office - Dealer - Equipment Sale", "id": "41101007", "nodes": [] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Techno City - Dealer - Equipment Sale", "id": "41101006", "nodes": [] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Karachi Office - Dealer - Equipment Sale", "id": "41101005", "nodes": [] }] }] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Purchase Return", "id": "412", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Supplier Name", "id": "41201", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Produt - Supplier Name - Purchase Return", "id": "41201001", "nodes": [] }] }] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Purchase Discount", "id": "413", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Supplier Name", "id": "41301", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Produt - Supplier Name - Purchase Discount", "id": "41301001", "nodes": [] }] }] }] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Services", "id": "42", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Service Income", "id": "421", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Dealer", "id": "42101", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Head Office - Dealer - Service Income", "id": "42101001", "nodes": [] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Islamabad Office - Dealer - Service Income", "id": "42101002", "nodes": [] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Lahore Office - Dealer - Service Income", "id": "42101003", "nodes": [] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Multan Office - Dealer - Service Income", "id": "42101004", "nodes": [] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "CSD - Dealer - Service Income", "id": "42101005", "nodes": [] }] }] }] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Other Income", "id": "43", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "ACMEL", "id": "431", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "JPP", "id": "43102", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Credit Note - JPP - ACMEL", "id": "43102001", "nodes": [] }] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Rebate", "id": "43101", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Credit Note - Rebate - ACMEL", "id": "43101001", "nodes": [] }] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Incentives", "id": "43103", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Credit Note - Incentives - ACMEL", "id": "43103001", "nodes": [] }] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Others", "id": "43104", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Credit Note - Others - ACMEL", "id": "43104001", "nodes": [] }] }] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "MS", "id": "432", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Rebate", "id": "43202", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Credit Note - Rebate - MS", "id": "43202001", "nodes": [] }] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Price Protection", "id": "43201", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Credit Note - Price Protection - MS", "id": "43201001", "nodes": [] }] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Incentives", "id": "43203", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Credit Note - Incentives - MS", "id": "43203001", "nodes": [] }] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Co-operative", "id": "43204", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Credit Note - Co-operative - MS", "id": "43204001", "nodes": [] }] }] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Others", "id": "433", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Loan", "id": "43301", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Salary - Loan - Others Income", "id": "43301001", "nodes": [] }] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Exchange", "id": "43302", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Others - Others Income", "id": "43302001", "nodes": [] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Stay in Touch - Others - Other Income", "id": "43302002", "nodes": [] }] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Others", "id": "43303", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Vehicle - Others - Income", "id": "43303001", "nodes": [] }] }] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Incentive", "id": "434", "nodes": [{ "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "HP Incentive \u0026 Rebate", "id": "43401", "nodes": [] }, { "AccountCode": null, "AccountName": null, "Description": null, "ParentAccountCode": null, "AccountGroupID": null, "OpeningBalance": null, "AccountTypeID": null, "AccountLevel": null, "AccountCodeOld": null, "LocationID": null, "BalanceSheetAccountHead": null, "ISSubLedger": null, "pastableAccount": null, "FK_BSID": null, "text": "Dell", "id": "43402", "nodes": [] }] }] }] }]
            //var jsondata = [
            //               { "id": "ajson1", "parent": "#", "text": "Simple root node" },
            //               { "id": "ajson2", "parent": "#", "text": "Root node 2" },
            //               { "id": "ajson3", "parent": "ajson2", "text": "Child 1" },
            //               { "id": "ajson4", "parent": "ajson2", "text": "Child 2" },
            //];

            createJSTree(dataa);
            //var position = 'inside';
            //var parent = $('#basicTree').jstree('get_selected');
            //var newNode = { state: "open", data: dataa };

            //$('#basicTree').jstree("create_node", parent, position, newNode, true, true);
        });

        function createJSTree(jsondata) {
            $('#SimpleJSTree').jstree({
                'core': {
                    'data': jsondata
                }
            });
        }--%>


    </script>
</asp:Content>
