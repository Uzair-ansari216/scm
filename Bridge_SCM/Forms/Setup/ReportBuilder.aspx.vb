﻿Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports Excel = Microsoft.Office.Interop.Excel
Imports Microsoft.Office
Imports System.Web.Script.Serialization

Public Class ReportBuilder
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GC.Collect()
        GC.WaitForPendingFinalizers()
        GC.Collect()
        If Not IsPostBack Then

        End If

        TextBox1.Text = Session("UserID")
        TextBox1.ReadOnly = True
        TextBox2.Text = Session("UserName")
        TextBox2.ReadOnly = True
        TextBox3.Text = Session("CompanyID")
        TextBox3.ReadOnly = True
        TextBox11.Text = Session("CompanyName")
        TextBox11.ReadOnly = True
        TextBox4.Text = Session("LocationId")
        TextBox4.ReadOnly = True
        TextBox12.Text = Session("LocationName")
        TextBox12.ReadOnly = True
        TextBox5.Text = Session("isPowerUser")
        TextBox5.ReadOnly = True
        TextBox6.Text = Session("WorkingDate")
        TextBox6.ReadOnly = True
        TextBox7.Text = Session("yearID")
        TextBox7.ReadOnly = True
        TextBox8.Text = Session("yearTitle")
        TextBox8.ReadOnly = True
        TextBox9.Text = Session("Datefrom").ToString().Replace("12:00:00", "00:00:00")
        TextBox9.ReadOnly = True
        TextBox10.Text = Session("dateTo").ToString().Replace("12:00:00", "23:59:59")
        TextBox10.ReadOnly = True

    End Sub
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function checkReportParameter() As String
        Dim report As String = HttpContext.Current.Request.QueryString("id")
        Dim obj As New BizSoft.Bridge_SCM.Report()
        Dim list As New List(Of ReportParameter)
        Dim ds As DataSet = obj.GetReportParameters(report)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New ReportParameter() With {
                      .ReportID = row("ReportID").ToString,
                      .DataType = row("DataType").ToString,
                      .ObjectName = row("ObjectName").ToString,
                      .ParameterName = row("ParameterName").ToString,
                      .Message = row("Msg").ToString
                      })
            Next
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Else
            Return 0
        End If
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function Save() As String
        Try
            Dim IDS As String = HttpContext.Current.Request.QueryString("id")
            Dim id() As String
            Dim message As String
            Dim ht As New Hashtable
            Dim SelctionFormulaDS As DataSet
            Dim report As String = HttpContext.Current.Request.QueryString("ddlReports")
            Dim _NA As String = HttpContext.Current.Request.QueryString("ddlNA")
            Dim _PA As String = HttpContext.Current.Request.QueryString("ddlPA")
            Dim _UC As String = HttpContext.Current.Request.QueryString("ddlUC")
            Dim _Ward As String = HttpContext.Current.Request.QueryString("ddlWard")
            Dim _Block As String = HttpContext.Current.Request.QueryString("ddlBlock")
            Dim _Area As String = HttpContext.Current.Request.QueryString("ddlArea")
            Dim _Street As String = HttpContext.Current.Request.QueryString("ddlStreet")
            Dim reportCredentails = New ReportModule() With {
            .Name = HttpContext.Current.Request.QueryString("moduleName"),
            .ReportServer = HttpContext.Current.Request.QueryString("reportServer"),
            .ReportDatabase = HttpContext.Current.Request.QueryString("reportDatabase"),
            .ReportDbUserId = HttpContext.Current.Request.QueryString("reportDbUserId"),
            .ReportDbPassord = HttpContext.Current.Request.QueryString("reportDbPassword"),
            .ReportPath = HttpContext.Current.Request.QueryString("ReportPath"),
            .ReportConnectionType = HttpContext.Current.Request.QueryString("conType"),
            .ConnectionName = HttpContext.Current.Request.QueryString("conName"),
            .ConnectionUserId = HttpContext.Current.Request.QueryString("conUserId"),
            .ConnectionPassword = HttpContext.Current.Request.QueryString("conPassword")
            }

            Dim Todates As String = HttpContext.Current.Request.QueryString("Todate")
            'Dim ToDateWithTime As DateTime = HttpContext.Current.Request.QueryString("ToDateWithTime")
            Dim timee As String = " 23:59:59"
            Dim Todate As String = Todates + timee

            Dim result As String = ""
            Dim mAccountCode As String = ""
            'Dim ObjACt As New BizSoft.Bridge_SCM.Task

            Dim objReport As New BizSoft.DBManager.clsReport.oReport
            Dim StrList As New Generic.List(Of String)

            Dim oCulture = New System.Globalization.CultureInfo("en-US", True)

            Dim selectionsql As Object = ""
            Dim selectionsqlCode As String = ""
            objReport.Formula = ""

            If report = "-1" Then
                Return "Please Select Any Reports"
            End If

            If IDS = "" Then
                'Return "Please Select ChartofAccount"
            Else
                id = IDS.Split(",")
            End If

            Dim obj As New BizSoft.Bridge_SCM.Report()

            Dim dr As DataSet = obj.GetReportData(report)

            If dr.Tables(0).Rows.Count > 0 Then
                'If dr.Tables(0).Rows(0)("AutoCalling").ToString() = "1" Then

                'If report = "RptMemberCountnewHourwise.rpt" Then
                Dim ds As DataSet = obj.GetReportParameter(report)
                If dr.Tables(0).Rows(0)("IsSubReport").ToString() = "1" Then
                    objReport.rptIsSubLoad = True
                    objReport.rptSubReportCollection.Add("1")
                End If
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim ObjectValue As String = System.Net.WebUtility.HtmlDecode(HttpContext.Current.Request.QueryString(ds.Tables(0).Rows(i)("ObjectName")))
                    Dim ParameterName As String = ds.Tables(0).Rows(i)("ParameterName")

                    If ObjectValue = "-1" Or ObjectValue = "undefined" Or ObjectValue = "" Or ObjectValue = " " Then
                        Return ds.Tables(0).Rows(i)("Msg")
                    End If

                    ht.Add(ParameterName, ObjectValue)
                Next

                'ht.Add("FromDate", ToDateWithTime)
                'ht.Add("Todate", ToDateWithTime)
                'End If
                'Dim ds As DataSet = obj.GetReportParameter(report)
                'For i = 0 To ds.Tables(0).Rows.Count - 1
                '    Dim ObjectValue As String = HttpContext.Current.Request.QueryString(ds.Tables(0).Rows(i)("ObjectName"))
                '    Dim ParameterName As String = ds.Tables(0).Rows(i)("ParameterName")

                '    ht.Add(ParameterName, ObjectValue)
                'Next
                'Else
                If report = "RptBlockDetail.rpt" Then
                    If _Block = "-1" Or _Block = "undefined" Then
                        Return "Please Select Block "
                    End If

                End If

                If report = "RptProvinceAssemblyAnalysis.rpt" Then
                    If _PA = "-1" Or _PA = "undefined" Then
                        Return "Please Select PA "
                    End If
                End If

                If report = "RptUCAnalysis.rpt" Then
                    If _UC = "-1" Or _UC = "undefined" Then
                        Return "Please Select UC "
                    End If
                End If

                If report = "RptWardAnalysis.rpt" Then
                    If _Ward = "-1" Or _Ward = "undefined" Then
                        Return "Please Select Ward "
                    End If
                End If




                If report = "RptBlockDetail.rpt" Then
                    objReport.rptIsSubLoad = True
                    objReport.rptSubReportCollection.Add("1")
                    ht.Add("BlockCodeId", Val(_Block))
                End If

                If report = "RptProvinceAssemblyAnalysis.rpt" Then
                    ht.Add("PKPA", Val(_PA))
                End If
                If report = "RptUCAnalysis.rpt" Then
                    ht.Add("PKUCID", Val(_UC))
                End If
                If report = "RptWardAnalysis.rpt" Then

                    ht.Add("BlockCodeId", Val(_Block))
                    ht.Add("PKWardID", Val(_Ward))
                End If


            End If


            'End If

            objReport.strFileName = report
            HttpContext.Current.Session("ReportFileName") = report


            'ht.Add("ToDate", ToDate)
            ht.Add("User", HttpContext.Current.Session("UserID"))

            If dr.Tables(0).Rows(0)("IsSelectionFormula").ToString() = "1" Then
                SelctionFormulaDS = obj.GetReportSelectionFormula(report)
                selectionsql = SetCritria(report, SelctionFormulaDS)
                'selectionsql = selectionsqlCode & " " & selectionsql

                objReport.Formula = selectionsql(0)
                objReport.rptCreteria = selectionsql(1)
            Else

            End If
            'selectionsqlCode = SetCritriaCode(id)


            HttpContext.Current.Session("ogsReport") = objReport
            HttpContext.Current.Session("credentials") = reportCredentails

            Dim iAction As String = ""
            iAction = "Report Generated" & " " & dr.Tables(0).Rows(0)("ReportName").ToString() & " With Selection Formula" & " " & objReport.rptCreteria
            BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, report, "Report", "", Environment.MachineName)

            objReport.ht = ht
            objReport.StrList = StrList

            Return "1"
        Catch ex As Exception
            Dim ser As New JavaScriptSerializer()
            Return ser.Serialize("Report does not exist")
        End Try
    End Function




    Public Shared Function SetCritriaCode(ByVal list() As String) As String
        Dim FromDate As String = HttpContext.Current.Request.QueryString("txtFromDate")
        Dim ToDate As String = HttpContext.Current.Request.QueryString("txtToDate")

        Dim i As Integer
        Dim mAccountCode As String
        'Dim ObjACt As New BizSoft.Bridge_SCM.Task

        Dim selectionsqlCode As String = ""
        Dim AndFlag As Boolean = False
        Dim T As Integer

        selectionsqlCode = " {VnPropertyStock.Dated} >=date(" & Year(FromDate) & "," & Month(FromDate) & ", " & Day(FromDate) & ")  AND  {VnPropertyStock.Dated} <=date(" & Year(ToDate) & "," & Month(ToDate) & ", " & Day(ToDate) & ") "
        AndFlag = True
        If T = 0 Then
            Return selectionsqlCode
        Else
            T = list.Count

            For i = 0 To T - 1
                '   mAccountCode = ObjACt.StripID(Trim(list(i)))

                'If AndFlag = False Then
                '  selectionsqlCode = "({VnPropertyStock.ProjectLocation} = '" & mAccountCode & "'"
                '   AndFlag = True
                'Else
                selectionsqlCode = selectionsqlCode & " AND {VnPropertyStock.ProjectLocation} = '" & mAccountCode & "' OR"
                'End If
            Next i
            selectionsqlCode = selectionsqlCode & ")"

            Return selectionsqlCode
        End If
    End Function

    Public Shared Function SetCritria(ByVal reportsName As String, ByRef SFList As DataSet) As Object

        Dim _NA As String = HttpContext.Current.Request.QueryString("ddlNA")
        Dim _PA As String = HttpContext.Current.Request.QueryString("ddlPA")
        Dim _UC As String = HttpContext.Current.Request.QueryString("ddlUC")
        Dim _Ward As String = HttpContext.Current.Request.QueryString("ddlWard")
        Dim _Block As String = HttpContext.Current.Request.QueryString("ddlBlock")
        Dim _Area As String = HttpContext.Current.Request.QueryString("ddlArea")
        Dim _Street As String = HttpContext.Current.Request.QueryString("ddlStreet")
        Dim _FromDate As String = HttpContext.Current.Request.QueryString("Fromdate")
        Dim _ToDate As String = HttpContext.Current.Request.QueryString("Todate")
        Dim _FromTime As String = HttpContext.Current.Request.QueryString("Fromtime")
        Dim _ToTime As String = HttpContext.Current.Request.QueryString("Totime")
        Dim _ComboBox1 As String = HttpContext.Current.Request.QueryString("cmb1")
        Dim _ComboBox2 As String = HttpContext.Current.Request.QueryString("cmb2")
        Dim _ComboBox3 As String = HttpContext.Current.Request.QueryString("cmb3")
        Dim _ComboBox4 As String = HttpContext.Current.Request.QueryString("cmb4")
        Dim _TextBox1 As String = HttpContext.Current.Request.QueryString("txt1")
        Dim _TextBox2 As String = HttpContext.Current.Request.QueryString("txt2")
        Dim _TextBox3 As String = HttpContext.Current.Request.QueryString("txt3")
        Dim _TextBox4 As String = HttpContext.Current.Request.QueryString("txt4")
        Dim _ReportPath As String = HttpContext.Current.Request.QueryString("ReportPath")
        Dim selectionsql As String = ""
        Dim selectionFormula As String = ""
        Dim AndFlag As Boolean = False
        'changes
        Dim reportDocument As ReportDocument = New ReportDocument()
        reportDocument.Load(Path.Combine(HttpContext.Current.Server.MapPath(_ReportPath), reportsName))

        If SFList.Tables(0).Rows.Count > 0 Then
            For Each item As DataRow In SFList.Tables(0).Rows
                If (item("ObjectName") = "FromDate") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + _FromDate
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + _FromDate
                    End If
                ElseIf (item("ObjectName") = "ToDate") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + _ToDate
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + _ToDate
                    End If
                ElseIf (item("ObjectName") = "FromTime") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + _FromTime
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + _FromTime
                    End If
                ElseIf (item("ObjectName") = "ToTime") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + _ToTime
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + _ToTime
                    End If
                ElseIf (item("ObjectName") = "cmb1") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + IIf(_ComboBox1 = "-1", "", _ComboBox1)
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + IIf(_ComboBox1 = "-1", "", _ComboBox1)
                    End If
                ElseIf (item("ObjectName") = "cmb2") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + IIf(_ComboBox2 = "-1", "", _ComboBox2)
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + IIf(_ComboBox2 = "-1", "", _ComboBox2)
                    End If
                ElseIf (item("ObjectName") = "cmb3") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + IIf(_ComboBox3 = "-1", "", _ComboBox3)
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + IIf(_ComboBox3 = "-1", "", _ComboBox3)
                    End If
                ElseIf (item("ObjectName") = "cmb4") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + IIf(_ComboBox4 = "-1", "", _ComboBox4)
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + IIf(_ComboBox4 = "-1", "", _ComboBox4)
                    End If
                ElseIf (item("ObjectName") = "txt1") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + IIf(_TextBox1 = "", "", _TextBox1)
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + IIf(_TextBox1 = "", "", _TextBox1)
                    End If
                ElseIf (item("ObjectName") = "txt2") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + IIf(_TextBox2 = "", "", _TextBox2)
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + IIf(_TextBox2 = "", "", _TextBox2)
                    End If
                ElseIf (item("ObjectName") = "TextBox3") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + IIf(_TextBox3 = "", "", _TextBox3)
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + IIf(_TextBox3 = "", "", _TextBox3)
                    End If
                ElseIf (item("ObjectName") = "TextBox4") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + IIf(_TextBox4 = "", "", _TextBox4)
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + IIf(_TextBox4 = "", "", _TextBox4)
                    End If
                End If
            Next
            ' For i = 0 To SFList.Tables(0).Rows.Count - 1

            'Dim ObjectValue As String = System.Net.WebUtility.HtmlDecode(HttpContext.Current.Request.QueryString(SelctionFormulaDS.Tables(0).Rows(i)("ObjectName")))
            'Dim ParameterName As String = SelctionFormulaDS.Tables(0).Rows(i)("ParameterName")

            'If ObjectValue = "-1" Or ObjectValue = "undefined" Or ObjectValue = "" Or ObjectValue = " " Then
            '    Return SelctionFormulaDS.Tables(0).Rows(i)("Msg")
            'End If


            'ht.Add(ParameterName, ObjectValue)
            'Next
        End If
        selectionFormula = reportDocument.RecordSelectionFormula
        'changes






        Return {selectionsql, selectionFormula}
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function ExportToExcel()
        'Initialize the objects before use
        Dim dataAdapter As New SqlClient.SqlDataAdapter()
        Dim dataSet As New DataSet
        Dim command As New SqlClient.SqlCommand
        Dim datatableMain As New System.Data.DataTable()
        Dim connection As New SqlClient.SqlConnection

        'Assign your connection string to connection object
        connection.ConnectionString = "Data Source=DEV-01 ;Initial Catalog=Nlg_FGood ;User ID=sa ;Password=123 ;"
        command.Connection = connection
        command.CommandType = CommandType.Text
        'You can use any command select
        command.CommandText = "Select * from GS_Report_Setup"
        dataAdapter.SelectCommand = command


        'Dim f As FolderBrowserDialog = New FolderBrowserDialog
        Try
            'If f.ShowDialog() = DialogResult.OK Then
            'This section help you if your language is not English.
            System.Threading.Thread.CurrentThread.CurrentCulture =
                System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
            'Dim oExcel As Application
            'Dim oBook As Workbook
            'Dim oSheet As Worksheet
            'Dim xlApp As Excel.Application = Nothing
            'Dim xlWorkBooks As Excel.Workbooks = Nothing
            'Dim xlWorkBook As Excel.Workbook = Nothing
            'Dim xlWorkSheet As Excel.Worksheet = Nothing
            'Dim xlWorkSheets As Excel.Sheets = Nothing
            'Dim xlCells As Excel.Range = Nothing
            'xlApp = New Excel.Application
            'xlApp.DisplayAlerts = False
            'xlWorkBooks = xlApp.Workbooks
            Dim strPath As String
            Dim strNewPath As String

            strPath = HttpContext.Current.Server.MapPath("~/Files/text.xlsx")
            'strNewPath = strPath.Replace("\Forms\Setup", "Files")
            'If strNewPath.IndexOf("\Forms\Setup") > 0 Then
            '    strNewPath = strNewPath.Replace("\Forms\Setup", "\Files")
            'Else
            '    strNewPath = strNewPath.Replace("\Forms\Setup", "\Files")
            'End If
            'Dim finalPath = strNewPath + ""
            'xlWorkBook = xlWorkBooks.Open(strPath)
            'xlApp.Visible = False
            'xlWorkSheet = xlWorkBook.Sheets(1)

            'oExcel = CreateObject("Excel.Application")
            'oBook = oExcel.Workbooks.Add(Type.Missing)
            'oSheet = oBook.Worksheets(1)

            Dim dc As DataColumn
            Dim dr As DataRow
            Dim colIndex As Integer = 0
            Dim rowIndex As Integer = 0

            'Fill data to datatable
            connection.Open()
            dataAdapter.Fill(datatableMain)
            connection.Close()


            'Export the Columns to excel file
            For Each dc In datatableMain.Columns
                colIndex = colIndex + 1
                'xlWorkSheet.Cells(1, colIndex) = dc.ColumnName
            Next

            'Export the rows to excel file
            For Each dr In datatableMain.Rows
                rowIndex = rowIndex + 1
                colIndex = 0
                For Each dc In datatableMain.Columns
                    colIndex = colIndex + 1
                    'xlWorkSheet.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName)
                Next
            Next

            'Set final path
            'Dim fileName As String = "\ExportedAuthors" + ".xls"
            '
            'txtPath.Text = finalPath
            'xlWorkSheet.Columns.AutoFit()
            'Save file in final path

            'oBook.SaveAs(strNewPath, XlFileFormat.xlWorkbookNormal, Type.Missing,
            'Type.Missing, Type.Missing, Type.Missing, XlSaveAsAccessMode.xlExclusive,
            'Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing)

            'Release the objects
            'ReleaseObject(xlWorkSheet)
            'xlWorkBook.Save()
            'xlWorkBook.Close(False, Type.Missing, Type.Missing)
            'ReleaseObject(xlWorkBook)
            'xlApp.Quit()
            'ReleaseObject(xlApp)
            'Some time Office application does not quit after automation: 
            'so i am calling GC.Collect method.
            GC.Collect()

            'MessageBox.Show("Export done successfully!")

            'End If
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK)
        End Try
    End Function

    Public Shared Function ReleaseObject(ByVal o As Object)
        Try
            While (System.Runtime.InteropServices.Marshal.ReleaseComObject(o) > 0)
            End While
        Catch
        Finally
            o = Nothing
        End Try
    End Function

End Class