﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="Currency.aspx.vb" Inherits="Bridge_SCM.Currency" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/CurrencyScript")%>


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header card-header-text">
                    <h4 class="card-title" style="color: #000000">Currency </h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-primary pull-right" id="btnAdd" _recordId="0" title="Add New Currency"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">

                            <table id='tableCity' class="table table-striped table-bordered display">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>ID</th>
                                        <th>Currency</th>
                                        <th>Symbol</th>
                                        <th>Unit</th>
                                        <th>Description</th>
                                        <th>DefaultCurrency</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>
                <%--</form> --%>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalAdd"  data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body" id="currencyForm">

                    <input type="hidden" id="txtID" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="ValidationSummary" class="new badge"></span>
                        </div>
                    </div>


                    <div class="row">


                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Currency</label>
                                <input type="text" class="form-control" id="txtcurrency" name="txtcurrency" placeholder="" />
                                <span id="rCurrency" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Symbol</label>
                                <input type="text" class="form-control" id="txtsymbol" name="txtsymbol" placeholder="" />
                                <span id="rSymbol" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Unit</label>
                                <input type="text" class="form-control" id="txtunit" name="txtunit" placeholder="" />
                                <span id="rUnit" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Description</label>
                                <input type="text" class="form-control" id="txtdescription" name="txtdescription" placeholder="" />
                                <span id="rDescription" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 checkbox-radios">
                            <div class="checkbox">
                                <label>
                                    <input id="isdefault" type="checkbox" name="active" />
                                    Is Default
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-fill btn-default" onclick="OnSave()">Save changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDelete()">Confirm</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
