﻿Public Class ChartOfAccount
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            FillDropdown()
        End If
    End Sub
    Private Sub FillDropdown()
        Dim objAccounttype As New BizSoft.Bridge_SCM.ChartOFAccount
        Dim dtAccounttype As DataSet = objAccounttype.GetListAccountType



        ddlAccounType.DataSource = dtAccounttype
        ddlAccounType.DataTextField = "Name"
        ddlAccounType.DataValueField = "ID"
        ddlAccounType.DataBind()


        Dim objlocation As New BizSoft.Bridge_SCM.Location
        Dim dtlocation As DataSet = objlocation.GetList(HttpContext.Current.Session("CompanyID"))

        ddlLocation.DataSource = dtlocation
        ddlLocation.DataTextField = "Name"
        ddlLocation.DataValueField = "ID"
        ddlLocation.DataBind()



        'Dim objAccountCode As New BizSoft.Bridge_SCM.ChartOFAccount
        'Dim dtAccountCode As DataSet = objAccountCode.GetListAccountCode

        'ddlOldAccountCode.DataSource = dtAccountCode
        'ddlOldAccountCode.DataTextField = "Name"
        'ddlOldAccountCode.DataValueField = "ID"
        'ddlOldAccountCode.DataBind()

    End Sub
End Class