﻿Imports System.Reflection
Imports System.Web.Script.Serialization

Public Class Voucher
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function Print() As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Dim Message As String = ""
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("General Voucher", "isShow", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim ht As New Hashtable
                HttpContext.Current.Session("reportFormat") = ConfigurationManager.AppSettings("DefaultReportFormat").ToString()
                Dim report As String = "rptVoucher.rpt"
                Dim VoucherID As String = HttpContext.Current.Request.QueryString("VoucherID")

                Dim objReport As New BizSoft.DBManager.clsReport.oReport


                objReport.strFileName = report
                HttpContext.Current.Session("ReportFileName") = report
                Dim StrList As New Generic.List(Of String)

                ht.Add("VoucherID", VoucherID)

                HttpContext.Current.Session("ogsReport") = objReport

                objReport.ht = ht
                objReport.StrList = StrList
                Dim iAction As String = ""
                iAction = "Generate Voucher Report"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "General Voucher", "VoucherMaster", "", Environment.MachineName)
                objModel.Status = True
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
            'Return "1"
        Catch ex As Exception
            BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, ex.Message, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "General Voucher", "VoucherMaster", "", Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
            Message = ex.Message
            Return Message
        End Try


    End Function
End Class