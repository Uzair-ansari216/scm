﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="CurrencyExchangeRate.aspx.vb" Inherits="Bridge_SCM.CurrencyExchangeRate" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header card-header-text">
                    <h4 class="card-title" style="color: #000000">Currency Exchange Rate</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-primary pull-right" id="btnadd" title="Add New Exchange Rate" _recordid="0"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">

                            <table id='gridContainer' class="table table-striped table-bordered display">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>ID</th>
                                        <th>Currency</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>
                <%--</form> --%>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exratemodal" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body" id="exchangeRateForm">

                    <input type="hidden" id="txtID" value="" />
                    <div class="row">
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Currency</label>
                                <select id="ddlcurrency" class="form-control"></select>
                                <span id="rCountry" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">From Date</label>
                                <input type="text" class="form-control datepicker" id="txtfromdate" name="txtfromdate" placeholder="" />
                                <span id="rFromDate" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">To Date</label>
                                <input type="text" class="form-control datepicker" id="txttodate" name="txttodate" placeholder="" />
                                <span id="rToDate" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Exchange Rate</label>
                                <input type="text" class="form-control" id="txtexchangerate" name="txtexchangerate" placeholder="" />
                                <span id="rExchangeRate" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <i id="searchrate" class="fas fa-search btn" title="Search Exchange Rate" style="padding-right: 15px; padding-left: 15px;"></i>
                    <button type="button" class="btn btn-fill btn-default" id="btnsave">Save changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                <div class="card" id="rate-card" style="box-shadow: none">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <table id='rateGridContainer' class="table table-striped table-bordered display w-100">
                                    <thead class="header bg-primary">
                                        <tr class="">
                                            <th>Date</th>
                                            <th>Currency</th>
                                            <th>Exchange Rate</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDelete()">Confirm</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        const serviceUrl = "../../appServices/CurrencyExchangeRateService.asmx"

        $(function () {
            $("body #exchangeRateForm").on("focus", "input, select", function () {
                var service_ToolTip = $("#hdnToolTip").val()
                if (service_ToolTip == 'on') {
                    showCurrentToopTip(this)
                }
            })

            $("body #exchangeRateForm").on("focusout", "input, select", function () {
                var service_ToolTip = $("#hdnToolTip").val()
                if (service_ToolTip == 'on') {
                    hideToopTipExceptCurrent(this)
                } else {
                    hideValidationMessage(this)
                }
            })

            $("body #exchangeRateForm").on("keypress paste", "input", function (key) {
                var currentElement = $(this)
                var type = $(currentElement).attr("_fieldtype")
                if (type != "") {
                    return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
                }
            })

            bindGetCurrencies();
            bindAddEditExchangeRate();
            bindSaveExchangeRate();
            bindSearchRate();
            demo.initFormExtendedDatetimepickers();
            //$(".datepicker").datepicker({
            //    autoclose: true, changeMonth: true,
            //    changeYear: true, yearRange: "-90:+00"
            //});

            $("body").on("click", ".btneditrate", function () {
                let currentEle = $(this)
                let rate = currentEle.closest('tr').find('td:eq(2)').text()
                currentEle.closest('tr').find('td:eq(2)').html('<input type="text" class="form-control" value=' + rate + '>')
                currentEle.closest('tr').find('td:eq(3) a').removeClass('btneditrate').addClass('btnupdaterate')
                currentEle.closest('tr').find('td:eq(3) i').removeClass('fas fa-pencil-alt').addClass('fas fa-save')
            })

            $("body").on("click", ".btnupdaterate", function () {
                let currentEle = $(this)
                let id = currentEle.attr('_recordid')
                let date = currentEle.closest('tr').find('td:eq(0)').text()
                let rate = currentEle.closest('tr').find('td:eq(2) input').val()
                currentEle.closest('tr').find('td:eq(2)').html('').text(rate)

                $.ajax({
                    type: "POST",
                    url: serviceUrl + "/updateExchangeRate",
                    data: JSON.stringify({ id: id, rateDate: date, rate: rate }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var data = JSON.parse(response.d);
                        currentEle.closest('tr').find('td:eq(3) a').removeClass('btnupdaterate').addClass('btneditrate')
                        currentEle.closest('tr').find('td:eq(3) i').removeClass('fas fa-save').addClass('fas fa-pencil-alt')
                    },
                    error: function (responce) {

                    }
                });
            })

        })

        function bindGetCurrencies() {
            $.ajax({
                type: "POST",
                url: serviceUrl + "/getAllCurrencies",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var data = JSON.parse(response.d);
                    //fill currency Dropdown
                    $("#ddlcurrency").find("option").remove().end().append("<option value='0'>-- Select Currency --</option>")
                    $.each(data, function (index, item) {
                        $("#ddlcurrency").append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });
                    $("#ddlcurrency").find("option:first").prop("selected", true)
                    MakeDataGridForCurrency(data)
                },
                error: function (responce) {

                }
            });
        }
        function bindAddEditExchangeRate() {
            $("body").on("click", "#btnadd , .btnedit", function () {
                clearAllMessages("exchangeRateForm")

                var service_DeveloperMode = $("#hdnDevMode").val()
                if (service_DeveloperMode == 'on') {
                    addFormFields("Currency Exchange Rate", "exchangeRateForm")
                }
                //var service_ToolTip = $("#hdnToolTip").val()
                //if (service_ToolTip == 'on') {

                setToolTip("Currency Exchange Rate", "exchangeRateForm", service_DeveloperMode)

                //}

                let id = $(this).attr("_recordId")
                $.ajax({
                    type: "POST",
                    url: serviceUrl + "/AddEditExchangeRate",
                    data: JSON.stringify({ id: id }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var data = JSON.parse(response.d);
                        if (!data.Status) {
                            $("#Validation").fadeIn(1000);
                            $("#Validation").text(data.Message);
                            $("#Validation").fadeOut(5000);
                            $("#modalAdd").modal('hide');
                        } else {
                            if (parseInt(id) > 0) {
                                $("#formModalLabel").text("Edit Exchange Rate");
                                $("#txtID").val(data.GenericList.Id)
                            } else {
                                $("#formModalLabel").text("Add Exchange Rate");
                                $("#txtID").val(0)
                                $("#txttodate").val("")
                            }
                            $("#ddlcurrency").val(data.GenericList.Currency)
                            $("#txtfromdate").val(data.Data)
                            $("#txtexchangerate").val(data.GenericList.ExchangeRate)
                            $("#rate-card").hide()
                            $("#exratemodal").modal()
                        }
                    },
                    error: function (responce) {

                    }, complete: function () {
                        isValueExist("#ddlcurrency", $("#ddlcurrency").val())
                        isValueExist("#txtfromdate", $("#txtfromdate").val())
                        isValueExist("#txtexchangerate", $("#txtexchangerate").val())
                    }
                });

            })
        }

        function bindSaveExchangeRate() {
            $("body").on("click", "#btnsave", function () {
                var isValid = true

                $("body #exchangeRateForm input").each(function (index, row) {
                    if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
                        if ($(row).val() == "") {
                            $(row).parent().find("span").eq(2).removeClass("hide")
                            isValid = false
                        } else {
                            $(row).parent().find("span").eq(2).addClass("hide")
                        }
                    }
                })

                $("body #exchangeRateForm select").each(function (index, row) {
                    if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
                        if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                            $(row).parent().find("span").eq(2).removeClass("hide")
                            isValid = false
                        } else {
                            $(row).parent().find("span").eq(2).addClass("hide")
                        }
                    }
                })

                if (isValid) {
                    var formData = {
                        Id: $("#txtID").val(),
                        Currency: $("#ddlcurrency").val(),
                        ExchangeRate: $("#txtexchangerate").val(),
                        FromDate: $("#txtfromdate").val(),
                        ToDate: $("#txttodate").val(),
                    }
                    $.ajax({
                        type: "POST",
                        url: serviceUrl + "/saveExchangeRate",
                        data: JSON.stringify({ rateVm: formData }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var data = JSON.parse(response.d);
                            if (response.d == "sessionNotFound") {
                                alert("sessionNotFound");
                                window.location.href = loginUrl;
                            } else {
                                var data = JSON.parse(response.d);
                                console.log(data);
                                if (data == "success") {
                                    $("#Validation").fadeIn(1000);
                                    $("#Validation").text("Currency exchange rate save successfully");
                                    $("#Validation").fadeOut(5000);
                                    $("#txtfromdate , #txttodate, #txtexchangerate").val("")
                                    $("#ddlcurrency").find("option:first").prop("selected", true)
                                    $("#exratemodal").modal('hide');
                                } else if (data.Message == "exist") {
                                    $("#Validation").fadeIn(1000);
                                    $("#Validation").text(exist);
                                    $("#Validation").fadeOut(5000);
                                    $("#exratemodal").modal('hide');
                                } else if (data.Message == "update") {
                                    $("#Validation").fadeIn(1000);
                                    $("#Validation").text(update);
                                    $("#Validation").fadeOut(5000);
                                    $("#exratemodal").modal('hide');
                                }
                            }
                        },
                        error: function (response) {

                        }
                    });
                }
            })
        }

        function bindSearchRate() {
            $("body").on("click", "#searchrate", function () {
                let fromDate = $("#txtfromdate").val()
                let toDate = $("#txttodate").val()
                let currency = parseInt($("#ddlcurrency").val())

                if (fromDate != "" && toDate != "" && currency > 0) {
                    $.ajax({
                        type: "POST",
                        url: serviceUrl + "/getExchangeRate",
                        data: JSON.stringify({ fromDate: fromDate, toDate: toDate, currency: currency }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var data = JSON.parse(response.d);
                            MakeDataGridForRate(data)
                            $("#rate-card").show()
                        },
                        error: function (responce) {

                        }
                    });
                } else {
                    alert("Currency, from and to date is required")
                }
            })
        }

        function MakeDataGridForCurrency(datasource) {
            $("#gridContainer").DataTable().destroy();

            $("#gridContainer").DataTable({
                "searching": true,
                "bLengthChange": true,
                "scrollY": 400,
                "sSort": true,
                "order": [0, 'desc'],
                "bInfo": false,
                dom: 'Bfrtip',
                buttons: [
                     {
                         extend: 'print',
                         footer: true,
                         exportOptions: {
                             columns: [0, 1, 3]
                         },
                         title: 'Currency',
                         text: '<i class="fa fa-lg fa-print"></i>  Print'

                     },
                     {
                         extend: 'excelHtml5',
                         footer: true,
                         exportOptions: {
                             columns: [0, 1, 3]
                         },
                         title: 'Currency',
                         text: '<i class="fa fa-lg fa-file-excel"></i>  Excel',
                     },
                    {
                        extend: 'pdf',
                        footer: true,
                        exportOptions: {
                            columns: [0, 1, 3]
                        },
                        title: 'Currency',
                        text: '<i class="fa fa-lg fa-file-pdf"></i>  PDF'
                    },
                ],
                "aaData": datasource,
                "aoColumns": [

                                { data: "Value", sDefaultContent: "" },
                                { data: "Text", sDefaultContent: "", width: "20%" },
                                {
                                    data: null,
                                    width: "5%",
                                    sDefaultContent: "",
                                    "orderable": false,
                                    render: function (data, type, row) {
                                        if (type === 'display') {
                                            var del = ' <a href="#" _recordId="' + data.Value + '" class="btnedit" title="Exchange Rate"><i class="fas fa-exchange-alt"></i></a>';
                                            var edit = ' <a href="#" _recordId="' + data.Value + '" class="btnedit" title="Edit Exchange Rate"><i class="fas fa-pencil-alt" ></i></a>';
                                            var html = del;
                                            return html;
                                        }
                                        return data;
                                    }
                                },
                ],
                "columnDefs": [
                    { "targets": [0], "visible": false },

                ]
            });
        }

        function MakeDataGridForRate(dataSource) {
            $("#rateGridContainer").DataTable().destroy();

            $("#rateGridContainer").DataTable({
                "searching": true,
                "sSort": true,
                "fixedHeader": true,
                "pageLength": 5,
                oLanguage: {
                    sEmptyTable: "No results found"
                },
                "lengthMenu": [5, 10, 25],
                "bLengthChange": true,
                "aaData": dataSource,
                "columns": [
                                { data: "FromDate", width: "20%", },
                                { data: "Currency" },
                                { data: "ExchangeRate" },
                                 {
                                     data: null,
                                     width: "5%",
                                     sDefaultContent: "",
                                     "orderable": false,
                                     render: function (data, type, row) {
                                         if (type === 'display') {
                                             var edit = ' <a href="#" _recordId="' + data.Id + '" class="btneditrate" title="Edit Exchange Rate"><i class="fas fa-pencil-alt" ></i></a>';
                                             var html = edit;
                                             return html;
                                         }
                                         return data;
                                     }
                                 },

                ],

            });
        }
    </script>
</asp:Content>
