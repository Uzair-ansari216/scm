﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="PurchaseOrder.aspx.vb" Inherits="Bridge_SCM.PurchaseOrder" %>

<%@ Import Namespace="System.Web.Optimization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/PurchaseOrderScript")%>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-text">
                    <h4 class="card-title" style="color: #000000">Purchase Order</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-primary pull-right" id="addPO" _recordId="0" title="Add Purchase Order"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-content table-responsive">
                                <table id='purchaseorders' class="table table-striped table-bordered display" style="width: 100%">
                                    <thead class="header bg-primary">
                                        <tr class="">
                                            <th>PO ID</th>
                                            <th>Purchase Order No</th>
                                            <th>Purchase Order Date</th>
                                            <th>To </th>
                                            <th></th>

                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalAddPO" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabelPO"></h4>
                </div>
                <div class="modal-body" id="PurchaseOrderForm">
                    <div class="row">
                        <div class="col-sm-12">
                            <!--      Wizard container        -->
                            <div class="wizard-container">
                                <div class="card wizard-card" data-color="rose" id="wizardProfile">
                                    <form>
                                        <!--        You can switch " data-color="purple" "  with one of the next bright colors: "green", "orange", "red", "blue"       -->

                                        <div class="wizard-navigation">
                                            <ul>
                                                <li>
                                                    <a href="#orderInfo" data-toggle="tab">Order Information</a>
                                                </li>
                                                <li>
                                                    <a href="#productInfo" data-toggle="tab" id="pi">Product Information</a>
                                                </li>
                                                <li>
                                                    <a href="#status" data-toggle="tab">Status</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane" id="orderInfo">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span id="ValidationSummary" class="new badge"></span>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <input type="hidden" id="POID" />
                                                            <input type="hidden" id="PONumber" />
                                                            <label class="control-label">From / Bill To</label>
                                                            <asp:DropDownList ID="cboFromBillTo" class="form-control cboFromBillTo" runat="server" _type="From">
                                                            </asp:DropDownList>
                                                            <span id="rCboFromBillTo" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                            <%--<select id="cboFromBillTo" class="form-control"></select>--%>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">To</label>
                                                            <asp:DropDownList ID="cboBillTo" class="form-control cboBillTo" runat="server" _type="To"></asp:DropDownList>
                                                            <%--<select id="cboBillTo" class="form-control"></select>--%>
                                                            <span id="rCboBillTo" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Ship To</label>
                                                            <asp:DropDownList ID="cboShipTo" class="form-control cboShipTo" runat="server" _type="Ship"></asp:DropDownList>
                                                            <%--<select id="cboShipTo" class="form-control"></select>--%>
                                                            <span id="rCboShipTo" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Address</label>
                                                            <input type="text" class="form-control" id="txtFromBillTo" name="txtFromBillTo" placeholder="Address" />
                                                            <span id="rFromBillTo" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Address</label>
                                                            <input type="text" class="form-control" id="txtBillTo" name="txtBillTo" placeholder="Address" />
                                                            <span id="rBillTo" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Address</label>
                                                            <input type="text" class="form-control" id="txtShipTo" name="txtShipTo" placeholder="Address" />
                                                            <span id="rShipTo" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">PO Date</label>
                                                            <input type="text" class="form-control datePicker" id="dtPODate" name="DTBillDate" placeholder="" />
                                                            <span id="rdtPODate" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Currency</label>
                                                            <asp:DropDownList ID="cboCurrency" class="form-control cboCurrency" runat="server"></asp:DropDownList>
                                                            <%--<select id="cboCurrency" class="form-control"></select>--%>
                                                            <span id="rCboCurrency" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Exchange Rate</label>
                                                            <input type="text" class="form-control" id="txtExchange" name="txtExchange" placeholder="" />
                                                            <span id="rExchange" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Billing Month</label>
                                                            <input type="text" class="form-control datePicker" id="DTBillDate" name="DTBillDate" placeholder="" />
                                                            <span id="rDTBillDate" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Spot #</label>
                                                            <input type="text" class="form-control" id="txtSPOTNumber" name="txtSPOTNumber" placeholder="" />
                                                            <span id="rSPOTNumber" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Status</label>
                                                            <asp:DropDownList ID="cboPOStatus" class="form-control cboPOStatus" runat="server"></asp:DropDownList>
                                                            <%--<select id="cboPOStatus" class="form-control"></select>--%>
                                                            <span id="rCboPOStatus" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">PO Reference</label>
                                                            <input type="text" class="form-control" id="txtPORef" name="txtPORef" placeholder="" />
                                                            <span id="rPORef" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Signature</label>
                                                            <asp:DropDownList ID="cboSignature" class="form-control cboSignature" runat="server"></asp:DropDownList>
                                                            <%--<select id="cboSignature" class="form-control"></select>--%>
                                                            <span id="rSignature" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Notes</label>
                                                            <textarea class="form-control" id="txtNotes" name="txtNotes" placeholder=""></textarea>
                                                            <span id="rNotes" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Comments</label>
                                                            <textarea class="form-control" id="txtComments" name="txtComments" placeholder=""></textarea>
                                                            <span id="rComments" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Location</label>
                                                            <asp:DropDownList ID="cboLocation" class="form-control cboLocation" runat="server">
                                                            </asp:DropDownList>
                                                            <%--<select id="cboFromBillTo" class="form-control"></select>--%>
                                                            <span id="rCboLocation" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="productInfo">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <button type="button" class="btn btn-primary pull-right" id="addProductinfo" _action="add">
                                                            <i class="fas fa-plus"></i>
                                                            <div class="ripple-container"></div>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <table id='tableData' class="table table-striped table-bordered display" style="width: 100%">
                                                            <thead class="header bg-primary">
                                                                <tr class="">
                                                                    <th>Model Number</th>
                                                                    <th>Part Number</th>
                                                                    <th>Description</th>
                                                                    <th>Unit</th>
                                                                    <th>Quantity</th>
                                                                    <th>Unit Price</th>
                                                                    <th>Amount</th>
                                                                    <th>PKR Ammount</th>
                                                                    <th>Other Cost</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="tbldetailbody"></tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-8"></div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group label-floating pull-right">
                                                            <input type="text" class="form-control" id="txtTotalAmount" name="txtTotalAmount" readonly="readonly" placeholder="Total Amount" />
                                                            <span id="rTotalAmount" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group label-floating pull-right">
                                                            <input type="text" class="form-control" id="txtPKRTotalAmount" name="txtPKRTotalAmount" readonly="readonly" placeholder="PKR Total" />
                                                            <span id="rEmail1" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="tab-pane" id="status">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Reason</label>
                                                            <asp:DropDownList ID="cboRegion" class="form-control cboRegion" runat="server"></asp:DropDownList>
                                                            <%--<select id="cboPOStatus" class="form-control"></select>--%>
                                                            <span id="rLocation" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Reason Detail</label>
                                                            <textarea class="form-control" id="txtReasonDetail" name="txtReasonDetail" placeholder=""></textarea>
                                                            <span id="rReasonDetail" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <button type="button" class="btn btn-primary pull-right" title="Edit Principle Status" id="btneditstatus" style="display: none"><i class="fas fa-pencil-alt"></i></button>
                                                        <%--<button type="button" class="btn btn-primary pull-right" title="Save Principle Status"><i class="fa fa-save"></i></button>--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="wizard-footer row">
                                <div class="pull-right col-md-5 col-sm-5">
                                    <button type="button" class="btn btn-fill btn-default" id="btnNext">Next</button>
                                    <button type="button" class="btn btn-fill btn-default" id="btnSave">Save</button>
                                    <button type="button" class="btn btn-fill btn-default" id="btnPrint" onclick="bindPrintClick()" title="Print" style="display: none">Print</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <%-- <input type='button' class='btn btn-next btn-fill btn-rose btn-wd' name='next' value='Next' />
                                    <input type='button' class='btn btn-finish btn-fill btn-rose btn-wd' name='finish' value='Finish' />--%>
                                </div>
                                <div class="pull-left col-md-4 col-sm-4">
                                    <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' id="btnPrevious" name='previous' value='Previous' />
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            </form>
                        </div>
                    </div>
                    <!-- wizard container -->
                </div>
            </div>
            <%--<input type="hidden" id="txtID" value="" />


            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-fill btn-default" onclick="OnSave()">Save changes</button>
            </div>--%>
        </div>
    </div>



    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="width: 55%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body" id="modal-product-info">
                    <input type="hidden" id="itemid" />
                    <input type="hidden" id="GUID" value="" />
                    <input type="hidden" id="txtMID" value="" />
                    <input type="hidden" id="Hidden2" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="Span2" class="new badge"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <%--<label class="control-label">Model #</label>--%>
                                <input type="text" class="form-control" id="txtModel" readonly="readonly" name="txtModel" placeholder="Model #" />
                                <span id="rModel" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary btn-sm pull-right" id="btnsearchproduct"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <%--<label class="control-label">Part #</label>--%>
                                <input type="text" class="form-control" id="txtPart" readonly="readonly" name="txtPart" placeholder="Part #" />
                                <span id="rPart" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <%--<label class="control-label">Default Description</label>--%>
                                <input type="text" class="form-control" id="txtDefaultDescription" readonly="readonly" name="txtDefaultDescription" placeholder="Default Description" />
                                <span id="rDefaultDescription" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <%--<label class="control-label">Unit</label>--%>
                                <input type="text" class="form-control" id="txtUnit" name="txtUnit" placeholder="Unit" />
                                <span id="rUnit" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Quantity</label>
                                <input type="text" class="form-control" id="txtQuantity" name="txtQuantity" placeholder="" />
                                <span id="rQuantity" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Unit Price</label>
                                <input type="text" class="form-control" id="txtUnitPrice" name="txtUnitPrice" placeholder="" />
                                <span id="rUnitPrice" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Amount</label>
                                <input type="text" class="form-control" id="txtAmount" name="txtAmount" placeholder="" />
                                <span id="rAmount" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">PKR Amount</label>
                                <input type="text" class="form-control" id="txtPKRAmount" name="txtPKRAmount" placeholder="" />
                                <span id="rPKRAmount" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Other Cost</label>
                                <input type="text" class="form-control" id="txtOtherCost" name="txtOtherCost" placeholder="" />
                                <span id="rOtherCost" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Remarks</label>
                                <input type="text" class="form-control" id="txtItemRemarks" name="txtItemRemarks" placeholder="" />
                                <span id="rItemRemarks" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-fill btn-default" id="btnSaveDetail">Save changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDelete2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H3">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDeleteDetail()">Confirm</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalView2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="OnClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H4"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <table id='tblProductList' class="table table-striped table-bordered display" style="width: 125%; margin-left: -58px">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>Select</th>
                                        <th>ID</th>
                                        <th>Part#</th>
                                        <th>Model#</th>
                                        <th>Product</th>
                                        <th>ItemClass</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalShow" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="width: 50%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="OnClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-content table-responsive">
                                <table id='purchaseInvoicesTable' class="table table-striped table-bordered display" style="width: 100%">
                                    <thead class="header bg-primary">
                                        <tr class="">
                                            <th>PO Date</th>
                                            <th>PO Number</th>
                                            <th>To Name</th>
                                            <th>Number</th>
                                            <th>PO</th>
                                            <th></th>
                                            <th>Select</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="POdeletemodal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete Purchase Order</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record ?</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDelete()">Confirm</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        function bindPrintClick() {
            var reportPath = '<%= System.Configuration.ConfigurationManager.AppSettings("ReportPath").ToString() %>'
            let PurchaseOrderID = $("#POID").val();
            var pagename = "PurchaseOrder.aspx";
            let urlname = "generatePurchaseOrderReport";
            if (PurchaseOrderID == "") {
                alert("ID is required")
            }
            else {
                $.ajax({
                    type: "POST",
                    url: "" + serviceUrl + "/generatePurchaseOrderReport",
                    data: JSON.stringify({ POID: PurchaseOrderID }),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (res) {
                        var data = res.d;
                        if (data == "1") {
                            window.open(reportPath + '?isGeneratedByBuilder=' + false);
                            $("#FormError").css('display', 'none');
                            $("#Error").empty();
                        } else {
                            alert(data)
                        }
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }


        
function bindSavePurchaseOrder() {
    $("body").on("click", "#btnSave", function () {
        var isValid = true

        $("body #PurchaseOrderForm input").each(function (index, row) {
            if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
                if ($(row).val() == "") {
                    $(row).parent().find("span").eq(2).removeClass("hide")
                    isValid = false
                } else {
                    $(row).parent().find("span").eq(2).addClass("hide")
                }
            }
        })

        $("body #PurchaseOrderForm select").each(function (index, row) {
            if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
                if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                    $(row).parent().find("span").eq(2).removeClass("hide")
                    isValid = false
                } else {
                    $(row).parent().find("span").eq(2).addClass("hide")
                }
            }
        })

        var detailArray = new Array()

        if (isValid) {
            var formData = {
                FromBillTo: $(".cboFromBillTo").val(),
                FromBillToAddress: $("#txtFromBillTo").val(),
                _To: $(".cboBillTo").find("option:selected").val(),
                ToAddress: $("#txtBillTo").val(),
                ShipTo: $(".cboShipTo").find("option:selected").val(),
                ShipToAddress: $("#txtShipTo").val(),
                PurchaseOrderDate: $("#dtPODate").val(),
                Currency: $(".cboCurrency").val(),
                ExchangeRate: $("#txtExchange").val(),
                BillDate: $("#DTBillDate").val(),
                SpotNumber: $("#txtSPOTNumber").val(),
                Status: $(".cboPOStatus").val(),
                PurchaseOrderReference: $("#txtPORef").val(),
                Signature: $(".cboSignature").val(),
                Notes: $("#txtNotes").val(),
                Comments: $("#txtComments").val(),
                Reason: $(".cboRegion").val(),
                ReasonDetail: $("#txtReasonDetail").val(),
                TotalAmount: $("#txtTotalAmount").val(),
                TotalPKR: $("#txtPKRTotalAmount").val(),
                Location: $(".cboLocation").val(),
                FromFinancialYearDate: '<%= Session("Datefrom") %>',
                ToFinancialYearDate: '<%= Session("dateTo") %>',
                PurchaseOrderID: $("#POID").val(),
                PurchaseOrderNumber: $("#PONumber").val()
            }

            $("#tbldetailbody tr").each(function (index, row) {
                debugger

                var column = new Array()
                var formDetail = {
                    ModalNumber: $(row).find('td:nth-child(1)').text(),
                    PartNumber: $(row).find('td:nth-child(2)').text(),
                    Description: $(row).find('td:nth-child(3)').text(),
                    Unit: $(row).find('td:nth-child(4)').text(),
                    Quantity: $(row).find('td:nth-child(5)').text(),
                    UnitPrice: $(row).find('td:nth-child(6)').text(),
                    Amount: $(row).find('td:nth-child(7)').text(),
                    PKRAmount: $(row).find('td:nth-child(8)').text(),
                    OtherCost: $(row).find('td:nth-child(9)').text(),
                    Remarks: $(row).find('td:nth-child(10)').text(),
                    ItemId: $(row).find('td:nth-child(11)').text()
                }
                detailArray.push(formDetail)
            })
            $.ajax({
                type: "POST",
                url: "" + serviceUrl + "/SavePurchaseOrder?detail=" + JSON.stringify(detailArray),
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ model: formData }),
                dataType: "json",
                success: function (response) {
                    //alert(responce)
                    var result = JSON.parse(response.d)
                    MakeDataGridForPurchaseOrder(response.d)
                    //$("#modalAddPO").modal('hide')
                    $("#Validation").fadeIn(1000);
                    $("#Validation").text("Purchase Order saved successfully");
                    $("#Validation").fadeOut(5000);
                    //clearFields()
                    $("#btnSave").css("display", "none")
                    $("#btnPrint").css("display", "")
                    $("#POID").val(result[0].PurchaseOrderID)
                    //if (type == "From") {
                    //    $("#txtFromBillTo").val(result)
                    //} else if (type == "To") {
                    //    $("#txtBillTo").val(result)
                    //} else {
                    //    $("#txtShipTo").val(result)
                    //}

                }, error: function (responce) {
                    console.log(responce)
                    alert(responce)
                }

            })
        } else {
            alert("Some required data in needed to save. check all fields")
        }

    })
}
    </script>
</asp:Content>
