﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="AssetLocation.aspx.vb" Inherits="Bridge_SCM.AssetLocation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/AssetLocation")%>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-text">
                    <h4 class="card-title" style="color: #000000">Asset Location</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-primary pull-right" id="btnAdd" _recordid="0" title="Add New Asset Location"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <table id='assetlocationcontainer' class="table table-striped table-bordered display">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>Asset Location</th>
                                        <th>Description</th>
                                        <th>Location</th>
                                        <th>Department</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="assetlocationmodal" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body" id="assetLocationForm">
                    <input type="hidden" id="txtID" value="" />
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Asset Location</label>
                                <input type="text" class="form-control" id="assetlocation" name="assetlocation" placeholder="" />
                                <span id="rAssetLocation" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Description</label>
                                <input type="text" class="form-control" id="description" name="description" placeholder="" />
                                <span id="rDescription" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Location</label>
                                <input type="text" class="form-control" id="location" name="location" placeholder="" />
                                <span id="rLocation" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Department</label>
                                <input type="text" class="form-control" id="department" name="department" placeholder="" />
                                <span id="rDepartment" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-fill btn-default" id="btnsave">Save changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="assetlocationdeletemodal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record ?</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="deleteAssetLocation()">Confirm</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
