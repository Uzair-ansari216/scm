﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="ChangePassword.aspx.vb" Inherits="Bridge_SCM.ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="page-content">

                    <div class="card-header card-header-text">
                        <h4 class="card-title" style="color: #000000">Change Password</h4>
                    </div>

                    <div class="card-content" id="changePasswordForm">
                        <div class="row">
                            <div class="col-sm-9">
                                <span id="Validation" class="new badge"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-11">
                                <div class="form-group label-floating">
                                    <label class="control-label">Current Password</label>
                                    <input type="password" class="form-control" id="txtcurrentpassword" name="txtcurrentpassword" placeholder="" />
                                    <span id="rCurrentpassword" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                    <span id="oldpass" class="requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-1 icon icon-info">
                                <i class="zmdi zmdi-eye display" style="font-size: x-large; margin-top: 20px;"></i>
                            </div>
                            <div class="col-md-3 col-sm-11">
                                <div class="form-group label-floating">
                                    <label class="control-label">New Password</label>
                                    <input type="password" class="form-control" id="txtnewpassword" name="txtnewpassword" placeholder="" />
                                    <span id="rNewpassword" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-1 icon icon-info">
                                <i class="zmdi zmdi-eye display" style="font-size: x-large; margin-top: 20px;"></i>
                            </div>
                            <div class="col-md-3 col-sm-11">
                                <div class="form-group label-floating">
                                    <label class="control-label">Confirm New Password</label>
                                    <input type="password" class="form-control" id="txtcnewpassword" name="txtcnewpassword" placeholder="" />
                                    <span id="rCnewpassword" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-1 icon icon-info">
                                <i class="zmdi zmdi-eye display" style="font-size: x-large; margin-top: 20px;"></i>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <button type="button" class="btn btn-primary pull-right" id="btnsave" title="Save"><i class="fas fa-save"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        const serviceurl = "../../appServices/LoginService.asmx";
        let isPasswordMatched = true
        $(function () {

            clearAllMessages("changePasswordForm")

            var service_DeveloperMode = $("#hdnDevMode").val()
            if (service_DeveloperMode == 'on') {
                addFormFields("Change Password", "changePasswordForm")
            }
            //var service_ToolTip = $("#hdnToolTip").val()
            //if (service_ToolTip == 'on') {

            setToolTip("Change Password", "changePasswordForm", service_DeveloperMode)

            //}

            $("body #changePasswordForm").on("focus", "input, select", function () {
                var service_ToolTip = $("#hdnToolTip").val()
                if (service_ToolTip == 'on') {
                    showCurrentToopTip(this)
                }
            })

            $("body #changePasswordForm").on("focusout", "input, select", function () {
                var service_ToolTip = $("#hdnToolTip").val()
                if (service_ToolTip == 'on') {
                    hideToopTipExceptCurrent(this)
                } else {
                    hideValidationMessage(this)
                }
            })

            $("body #changePasswordForm").on("keypress paste", "input", function (key) {
                var currentElement = $(this)
                var type = $(currentElement).attr("_fieldtype")
                if (type != "") {
                    return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
                }
            })

            $("#txtnewpassword , #txtcnewpassword").on("focusout", function () {
                let newPassword = $("#txtnewpassword").val()
                let confirmPassword = $("#txtcnewpassword").val()
                if (newPassword != "" && confirmPassword != "" && newPassword != confirmPassword) {
                    $("#txtcnewpassword").parent().find('span:eq(3)').removeClass('hide').text("Password not match")
                    isPasswordMatched = false
                } else {
                    $("#txtcnewpassword").parent().find('span:eq(3)').addClass('hide').text("")
                    isPasswordMatched = true
                }
            })

            $(".display").on("click", function () {
                let currentElement = $(this)
                if (currentElement.hasClass("zmdi-eye")) {
                    currentElement.removeClass("zmdi-eye").addClass("zmdi-eye-off")
                    $(currentElement.closest('div')).prev('div').find('input').attr('type', 'text')
                } else {
                    currentElement.removeClass("zmdi-eye-off").addClass("zmdi-eye")
                    $(currentElement.closest('div')).prev('div').find('input').attr('type', 'password')
                }

            })
            bindSave();
        })
        function bindSave() {
            $("#btnsave").on("click", function () {
                var isValid = true

                $("body #changePasswordForm input").each(function (index, row) {
                    if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
                        if ($(row).val() == "") {
                            $(row).parent().find("span").eq(2).removeClass("hide")
                            isValid = false
                        } else {
                            $(row).parent().find("span").eq(2).addClass("hide")
                        }
                    }
                })

                if (isValid && isPasswordMatched) {
                    $.ajax({
                        type: "POST",
                        url: "" + serviceurl + "/changePassword",
                        data: JSON.stringify({ currentPassword: $("#txtcurrentpassword").val(), newPassword: $("#txtnewpassword").val() }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var data = JSON.parse(response.d)
                            if (data.Item2) {
                                $("#oldpass").text("")
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text(data.Item1);
                                $("#Validation").fadeOut(5000);
                            } else {
                                $("#oldpass").text(data.Item1)
                            }
                        },
                        error: function (response) {

                        }
                    });
                }

            })
        }
    </script>

</asp:Content>

