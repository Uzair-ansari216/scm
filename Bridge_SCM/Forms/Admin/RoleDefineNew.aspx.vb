﻿Imports System.Web.Script.Services
Imports System.Web.Script.Serialization

Partial Public Class RoleDefineNew
    Inherits System.Web.UI.Page
    Public Shared objSO As New BizSoft.Bridge_SCM.DefineRole()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        clsCommon.ValidSession()
        If Not Page.IsPostBack Then
            Session.Add("Role_Detail", SalesOderDetail())
        End If

    End Sub
    Public Shared Function SalesOderDetail() As DataTable
        Dim objSO As New BizSoft.Bridge_SCM.DefineRole()
        Dim dt As DataTable
        dt = objSO.CreateRoleDefineDatatable()
        Return dt
    End Function

    ' GetViewGridDetailsList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetViewGridDetailsList() As List(Of Dictionary(Of String, Object))
        Dim rows As New List(Of Dictionary(Of String, Object))()
        If HttpContext.Current.Request.QueryString.Count > 0 Then
            Dim id As Double = HttpContext.Current.Request.QueryString("id")
            Dim objSO As New BizSoft.Bridge_SCM.DefineRole()
            Dim dt As DataTable = objSO.GetDetailRole_New(id)
            Dim dtCopy As DataTable = dt.Copy()
            Dim ds As New DataSet
            ds.Tables.Add(dtCopy)

            HttpContext.Current.Session("Role_Detail") = ds

            Dim row As Dictionary(Of String, Object)

            For Each dr As DataRow In dt.Rows
                row = New Dictionary(Of String, Object)
                For Each col As DataColumn In dt.Columns
                    row.Add(col.ColumnName, dr(col))

                Next
                rows.Add(row)
            Next

        End If

        Return rows
    End Function


End Class