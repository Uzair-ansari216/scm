﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReportRoleDefine.aspx.vb" Inherits="Bridge_SCM.ReportRoleDefine" MasterPageFile="~/Forms/MasterPages/Main.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/ReportRoleDefineScript")%>

    <input type="hidden" value="add" id="action" />


    <div class="modal fade" id="modalView" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Role List </h4>
                </div>
                <div class="row">
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <table id='tableDetail' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>Select</th>
                                        <th>ID</th>
                                        <th>Role</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="page-content">

                    <div class="card-header card-header-text">
                        <h4 class="card-title" style="color:#000000">Report Role Define</h4>
                    </div>

                    <div class="card-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <span id="Validation" class="new badge"></span>
                                <div id="FormMsg" class="new badge"></div>
                                <div id="divTopButtons" class="pull-right">
                                    <button id="btnSave" onclick="OnSave()" type="button" class="btn btn-primary">
                                        <i class="fas fa-save"></i>
                                    </button>
                                    <button id="btnView" type="button" onclick="OnView()" class="btn btn-primary">
                                        <i class="fas fa-eye"></i>
                                    </button>

                                </div>
                            </div>
                        </div>

                        <input type="hidden" id="ddlMember" value="" />
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <label class="control-label">Role ID</label>
                                    <input type="text" class="form-control" readonly="readonly" id="txtRoleID" name="txtRoleID" placeholder="" />
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <label class="control-label">Role Name</label>
                                    <input type="text" class="form-control" id="txtRoleName" readonly="readonly" name="txtRoleName" placeholder="" />
                                </div>
                                <span id="rRoleName" class="new badge hide">required</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <input type="button" id="btnCheckAll" style="display: block" class="check btn btn-primary" value="uncheck all" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="modal-body">
                                <div class="col-lg-12">
                                    <div class="card-content table-responsive">
                                        <table id='tblReport' class="table table-striped table-bordered display" style="width: 100%">
                                            <thead class="header bg-primary">
                                                <tr class="">
                                                    <th>ID</th>
                                                    <th>Is View</th>
                                                    <th>Module</th>
                                                    <th>Category</th>
                                                    <th>Report</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
