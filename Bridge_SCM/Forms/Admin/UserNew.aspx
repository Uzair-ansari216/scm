﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UserNew.aspx.vb" Inherits="Bridge_SCM.UserNew" MasterPageFile="~/Forms/MasterPages/Main.Master" %>

<%--<%@ Register Src="~/Forms/UserControls/UserList.ascx" TagName="ViewList" TagPrefix="UserList" %>--%>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        var pagename = 'UserNew.aspx';
        var serviceUrl = "../../appServices/UserService.asmx";
        var count = 0;
        var tblDetail = "#tableDetail";
        var tblMember = "#tblMembers";
        var tblLocation = "#tblLocation";

        var success = "Record has been saved Successfully!";

        var table, table2, table3, table4;
        $(document).ready(function () {
            OnLoadGridLocation();
            OnLoad();
            OnSelect();
        });

        function OnLoadGridLocation() {
            $.ajax({
                type: "POST",
                url: "" + serviceUrl + "/loadGridLocation",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $(tblLocation).DataTable().destroy();
                    LoadDataTableLocation(response.d);
                },
                error: OnErrorCall
            });
        }

        function LoadDataTableLocation(datasource) {
            table4 = $(tblLocation).DataTable({
                "searching": false,
                "bLengthChange": false,
                "sSort": true,
                "order": [0, 'desc'],
                "bInfo": false,
                "bPaginate": false,
                "aaData": JSON.parse(datasource),
                "aoColumns": [
                                {
                                    data: null,
                                    sDefaultContent: "",
                                    "orderable": false,
                                    render: function (data, type, row) {
                                        if (type === 'display') {
                                            var select = '<div class="checkbox"><label><input type="checkbox" name="optionsCheckboxes" id="' + data.LocationID + '" ><span class="checkbox-material"><span class="check"></span></span></label></div>';
                                            var html = select;
                                            return html;
                                        }
                                        return data;
                                    }
                                },
                                { data: "LocationID", sDefaultContent: "" },
                                { data: "Location", sDefaultContent: "" },
                                { data: "LocationCode", sDefaultContent: "" },
                                { data: "CompanyName", sDefaultContent: "" }

                ],
                "columnDefs": [
                    { "targets": [1], "visible": false }

                ]
            });
        }

        function OnLoad() {
            $("#txtCode").attr('readonly', 'readonly');


            //  GetUserRoleList
            $.ajax({
                type: "POST",
                url: '' + serviceUrl + '/GetUserRoleList',
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    $("#ddlRole").empty();
                    $("#ddlRole").append("<option value='-1'>--Please Select--</option>");
                    jQuery.each(res.d, function (index, item) {
                        $("#ddlRole").append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });

                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });

            //  GetReportTypeList

            $.ajax({
                type: "POST",
                url: '' + serviceUrl + '/GetEmployeeList',
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    $("#ddlEmployee").empty();
                    $("#ddlEmployee").append("<option value='-1'>--Please Select--</option>");
                    jQuery.each(res.d, function (index, item) {
                        $("#ddlEmployee").append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });

                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });

            $.ajax({
                type: "POST",
                url: '' + serviceUrl + '/GetLocationList',
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    $("#ddlLocation").empty();
                    $("#ddlLocation").append("<option value='-1'>--Please Select--</option>");
                    jQuery.each(res.d, function (index, item) {
                        $("#ddlLocation").append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });

                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });

        }
        function OnSelect() {
            $(tblDetail).on('click', '.select', function () {
                var row = $(this).closest('tr');
                var rowIndex = table2.row(row).index();
                var rowData = table2.row(rowIndex).data();
                console.log(rowData);
                OnSelectID(rowData.Value)
                OnSelectLocation(rowData.Value)
            });
        }
        function OnSelectID(ID) {
            $.ajax({
                type: "POST",
                url: '' + serviceUrl + '/GetDetails?id=' + ID,
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    var data = res.d;
                    console.log(data);
                    $("#txtCode").val(data[0]);
                    $("#txtLogin").val(data[1]);
                    $("#ddlRole").val(data[2]);
                    $("#txtPassword").val(data[3]);
                    $("#txtRePassword").val(data[3]);
                    $("#txtUserName").val(data[4]);
                    var ischk = data[5];
                    if (ischk == "True") {
                        $("#chkIsPower").prop('checked', true);
                    }
                    else if (ischk == "False") {
                        $("#chkIsPower").prop('checked', false);
                    }
                    if (data[8] == "") {
                        $("#ddlEmployee").val("-1");
                    } else {
                        $("#ddlEmployee").val(data[8]);
                    }
                    $("#ddlLocation").val(data[9]);

                    var isrest = data[10];
                    if (isrest == "True") {
                        $("#chkIsRest").prop('checked', true);
                    }
                    else if (isrest == "False") {
                        $("#chkIsRest").prop('checked', false);
                    }
                    $("#action").val("edit")
                    $("#modalView").modal('hide');
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        }

        function OnSelectLocation(ID) {
            $.ajax({
                type: "POST",
                url: '' + serviceUrl + '/GetDetailsLocation?id=' + ID,
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    var data = JSON.parse(res.d);

                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            $('input[id=' + data[i].ID + ']').attr('checked', true);
                            $('input[id=' + data[i].ID + ']').prop('checked', true);
                        }
                    } else {
                        $('input[name="optionsCheckboxes"]').attr('checked', false);
                        $('input[name="optionsCheckboxes"]').prop('checked', false);
                    }

                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        }
        function AddNew() {
            $("#action").val("add");
            $("#FormMsg").empty();
            $("#txtCode").val("");
            $("#txtLogin").val("");
            $("#ddlRole").val("-1");
            $("#ddlEmployee").val("-1");
            $("#ddlLocation").val("-1");
            $("#txtPassword").val("");
            $("#txtRePassword").val("");
            $("#txtUserName").val("");
            $("#chkIsPower").prop('checked', false);
            $("#chkIsRest").prop('checked', false);
        }

        function AddNew2() {
            $("#action").val("add");
            $("#FormMsg").empty();
            $("#txtCode").val("");
            $("#txtLogin").val("");
            $("#ddlRole").val("-1");
            $("#ddlEmployee").val("-1");
            $("#ddlLocation").val("-1");
            $("#txtPassword").val("");
            $("#txtRePassword").val("");
            $("#txtUserName").val("");
            $("#chkIsPower").prop('checked', false);
            $("#chkIsRest").prop('checked', false);

            OnLoadGridLocation()
        }

        function View() {
            OnLoadGrid();
            
        }

        function OnLoadGrid() {
            $.ajax({
                type: "POST",
                url: '' + serviceUrl + '/GetGridList',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    let data = JSON.parse(response.d);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                        $("#modalAdd").modal('hide');
                    } else {
                        $(tblDetail).DataTable().destroy();
                        LoadDataTableDetail(data.GenericList);
                        $("#modalView").modal();
                    }
                },
                error: OnErrorCall
            });
        }

        function LoadDataTableDetail(datasource) {
            table2 = $(tblDetail).DataTable({
                "searching": false,
                "bLengthChange": false,
                "sSort": true,
                "order": [0, 'desc'],
                "bInfo": false,
                "aaData": datasource,
                "aoColumns": [
                     {
                         data: null,
                         sDefaultContent: "",
                         "orderable": false,
                         render: function (data, type, row) {
                             if (type === 'display') {
                                 var del = ' <a href="#" class="select" title="Select">Select</a>';
                                 var html = del; return html;
                             }
                             return data;
                         }
                     },
                                { data: "Value", sDefaultContent: "" },
                                { data: "Text", sDefaultContent: "" }

                ]
            });
        }

        function OnErrorCall() {
            alert("Something went wrong");
        }

        function Save() {

            $("#FormMsg").empty();
            $("Validation").text("");

            var code = $("#txtCode").val();
            var login = $("#txtLogin").val();
            var role = $("#ddlRole").val();
            var password = $("#txtPassword").val();
            var rePassword = $("#txtRePassword").val();
            var userName = $("#txtUserName").val();
            var chkIsPower = $("#chkIsPower");
            var chkIsRest = $("#chkIsRest");
            var employee = $("#ddlEmployee").val();
            var location = $("#ddlLocation").val();

            if (login == "") {
                $("#FormMsg").append("<span class='dialogerror'> Login required  </span> <br>");
                $("#txtLogin").addClass("haserror");
            }
            else if (role == "-1") {
                $("#FormMsg").append("<span class='dialogerror'> Please Select User Role </span> <br>");
                $("#ddlRole").addClass("haserror");
            }
            else if (location == "-1") {
                $("#FormMsg").append("<span class='dialogerror'> Please Select Location </span> <br>");
                $("#ddlReportType").addClass("haserror");
            }
            else if (password == "") {
                $("#FormMsg").append("<span class='dialogerror'> Password required  </span> <br>");
                $("#txtPassword").addClass("haserror");
            }
            else if (rePassword == "") {
                $("#FormMsg").append("<span class='dialogerror'> Re-Password required  </span> <br>");
                $("#txtRePassword").addClass("haserror");
            }
            else if (password != rePassword) {
                $("#FormMsg").append("<span class='dialogerror'> Password does not match with Re-Password  </span> <br>");
                $("#txtPassword").addClass("haserror");
                $("#txtRePassword").addClass("haserror");
            }
            else if (userName == "") {
                $("#FormMsg").append("<span class='dialogerror'> User Name required  </span> <br>");
                $("#txtPassword").addClass("haserror");
            }
            else {
                var act = $("#action").val();

                if ($('#chkIsPower').is(':checked')) {
                    IsPower = 1;
                } else {
                    IsPower = 0;
                }

                if ($('#chkIsRest').is(':checked')) {
                    IsRest = 1;
                } else {
                    IsRest = 0;
                }

                var spFound;
                $.getScript('../../FormScript/ValidationScript.js', function () {
                    spFound = valid();
                    if (spFound == false) {
                        $.ajax({
                            type: "POST",
                            url: '' + serviceUrl + '/Save?action=' + act + '&code=' + code +
                                '&login=' + login + '&role=' + role + '&password=' + password +
                                 '&rePassword=' + rePassword + '&userName=' + userName + '&IsPower=' + IsPower + '&Employee=' + employee + '&Location=' + location + '&IsRest=' + IsRest,
                            contentType: "application/json",
                            dataType: "json",
                            success: function (res) {
                                
                                var data = JSON.parse(res.d);
                                if (!data.Status) {
                                    $("#Validation").fadeIn(1000);
                                    $("#Validation").text(data.Message);
                                    $("#Validation").fadeOut(5000);
                                    $("#modalAdd").modal('hide');
                                }
                                if (data != null) {
                                    if (data.ID != null) {
                                        $("#action").val("edit");
                                    }

                                    if (data.ID > 0) {
                                        DeleteDetail(data.ID)

                                    }

                                    $("#txtCode").val(data.ID);
                                    var result = data.Message;
                                    $("#Validation").fadeIn(1000);
                                    $("#Validation").text(result);
                                    $("#Validation").fadeOut(5000);


                                    refreshViewGrid();
                                    AddNew();
                                }
                                console.log(res);

                            },
                            failure: function (errMsg) {
                                alert(errMsg);
                            }
                        });
                    }
                    else {
                        alert("Illegal Characters Detected!")
                    }
                });
            }

        }

        function SaveDetail(UserID, ID) {
            if (ID > 0) {
                $.ajax({
                    type: "POST",
                    url: '' + serviceUrl + '/SaveDetail?UserID=' + UserID + '&ID=' + ID,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (res) {
                        var data = res.d;

                        console.log(res);

                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        function DeleteDetail(UserID) {
            if (UserID > 0) {
                $.ajax({
                    type: "POST",
                    url: '' + serviceUrl + '/DeleteDetail?UserID=' + UserID,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (res) {
                        var data = res.d;
                        $IDs = $("#tblLocation input:checkbox:checked").map(function () {
                            SaveDetail(UserID, $(this).attr("id"))
                        }).get();
                        console.log(res);

                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        function ShowPassword() {
            var btn = $("#lnkShow").text();
            if (btn == "Show Password") {
                $('#txtPassword').prop({ type: "text" });
                $('#txtRePassword').prop({ type: "text" });
                $("#lnkShow").text("Hide Password");
            }
            else if (btn == "Hide Password") {
                $('#txtPassword').prop({ type: "password" });
                $('#txtRePassword').prop({ type: "password" });
                $("#lnkShow").text("Show Password");
            }
            else {
                $('#txtPassword').prop({ type: "password" });
                $('#txtRePassword').prop({ type: "password" });
                $("#lnkShow").text("Show Password");
            }

        }

        function refreshViewGrid() {
            $.ajax({
                type: "POST",
                url: '' + serviceUrl + '/GetGridList',
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    var data = res.d;
                    var trf = $("#usergrdList tbody:first tr:first")[0];
                    $("#usergrdList tbody:first").empty().append(trf);

                    for (var i = 0; i <= data.length; i++) {
                        $("#usergrdList").jqGrid('addRowData', i + 1, data[i]);

                    }

                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });

        }
    </script>

    <input type="hidden" value="add" id="action" />


    <div class="modal fade" id="modalView" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Users List </h4>
                </div>

                <hr />
                <div class="row">
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <table id='tableDetail' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>Select</th>
                                        <th>ID</th>
                                        <th>Name</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="page-content">

                    <div class="card-header card-header-text">
                        <h4 class="card-title" style="color: #000000">User</h4>
                    </div>

                    <div class="card-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <span id="Validation" class="new badge"></span>
                                <div id="FormMsg" class="new badge"></div>
                                <div id="divTopButtons" class="pull-right">
                                    <button class="btn btn-primary" type="button" onclick="AddNew2()">
                                        <i class="fas fa-sync-alt"></i>
                                    </button>
                                    <button id="btnSave" onclick="Save()" type="button" class="btn btn-primary">
                                        <i class="fas fa-save"></i>
                                    </button>
                                    <button id="btnView" type="button" onclick="View()" class="btn btn-primary">
                                        <i class="fas fa-eye"></i>
                                    </button>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <label class="control-label">Code</label>
                                    <input type="text" value="" id="txtCode" class="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <label class="control-label">Login</label>
                                    <input type="text" value="" id="txtLogin" class="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <label class="control-label">Role</label>
                                    <select id="ddlRole" class="form-control"></select>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="ddlMember" value="" />
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <label class="control-label">Password</label>
                                    <input type="password" value="" id="txtPassword" class="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <label class="control-label">Re-Password</label>
                                    <input type="password" value="" id="txtRePassword" class="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label"></label>
                                <a href="#" id="lnkShow" onclick="ShowPassword()">Show Password</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <label class="control-label">User Name</label>
                                    <input type="text" value="" id="txtUserName" class="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <label class="control-label">Employee</label>
                                    <select id="ddlEmployee" class="form-control"></select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <label class="control-label">Location</label>
                                    <select id="ddlLocation" class="form-control"></select>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-sm-4 checkbox-radios">
                                <div class="checkbox">
                                    <label>
                                        <input name="form-field-checkbox" class="ace ace-checkbox-2" id="chkIsPower" type="checkbox" />
                                        Power User
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4 checkbox-radios">
                                <div class="checkbox">
                                    <label>
                                        <input name="form-field-checkbox" class="ace ace-checkbox-2" id="chkIsRest" type="checkbox" />
                                        Restrict User
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="modal-body">
                                <div class="col-lg-12">
                                    <div class="card-content table-responsive">
                                        <table id='tblLocation' class="table table-striped table-bordered display" style="width: 100%">
                                            <thead class="header bg-primary">
                                                <tr class="">
                                                    <th></th>
                                                    <th>LocationID</th>
                                                    <th>Location</th>
                                                    <th>Location Code</th>
                                                    <th>Company</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
