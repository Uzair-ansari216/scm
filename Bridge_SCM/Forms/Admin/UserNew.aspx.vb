﻿
Imports System.Web.Script.Serialization
Partial Public Class UserNew
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        clsCommon.ValidSession()
    End Sub
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function loadGridMember() As String
        Try
            Dim list As New List(Of UserProfileModel)
            Dim obj As New BizSoft.Bridge_SCM.User()
            Dim ds As DataSet = obj.GetMembers()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    If row("Pic").ToString = "" Then
                        list.Add(New UserProfileModel() With {
                          .ID = row("ID").ToString,
                          .Name = row("Fname").ToString,
                          .CNIC = row("CNICNo").ToString,
                          .Email = row("Email").ToString,
                          .Mobile = row("MobileNo").ToString,
                          .Pic = "user.jpg"
                          })
                    Else
                        list.Add(New UserProfileModel() With {
                          .ID = row("ID").ToString,
                          .Name = row("Fname").ToString,
                          .CNIC = row("CNICNo").ToString,
                          .Email = row("Email").ToString,
                          .Mobile = row("MobileNo").ToString,
                          .Pic = row("Pic").ToString
                          })
                    End If
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetMemberList() As List(Of DropdownModel)
        Dim objDR As New BizSoft.Bridge_SCM.DefineRole
        Dim dtDR As DataSet = objDR.GetMembers

        Dim ddlList As New List(Of DropdownModel)

        For Each dr As DataRow In dtDR.Tables(0).Rows
            Dim ddl As New DropdownModel
            ddl.Text = dr("Fname").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function
    Protected Sub btnDelete_Click()

        'If BizSoft.Utilities.HasRights("mnuAdminUserDefine", "IsDelete", "1") = False Then
        '    lblMessage.Text = "Sorry, you don't have sufficient rights to perform  this action"
        '    Exit Sub
        'End If

        'Dim objCity As New BizSoft.Bridge_RS.User
        'Dim intSatatus As Boolean = 0
        'Dim retCode As Integer = 0
        'Dim strMsg As String = ""
        'Try
        '    If Convert.ToInt64(hdnRecID.Value) > 0 Then
        '        objCity.Delete(Convert.ToInt64(hdnRecID.Value), Session("CompanyID"), retCode, intSatatus, strMsg)
        '    End If
        '    If intSatatus Then
        '        lblMessage.Text = strMsg
        '        txtCode.Text = ""
        '        txtLogin.Text = ""
        '        txtName.Text = ""
        '        txtpassword.Text = ""
        '        txtRepassword.Text = ""

        '        hdnRecID.Value = "0"
        '    Else
        '        lblMessage.Text = strMsg
        '    End If

        'Catch ex As Exception
        '    lblMessage.Text = ex.Message
        'End Try
    End Sub
End Class