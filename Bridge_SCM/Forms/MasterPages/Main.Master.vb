﻿Public Class Main
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        menucell.InnerHtml = GenerateMenu()
    End Sub

    Private Function GenerateMenu() As String
        Dim StrQry, StrMenu, StrChildMenu As String

        Dim ds As New DataSet()

        StrQry = "getMenu Parent,0,0," & Session("UserID") & ""

        ds = BizSoft.DBManager.GetDataSet(StrQry)
        If ds.Tables(0).Rows.Count > 0 Then
            StrMenu = "<ul Class='nav'><li style='background-color:#35c3ef'><a style='background-image: url(../../BackgroundImage/main-icon-01.png);  height: 50px;color: white !important; background-size: cover;' href='/Forms/Admin/Home.aspx'></a></li>"
            For I As Integer = 0 To ds.Tables(0).Rows.Count - 1
                StrMenu += "<li> <a style='background-image: url(../../BackgroundImage/" & ds.Tables(0).Rows(I)("BackgroundImage").ToString & ");  height: 50px;color: white !important; background-size: cover;' data-toggle='collapse' href='#" & ds.Tables(0).Rows(I)("ID").ToString & "' class='collapsed main-menu' aria-expanded='false' style='background-color:#35c3ef'><b class='caret'></b></a>"
                If ds.Tables(0).Rows(I)("ChildCount").ToString > 0 Then
                    StrChildMenu = GenerateChild(ds.Tables(0).Rows(I)("ID").ToString, ds.Tables(0).Rows(I)("RoleID").ToString)
                    StrMenu += StrChildMenu.ToString
                End If

                StrMenu += "</li>"
            Next
            StrMenu += "</ul>"
        End If

        Return StrMenu
    End Function
    Private Function GenerateChild(ByVal MenuID As Integer, ByVal RoleID As Integer) As String
        Dim StrQry As String
        Dim StrMenu As String = ""
        Dim ds As New DataSet()

        StrQry = "getMenu Child," & RoleID & ",'" & MenuID & "'," & Session("UserID") & ""
        ds = BizSoft.DBManager.GetDataSet(StrQry)
        If ds.Tables(0).Rows.Count > 0 Then
            StrMenu += "<div class='collapse' id=" & ds.Tables(0).Rows(0)("ParentID").ToString & " aria-expanded='false' style='height: 0px;'> <ul class='nav'> "
            For I As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim row As DataRow = ds.Tables(0).Rows(I)
                'If I <> ds.Tables(0).Rows.Count - 1 Then
                Dim nextRow As DataRow = ds.Tables(0)(I + 1)
                If nextRow Is Nothing Then
                    StrMenu += "<li><a class='menu1' style='background-image: url(../../BackgroundImage/" & ds.Tables(0).Rows(I)("BackgroundImage").ToString & ");  height: 41px;color: white !important; background-size: cover;' href='" & ds.Tables(0).Rows(I)("WP_FormName").ToString & "'></a>"
                    'StrMenu += GenerateSubChild(ds.Tables(0).Rows(I)("ID").ToString, ds.Tables(0).Rows(I)("RoleID").ToString)
                    StrMenu += "</li>"
                Else
                    If row("SubMenu").ToString = "2" And nextRow("SubMenu").ToString = "2" Then
                        StrMenu += "<li>"
                        'StrMenu += " <ul class='nav'><li class='inner-menu'>"
                        For index = 1 To 2
                            If index = 1 Then
                                StrMenu += "<a class='menu2' style='background-image: url(../../BackgroundImage/" & row("BackgroundImage").ToString & ");     height: 41px;color: white !important; background-size: cover;' href='" & row("WP_FormName").ToString & "'></a>"
                            Else
                                StrMenu += "<a class='menu2' style='background-image: url(../../BackgroundImage/" & nextRow("BackgroundImage").ToString & ");     height: 41px;color: white !important;  background-size: cover;' href='" & nextRow("WP_FormName").ToString & "'></a>"
                            End If

                        Next


                        'StrMenu += "</li>"
                        'StrMenu += "</ul>"
                        StrMenu += "</li>"
                        I = I + 1
                    Else
                        StrMenu += "<li><a class='menu1' style='background-image: url(../../BackgroundImage/" & ds.Tables(0).Rows(I)("BackgroundImage").ToString & ");color: white !important;    height: 41px; background-size: cover;' href='" & ds.Tables(0).Rows(I)("WP_FormName").ToString & "'></a>"
                        'StrMenu += GenerateSubChild(ds.Tables(0).Rows(I)("ID").ToString, ds.Tables(0).Rows(I)("RoleID").ToString)
                        StrMenu += "</li>"

                        'End If
                    End If
                End If

                'If ds.Tables(0).Rows(I)("SubMenu").ToString() = "2" And ds.Tables(0).Rows(I + 1)("SubMenu").ToString() = "2" Then

                'Else
                '    StrMenu += "<li><a class='menu1' href='" & ds.Tables(0).Rows(I)("WP_FormName").ToString & "'>" & ds.Tables(0).Rows(I)("MenuName").ToString & "</a>"
                '    'StrMenu += GenerateSubChild(ds.Tables(0).Rows(I)("ID").ToString, ds.Tables(0).Rows(I)("RoleID").ToString)
                '    StrMenu += "</li>"
                'End If
            Next
            StrMenu += "</ul>"
            StrMenu += "</div>"
        End If

        Return StrMenu
    End Function

    Private Function GenerateChild2(ByVal MenuID As Integer, ByVal RoleID As Integer) As String
        Dim StrQry As String
        Dim StrMenu As String = ""
        Dim ds As New DataSet()

        StrQry = "getMenu Child," & RoleID & ",'" & MenuID & "'"
        ds = BizSoft.DBManager.GetDataSet(StrQry)
        If ds.Tables(0).Rows.Count > 0 Then
            StrMenu += "<div class='collapse' id=" & ds.Tables(0).Rows(0)("ParentID").ToString & " aria-expanded='false' style='height: 0px;'> <ul class='nav'> "
            For I As Integer = 0 To ds.Tables(0).Rows.Count - 1
                StrMenu += "<li><a href='" & ds.Tables(0).Rows(I)("WP_FormName").ToString & "'>" & ds.Tables(0).Rows(I)("MenuName").ToString & "</a>"
                'StrMenu += GenerateSubChild(ds.Tables(0).Rows(I)("ID").ToString, ds.Tables(0).Rows(I)("RoleID").ToString)
                StrMenu += "</li>"
            Next
            StrMenu += "</ul>"
            StrMenu += "</div>"
        End If

        Return StrMenu
    End Function

    Private Function GenerateSubChild(ByVal MenuID As Integer, ByVal RoleID As String) As String
        Dim StrQry As String
        Dim StrMenu As String = ""
        Dim ds As New DataSet()

        StrQry = " Select ID,ParentID,MenuName,M.WP_FormName, isView,isAdd,isEdit,isDelete from GL_Menu M"
        StrQry += " inner join GL_ROLE_DETAIL D on M.DT_MenuObjectName=D.MnueName Where  RoleID=" & RoleID & " and M.ParentID='" & MenuID & "' And M.IsActive = 0 "
        'AND D.IsShow = 1"
        'StrQry += " order by M.MenuOrder"
        ds = BizSoft.DBManager.GetDataSet(StrQry)
        If ds.Tables(0).Rows.Count > 0 Then
            StrMenu += "  <ul class='submenu'> "
            For I As Integer = 0 To ds.Tables(0).Rows.Count - 1
                StrMenu += "<li><a href='" & ds.Tables(0).Rows(I)("WP_FormName").ToString & "'>" & ds.Tables(0).Rows(I)("MenuName").ToString & "</a>"
                'GenerateChild(ds.Tables(0).Rows(I)("ID").ToString)
                StrMenu += "</li>"

            Next
            StrMenu += "</ul>"
        End If

        Return StrMenu
    End Function

End Class