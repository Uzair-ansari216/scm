﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="VendorAgeingNew.aspx.vb" Inherits="Bridge_SCM.VendorAgeingNew" MasterPageFile="~/Forms/MasterPages/Main.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


     <div class="panel panel-default">
        <div class="panel-heading">
            <h5>Ageing Reports </h5>
        </div>
              <div class="panel-body">

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-6">

                            <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-4 text-right">
                                    <span class="cr">From Date</span>
                                </div>
                                <div class="col-sm-6">
                                    <div class="input-group date">
                                        <input type="text" id="txtFromDate" class="form-control" />
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-4 text-right">
                                    <span class="cr">To Date</span>
                                </div>
                                <div class="col-sm-6">
                                    <div class="input-group date">
                                        <input type="text" id="txtToDate" class="form-control" />
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>

                                </div>
                            </div>

                             <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-4 text-right">
                                    <span class="cr">Supplier</span>
                                </div>
                                <div class="col-sm-8">
                                   <select id="ddlSupllier" class="select"></select>
                                </div>
                            </div>

                             <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-4 text-right">
                                    <span class="cr">Supplier Type</span>
                                </div>
                                <div class="col-sm-8">
                                   <select id="ddlSupplierType" class="select"></select>
                                </div>
                            </div>

                             <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-4 text-right">
                                    <span class="cr">Reports</span>
                                </div>
                                <div class="col-sm-8">
                                   <select id="ddlReports" class="select"></select>
                                </div>
                            </div>

                        </div>
                  
                    <div class="col-sm-6">
                        
                           <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-4 text-right">
                                    <span class="cr">Location</span>
                                </div>
                                <div class="col-sm-8">
                                   <select id="ddlLocation" class="select"></select>
                                </div>
                            </div>

                          <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-4 text-right">
                                    <span class="cr">Days Interval</span>
                                </div>
                              <div class="col-sm-8">
                                  <div class="col-sm-3" style="padding: 0px;">
                                      <input type="text" id="txtInterval1" class="form-control" style="width:90%" />
                                  </div>

                                   <div class="col-sm-3"  style="padding: 0px;">
                                      <input type="text" id="txtInterval2" class="form-control"  style="width:90%" />
                                  </div>

                                   <div class="col-sm-3"  style="padding: 0px;">
                                      <input type="text" id="txtInterval3" class="form-control"  style="width:90%" />
                                  </div>

                                   <div class="col-sm-3"  style="padding: 0px;">
                                      <input type="text" id="txtInterval4" class="form-control"  style="width:90%" />
                                  </div>
                              </div>
                            </div>

                            <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-4 text-right">
                                    <span class="cr">Ageing Days</span>
                                </div>
                                <div class="col-sm-3">
                                   <input type="text" id="txtAgeingDays"  class="form-control"  style="width:90%" />
                                </div>
                            </div>

                            <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-4 text-right" style="padding: 0px;">
                                    <span class="cr">Ageing Days Interval</span>
                                </div>
                                <div class="col-sm-3">
                                   <input type="text" id="txtAgeingDaysInterval" class="form-control"  style="width:90%" />
                                </div>
                            </div>

                    </div>
                </div>

                     <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-12 ">
                            <div class="pull-right">
                                <button type="button" class="btn btn-primary btn-md" onclick="Save()">
                                    <span class="glyphicon glyphicon-search"></span>Show
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
        </div>


</asp:Content>