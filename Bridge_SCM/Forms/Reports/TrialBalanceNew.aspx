﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TrialBalanceNew.aspx.vb" Inherits="Bridge_SCM.TrialBalanceNew" MasterPageFile="~/Forms/MasterPages/Main.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../../content/bootstrap.css" rel="stylesheet" />
    <link href="../../Js/JQGridReq/CustomCss.css" rel="stylesheet" />
    <link href="../../Js/jquery-ui.css" rel="stylesheet" />
    <link href="../../content/Checkbox/customcss.css" rel="stylesheet" />
    <link href="../../content/Checkbox/customradio.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Scripts/jquery-2.1.1.min.js"></script>
    <script src="../../Js/jquery-ui.js"></script>
    <script src="../../content/Checkbox/customjs.js"></script>
    <script src="../../Js/customMethods.js"></script>

    <link href='https://fonts.googleapis.com/css?family=Slabo+27px|Exo+2' rel='stylesheet' type='text/css'>

    <style type="text/css">
        body {
            font-family: 'Slabo 27px', serif;
        }
    </style>

    <script type="text/javascript">

        var pagename = "TrialBalanceNew.aspx";

        $(document).ready(function () {
            $("#txtFromDate").datepicker();
            $("#txtToDate").datepicker();

            $("#txtFromDate").val(GetToday());
            $("#txtToDate").val(GetToday());

            OnLoad();

        });

        function OnLoad() {
            FillDropdown(pagename, "GetLocationList", "ddlLocation");
            FillDropdown(pagename, "GeSegmentList", "ddlSegment");
            FillDropdown(pagename, "GetDepartmentList", "ddlDepartment");
            FillDropdown(pagename, "GeProjectList", "ddlProject");
            FillDropdown(pagename, "GetEmployeeList", "ddlEmployee");
        }



        function Save() {
            alert('OK')
            //var data = $(".select").select2('data');
            var IDS = [];

            alert('OK')
            //jQuery.each(data, function (index, item) {
            //    IDS.push(item.id);
            //});
            alert('1')
            var txtFromDate = $("#txtFromDate").val();
            var txtToDate = $("#txtToDate").val();

            
            var chkAccountLevel = $("#chkAccountLevel").val();
            var chkLocation = $("#chkLocation").val();
            var chkSegment = $("#chkSegment").val();
            var chkDepartment = $("#chkDepartment").val();
            var chkProject = $("#chkProject").val();
            var chkEmployee = $("#chkEmployee").val();
            


            var cAccountLevel = 0;            
            var cLocation = 0;
            var cSegment = 0;
            var cDepartment = 0;
            var cProject = 0;
            var cEmployee = 0;
            
            alert('2')
            if ($("#chkAccountLevel").is(":Checked")) {
                cAccountLevel = 1;
            } else { cAccountLevel = 0; }

            if ($("#chkLocation").is(":Checked")) {
                cLocation = 1;
            } else { cLocation = 0; }

            if ($("#chkSegment").is(":Checked")) {
                cSegment = 1;
            } else { cSegment = 0; }

            if ($("#chkDepartment").is(":Checked")) {
                cDepartment = 1;
            } else { cDepartment = 0; }

            if ($("#chkProject").is(":Checked")) {
                cProject = 1;
            } else { cProject = 0; }

            if ($("#chkEmployee").is(":Checked")) {
                cEmployee = 1;
            } else { cEmployee = 0; }

           

            var ddlAccountLevel = $("#ddlAccountLevel :selected").val();           
            var ddlLocation = $("#ddlLocation :selected").val();
            var ddlSegment = $("#ddlSegment :selected").val();
            var ddlDepartment = $("#ddlDepartment :selected").val();
            var ddlProject = $("#ddlProject :selected").val();
            var ddlEmployee = $("#ddlEmployee :selected").val();
            //alert('3')
            urlname = "Save";
            $.ajax({
                type: "POST",
                url: '' + pagename + '/' + urlname + '?id=' + IDS + '&txtFromDate=' + txtFromDate + '&txtToDate=' + txtToDate +
                    '&chkAccountLevel=' + cAccountLevel + '&chkLocation=' + cLocation +
                    '&chkSegment=' + cSegment + '&chkDepartment=' + cDepartment + '&chkProject=' + cProject +
                    '&chkEmployee=' + cEmployee + 
                    '&rdoNet=' + rdoNet + '&rdoGross=' + rdoGross + '&ddlAccountLevel=' + ddlAccountLevel +
                    '&ddlLocation=' + ddlLocation + '&ddlSegment=' + ddlSegment + '&ddlDepartment=' + ddlDepartment +
                    '&ddlProject=' + ddlProject + '&ddlEmployee=' + ddlEmployee,
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    var data = res.d;
                    if (data == "1") {
                        window.open("../Ctrls/ReportViewer.aspx");
                        $("#FormError").css('display', 'none');
                        $("#Error").empty();

                    } else {
                        $("#FormError").css('display', '');
                        $("#Error").empty();
                        $("#Error").text(data);
                    }
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        }
    </script>


    <%-- <div class="radio">
          <label>
            <input type="radio" name="o1" value="">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            Option one is this and that — be sure to include why it's great
          </label>
        </div>
  <div class="radio">
          <label>
            <input type="radio" name="o1" value="" checked>
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            Option two is checked by default
          </label>
        </div>
  <div class="radio disabled">
          <label>
            <input type="radio" name="o1" value="" disabled>
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            Option three is disabled
          </label>
        </div>--%>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h5>Trail Balance</h5>
        </div>
              <div class="panel-body">
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-12 bg-primary">
                            <span>Period</span>
                        </div>
                    </div>

                   

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-2 text-right">
                            <span class="cr">From Date</span>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group date">
                                <input type="text" id="txtFromDate" class="form-control" />
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>

                        <div class="col-sm-2 text-right">
                            <span class="cr">To Date</span>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group date">
                                <input type="text" id="txtToDate" class="form-control" />
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-calendar"></i></span>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="panel panel-success">
                <div class="panel-body">

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-12 bg-success">
                            <span>Type</span>
                        </div>
                    </div>
                  

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-2">
                            <div class="radio">
                                <label>
                                    <input type="radio" id="rdoNet" name="radNet" value="">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    Net
                                </label>
                            </div>

                        </div>

                        <div class="col-sm-2">
                            <div class="radio">
                                <label>
                                    <input type="radio" id="rdoGross" name="radNet" value="1"  />
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    Gross
                                </label>
                            </div>
                        </div>

                    </div>

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-3">
                            <div class="radio">
                                <%--<label>
                                    <input type="radio" name="radLedger" value=""  />
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    Ledger For A/c Code
                                </label>--%>
                            </div>

                        </div>
                        <div class="col-sm-3">
                            <div class="checkbox checkbox-primary">
                                <%--<span class="button-checkbox">
                                    <button type="button" class="" data-color="primary" style="border: 0 #fff">Opening Balance</button>
                                    <input type="checkbox" />
                                </span>--%>

                            </div>

                        </div>

                    </div>


                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-body">

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-12 bg-info">
                            <span>Option</span>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-3 text-right">
                            <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Chart Of Account level</button>
                                <input type="checkbox" id="chkAccountLevel"   />
                            </span>

                        </div>
                        <div class="col-sm-3">
                            <select id="ddlAccountLevel" class="select" style="width: 100%;"></select>
                        </div>

                        <div class="col-sm-3 text-right">
                            <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Location</button>
                                <input type="checkbox" id ="chkLocation" />
                            </span>
                        </div>

                        <div class="col-sm-3">
                            <select id="ddlLocation" class="select" style="width: 100%;"></select>
                        </div>

                    </div>

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-3 text-right">
                            <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Segment</button>
                                <input type="checkbox" id ="chkSegment" />
                            </span>


                        </div>
                        <div class="col-sm-3">
                            <select id="ddlSegment" class="select" style="width: 100%;"></select>
                        </div>


                        <div class="col-sm-3 text-right">
                            <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Department</button>
                                <input type="checkbox" id="chkDepartment" />
                            </span>
                        </div>

                        <div class="col-sm-3">
                            <select id="ddlDepartment" class="select" style="width: 100%;"></select>
                        </div>

                    </div>

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-3 text-right">
                            <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Project</button>
                                <input type="checkbox" id="chkProject"/>
                            </span>


                        </div>
                        <div class="col-sm-3">
                            <select id="ddlProject" class="select" style="width: 100%;"></select>
                        </div>


                        <div class="col-sm-3 text-right">
                            <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Employee</button>
                                <input type="checkbox" id="chkEmployee"  />
                            </span>
                        </div>

                        <div class="col-sm-3">
                            <select id="ddlEmployee" class="select" style="width: 100%;"></select>
                        </div>

                    </div>

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-12">
                            <div class="pull-right">
                                <button type="button" class="btn btn-primary btn-md" onclick="Save()">
                                    <span class="glyphicon glyphicon-search"></span>Show
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</asp:Content>
