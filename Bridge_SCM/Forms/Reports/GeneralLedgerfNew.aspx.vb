﻿Imports System.Web.Script.Services

Partial Public Class GeneralLedgerfNew
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

        End If
    End Sub

    'GetAccountCodeList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetAccountCodeList() As List(Of clsDropdownModel)
        Dim objAccountCode As New BizSoft.Bridge_SCM.GeneralLedger
        Dim dtAccountCode As DataSet = objAccountCode.GetListAccountCode(HttpContext.Current.Session("CompanyID"))

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtAccountCode.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function

    'GetReportList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetAccountCodetList() As List(Of clsDropdownModel)
        Dim ddlList As New List(Of clsDropdownModel)
        Dim id As String = HttpContext.Current.Request.QueryString("id")
        If HttpContext.Current.Request.QueryString.Count > 0 Then
            If id <> "" Then
                Dim objAcLevel As New BizSoft.Bridge_SCM.GeneralLedger
                Dim dtAcLevel As DataSet = objAcLevel.GetAccountLevelList(id, HttpContext.Current.Session("CompanyID"))

                For Each dr As DataRow In dtAcLevel.Tables(0).Rows
                    Dim ddl As New clsDropdownModel
                    ddl.Text = dr("Name").ToString + "??" + dr("ID").ToString
                    ddl.Value = dr("ID").ToString
                    ddlList.Add(ddl)
                Next
            End If
        End If

        Return ddlList
    End Function

    ' Save
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function Save() As String

        Dim IDS As String = HttpContext.Current.Request.QueryString("id")
        Dim id() As String

        Dim ht As New Hashtable


        ' &txtFromDate=' + txtFromDate + '&txtToDate=' + txtToDate +
        '&chkVocType=' + cVocType + '&chkVocStatus=' + cVocStatus + '&chkLocation=' + cLocation +
        '&chkSegment=' + cSegment + '&chkDepartment=' + cDepartment + '&chkProject=' + cProject +
        '&chkEmployee=' + cEmployee + '&chkDebtor=' + cDebtor + '&chkCreditor=' + cCreditor +
        '&radAccountAC=' + radAccountAC + '&ddlAccountType=' + ddlAccountType + '&ddlVocStatus=' + ddlVocStatus +
        '&ddlLocation=' + ddlLocation + '&ddlSegment=' + ddlSegment + '&ddlDepartment=' + ddlDepartment +
        '&ddlProject=' + ddlProject + '&ddlEmployee=' + ddlEmployee + '&ddlDebtor=' + ddlDebtor + 
        '&ddlCreditor=' + ddlCreditor + '&ddlReports=' + ddlReports + '&ddlDetailAccountAC=' + ddlDetailAccountAC

        Dim report As String = HttpContext.Current.Request.QueryString("ddlReports")
        Dim ChkProject As String = HttpContext.Current.Request.QueryString("chkProject")
        Dim FromDate As String = HttpContext.Current.Request.QueryString("txtFromDate")
        Dim ToDate As String = HttpContext.Current.Request.QueryString("txtToDate")
        Dim VoucherType As String = HttpContext.Current.Request.QueryString("ddlAccountType")
        Dim Project As String = HttpContext.Current.Request.QueryString("ddlProject")
        Dim Location As String = HttpContext.Current.Request.QueryString("ddlLocation")
        Dim Employee As String = HttpContext.Current.Request.QueryString("ddlEmployee")
        Dim Debtor As String = HttpContext.Current.Request.QueryString("ddlDebtor")


        ' cmbProject  cmbLocation   cmbEmployee  
        Dim result As String = ""
        Dim mAccountCode As String = ""
        Dim ObjACt As New BizSoft.Bridge_SCM.ReportChartOFAccount
        Dim objReport As New BizSoft.DBManager.clsReport.oReport
        Dim StrList As New Generic.List(Of String)

        Dim oCulture = New System.Globalization.CultureInfo("en-US", True)

        Dim selectionsql As String = ""
        Dim selectionsqlCode As String = ""
        objReport.Formula = ""
        If report = "-1" Then
            Return "Please Select Any Reports"
        End If

        ' cmbProject  cmbLocation   cmbEmployee  
        If report = "RptProjectGL.rpt" Then
            If IDS = "" Then
            Else
                id = IDS.Split(",")
            End If
        Else
            If IDS = "" Then
                Return "Please Select ChartofAccount"

            Else
                id = IDS.Split(",")
            End If
        End If

        If report = "RptProjectGL.rpt" Then
            If ChkProject = "1" Then
                If Project = "-1" Then
                    Return "Please Select Project Code"
                End If
            Else
                Return "Please Select Project Code"
            End If
        End If

        objReport.strFileName = report
        HttpContext.Current.Session("ReportFileName") = report
        If report = "RptGLNew.rpt" Then
            ht.Add("FromDate", FromDate)
            ht.Add("ToDate", ToDate)
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))
            ht.Add("User", HttpContext.Current.Session("UserID"))
            ht.Add("VoucherType", VoucherType)
            ht.Add("LocationID", HttpContext.Current.Session("LocationID"))
            ht.Add("AccountCode", mAccountCode)

            selectionsqlCode = SetCritriaCode(id)
            selectionsql = SetCritria()
            selectionsql = selectionsqlCode & " " & selectionsql

            objReport.Formula = selectionsql
            objReport.rptCreteria = selectionsql
            'Return "1"

        ElseIf report = "RptGlDetail.rpt" Then
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))
            ht.Add("User", HttpContext.Current.Session("UserID"))
            ht.Add("FromDate", FromDate)
            ht.Add("ToDate", ToDate)
            ht.Add("AccountCode", ObjACt.StripID(Trim(id(0))))
            ht.Add("Cri", ObjACt.StripID(Trim(id(0))))
            'Return "1"

        ElseIf report = "RptSubGLrpt.rpt" Then
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))
            ht.Add("User", HttpContext.Current.Session("UserID"))
            ht.Add("FromDate", FromDate)
            ht.Add("ToDate", ToDate)
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("AccountCode", ObjACt.StripID(Trim(id(0))))
            'Return "1"

        ElseIf report = "RptProjectGL.rpt" Then
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))
            ht.Add("User", HttpContext.Current.Session("UserID"))
            ht.Add("FromDate", FromDate)
            ht.Add("ToDate", ToDate)
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            If IDS = "" Then
                ht.Add("AccountCode", "0")
                ht.Add("Cri", "0")
            Else
                ht.Add("AccountCode", ObjACt.StripID(Trim(id(0))))
                ht.Add("Cri", ObjACt.StripID(Trim(id(0))))
            End If

            ht.Add("ProjectID", Project)
            'result = "1"

        ElseIf report = "RptProjectGLAccountCodeWise.rpt" Then
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))
            ht.Add("User", HttpContext.Current.Session("UserID"))
            ht.Add("FromDate", FromDate)
            ht.Add("ToDate", ToDate)
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))

            ht.Add("Cri", ObjACt.StripID(Trim(id(0))))
            ht.Add("ProjectID", Project)
            'Return "1"

        ElseIf report = "RptCash-BankFlow_LocationWise.rpt" Then
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))
            ht.Add("FromDate", FromDate)
            ht.Add("ToDate", ToDate)
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("AccountCode", ObjACt.StripID(Trim(id(0))))
            ht.Add("LocationID", HttpContext.Current.Session("LocationID"))
            'Return "1"

        ElseIf report = "RptCash-BankFlowSummary.rpt" Then
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))
            ht.Add("FromDate", FromDate)
            ht.Add("ToDate", ToDate)
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("AccountCode", ObjACt.StripID(Trim(id(0))))
            ht.Add("LocationID", HttpContext.Current.Session("LocationID"))
            'Return "1"

        ElseIf report = "RptSubGLrpt" Then
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))
            ht.Add("FromDate", FromDate)
            ht.Add("ToDate", ToDate)
            ht.Add("User", HttpContext.Current.Session("UserID"))
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("ProjectID", Project)
            ht.Add("AccountCode", ObjACt.StripID(Trim(id(0))))
            ht.Add("Cri", ObjACt.StripID(Trim(id(0))))
            'Return "1"


        ElseIf report = "Projects.rpt" Then
            ht.Add("ToDate", ToDate)
            ht.Add("User", HttpContext.Current.Session("UserID"))
            ht.Add("LocationID", HttpContext.Current.Session("LocationID"))
            ht.Add("LocationName", Location)
            'Return "1"

        ElseIf report = "RPTEmployActivityWithnullReocrd.rpt" Then

            ht.Add("FromDate", FromDate)
            ht.Add("ToDate", ToDate)
            ht.Add("Empid", ObjACt.StripID(Employee))
            ht.Add("AccountCode", ObjACt.StripID(Trim(id(0))))
            'Return "1"

        ElseIf report = "rptMonthlyCompration_Net.rpt" Then
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))
            ht.Add("FromDate", FromDate)
            ht.Add("ToDate", ToDate & " 11:59:59 PM ")
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("LocationID", HttpContext.Current.Session("LocationID"))
            'Return "1"

        ElseIf report = "RtpComprationCodeWise.rpt" Then
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))
            ht.Add("FromDate", FromDate)
            ht.Add("ToDate", ToDate & " 11:59:59 PM ")
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("LocationID", HttpContext.Current.Session("LocationID"))
            ht.Add("AccountCode", ObjACt.StripID(Trim(id(0))))
            'Return "1"

        ElseIf report = "rptMonthlyCompration_Both.rpt" Then
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("FromDate", FromDate)
            ht.Add("ToDate", ToDate & " 11:59:59 PM ")
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))
            ht.Add("LocationID", HttpContext.Current.Session("LocationID"))
            'Return "1"

        ElseIf report = "RptPostedCheque.rpt" Then
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("fromDate", FromDate)
            ht.Add("ToDate", ToDate & " 11:59:59 PM ")
            'Return "1"

        ElseIf report = "RptPostedChequeBankWise.rpt" Then
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("fromDate", FromDate)
            ht.Add("ToDate", ToDate & " 11:59:59 PM ")
            ' Return "1"
        ElseIf report = "RptPostedChequeBankWiseSummary.rpt" Then
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("fromDate", FromDate)
            ht.Add("ToDate", ToDate & " 11:59:59 PM ")
            ' Return "1"
        ElseIf report = "RptPostedChequeMonthWiseBankWiseSummary.rpt" Then
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("fromDate", FromDate)
            ht.Add("ToDate", ToDate & " 11:59:59 PM ")

        End If
        HttpContext.Current.Session("ogsReport") = objReport

        objReport.ht = ht
        objReport.StrList = StrList
        Return "1"

    End Function




    Public Shared Function SetCritriaCode(ByVal list() As String) As String
        'Dim list As New ArrayList

        Dim FromDate As String = HttpContext.Current.Request.QueryString("txtFromDate")
        Dim ToDate As String = HttpContext.Current.Request.QueryString("txtToDate")


        Dim i As Integer
        Dim mAccountCode As String
        Dim ObjACt As New BizSoft.Bridge_SCM.ReportChartOFAccount

        Dim selectionsqlCode As String = ""
        Dim AndFlag As Boolean = False
        Dim T As Integer


        selectionsqlCode = " {VNGL.VoucherDate} >=date(" & Year(FromDate) & "," & Month(FromDate) & ", " & Day(FromDate) & ")  AND  {VNGL.VoucherDate} <=date(" & Year(ToDate) & "," & Month(ToDate) & ", " & Day(ToDate) & ") AND"

        T = list.Count
        For i = 0 To T - 1
            mAccountCode = ObjACt.StripID(Trim(list(i)))
            If AndFlag = False Then
                selectionsqlCode = selectionsqlCode & " ({VNGL.AccountCode} = '" & mAccountCode & "'"
                AndFlag = True
            Else
                selectionsqlCode = selectionsqlCode & "OR {VNGL.AccountCode} = '" & mAccountCode & "'"
            End If
        Next i
        selectionsqlCode = selectionsqlCode & ")"

        Return selectionsqlCode
    End Function

    Public Shared Function SetCritria() As String


        Dim ChkLocation As String = HttpContext.Current.Request.QueryString("chkLocation")
        Dim Location As String = HttpContext.Current.Request.QueryString("ddlLocation")

        Dim ChkSegment As String = HttpContext.Current.Request.QueryString("chkSegment")
        Dim Segment As String = HttpContext.Current.Request.QueryString("ddlSegment")
        Dim ChkDepartment As String = HttpContext.Current.Request.QueryString("chkDepartment")
        Dim Department As String = HttpContext.Current.Request.QueryString("ddlDepartment")
        Dim ChkProject As String = HttpContext.Current.Request.QueryString("chkProject")
        Dim Project As String = HttpContext.Current.Request.QueryString("ddlProject")
        Dim ChkEmployee As String = HttpContext.Current.Request.QueryString("chkEmployee")
        Dim Employee As String = HttpContext.Current.Request.QueryString("ddlEmployee")
        Dim ChkVoctype As String = HttpContext.Current.Request.QueryString("chkVocType")
        Dim Voctype As String = HttpContext.Current.Request.QueryString("ddlAccountType")
        Dim ChkDebtor As String = HttpContext.Current.Request.QueryString("chkDebtor")
        Dim Debtor As String = HttpContext.Current.Request.QueryString("ddlDebtor")
        Dim ChkCreditor As String = HttpContext.Current.Request.QueryString("chkCreditor")
        Dim Creditor As String = HttpContext.Current.Request.QueryString("ddlCreditor")


        Dim selectionsql As String = ""
        Dim AndFlag As Boolean = True

        If ChkLocation = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {VNGL.LocationID} = " & Location
            Else
                selectionsql = "{VNGL.LocationID} = " & Location
                AndFlag = True
            End If
            AndFlag = True
        End If

        If ChkSegment = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {VNGL.SegmentID} = " & Segment
            Else
                selectionsql = "{VNGL.SegmentID} = " & Segment
                AndFlag = True
            End If
            AndFlag = True
        End If

        If ChkDepartment = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {VNGL.DepartmentID} = '" & Trim(Department) & "'"
            Else

                selectionsql = " {VNGL.DepartmentID}= '" & Trim(Department) & "'"
                AndFlag = True
            End If
        End If

        If ChkProject = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {VNGL.ProjectId} = '" & Trim(Project) & "'"
            Else
                selectionsql = " {VNGL.ProjectId} = '" & Trim(Project) & "'"
                AndFlag = True
            End If
        End If


        If ChkEmployee = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {VNGL.EmployeeID} = '" & Trim(Employee) & "'"
            Else
                selectionsql = " {VNGL.EmployeeID} = '" & Trim(Employee) & "'"
                AndFlag = True
            End If
        End If
        If ChkVoctype = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {VNGL.VoucherTypeID} = '" & Voctype & "'"
            Else
                selectionsql = "{VNGL.VoucherTypeID} = '" & Voctype.Trim & "'"
                AndFlag = True
            End If
            AndFlag = True
        End If

        If ChkDebtor = "1" Then

            If AndFlag Then
                selectionsql = selectionsql & " AND {VNGL.PayToOrReceivedFrom} = '" & Trim(Debtor) & "'"
            Else
                selectionsql = "{VNGL.Cust} = '" & Trim(Debtor) & "'"
                AndFlag = True
            End If
        End If

        If ChkCreditor = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {VNGL.Sup} = '" & Trim(Creditor) & "'"
            Else
                selectionsql = "{VNGL.Sup} = '" & Trim(Creditor) & "'"
                AndFlag = True
            End If
        End If

        Return selectionsql

    End Function



End Class