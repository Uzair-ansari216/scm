﻿Partial Public Class RPTChartOfAccountNew
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

        End If
    End Sub

  
    ' GetAccountGroupList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetAccountGroupList() As List(Of clsDropdownModel)
        Dim objLoc As New BizSoft.Bridge_SCM.ChartOfAccount
        Dim dtGroup As DataSet = objLoc.GetAccountGroupsList()

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtGroup.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function

    ' GetAccountTypeList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetAccountTypeList() As List(Of clsDropdownModel)
        Dim objLoc As New BizSoft.Bridge_SCM.ReportChartOFAccount
        Dim dtType As DataSet = objLoc.GetAccountTypeList

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtType.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function

    ' Save
    <System.Web.Services.WebMethod>
    Public Shared Function Save() As String
        Dim ht As New Hashtable
        Dim objReport As New BizSoft.DBManager.clsReport.oReport
        Dim ID As String = ""
        Dim chkTV As String = HttpContext.Current.Request.QueryString("chkTV")
        Dim oCulture = New System.Globalization.CultureInfo("en-US", True)

        If chkTV = "1" Then
            objReport.strFileName = "rptCOA.rpt"
        Else
            objReport.strFileName = "rptCOALevels.rpt"
        End If

        If chkTV = "1" Then
            ht.Add("Company", HttpContext.Current.Session("CompanyID"))
            ht.Add("User", HttpContext.Current.Session("UserID"))
        Else
            ht.Add("Company", HttpContext.Current.Session("CompanyID"))
            ht.Add("User", HttpContext.Current.Session("UserID"))
            '    ht.Add("YearTitle", HttpContext.Current.Session("yearTitle"))
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))

        End If

        objReport.ht = ht
        ID = "1"
        HttpContext.Current.Session("ogsReport") = objReport
        Return ID

    End Function


End Class