﻿Partial Public Class VendorAgeingNew
    Inherits System.Web.UI.Page

    ' DROPDOWN METHOD
    Public Shared Function FillDropdown(ByVal ds As DataSet) As List(Of clsDropdownModel)
        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In ds.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      
    End Sub

    ' GeSupplierList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GeSupplierList() As List(Of clsDropdownModel)
        Dim objSupplier As New BizSoft.Bridge_SCM.Supplier()
        Dim dtSupplier As DataSet = objSupplier.GetSupplier(HttpContext.Current.Session("CompanyID"))

        Return FillDropdown(dtSupplier)
    End Function

    ' GetSupplierTypeList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetSupplierTypeList() As List(Of clsDropdownModel)
        Dim objSupplierType As New BizSoft.Bridge_SCM.Supplier()
        Dim dtSupplierType As DataSet = objSupplierType.GetSupplierType

        Return FillDropdown(dtSupplierType)
    End Function

    ' GetReportsList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetReportsList() As List(Of clsDropdownModel)
        Dim objReport As New BizSoft.Bridge_SCM.GeneralLedger
        Dim frmName As String = "MDI_VendorAgeing"
        Dim dtReport As DataSet = objReport.GetReportDetails(frmName)

        Return FillDropdown(dtReport)
    End Function

    ' GetLocationList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetLocationList() As List(Of clsDropdownModel)
        Dim objlocation As New BizSoft.Bridge_SCM.Supplier()
        Dim dtlocation As DataSet = objlocation.GetListLocation(HttpContext.Current.Session("CompanyID"))

        Return FillDropdown(dtlocation)
    End Function

    ' Save
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function Save()

        ' txtFromDate=' + txtFromDate + '&txtToDate=' + txtToDate + '&ddlSupllier=' + ddlSupllier +
        '&ddlSupplierType=' + ddlSupplierType + '&ddlReports=' + ddlReports + '&ddlLocation=' + ddlLocation +
        '&txtAgeingDays=' + txtAgeingDays + '&txtAgeingDaysInterval=' + txtAgeingDaysInterval,
        Dim txtFromDate As String = HttpContext.Current.Request.QueryString("txtFromDate")
        Dim txtToDate As String = HttpContext.Current.Request.QueryString("txtToDate")
        Dim ddlSupllier As String = HttpContext.Current.Request.QueryString("ddlSupllier")
        Dim ddlSupplierType As String = HttpContext.Current.Request.QueryString("ddlSupplierType")
        Dim ddlReports As String = HttpContext.Current.Request.QueryString("ddlReports")
        Dim ddlLocation As String = HttpContext.Current.Request.QueryString("ddlLocation")
        Dim txtAgeingDays As String = HttpContext.Current.Request.QueryString("txtAgeingDays")
        Dim txtAgeingDaysInterval As String = HttpContext.Current.Request.QueryString("txtAgeingDaysInterval")

        Dim txtInterval1 As String = HttpContext.Current.Request.QueryString("txtInterval1")
        Dim txtInterval2 As String = HttpContext.Current.Request.QueryString("txtInterval2")
        Dim txtInterval3 As String = HttpContext.Current.Request.QueryString("txtInterval3")
        Dim txtInterval4 As String = HttpContext.Current.Request.QueryString("txtInterval4")

        Dim ht As New Hashtable
        Dim mAccountCode As String = ""
        Dim ObjACt As New BizSoft.Bridge_SCM.ReportChartOFAccount
        Dim objReport As New BizSoft.DBManager.clsReport.oReport
        Dim oCulture = New System.Globalization.CultureInfo("en-US", True)
        Dim ID As String = ""
        objReport.strFileName = ddlReports ' value

        If ddlReports = "RPTVenderAg.rpt" Then ' value
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("SupplierTypeID", ddlSupplierType)
            ht.Add("ToDate", txtToDate & " 11:59:59 PM ")
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))


        ElseIf ddlReports = "Supplier Ageing (Day Interval)" Then ' text
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("SupplierTypeID", ddlSupplierType)
            ht.Add("ToDate", txtToDate & " 11:59:59 PM ")
            ht.Add("DR1", Val(txtAgeingDaysInterval))
            ht.Add("DR2", Val(txtAgeingDaysInterval) * 2)
            ht.Add("DR3", Val(txtAgeingDaysInterval) * 3)
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))

        ElseIf ddlReports = "Supplier Ageing (With Op Days)" Then ' text
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("SupplierTypeID", ddlSupplierType)
            ht.Add("ToDate", txtToDate & " 11:59:59 PM ")
            ht.Add("DR1", Val(txtInterval1))
            ht.Add("DR2", Val(txtInterval2))
            ht.Add("DR3", Val(txtInterval3))
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))

        ElseIf ddlReports = "RPTVenderAgOpDaysWOPDC.rpt" Then ' value
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("SupplierTypeID", ddlSupplierType)
            ht.Add("ToDate", txtToDate & " 11:59:59 PM ")
            ht.Add("DR1", Val(txtInterval1))
            ht.Add("DR2", Val(txtInterval2))
            ht.Add("DR3", Val(txtInterval3))
            ht.Add("DR4", Val(txtInterval4))
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))

        ElseIf ddlReports = "RPTVenderAgLocationW.rpt" Then ' value
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("SupplierTypeID", ddlSupplierType)
            ht.Add("ToDate", txtToDate & " 11:59:59 PM ")
            ht.Add("DR1", Val(txtInterval1))
            ht.Add("DR2", Val(txtInterval2))
            ht.Add("DR3", Val(txtInterval3))
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))
            ht.Add("LocationID", ddlLocation)

        ElseIf ddlReports = "RPTVenderAgLocationWSummary.rpt" Then ' value
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("SupplierTypeID", ddlSupplierType)
            ht.Add("ToDate", txtToDate & " 11:59:59 PM ")
            ht.Add("DR1", Val(txtInterval1))
            ht.Add("DR2", Val(txtInterval2))
            ht.Add("DR3", Val(txtInterval3))
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))
            ht.Add("LocationID", ddlLocation)

        ElseIf ddlReports = "RPTVenderAgGroupSummary.rpt" Then ' value
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("SupplierTypeID", ddlSupplierType)
            ht.Add("ToDate", txtToDate & " 11:59:59 PM ")
            ht.Add("DR1", Val(txtInterval1))
            ht.Add("DR2", Val(txtInterval2))
            ht.Add("DR3", Val(txtInterval3))
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))
            ht.Add("LocationID", ddlLocation)

        ElseIf ddlReports = "RPTVenderAgOpDaysE.rpt" Then ' value
            GenrateBalance(txtToDate)
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("SupplierTypeID", ddlSupplierType)
            ht.Add("ToDate", txtToDate & " 11:59:59 PM ")
            ht.Add("SupplierID", ddlSupplierType)
            ht.Add("DR1", Val(txtInterval1))
            ht.Add("DR2", Val(txtInterval2))
            ht.Add("DR3", Val(txtInterval3))
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))
            ht.Add("AGDays", Val(txtAgeingDays))

        ElseIf ddlReports = "RPTVenderAgOpDaysETypeWiseSummary.rpt" Then ' value
            GenrateBalance(txtToDate)
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("ToDate", txtToDate & " 11:59:59 PM ")
            ht.Add("SupplierTypeID", ddlSupplierType)
            ht.Add("SupplierID", ddlSupllier)
            ht.Add("DR1", Val(txtInterval1))
            ht.Add("DR2", Val(txtInterval2))
            ht.Add("DR3", Val(txtInterval3))
            ht.Add("DR4", Val(txtInterval4))
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyName"))
            ht.Add("AGDays", Val(txtAgeingDays))

        ElseIf ddlReports = "RPTVenderAgOpDaysETypeWise.rpt" Then ' value
            GenrateBalance(txtToDate)
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("SupplierTypeID", ddlSupplierType)
            ht.Add("ToDate", txtToDate & " 11:59:59 PM ")
            ht.Add("DR1", Val(txtInterval1))
            ht.Add("DR2", Val(txtInterval2))
            ht.Add("DR3", Val(txtInterval3))
            ht.Add("DR4", Val(txtInterval4))
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))
            ht.Add("AGDays", Val(txtAgeingDays))

        ElseIf ddlReports = "RptAgAsonByOffSet.rpt" Then ' value
            GenrateBalance(txtToDate)
            ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))
            ht.Add("SupplierTypeID", ddlSupplierType)
            ht.Add("ToDate", txtToDate & " 11:59:59 PM ")
            ht.Add("DR1", Val(txtInterval1))
            ht.Add("DR2", Val(txtInterval2))
            ht.Add("DR3", Val(txtInterval3))
            ht.Add("CompanyName", HttpContext.Current.Session("CompanyID"))
            ht.Add("AGDays", Val(txtAgeingDays))

        ElseIf ddlReports = "RptAg.rpt" Then ' value
            ht.Add("FromDate", txtFromDate & " 11:59:59 PM ")
            ht.Add("ToDate", txtToDate & " 11:59:59 PM ")

            ht.Add("CustomerID", Val(txtInterval1))
            ht.Add("AccountCode", Val(txtInterval2))
            ht.Add("FK_Employee", Val(txtInterval3))
            ht.Add("LocationID", HttpContext.Current.Session("CompanyID"))
            ht.Add("AGDays", Val(txtAgeingDays))


        End If
        objReport.ht = ht
        ID = "1"
        HttpContext.Current.Session("ogsReport") = objReport
        Return ID
    End Function

    'GenrateBalance
    Public Shared Function GenrateBalance(ByVal ToDate As String) As DataSet
        Dim StrQry As String
        StrQry = "GUIRPT_AccountCodeBalance '" & ToDate & "'," & HttpContext.Current.Session("CompanyID") & ", " & BizSoft.Utilities.mUserId & ""
        Return BizSoft.DBManager.GetDataSet(StrQry)
    End Function

End Class