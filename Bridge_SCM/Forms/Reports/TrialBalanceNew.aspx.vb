﻿Partial Public Class TrialBalanceNew
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub

    ' GeSegmentList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GeSegmentList() As List(Of clsDropdownModel)
        Dim objSegment As New BizSoft.Bridge_SCM.GeneralLedger
        Dim dtSegment As DataSet = objSegment.GetListSegment(HttpContext.Current.Session("CompanyID"))

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtSegment.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function

    ' GeProjectList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GeProjectList() As List(Of clsDropdownModel)
        Dim objProject As New BizSoft.Bridge_SCM.GeneralLedger
        Dim dtProject As DataSet = objProject.GetListproject(HttpContext.Current.Session("CompanyID"))

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtProject.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function

    ' GetLocationList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetLocationList() As List(Of clsDropdownModel)
        Dim objlocation As New BizSoft.Bridge_SCM.Supplier()
        Dim dtlocation As DataSet = objlocation.GetListLocation(HttpContext.Current.Session("CompanyID"))

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtlocation.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function


    ' GetDepartmentList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetDepartmentList() As List(Of clsDropdownModel)
        Dim objDepartment As New BizSoft.Bridge_SCM.Department
        Dim dtDepartment As DataSet = objDepartment.GetList(HttpContext.Current.Session("CompanyID"))

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtDepartment.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function


    ' GetEmployeeList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetEmployeeList() As List(Of clsDropdownModel)
        Dim objEmployee As New BizSoft.Bridge_SCM.Employee()
        Dim dtEmployee As DataSet = objEmployee.GetListStatus(HttpContext.Current.Session("CompanyID"))

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtEmployee.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function

    ' Save
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function Save() As String

        Dim IDS As String = HttpContext.Current.Request.QueryString("id")
        Dim id() As String

        Dim ht As New Hashtable



        Dim FromDate As String = HttpContext.Current.Request.QueryString("txtFromDate")
        Dim ToDate As String = HttpContext.Current.Request.QueryString("txtToDate")

        Dim chkAccountLevel As String = HttpContext.Current.Request.QueryString("chkAccountLevel")
        Dim chkLocation As String = HttpContext.Current.Request.QueryString("chkLocation")
        Dim chkSegment As String = HttpContext.Current.Request.QueryString("chkSegment")
        Dim chkEmployee As String = HttpContext.Current.Request.QueryString("chkEmployee")
        Dim chkProject As String = HttpContext.Current.Request.QueryString("chkProject")
        Dim chkDepartment As String = HttpContext.Current.Request.QueryString("chkDepartment")

        Dim Department As String = HttpContext.Current.Request.QueryString("ddlDepartment")
        Dim Project As String = HttpContext.Current.Request.QueryString("ddlProject")
        Dim Employee As String = HttpContext.Current.Request.QueryString("ddlEmployee")
        Dim AccountLevel As String = HttpContext.Current.Request.QueryString("ddlAccountLevel")
        Dim Location As String = HttpContext.Current.Request.QueryString("ddlLocation")
        Dim Segment As String = HttpContext.Current.Request.QueryString("ddlSegment")

        Dim NetOpt As String = HttpContext.Current.Request.QueryString("rdoNet")
        'Dim NetGross As String = HttpContext.Current.Request.QueryString("rdoGross")
        Dim NetGross As String = 1
        ' cmbProject  cmbLocation   cmbEmployee  
        Dim result As String = ""
        Dim mAccountCode As String = ""
        Dim ObjACt As New BizSoft.Bridge_SCM.ReportChartOFAccount
        Dim objReport As New BizSoft.DBManager.clsReport.oReport
        Dim StrList As New Generic.List(Of String)

        Dim oCulture = New System.Globalization.CultureInfo("en-US", True)

        Dim selectionsql As String = ""
        Dim selectionsqlCode As String = ""
        Dim report As String = ""
        Dim chkG As String = "0"

        If NetGross = 1 Then
            chkG = 1
        Else
            chkG = 0
        End If

        Dim ds As DataSet = ObjACt.GetTrailBalanceData(chkG, HttpContext.Current.Session("CompanyID"), FromDate, ToDate)
        ObjACt.DeleteTrailBalance()
        'For i =1 In ds.Tables(0).Rows.Count > 0 Then
        Dim dr As DataRow
        For Each dr In ds.Tables(0).Rows
            Dim ClosingBalance As Double = 0
            Dim ODebit As Double
            Dim OCredit As Double
            Dim Debit As Double
            Dim Credit As Double
            Dim CDebit As Double
            Dim CCredit As Double
            Dim SAccount As String = ""
            ODebit = IIf(dr("op") > 0, dr("op"), 0)
            OCredit = IIf(dr("op") < 0, dr("op"), 0) * -1

            If NetGross = 1 Then
                Debit = dr("Debit")
                Credit = dr("Credit")
            Else
                Debit = IIf(dr("activity") > 0, dr("activity"), 0)
                Credit = IIf(dr("activity") < 0, dr("activity"), 0) * -1
            End If

            ClosingBalance = (ODebit + Debit) - (OCredit + Credit)

            CDebit = IIf(ClosingBalance > 0, ClosingBalance, 0)
            CCredit = IIf(ClosingBalance < 0, ClosingBalance, 0) * -1
            SAccount = 0

            ObjACt.InsertTrailBalance(HttpContext.Current.Session("UserID"), HttpContext.Current.Session("CompanyID"), dr("AccountCode"), "-", ODebit.ToString(), OCredit.ToString(), Debit.ToString(), Credit.ToString(), CDebit.ToString(), CCredit.ToString(), SAccount)

        Next

        

        If NetGross = 1 Then
            report = "SimpleTrialWithOpDrillDown.rpt"
        Else
            report = "SimpleTrialDrillDownClosing.rpt"
        End If

        objReport.Formula = ""

        objReport.strFileName = report
        HttpContext.Current.Session("ReportFileName") = report
        ' If report = "RptGLNew.rpt" Then
        ht.Add("DateF", FromDate)
        ht.Add("DateT", ToDate)
        ht.Add("rptCriteria", Location)
        ht.Add("User", HttpContext.Current.Session("UserID"))

        'selectionsqlCode = SetCritriaCode(id)
        'selectionsql = SetCritria()
        'selectionsql = selectionsqlCode & " " & selectionsql

        'objReport.Formula = selectionsql
        'objReport.rptCreteria = selectionsql
        'Return "1"      
        'End If
        HttpContext.Current.Session("ogsReport") = objReport
        objReport.ht = ht
        objReport.StrList = StrList
        Return "1"

    End Function




    Public Shared Function SetCritriaCode(ByVal list() As String) As String
        'Dim list As New ArrayList

        Dim FromDate As String = HttpContext.Current.Request.QueryString("txtFromDate")
        Dim ToDate As String = HttpContext.Current.Request.QueryString("txtToDate")

        Dim Gross As String = HttpContext.Current.Request.QueryString("rdoGross")
        Dim Net As String = HttpContext.Current.Request.QueryString("radNet")

        Dim i As Integer
        Dim mAccountCode As String
        Dim ObjACt As New BizSoft.Bridge_SCM.ReportChartOFAccount

        Dim selectionsqlCode As String = ""
        Dim AndFlag As Boolean = False
        Dim T As Integer


        selectionsqlCode = " {VNGL.VoucherDate} >=date(" & Year(FromDate) & "," & Month(FromDate) & ", " & Day(FromDate) & ")  AND  {VNGL.VoucherDate} <=date(" & Year(ToDate) & "," & Month(ToDate) & ", " & Day(ToDate) & ") AND"

        T = list.Count
        For i = 0 To T - 1
            mAccountCode = ObjACt.StripID(Trim(list(i)))
            If AndFlag = False Then
                selectionsqlCode = selectionsqlCode & " ({VNGL.AccountCode} = '" & mAccountCode & "'"
                AndFlag = True
            Else
                selectionsqlCode = selectionsqlCode & "OR {VNGL.AccountCode} = '" & mAccountCode & "'"
            End If
        Next i
        selectionsqlCode = selectionsqlCode & ")"

        Return selectionsqlCode
    End Function

    Public Shared Function SetCritria() As String


        Dim ChkLocation As String = HttpContext.Current.Request.QueryString("chkLocation")
        Dim Location As String = HttpContext.Current.Request.QueryString("ddlLocation")

        Dim ChkSegment As String = HttpContext.Current.Request.QueryString("chkSegment")
        Dim Segment As String = HttpContext.Current.Request.QueryString("ddlSegment")
        Dim ChkDepartment As String = HttpContext.Current.Request.QueryString("chkDepartment")
        Dim Department As String = HttpContext.Current.Request.QueryString("ddlDepartment")
        Dim ChkProject As String = HttpContext.Current.Request.QueryString("chkProject")
        Dim Project As String = HttpContext.Current.Request.QueryString("ddlProject")
        Dim ChkEmployee As String = HttpContext.Current.Request.QueryString("chkEmployee")
        Dim Employee As String = HttpContext.Current.Request.QueryString("ddlEmployee")
        Dim ChkVoctype As String = HttpContext.Current.Request.QueryString("chkVocType")
        Dim Voctype As String = HttpContext.Current.Request.QueryString("ddlAccountType")
        Dim ChkDebtor As String = HttpContext.Current.Request.QueryString("chkDebtor")
        Dim Debtor As String = HttpContext.Current.Request.QueryString("ddlDebtor")
        Dim ChkCreditor As String = HttpContext.Current.Request.QueryString("chkCreditor")
        Dim Creditor As String = HttpContext.Current.Request.QueryString("ddlCreditor")

        Dim selectionsql As String = ""
        Dim AndFlag As Boolean = True

        If ChkLocation = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {VNGL.LocationID} = " & Location
            Else
                selectionsql = "{VNGL.LocationID} = " & Location
                AndFlag = True
            End If
            AndFlag = True
        End If

        If ChkSegment = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {VNGL.SegmentID} = " & Segment
            Else
                selectionsql = "{VNGL.SegmentID} = " & Segment
                AndFlag = True
            End If
            AndFlag = True
        End If

        If ChkDepartment = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {VNGL.DepartmentID} = '" & Trim(Department) & "'"
            Else

                selectionsql = " {VNGL.DepartmentID}= '" & Trim(Department) & "'"
                AndFlag = True
            End If
        End If

        If ChkProject = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {VNGL.ProjectId} = '" & Trim(Project) & "'"
            Else
                selectionsql = " {VNGL.ProjectId} = '" & Trim(Project) & "'"
                AndFlag = True
            End If
        End If


        If ChkEmployee = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {VNGL.EmployeeID} = '" & Trim(Employee) & "'"
            Else
                selectionsql = " {VNGL.EmployeeID} = '" & Trim(Employee) & "'"
                AndFlag = True
            End If
        End If
        If ChkVoctype = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {VNGL.VoucherTypeID} = '" & Voctype & "'"
            Else
                selectionsql = "{VNGL.VoucherTypeID} = '" & Voctype.Trim & "'"
                AndFlag = True
            End If
            AndFlag = True
        End If

        If ChkDebtor = "1" Then

            If AndFlag Then
                selectionsql = selectionsql & " AND {VNGL.PayToOrReceivedFrom} = '" & Trim(Debtor) & "'"
            Else
                selectionsql = "{VNGL.Cust} = '" & Trim(Debtor) & "'"
                AndFlag = True
            End If
        End If

        If ChkCreditor = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {VNGL.Sup} = '" & Trim(Creditor) & "'"
            Else
                selectionsql = "{VNGL.Sup} = '" & Trim(Creditor) & "'"
                AndFlag = True
            End If
        End If

        Return selectionsql

    End Function

End Class