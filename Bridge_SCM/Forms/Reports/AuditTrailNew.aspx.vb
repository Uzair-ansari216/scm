﻿Partial Public Class AuditTrailNew
    Inherits System.Web.UI.Page


    ' GetLocationList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetLocationList() As List(Of clsDropdownModel)
        Dim objlocation As New BizSoft.Bridge_SCM.Supplier()
        Dim dtlocation As DataSet = objlocation.GetListLocation(HttpContext.Current.Session("CompanyID"))

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtlocation.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function



    

    ' GetDepartmentList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetDepartmentList() As List(Of clsDropdownModel)
        Dim objDepartment As New BizSoft.Bridge_SCM.Department
        Dim dtDepartment As DataSet = objDepartment.GetList(HttpContext.Current.Session("CompanyID"))

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtDepartment.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function

    ' GetEmployeeList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetEmployeeList() As List(Of clsDropdownModel)
        Dim objEmployee As New BizSoft.Bridge_SCM.Employee()
        Dim dtEmployee As DataSet = objEmployee.GetListStatus(HttpContext.Current.Session("CompanyID"))

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtEmployee.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function

    ' GeSegmentList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GeSegmentList() As List(Of clsDropdownModel)
        Dim objSegment As New BizSoft.Bridge_SCM.GeneralLedger
        Dim dtSegment As DataSet = objSegment.GetListSegment(HttpContext.Current.Session("CompanyID"))

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtSegment.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function

    ' GeProjectList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GeProjectList() As List(Of clsDropdownModel)
        Dim objProject As New BizSoft.Bridge_SCM.GeneralLedger
        Dim dtProject As DataSet = objProject.GetListproject(HttpContext.Current.Session("CompanyID"))

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtProject.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function

    ' GeVoucherTypeList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GeVoucherTypeList() As List(Of clsDropdownModel)
        Dim objvoucherType As New BizSoft.Bridge_SCM.GeneralLedger
        Dim dtvoucherType As DataSet = objvoucherType.GetListVoucherType

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtvoucherType.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function

    ' GeReportList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GeReportList() As List(Of clsDropdownModel)
        Dim objReport As New BizSoft.Bridge_SCM.GeneralLedger
        Dim frmName As String = "mnuREPORTS_AuditTrail"
        Dim dtReport As DataSet = objReport.GetReportDetails(frmName)

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtReport.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function


    '' GeReportList
    '<System.Web.Services.WebMethod(EnableSession:=True)>
    'Public Shared Function GeReportList() As List(Of clsDropdownModel)
    '    Dim objReport As New BizSoft.Bridge_RS.GeneralLedger
    '    Dim frmName As String = "mnuREPORTS_AuditTrail"
    '    Dim dtReport As DataSet = objReport.GetReportDetails(frmName)

    '    Dim ddlList As New List(Of clsDropdownModel)

    '    For Each dr As DataRow In dtReport.Tables(0).Rows
    '        Dim ddl As New clsDropdownModel
    '        ddl.Text = dr("Name").ToString
    '        ddl.Value = dr("ID").ToString
    '        ddlList.Add(ddl)
    '    Next
    '    Return ddlList
    'End Function

    '<System.Web.Services.WebMethod>
    'Public Shared Function GetLocationList() As String

    '    Dim result As String = ""
    '    Dim objlocation As New BizSoft.Bridge_RS.Supplier()
    '    Dim dtlocation As DataSet = objlocation.GetListLocation

    '    result = "<option role='option' value='-1'>--Select--</option>"

    '    For Each dataRow As DataRow In dtlocation.Tables(0).Rows
    '        result = result + "<option role='option' value=" + dataRow("ID").ToString() + " > " + dataRow("Name").ToString() + " </option> "

    '    Next

    '    Return result

    'End Function

    '<System.Web.Services.WebMethod>
    'Public Shared Function GetDepartmentList() As String

    '    Dim result As String = ""
    '    Dim objDepartment As New BizSoft.Bridge_RS.Department
    '    Dim dtDepartment As DataSet = objDepartment.GetList

    '    result = "<option role='option' value='-1'>--Select--</option>"

    '    For Each dataRow As DataRow In dtDepartment.Tables(0).Rows
    '        result = result + "<option role='option' value=" + dataRow("ID").ToString() + " > " + dataRow("Name").ToString() + " </option> "

    '    Next

    '    Return result

    'End Function

    '<System.Web.Services.WebMethod>
    'Public Shared Function GetEmployeeList() As String

    '    Dim result As String = ""
    '    Dim objEmployee As New BizSoft.Bridge_RS.Employee()
    '    Dim dtEmployee As DataSet = objEmployee.GetListStatus

    '    result = "<option role='option' value='-1'>--Select--</option>"

    '    For Each dataRow As DataRow In dtEmployee.Tables(0).Rows
    '        result = result + "<option role='option' value=" + dataRow("ID").ToString() + " > " + dataRow("Name").ToString() + " </option> "

    '    Next

    '    Return result

    'End Function

    '<System.Web.Services.WebMethod>
    'Public Shared Function GeSegmentList() As String

    '    Dim result As String = ""
    '    Dim objSegment As New BizSoft.Bridge_RS.GeneralLedger
    '    Dim dtSegment As DataSet = objSegment.GetListSegment

    '    result = "<option role='option' value='-1'>--Select--</option>"

    '    For Each dataRow As DataRow In dtSegment.Tables(0).Rows
    '        result = result + "<option role='option' value=" + dataRow("ID").ToString() + " > " + dataRow("Name").ToString() + " </option> "

    '    Next

    '    Return result

    'End Function

    '<System.Web.Services.WebMethod>
    'Public Shared Function GeProjectList() As String

    '    Dim result As String = ""
    '    Dim objProject As New BizSoft.Bridge_RS.GeneralLedger
    '    Dim dtProject As DataSet = objProject.GetListproject

    '    result = "<option role='option' value='-1'>--Select--</option>"

    '    For Each dataRow As DataRow In dtProject.Tables(0).Rows
    '        result = result + "<option role='option' value=" + dataRow("ID").ToString() + " > " + dataRow("Name").ToString() + " </option> "

    '    Next

    '    Return result

    'End Function

    '<System.Web.Services.WebMethod>
    'Public Shared Function GeVoucherTypeList() As String

    '    Dim result As String = ""
    '    Dim objvoucherType As New BizSoft.Bridge_RS.GeneralLedger
    '    Dim dtvoucherType As DataSet = objvoucherType.GetListVoucherType

    '    result = "<option role='option' value='-1'>--Select--</option>"

    '    For Each dataRow As DataRow In dtvoucherType.Tables(0).Rows
    '        result = result + "<option role='option' value=" + dataRow("ID").ToString() + " > " + dataRow("Name").ToString() + " </option> "

    '    Next

    '    Return result

    'End Function

    '<System.Web.Services.WebMethod>
    'Public Shared Function GeReportList() As String

    '    Dim result As String = ""
    '    Dim objReport As New BizSoft.Bridge_RS.GeneralLedger
    '    Dim frmName As String = "mnuREPORTS_AuditTrail"
    '    Dim dtReport As DataSet = objReport.GetReportDetails(frmName)

    '    result = "<option role='option' value='-1'>--Select--</option>"

    '    For Each dataRow As DataRow In dtReport.Tables(0).Rows
    '        result = result + "<option role='option' value=" + dataRow("ID").ToString() + " > " + dataRow("Name").ToString() + " </option> "

    '    Next

    '    Return result

    'End Function

    ' Save
    <System.Web.Services.WebMethod>
    Public Shared Function Save() As String
        Dim ID As String = ""
        '  ddlVocType   ddlVocStatus   ddlLocation   ddlSegment  ddlDepartment 
        ' ddlProject  ddlCreditor  ddlReports   ddlEmployee   ddlDebtor


        Dim ddlVocType As String = HttpContext.Current.Request.QueryString("ddlVocType")
        Dim ddlVocStatus As String = HttpContext.Current.Request.QueryString("ddlVocStatus")
        Dim ddlLocation As String = HttpContext.Current.Request.QueryString("ddlLocation")
        Dim ddlSegment As String = HttpContext.Current.Request.QueryString("ddlSegment")
        Dim ddlDepartment As String = HttpContext.Current.Request.QueryString("ddlDepartment")
        Dim ddlProject As String = HttpContext.Current.Request.QueryString("ddlProject")
        Dim ddlCreditor As String = HttpContext.Current.Request.QueryString("ddlCreditor")
        Dim ddlReports As String = HttpContext.Current.Request.QueryString("ddlReports")
        Dim ddlEmployee As String = HttpContext.Current.Request.QueryString("ddlEmployee")
        Dim ddlDebtor As String = HttpContext.Current.Request.QueryString("ddlDebtor")

        Dim FromDate As String = HttpContext.Current.Request.QueryString("FromDate")
        Dim ToDate As String = HttpContext.Current.Request.QueryString("ToDate")

        Dim query As String = ""
        Dim ht As New Hashtable
        Dim msg As String = ""
        Dim mAccountCode As String = ""
        Dim ObjACt As New BizSoft.Bridge_SCM.ChartOfAccount
        Dim ObjAudit As New BizSoft.Bridge_SCM.AuditTraill
        Dim objReport As New BizSoft.DBManager.clsReport.oReport
        Dim oCulture = New System.Globalization.CultureInfo("en-US", True)

        If ddlReports <> "-1" Then
            objReport.strFileName = ddlReports  ' cmbReport.SelectedValue.ToString

            ObjAudit.GenerateAuditTrail(FromDate, ToDate, ddlVocStatus, ddlVocType, ddlLocation,
                                        ddlDepartment, ddlProject, ddlEmployee, "", ddlSegment,
                                        HttpContext.Current.Session("CompanyID"), 0, msg)

            If ddlReports = "AuditTrial.rpt" Then

                query = SetCritria(ddlLocation, ddlDepartment, ddlProject, ddlEmployee, ddlVocType, ddlDebtor)
                objReport.rptCreteria = query
                ht.Add("User", HttpContext.Current.Session("UserID"))
                ht.Add("rptCriteria", query)

            End If

            objReport.ht = ht
            ID = 1
            HttpContext.Current.Session("ogsReport") = objReport
        End If

        Return ID
    End Function

    Public Shared Function SetCritria(ByVal Location As String, ByVal Department As String, ByVal Project As String, ByVal Employee As String, ByVal Voctype As String, ByVal Debtor As String) As String

        Dim ChkSegment As String = HttpContext.Current.Request.QueryString("ChkSegment")
        Dim ChkDepartment As String = HttpContext.Current.Request.QueryString("ChkDepartment")
        Dim ChkProject As String = HttpContext.Current.Request.QueryString("ChkProject")
        Dim ChkEmployee As String = HttpContext.Current.Request.QueryString("ChkEmployee")
        Dim ChkVoctype As String = HttpContext.Current.Request.QueryString("ChkVoctype")
        Dim ChkDebtor As String = HttpContext.Current.Request.QueryString("ChkDebtor")

        Dim selectionsql As String = ""
        Dim AndFlag As Boolean = False

       
        If ChkSegment = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {VNGL.SegmentID} = " & Location
            Else
                selectionsql = "{VNGL.SegmentID} = " & Location
            End If
            AndFlag = True
        End If
        If ChkDepartment = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {TempAudit.DepartmentID} = '" & Trim(Department) & "'"
            Else
                selectionsql = " {TempAudit.DepartmentID}= '" & Trim(Department) & "'"
                AndFlag = True
            End If
        End If
        If ChkProject = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {TempAudit.ProjectId} = '" & Trim(Project) & "'"
            Else
                selectionsql = " {TempAudit.ProjectId} = '" & Trim(Project) & "'"
            End If
        End If
        If ChkEmployee = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {TempAudit.EmployeeID} = '" & Trim(Employee) & "'"
            Else
                selectionsql = " {TempAudit.EmployeeID} = '" & Trim(Employee) & "'"
            End If
        End If
        If ChkVoctype = "1" Then
            If AndFlag Then
                selectionsql = selectionsql & " AND {TempAudit.VouchertypeID} = '" & Voctype & "'"
            Else
                selectionsql = "{TempAudit.VouchertypeID} = '" & Voctype.Trim & "'"
            End If
            AndFlag = True
        End If

        If ChkDebtor = "1" Then

            If AndFlag Then
                selectionsql = selectionsql & " AND {TempAudit.PayToOrReceivedFrom} = '" & Trim(Debtor) & "'"
            Else
                selectionsql = "{TempAudit.Cust} = '" & Trim(Debtor) & "'"
            End If
        End If

        Return selectionsql
    End Function

End Class