﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="Bridge_SCM._Default" %>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />

    <script src="dtLayout/DataTables-1.10.12/media/js/jquery.min.js"></script>
    <link rel="apple-touch-icon" sizes="76x76" href="Layout/assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="Layout/assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>FMIS</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="Layout/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="Layout/assets/css/turbo.css" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="Layout/assets/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->


    <link href="dtLayout/fonts/fonts.css" rel="stylesheet" />
    <link href="dtLayout/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

</head>
<script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/29/0/common.js"></script>
<script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/29/0/util.js"></script>
<script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/29/0/stats.js"></script>
<script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/29/0/common.js"></script>
<script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/29/0/util.js"></script>
<script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/29/0/stats.js"></script>
<script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/29/0/common.js"></script>
<script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/29/0/util.js"></script>
<script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/29/0/stats.js"></script>


<body style="background-image: url('Layout/Images/nbackGroudImage.jpg')">
   
    <%: Scripts.Render("~/bundles/LoginScript")%>
    <form id="form1" runat="server">
        <nav class="navbar navbar-primary navbar-transparent navbar-absolute">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href=""></a>
                </div>
            </div>
        </nav>
        <div class="wrapper wrapper-full-page">
            <div class="full-page login-page" data-color="white">
                <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                                <form method="#" action="#">
                                    <div class="card card-login card-hidden" style="background-color: #35c3ef">
                                        <div class="card-header text-center">
                                            <h4 class="card-title">Login</h4>
                                        </div>
                                        <div class="card-content">
                                            <div class="social-line">
                                                <span id="lblAllError" class="label label-danger"></span>
                                            </div>

                                            <h5 class="text-center">Application Account</h5>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">face</i>
                                                </span>
                                                <div class="form-group label-floating">
                                                    <label class="control-label">User Name</label>
                                                    <asp:TextBox ID="txtCode" placeholder="" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">lock_outline</i>
                                                </span>
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Password</label>
                                                    <asp:TextBox ID="txtpasswd" runat="server" placeholder="" CssClass="form-control" TextMode="Password"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">work</i>
                                                </span>
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Company</label>
                                                    <asp:DropDownList ID="cmbCompany" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>

                                                <div class="col-lg-5 col-md-6 col-sm-3">
                                                    <div class="btn-group bootstrap-select">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">open_with</i>
                                                </span>
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Location</label>
                                                    <asp:DropDownList ID="cmbLocation" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                                <div class="col-lg-5 col-md-6 col-sm-3">
                                                    <div class="btn-group bootstrap-select">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">open_with</i>
                                                </span>
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Financial Year</label>
                                                    <asp:DropDownList ID="cmbFinancial" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                                <div class="col-lg-5 col-md-6 col-sm-3">
                                                    <div class="btn-group bootstrap-select">
                                                    </div>
                                                </div>

                                                <span class="input-group-addon"></span>
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Working Date</label>
                                                    <input type="date" class="form-control" id="txtWorkingDate" name="txtWorkingDate" placeholder="" />
                                                </div>
                                            </div>
                                            <div class="input-group">
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <button type="button" class="btn btn-fill btn-default" onclick="OnSave()">Sign In</button>
                                            <%--<button type="submit" class="btn btn-rose btn-wd btn-lg" style="background:#999"  >Sign In</button>--%>
                                            <%--<asp:Button ID="btnLogin" Text="Sign In" CssClass="btn btn-rose btn-wd btn-lg" Style="background: #999" />--%>
                                            <%--<p>New to Turbo?&nbsp;&nbsp;
                                        	<a href="register.html">
                            					<i class="material-icons">person_add</i>Register
                        					</a>
                        				</p>--%>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="footer">
                    <div class="container">

                        <p class="copyright pull-right">
                            Copyright &copy;
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                            <a href="www.Bizsoft.pk">BizSoft Solutions (Pvt)Ltd.</a>
                        </p>
                    </div>
                </footer>
            </div>
        </div>
    </form>
</body>
<!--   Core JS Files   -->
<script src="Layout/assets/vendors/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="Layout/assets/vendors/jquery-ui.min.js" type="text/javascript"></script>
<script src="Layout/assets/vendors/bootstrap.min.js" type="text/javascript"></script>
<script src="Layout/assets/vendors/material.min.js" type="text/javascript"></script>
<script src="Layout/assets/vendors/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<!-- Forms Validations Plugin -->
<script src="Layout/assets/vendors/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="Layout/assets/vendors/moment.min.js"></script>
<!--  Charts Plugin -->
<script src="Layout/assets/vendors/chartist.min.js"></script>
<!--  Plugin for the Wizard -->
<script src="Layout/assets/vendors/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin    -->
<script src="Layout/assets/vendors/bootstrap-notify.js"></script>
<!-- DateTimePicker Plugin -->
<script src="Layout/assets/vendors/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<script src="Layout/assets/vendors/jquery-jvectormap.js"></script>
<!-- Sliders Plugin -->
<script src="Layout/assets/vendors/nouislider.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js"></script>
<!-- Select Plugin -->
<script src="Layout/assets/vendors/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin    -->
<script src="Layout/assets/vendors/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin -->
<script src="Layout/assets/vendors/sweetalert2.js"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="Layout/assets/vendors/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin    -->
<script src="Layout/assets/vendors/fullcalendar.min.js"></script>
<!-- TagsInput Plugin -->
<script src="Layout/assets/vendors/jquery.tagsinput.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="Layout/assets/js/turbo.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="Layout/assets/js/demo.js"></script>

<script type="text/javascript">
    $().ready(function () {
        demo.checkFullPageBackgroundImage();

        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    });
</script>

<script>
    function label() {
        $(".label-floating").addClass("is-focused")
    }

    setInterval(label, 5)

   
</script>

</html>
