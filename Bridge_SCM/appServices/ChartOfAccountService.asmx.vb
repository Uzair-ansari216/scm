﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services
Imports System.Reflection

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class ChartOfAccountService
    Inherits System.Web.Services.WebService
    Dim obj As New BizSoft.Bridge_SCM.ChartOFAccount

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function onSaveChart(model As ChartOfAccTreeModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Chart Of Account", If(model.id = "", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim obj As New BizSoft.Bridge_SCM.ChartOFAccount()
                Dim intSatatus As Boolean = 0
                Dim msg As String = ""
                Dim NewID As Long = 0
                Dim GLV_accountCode As String = ""
                Dim GLV_parentCode_i As String = ""
                Dim GLV_levels_i As Integer = 0

                GLV_accountCode = HttpContext.Current.Session("GLV_accountCode")
                GLV_parentCode_i = HttpContext.Current.Session("GLV_parentCode")
                GLV_levels_i = HttpContext.Current.Session("GLV_levels")


                If HttpContext.Current.Session("action") = "add" Then
                    objModel.Message = obj.AddNew(model.AccountCode, model.AccountName, model.Description, GLV_parentCode_i, model.OpeningBalance, GLV_levels_i, HttpContext.Current.Session("UserID"), Date.Now.ToString(), model.AccountTypeID, HttpContext.Current.Session("CompanyID"), model.ISSubLedger, model.AccountCodeOld, model.LocationID, model.BalanceSheetAccountHead, model.ISSubLedger, model.FK_BSID, intSatatus, msg, NewID)
                    objModel.ID = NewID
                    objModel.Status = True
                Else
                    objModel.Message = obj.UpdateRecords(model.AccountCode, model.AccountName, model.Description, model.ParentAccountCode, model.OpeningBalance, model.AccountLevel, HttpContext.Current.Session("UserID"), Date.Now.ToString(), model.AccountTypeID, HttpContext.Current.Session("CompanyID"), model.ISSubLedger, model.AccountCodeOld, model.LocationID, model.BalanceSheetAccountHead, model.ISSubLedger, model.FK_BSID, intSatatus, msg)
                    objModel.ID = model.id
                    objModel.Status = True
                End If
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Function GetAccountsList() As List(Of DropdownModel)
        Dim ddlList As New List(Of DropdownModel)
        Dim objChartOFAccount As New BizSoft.Bridge_SCM.ChartOFAccount
        Dim dtChartOFAccount As DataSet = objChartOFAccount.GetTopAccountLevelCode

        For Each dr As DataRow In dtChartOFAccount.Tables(0).Rows
            Dim ddl As New DropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function


    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Function GetList() As String

        Dim objModel As New MessageModel
        Dim isValidUser As Boolean = True
        If BizSoft.Utilities.HasRights("Chart Of Account", "isView", "1") = False Then
            objModel.Message = "Sorry, you don't have sufficient rights to view this record"
            objModel.Status = False
            isValidUser = False
        End If

        Dim data As New List(Of ChartOfAccTreeModel)
        Dim Jserializer As New System.Web.Script.Serialization.JavaScriptSerializer()

        If isValidUser Then
            Dim id As String = HttpContext.Current.Request.QueryString("id")
            Dim objChartOfAccount As New BizSoft.Bridge_SCM.ChartOFAccount
            Dim ds As DataSet = objChartOfAccount.GetCharOfAccount(Convert.ToInt32(id), HttpContext.Current.Session("CompanyID"))
            Dim dv As New DataView(ds.Tables(0))
            Dim dvItem As New DataView(ds.Tables(0))
            dvItem.RowFilter = "AccountCode='" & id & "'"
            Dim dsItem As New DataSet
            dsItem.Tables.Add(dvItem.ToTable)

            dv.RowFilter = "ParentAccountCode='" & id & "'"
            Dim newds As New DataSet
            newds.Tables.Add(dv.ToTable)

            Dim item As New ChartOfAccTreeModel
            item.text = dsItem.Tables(0).Rows(0)("AccountName") + " ? " + dsItem.Tables(0).Rows(0)("AccountCode") + " ?"
            item.id = dsItem.Tables(0).Rows(0)("AccountCode")
            item.nodes = ChildItems(ds, item.id)

            data.Add(item)
            objModel.GenericList = data
            objModel.Status = True
        End If
        Dim result As String = Jserializer.Serialize(objModel)

        Return result
    End Function

    Private Shared Function ChildItems(ByVal ds As DataSet, ByVal id As String) As List(Of ChartOfAccTreeModel)
        Dim dv As New DataView(ds.Tables(0))
        dv.RowFilter = "ParentAccountCode='" & id & "'"
        Dim child As ChartOfAccTreeModel
        Dim childItem As New List(Of ChartOfAccTreeModel)
        For Each childDr As DataRow In dv.ToTable().Rows
            child = New ChartOfAccTreeModel
            child.id = childDr("AccountCode")
            child.text = childDr("AccountCode") & " - " & childDr("AccountName")
            child.nodes = ChildItems(ds, child.id)

            childItem.Add(child)
        Next
        Return childItem
    End Function

    ' GetCode
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Function GetCode() As List(Of String)
        Dim result As List(Of String)
        result = AddNew()
        Return result
    End Function
    Private Shared Function AddNew() As List(Of String)
        Dim action As String = HttpContext.Current.Request.QueryString("action")
        Dim Id As String = HttpContext.Current.Request.QueryString("editId")
        Dim LeavesId As String = HttpContext.Current.Request.QueryString("LeavesId")
        HttpContext.Current.Session("action") = "add"
        Dim result As String = ""
        Dim listOfResult As List(Of String) = New List(Of String)


        Dim ObjAcct As New BizSoft.Bridge_SCM.ChartOFAccount
        Dim accountCode As DataSet = New DataSet()
        Dim ser As New JavaScriptSerializer()
        Dim ddlList As New List(Of DropdownModel)
        Dim value As String = "0"
        Dim GLCode As String = "0"
        Dim GLCodeNew As String
        Dim GLV_parentCode_N As String = ""
        Dim GLV_accountCode_N As String = ""
        Dim GLV_levels_N As Integer = 0
        BizSoft.Utilities.GLF_NewRecoerd = True
        BizSoft.Utilities.GLF_EditRecord = False

        If Id <> "" Then
            value = Id
            accountCode = ObjAcct.GetAccountCode(Id)

            For Each dr As DataRow In accountCode.Tables(0).Rows
                Dim ddl As New DropdownModel
                ddl.Text = dr("Heading").ToString
                ddl.Value = dr("id").ToString
                ddlList.Add(ddl)
            Next
        End If


        BizSoft.Utilities.AccountGroupID = LeavesId
        'HttpContext.Current.Items("hAccountGroupID") = LeavesId
        HttpContext.Current.Session("h_AccountGroupID") = LeavesId


        BizSoft.Utilities.GL_holdValue = ""
        BizSoft.Utilities.GLV_parentCode = value
        'HttpContext.Current.Items("hParentAccountCode") = value
        HttpContext.Current.Session("GLV_parentCode") = value
        GLV_parentCode_N = value
        Dim dsLevels As DataSet = ObjAcct.GetAcountLevels(value)

        'Dim ds As DataSet = ObjAcct.GetAccountACCountPrandCode(value, HttpContext.Current.Session("CompanyID"))
        'If ds.Tables(0).Rows.Count > 0 Then
        '    'BizSoft.Utilities.GLV_accountCode = ds.Tables(0).Rows.Count + 1
        '    GLV_accountCode_N = ds.Tables(0).Rows.Count + 1
        '    HttpContext.Current.Session("GLV_accountCode") = GLV_accountCode_N
        '    GLCode = ds.Tables(0).Rows.Count + 1
        'Else
        '    ' BizSoft.Utilities.GLV_accountCode = Format(TreeView1.SelectedNode.ChildNodes.Count + 1)
        '    '  GLCode = String.Format(TreeView1.SelectedNode.ChildNodes.Count + 1)
        '    'BizSoft.Utilities.GLV_accountCode = 1
        '    GLV_accountCode_N = 1
        '    HttpContext.Current.Session("GLV_accountCode") = GLV_accountCode_N
        '    GLCode = 1
        '    'HttpContext.Current.Session("h_AccountLevel") = 1
        'End If

        If Len(value) = 1 Then

            'BizSoft.Utilities.GLV_accountCode = BizSoft.Utilities.GLV_parentCode & Format(Convert.ToInt64(BizSoft.Utilities.GLV_accountCode), "00")
            'GLV_accountCode_N = GLV_parentCode_N & Format(Convert.ToInt64(GLV_accountCode_N), "00")

            Dim dsUniq As DataSet = ObjAcct.GetISUnique(value, HttpContext.Current.Session("CompanyID"))
            HttpContext.Current.Session("GLV_accountCode") = GLV_accountCode_N
            If dsUniq.Tables(0).Rows.Count > 0 Then

                'BizSoft.Utilities.GLV_accountCode = BizSoft.Utilities.GLV_accountCode + 1
                'GLV_accountCode_N = GLV_accountCode_N + 1
                HttpContext.Current.Session("GLV_accountCode") = dsUniq.Tables(0).Rows(0)("NewCode").ToString()
            Else
                ' BizSoft.Utilities.GLV_accountCode = BizSoft.Utilities.GLV_accountCode
                GLV_accountCode_N = GLV_accountCode_N
                HttpContext.Current.Session("GLV_accountCode") = GLV_accountCode_N
            End If
            'BizSoft.Utilities.GLV_levels = 2
            GLV_levels_N = dsUniq.Tables(0).Rows(0)("Levels").ToString()
            HttpContext.Current.Session("GLV_levels") = GLV_levels_N
            result = HttpContext.Current.Session("GLV_accountCode")



        ElseIf Len(value) = 3 Then
            ' Dim dsUniq As DataSet = ObjAcct.GetISUnique(BizSoft.Utilities.GLV_accountCode)
            Dim dsUniq As DataSet = ObjAcct.GetISUnique(value, HttpContext.Current.Session("CompanyID"))
            'BizSoft.Utilities.GLV_accountCode = BizSoft.Utilities.GLV_parentCode & Format(Convert.ToInt64(BizSoft.Utilities.GLV_accountCode), "00")
            'GLV_accountCode_N = GLV_parentCode_N & Format(Convert.ToInt64(GLV_accountCode_N), "00")
            'HttpContext.Current.Session("GLV_accountCode") = HttpContext.Current.Session("GLV_parentCode") & Format(Convert.ToInt64(HttpContext.Current.Session("GLV_accountCode")), "00")
            HttpContext.Current.Session("GLV_accountCode") = GLV_accountCode_N
            If dsUniq.Tables(0).Rows.Count > 0 Then
                'BizSoft.Utilities.GLV_accountCode = BizSoft.Utilities.GLV_accountCode + 1


                'GLV_accountCode_N = GLV_accountCode_N + 1
                HttpContext.Current.Session("GLV_accountCode") = dsUniq.Tables(0).Rows(0)("NewCode").ToString()
            Else
                'BizSoft.Utilities.GLV_accountCode = BizSoft.Utilities.GLV_accountCode
                GLV_accountCode_N = GLV_accountCode_N
                HttpContext.Current.Session("GLV_accountCode") = GLV_accountCode_N ' HttpContext.Current.Session("GLV_accountCode")
            End If
            'BizSoft.Utilities.GLV_levels = 3
            GLV_levels_N = dsUniq.Tables(0).Rows(0)("Levels").ToString()
            HttpContext.Current.Session("GLV_levels") = GLV_levels_N
            result = HttpContext.Current.Session("GLV_accountCode")
        ElseIf Len(value) = 5 Then

            Dim dsUniq As DataSet = ObjAcct.GetISUnique(value, HttpContext.Current.Session("CompanyID"))
            'GLCodeNew = HttpContext.Current.Session("GLV_parentCode ") & Format(Convert.ToInt64(GLCode), "000")
            'GLCodeNew = GLV_parentCode_N & Format(Convert.ToInt64(GLCode), "000")

            'GLV_accountCode_N = GLV_parentCode_N & Format(Convert.ToInt64(GLV_accountCode_N), "000")
            'HttpContext.Current.Session("GLV_accountCode") = GLV_accountCode_N
            'this place
            If dsUniq.Tables(0).Rows.Count > 0 Then
                'GLV_accountCode_N = GLV_accountCode_N + 1
                HttpContext.Current.Session("GLV_accountCode") = dsUniq.Tables(0).Rows(0)("NewCode").ToString()
            Else
                GLV_accountCode_N = GLV_accountCode_N
                HttpContext.Current.Session("GLV_accountCode") = GLV_accountCode_N
            End If
            'BizSoft.Utilities.GLV_levels = 4
            GLV_levels_N = dsUniq.Tables(0).Rows(0)("Levels").ToString()
            HttpContext.Current.Session("GLV_levels") = GLV_levels_N
            result = HttpContext.Current.Session("GLV_accountCode")

        ElseIf Len(value) = 8 Then

            Dim dsUniq As DataSet = ObjAcct.GetISUnique(value, HttpContext.Current.Session("CompanyID"))
            'GLV_accountCode_N = GLV_parentCode_N & Format(Convert.ToInt64(GLV_accountCode_N), "0000")

            ' HttpContext.Current.Session("GLV_accountCode") = GLV_accountCode_N
            'this place
            If dsUniq.Tables(0).Rows.Count > 0 Then
                'GLV_accountCode_N = GLV_accountCode_N + 1
                HttpContext.Current.Session("GLV_accountCode") = dsUniq.Tables(0).Rows(0)("NewCode").ToString()
            Else
                GLV_accountCode_N = GLV_accountCode_N
                HttpContext.Current.Session("GLV_accountCode") = GLV_accountCode_N
            End If
            'BizSoft.Utilities.GLV_levels = 5
            GLV_levels_N = dsUniq.Tables(0).Rows(0)("Levels").ToString()
            HttpContext.Current.Session("GLV_levels") = GLV_levels_N
            result = HttpContext.Current.Session("GLV_accountCode")
            'HttpContext.Current.Items("hAccountLevel") = 5
        ElseIf Len(value) > 8 Then

            'GLV_accountCode_N = GLV_parentCode_N & Format(Convert.ToInt64(GLV_accountCode_N), "0000")
            'BizSoft.Utilities.GLV_accountCode = BizSoft.Utilities.GLV_parentCode & Format(Convert.ToInt64(BizSoft.Utilities.GLV_accountCode), "0000")
            'HttpContext.Current.Session("GLV_accountCode") = GLV_accountCode_N

            Dim dsUniq As DataSet = ObjAcct.GetISUnique(value, HttpContext.Current.Session("CompanyID"))
            If dsUniq.Tables(0).Rows.Count > 0 Then
                'GLV_accountCode_N = GLV_accountCode_N + 1
                HttpContext.Current.Session("GLV_accountCode") = dsUniq.Tables(0).Rows(0)("NewCode").ToString()
            Else
                GLV_accountCode_N = GLV_accountCode_N
                HttpContext.Current.Session("GLV_accountCode") = GLV_accountCode_N
            End If
            'BizSoft.Utilities.GLV_levels = 6
            GLV_levels_N = dsUniq.Tables(0).Rows(0)("Levels").ToString()
            HttpContext.Current.Session("GLV_levels") = GLV_levels_N
            result = HttpContext.Current.Session("GLV_accountCode")  'BizSoft.Utilities.GLV_accountCode

        Else
            'GLV_accountCode_N = GLV_parentCode_N & Format(Convert.ToInt64(GLV_accountCode_N), "000")
            'HttpContext.Current.Session("GLV_accountCode") = GLV_accountCode_N
            Dim dsUniq As DataSet = ObjAcct.GetISUnique(value, HttpContext.Current.Session("CompanyID"))
            If dsUniq.Tables(0).Rows.Count > 0 Then
                'GLV_accountCode_N = GLV_accountCode_N + 1
                HttpContext.Current.Session("GLV_accountCode") = dsUniq.Tables(0).Rows(0)("NewCode").ToString()
            Else
                GLV_accountCode_N = GLV_accountCode_N
                HttpContext.Current.Session("GLV_accountCode") = GLV_accountCode_N
            End If
            GLV_levels_N = dsUniq.Tables(0).Rows(0)("Levels").ToString()
            HttpContext.Current.Session("GLV_levels") = GLV_levels_N
            result = HttpContext.Current.Session("GLV_accountCode")
        End If
        'Dim dsLevels As DataSet = ObjAcct.GetAcountLevels(BizSoft.Utilities.GLV_accountCode)

        'If dsLevels.Tables(0).Rows.Count > 0 Then
        '    GLV_levels_N = GLV_levels_N + 1
        '    HttpContext.Current.Session("GLV_levels") = GLV_levels_N  'Convert.ToString(dsLevels.Tables(0).Rows(0)("Levels")) + 1
        'Else
        '    GLV_levels_N = 5
        '    HttpContext.Current.Session("GLV_levels") = GLV_levels_N
        'End If


        'If GLV_levels_N > 5 Then
        '    result = "failed"
        'End If

        'BizSoft.Utilities.ParentAccountCode = value
        HttpContext.Current.Session("GLV_parentCode ") = value

        listOfResult.Add(result)
        listOfResult.Add(ser.Serialize(ddlList))
        Return listOfResult

    End Function


    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Function GetEditDetails() As List(Of String)
        Dim list As New List(Of String)
        Try
            Dim action As String = HttpContext.Current.Request.QueryString("action")
            Dim editId As String = HttpContext.Current.Request.QueryString("editId")
            Dim LeavesId As String = HttpContext.Current.Request.QueryString("LeavesId")

            HttpContext.Current.Session("action") = "edit"
            Dim result As String = ""
            If action = "edit" Then
                If editId <> "" Then
                    Dim objLookup As New BizSoft.Bridge_SCM.ChartOFAccount
                    Dim accountCode As DataSet = New DataSet()
                    Dim ser As New JavaScriptSerializer()
                    Dim ddlList As New List(Of DropdownModel)
                    Dim mQuery As String
                    Dim CurrentDs As DataSet
                    Dim ds As DataSet = objLookup.GetAcountLevels(editId)
                    If ds.Tables(0).Rows.Count > 0 Then
                        If Convert.ToString(ds.Tables(0).Rows(0)("levels")) <= 3 Then
                            mQuery = "select * from vChartofAccount   WHERE AccountCode = '" & editId & "'"
                        Else
                            If Len(editId) > 5 Then
                                mQuery = "select * from vChartofAccount   WHERE AccountCode = '" & editId & "'  AND CompanyID = " & HttpContext.Current.Session("CompanyID") & ""
                            Else
                                mQuery = "select * from vChartofAccount   WHERE AccountCode = '" & editId & "'"
                            End If
                        End If
                    End If
                    CurrentDs = objLookup.Getds(mQuery)
                    If CurrentDs.Tables(0).Rows.Count > 0 Then

                        ' list.Add(Convert.ToString(CurrentDs.Tables(0).Rows(0)("AccountCode"))
                        list.Add(Convert.ToString(CurrentDs.Tables(0).Rows(0)("AccountCode")))
                        list.Add(Convert.ToString(CurrentDs.Tables(0).Rows(0)("AccountName")))
                        list.Add(Convert.ToString(CurrentDs.Tables(0).Rows(0)("Description")))
                        list.Add(Convert.ToString(CurrentDs.Tables(0).Rows(0)("AccountCodeOld")))
                        list.Add(Convert.ToString(CurrentDs.Tables(0).Rows(0)("OpeningBalance")))
                        list.Add(CurrentDs.Tables(0).Rows(0)("AccountType").ToString)
                        list.Add(CurrentDs.Tables(0).Rows(0)("parentAccountCode").ToString)

                        list.Add(CurrentDs.Tables(0).Rows(0)("BalanceSheetLevel").ToString)
                        'chkSummary.Checked = True

                        If IsDBNull(CurrentDs.Tables(0).Rows(0)("DateCreated")) = False Then
                            list.Add(Date.Now.ToShortDateString)  'CurrentDs.Tables(0).Rows(0)("DateCreated")
                        Else
                            list.Add(Date.Now.ToString)
                        End If

                        accountCode = objLookup.GetAccountCode(editId)

                        For Each dr As DataRow In accountCode.Tables(0).Rows
                            Dim ddl As New DropdownModel
                            ddl.Text = dr("Heading").ToString
                            ddl.Value = dr("id").ToString
                            ddlList.Add(ddl)
                        Next
                        list.Add(ser.Serialize(ddlList))

                        Return list
                    End If


                End If
            End If
            Return list
        Catch ex As Exception

        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDelete(ByVal dataID As String, ByVal dataName As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Chart Of Account", "IsDelete", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim obj As New BizSoft.Bridge_SCM.ChartOFAccount()
                Dim intSatatus As Boolean = 0
                Dim msg As String = ""
                'Dim NewID As Integer

                If dataID <> "" Then
                    objModel.Message = obj.Delete(dataID.ToString(), dataName, "0", HttpContext.Current.Session("CompanyID").ToString())
                    objModel.ID = dataID
                    objModel.Status = True
                End If
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function


End Class