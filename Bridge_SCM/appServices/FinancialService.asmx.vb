﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization

<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class FinancialService
    Inherits System.Web.Services.WebService
    Private financial As New BizSoft.Bridge_SCM.clsFinancial
    Private ds As DataSet
    Private ser As New JavaScriptSerializer()

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getFinancialThemes() As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            Dim themeList = getThemes()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(themeList)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function saveFinancialThemes(ByVal name As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            ds = financial.SaveTheme(name)
            Dim themeList = getThemes()

            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(themeList)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function saveFinancialDetail(model As FinancialModel, ByVal IsBold As Int32, ByVal RowColour As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            ds = financial.SaveDetail(model, IsBold, RowColour)
            Dim list As New List(Of FinancialModel)
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New FinancialModel() With {
                      .ID = row("DetailID").ToString(),
                      .ParentCode = row("ParentCode").ToString(),
                      .code = row("Code").ToString(),
                      .Heading = row("Heading").ToString(),
                      .Amount = row("Amount").ToString(),
                      .DebitAmount = row("DrAmount").ToString(),
                      .CreditAmount = row("CrAmount").ToString(),
                      .NetAmount = row("NetAmount").ToString(),
                      .IsBold = If(IsDBNull(row("isBold")), False, row("isBold")),
                      .RowColour = If(IsDBNull(row("bg_Color")), "", row("bg_Color")).ToString()
                    })
                Next
            End If

            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getFinancialDetail(ByVal theme As String, ByVal document As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            ds = financial.getDetails(theme, document)
            Dim list As New List(Of FinancialModel)
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New FinancialModel() With {
                      .ID = row("DetailID").ToString(),
                      .ParentCode = row("ParentCode").ToString(),
                      .code = row("Code").ToString(),
                      .Heading = row("Heading").ToString(),
                      .Amount = row("Amount").ToString(),
                      .DebitAmount = row("DrAmount").ToString(),
                      .CreditAmount = row("CrAmount").ToString(),
                      .NetAmount = row("NetAmount").ToString(),
                      .IsBold = If(IsDBNull(row("isBold")), False, row("isBold")),
                      .RowColour = If(IsDBNull(row("bg_Color")), "", row("bg_Color")).ToString()
                    })
                Next
            End If

            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function addEditFinancialDetail(ByVal id As String, ByVal theme As String, ByVal code As String, ByVal parentCode As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            Dim financialVm As New FinancialModel
            If Convert.ToInt32(id) > 0 Then
                ds = financial.getByID(id)
                If ds.Tables(0).Rows.Count > 0 Then
                    financialVm.ID = ds.Tables(0).Rows(0)("DetailID")
                    financialVm.code = ds.Tables(0).Rows(0)("Code")
                    financialVm.ParentCode = If(IsDBNull(ds.Tables(0).Rows(0)("ParentCode")), "", ds.Tables(0).Rows(0)("ParentCode"))
                    financialVm.Heading = ds.Tables(0).Rows(0)("Heading")
                    financialVm.FixedValue = If(IsDBNull(ds.Tables(0).Rows(0)("fixedValue")), "", ds.Tables(0).Rows(0)("fixedValue"))
                    financialVm.AddToCode = If(IsDBNull(ds.Tables(0).Rows(0)("AddtoCode")), "", ds.Tables(0).Rows(0)("AddtoCode"))
                    financialVm.NotesCode = If(IsDBNull(ds.Tables(0).Rows(0)("NotesCode")), "", ds.Tables(0).Rows(0)("NotesCode"))
                    financialVm.Impact = ds.Tables(0).Rows(0)("Impact")
                    financialVm.IsShowInReport = ds.Tables(0).Rows(0)("isShowInReport")
                    financialVm.IsBold = ds.Tables(0).Rows(0)("isBold")
                    financialVm.RowColour = ds.Tables(0).Rows(0)("bg_Color")
                End If
            Else
                ds = financial.getCode(theme, code)
                If ds.Tables(0).Rows.Count > 0 Then
                    financialVm.code = ds.Tables(0).Rows(0)("Column1")
                End If
            End If

            Dim financialHeadinglist As New List(Of DropdownModel)
            ds = financial.GetHeadings(theme, id, parentCode)
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).AsEnumerable()
                    financialHeadinglist.Add(New DropdownModel() With {
                          .Value = row("DetailID").ToString(),
                          .Text = row("Heading").ToString()
                        })
                Next
            End If
            financialVm.headingList = financialHeadinglist

            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(financialVm)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function deleteFinancialDetail(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            Dim data = financial.deleteDetail(id)

            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(data)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function generateTrialBalance(ByVal fromDate As Date, ByVal toDate As Date, ByVal financialTheme As Int32, ByVal document As Int32) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            ds = financial.GetTrialBalance(fromDate, toDate)
            financial.refreshFinancialDetails(financialTheme)

            'Get Financial details by financial theme and document
            ds = financial.getDetails(financialTheme, document)
            Dim financialDetaillist As New List(Of FinancialModel)
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    financialDetaillist.Add(New FinancialModel() With {
                      .ID = row("DetailID").ToString(),
                      .ParentCode = row("ParentCode").ToString(),
                      .code = row("Code").ToString(),
                      .Heading = row("Heading").ToString(),
                      .Amount = row("Amount").ToString(),
                      .DebitAmount = row("DrAmount").ToString(),
                      .CreditAmount = row("CrAmount").ToString(),
                      .NetAmount = row("NetAmount").ToString(),
                      .IsBold = If(IsDBNull(row("isBold")), False, row("isBold"))
                    })
                Next
            End If

            Dim resultSet As New MessageModel
            resultSet.Status = True
            resultSet.Message = "Trial Balance generated successfully"
            resultSet.GenericList = financialDetaillist

            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(resultSet)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getFinancialMapping(ByVal financialtheme As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            Dim list As List(Of TrialBalanceModel)
            ds = financial.TrialBalance(financialtheme)
            If ds.Tables(0).Rows.Count > 0 Then
                list = getTrialBalances(ds)
            End If

            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function MapTrialBalance(model As List(Of FinancialMappingModel), ByVal financialtheme As String, ByVal document As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            Dim result As String = ""
            Dim trialBalancelist As List(Of TrialBalanceModel)
            financial.SaveMapping(model, financialtheme)

            'Get Trial Balances by financial theme
            ds = financial.TrialBalance(financialtheme)
            If ds.Tables(0).Rows.Count > 0 Then
                trialBalancelist = getTrialBalances(ds)
            End If

            'Get Financial details by financial theme and document
            ds = financial.getDetails(financialtheme, document)
            Dim financialDetaillist As New List(Of FinancialModel)
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    financialDetaillist.Add(New FinancialModel() With {
                      .ID = row("DetailID").ToString(),
                      .ParentCode = row("ParentCode").ToString(),
                      .code = row("Code").ToString(),
                      .Heading = row("Heading").ToString(),
                      .Amount = row("Amount").ToString(),
                      .DebitAmount = row("DrAmount").ToString(),
                      .CreditAmount = row("CrAmount").ToString(),
                      .NetAmount = row("NetAmount").ToString(),
                      .IsBold = If(IsDBNull(row("isBold")), False, row("isBold"))
                    })
                Next
            End If

            Dim resultSet = New FinancialResultSet With
            {
            .TrialBalanceList = trialBalancelist,
            .FinancialDetailList = financialDetaillist
            }

            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(resultSet)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FinancialUnMapping(ByVal code As String, ByVal detailId As String, ByVal financialtheme As String, ByVal document As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            Dim trialBalancelist As List(Of TrialBalanceModel)
            ds = financial.UnMapping(code, detailId, financialtheme)
            ds = financial.TrialBalance(financialtheme)
            If ds.Tables(0).Rows.Count > 0 Then
                trialBalancelist = getTrialBalances(ds)
            End If


            'Get Financial details by financial theme and document
            ds = financial.getDetails(financialtheme, document)
            Dim financialDetaillist As New List(Of FinancialModel)
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    financialDetaillist.Add(New FinancialModel() With {
                      .ID = row("DetailID").ToString(),
                      .ParentCode = row("ParentCode").ToString(),
                      .code = row("Code").ToString(),
                      .Heading = row("Heading").ToString(),
                      .Amount = row("Amount").ToString(),
                      .DebitAmount = row("DrAmount").ToString(),
                      .CreditAmount = row("CrAmount").ToString(),
                      .NetAmount = row("NetAmount").ToString(),
                      .IsBold = If(IsDBNull(row("isBold")), False, row("isBold"))
                    })
                Next
            End If
            Dim resultSet = New FinancialResultSet With
            {
            .TrialBalanceList = trialBalancelist,
            .FinancialDetailList = financialDetaillist
            }

            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(resultSet)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getHeadingDetail(ByVal id As Int32, ByVal theme As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            ds = financial.getHeadingDetails(id, theme)
            Dim headingDetailList As New List(Of FinancialModel)
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    headingDetailList.Add(New FinancialModel() With {
                      .code = row("Accountcode").ToString(),
                      .DebitAmount = row("Dr_Amount").ToString(),
                      .CreditAmount = row("Cr_Amount").ToString(),
                      .Heading = row("AccountName").ToString()
                    })
                Next
            End If

            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(headingDetailList)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function refreshFinancialDetails(ByVal financialTheme As Int32, ByVal document As Int32) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            ds = financial.refreshFinancialDetails(financialTheme)

            'Get Financial details by financial theme and document
            ds = financial.getDetails(financialTheme, document)
            Dim financialDetaillist As New List(Of FinancialModel)
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    financialDetaillist.Add(New FinancialModel() With {
                      .ID = row("DetailID").ToString(),
                      .ParentCode = row("ParentCode").ToString(),
                      .code = row("Code").ToString(),
                      .Heading = row("Heading").ToString(),
                      .Amount = row("Amount").ToString(),
                      .DebitAmount = row("DrAmount").ToString(),
                      .CreditAmount = row("CrAmount").ToString(),
                      .NetAmount = row("NetAmount").ToString(),
                      .IsBold = If(IsDBNull(row("isBold")), False, row("isBold"))
                    })
                Next
            End If

            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(financialDetaillist)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getUnMappedChartOfAccount(ByVal financialTheme As String, ByVal document As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            Dim unMappedCOAlist As New List(Of TrialBalanceModel)
            ds = financial.GetUnMappedChartOfAccount(financialTheme, document)
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).AsEnumerable()
                    unMappedCOAlist.Add(New TrialBalanceModel() With {
                          .AccountType = row("AccountType").ToString(),
                          .UserID = row("userID").ToString(),
                          .CompanyID = row("companyID").ToString(),
                          .AcountCode = row("accountCode").ToString(),
                          .AccountName = row("accountName").ToString(),
                          .ODabit = row("ODebit").ToString(),
                          .OCredit = row("OCredit").ToString(),
                          .Debit = row("Debit").ToString(),
                          .Credit = row("Credit").ToString(),
                          .CDebit = row("CDebit").ToString(),
                          .CCredit = row("CCredit").ToString(),
                          .SAccount = row("SAccount").ToString()
                        })
                Next
            End If

            Dim financialHeadinglist As New List(Of DropdownModel)
            ds = financial.GetFinancialHeadings(financialTheme)
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).AsEnumerable()
                    financialHeadinglist.Add(New DropdownModel() With {
                          .Value = row("DetailID").ToString(),
                          .Text = row("Title").ToString()
                        })
                Next
            End If

            Dim resultSet = New FinancialResultSet With
            {
            .TrialBalanceList = unMappedCOAlist,
            .FinancialHeadingList = financialHeadinglist
            }

            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(resultSet)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function financialHeadingMapping(model As List(Of FinancialMappingModel), ByVal financialtheme As String, ByVal document As String) As String
        Try
            financial.FinancialMapping(model, financialtheme)

            'Get Financial details by financial theme and document
            ds = financial.getDetails(financialtheme, document)
            Dim financialDetailList As New List(Of FinancialModel)
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    financialDetailList.Add(New FinancialModel() With {
                      .ID = row("DetailID").ToString(),
                      .ParentCode = row("ParentCode").ToString(),
                      .code = row("Code").ToString(),
                      .Heading = row("Heading").ToString(),
                      .Amount = row("Amount").ToString(),
                      .DebitAmount = row("DrAmount").ToString(),
                      .CreditAmount = row("CrAmount").ToString(),
                      .NetAmount = row("NetAmount").ToString(),
                      .IsBold = If(IsDBNull(row("isBold")), False, row("isBold"))
                    })
                Next
            End If

            Dim resultSet As New MessageModel
            resultSet.Status = True
            resultSet.Message = "Financial heading mapped successfully"
            resultSet.GenericList = financialDetailList

            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(resultSet)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    Private Function getThemes() As List(Of DropdownModel)
        Dim themeList As New List(Of DropdownModel)
        ds = financial.GetAllThemes()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                themeList.Add(New DropdownModel() With {
                      .Value = row("ID").ToString(),
                      .Text = row("financialName").ToString()
                    })
            Next
        End If
        Return themeList
    End Function

    Private Function getTrialBalances(ds As DataSet) As List(Of TrialBalanceModel)
        Dim list As New List(Of TrialBalanceModel)
        For Each row As DataRow In ds.Tables(0).AsEnumerable()
            list.Add(New TrialBalanceModel() With {
                          .Heading = row("Heading").ToString(),
                          .AccountType = row("AccountType").ToString(),
                          .UserID = row("userID").ToString(),
                          .CompanyID = row("companyID").ToString(),
                          .AcountCode = row("accountCode").ToString(),
                          .AccountName = row("accountName").ToString(),
                          .ODabit = row("ODebit").ToString(),
                          .OCredit = row("OCredit").ToString(),
                          .Debit = row("Debit").ToString(),
                          .Credit = row("Credit").ToString(),
                          .CDebit = row("CDebit").ToString(),
                          .CCredit = row("CCredit").ToString(),
                          .SAccount = row("SAccount").ToString(),
                          .ParentCode = row("ParentCode").ToString()
                        })
        Next
        Return list
    End Function
End Class