﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class DepartmentService
    Inherits System.Web.Services.WebService


    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGrid() As String
        Try
            Dim objModel As New MessageModel
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Department", "isView", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to view this record"
                objModel.Status = False
                isValidUser = False
            End If

            Dim list As New List(Of DepartmentModel)
            If isValidUser Then
                Dim objBankTitle As New BizSoft.Bridge_SCM.Department()
                Dim ds As DataSet = objBankTitle.GetList()

                If ds.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In ds.Tables(0).Rows
                        list.Add(New DepartmentModel() With {
                          .ID = row("ID").ToString(),
                          .DepartmentName = row("Name").ToString(),
                          .Description = row("Description").ToString(),
                          .DepartmentHead = row("FirstName").ToString(),
                          .Code = row("DepartmentHead").ToString()
                        })

                    Next
                End If
                objModel.GenericList = list
                objModel.Status = True
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function AddEditDepartment(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Department", If(id = "0", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim department As New DepartmentModel
                Dim departmentModel As New BizSoft.Bridge_SCM.Department()
                If id <> "0" Then
                    Dim ds As DataSet = departmentModel.GetById(id)
                    If ds.Tables(0).Rows.Count > 0 Then
                        department.ID = ds.Tables(0).Rows(0)("DepartmentID").ToString()
                        department.DepartmentHead = ds.Tables(0).Rows(0)("DepartmentHead").ToString()
                        department.DepartmentName = ds.Tables(0).Rows(0)("DepartmentName").ToString()
                        department.Description = ds.Tables(0).Rows(0)("Description").ToString()
                    End If
                End If
                objModel.GenericList = department
                objModel.Status = True
            End If


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSave(model As DepartmentModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.Department
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As String

            If model.ID = "" Then
                objModel.Message = obj.Add(model.Code, model.DepartmentName, model.DepartmentHead, model.Description, intSatatus, msg, NewID)
                objModel.ID = NewID
                objModel.Status = True
            Else
                objModel.Message = obj.Update(model.Code, model.DepartmentName, model.DepartmentHead, model.Description, intSatatus, msg)
                objModel.ID = model.Code
                objModel.Status = True
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDelete(ByVal dataID As String, ByVal dataName As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.Department()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Integer

            If dataID <> "" Then
                objModel.Message = obj.Delete(dataID, dataName, intSatatus, msg)
                objModel.ID = dataID
                objModel.Status = True
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetHeadPerson() As String

        Dim obj As New BizSoft.Bridge_SCM.Department()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetList2()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("EmployeeID").ToString,
                      .Text = row("Emp").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function


End Class