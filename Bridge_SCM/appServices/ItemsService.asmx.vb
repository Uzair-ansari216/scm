﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class ItemsService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGrid() As String
        Try
            Dim list As New List(Of ItemModel)
            Dim objBankTitle As New BizSoft.Bridge_SCM.clsItem()
            Dim ds As DataSet = objBankTitle.GetList()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    If row("isActive") = True And row("GST") = True Then
                        list.Add(New ItemModel() With {
                          .ID = row("PK_Item").ToString(),
                          .FK_ItemCategory = row("FK_ItemCategory").ToString(),
                          .FK_BrandID = row("FK_BrandID").ToString(),
                          .FK_Unit = row("FK_Unit").ToString(),
                          .ItemCategory = row("ItemCategory").ToString(),
                          .SegmentName = row("SegmentName").ToString(),
                          .Unit = row("Unit").ToString(),
                          .PartNumber = row("PartNumber").ToString(),
                          .ModelNumber = row("ModelNumber").ToString(),
                          .ProductName = row("ProductName").ToString(),
                          .Description = row("Description").ToString(),
                          .MinQuantity = row("MinQuantity").ToString(),
                          .MaxQuantity = row("MaxQuantity").ToString(),
                          .ReOrderQuantity = row("ReOrderQuantity").ToString(),
                          .DeliveryLandTime = row("DeliveryLandTime").ToString(),
                          .SalePoint = row("SalePoint").ToString(),
                          .ExpGST = row("ExpGST").ToString(),
                          .WarrantyYear = row("WarrantyYear").ToString(),
                          .WarrantyNote = row("WarrantyNote").ToString(),
                          .isActive = "True",
                          .GST = "True",
                          .AvailableForSO = row("AvailableForSO").ToString(),
                          .ItemToBeRepost = row("ItemToBeRepost").ToString(),
                          .isSerialNo = row("isSerialNo").ToString()
                        })
                    ElseIf row("isActive") = False And row("GST") = False Then
                        list.Add(New ItemModel() With {
                    .ID = row("PK_Item").ToString(),
                    .FK_ItemCategory = row("FK_ItemCategory").ToString(),
                    .FK_BrandID = row("FK_BrandID").ToString(),
                    .FK_Unit = row("FK_Unit").ToString(),
                    .ItemCategory = row("ItemCategory").ToString(),
                    .SegmentName = row("SegmentName").ToString(),
                    .Unit = row("Unit").ToString(),
                    .PartNumber = row("PartNumber").ToString(),
                    .ModelNumber = row("ModelNumber").ToString(),
                    .ProductName = row("ProductName").ToString(),
                    .Description = row("Description").ToString(),
                    .MinQuantity = row("MinQuantity").ToString(),
                    .MaxQuantity = row("MaxQuantity").ToString(),
                    .ReOrderQuantity = row("ReOrderQuantity").ToString(),
                    .DeliveryLandTime = row("DeliveryLandTime").ToString(),
                    .SalePoint = row("SalePoint").ToString(),
                    .ExpGST = row("ExpGST").ToString(),
                    .WarrantyYear = row("WarrantyYear").ToString(),
                    .WarrantyNote = row("WarrantyNote").ToString(),
                    .isActive = "False",
                    .GST = "False",
                    .AvailableForSO = row("AvailableForSO").ToString(),
                    .ItemToBeRepost = row("ItemToBeRepost").ToString(),
                    .isSerialNo = row("isSerialNo").ToString()
                })
                    ElseIf row("isActive") = True And row("GST") = False Then
                        list.Add(New ItemModel() With {
                    .ID = row("PK_Item").ToString(),
                    .FK_ItemCategory = row("FK_ItemCategory").ToString(),
                    .FK_BrandID = row("FK_BrandID").ToString(),
                    .FK_Unit = row("FK_Unit").ToString(),
                    .ItemCategory = row("ItemCategory").ToString(),
                    .SegmentName = row("SegmentName").ToString(),
                    .Unit = row("Unit").ToString(),
                    .PartNumber = row("PartNumber").ToString(),
                    .ModelNumber = row("ModelNumber").ToString(),
                    .ProductName = row("ProductName").ToString(),
                    .Description = row("Description").ToString(),
                    .MinQuantity = row("MinQuantity").ToString(),
                    .MaxQuantity = row("MaxQuantity").ToString(),
                    .ReOrderQuantity = row("ReOrderQuantity").ToString(),
                    .DeliveryLandTime = row("DeliveryLandTime").ToString(),
                    .SalePoint = row("SalePoint").ToString(),
                    .ExpGST = row("ExpGST").ToString(),
                    .WarrantyYear = row("WarrantyYear").ToString(),
                    .WarrantyNote = row("WarrantyNote").ToString(),
                    .isActive = "True",
                    .GST = "False",
                    .AvailableForSO = row("AvailableForSO").ToString(),
                    .ItemToBeRepost = row("ItemToBeRepost").ToString(),
                    .isSerialNo = row("isSerialNo").ToString()
                })
                    ElseIf row("isActive") = False And row("GST") = True Then
                        list.Add(New ItemModel() With {
                    .ID = row("PK_Item").ToString(),
                    .FK_ItemCategory = row("FK_ItemCategory").ToString(),
                    .FK_BrandID = row("FK_BrandID").ToString(),
                    .FK_Unit = row("FK_Unit").ToString(),
                    .ItemCategory = row("ItemCategory").ToString(),
                    .SegmentName = row("SegmentName").ToString(),
                    .Unit = row("Unit").ToString(),
                    .PartNumber = row("PartNumber").ToString(),
                    .ModelNumber = row("ModelNumber").ToString(),
                    .ProductName = row("ProductName").ToString(),
                    .Description = row("Description").ToString(),
                    .MinQuantity = row("MinQuantity").ToString(),
                    .MaxQuantity = row("MaxQuantity").ToString(),
                    .ReOrderQuantity = row("ReOrderQuantity").ToString(),
                    .DeliveryLandTime = row("DeliveryLandTime").ToString(),
                    .SalePoint = row("SalePoint").ToString(),
                    .ExpGST = row("ExpGST").ToString(),
                    .WarrantyYear = row("WarrantyYear").ToString(),
                    .WarrantyNote = row("WarrantyNote").ToString(),
                    .isActive = "False",
                    .GST = "True",
                    .AvailableForSO = row("AvailableForSO").ToString(),
                    .ItemToBeRepost = row("ItemToBeRepost").ToString(),
                    .isSerialNo = row("isSerialNo").ToString()
                })
                    End If
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetAll() As String
        Try
            Dim list As New List(Of ItemModel)
            Dim objBankTitle As New BizSoft.Bridge_SCM.clsItem()
            Dim ds As DataSet = objBankTitle.GetAll()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New ItemModel() With {
                          .ID = row("PK_Item").ToString(),
                          .PartNumber = row("PartNumber").ToString(),
                          .ModelNumber = row("ModelNumber").ToString(),
                          .Description = row("Description").ToString()
                })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSave(model As ItemModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.clsItem
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Long

            If model.ID = "" Then
                objModel.Message = obj.Add(0, model.FK_ItemCategory, model.FK_BrandID, model.FK_Unit, model.PartNumber, model.ModelNumber, model.ProductName, model.Description, model.MinQuantity, model.MaxQuantity, model.ReOrderQuantity, model.DeliveryLandTime, model.SalePoint, model.ExpGST, model.WarrantyYear, model.WarrantyNote, model.isActive, model.GST, model.AvailableForSO, model.ItemToBeRepost, model.isSerialNo, intSatatus, msg, NewID)
                objModel.ID = NewID
            Else
                objModel.Message = obj.Update(model.ID, model.FK_ItemCategory, model.FK_BrandID, model.FK_Unit, model.PartNumber, model.ModelNumber, model.ProductName, model.Description, model.MinQuantity, model.MaxQuantity, model.ReOrderQuantity, model.DeliveryLandTime, model.SalePoint, model.ExpGST, model.WarrantyYear, model.WarrantyNote, model.isActive, model.GST, model.AvailableForSO, model.ItemToBeRepost, model.isSerialNo, intSatatus, msg)
                objModel.ID = model.ID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getItemById(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.clsItem
            Dim item As New ItemModel
            Dim ds As DataSet = obj.getById(id)
            If ds.Tables(0).Rows.Count > 0 Then
                item.ID = ds.Tables(0).Rows(0)("PK_item")
                item.FK_ItemCategory = ds.Tables(0).Rows(0)("FK_itemCategory")
                item.FK_BrandID = ds.Tables(0).Rows(0)("FK_BrandID")
                item.FK_Unit = ds.Tables(0).Rows(0)("FK_Unit")
                item.PartNumber = ds.Tables(0).Rows(0)("PartNumber")
                item.ModelNumber = ds.Tables(0).Rows(0)("ModelNumber")
                item.ProductName = ds.Tables(0).Rows(0)("ProductName")
                item.Description = ds.Tables(0).Rows(0)("Description")
                item.MinQuantity = ds.Tables(0).Rows(0)("MinQuantity")
                item.MaxQuantity = ds.Tables(0).Rows(0)("MaxQuantity")
                item.ReOrderQuantity = ds.Tables(0).Rows(0)("ReOrderQuantity")
                item.DeliveryLandTime = ds.Tables(0).Rows(0)("DeliveryLandTime")
                item.SalePoint = ds.Tables(0).Rows(0)("SalePoint")
                item.ExpGST = ds.Tables(0).Rows(0)("ExpGST")
                item.WarrantyYear = ds.Tables(0).Rows(0)("WarrantyYear")
                item.WarrantyNote = ds.Tables(0).Rows(0)("WarrantyNote")
                item.isActive = ds.Tables(0).Rows(0)("isActive")
                item.GST = ds.Tables(0).Rows(0)("GST")
                item.AvailableForSO = ds.Tables(0).Rows(0)("AvailableForSO")
                item.ItemToBeRepost = ds.Tables(0).Rows(0)("ItemToBeRepost")
                item.isSerialNo = ds.Tables(0).Rows(0)("isSerialNo")
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(item)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDelete(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.clsItem()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""

            If dataID <> "" Then
                objModel.Message = obj.Delete(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetItem() As String

        Dim obj As New BizSoft.Bridge_SCM.clsItemCategory()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetLists2()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("PK_ItemCategory").ToString,
                      .Text = row("ItemCategory").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetBrand() As String

        Dim obj As New BizSoft.Bridge_SCM.clsItem()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetListBrand()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("SegmentID").ToString,
                      .Text = row("SegmentName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetUnit() As String

        Dim obj As New BizSoft.Bridge_SCM.clsItem()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetListUnit()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("PK_Unit").ToString,
                      .Text = row("Unit").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function

End Class