﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class LocationService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGrid() As String
        Try
            Dim objModel As New MessageModel
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Locations", "isView", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to view this record"
                objModel.Status = False
                isValidUser = False
            End If

            Dim list As New List(Of LocationModel)
            If isValidUser Then
                Dim objBankTitle As New BizSoft.Bridge_SCM.Location()
                Dim ds As DataSet = objBankTitle.GetList()

                If ds.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In ds.Tables(0).Rows
                        list.Add(New LocationModel() With {
                          .ID = row("LocationID").ToString(),
                          .Location = row("Location").ToString(),
                          .LocationCode = row("LocationCode").ToString(),
                          .FK_CityID = row("FK_CityID").ToString(),
                          .cityName = row("cityName").ToString(),
                          .Accounts_Receivable = row("Accounts_Receivable").ToString(),
                          .Sales_Credit = row("Sales_Credit").ToString(),
                          .Sales_Tax = row("Sales_Tax").ToString(),
                          .Sales_Return = row("Sales_Return").ToString(),
                          .Stock = row("Stock").ToString(),
                          .CostofGoodSold = row("CostofGoodSold").ToString(),
                          .Purchase = row("Purchase").ToString(),
                          .LocalPurchase = row("LocalPurchase").ToString(),
                          .Discount = row("Discount").ToString(),
                          .Charges = row("Charges").ToString(),
                          .StockInTransit = row("StockInTransit").ToString(),
                          .SalesReturn = row("SalesReturn").ToString(),
                         .ImportCost = row("ImportCost").ToString(),
                         .ImportCostDr = row("ImportCostDr").ToString()
                        })
                    Next
                End If
                objModel.GenericList = list
                objModel.Status = True
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function AddEditLocation(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Locations", If(id = "0", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim location As New LocationModel
                Dim locationModel As New BizSoft.Bridge_SCM.Location()
                If id <> "0" Then
                    Dim ds As DataSet = locationModel.GetById(id)
                    If ds.Tables(0).Rows.Count > 0 Then
                        location.ID = ds.Tables(0).Rows(0)("LocationID").ToString()
                        location.Location = ds.Tables(0).Rows(0)("Location").ToString()
                        location.FK_CityID = ds.Tables(0).Rows(0)("FK_CityID").ToString()
                        location.LocationCode = ds.Tables(0).Rows(0)("LocationCode").ToString()
                        'location.cityName = ds.Tables(0).Rows(0)("SegmentName").ToString()
                        'location.Title = ds.Tables(0).Rows(0)("SegmentName").ToString()
                        'location.AccountName = ds.Tables(0).Rows(0)("SegmentName").ToString()
                        'location.AccountCode = ds.Tables(0).Rows(0)("SegmentName").ToString()
                        location.Accounts_Receivable = ds.Tables(0).Rows(0)("Accounts_Receivable").ToString()
                        location.Sales_Credit = ds.Tables(0).Rows(0)("Sales_Credit").ToString()
                        location.Sales_Tax = ds.Tables(0).Rows(0)("Sales_Tax").ToString()
                        location.Sales_Return = ds.Tables(0).Rows(0)("Sales_Return").ToString()
                        location.Stock = ds.Tables(0).Rows(0)("Stock").ToString()
                        location.CostofGoodSold = ds.Tables(0).Rows(0)("CostofGoodSold").ToString()
                        location.Purchase = ds.Tables(0).Rows(0)("Purchase").ToString()
                        location.LocalPurchase = ds.Tables(0).Rows(0)("LocalPurchase").ToString()
                        location.Discount = ds.Tables(0).Rows(0)("Discount").ToString()
                        location.Charges = ds.Tables(0).Rows(0)("Charges").ToString()
                        location.StockInTransit = ds.Tables(0).Rows(0)("StockInTransit").ToString()
                        location.SalesReturn = ds.Tables(0).Rows(0)("SalesReturn").ToString()
                        location.ImportCost = ds.Tables(0).Rows(0)("ImportCost").ToString()
                        location.ImportCostDr = ds.Tables(0).Rows(0)("ImportCostDr").ToString()
                    End If
                End If
                objModel.GenericList = location
                objModel.Status = True
            End If


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSave(model As LocationModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.Location
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Long

            If model.ID = "" Then
                objModel.Message = obj.Add(model, intSatatus, msg, NewID)
                objModel.ID = NewID
                objModel.Status = True
            Else
                objModel.Message = obj.Update(model, intSatatus, msg)
                objModel.ID = model.ID
                objModel.Status = True
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDelete(ByVal dataID As String, ByVal dataName As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Locations", "IsDelete", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim obj As New BizSoft.Bridge_SCM.Location()
                Dim intSatatus As Boolean = 0
                Dim msg As String = ""
                Dim NewID As Integer

                If dataID <> "" Then
                    objModel.Message = obj.Delete(dataID, dataName, intSatatus, msg)
                    objModel.ID = dataID
                    objModel.Status = True
                End If
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetCityList() As String

        Dim obj As New BizSoft.Bridge_SCM.Location()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetCity()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                     .Value = row("ID").ToString,
                     .Text = row("Name").ToString
                     })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridAccount() As String
        Try
            Dim list As New List(Of LocationModel)
            Dim objBankTitle As New BizSoft.Bridge_SCM.Location()
            Dim ds As DataSet = objBankTitle.GetAccountList()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New LocationModel() With {
                      .ID = row("ID").ToString(),
                      .Title = row("AccountTitle").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridDetail() As String
        Try
            Dim list As New List(Of LocationModel)
            Dim obj As New BizSoft.Bridge_SCM.Location()
            Dim ds As DataSet = obj.AccountCode()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New LocationModel() With {
                      .AccountName = row("AccountName").ToString(),
                      .AccountCode = row("AccountCode").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetAccountName(AccountCode As String) As String
        Try
            Dim list As New List(Of LocationModel)
            Dim obj As New BizSoft.Bridge_SCM.Location()
            Dim ds As DataSet = obj.AccountName(AccountCode)

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New LocationModel() With {
                      .AccountName = row("AccountName").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
End Class