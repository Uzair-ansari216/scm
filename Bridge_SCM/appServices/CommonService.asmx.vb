﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class CommonService
    Inherits System.Web.Services.WebService
    Private common As New BizSoft.Bridge_SCM.Common

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function UpdateAppServices(developerMode As String, toolTip As String, training As String, voiceMessage As String) As String
        Try
            Dim asasd = HttpContext.Current.Session("AppServices")
            Dim services As AppServicesModel = New AppServicesModel()

            If developerMode = "on" Then
                Session("DeveloperMode") = True
            Else
                Session("DeveloperMode") = False
            End If

            If toolTip = "on" Then
                Session("ToolTip") = True
            Else
                Session("ToolTip") = False
            End If

            If training = "on" Then
                Session("Training") = True
            Else
                Session("Training") = False
            End If

            If voiceMessage = "on" Then
                Session("VoiceMessage") = True
            Else
                Session("VoiceMessage") = False
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize("")
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function AddFormFields(model As List(Of CommonModel)) As String
        Try
            Dim result As String = ""
            'Dim jsSerializer As New JavaScriptSerializer()
            'Dim controlList = jsSerializer.Deserialize(Of List(Of CommonModel))(HttpContext.Current.Request.QueryString("control"))

            common.SaveFormControls(model)


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(result)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getRequiredToolTip() As String
        Try
            Dim formName As String = HttpContext.Current.Request.QueryString("formName")
            Dim list As New List(Of CommonModel)
            Dim ds As DataSet = common.GetRequiredFields(formName)
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New CommonModel() With {
                      .MenuName = row("MenuName").ToString(),
                      .ObjectName = row("Object_Name").ToString(),
                      .Label = row("Label").ToString(),
                      .ToolTip = row("Tooltip").ToString(),
                      .FieldTypeValidationMessage = row("FieldTypeValidationMsg").ToString(),
                      .FieldType = row("FieldType").ToString(),
                      .MaximumLength = row("MaxLength").ToString(),
                      .IsMendatory = row("isMandatory").ToString(),
                      .MendatoryMessage = row("Mandatory_msg").ToString()
                    })
                Next
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getMenuList() As String
        Try

            Dim list As New List(Of DropdownModel)
            Dim ds As DataSet = common.GetMenuList()
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString(),
                      .Text = row("MenuName").ToString()
                    })
                Next
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getFormControls(form As String) As String
        Try

            Dim list As New List(Of CommonModel)
            Dim ds As DataSet = common.GetControlsByForm(form)
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New CommonModel() With {
                      .MenuName = row("MenuName").ToString(),
                      .ObjectName = row("Object_Name").ToString(),
                      .Label = row("Label").ToString(),
                      .ToolTip = row("Tooltip").ToString(),
                      .FieldTypeValidationMessage = row("FieldTypeValidationMsg").ToString(),
                      .FieldType = row("FieldType").ToString(),
                      .MaximumLength = row("MaxLength").ToString(),
                      .IsMendatory = row("isMandatory").ToString(),
                      .MendatoryMessage = row("Mandatory_msg").ToString(),
                      .FieldTypeValidMessage = row("FieldTypeValidationMsg").ToString()
                    })
                Next
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function UpdateControlContent(model As List(Of CommonModel)) As String
        Try
            Dim result As String = ""
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""

            'Dim jsSerializer As New JavaScriptSerializer()
            'Dim controlList = jsSerializer.Deserialize(Of List(Of CommonModel))(HttpContext.Current.Request.QueryString("control"))
            Dim objModel As New MessageModel
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Form Control", "isEdit", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim message = common.UpdateControlsContent(model, intSatatus, msg)
                objModel.Status = True
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function isAuthenticatedForDelete(ByVal form As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            Select Case form
                Case "BP"
                    form = "Bank Payment"
                Case "CP"
                    form = "Cash Payment"
                Case "BR"
                    form = "Bank Recipt"
                Case "CR"
                    form = "Cash Recipt"
            End Select
            If BizSoft.Utilities.HasRights(form, "IsDelete", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            Else
                objModel.Status = True
                isValidUser = True
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function isValidUserForDelete(ByVal username As String, ByVal password As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim ds = common.validUser(username, password)
            If ds.Tables(0).Rows.Count > 0 Then
                objModel.Status = True
            Else
                objModel.Status = False
                objModel.Message = "Incorrect username and password"
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
End Class