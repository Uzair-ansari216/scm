﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class CountryService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGrid() As String
        Try
            Dim list As New List(Of CountryModel)
            Dim objBankTitle As New BizSoft.Bridge_SCM.clsCountry()
            Dim ds As DataSet = objBankTitle.GetList()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New CountryModel() With {
                      .ID = row("ID").ToString(),
                      .Countrycode = row("countryCode").ToString(),
                      .Countryname = row("countryName").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function AddEditCountry(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Country", If(id = "0", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim country As New CountryModel
                Dim countryModel As New BizSoft.Bridge_SCM.clsCountry()
                If id <> "0" Then
                    Dim ds As DataSet = countryModel.GetById(id)
                    If ds.Tables(0).Rows.Count > 0 Then
                        country.ID = ds.Tables(0).Rows(0)("countryID").ToString
                        country.Countrycode = ds.Tables(0).Rows(0)("countryCode").ToString
                        country.Countryname = ds.Tables(0).Rows(0)("countryName").ToString
                    End If
                End If
                objModel.GenericList = country
                objModel.Status = True
            End If


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSave(model As CountryModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.clsCountry
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Long

            If model.ID = "" Then
                objModel.Message = obj.Add(0, model.Countrycode, model.Countryname, intSatatus, msg, NewID)
                objModel.ID = NewID
            Else
                objModel.Message = obj.Update(model.ID, model.Countrycode, model.Countryname, intSatatus, msg)
                objModel.ID = model.ID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDelete(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.clsCountry()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Integer

            If dataID <> "" Then
                objModel.Message = obj.Delete(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

End Class