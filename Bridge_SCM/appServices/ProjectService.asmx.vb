﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class ProjectService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGrid() As String
        Try
            Dim objModel As New MessageModel
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Projects", "isView", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to view this record"
                objModel.Status = False
                isValidUser = False
            End If

            Dim list As New List(Of ProjectModel)
            If isValidUser Then
                Dim objBankTitle As New BizSoft.Bridge_SCM.clsProject()
                Dim ds As DataSet = objBankTitle.GetList()

                If ds.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In ds.Tables(0).Rows
                        list.Add(New ProjectModel() With {
                          .ID = row("ProjectID").ToString(),
                          .ProjectTitle = row("ProjectTitle").ToString(),
                          .ProjectType = row("ProjectType").ToString(),
                          .ClientID = row("ClientID").ToString(),
                          .Company = row("Company").ToString(),
                          .StartDate = row("StartDate").ToString(),
                          .EndDate = row("EndDate").ToString(),
                          .DepartmentCode = row("DepartmentCode").ToString(),
                          .EstimatedDuration = row("EstimatedDuration").ToString(),
                          .EstimatedCost = row("EstimatedCost").ToString(),
                          .DebtorAccountCode = row("DebtorAccountCode").ToString(),
                          .DebtorOpBal = row("DebtorOpBal").ToString(),
                          .CreditorAccountCode = row("CreditorAccountCode").ToString(),
                          .CreditorOpBal = row("CreditorOpBal").ToString(),
                          .Description = row("Description").ToString()
                        })

                    Next
                End If
                objModel.GenericList = list
                objModel.Status = True
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function AddEditProject(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Projects", If(id = "0", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim project As New ProjectModel
                Dim projectModel As New BizSoft.Bridge_SCM.clsProject()
                If id <> "0" Then
                    Dim ds As DataSet = projectModel.GetById(id)
                    If ds.Tables(0).Rows.Count > 0 Then
                        project.ID = ds.Tables(0).Rows(0)("ProjectID").ToString()
                        project.ProjectTitle = ds.Tables(0).Rows(0)("ProjectTitle").ToString()
                        project.Description = ds.Tables(0).Rows(0)("Description").ToString()
                        project.ProjectType = ds.Tables(0).Rows(0)("ProjectType").ToString()
                        project.ClientID = ds.Tables(0).Rows(0)("ClientID").ToString()
                        project.StartDate = ds.Tables(0).Rows(0)("StartDate").ToString()
                        project.EndDate = ds.Tables(0).Rows(0)("EndDate").ToString()
                        project.DepartmentCode = ds.Tables(0).Rows(0)("DepartmentCode").ToString()
                        project.EstimatedDuration = ds.Tables(0).Rows(0)("EstimatedDuration").ToString()
                        project.EstimatedCost = ds.Tables(0).Rows(0)("EstimatedCost").ToString()
                        project.DebtorAccountCode = ds.Tables(0).Rows(0)("DebtorAccountCode").ToString()
                        project.DebtorOpBal = ds.Tables(0).Rows(0)("DebtorOpBal").ToString()
                        project.CreditorAccountCode = ds.Tables(0).Rows(0)("CreditorAccountCode").ToString()
                        project.CreditorOpBal = ds.Tables(0).Rows(0)("CreditorOpBal").ToString()
                        project.Company = ds.Tables(0).Rows(0)("CompanyID").ToString()
                    End If
                End If
                objModel.GenericList = project
                objModel.Status = True
            End If


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSave(model As ProjectModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.clsProject
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Long

            If model.ID = "" Then
                objModel.Message = obj.Add(0, model.ProjectTitle, model.ClientID, model.ProjectType, model.StartDate, model.EndDate, model.DepartmentCode, model.EstimatedDuration, model.EstimatedCost, model.DebtorAccountCode, model.DebtorOpBal, model.CreditorAccountCode, model.CreditorOpBal, model.Description, intSatatus, msg, NewID)
                objModel.ID = NewID
                objModel.Status = True
            Else
                objModel.Message = obj.Update(model.ID, model.ProjectTitle, model.ProjectType, model.ClientID, model.StartDate, model.EndDate, model.DepartmentCode, model.EstimatedDuration, model.EstimatedCost, model.DebtorAccountCode, model.DebtorOpBal, model.CreditorAccountCode, model.CreditorOpBal, model.Description, intSatatus, msg)
                objModel.ID = model.ID
                objModel.Status = True
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDelete(ByVal dataID As String, ByVal dataName As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Projects", "IsDelete", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim obj As New BizSoft.Bridge_SCM.clsProject()
                Dim intSatatus As Boolean = 0
                Dim msg As String = ""

                If dataID <> "" Then
                    objModel.Message = obj.Delete(dataID, dataName, intSatatus, msg)
                    objModel.ID = dataID
                    objModel.Status = True
                End If
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetClient() As String

        Dim obj As New BizSoft.Bridge_SCM.clsProject()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetListClient()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("CustomerID").ToString,
                      .Text = row("Company").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetProjecType() As String

        Dim obj As New BizSoft.Bridge_SCM.clsProject()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetListProjecType()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("projecttype").ToString,
                      .Text = row("projecttype").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetDepartmentCode() As String

        Dim obj As New BizSoft.Bridge_SCM.clsProject()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetListsDepartmentCode()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("DepartmentID").ToString,
                      .Text = row("DepartmentName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridDetailAccountCode() As String
        Try
            Dim list As New List(Of GVoucherModel)
            Dim obj As New BizSoft.Bridge_SCM.GVoucher()
            Dim ds As DataSet = obj.AccountCode()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New GVoucherModel() With {
                      .AccountName = row("AccountName").ToString(),
                      .AccountCode = row("AccountCode").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
End Class