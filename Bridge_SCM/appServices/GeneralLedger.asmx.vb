﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class GeneralLedger
    Inherits System.Web.Services.WebService
    Dim ser As New JavaScriptSerializer()
    ' GetCustomerList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetCustomerList() As String
        Dim objlocation As New BizSoft.Bridge_SCM.Dealer()
        Dim dtlocation As DataSet = objlocation.GetCompanyList(HttpContext.Current.Session("CompanyID"))

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtlocation.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(ddlList)
    End Function

    ' GetLocationList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Function GetLocationList() As String
        Dim objlocation As New BizSoft.Bridge_SCM.Supplier()
        Dim dtlocation As DataSet = objlocation.GetListLocation(HttpContext.Current.Session("CompanyID"))

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtlocation.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(ddlList)
    End Function

    ' GetDepartmentList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Function GetDepartmentList() As String
        Dim objDepartment As New BizSoft.Bridge_SCM.Department
        Dim dtDepartment As DataSet = objDepartment.GetList(HttpContext.Current.Session("CompanyID"))

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtDepartment.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(ddlList)
    End Function
    'GetEmployeeList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Function GetEmployeeList() As String
        Dim objEmployee As New BizSoft.Bridge_SCM.Employee()
        Dim dtEmployee As DataSet = objEmployee.GetListStatus(HttpContext.Current.Session("CompanyID"))

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtEmployee.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(ddlList)
    End Function
    ' GetSegmentList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Function GetSegmentList() As String
        Dim objSegment As New BizSoft.Bridge_SCM.GeneralLedger
        Dim dtSegment As DataSet = objSegment.GetListSegment(HttpContext.Current.Session("CompanyID"))

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtSegment.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(ddlList)
    End Function
    'GetProjectList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Function GetProjectList() As String
        Dim objProject As New BizSoft.Bridge_SCM.GeneralLedger
        Dim dtProject As DataSet = objProject.GetListproject(HttpContext.Current.Session("CompanyID"))

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtProject.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(ddlList)
    End Function

    ' GetvoucherTypeList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Function GetvoucherTypeList() As String
        Dim objvoucherType As New BizSoft.Bridge_SCM.GeneralLedger
        Dim dtvoucherType As DataSet = objvoucherType.GetListVoucherType

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtvoucherType.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(ddlList)
    End Function

    'GetDetailAccountList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Function GetDetailAccountList() As String
        Dim objDetailAccountList As New BizSoft.Bridge_SCM.GeneralLedger
        Dim dtDetailAccountList As DataSet = objDetailAccountList.GetListDetailAccountList(HttpContext.Current.Session("CompanyID"))

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtDetailAccountList.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(ddlList)
    End Function

    'GetReportList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Function GetReportList() As String
        Dim objReport As New BizSoft.Bridge_SCM.GeneralLedger
        Dim frmName As String = "mnuREPORTS_GLListofAcc"
        Dim dtReport As DataSet = objReport.GetReportDetails(frmName)

        Dim ddlList As New List(Of clsDropdownModel)

        For Each dr As DataRow In dtReport.Tables(0).Rows
            Dim ddl As New clsDropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(ddlList)
    End Function
End Class