﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services
Imports System.Data.SqlClient
Imports System.IO
Imports System.Drawing

<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class ReportSetupService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGrid() As String
        Try

            Dim list As New List(Of ReportSetupModel)
            Dim objBankTitle As New BizSoft.Bridge_SCM.ReportSetup()
            Dim ds As DataSet = objBankTitle.GetList()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New ReportSetupModel() With {
                      .ReportName = row("ReportName").ToString(),
                      .IsActive = row("IsActive").ToString(),
                      .ReportID = row("ReportID").ToString(),
                      .AutoCalling = row("AutoCalling").ToString(),
                      .IsSubReport = row("IsSubReport").ToString(),
                      .FKReportCategory = row("FKReportCategory").ToString(),
                      .ReportModule = row("Name").ToString(),
                      .ReportCategory = row("Description").ToString(),
                      .IsSelectionFormula = row("IsSelectionFormula").ToString(),
                      .FKReportModule = row("FKReportModule"),
                      .ReportConnectionType = row("ReportConnectionType").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSave(model As ReportSetupModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.ReportSetup
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Long

            If model.ID = "" Then
                objModel.Message = obj.Add(model, intSatatus, msg, NewID)
                objModel.ID = NewID
            Else
                objModel.Message = obj.Update(model, intSatatus, msg)
                objModel.ID = model.ID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDelete(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.ReportSetup()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Integer

            If dataID <> "" Then
                objModel.Message = obj.Delete(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FillReportModule() As String
        Dim obj As New BizSoft.Bridge_SCM.ReportSetup()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.FillReportModule()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Name").ToString
                      })
            Next
        End If
        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)

    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function ReportCategoryFill() As String
        Dim obj As New BizSoft.Bridge_SCM.ReportSetup()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.FillReportCategory()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Description").ToString
                      })
            Next
        End If
        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSaveDetail(model As ReportSetupModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.ReportSetup
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""

            If model.Mode = "Add" Then
                objModel.Message = obj.AddDetail(model, intSatatus, msg)
            Else
                'objModel.Message = obj.UpdateDetail(model, intSatatus, msg)
                'objModel.ID = model.ID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridDetail(id As String) As String
        Try
            Dim list As New List(Of ReportSetupModel)
            Dim obj As New BizSoft.Bridge_SCM.ReportSetup()
            Dim ds As DataSet = obj.GetDetail(id)

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New ReportSetupModel() With {
                      .ReportID = row("ReportID").ToString(),
                      .DataType = row("DataType").ToString(),
                      .ParameterName = row("ParameterName").ToString(),
                      .ObjectName = row("ObjectName").ToString(),
                      .Msg = row("Msg").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function DeleteReportDetail(ByVal dataID As String, objectName As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.ReportSetup()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Integer

            If dataID <> "" Then
                objModel.Message = obj.DeleteReportDetail(dataID, objectName, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getReportParameterByReportName(id As String) As String
        Try
            Dim list As New List(Of ReportSetupModel)
            Dim obj As New BizSoft.Bridge_SCM.ReportSetup()
            Dim ds As List(Of String) = obj.GetParameterByReportName(id)

            'If ds.Tables(0).Rows.Count > 0 Then
            '    For Each row As DataRow In ds.Tables(0).Rows
            '        list.Add(New ReportParameters() With {
            '          .Name = row("ReportID").ToString(),

            '          .Msg = row("Msg").ToString()
            '        })
            '    Next
            'End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(ds)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDeleteDetail(ByVal dataID As String, ByVal ParameterName As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.ReportSetup()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""

            If dataID <> "" Then
                objModel.Message = obj.DeleteDetail(dataID, ParameterName, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSaveDetail2(model As ReportSetupModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.ReportSetup
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim variables = ""
            If model.SessionVariable.Length > 0 Then
                variables = String.Join(",", model.SessionVariable)
            End If
            objModel.Message = obj.AddDetail2(model, variables, intSatatus, msg)


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getSessionVariables() As String
        Try
            Dim list As New List(Of DropdownModel)
            Dim obj As New BizSoft.Bridge_SCM.ReportSetup()
            Dim ds As DataSet = obj.GetSessionVariables()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString(),
                      .Text = row("SessionVariableName").ToString()
                     })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridDetail2(id As String) As String
        Try
            Dim list As New List(Of ReportSetupModel)
            Dim obj As New BizSoft.Bridge_SCM.ReportSetup()
            Dim ds As DataSet = obj.GetDetail2(id)

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New ReportSetupModel() With {
                      .ReportID = row("ReportID").ToString(),
                      .ObjectName = row("ObjectName").ToString(),
                      .ObjectType = row("ObjectType").ToString(),
                      .Query = row("Query").ToString(),
                      .Label = row("Label").ToString(),
                      .DefaultValue = row("DefaultValue").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDeleteDetail2(ByVal dataID As String, ByVal ObjectName As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.ReportSetup()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""

            If dataID <> "" Then
                objModel.Message = obj.DeleteDetail2(dataID, ObjectName, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
#Region "Selection Formula"

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridForSF(id As String) As String
        Try
            Dim list As New List(Of ReportSetupModel)
            Dim obj As New BizSoft.Bridge_SCM.ReportSetup()
            Dim ds As DataSet = obj.GetDetailForSF(id)

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New ReportSetupModel() With {
                      .ID = row("ID"),
                      .ReportID = row("ReportID").ToString(),
                      .ObjectName = row("ObjectName").ToString(),
                      .SelectionFormula = row("SelectionFormula").ToString()
                    })
                    '.Msg = row("Msg").ToString()
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function SaveSelectionFormula(model As ReportSetupModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.ReportSetup
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Long

            If model.ID = "" Then
                objModel.Message = obj.AddSelectionFormula(model, intSatatus, msg, NewID)
                objModel.ID = NewID
            Else
                'objModel.Message = obj.UpdateSelectionFormula(model, intSatatus, msg)
                'objModel.ID = model.ID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDeleteSelectionFormula(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.ReportSetup()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Integer

            If dataID <> "" Then
                objModel.Message = obj.DeleteSelectionFormula(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
#End Region

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridForSP(id As String) As String
        Try
            Dim list As New List(Of ReportSetupModel)
            Dim obj As New BizSoft.Bridge_SCM.ReportSetup()
            Dim ds As DataSet = obj.GetDetailForSP(id)

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New ReportSetupModel() With {
                      .ID = row("ID").ToString(),
                      .ReportID = row("ReportID").ToString(),
                      .StoreProcedure = row("SP_Name").ToString(),
                      .ExecutionOrder = row("Sorton").ToString()
                    })

                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function SaveStoreProcedure(model As ReportSetupModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.ReportSetup
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Long

            If model.ID = "" Then
                objModel.Message = obj.AddStoreProcedure(model, intSatatus, msg, NewID)
                objModel.ID = NewID
            Else
                'objModel.Message = obj.UpdateSelectionFormula(model, intSatatus, msg)
                'objModel.ID = model.ID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDeleteStoreProcedure(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.ReportSetup()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Integer

            If dataID <> "" Then
                objModel.Message = obj.DeleteStoreProcedure(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FillReportCategories(ID As Int32) As String
        Dim obj As New BizSoft.Bridge_SCM.Report
        Dim list As New List(Of DropdownModel)
        Dim remodule As New ReportModule
        Dim ds As DataSet = obj.FillCategories(ID)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim CategoryList = New List(Of DropdownModel)
            For Each row As DataRow In ds.Tables(0).Rows
                CategoryList.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Name").ToString
                      })
            Next
            remodule.DropDown = CategoryList

        End If

        'If ds.Tables(0).Rows.Count > 0 Then
        '    For Each row As DataRow In ds.Tables(0).Rows
        '        list.Add(New DropdownModel() With {
        '              .Value = row("ID").ToString,
        '              .Text = row("Name").ToString
        '              })
        '    Next
        'End If
        Dim reportPathDs As DataSet = obj.GetReportPathByModule(ID)
        remodule.ReportPath = reportPathDs.Tables(0).Rows(0)("ReportPath")
        Session("reportPath") = reportPathDs.Tables(0).Rows(0)("ReportPath")
        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(remodule)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Sub uploadImage()

        Dim str As String = ""
        If (HttpContext.Current.Session.Count > 0) Then
            If (HttpContext.Current.Request.Files.AllKeys.Any()) Then
                Dim httpPostedFile As HttpPostedFile = HttpContext.Current.Request.Files("UploadedImage")

                If Not IsNothing(httpPostedFile) Then
                    Dim fileSavePath As String = Path.Combine(HttpContext.Current.Server.MapPath("~/UploadedFiles"), httpPostedFile.FileName)
                    Dim reportPath = Session("reportPath")
                    Dim binaryReader As New BinaryReader(HttpContext.Current.Request.Files("UploadedImage").InputStream)
                    'fileData = binaryReader.ReadBytes(HttpContext.Current.Request.Files("UploadedImage").ContentLength)

                    'Folder
                    Dim myPhysPath As String = System.IO.Path.Combine(HttpContext.Current.Server.MapPath(reportPath), httpPostedFile.FileName)
                    HttpContext.Current.Request.Files("UploadedImage").SaveAs(myPhysPath)
                    'End

                    str = "success"
                End If
            End If
        End If
    End Sub
End Class