﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class AssetBookService
    Inherits System.Web.Services.WebService
    Dim assetBookRepository As New BizSoft.Bridge_SCM.clsAssetBook()
    Dim dataSet As DataSet
    Dim ser As New JavaScriptSerializer()
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getAllAssetBook() As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            Dim assetBookList As New List(Of AssetBookModel)
            dataSet = assetBookRepository.GetAssetBooks()
            If dataSet.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In dataSet.Tables(0).Rows
                    assetBookList.Add(New AssetBookModel() With {
                          .Id = Convert.ToInt32(row("ID")),
                          .Title = row("BooK_Title").ToString,
                          .Description = row("booK_Decs").ToString,
                          .IsDefault = Convert.ToInt16(row("Is_Default"))
                          })
                Next
            End If
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(assetBookList)
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function AddEditAssetBook(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Asset Book", If(id = "0", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim assetBook As New AssetBookModel
                If id <> "0" Then
                    Dim ds As DataSet = assetBookRepository.GetById(id)
                    If ds.Tables(0).Rows.Count > 0 Then
                        assetBook.Id = Convert.ToInt32(ds.Tables(0).Rows(0)("ID"))
                        assetBook.Title = ds.Tables(0).Rows(0)("Book_Title").ToString()
                        assetBook.Description = ds.Tables(0).Rows(0)("book_Decs").ToString()
                        assetBook.IsDefault = Convert.ToInt16(ds.Tables(0).Rows(0)("Is_Default"))
                    End If
                End If
                objModel.GenericList = assetBook
                objModel.Status = True
            End If

            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function saveAssetBook(assetBookVm As AssetBookModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim status As Boolean = True
            Dim message As String = ""
            Dim newID As Long

            If assetBookVm.Id > 0 Then
                objModel.Message = assetBookRepository.Update(assetBookVm, status, message)
                objModel.ID = assetBookVm.Id
                objModel.Status = status
            Else
                objModel.Message = assetBookRepository.Add(assetBookVm, status, message, newID)
                objModel.ID = newID
                objModel.Status = status
            End If

            Dim assetBookList As New List(Of AssetBookModel)
            dataSet = assetBookRepository.GetAssetBooks()
            If dataSet.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In dataSet.Tables(0).Rows
                    assetBookList.Add(New AssetBookModel() With {
                          .Id = Convert.ToInt32(row("ID")),
                          .Title = row("BooK_Title").ToString,
                          .Description = row("booK_Decs").ToString,
                          .IsDefault = Convert.ToInt16(row("Is_Default"))
                          })
                Next
            End If

            objModel.GenericList = assetBookList
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function deleteAssetBook(ByVal id As Int32, ByVal title As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim status As Boolean = True
            Dim message As String = ""

            objModel.Message = assetBookRepository.Delete(id, title, status, message)
            objModel.ID = id
            objModel.Status = status

            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
End Class