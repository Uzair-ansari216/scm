﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class ReportRoleDefineService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGrid() As String
        Try
            Dim list As New List(Of ReportRoleDefineModel)
            Dim objBankTitle As New BizSoft.Bridge_SCM.ReportRoleDefine()
            Dim ds As DataSet = objBankTitle.GetList()
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New ReportRoleDefineModel() With {
                          .ReportModule = row("Module").ToString(),
                          .ReportCategory = row("Category").ToString(),
                          .ReportID = row("ID").ToString(),
                          .ReportName = row("Report").ToString()
                        })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function DeleteDetail(ID As String) As String
        Dim objModel As New MessageModel

        Dim obj As New BizSoft.Bridge_SCM.ReportRoleDefine

        obj.DeleteDetail(ID)

        Dim iAction As String = ""
        iAction = "Delete Recored Role Detail"
        BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Report Role Detail", "tbl_Report_Role_Detail", "", Environment.MachineName)


        Return "success"
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function SaveDetail(ID As String, ReportID As String, isView As Boolean) As String
        Dim objModel As New MessageModel
        Dim obj As New BizSoft.Bridge_SCM.ReportRoleDefine

        Dim intSatatus As Boolean = 0
        Dim strMsg As String = ""

        obj.AddDetail(ID, ReportID, isView)

        Dim iAction As String = ""
        iAction = "Add new Recored Report Role Detail"
        BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Report Role Define", "tbl_Report_Role_Detail", "", Environment.MachineName)

        If intSatatus Then
            objModel.Message = "Record has been saved successfully!"
        End If
        objModel.Message = strMsg

        Return objModel.Message
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetGridList() As String
        Try
            Dim list As New List(Of DropdownModel)
            Dim obj As New BizSoft.Bridge_SCM.ReportRoleDefine
            Dim ds As DataSet = obj.GetListView

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New DropdownModel() With {
                      .Value = row("RoleID").ToString(),
                      .Text = row("RoleName").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try

    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetDetailsRole(ID As String) As String
        Dim list As New List(Of ReportRoleDefineModel)
        Dim obj As New BizSoft.Bridge_SCM.ReportRoleDefine
        Dim ds As DataSet = obj.GetRoleDetail(ID)

        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New ReportRoleDefineModel() With {
                  .ReportID = row("ReportID").ToString,
                  .isView = row("isView").ToString
                  })
            Next
        End If



        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
End Class