﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class CityService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGrid() As String
        Try
            Dim list As New List(Of CityModel)
            Dim objBankTitle As New BizSoft.Bridge_SCM.clsCity()
            Dim ds As DataSet = objBankTitle.GetList()
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New CityModel() With {
                      .ID = row("cityID").ToString(),
                      .cityCode = row("cityCode").ToString(),
                      .cityName = row("cityName").ToString(),
                      .countryID = row("countryID").ToString(),
                      .countryName = row("countryName").ToString()
                    })

                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function AddEditCity(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("City", If(id = "0", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim city As New CityModel
                Dim cityModel As New BizSoft.Bridge_SCM.clsCity()
                If id <> "0" Then
                    Dim ds As DataSet = cityModel.GetById(id)
                    If ds.Tables(0).Rows.Count > 0 Then
                        city.ID = ds.Tables(0).Rows(0)("cityID").ToString
                        city.cityCode = ds.Tables(0).Rows(0)("cityCode").ToString
                        city.cityName = ds.Tables(0).Rows(0)("cityName").ToString
                        city.countryID = ds.Tables(0).Rows(0)("countryID").ToString
                    End If
                End If
                objModel.GenericList = city
                objModel.Status = True
            End If


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSave(model As CityModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.clsCity
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Long

            If model.ID = "" Then
                objModel.Message = obj.Add(0, model.cityCode, model.cityName, model.countryID, intSatatus, msg, NewID)
                objModel.ID = NewID
            Else
                objModel.Message = obj.Update(model.ID, model.cityCode, model.cityName, model.countryID, intSatatus, msg)
                objModel.ID = model.ID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDelete(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.clsCity()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Integer

            If dataID <> "" Then
                objModel.Message = obj.Delete(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetCountryName() As String

        Dim obj As New BizSoft.Bridge_SCM.clsCity()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetList2()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("countryName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
End Class