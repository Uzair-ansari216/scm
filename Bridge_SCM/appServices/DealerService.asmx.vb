﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class DealerService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function loadGrid() As String
        Try
            Dim list As New List(Of DealerModel)
            Dim objBankTitle As New BizSoft.Bridge_SCM.Dealer()
            Dim ds As DataSet = objBankTitle.GetList()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New DealerModel() With { _
                      .ID = row("CustomerID").ToString(), _
                      .Company = row("Company").ToString(), _
                      .FirstName = row("FirstName").ToString(), _
                      .LastName = row("LastName").ToString(), _
                      .Address = row("Address").ToString(), _
                      .DebtorAccountCode = row("DebtorAccountCode").ToString(), _
                      .CreditorAccountCode = row("CreditorAccountCode").ToString(), _
                      .countryName = row("countryID").ToString(), _
                      .cityName = row("cityID").ToString(), _
                      .Location = row("LocationID").ToString(), _
                      .Phone = row("Phone").ToString(), _
                      .Email = row("Email").ToString(), _
                      .CBillingName = row("CBillingName").ToString(), _
                      .Fax = row("Fax").ToString(), _
                      .DebtorOpBal = row("DebtorOpBal").ToString(), _
                      .CreditorOpBal = row("CreditorOpBal").ToString(), _
                      .NTN = row("NTN").ToString(), _
                      .STN = row("STN").ToString(), _
                      .CellNo1 = row("CellNo1").ToString(), _
                      .CellNo2 = row("CellNo2").ToString(), _
                      .Email1 = row("Email1").ToString(), _
                      .Email2 = row("Email2").ToString(), _
                      .CustomerTypeID = row("CustomerTypeID").ToString(), _
                      .Terms = row("Terms").ToString(), _
                      .CreditLimit = row("CreditLimit").ToString(), _
                      .CustomerLogin = row("CustomerLogin").ToString(), _
                      .CustomerGroupID = row("CustomerGroupID").ToString(), _
                      .EmployeeID = row("EmployeeID").ToString(), _
                      .FK_RateMaster = row("FK_RateMaster").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function AddEditCustomer(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Customer", If(id = "0", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim dealer As New DealerModel
                Dim dealerModel As New BizSoft.Bridge_SCM.Dealer()
                If id <> "0" Then
                    Dim ds As DataSet = dealerModel.GetById(id)
                    If ds.Tables(0).Rows.Count > 0 Then
                        dealer.ID = ds.Tables(0).Rows(0)("CustomerID").ToString()
                        dealer.Company = ds.Tables(0).Rows(0)("Company").ToString()
                        dealer.FirstName = ds.Tables(0).Rows(0)("FirstName").ToString()
                        dealer.LastName = ds.Tables(0).Rows(0)("LastName").ToString()
                        dealer.Address = ds.Tables(0).Rows(0)("Address").ToString()
                        dealer.DebtorAccountCode = ds.Tables(0).Rows(0)("DebtorAccountCode").ToString()
                        dealer.CreditorAccountCode = ds.Tables(0).Rows(0)("CreditorAccountCode").ToString()
                        dealer.countryName = ds.Tables(0).Rows(0)("countryID").ToString()
                        dealer.cityName = ds.Tables(0).Rows(0)("cityID").ToString()
                        dealer.Location = ds.Tables(0).Rows(0)("LocationID").ToString()
                        dealer.Phone = ds.Tables(0).Rows(0)("Phone").ToString()
                        dealer.Email = ds.Tables(0).Rows(0)("Email").ToString()
                        dealer.CBillingName = ds.Tables(0).Rows(0)("CBillingName").ToString()
                        dealer.Fax = ds.Tables(0).Rows(0)("Fax").ToString()
                        dealer.DebtorOpBal = ds.Tables(0).Rows(0)("DebtorOpBal").ToString()
                        dealer.CreditorOpBal = ds.Tables(0).Rows(0)("CreditorOpBal").ToString()
                        dealer.NTN = ds.Tables(0).Rows(0)("NTN").ToString()
                        dealer.STN = ds.Tables(0).Rows(0)("STN").ToString()
                        dealer.CellNo1 = ds.Tables(0).Rows(0)("CellNo1").ToString()
                        dealer.CellNo2 = ds.Tables(0).Rows(0)("CellNo2").ToString()
                        dealer.Email1 = ds.Tables(0).Rows(0)("Email1").ToString()
                        dealer.Email2 = ds.Tables(0).Rows(0)("Email2").ToString()
                        dealer.CustomerTypeID = ds.Tables(0).Rows(0)("CustomerTypeID").ToString()
                        dealer.Terms = ds.Tables(0).Rows(0)("Terms").ToString()
                        dealer.CreditLimit = ds.Tables(0).Rows(0)("CreditLimit").ToString()
                        dealer.CustomerLogin = ds.Tables(0).Rows(0)("CustomerLogin").ToString()
                        dealer.CustomerGroupID = ds.Tables(0).Rows(0)("CustomerGroupID").ToString()
                        dealer.EmployeeID = ds.Tables(0).Rows(0)("EmployeeID").ToString()
                        dealer.FK_RateMaster = ds.Tables(0).Rows(0)("FK_RateMaster").ToString()
                    End If
                End If
                objModel.GenericList = dealer
                objModel.Status = True
            End If


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function OnSave(model As DealerModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.Dealer
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""

            If model.ID = "" Then
                objModel.Message = obj.Add(model, intSatatus, msg)
                objModel.ID = 0
            Else
                objModel.Message = obj.Update(model, intSatatus, msg)
                objModel.ID = model.ID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function OnDelete(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.Dealer()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""

            If dataID <> "" Then
                objModel.Message = obj.Delete(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetCustomerGroupList() As String

        Dim obj As New BizSoft.Bridge_SCM.Dealer()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetCustomerGroup()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("ID").ToString, _
                      .Text = row("CustomerGroup").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetLocationList() As String

        Dim obj As New BizSoft.Bridge_SCM.clsLogin()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.FillLocation()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("ID").ToString, _
                      .Text = row("Name").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetCityList() As String

        Dim obj As New BizSoft.Bridge_SCM.Dealer()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetCity()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("cityID").ToString, _
                      .Text = row("cityName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetCountryList() As String

        Dim obj As New BizSoft.Bridge_SCM.Dealer()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetCountry()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("countryID").ToString, _
                      .Text = row("countryName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetSalesPersonList() As String

        Dim obj As New BizSoft.Bridge_SCM.Dealer()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetSalesPerson()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("EmployeeID").ToString, _
                      .Text = row("FirstName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetPricingList() As String

        Dim obj As New BizSoft.Bridge_SCM.Dealer()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetPricing()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("ID").ToString, _
                      .Text = row("Description").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetCustomerTypeList() As String

        Dim obj As New BizSoft.Bridge_SCM.Dealer()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetCustomerType()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("CustomerTypeID").ToString, _
                      .Text = row("CustomerType").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetCategoryList() As String

        Dim obj As New BizSoft.Bridge_SCM.Dealer()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetCategory()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("ID").ToString, _
                      .Text = row("Description").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function loadGridDetail() As String
        Try
            Dim list As New List(Of DealerModel)
            Dim obj As New BizSoft.Bridge_SCM.Dealer()
            Dim ds As DataSet = obj.GetDetail()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New DealerModel() With { _
                      .ID = row("CustomerID").ToString(), _
                      .Company = row("Company").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function FindData(ID As String) As String
        Try
            Dim list As New List(Of DealerModel)
            Dim obj As New BizSoft.Bridge_SCM.Dealer()
            Dim ds As DataSet = obj.FindData(ID)

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New DealerModel() With { _
                      .ID = row("CustomerID").ToString(), _
                      .Company = row("Company").ToString(), _
                      .FirstName = row("FirstName").ToString(), _
                      .Address = row("Address").ToString(), _
                      .DebtorAccountCode = row("DebtorAccountCode").ToString(), _
                      .CreditorAccountCode = row("CreditorAccountCode").ToString(), _
                      .countryName = row("countryID").ToString(), _
                      .cityName = row("cityID").ToString(), _
                      .Location = row("LocationID").ToString(), _
                      .Phone = row("Phone").ToString(), _
                      .Email = row("Email").ToString(), _
                      .CBillingName = row("CBillingName").ToString(), _
                      .Fax = row("Fax").ToString(), _
                      .DebtorOpBal = row("DebtorOpBal").ToString(), _
                      .CreditorOpBal = row("CreditorOpBal").ToString(), _
                      .NTN = row("NTN").ToString(), _
                      .STN = row("STN").ToString(), _
                      .CellNo1 = row("CellNo1").ToString(), _
                      .CellNo2 = row("CellNo2").ToString(), _
                      .Email1 = row("Email1").ToString(), _
                      .Email2 = row("Email2").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
End Class