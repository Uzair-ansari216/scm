﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class SupplierService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGrid() As String
        Try
            Dim list As New List(Of SupplierModel)
            Dim objBankTitle As New BizSoft.Bridge_SCM.Supplier()
            Dim ds As DataSet = objBankTitle.GetList()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New SupplierModel() With {
                      .SupplierID = row("SupplierID").ToString(),
                      .Company = row("Company").ToString(),
                      .Address = row("Address").ToString(),
                      .Contact = row("Contact").ToString(),
                      .ContactPhone = row("ContactPhone").ToString(),
                      .Phone = row("Phone").ToString(),
                      .Fax = row("Fax").ToString(),
                      .Email = row("Email").ToString(),
                      .AltContact = row("AltContact").ToString(),
                      .AltPhone = row("AltPhone").ToString(),
                      .Terms = row("Terms").ToString(),
                      .CreditLimit = row("CreditLimit").ToString(),
                      .Notes = row("Notes").ToString(),
                      .LocationID = row("LocationID").ToString(),
                      .DebtorAccountCode = row("DebtorAccountCode").ToString(),
                      .DebtorOpBal = row("DebtorOpBal").ToString(),
                      .CreditorAccountCode = row("CreditorAccountCode").ToString(),
                      .CreditorOpBal = row("CreditorOpBal").ToString(),
                      .NTN = row("NTN").ToString(),
                      .STN = row("STN").ToString(),
                      .cityID = row("cityID").ToString(),
                      .countryID = row("countryID").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function AddEditSupplier(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Supplier", If(id = "0", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim supplier As New SupplierModel
                Dim supplierModel As New BizSoft.Bridge_SCM.Supplier()
                If id <> "0" Then
                    Dim ds As DataSet = supplierModel.GetById(id)
                    If ds.Tables(0).Rows.Count > 0 Then
                        supplier.SupplierID = ds.Tables(0).Rows(0)("SupplierID").ToString()
                        supplier.Company = ds.Tables(0).Rows(0)("Company").ToString()
                        supplier.Address = ds.Tables(0).Rows(0)("Address").ToString()
                        supplier.Contact = ds.Tables(0).Rows(0)("Contact").ToString()
                        supplier.ContactPhone = ds.Tables(0).Rows(0)("ContactPhone").ToString()
                        supplier.Phone = ds.Tables(0).Rows(0)("Phone").ToString()
                        supplier.Fax = ds.Tables(0).Rows(0)("Fax").ToString()
                        supplier.Email = ds.Tables(0).Rows(0)("Email").ToString()
                        supplier.AltContact = ds.Tables(0).Rows(0)("AltContact").ToString()
                        supplier.AltPhone = ds.Tables(0).Rows(0)("AltPhone").ToString()
                        supplier.Terms = ds.Tables(0).Rows(0)("Terms").ToString()
                        supplier.CreditLimit = ds.Tables(0).Rows(0)("CreditLimit").ToString()
                        supplier.Notes = ds.Tables(0).Rows(0)("Notes").ToString()
                        supplier.LocationID = ds.Tables(0).Rows(0)("LocationID").ToString()
                        supplier.DebtorAccountCode = ds.Tables(0).Rows(0)("DebtorAccountCode").ToString()
                        supplier.DebtorOpBal = ds.Tables(0).Rows(0)("DebtorOpBal").ToString()
                        supplier.CreditorAccountCode = ds.Tables(0).Rows(0)("CreditorAccountCode").ToString()
                        supplier.CreditorOpBal = ds.Tables(0).Rows(0)("CreditorOpBal").ToString()
                        supplier.NTN = ds.Tables(0).Rows(0)("NTN").ToString()
                        supplier.STN = ds.Tables(0).Rows(0)("STN").ToString()
                        supplier.cityID = ds.Tables(0).Rows(0)("cityID").ToString()
                        supplier.countryID = ds.Tables(0).Rows(0)("countryID").ToString()
                    End If
                End If
                objModel.GenericList = supplier
                objModel.Status = True
            End If


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSave(model As SupplierModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.Supplier
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Long

            If model.Mode = "Add" Then
                objModel.Message = obj.Add(model, intSatatus, msg, NewID)
                objModel.ID = NewID
            ElseIf model.Mode = "Edit" Then
                objModel.Message = obj.Update(model, intSatatus, msg)
                objModel.ID = model.SupplierID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDelete(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.Supplier()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Integer

            If dataID <> "" Then
                objModel.Message = obj.Delete(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetCountryList() As String

        Dim obj As New BizSoft.Bridge_SCM.Supplier()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetCountry()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Name").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetLocationList() As String

        Dim obj As New BizSoft.Bridge_SCM.clsLogin()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.FillLocation()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                     .Value = row("ID").ToString,
                     .Text = row("Name").ToString
                     })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetCityList() As String

        Dim obj As New BizSoft.Bridge_SCM.Supplier()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetCity()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                     .Value = row("ID").ToString,
                     .Text = row("Name").ToString
                     })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
End Class