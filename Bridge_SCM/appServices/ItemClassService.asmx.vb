﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class ItemClassService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function loadGrid() As String
        Try
            Dim list As New List(Of ItemClassModel)
            Dim objBankTitle As New BizSoft.Bridge_SCM.clsItemClass()
            Dim ds As DataSet = objBankTitle.GetList()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    If row("isActive") = True And row("isInvoice") = True Then
                        list.Add(New ItemClassModel() With { _
                          .ID = row("PK_ItemClass").ToString(), _
                          .ItemClass = row("ItemClass").ToString(), _
                          .ClassGroup = row("ClassGroup").ToString(), _
                          .Fk_ClassGroup = row("FK_ClassGroup").ToString(), _
                          .Description = row("Description").ToString(), _
                          .isInvoice = "True", _
                          .isActive = "True"
                        })
                    ElseIf row("isActive") = False And row("isInvoice") = False Then
                        list.Add(New ItemClassModel() With { _
                         .ID = row("PK_ItemClass").ToString(), _
                         .ItemClass = row("ItemClass").ToString(), _
                         .ClassGroup = row("ClassGroup").ToString(), _
                         .Fk_ClassGroup = row("FK_ClassGroup").ToString(), _
                         .Description = row("Description").ToString(), _
                         .isInvoice = "False", _
                         .isActive = "False"
                       })
                    ElseIf row("isActive") = True And row("isInvoice") = False Then
                        list.Add(New ItemClassModel() With { _
                         .ID = row("PK_ItemClass").ToString(), _
                         .ItemClass = row("ItemClass").ToString(), _
                         .ClassGroup = row("ClassGroup").ToString(), _
                         .Fk_ClassGroup = row("FK_ClassGroup").ToString(), _
                         .Description = row("Description").ToString(), _
                         .isInvoice = "False", _
                         .isActive = "True"
                       })
                    ElseIf row("isActive") = False And row("isInvoice") = True Then
                        list.Add(New ItemClassModel() With { _
                         .ID = row("PK_ItemClass").ToString(), _
                         .ItemClass = row("ItemClass").ToString(), _
                         .ClassGroup = row("ClassGroup").ToString(), _
                         .Fk_ClassGroup = row("FK_ClassGroup").ToString(), _
                         .Description = row("Description").ToString(), _
                         .isInvoice = "True", _
                         .isActive = "False"
                       })
                    End If
                   
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function OnSave(model As ItemClassModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.clsItemClass
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Long

            If model.ID = "" Then
                objModel.Message = obj.Add(0, model.ItemClass, model.Description, model.Fk_ClassGroup, model.isActive, model.isInvoice, intSatatus, msg, NewID)
                objModel.ID = NewID
            Else
                objModel.Message = obj.Update(model.ID, model.ItemClass, model.Description, model.Fk_ClassGroup, model.isActive, model.isInvoice, intSatatus, msg)
                objModel.ID = model.ID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function OnDelete(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.clsItemClass()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Integer

            If dataID <> "" Then
                objModel.Message = obj.Delete(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    '    <System.Web.Services.WebMethod(EnableSession:=True)> _
    '<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    '    Public Function GetResourceType() As String

    '        Dim obj As New BizSoft.Bridge_SCM.clsClassGroup()
    '        Dim list As New List(Of DropdownModel)
    '        Dim ds As DataSet = obj.GetList()
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            For Each row As DataRow In ds.Tables(0).Rows
    '                list.Add(New DropdownModel() With { _
    '                      .Value = row("ID").ToString, _
    '                      .Text = row("Name").ToString
    '                      })
    '            Next
    '        End If

    '        Dim ser As New JavaScriptSerializer()
    '        ser.MaxJsonLength = Integer.MaxValue
    '        Return ser.Serialize(list)
    '    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetClassGroup() As String

        Dim obj As New BizSoft.Bridge_SCM.clsItemClass()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetGroupList()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("PK_ClassGroup").ToString, _
                      .Text = row("ClassGroup").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function

End Class