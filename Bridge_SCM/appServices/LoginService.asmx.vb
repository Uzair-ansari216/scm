﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization

<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class LoginService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function userLogin(ByVal model As LoginModel) As String
        Try
            Dim isLogin As Boolean = False
            Dim obj As New BizSoft.Bridge_SCM.clsLogin()
            'isLogin = obj.Login(model)

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(isLogin)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    Public Function Check(ByVal model As LoginModel) As String
        Try
            Dim isLogin As Boolean = False
            Dim obj As New BizSoft.Bridge_SCM.clsLogin()
            'isLogin = obj.Login(model)

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(isLogin)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetCompanyList(ByVal model As LoginModel) As String

        Dim obj As New BizSoft.Bridge_SCM.clsLogin()
        Dim list As New List(Of DropdownModel)


        Dim ds As DataSet = obj.GetCompany(model.User)
        If IsNothing(ds) Then
        Else
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New DropdownModel() With {
                          .Value = row("CompanyID").ToString,
                          .Text = row("CompanyName").ToString
                          })
                Next
            End If
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetLocationList(ByVal model As LoginModel) As String

        Dim obj As New BizSoft.Bridge_SCM.clsLogin()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetLocation(model.User)
        If IsNothing(ds) Then
        Else
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New DropdownModel() With {
                          .Value = row("ID").ToString,
                          .Text = row("Name").ToString
                          })
                Next
            End If
        End If
        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetLocationListByCompany(ByVal company As String, user As String) As String

        Dim obj As New BizSoft.Bridge_SCM.clsLogin()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetLocationByCompanyId(company, user)
        If IsNothing(ds) Then
        Else
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New DropdownModel() With {
                          .Value = row("ID").ToString,
                          .Text = row("Name").ToString
                          })
                Next
            End If
        End If
        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function Login(ByVal model As LoginModel) As String
        Try
            Dim objLogin As New BizSoft.Bridge_SCM.clsLogin()
            Dim blnStatus As Boolean = 0
            Dim strMsg As String = ""
            Dim isLogin As String
            Dim strArr As String() = New String(2) {}
            Dim appService As New AppServicesModel
            With appService
                .DeveloperMode = True
                .ToolTip = True
                .Training = True
                .VoiceMessage = True
            End With

            'strArr(0) = "Red"
            'strArr(1) = "Blue"
            'strArr(2) = "Green"


            'If cmbLocation.SelectedValue = -1 Then
            '    lblAllError.Text = "Location must be Selected..."
            '    Exit Function
            'End If

            'If cmbCompany.SelectedValue = -1 Then
            '    lblAllError.Text = "Company must be Selected..."
            '    Exit Function
            'End If

            Dim dr As DataSet = objLogin.Login(model.User, model.Password, blnStatus, strMsg)

            If blnStatus = False Then
                blnStatus = False
                strArr(0) = "Invalid UserName or Password"
                isLogin = "Invalid UserName or Password"
                'lblAllError.Text = "Invalid UserName or Password"
                Exit Function
            End If
            If dr.Tables(0).Rows.Count > 0 Then
                strArr(0) = "Welcome"
                strArr(1) = "Forms/Admin/Home.aspx"
                isLogin = "Welcome"
                blnStatus = True
                Session("UserID") = Convert.ToString(dr.Tables(0).Rows(0)("UserID"))
                Session("UserName") = Convert.ToString(dr.Tables(0).Rows(0)("UserName"))
                Session("CompanyID") = Convert.ToString(dr.Tables(0).Rows(0)("CompanyID"))
                Session("CompanyName") = Convert.ToString(dr.Tables(0).Rows(0)("CompanyName"))
                Session("isPowerUser") = Convert.ToString(dr.Tables(0).Rows(0)("isPowerUser"))
                Session("StatusID") = Convert.ToString(dr.Tables(0).Rows(0)("StatusID"))
                Session("LocationId") = Convert.ToString(dr.Tables(0).Rows(0)("LocationId"))
                Session("LocationName") = objLogin.GetLocationNameById(dr.Tables(0).Rows(0)("LocationId"))
                Session("WorkingDate") = model.WorkingDate
                Session("DeveloperMode") = appService.DeveloperMode
                Session("ToolTip") = appService.ToolTip
                Session("Training") = appService.Training
                Session("VoiceMessage") = appService.VoiceMessage
                Session("AccessRights") = objLogin.GetAllaccessbyUser(Session("UserID"))

                objLogin.GetFY(model.FinancialYear)

                Dim financialYear = HttpContext.Current.Session("yearTitle").ToString().Split("-")
                If Not (Convert.ToInt32(model.WorkingDate.ToString().Split("-")(0)) >= Convert.ToInt32(financialYear(0))) Or Not (Convert.ToInt32(model.WorkingDate.ToString().Split("-")(0)) <= Convert.ToInt32(financialYear(1))) Then
                    strArr(0) = "Working Date should be in between Financial year"
                Else
                    If Convert.ToBoolean(Session("isPowerUser")) = True Then
                        Session("LocationId") = model.Location
                        'Session("LocationName") = cmbLocation.SelectedItem.Text.ToString()
                        Session("CompanyID") = model.Company
                    Else
                        Session("LocationId") = Convert.ToString(dr.Tables(0).Rows(0)("LocationID"))
                        'Session("LocationName") = Convert.ToString(dr.Tables(0).Rows(0)("Location"))
                        Session("CompanyID") = Convert.ToString(dr.Tables(0).Rows(0)("CompanyID"))
                    End If
                    Dim iAction As String = ""
                    iAction = "User Logged in"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Default", "Users", "", Environment.MachineName)
                    'drNew = objLogin.LoginNew(txtCode.Text, txtpasswd.Text, blnStatus, strMsg)
                End If
            Else
                blnStatus = False
                strArr(0) = "Invalid UserName or Password"
                isLogin = "Invalid UserName or Password"
                'lblAllError.Text = "Invalid UserName or Password"
            End If
            If blnStatus Then
                BizSoft.Utilities.SetUserSession(dr.Tables(0).Rows(0))
                Dim objSetAccessRight As New BizSoft.Bridge_SCM.AccessRight()
                Dim ODSrght As New DataSet

                '  objSetAccessRight.GetAllaccessbyUser(Session("UserID"), ODSrght)
                ' Session("AccessRights") = ODSrght.Tables(0)

                'Response.Redirect("Forms/Admin/Home.aspx")
            Else
                'lblAllError.Text = strMsg
            End If
            Dim arrlist As New List(Of String)(strArr)
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(arrlist)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function changePassword(ByVal currentPassword As String, ByVal newPassword As String) As String
        Try
            Dim messages As String = ""
            Dim status As Boolean = True
            Dim NewID As Long
            Dim obj As New BizSoft.Bridge_SCM.clsLogin()
            Dim ds = obj.isOldPassword(currentPassword, newPassword, status, messages, NewID)

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(ds)
        Catch ex As Exception

        End Try
    End Function
End Class