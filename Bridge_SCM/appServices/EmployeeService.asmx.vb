﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class EmployeeService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGrid() As String
        Try
            Dim objModel As New MessageModel
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Employee", "isView", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to view this record"
                objModel.Status = False
                isValidUser = False
            End If

            Dim list As New List(Of EmployeeModel)
            If isValidUser Then
                Dim objBankTitle As New BizSoft.Bridge_SCM.Employee()
                Dim ds As DataSet = objBankTitle.GetList()

                If ds.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In ds.Tables(0).Rows
                        list.Add(New EmployeeModel() With {
                      .ID = row("EmployeeID").ToString(),
                      .FirstName = row("FirstName").ToString(),
                      .Gender = row("Gender").ToString(),
                      .DOB = row("DOB").ToString(),
                      .Address = row("Address").ToString(),
                      .Phone = row("Phone").ToString(),
                      .Fax = row("Fax").ToString(),
                      .cityID = row("cityID").ToString(),
                      .ContactReference1 = row("ContactReference1").ToString(),
                      .ContactReference2 = row("ContactReference2").ToString(),
                      .JoiningDate = row("JoiningDate").ToString(),
                      .EndDate = row("EndDate").ToString(),
                      .StatusID = row("StatusID").ToString(),
                      .Nationality = row("Nationality").ToString(),
                      .NIC = row("NIC").ToString(),
                      .CountriesTravelled = row("CountriesTravelled").ToString(),
                      .LanguagesSpoken = row("LanguagesSpoken").ToString(),
                      .LanguagesWritten = row("LanguagesWritten").ToString(),
                      .MaritalStatus = row("MaritalStatus").ToString(),
                      .PassportNo = row("PassportNo").ToString(),
                      .LocationID = row("LocationID").ToString(),
                      .DesignationID = row("DesignationID").ToString(),
                      .DepartmentID = row("DepartmentID").ToString(),
                      .CellNo = row("CellNo").ToString()
                    })
                    Next
                End If
                objModel.GenericList = list
                objModel.Status = True
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function AddEditEmployee(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Employee", If(id = "0", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim employee As New EmployeeModel
                Dim employeeModel As New BizSoft.Bridge_SCM.Employee()
                If id <> "0" Then
                    Dim ds As DataSet = employeeModel.GetById(id)
                    If ds.Tables(0).Rows.Count > 0 Then
                        employee.ID = ds.Tables(0).Rows(0)("EmployeeID").ToString()
                        employee.FirstName = ds.Tables(0).Rows(0)("FirstName").ToString()
                        employee.Gender = ds.Tables(0).Rows(0)("Gender").ToString()
                        employee.DOB = ds.Tables(0).Rows(0)("DOB").ToString()
                        employee.Address = ds.Tables(0).Rows(0)("Address").ToString()
                        employee.Phone = ds.Tables(0).Rows(0)("Phone").ToString()
                        employee.Fax = ds.Tables(0).Rows(0)("Fax").ToString()
                        employee.cityID = ds.Tables(0).Rows(0)("cityID").ToString()
                        employee.ContactReference1 = ds.Tables(0).Rows(0)("ContactReference1").ToString()
                        employee.ContactReference2 = ds.Tables(0).Rows(0)("ContactReference2").ToString()
                        employee.JoiningDate = ds.Tables(0).Rows(0)("JoiningDate").ToString()
                        employee.EndDate = ds.Tables(0).Rows(0)("EndDate").ToString()
                        employee.StatusID = ds.Tables(0).Rows(0)("StatusID").ToString()
                        employee.Nationality = ds.Tables(0).Rows(0)("Nationality").ToString()
                        employee.NIC = ds.Tables(0).Rows(0)("NIC").ToString()
                        employee.CountriesTravelled = ds.Tables(0).Rows(0)("CountriesTravelled").ToString()
                        employee.LanguagesSpoken = ds.Tables(0).Rows(0)("LanguagesSpoken").ToString()
                        employee.LanguagesWritten = ds.Tables(0).Rows(0)("LanguagesWritten").ToString()
                        employee.MaritalStatus = ds.Tables(0).Rows(0)("MaritalStatus").ToString()
                        employee.PassportNo = ds.Tables(0).Rows(0)("PassportNo").ToString()
                        employee.LocationID = ds.Tables(0).Rows(0)("LocationID").ToString()
                        employee.DesignationID = ds.Tables(0).Rows(0)("DesignationID").ToString()
                        employee.DepartmentID = ds.Tables(0).Rows(0)("DepartmentID").ToString()
                        employee.CellNo = ds.Tables(0).Rows(0)("CellNo").ToString()
                    End If
                End If
                objModel.GenericList = employee
                objModel.Status = True
            End If


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSave(model As EmployeeModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.Employee
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Long

            If model.Mode = "Add" Then
                objModel.Message = obj.Add(model, intSatatus, msg, NewID)
                objModel.ID = NewID
                objModel.Status = True
            ElseIf model.Mode = "Edit" Then
                objModel.Message = obj.Update(model, intSatatus, msg)
                objModel.ID = model.ID
                objModel.Status = True
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDelete(ByVal dataID As String, ByVal dataName As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Employee", "IsDelete", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim obj As New BizSoft.Bridge_SCM.Employee()
                Dim intSatatus As Boolean = 0
                Dim msg As String = ""
                Dim NewID As Integer

                If dataID <> "" Then
                    objModel.Message = obj.Delete(dataID, dataName, intSatatus, msg)
                    objModel.ID = dataID
                    objModel.Status = True
                End If
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetDesignationList() As String

        Dim obj As New BizSoft.Bridge_SCM.Employee()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetDesignation()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("DesignationID").ToString,
                      .Text = row("Designation").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetDepartmentList() As String

        Dim obj As New BizSoft.Bridge_SCM.Employee()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetDepartment()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("DepartmentID").ToString,
                      .Text = row("DepartmentName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetStatusList() As String

        Dim obj As New BizSoft.Bridge_SCM.Employee()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetStatus()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("StatusID").ToString,
                      .Text = row("StatusName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetMaritalStatusList() As String

        Dim obj As New BizSoft.Bridge_SCM.Employee()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetMaritalStatus()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Description").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetLocationList() As String

        Dim obj As New BizSoft.Bridge_SCM.clsLogin()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.FillLocation()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                     .Value = row("ID").ToString,
                     .Text = row("Name").ToString
                     })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetCityList() As String

        Dim obj As New BizSoft.Bridge_SCM.Employee()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetCity()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                     .Value = row("ID").ToString,
                     .Text = row("Name").ToString
                     })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
End Class