﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services
Imports System.Data.SqlClient

<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class GVoucherService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGrid(ByVal type As String) As String
        Try
            Dim list As New List(Of GVoucherModel)
            Dim obj As New BizSoft.Bridge_SCM.GVoucher()
            Dim ds As DataSet = obj.GetList(type)

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New GVoucherModel() With {
                      .ID = row("VoucherID").ToString(),
                      .VoucherTypeID = row("VoucherTypeID").ToString(),
                      .VoucherNo = row("VoucherNo").ToString(),
                      .VoucherDate = row("VoucherDate").ToString(),
                      .PayToOrReceivedFrom = row("PayToOrReceivedFrom").ToString(),
                      .ReferenceNo = row("ReferenceNo").ToString(),
                      .Description = row("Description").ToString()
                    })
                Next
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridSearch(model As GVoucherModel) As String
        Try
            Dim list As New List(Of GVoucherModel)
            Dim obj As New BizSoft.Bridge_SCM.GVoucher()
            Dim ds As DataSet = obj.GetList2(model)

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New GVoucherModel() With {
                      .ID = row("VoucherID").ToString(),
                      .VoucherTypeID = row("VoucherTypeID").ToString(),
                      .VoucherNo = row("VoucherNo").ToString(),
                      .VoucherDate = row("VoucherDate").ToString(),
                      .PayToOrReceivedFrom = row("PayToOrReceivedFrom").ToString(),
                      .ReferenceNo = row("ReferenceNo").ToString(),
                      .Description = row("Description").ToString()
                    })
                Next
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FillWorkingDate() As String
        Try

            Dim WorkingDate As String = HttpContext.Current.Session("WorkingDate")

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(WorkingDate)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetCurrentLocation() As String
        Try

            Dim LocationId As String = HttpContext.Current.Session("LocationId")

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(LocationId)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetLocationList() As String

        Dim obj As New BizSoft.Bridge_SCM.clsLogin()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.FillLocation()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Name").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetDepartmentList() As String

        Dim obj As New BizSoft.Bridge_SCM.GVoucher()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetDepartment()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("DepartmentID").ToString,
                      .Text = row("DepartmentName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetBrandList() As String

        Dim obj As New BizSoft.Bridge_SCM.GVoucher()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetBrand()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("SegmentID").ToString,
                      .Text = row("SegmentName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetVoucherTypeList() As String

        Dim obj As New BizSoft.Bridge_SCM.GVoucher()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetVoucherType()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("VoucherTypeID").ToString,
                      .Text = row("VoucherTypeID").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FillNameDebtor() As String

        Dim obj As New BizSoft.Bridge_SCM.GVoucher()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetDebtorList()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                     .Value = row("CustomerID").ToString,
                     .Text = row("FirstName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FillNameCreditor() As String

        Dim obj As New BizSoft.Bridge_SCM.GVoucher()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetCreditorList()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                     .Value = row("SupplierID").ToString,
                     .Text = row("SupplierName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSave(model As GVoucherModel, detailList As List(Of GVoucherModel)) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Dim transaction As SqlTransaction
        Try
            Dim obj As New BizSoft.Bridge_SCM.GVoucher
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim VoucherType As String = model.VoucherType 'HttpContext.Current.Session("VoucherType")
            Dim type As String = ""
            Select Case VoucherType
                Case "BP"
                    type = "Bank Payment"
                Case "CP"
                    type = "Cash Payment"
                Case "BR"
                    type = "Bank Recipt"
                Case "CR"
                    type = "Cash Recipt"
            End Select

            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights(type, If(model.ID = "", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                If VoucherType = "BP" Then
                    model.VoucherTypeID = 3 'Bank
                ElseIf VoucherType = "CP" Then
                    model.VoucherTypeID = 2 'Cash
                End If
                ' model.VoucherTypeID = 3
                Dim NewID As Long
                Using connection As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Default").ConnectionString)
                    connection.Open()
                    Dim command = connection.CreateCommand()
                    transaction = connection.BeginTransaction("PaymentReceiptVoucher")
                    command.Connection = connection
                    command.Transaction = transaction

                    If model.ID = "" Then
                        With obj.Add2(model, intSatatus, msg, NewID, command)
                            objModel.Message = .Item1
                            objModel.VoucherNo = .Item2
                            objModel.ID = NewID
                            objModel.Status = True
                        End With
                        If NewID > 0 Then
                            'Dim dt As DataTable = Session("Items")
                            obj.DeleteDetail2(NewID, command)

                            obj.AddDetail(model.VoucherType, NewID, model.AccountCode, model.Particulars, model.ChecqueNoOrDepositNo, model.LocationID, model.Credit, command)
                            If detailList.Count > 0 Then 'dt.Rows
                                'For Each row As DataRow In dt.Rows
                                '    objModel.Message = obj.AddDetail2(NewID, row("AccountCode").ToString(), row("Particulars").ToString(), row("LocationID").ToString(), row("DepartmentID").ToString(), row("ProjectID").ToString(), row("EmployeeID").ToString(), row("Debit").ToString(), row("Credit").ToString(), row("SegmentID").ToString())
                                'Next
                                For Each row As GVoucherModel In detailList
                                    objModel.Message = obj.AddDetail2(NewID, row.AccountCode, row.Particulars, row.LocationID, row.DepartmentID, row.ProjectID, row.EmployeeID, row.Debit, row.Credit, row.SegmentID, command)
                                Next
                            End If
                        End If
                        'objModel.ID = 0
                    Else
                        With obj.Update(model, intSatatus, msg, command)
                            objModel.Message = .Item1
                            objModel.VoucherNo = .Item2
                            objModel.Status = True
                        End With

                        If objModel.Message = "update" Then
                            'Dim dt As DataTable = Session("Items")
                            obj.DeleteDetail2(model.ID, command)
                            obj.AddDetail(model.VoucherType, model.ID, model.AccountCode, model.Particulars, model.ChecqueNoOrDepositNo, model.LocationID, model.Credit, command)
                            If detailList.Count > 0 Then 'dt.Rows
                                'For Each row As DataRow In dt.Rows
                                '    objModel.Message = obj.AddDetail2(model.ID, row("AccountCode").ToString(), row("Particulars").ToString(), row("LocationID").ToString(), row("DepartmentID").ToString(), row("ProjectID").ToString(), row("EmployeeID").ToString(), row("Debit").ToString(), row("Credit").ToString(), row("SegmentID").ToString())
                                'Next
                                For Each row As GVoucherModel In detailList
                                    objModel.Message = obj.AddDetail2(model.ID, row.AccountCode, row.Particulars, row.LocationID, row.DepartmentID, row.ProjectID, row.EmployeeID, row.Debit, row.Credit, row.SegmentID, command)
                                Next
                            End If
                            objModel.Message = "update"
                            objModel.ID = model.ID
                            obj.createVoucherLog(model.ID, command)
                        End If
                    End If
                    'Attempt to commit the transaction.
                    transaction.Commit()
                End Using
                'Session.Remove("Items")
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            'Attempt to roll back the transaction.
            Try
                transaction.Rollback()
            Catch exc As Exception
                'This catch block will handle any errors that may have occurred
                'on the server that would cause the rollback to fail, such as
                'a closed connection.
                Console.WriteLine("Rollback Exception Type: {0}", exc.GetType())
                Console.WriteLine("  Message: {0}", exc.Message)
            End Try

            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSaveDetail(model As VoucherModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.GVoucher
            Dim dt As DataTable = Session("Items")
            Dim table As New DataTable
            Dim a As DataRow
            If model.rowID = "" Then
                a = If(IsNothing(dt), getDatatable().NewRow(), dt.NewRow())
                Dim dtrow As DataRow = a

                dtrow("AccountCode") = model.AccountCode
                dtrow("Particulars") = model.Particulars
                dtrow("LocationID") = model.LocationID
                dtrow("LocationName") = model.LocationName.Trim()
                dtrow("DepartmentID") = model.DepartmentID
                dtrow("DepartmentName") = model.DepartmentName.Trim()
                dtrow("ProjectID") = model.ProjectID
                dtrow("ProjectName") = model.ProjectName
                dtrow("EmployeeID") = model.EmployeeID
                dtrow("EmployeeName") = model.EmployeeName
                dtrow("Debit") = model.Debit
                dtrow("Credit") = model.Credit
                dtrow("SegmentID") = model.SegmentID
                dtrow("SegmentName") = model.SegmentName.Trim()
                dtrow("AccountName") = model.AccountName.Trim()
                If IsNothing(dt) Then
                    a.Table.Rows.Add(dtrow)
                Else
                    dt.Rows.Add(dtrow)
                End If
            Else
                Dim rows As DataRow() = If(IsNothing(dt), getDatatable().Select("rowID = '" & model.rowID & "'"), dt.Select("rowID = '" & model.rowID & "'"))
                If rows.Length > 0 Then
                    rows(0)("AccountCode") = model.AccountCode
                    rows(0)("Particulars") = model.Particulars
                    rows(0)("LocationID") = model.LocationID
                    rows(0)("LocationName") = model.LocationName.Trim()
                    rows(0)("DepartmentID") = model.DepartmentID
                    rows(0)("DepartmentName") = model.DepartmentName.Trim()
                    rows(0)("ProjectID") = model.ProjectID
                    rows(0)("ProjectName") = model.ProjectName
                    rows(0)("EmployeeID") = model.EmployeeID
                    rows(0)("EmployeeName") = model.EmployeeName
                    rows(0)("Debit") = model.Debit
                    rows(0)("Credit") = model.Credit
                    rows(0)("SegmentID") = model.SegmentID
                    rows(0)("SegmentName") = model.SegmentName.Trim()
                    rows(0)("AccountName") = model.AccountName.Trim()
                Else
                    a = If(IsNothing(dt), getDatatable().NewRow(), dt.NewRow())
                    Dim dtrow As DataRow = a

                    dtrow("AccountCode") = model.AccountCode
                    dtrow("Particulars") = model.Particulars
                    dtrow("LocationID") = model.LocationID
                    dtrow("LocationName") = model.LocationName.Trim()
                    dtrow("DepartmentID") = model.DepartmentID
                    dtrow("DepartmentName") = model.DepartmentName.Trim()
                    dtrow("ProjectID") = model.ProjectID
                    dtrow("ProjectName") = model.ProjectName
                    dtrow("EmployeeID") = model.EmployeeID
                    dtrow("EmployeeName") = model.EmployeeName
                    dtrow("Debit") = model.Debit
                    dtrow("Credit") = model.Credit
                    dtrow("SegmentID") = model.SegmentID
                    dtrow("SegmentName") = model.SegmentName.Trim()
                    dtrow("AccountName") = model.AccountName.Trim()
                    If IsNothing(dt) Then
                        a.Table.Rows.Add(dtrow)
                    Else
                        dt.Rows.Add(dtrow)
                    End If
                End If

            End If
            'dt.Rows.Add(model.ID, model.PartNumber, model.ModelNumber, model.DefaultDesc, model.CurrentDesc, model.Quantity, model.Unit, model.UnitPrice, model.Remarks, model.BD_PRICE, model.FK_Item, model.DisCount_per)
            If IsNothing(dt) Then
                Session("Items") = a.Table
                If a.Table.Rows.Count > 0 Then
                    objModel.Message = "success"
                Else
                    objModel.Message = "failed"
                End If
            Else
                Session("Items") = dt
                If dt.Rows.Count > 0 Then
                    objModel.Message = "success"
                Else
                    objModel.Message = "failed"
                End If
            End If
            'Dim intSatatus As Boolean = 0
            'Dim msg As String = ""

            'If model.ID = "" Then
            '    objModel.Message = obj.AddDetail(model, intSatatus, msg)
            '    objModel.ID = 0
            'Else
            '    objModel.Message = obj.UpdateDetail(model, intSatatus, msg)
            '    objModel.ID = model.ID
            'End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridDetail() As String
        Try
            Dim list As New List(Of GVoucherModel)
            Dim obj As New BizSoft.Bridge_SCM.GVoucher()
            Dim ds As DataSet = obj.AccountCode()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New GVoucherModel() With {
                      .AccountName = row("AccountName").ToString(),
                      .AccountCode = row("AccountCode").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridDetailAccountCode(ByVal type As String) As String
        Try
            Dim list As New List(Of GVoucherModel)
            Dim obj As New BizSoft.Bridge_SCM.GVoucher()
            Dim ds As DataSet = obj.AccountCode2(type)

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New GVoucherModel() With {
                      .AccountName = row("AccountName").ToString(),
                      .AccountCode = row("AccountCode").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridProject() As String
        Try
            Dim list As New List(Of GVoucherModel)
            Dim obj As New BizSoft.Bridge_SCM.GVoucher()
            Dim ds As DataSet = obj.GetProjects()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New GVoucherModel() With {
                      .ProjectID = row("ProjectID").ToString(),
                      .ProjectTitle = row("ProjectTitle").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FillBankAccount(ByVal type As String) As String
        Try
            Dim list As New List(Of GVoucherModel)
            Dim obj As New BizSoft.Bridge_SCM.GVoucher()
            Dim ds As DataSet = obj.GetBankAccount()
            Dim VoucherType As String = type 'HttpContext.Current.Session("VoucherType")

            If VoucherType = "BP" Or VoucherType = "BR" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In ds.Tables(0).Rows
                        list.Add(New GVoucherModel() With {
                          .BankAccountCode = row("BankAccountCode").ToString(),
                          .AccountName = row("AccountName").ToString()
                        })
                    Next
                End If
            ElseIf VoucherType = "CP" Or VoucherType = "CR" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In ds.Tables(0).Rows
                        list.Add(New GVoucherModel() With {
                          .BankAccountCode = row("CashAccountCode").ToString(),
                          .AccountName = row("AccountName").ToString()
                        })
                    Next
                End If
            End If

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New GVoucherModel() With {
                      .BankAccountCode = row("BankAccountCode").ToString(),
                      .AccountName = row("AccountName").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridEmployee() As String
        Try
            Dim list As New List(Of GVoucherModel)
            Dim obj As New BizSoft.Bridge_SCM.GVoucher()
            Dim ds As DataSet = obj.GetEmployees()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New GVoucherModel() With {
                      .EmployeeID = row("EmployeeID").ToString(),
                      .EmployeeName = row("EmployeeName").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridDetail2(ID As String) As String
        Try
            Dim list As New List(Of GVoucherModel)
            Dim obj As New BizSoft.Bridge_SCM.GVoucher()
            Dim ds As DataSet = obj.GetItemDetail(ID)
            Dim rowCount As Integer = 0
            If ds.Tables(0).Rows.Count > 0 Then
                ds.Tables(0).Columns.Add("rowID", GetType(Integer))
                For Each row As DataRow In ds.Tables(0).Rows
                    row("LocationName") = obj.GetLocation(row("LocationID").ToString())
                    row("DepartmentName") = obj.GetDepartment(row("DepartmentID").ToString())
                    row("ProjectName") = obj.GetProjects(row("ProjectID").ToString())
                    row("EmployeeName") = obj.GetEmployees(row("EmployeeID").ToString())
                    row("SegmentName") = obj.GetBrand(row("SegmentID").ToString())
                    row("AccountName") = obj.GetAccountName(row("AccountCode").ToString())
                    rowCount += 1
                    row("rowID") = rowCount.ToString()
                    list.Add(New GVoucherModel() With {
                     .VoucherID = row("VoucherID").ToString(),
                      .AccountCode = row("AccountCode").ToString(),
                      .Particulars = row("Particulars").ToString(),
                      .LocationID = row("LocationID").ToString(),
                      .LocationName = obj.GetLocation(row("LocationID").ToString()),
                      .DepartmentID = row("DepartmentID").ToString(),
                      .DepartmentName = obj.GetDepartment(row("DepartmentID").ToString()),
                      .ProjectID = row("ProjectID").ToString(),
                      .ProjectName = obj.GetProjects(row("ProjectID").ToString()),
                      .EmployeeID = row("EmployeeID").ToString(),
                      .EmployeeName = obj.GetEmployees(row("EmployeeID").ToString()),
                      .Debit = row("Debit").ToString(),
                      .Credit = row("Credit").ToString(),
                      .SegmentID = row("SegmentID").ToString(),
                      .AccountName = obj.GetAccountName(row("AccountCode").ToString()),
                      .SegmentName = obj.GetBrand(row("SegmentID").ToString()),
                      .rowID = row("rowID").ToString(),
                      .Account = row("AccountCode").ToString() + " - " + obj.GetAccountName(row("AccountCode").ToString())
                    })
                Next
            End If
            Session("Items") = ds.Tables(0)
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridItems() As String
        Try
            Dim list As New List(Of GVoucherModel)
            Dim dt = getDatatable()
            Session("Items") = dt
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridItems2() As String
        Try
            Dim list As New List(Of GVoucherModel)
            Dim obj As New BizSoft.Bridge_SCM.GVoucher()
            Dim dt As DataTable = Session("Items")
            Dim rowCount As Integer = 0

            If dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    rowCount += 1
                    row("rowID") = rowCount.ToString()
                    list.Add(New GVoucherModel() With {
                      .VoucherID = row("VoucherID").ToString(),
                      .AccountCode = row("AccountCode").ToString(),
                      .Particulars = row("Particulars").ToString(),
                      .LocationID = row("LocationID").ToString(),
                      .LocationName = row("LocationName").ToString(),
                      .DepartmentID = row("DepartmentID").ToString(),
                      .DepartmentName = row("DepartmentName").ToString(),
                      .ProjectID = row("ProjectID").ToString(),
                      .ProjectName = row("ProjectName").ToString(),
                      .EmployeeID = row("EmployeeID").ToString(),
                      .EmployeeName = row("EmployeeName").ToString(),
                      .Debit = row("Debit").ToString(),
                      .Credit = row("Credit").ToString(),
                      .SegmentID = row("SegmentID").ToString(),
                      .SegmentName = row("SegmentName").ToString(),
                      .AccountName = row("AccountName").ToString(),
                      .Account = row("AccountCode").ToString() + " - " + row("AccountName").ToString(),
                      .rowID = rowCount.ToString
                    })
                Next
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FindData(ID As String) As String
        Try
            Dim list As New List(Of GVoucherModel)
            Dim obj As New BizSoft.Bridge_SCM.GVoucher()
            Dim ds As DataSet = obj.FindData(ID)

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New GVoucherModel() With {
                      .SupplierID = row("SupplierID").ToString(),
                      .SupplierCompany = row("SupplierCompany").ToString(),
                      .CustomerID = row("CustomerID").ToString(),
                      .CustomerCompany = row("CustomerCompany").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FindData2(ID As String) As String
        Try
            Dim rowCount As Integer = 0
            Dim list As New List(Of GVoucherModel)
            Dim detail As New List(Of GVoucherModel)
            Dim obj As New BizSoft.Bridge_SCM.GVoucher()
            Dim ds As DataSet = obj.FindData2(ID)
            Dim i = 0
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    If i = 0 Then
                        list.Add(New GVoucherModel() With {
                                              .Particulars = row("Particulars").ToString(),
                                              .Credit = row("Credit").ToString(),
                                              .ChecqueNoOrDepositNo = row("ChecqueNoOrDepositNo").ToString(),
                                              .AccountCode = row("AccountCode").ToString(),
                                              .AccountName = row("AccountName").ToString(),
                                              .LocationID = row("LocationID").ToString()
                                            })
                    End If
                    i += 1
                Next

                Dim j = 0
                ds.Tables(0).Columns.Add("rowID", GetType(Integer))
                For Each row As DataRow In ds.Tables(0).Rows
                    If j > 0 Then
                        row("LocationName") = obj.GetLocation(row("LocationID").ToString())
                        row("DepartmentName") = obj.GetDepartment(row("DepartmentID").ToString())
                        row("ProjectName") = obj.GetProjects(row("ProjectID").ToString())
                        row("EmployeeName") = obj.GetEmployees(row("EmployeeID").ToString())
                        row("SegmentName") = obj.GetBrand(row("SegmentID").ToString())
                        row("AccountName") = obj.GetAccountName(row("AccountCode").ToString())
                        rowCount += 1
                        row("rowID") = rowCount.ToString()
                        detail.Add(New GVoucherModel() With {
                             .VoucherID = row("VoucherID").ToString(),
                              .AccountCode = row("AccountCode").ToString(),
                              .Particulars = row("Particulars").ToString(),
                              .LocationID = row("LocationID").ToString(),
                              .LocationName = obj.GetLocation(row("LocationID").ToString()),
                              .DepartmentID = row("DepartmentID").ToString(),
                              .DepartmentName = obj.GetDepartment(row("DepartmentID").ToString()),
                              .ProjectID = row("ProjectID").ToString(),
                              .ProjectName = obj.GetProjects(row("ProjectID").ToString()),
                              .EmployeeID = row("EmployeeID").ToString(),
                              .EmployeeName = obj.GetEmployees(row("EmployeeID").ToString()),
                              .Debit = row("Debit").ToString(),
                              .Credit = row("Credit").ToString(),
                              .SegmentID = row("SegmentID").ToString(),
                              .AccountName = obj.GetAccountName(row("AccountCode").ToString()),
                              .SegmentName = obj.GetBrand(row("SegmentID").ToString()),
                              .rowID = row("rowID").ToString(),
                              .Account = row("AccountCode").ToString() + " - " + obj.GetAccountName(row("AccountCode").ToString())
                            })

                    End If
                    j += 1
                Next
                Dim firstRow As DataRow = ds.Tables(0).Rows(0)
                ds.Tables(0).Rows.Remove(firstRow)
                Session("Items") = ds.Tables(0)
            End If


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize({list, detail})
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDeleteItem(ByVal ID As String, ByVal AccountCode As String, ByVal Particulars As String, ByVal LocationID As String, ByVal DepartmentID As String, ByVal EmployeeID As String, ByVal Debit As String, ByVal Credit As String) As String

        Dim objModel As New MessageModel
        Try
            Dim ds As DataTable = Session("Items")
            For Each dr As DataRow In ds.Rows
                If ID = "0" Then
                    If dr("AccountCode").ToString() = AccountCode And dr("Particulars").ToString() = Particulars And dr("LocationID").ToString() = LocationID And dr("DepartmentID").ToString() = DepartmentID And dr("EmployeeID").ToString() = EmployeeID And dr("Debit").ToString() = Debit And dr("Credit").ToString() = Credit Then
                        dr.Delete()
                    End If
                    ds.AcceptChanges()
                Else
                    If dr("VoucherID").ToString() = ID And dr("AccountCode").ToString() = AccountCode And dr("Particulars").ToString() = Particulars And dr("LocationID").ToString() = LocationID And dr("DepartmentID").ToString() = DepartmentID And dr("EmployeeID").ToString() = EmployeeID And dr("Debit").ToString() = Debit And dr("Credit").ToString() = Credit Then
                        dr.Delete()
                    End If
                    ds.AcceptChanges()
                End If

            Next

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            'Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetAccountName(AccountCode As String) As String
        Try
            Dim list As New List(Of VoucherModel)
            Dim obj As New BizSoft.Bridge_SCM.GVoucher()
            Dim AccountName As String = ""
            Dim ds As DataSet = obj.GetAccountName2(AccountCode)
            If ds.Tables(0).Rows.Count > 0 Then
                list.Add(New VoucherModel() With {
                     .AccountName = ds.Tables(0).Rows(0)(1).ToString(),
                     .AccountCode = ds.Tables(0).Rows(0)(0).ToString()
                   })

            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetAccountCode(AccountName As String) As String
        Try
            Dim list As New List(Of VoucherModel)
            Dim obj As New BizSoft.Bridge_SCM.GVoucher()
            Dim ds As DataSet = obj.GetAccountCode2(AccountName)
            If ds.Tables(0).Rows.Count > 0 Then
                list.Add(New VoucherModel() With {
                     .AccountName = ds.Tables(0).Rows(0)(1).ToString(),
                     .AccountCode = ds.Tables(0).Rows(0)(0).ToString()
                   })

            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Sub clearItem()
        Try
            Session.Remove("Items")
        Catch ex As Exception

        End Try
    End Sub
    Private Function getDatatable() As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("VoucherID", GetType(String))
        dt.Columns.Add("AccountCode", GetType(String))
        dt.Columns.Add("Particulars", GetType(String))
        dt.Columns.Add("LocationID", GetType(String))
        dt.Columns.Add("LocationName", GetType(String))
        dt.Columns.Add("DepartmentID", GetType(String))
        dt.Columns.Add("DepartmentName", GetType(String))
        dt.Columns.Add("ProjectID", GetType(String))
        dt.Columns.Add("ProjectName", GetType(String))
        dt.Columns.Add("EmployeeID", GetType(String))
        dt.Columns.Add("EmployeeName", GetType(String))
        dt.Columns.Add("Debit", GetType(String))
        dt.Columns.Add("Credit", GetType(String))
        dt.Columns.Add("SegmentID", GetType(String))
        dt.Columns.Add("SegmentName", GetType(String))
        dt.Columns.Add("AccountName", GetType(String))
        dt.Columns.Add("rowID", GetType(Integer))
        Return dt
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function AddEditVoucherSupport(ByVal id As String) As String
        Try
            Dim list As New List(Of DropdownModel)
            Dim obj As New BizSoft.Bridge_SCM.Voucher()

            Dim supportDs As DataSet = obj.GetVoucherSupportById(id)
            Dim support As New VoucherSupportModel
            If supportDs.Tables(0).Rows.Count > 0 Then
                support.Id = supportDs.Tables(0).Rows(0)("ID").ToString()
                support.VoucherId = supportDs.Tables(0).Rows(0)("VoucherId").ToString()
                support.AccountCode = supportDs.Tables(0).Rows(0)("AccountCode").ToString()
                support.Currency = supportDs.Tables(0).Rows(0)("CurrencyID").ToString()
                support.Rate = supportDs.Tables(0).Rows(0)("CCRate").ToString()
                support.OrderId = supportDs.Tables(0).Rows(0)("OrderID").ToString()
                support.OrderQuantity = supportDs.Tables(0).Rows(0)("OrderQty").ToString()
                support.OrderRate = supportDs.Tables(0).Rows(0)("OrderRate").ToString()
                support.ExpenseDate = supportDs.Tables(0).Rows(0)("ExpenseDate").ToString()
                support.InvoiceReceived = supportDs.Tables(0).Rows(0)("InvoiceReceived").ToString()
                support.ExpenseCurrency = supportDs.Tables(0).Rows(0)("ExpCurrencyID").ToString()
            End If

            Dim ds As DataSet = obj.GetCurrency()
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString(),
                      .Text = row("Currency").ToString()
                    })
                Next
            End If

            Dim resultSet = New VoucherSupportResultSet() With {
            .VoucherSupport = support,
            .CurrencyList = list
            }

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(resultSet)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function SaveVoucherSupport(supportVm As VoucherSupportModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            Dim messages As String = ""
            Dim status As Boolean = True
            Dim NewID As Long
            Dim obj As New BizSoft.Bridge_SCM.Voucher()
            Dim resultSet = obj.SaveVoucherSupport(supportVm, status, messages, NewID)
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(resultSet)
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function deleteGVoucher(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.GVoucher()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Integer
            Using connection As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Default").ConnectionString)
                connection.Open()
                Dim command = connection.CreateCommand()
                command.Connection = connection
                If id <> "" Then
                    obj.createVoucherLog(id, command)
                    obj.DeleteVoucher(id)
                End If
            End Using


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
End Class

