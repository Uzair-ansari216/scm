﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class StatusApprovalService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function loadGrid() As String
        Try
            Dim list As New List(Of StatusApprovalModel)
            Dim objBankTitle As New BizSoft.Bridge_SCM.StatusApproval()
            Dim ds As DataSet = objBankTitle.GetList()
            Dim rowCount As Integer = 0
            ds.Tables(0).Columns.Add("row")

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    rowCount += 1
                    row("row") = rowCount
                    list.Add(New StatusApprovalModel() With { _
                      .OrderNo = row("OrderNo").ToString(), _
                      .SalesDate = row("SalesDate").ToString(), _
                      .CustomerName = row("CustomerName").ToString(), _
                      .EmployeeName = row("EmployeeName").ToString(), _
                      .Refrences = row("Refrences").ToString(), _
                      .Remarks = row("Remarks").ToString(), _
                      .PK_SalesOrder = row("PK_SalesOrder").ToString(), _
                      .Status2 = row("Status2").ToString(), _
                      .StatusRemarks = row("StatusRemarks").ToString(), _
                      .rowID = rowCount.ToString
                    })
                Next
            End If
            Session("Detail") = ds

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function OnSave(model As StatusApprovalModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.StatusApproval
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Long
            Dim ds As DataSet = Session("Detail")

            For Each row As DataRow In ds.Tables(0).Rows
                If Val(row("isSave").ToString()) = 0 Then

                ElseIf Val(row("isSave")) = 1 Then
                    obj.Delete(row("PK_SalesOrder").ToString())
                    objModel.Message = obj.Add(row("PK_SalesOrder").ToString(), model.Status, model.StatusDate, model.Remarks, msg)
                End If

            Next
            'If model.ID = "" Then
            '    objModel.Message = obj.Add(0, model.Description, model.Safty_ins, intSatatus, msg, NewID)
            '    objModel.ID = NewID
            'Else
            '    objModel.Message = obj.Update(model.ID, model.Description, model.Safty_ins, intSatatus, msg)
            '    objModel.ID = model.ID
            'End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function OnDelete(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.StatusApproval()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Integer

            If dataID <> "" Then
                'objModel.Message = obj.Delete(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetStatusList() As String

        Dim obj As New BizSoft.Bridge_SCM.StatusApproval()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetStatus()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("StatusID").ToString, _
                      .Text = row("Description").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function OnCheckedChange(ByVal rowID As String, ByVal chk As Integer) As String
        Dim ds As DataSet = Session("Detail")
        If ds.Tables(0).Rows.Count > 0 Then
            Dim strFilter As String = "row='" & rowID & "'"
            Dim oRow() As Data.DataRow = ds.Tables(0).Select(strFilter)
            oRow(0)("isSave") = chk
        End If
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function OnCheckedChange2(ByVal chk As Integer) As String
        Dim ds As DataSet = Session("Detail")
        If ds.Tables(0).Rows.Count > 0 Then
            'Dim strFilter As String = "row='" & rowID & "'"
            'Dim oRow() As Data.DataRow = ds.Tables(0).Select(strFilter)
            'oRow(0)("isSave") = chk

            For Each row As DataRow In ds.Tables(0).Rows
                row("isSave") = chk
            Next
        End If
    End Function
End Class