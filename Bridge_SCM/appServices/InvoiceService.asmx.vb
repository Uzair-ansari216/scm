﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services
Imports System.Reflection

<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class InvoiceService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGrid() As String
        Try
            Dim list As New List(Of SalesOrderModel)
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim ds As DataSet = obj.GetList()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New SalesOrderModel() With {
                      .ID = row("PK_SalesOrder").ToString(),
                      .Fname = row("Firstname").ToString(),
                      .SalesOrder = row("SalesOrder").ToString(),
                      .SalesDate2 = row("SalesDate").ToString(),
                      .FK_CustomerID = row("FK_CustomerID").ToString(),
                      .BillTo = row("BillTo").ToString(),
                      .ShipTo = row("ShipTo").ToString(),
                      .OrderType = row("OrderType").ToString(),
                      .OrderRef = row("OrderRef").ToString(),
                      .OrderRefDate = row("OrderRef").ToString(),
                      .FK_EmployeeID = row("FK_EmployeeID").ToString(),
                      .FK_LocationID = row("FK_LocationID").ToString(),
                      .Tax = row("Tax").ToString(),
                      .Charges = row("Charges").ToString(),
                      .Remarks = row("Remarks").ToString(),
                      .Billing = row("Billing").ToString(),
                      .Assembly = row("Assembly").ToString(),
                      .Delivery = row("Delivery").ToString(),
                      .CompanyID = row("CompanyID").ToString(),
                      .FK_Terms = row("FK_Terms").ToString(),
                      .FK_CurrencyID = row("FK_CurrencyID").ToString(),
                      .ExchangeRate = row("ExchangeRate").ToString(),
                      .FK_StatusID = row("FK_StatusID").ToString(),
                      .StatusDate = row("StatusDate").ToString(),
                      .Status = row("StatusName").ToString(),
                      .SalesPerson = row("SalesPerson").ToString(),
                      .StatusRemarks = row("StatusRemarks").ToString()
                    })
                Next
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetDeliveredFromList() As String

        Dim obj As New BizSoft.Bridge_SCM.GoodsDeliveryNote()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetDeliveredFrom()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("LocationID").ToString,
                      .Text = row("LocationName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetTypeList() As String

        Dim obj As New BizSoft.Bridge_SCM.GoodsDeliveryNote()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetTypeList()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Description").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetCurrencyList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetCurrency()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Name").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetTermsList() As String

        Dim obj As New BizSoft.Bridge_SCM.Invoice()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetTerms()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("PK_Terms").ToString,
                      .Text = row("Terms").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetSOTypeList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetSOType()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Description").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetCustomerList() As String

        Dim obj As New BizSoft.Bridge_SCM.Invoice()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetDealerRecord()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("CustomerID").ToString,
                      .Text = row("Company").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetSalesPersonList() As String

        Dim obj As New BizSoft.Bridge_SCM.Invoice()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetSalesPerson()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("EmployeeID").ToString,
                      .Text = row("FirstName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetStatusList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetStatus()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("StatusID").ToString,
                      .Text = row("StatusName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnLocationChange(ID As String) As String

        Dim obj As New BizSoft.Bridge_SCM.Invoice()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetReference(ID.Trim())
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("PK_SalesOrder").ToString,
                      .Text = row("SO").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnType2Change(ID As String, GDN As String, Type1 As String) As String

        Dim obj As New BizSoft.Bridge_SCM.GoodsDeliveryNote()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetType3(ID.Trim(), GDN, Type1)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("PK_Reference").ToString,
                      .Text = row("ReferenceNo").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnType3Change(ID As String, GDN As String, Type1 As String) As String

        Dim obj As New BizSoft.Bridge_SCM.GoodsDeliveryNote()
        Dim list As New List(Of GoodsDeliveryNoteModel)
        Dim rowCount As Integer = -1
        Dim ds As DataSet = obj.loadGrid(ID.Trim(), GDN, Type1)
        If ds.Tables(0).Rows.Count > 0 Then
            ds.Tables(0).Columns.Add("row")
            For Each row As DataRow In ds.Tables(0).Rows
                rowCount += 1
                row("row") = rowCount
                list.Add(New GoodsDeliveryNoteModel() With {
                      .Reference = ID,
                      .ID = row("ID").ToString(),
                      .ModelNumber = row("ModelNumber").ToString(),
                      .PartNumber = row("PartNumber").ToString(),
                      .Description = row("Description").ToString(),
                      .TotalQuantity = row("TotalQuantity").ToString(),
                      .Unit = row("Unit").ToString(),
                      .Issued = row("Issued").ToString(),
                      .Quantity = row("Quantity").ToString(),
                      .PK_Item = row("PK_Item").ToString(),
                      .FK_Reference = row("FK_Reference").ToString(),
                      .Stock = 0,
                      .rowID = rowCount
                      })

                Session("GDN") = ds
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSave(model As InvoiceModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Invoice", If(model.ID = "", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim jsSerializer As New JavaScriptSerializer()
                Dim invoiceDetailList = jsSerializer.Deserialize(Of List(Of InvoiceModel))(HttpContext.Current.Request.QueryString("detail"))

                Dim obj As New BizSoft.Bridge_SCM.Invoice
                Dim msg As String = ""

                Dim resultSet = obj.Add(model, invoiceDetailList, msg)
                objModel.Status = True
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function SaveDetail(ByVal row As String, ByVal qty As String, ByVal masterID As String, DeliverdType As String) As String
        Dim objModel As New MessageModel
        Try


            Dim obj As New BizSoft.Bridge_SCM.GoodsDeliveryNote
            Dim ds As DataSet = Session("GDN")
            If ds.Tables(0).Rows.Count > 0 Then
                Dim strFilter As String = "row='" & row & "'"
                Dim oRow() As Data.DataRow = ds.Tables(0).Select(strFilter)
                ' Dim RecDate As String = Session("RecDate")
                objModel.Message = obj.SaveReceivedDetail(masterID, DeliverdType, oRow(0)("FK_Reference").ToString, oRow(0)("PK_Item").ToString, oRow(0)("Description").ToString, qty, oRow(0)("id").ToString)
                'objModel.Message = obj.UpdadeReceivedDetail(oRow(0)("FkBookingID").ToString, oRow(0)("Payment").ToString, NewAmount, RecDate)
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDelete(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.Invoice()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""

            If dataID <> "" Then
                objModel.Message = obj.Delete(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSaveDetail(model As GoodsDeliveryNoteModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.GoodsDeliveryNote

            Dim intSatatus As Boolean = 0
            Dim msg As String = ""

            If model.ID = "" Then
                objModel.Message = obj.AddDetail(model, intSatatus, msg)
                objModel.ID = 0
            Else
                objModel.Message = obj.UpdateDetail(model, intSatatus, msg)
                objModel.ID = model.ID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridDetail() As String
        Try
            Dim list As New List(Of InvoiceModel)
            Dim obj As New BizSoft.Bridge_SCM.Invoice()
            Dim ds As DataSet = obj.ItemList()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New InvoiceModel() With {
                      .ID = row("PK_Invoice").ToString(),
                      .InvoiceNumber = row("InvoiceNo").ToString(),
                      .InvoiceDate = row("InvoiceDate").ToString(),
                      .SalesOrder = row("salesorder").ToString(),
                      .FK_CustomerID = row("CustomerName").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridDetail2(ID As String) As String
        Try
            Dim invoice As New InvoiceModel
            Dim detailList As New List(Of SalesOrderModel)
            Dim obj As New BizSoft.Bridge_SCM.Invoice()
            Dim ds As DataSet = obj.getSaleOrderById(ID)
            If ds.Tables(0).Rows.Count > 0 Then
                invoice.FK_CustomerID = ds.Tables(0).Rows(0)("FK_CustomerID").ToString()
                invoice.FK_EmployeeID = ds.Tables(0).Rows(0)("FK_EmployeeID").ToString()
                invoice.SalesDate = ds.Tables(0).Rows(0)("SalesDate").ToString()
                invoice.BillTo = ds.Tables(0).Rows(0)("BillTo").ToString()
                invoice.Billing = ds.Tables(0).Rows(0)("Billing").ToString()
                invoice.Tax = ds.Tables(0).Rows(0)("Tax").ToString()
                invoice.Charges = ds.Tables(0).Rows(0)("Charges").ToString()
                invoice.Remarks = ds.Tables(0).Rows(0)("Remarks").ToString()
                invoice.FK_Terms = ds.Tables(0).Rows(0)("PK_Terms").ToString()
            End If

            Dim detailDs As DataSet = obj.getSaleOrderDetailById(ID)
            If detailDs.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In detailDs.Tables(0).Rows
                    detailList.Add(New SalesOrderModel() With {
                      .ModelNumber = row("ModelNumber").ToString(),
                      .PartNumber = row("PartNumber").ToString(),
                      .DefaultDescription = row("Description").ToString(),
                      .Quantity = row("Quantity").ToString(),
                      .Unit = row("Unit").ToString(),
                      .UnitPrice = row("UnitPrice").ToString(),
                      .Amount = row("Amount").ToString(),
                      .Remarks = row("Remarks").ToString(),
                      .FK_Item = row("FK_Item").ToString()
                    })
                Next
            End If

            Dim resultSet = New InvoiceResultSet() With
            {
            .InvoiceSaleOrder = invoice,
            .InvoiceSaleOrderDetail = detailList
            }

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(resultSet)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FindData(ID As String) As String
        Try
            Dim invoice As New InvoiceModel
            Dim obj As New BizSoft.Bridge_SCM.Invoice()
            Dim ds As DataSet = obj.FindData(ID)

            If ds.Tables(0).Rows.Count > 0 Then
                invoice.ID = ds.Tables(0).Rows(0)("PK_Invoice")
                invoice.InvoiceNumber = ds.Tables(0).Rows(0)("InvoiceNo")
                invoice.InvoiceDate = ds.Tables(0).Rows(0)("InvoiceDate")
                invoice.InvoiceDueDate = ds.Tables(0).Rows(0)("DueDate")
                invoice.FK_Terms = ds.Tables(0).Rows(0)("FK_Terms")
                invoice.FK_SalesOrder = ds.Tables(0).Rows(0)("FK_SalesOrder")
                invoice.FK_CustomerID = ds.Tables(0).Rows(0)("FK_CustomerID")
                invoice.BillTo = ds.Tables(0).Rows(0)("BillTo")
                invoice.FK_EmployeeID = ds.Tables(0).Rows(0)("FK_EmployeeID")
                invoice.isgst = ds.Tables(0).Rows(0)("Tax")
                invoice.Charges = ds.Tables(0).Rows(0)("Charges")
                invoice.Discount = ds.Tables(0).Rows(0)("Discount")
                invoice.Remarks = ds.Tables(0).Rows(0)("Remarks")
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(invoice)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetStock(ID As String, Datee As String, LocationID As String, CurrentID As String) As String
        Try
            Dim list As New List(Of SalesOrderModel)
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim Stock As Integer = obj.GetStkQtyLoc(ID, Datee, LocationID, CurrentID)


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(Stock)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDeleteDetail(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.GoodsDeliveryNote()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""

            If dataID <> "" Then
                objModel.Message = obj.DeleteDetail(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetLocationList() As String

        Dim obj As New BizSoft.Bridge_SCM.clsLogin()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.FillLocation()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Name").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function generateInvoiceReport(ByVal InvoiceId As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Dim Message As String = ""
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Invoice", "isShow", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim ht As New Hashtable

                Dim report As String = CommonConstant.invoiceReport
                Dim objReport As New BizSoft.DBManager.clsReport.oReport


                objReport.strFileName = report
                HttpContext.Current.Session("ReportFileName") = report
                Dim StrList As New Generic.List(Of String)

                ht.Add("showLogo", True)
                ht.Add("User", HttpContext.Current.Session("UserName"))
                ht.Add("InvoiceID", InvoiceId)

                HttpContext.Current.Session("ogsReport") = objReport

                objReport.ht = ht
                objReport.StrList = StrList
                Dim iAction As String = ""
                iAction = "Generate Invoice Report"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Invoice", "VWRPT_INV_Invoice", "", Environment.MachineName)
                objModel.Status = True
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
            'Return "1"
        Catch ex As Exception
            BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, ex.Message, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Invoice", "VWRPT_INV_Invoice", "", Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
            Message = ex.Message
            Return Message

        End Try
    End Function
End Class