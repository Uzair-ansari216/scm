﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class AssetLocationService
    Inherits System.Web.Services.WebService
    Dim assetLocationRepository As New BizSoft.Bridge_SCM.clsAssetLocation()
    Dim dataSet As DataSet
    Dim ser As New JavaScriptSerializer()
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getAllAssetLocation() As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            Dim assetLocationList As New List(Of AssetLocationModel)
            dataSet = assetLocationRepository.GetAssetLocations()
            If dataSet.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In dataSet.Tables(0).Rows
                    assetLocationList.Add(New AssetLocationModel() With {
                          .Id = Convert.ToInt32(row("ID")),
                          .AssetLocation = row("Asset_Location").ToString,
                          .Description = row("Description").ToString,
                          .Location = Convert.ToInt16(row("Location")),
                          .Department = Convert.ToInt16(row("DepartmentName"))
                          })
                Next
            End If
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(assetLocationList)
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function AddEditAssetLocation(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Asset Location", If(id = "0", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim assetLocation As New AssetLocationModel
                If id <> "0" Then
                    Dim ds As DataSet = assetLocationRepository.GetById(id)
                    If ds.Tables(0).Rows.Count > 0 Then
                        assetLocation.Id = Convert.ToInt32(ds.Tables(0).Rows(0)("ID"))
                        assetLocation.AssetLocation = ds.Tables(0).Rows(0)("Asset_Location").ToString()
                        assetLocation.Description = ds.Tables(0).Rows(0)("Description").ToString()
                        assetLocation.Location = Convert.ToInt16(ds.Tables(0).Rows(0)("LocationID"))
                        assetLocation.Department = Convert.ToInt16(ds.Tables(0).Rows(0)("DepartmentID"))
                    End If
                End If
                objModel.GenericList = assetLocation
                objModel.Status = True
            End If

            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function saveAssetLocation(assetLocationVm As AssetLocationModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim status As Boolean = True
            Dim message As String = ""
            Dim newID As Long

            If assetLocationVm.Id > 0 Then
                objModel.Message = assetLocationRepository.Update(assetLocationVm, status, message)
                objModel.ID = assetLocationVm.Id
                objModel.Status = status
            Else
                objModel.Message = assetLocationRepository.Add(assetLocationVm, status, message, newID)
                objModel.ID = newID
                objModel.Status = status
            End If

            Dim assetLocationList As New List(Of AssetLocationModel)
            dataSet = assetLocationRepository.GetAssetLocations()
            If dataSet.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In dataSet.Tables(0).Rows
                    assetLocationList.Add(New AssetLocationModel() With {
                          .Id = Convert.ToInt32(row("ID")),
                          .AssetLocation = row("Asset_Location").ToString,
                          .Description = row("Description").ToString,
                          .Location = Convert.ToInt16(row("Location")),
                          .Department = Convert.ToInt16(row("DepartmentName"))
                          })
                Next
            End If

            objModel.GenericList = assetLocationList
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function deleteAssetLocation(ByVal id As Int32, ByVal title As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim status As Boolean = True
            Dim message As String = ""

            objModel.Message = assetLocationRepository.Delete(id, title, status, message)
            objModel.ID = id
            objModel.Status = status

            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

End Class