﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

<ScriptService()>
<WebService(Namespace:="http://tempuri.org/")>
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class ReportBuilderService

    Inherits System.Web.Services.WebService


    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetReportType() As String

        Dim ReportType As String = HttpContext.Current.Session("ReportType")

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(ReportType)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FillReports(ID As String) As String
        Dim obj As New BizSoft.Bridge_SCM.Report
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.FillReports(ID)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Name").ToString
                      })
            Next
        End If
        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
#Region "Report Module"

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FillReportModule() As String
        Dim obj As New BizSoft.Bridge_SCM.Report
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.FillReportModule()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("Id").ToString,
                      .Text = row("Name").ToString
                      })
            Next
        End If
        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function


    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FillReportCategories(ID As Int32) As String
        Dim obj As New BizSoft.Bridge_SCM.Report
        Dim remodule As New ReportModule
        Dim ds As DataSet = obj.FillCategories(ID)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim CategoryList = New List(Of DropdownModel)
            For Each row As DataRow In ds.Tables(0).Rows
                CategoryList.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Name").ToString
                      })
            Next
            remodule.DropDown = CategoryList

        End If
        Dim reportCredentials As DataSet = obj.GetReportCredentialsByModule(ID)
        remodule.Name = reportCredentials.Tables(0).Rows(0)("Name")
        remodule.ReportServer = reportCredentials.Tables(0).Rows(0)("ReportServer")
        remodule.ReportDatabase = reportCredentials.Tables(0).Rows(0)("ReportDatabase")
        remodule.ReportDbUserId = reportCredentials.Tables(0).Rows(0)("ReportDbUserId")
        remodule.ReportDbPassord = reportCredentials.Tables(0).Rows(0)("ReportDbPassword")
        remodule.ReportPath = IIf(String.IsNullOrEmpty(reportCredentials.Tables(0).Rows(0)("ReportPath")), "default", reportCredentials.Tables(0).Rows(0)("ReportPath"))
        remodule.ReportConnectionType = IIf(String.IsNullOrEmpty(reportCredentials.Tables(0).Rows(0)("ReportConnectionType")), "default", reportCredentials.Tables(0).Rows(0)("ReportConnectionType"))
        remodule.ConnectionName = IIf(String.IsNullOrEmpty(reportCredentials.Tables(0).Rows(0)("ConnectionName")), "default", reportCredentials.Tables(0).Rows(0)("ConnectionName"))
        remodule.ConnectionUserId = IIf(String.IsNullOrEmpty(reportCredentials.Tables(0).Rows(0)("ConnectionUserId")), "default", reportCredentials.Tables(0).Rows(0)("ConnectionUserId"))
        remodule.ConnectionPassword = IIf(String.IsNullOrEmpty(reportCredentials.Tables(0).Rows(0)("ConnectionPassword")), "default", reportCredentials.Tables(0).Rows(0)("ConnectionPassword"))

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(remodule)
    End Function
#End Region

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FillReportsCategory() As String
        Dim obj As New BizSoft.Bridge_SCM.Report
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.FillReportsCategory()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Description").ToString
                      })
            Next
        End If
        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function Find(ID As String) As String
        Dim obj As New BizSoft.Bridge_SCM.Report()
        Dim list As New List(Of ReportBuilderModel)
        Dim ds As DataSet = obj.GetData(ID)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New ReportBuilderModel() With {
                      .ReportID = row("ReportID").ToString,
                      .ObjectName = row("ObjectName").ToString,
                      .Label = row("Label").ToString,
                      .ObjectType = row("ObjectType").ToString
                      })
            Next
        End If
        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FindControls(CmbName As String, ID As String, server As String, userId As String, password As String, database As String) As String
        Try
            Dim obj As New BizSoft.Bridge_SCM.Report()
            Dim list As New List(Of DropdownModel)
            Dim ds As DataSet = obj.GetData2(CmbName, ID)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim ds2 As DataSet = BizSoft.DBManager.GetDataSetForModuleReportParameter(ds.Tables(0).Rows(0)("Query").ToString(), server, userId, password, database)
                For Each row As DataRow In ds2.Tables(0).Rows
                    list.Add(New DropdownModel() With {
                          .Value = row("ID").ToString,
                          .Text = row("Description").ToString
                          })
                Next
                HttpContext.Current.Session("ConfigurationName") = Nothing
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
End Class
