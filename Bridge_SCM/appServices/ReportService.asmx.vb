﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO

<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class ReportService

    Inherits System.Web.Services.WebService



    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetReportType() As String

        Dim ReportType As String = HttpContext.Current.Session("ReportType")

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(ReportType)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FillReports(ID As String) As String
        Dim obj As New BizSoft.Bridge_SCM.Report
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.FillReports(ID)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Name").ToString
                      })
            Next
        End If
        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
#Region "Report Module"

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FillReportModule() As String
        Dim obj As New BizSoft.Bridge_SCM.Report
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.FillReportModule()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("Id").ToString,
                      .Text = row("Name").ToString
                      })
            Next
        End If
        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function


    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FillReportCategories(ID As Int32) As String
        Dim obj As New BizSoft.Bridge_SCM.Report
        Dim remodule As New ReportModule
        Dim ds As DataSet = obj.FillCategories(ID)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim CategoryList = New List(Of DropdownModel)
            For Each row As DataRow In ds.Tables(0).Rows
                CategoryList.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Name").ToString
                      })
            Next
            remodule.DropDown = CategoryList

        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(remodule)
    End Function
#End Region

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FillReportsCategory() As String
        Dim obj As New BizSoft.Bridge_SCM.Report
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.FillReportsCategory()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Description").ToString
                      })
            Next
        End If
        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function Find(ID As String) As String
        Dim obj As New BizSoft.Bridge_SCM.Report()
        Dim list As New List(Of ReportModel)
        Dim ds As DataSet = obj.GetData(ID)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New ReportModel() With {
                      .ReportID = row("ReportID").ToString,
                      .ObjectName = row("ObjectName").ToString,
                      .Label = row("Label").ToString,
                      .ObjectType = row("ObjectType").ToString,
                      .DefaultValue = row("DefaultValue").ToString
                      })
            Next
        End If
        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function findStoreProcedure(ID As String) As String
        Dim obj As New BizSoft.Bridge_SCM.Report()
        Dim list As New List(Of ReportStoreProcedureParameter)
        Dim ds As DataSet = obj.GetStoreProcedure(ID)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                Dim controlDs As DataSet = obj.GetControlsForSp(row("ReportID").ToString, row("SP_Name").ToString)
                If controlDs.Tables(0).Rows.Count > 0 Then
                    For Each item As DataRow In controlDs.Tables(0).Rows
                        list.Add(New ReportStoreProcedureParameter() With {
                                       .ReportID = item("ReportID").ToString,
                                       .SP_Name = item("SP_Name").ToString,
                                       .DataType = item("DataType").ToString,
                                       .ParameterName = item("ParameterName").ToString,
                                       .ObjectName = item("ObjectName").ToString,
                                       .Message = item("Msg").ToString
                                       })
                    Next
                End If

            Next
        End If
        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FindControls(CmbName As String, ID As String, moduleId As String) As String
        Try

            Dim upgratedQuery = ""
            Dim obj As New BizSoft.Bridge_SCM.Report()
            Dim remodule As New ReportModule
            Dim reportCredentials As DataSet = obj.GetReportCredentialsByModule(moduleId)
            remodule.ReportServer = reportCredentials.Tables(0).Rows(0)("ReportServer")
            remodule.ReportDatabase = reportCredentials.Tables(0).Rows(0)("ReportDatabase")
            remodule.ReportDbUserId = reportCredentials.Tables(0).Rows(0)("ReportDbUserId")
            remodule.ReportDbPassord = reportCredentials.Tables(0).Rows(0)("ReportDbPassword")
            Dim list As New List(Of DropdownModel)
            Dim ds As DataSet = obj.GetData2(CmbName, ID)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim sessVar = ds.Tables(0).Rows(0)("SessionVariable").ToString().Split(",")
                Dim query = ds.Tables(0).Rows(0)("Query").ToString()
                If sessVar.Length > 0 Then
                    For Each item As String In sessVar
                        If item = "UserID" Then
                            upgratedQuery = query.Replace("@UserID", Session("UserID"))
                        ElseIf item = "UserName" Then
                            upgratedQuery = ds.Tables(0).Rows(0)("Query").ToString().Replace("@UserName", Session("UserName"))
                        ElseIf item = "CompanyID" Then
                            upgratedQuery = query.Replace("@CompanyID", Session("CompanyID"))
                        ElseIf item = "CompanyName" Then
                            upgratedQuery = query.Replace("@CompanyName", Session("CompanyName"))
                        ElseIf item = "isPowerUser" Then
                            upgratedQuery = query.Replace("@PowerUser", Session("isPowerUser"))
                        ElseIf item = "StatusID" Then
                            upgratedQuery = query.Replace("@StatusID", Session("StatusID"))
                        ElseIf item = "LocationId" Then
                            upgratedQuery = query.Replace("@LocationId", Session("LocationId"))
                        ElseIf item = "LocationName" Then
                            upgratedQuery = query.Replace("@LocationName", Session("LocationName"))
                        ElseIf item = "WorkingDate" Then
                            upgratedQuery = query.Replace("@WorkingDate", Session("WorkingDate"))
                        ElseIf item = "DeveloperMode" Then
                            upgratedQuery = query.Replace("@DeveloperMode", Session("DeveloperMode"))
                        ElseIf item = "ToolTip" Then
                            upgratedQuery = query.Replace("@ToolTip", Session("ToolTip"))
                        ElseIf item = "Training" Then
                            upgratedQuery = query.Replace("@Training", Session("Training"))
                        ElseIf item = "VoiceMessage" Then
                            upgratedQuery = query.Replace("@VoiceMessage", Session("VoiceMessage"))
                        End If
                        query = upgratedQuery
                    Next
                End If

                Dim ds2 As DataSet = BizSoft.DBManager.GetDataSetForModuleReportParameter(query, remodule.ReportServer, remodule.ReportDbUserId, remodule.ReportDbPassord, remodule.ReportDatabase)
                For Each row As DataRow In ds2.Tables(0).Rows
                    list.Add(New DropdownModel() With {
                          .Value = row(0).ToString,
                          .Text = row(1).ToString
                          })
                Next
                HttpContext.Current.Session("ConfigurationName") = Nothing
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function checkReportParameter() As String
        Dim report As String = HttpContext.Current.Request.QueryString("id")
        Dim obj As New BizSoft.Bridge_SCM.Report()
        Dim list As New List(Of ReportParameter)
        Dim ds As DataSet = obj.GetReportParameters(report)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New ReportParameter() With {
                      .ReportID = row("ReportID").ToString,
                      .DataType = row("DataType").ToString,
                      .ObjectName = row("ObjectName").ToString,
                      .ParameterName = row("ParameterName").ToString,
                      .Message = row("Msg").ToString
                      })
            Next
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Else
            Return 0
        End If
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Function Save() As String
        Try
            Dim IDS As String = HttpContext.Current.Request.QueryString("id")
            Dim id() As String
            Dim message As String
            Dim ht As New Hashtable
            Dim htForSp As New Hashtable
            Dim SelctionFormulaDS As DataSet
            Dim report As String = HttpContext.Current.Request.QueryString("ddlReports")
            Dim _NA As String = HttpContext.Current.Request.QueryString("ddlNA")
            Dim _PA As String = HttpContext.Current.Request.QueryString("ddlPA")
            Dim _UC As String = HttpContext.Current.Request.QueryString("ddlUC")
            Dim _Ward As String = HttpContext.Current.Request.QueryString("ddlWard")
            Dim _Block As String = HttpContext.Current.Request.QueryString("ddlBlock")
            Dim _Area As String = HttpContext.Current.Request.QueryString("ddlArea")
            Dim _Street As String = HttpContext.Current.Request.QueryString("ddlStreet")
            Dim reportModule = HttpContext.Current.Request.QueryString("reportModule")
            Session("reportFormat") = HttpContext.Current.Request.QueryString("reportFormat")
            Dim obj As New BizSoft.Bridge_SCM.Report
            Dim remodule As New ReportModule
            Dim reportCredentials As DataSet = obj.GetReportCredentialsByModule(reportModule)
            remodule.Name = reportCredentials.Tables(0).Rows(0)("Name")
            remodule.ReportServer = reportCredentials.Tables(0).Rows(0)("ReportServer")
            remodule.ReportDatabase = reportCredentials.Tables(0).Rows(0)("ReportDatabase")
            remodule.ReportDbUserId = reportCredentials.Tables(0).Rows(0)("ReportDbUserId")
            remodule.ReportDbPassord = reportCredentials.Tables(0).Rows(0)("ReportDbPassword")
            remodule.ReportPath = IIf(String.IsNullOrEmpty(reportCredentials.Tables(0).Rows(0)("ReportPath")), "default", reportCredentials.Tables(0).Rows(0)("ReportPath"))
            If IsDBNull(reportCredentials.Tables(0).Rows(0)("ReportConnectionType")) Then
                remodule.ReportConnectionType = ""
            Else
                remodule.ReportConnectionType = reportCredentials.Tables(0).Rows(0)("ReportConnectionType")
            End If

            If IsDBNull(reportCredentials.Tables(0).Rows(0)("ConnectionName")) Then
                remodule.ConnectionName = ""
            Else
                remodule.ConnectionName = reportCredentials.Tables(0).Rows(0)("ConnectionName")
            End If

            If IsDBNull(reportCredentials.Tables(0).Rows(0)("ConnectionUserId")) Then
                remodule.ConnectionUserId = ""
            Else
                remodule.ConnectionUserId = reportCredentials.Tables(0).Rows(0)("ConnectionUserId")
            End If

            If IsDBNull(reportCredentials.Tables(0).Rows(0)("ConnectionPassword")) Then
                remodule.ConnectionPassword = ""
            Else
                remodule.ConnectionPassword = reportCredentials.Tables(0).Rows(0)("ConnectionPassword")
            End If


            Dim Todates As String = HttpContext.Current.Request.QueryString("Todate")
            'Dim ToDateWithTime As DateTime = HttpContext.Current.Request.QueryString("ToDateWithTime")
            Dim timee As String = " 23:59:59"
            Dim Todate As String = Todates + timee

            Dim result As String = ""
            Dim mAccountCode As String = ""
            'Dim ObjACt As New BizSoft.Bridge_SCM.Task

            Dim objReport As New BizSoft.DBManager.clsReport.oReport
            Dim StrList As New Generic.List(Of String)

            Dim oCulture = New System.Globalization.CultureInfo("en-US", True)

            Dim selectionsql As Object = ""
            Dim selectionsqlCode As String = ""
            objReport.Formula = ""

            If report = "-1" Then
                Return "Please Select Any Reports"
            End If

            If IDS = "" Then
                'Return "Please Select ChartofAccount"
            Else
                id = IDS.Split(",")
            End If

            Dim sp_ParameterDs As DataSet = obj.GetReportSpParameter(report)
            If sp_ParameterDs.Tables(0).Rows.Count > 0 Then
                Dim storeProcedure = sp_ParameterDs.Tables(0).Rows(0)("SP_Name").ToString()
                Dim spParameterList As New SqlClient.SqlCommand
                For i = 0 To sp_ParameterDs.Tables(0).Rows.Count - 1

                    Dim ObjectValue As String = System.Net.WebUtility.HtmlDecode(HttpContext.Current.Request.QueryString(sp_ParameterDs.Tables(0).Rows(i)("ObjectName")))
                    Dim ParameterName As String = sp_ParameterDs.Tables(0).Rows(i)("ParameterName")
                    'spParameterList.Parameters.AddWithValue(ParameterName, ObjectValue)
                    'If ObjectValue = "-1" Or ObjectValue = "undefined" Or ObjectValue = "" Or ObjectValue = " " Then
                    '    Return ds.Tables(0).Rows(i)("Msg")
                    'End If

                    htForSp.Add(ParameterName, ObjectValue)
                Next

                obj.ExecuteSp(htForSp, storeProcedure)
            End If

            Dim dr As DataSet = obj.GetReportData(report)

            If dr.Tables(0).Rows.Count > 0 Then
                'If dr.Tables(0).Rows(0)("AutoCalling").ToString() = "1" Then

                'If report = "RptMemberCountnewHourwise.rpt" Then
                Dim ds As DataSet = obj.GetReportParameter(report)
                If dr.Tables(0).Rows(0)("IsSubReport").ToString() = "1" Then
                    objReport.rptIsSubLoad = True
                    objReport.rptSubReportCollection.Add("1")
                End If
                For i = 0 To ds.Tables(0).Rows.Count - 1

                    Dim ObjectValue As String = System.Net.WebUtility.HtmlDecode(HttpContext.Current.Request.QueryString(ds.Tables(0).Rows(i)("ObjectName")))
                    Dim ParameterName As String = ds.Tables(0).Rows(i)("ParameterName")

                    If ObjectValue = "-1" Or ObjectValue = "undefined" Or ObjectValue = "" Or ObjectValue = " " Then
                        Return ds.Tables(0).Rows(i)("Msg")
                    End If

                    ht.Add(ParameterName, ObjectValue)
                Next

                'ht.Add("FromDate", ToDateWithTime)
                'ht.Add("Todate", ToDateWithTime)
                'End If
                'Dim ds As DataSet = obj.GetReportParameter(report)
                'For i = 0 To ds.Tables(0).Rows.Count - 1
                '    Dim ObjectValue As String = HttpContext.Current.Request.QueryString(ds.Tables(0).Rows(i)("ObjectName"))
                '    Dim ParameterName As String = ds.Tables(0).Rows(i)("ParameterName")

                '    ht.Add(ParameterName, ObjectValue)
                'Next
                'Else
                If report = "RptBlockDetail.rpt" Then
                    If _Block = "-1" Or _Block = "undefined" Then
                        Return "Please Select Block "
                    End If

                End If

                If report = "RptProvinceAssemblyAnalysis.rpt" Then
                    If _PA = "-1" Or _PA = "undefined" Then
                        Return "Please Select PA "
                    End If
                End If

                If report = "RptUCAnalysis.rpt" Then
                    If _UC = "-1" Or _UC = "undefined" Then
                        Return "Please Select UC "
                    End If
                End If

                If report = "RptWardAnalysis.rpt" Then
                    If _Ward = "-1" Or _Ward = "undefined" Then
                        Return "Please Select Ward "
                    End If
                End If




                If report = "RptBlockDetail.rpt" Then
                    objReport.rptIsSubLoad = True
                    objReport.rptSubReportCollection.Add("1")
                    ht.Add("BlockCodeId", Val(_Block))
                End If

                If report = "RptProvinceAssemblyAnalysis.rpt" Then
                    ht.Add("PKPA", Val(_PA))
                End If
                If report = "RptUCAnalysis.rpt" Then
                    ht.Add("PKUCID", Val(_UC))
                End If
                If report = "RptWardAnalysis.rpt" Then

                    ht.Add("BlockCodeId", Val(_Block))
                    ht.Add("PKWardID", Val(_Ward))
                End If


            End If


            'End If

            objReport.strFileName = report
            HttpContext.Current.Session("ReportFileName") = report


            'ht.Add("ToDate", ToDate)
            'ht.Add("User", HttpContext.Current.Session("UserID"))
            'ht.Add("CompanyID", HttpContext.Current.Session("CompanyID"))

            If dr.Tables(0).Rows(0)("IsSelectionFormula").ToString() = "1" Then
                SelctionFormulaDS = obj.GetReportSelectionFormula(report)
                selectionsql = SetCritria(report, SelctionFormulaDS, remodule.ReportPath)
                'selectionsql = selectionsqlCode & " " & selectionsql

                objReport.Formula = selectionsql(0)
                objReport.rptCreteria = selectionsql(1)
            Else

            End If
            'selectionsqlCode = SetCritriaCode(id)


            HttpContext.Current.Session("ogsReport") = objReport
            HttpContext.Current.Session("credentials") = remodule

            Dim iAction As String = ""
            iAction = "Report Generated" & " " & dr.Tables(0).Rows(0)("ReportName").ToString() & " With Selection Formula" & " " & objReport.rptCreteria
            BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, report, "Report", "", Environment.MachineName)

            objReport.ht = ht
            objReport.StrList = StrList

            Return "1"
        Catch ex As Exception
            Dim ser As New JavaScriptSerializer()
            Return ser.Serialize(ex.Message)
        End Try
    End Function

    Public Function SetCritriaCode(ByVal list() As String) As String
        Dim FromDate As String = HttpContext.Current.Request.QueryString("txtFromDate")
        Dim ToDate As String = HttpContext.Current.Request.QueryString("txtToDate")

        Dim i As Integer
        Dim mAccountCode As String
        'Dim ObjACt As New BizSoft.Bridge_SCM.Task

        Dim selectionsqlCode As String = ""
        Dim AndFlag As Boolean = False
        Dim T As Integer

        selectionsqlCode = " {VnPropertyStock.Dated} >=date(" & Year(FromDate) & "," & Month(FromDate) & ", " & Day(FromDate) & ")  AND  {VnPropertyStock.Dated} <=date(" & Year(ToDate) & "," & Month(ToDate) & ", " & Day(ToDate) & ") "
        AndFlag = True
        If T = 0 Then
            Return selectionsqlCode
        Else
            T = list.Count

            For i = 0 To T - 1
                '   mAccountCode = ObjACt.StripID(Trim(list(i)))

                'If AndFlag = False Then
                '  selectionsqlCode = "({VnPropertyStock.ProjectLocation} = '" & mAccountCode & "'"
                '   AndFlag = True
                'Else
                selectionsqlCode = selectionsqlCode & " AND {VnPropertyStock.ProjectLocation} = '" & mAccountCode & "' OR"
                'End If
            Next i
            selectionsqlCode = selectionsqlCode & ")"

            Return selectionsqlCode
        End If
    End Function

    Public Function SetCritria(ByVal reportsName As String, ByRef SFList As DataSet, _ReportPath As String) As Object

        Dim _NA As String = HttpContext.Current.Request.QueryString("ddlNA")
        Dim _PA As String = HttpContext.Current.Request.QueryString("ddlPA")
        Dim _UC As String = HttpContext.Current.Request.QueryString("ddlUC")
        Dim _Ward As String = HttpContext.Current.Request.QueryString("ddlWard")
        Dim _Block As String = HttpContext.Current.Request.QueryString("ddlBlock")
        Dim _Area As String = HttpContext.Current.Request.QueryString("ddlArea")
        Dim _Street As String = HttpContext.Current.Request.QueryString("ddlStreet")
        Dim _FromDate As String = HttpContext.Current.Request.QueryString("Fromdate")
        Dim _ToDate As String = HttpContext.Current.Request.QueryString("Todate")
        Dim _FromTime As String = HttpContext.Current.Request.QueryString("Fromtime")
        Dim _ToTime As String = HttpContext.Current.Request.QueryString("Totime")
        Dim _ComboBox1 As String = HttpContext.Current.Request.QueryString("cmb1")
        Dim _ComboBox2 As String = HttpContext.Current.Request.QueryString("cmb2")
        Dim _ComboBox3 As String = HttpContext.Current.Request.QueryString("cmb3")
        Dim _ComboBox4 As String = HttpContext.Current.Request.QueryString("cmb4")
        Dim _TextBox1 As String = HttpContext.Current.Request.QueryString("txt1")
        Dim _TextBox2 As String = HttpContext.Current.Request.QueryString("txt2")
        Dim _TextBox3 As String = HttpContext.Current.Request.QueryString("txt3")
        Dim _TextBox4 As String = HttpContext.Current.Request.QueryString("txt4")
        Dim selectionsql As String = ""
        Dim selectionFormula As String = ""
        Dim AndFlag As Boolean = False
        'changes
        Dim reportDocument As ReportDocument = New ReportDocument()
        reportDocument.Load(Path.Combine(HttpContext.Current.Server.MapPath(_ReportPath), reportsName))

        If SFList.Tables(0).Rows.Count > 0 Then
            For Each item As DataRow In SFList.Tables(0).Rows
                If (item("ObjectName") = "FromDate") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + _FromDate
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + _FromDate
                    End If
                ElseIf (item("ObjectName") = "ToDate") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + _ToDate
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + _ToDate
                    End If
                ElseIf (item("ObjectName") = "FromTime") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + _FromTime
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + _FromTime
                    End If
                ElseIf (item("ObjectName") = "ToTime") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + _ToTime
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + _ToTime
                    End If
                ElseIf (item("ObjectName") = "cmb1") Then
                    If (reportDocument.RecordSelectionFormula = "" And _ComboBox1 <> "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + IIf(_ComboBox1 = "-1", "", "[" + _ComboBox1 + "]")
                    Else
                        If _ComboBox1 <> "" And _ComboBox1 <> "-1" Then
                            reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + IIf(_ComboBox1 = "-1", "", "[" + _ComboBox1 + "]")
                        Else
                            reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula
                        End If
                    End If
                ElseIf (item("ObjectName") = "cmb2") Then
                    If (reportDocument.RecordSelectionFormula = "" And _ComboBox2 <> "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + IIf(_ComboBox2 = "-1", "", "[" + _ComboBox2 + "]")
                    Else
                        If _ComboBox2 <> "" And _ComboBox2 <> "-1" Then
                            reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + IIf(_ComboBox2 = "-1", "", "[" + _ComboBox2 + "]")
                        Else
                            reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula
                        End If
                    End If
                ElseIf (item("ObjectName") = "cmb3") Then
                    If (reportDocument.RecordSelectionFormula = "" And _ComboBox3 <> "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + IIf(_ComboBox3 = "-1", "", "[" + _ComboBox3 + "]")
                    Else
                        If _ComboBox3 <> "" And _ComboBox3 <> "-1" Then
                            reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + IIf(_ComboBox3 = "-1", "", "[" + _ComboBox3 + "]")
                        Else
                            reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula
                        End If
                    End If
                ElseIf (item("ObjectName") = "cmb4") Then
                    If (reportDocument.RecordSelectionFormula = "" And _ComboBox4 <> "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + IIf(_ComboBox4 = "-1", "", "[" + _ComboBox4 + "]")
                    Else
                        If _ComboBox4 <> "" And _ComboBox4 <> "-1" Then
                            reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + IIf(_ComboBox4 = "-1", "", "[" + _ComboBox4 + "]")
                        Else
                            reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula
                        End If
                    End If
                ElseIf (item("ObjectName") = "txt1") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + IIf(_TextBox1 = "", "", _TextBox1)
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + IIf(_TextBox1 = "", "", _TextBox1)
                    End If
                ElseIf (item("ObjectName") = "txt2") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + IIf(_TextBox2 = "", "", _TextBox2)
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + IIf(_TextBox2 = "", "", _TextBox2)
                    End If
                ElseIf (item("ObjectName") = "TextBox3") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + IIf(_TextBox3 = "", "", _TextBox3)
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + IIf(_TextBox3 = "", "", _TextBox3)
                    End If
                ElseIf (item("ObjectName") = "TextBox4") Then
                    If (reportDocument.RecordSelectionFormula = "") Then
                        reportDocument.RecordSelectionFormula = item("SelectionFormula") + IIf(_TextBox4 = "", "", _TextBox4)
                    Else
                        reportDocument.RecordSelectionFormula = reportDocument.RecordSelectionFormula + " and " + item("SelectionFormula") + IIf(_TextBox4 = "", "", _TextBox4)
                    End If
                End If
            Next
            ' For i = 0 To SFList.Tables(0).Rows.Count - 1

            'Dim ObjectValue As String = System.Net.WebUtility.HtmlDecode(HttpContext.Current.Request.QueryString(SelctionFormulaDS.Tables(0).Rows(i)("ObjectName")))
            'Dim ParameterName As String = SelctionFormulaDS.Tables(0).Rows(i)("ParameterName")

            'If ObjectValue = "-1" Or ObjectValue = "undefined" Or ObjectValue = "" Or ObjectValue = " " Then
            '    Return SelctionFormulaDS.Tables(0).Rows(i)("Msg")
            'End If


            'ht.Add(ParameterName, ObjectValue)
            'Next
        End If
        selectionFormula = reportDocument.RecordSelectionFormula
        'changes


        Return {selectionsql, selectionFormula}
    End Function
End Class
