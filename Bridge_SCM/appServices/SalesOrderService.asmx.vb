﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class SalesOrderService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function loadGrid() As String
        Try
            Dim list As New List(Of SalesOrderModel)
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim ds As DataSet = obj.GetList()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New SalesOrderModel() With { _
                      .ID = row("PK_SalesOrder").ToString(), _
                      .Fname = row("Firstname").ToString(), _
                      .SalesOrder = row("SalesOrder").ToString(), _
                      .SalesDate2 = row("SalesDate").ToString(), _
                      .FK_CustomerID = row("FK_CustomerID").ToString(), _
                      .BillTo = row("BillTo").ToString(), _
                      .ShipTo = row("ShipTo").ToString(), _
                      .OrderType = row("OrderType").ToString(), _
                      .OrderRef = row("OrderRef").ToString(), _
                      .OrderRefDate = row("OrderRef").ToString(), _
                      .FK_EmployeeID = row("FK_EmployeeID").ToString(), _
                      .FK_LocationID = row("FK_LocationID").ToString(), _
                      .Tax = row("Tax").ToString(), _
                      .Charges = row("Charges").ToString(), _
                      .Remarks = row("Remarks").ToString(), _
                      .Billing = row("Billing").ToString(), _
                      .Assembly = row("Assembly").ToString(), _
                      .Delivery = row("Delivery").ToString(), _
                      .CompanyID = row("CompanyID").ToString(), _
                      .FK_Terms = row("FK_Terms").ToString(), _
                      .FK_CurrencyID = row("FK_CurrencyID").ToString(), _
                      .ExchangeRate = row("ExchangeRate").ToString(), _
                      .FK_StatusID = row("FK_StatusID").ToString(), _
                      .StatusDate = row("StatusDate").ToString(), _
                      .Status = row("StatusName").ToString(), _
                      .SalesPerson = row("SalesPerson").ToString(), _
                      .StatusRemarks = row("StatusRemarks").ToString()
                    })
                Next
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetLocationList() As String

        Dim obj As New BizSoft.Bridge_SCM.clsLogin()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.FillLocation()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("ID").ToString, _
                      .Text = row("Name").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetCurrencyList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetCurrency()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("ID").ToString, _
                      .Text = row("Name").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetTermsList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetTerms()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("ID").ToString, _
                      .Text = row("Name").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetSOTypeList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetSOType()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("ID").ToString, _
                      .Text = row("Description").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetCustomerList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetDealerRecord()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("ID").ToString, _
                      .Text = row("Name").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetSalesPersonList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetSalesPerson()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("EmployeeID").ToString, _
                      .Text = row("FirstName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetStatusList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetStatus()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("StatusID").ToString, _
                      .Text = row("StatusName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function OnCustomerChange(ID As String) As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetCustomerDetail(ID)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Text = row("Address").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function OnSave(model As SalesOrderModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim ResultSet

            If model.ID = "" Then
                objModel.Message = obj.Add(model, intSatatus, msg)
                objModel.ID = 0
            Else
                ResultSet = obj.Update(model, intSatatus, msg)
                objModel.ID = model.ID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function OnSaveDetail(model As SalesOrderModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder

            Dim intSatatus As Boolean = 0
            Dim msg As String = ""

            If model.ID = "" Then
                objModel.Message = obj.AddDetail(model, intSatatus, msg)
                objModel.ID = 0
            Else
                objModel.Message = obj.UpdateDetail(model, intSatatus, msg)
                objModel.ID = model.ID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function loadGridDetail() As String
        Try
            Dim list As New List(Of SalesOrderModel)
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim ds As DataSet = obj.ItemList()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New SalesOrderModel() With { _
                      .ID = row("PK_Item").ToString(), _
                      .PartNumber = row("PartNumber").ToString(), _
                      .ModelNumber = row("ModelNumber").ToString(), _
                      .ProductName = row("ProductName").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function loadGridDetail2(ID As String) As String
        Try
            Dim list As New List(Of SalesOrderModel)
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim ds As DataSet = obj.GetItemDetail(ID)
            Dim a As Integer
            Dim b As Integer
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    a = row("BD_PRICE") - ((row("BD_PRICE") * row("DisCount_per")) / 100)
                    list.Add(New SalesOrderModel() With { _
                      .ID = row("ID").ToString(), _
                      .PartNumber = row("PartNumber").ToString(), _
                      .ModelNumber = row("ModelNumber").ToString(), _
                      .DefaultDesc = row("DefaultDesc").ToString(), _
                      .CurrentDesc = row("CurrentDesc").ToString(), _
                      .Quantity = row("Quantity").ToString(), _
                      .Unit = row("Unit").ToString(), _
                      .UnitPrice = row("UnitPrice").ToString(), _
                      .Remarks = row("Remarks").ToString(), _
                      .BD_PRICE = row("BD_PRICE").ToString(), _
                      .FK_Item = row("FK_Item").ToString(), _
                      .DisCount_per = row("DisCount_per").ToString(), _
                      .PriceAfterDiscount = a, _
                      .Amount = a * row("Quantity")
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function FindData(ID As String) As String
        Try
            Dim list As New List(Of SalesOrderModel)
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim ds As DataSet = obj.FindData(ID)

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New SalesOrderModel() With { _
                      .Unit = row("Unit").ToString(), _
                      .BDPrice = row("SalePoint").ToString(), _
                      .PartNumber = row("PartNumber").ToString(), _
                      .ModelNumber = row("ModelNumber").ToString(), _
                      .DefaultDescription = row("Description").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetStock(ID As String, Datee As String, LocationID As String, CurrentID As String) As String
        Try
            Dim list As New List(Of SalesOrderModel)
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim Stock As Integer = obj.GetStkQtyLoc(ID, Datee, LocationID, CurrentID)


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(Stock)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDeleteDetail(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""

            If dataID <> "" Then
                objModel.Message = obj.DeleteDetail(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

End Class