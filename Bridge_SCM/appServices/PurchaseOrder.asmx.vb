﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization
Imports System.Reflection

<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class PurchaseOrder1
    Inherits System.Web.Services.WebService
    Private purchaseOrder As New BizSoft.Bridge_SCM.PurchaseOrder
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetAllPurchaseOrder() As String
        Try
            Dim list = GetAll()
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetAddress(item As String, type As String) As String
        Try
            Dim result As String = ""
            Dim master As New BizSoft.Bridge_SCM.Master
            If type = "From" Then
                result = master.GF_codeDiscription("Address±VWGUI_CompanyInformation±CompanyID = " & item)
            ElseIf type = "To" Then
                result = master.GF_codeDiscription("Address±Supplier±SupplierID = '" & item & "'")
            Else
                If item = 1 Then
                    result = master.GF_codeDiscription("Address±CompanySetup±CompanyID = " & item)
                Else
                    result = master.GF_codeDiscription("Address±Customer±CustomerID = " & item)
                End If
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(result)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetProducts() As String
        Try
            Dim list As New List(Of PurchaseOrderProduct)
            Dim ds As DataSet = purchaseOrder.GetAllProducts()
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New PurchaseOrderProduct() With {
                      .ID = row("PK_Item").ToString(),
                      .PartNumber = row("PartNumber").ToString(),
                      .ModelNumber = row("ModelNumber").ToString(),
                      .ProductName = row("ProductName").ToString(),
                      .ItemClass = row("ItemClass").ToString()
                    })
                Next
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FindProductDetail(ID As String) As String
        Try
            Dim list As New List(Of PurchaseOrderProduct)
            Dim ds As DataSet = purchaseOrder.GetDetailById(ID)

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New PurchaseOrderProduct() With {
                      .ID = row("PK_Item").ToString(),
                      .Unit = row("Unit").ToString(),
                      .PartNumber = row("PartNumber").ToString(),
                      .ModelNumber = row("ModelNumber").ToString(),
                      .DefaultDescription = row("Description").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function AddPO() As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Purchase Order", "isAdd", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            Else
                objModel.Status = True
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function SavePurchaseOrder(model As PurchaseOrderModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            'Dim result As String = ""
            Dim jsSerializer As New JavaScriptSerializer()
            Dim productInformationList = jsSerializer.Deserialize(Of List(Of PurchaseOrderDetail))(HttpContext.Current.Request.QueryString("detail"))

            purchaseOrder.AddPurchaseOrder(model, productInformationList)
            Dim list = GetAll()
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function UpdatePurchaseOrderStatus(ByVal reason As String, ByVal detail As String, ByVal id As String) As String
        Try

            Dim message = purchaseOrder.UpdatePOStatus(reason, detail, id)
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(message)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetPuchaseInvoices() As String
        Try
            Dim list As New List(Of PurchaseOrderModel)
            Dim ds As DataSet = purchaseOrder.GetList()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New PurchaseOrderModel() With {
                    .PurchaseOrderDate = row("PODate").ToString(),
                    .PurchaseOrderNumber = row("PONumber").ToString(),
                    .ToName = row("ToName").ToString(),
                    .NUmber = row("Number"),
                    .PurchaseOrder = row("PK_PurchaseOrder")
                    })
                Next
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function


    Private Function GetAll() As List(Of PurchaseOrderModel)
        Dim list As New List(Of PurchaseOrderModel)
        Dim ds As DataSet = purchaseOrder.GetAllPurchaseOrder()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New PurchaseOrderModel() With {
                      .PurchaseOrderID = row("PK_PurchaseOrder").ToString(),
                      .PurchaseOrderNumber = row("PONumber").ToString(),
                      .PurchaseOrderDate = row("PODate").ToString(),
                      .ToAddress = row("ToName").ToString()
                    })
            Next
        End If
        Return list
    End Function


    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function deletePurchaseOrder(ByVal dataID As String, ByVal name As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Integer

            If dataID <> "" Then
                objModel.Message = purchaseOrder.Delete(dataID, name, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getPurchaseOrderById(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Purchase Order", "isEdit", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim po As New PurchaseOrderModel

                If id <> "" Then
                    Dim ds As DataSet = purchaseOrder.getById(id)
                    If ds.Tables(0).Rows.Count > 0 Then
                        po.PurchaseOrderID = ds.Tables(0).Rows(0)("PK_PurchaseOrder")
                        po.PurchaseOrderNumber = ds.Tables(0).Rows(0)("PONumber")
                        po.PurchaseOrderDate = ds.Tables(0).Rows(0)("PODate")
                        po.Currency = ds.Tables(0).Rows(0)("FK_CurrencyID")
                        po.ExchangeRate = ds.Tables(0).Rows(0)("ExchangeRate")
                        po.FromBillTo = ds.Tables(0).Rows(0)("FK_FromBillTo").ToString()
                        po.FromBillToAddress = ds.Tables(0).Rows(0)("FromBillTo").ToString()
                        po._To = ds.Tables(0).Rows(0)("FK_To").ToString()
                        po.ToAddress = ds.Tables(0).Rows(0)("Too").ToString()
                        po.ShipTo = ds.Tables(0).Rows(0)("FK_ShipTo").ToString()
                        po.ShipToAddress = ds.Tables(0).Rows(0)("ShipTo").ToString()
                        po.Status = ds.Tables(0).Rows(0)("FK_StatusID").ToString()
                        po.Signature = ds.Tables(0).Rows(0)("FK_Signature").ToString()
                        po.Notes = ds.Tables(0).Rows(0)("Notes").ToString()
                        po.Comments = ds.Tables(0).Rows(0)("Comments").ToString()
                        po.SpotNumber = ds.Tables(0).Rows(0)("SpotNumber").ToString()
                        po.Company = ds.Tables(0).Rows(0)("CompanyID").ToString()
                        po.PurchaseOrderReference = ds.Tables(0).Rows(0)("PORef").ToString()
                        po.BillDate = ds.Tables(0).Rows(0)("BillDate").ToString()

                    End If
                End If

                Dim list As New List(Of PurchaseOrderDetail)
                Dim detailDs As DataSet = purchaseOrder.GetDetailsById(id)

                If detailDs.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In detailDs.Tables(0).Rows
                        list.Add(New PurchaseOrderDetail() With {
                          .ItemId = row("FK_Item").ToString(),
                          .ModalNumber = row("ModelNumber").ToString(),
                          .PartNumber = row("PartNumber").ToString(),
                          .Description = row("Description").ToString(),
                          .Unit = row("Unit").ToString(),
                          .Quantity = row("Quantity").ToString(),
                          .UnitPrice = row("UnitPrice").ToString(),
                          .OtherCost = row("OtherCost").ToString()
                        })
                    Next
                End If

                Dim poStatus As DataSet = purchaseOrder.GetPurchaseOrderStatus(id)
                If poStatus.Tables(0).Rows.Count > 0 Then
                    po.Reason = poStatus.Tables(0).Rows(0)("FK_Reason")
                    po.ReasonDetail = poStatus.Tables(0).Rows(0)("Detail")
                End If

                Dim resultSet As New ResultSet() With
               {
               .PurchaseOrder = po,
               .PODetail = list
                }
                objModel.PurchaseOrder = po
                objModel.GenericList = list
                objModel.Status = False
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function generatePurchaseOrderReport(ByVal POID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim Message As String = ""
        Try
            Dim ht As New Hashtable

            Dim report As String = CommonConstant.purchaseOrderReport

            Dim objReport As New BizSoft.DBManager.clsReport.oReport


            objReport.strFileName = report
            HttpContext.Current.Session("ReportFileName") = report
            Dim StrList As New Generic.List(Of String)

            'ht.Add("isDisplayGrid", True)
            ht.Add("POID", POID)

            HttpContext.Current.Session("ogsReport") = objReport

            objReport.ht = ht
            objReport.StrList = StrList
            Dim iAction As String = ""
            iAction = "Generate Purchase Order Report"
            BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Purchase Order", "VWRPT_INV_PurchaseOrder", "", Environment.MachineName)

            Return "1"
        Catch ex As Exception
            BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, ex.Message, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Purchase Order", "VWRPT_INV_PurchaseOrder", "", Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
            Message = ex.Message
            Return Message
        End Try


    End Function
End Class