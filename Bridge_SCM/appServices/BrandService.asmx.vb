﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class BrandService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGrid() As String
        Try
            Dim objModel As New MessageModel
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Business Segment", "isView", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to view this record"
                objModel.Status = False
                isValidUser = False
            End If

            Dim list As New List(Of BrandModel)
            If isValidUser Then
                Dim objBankTitle As New BizSoft.Bridge_SCM.clsBrand()
                Dim ds As DataSet = objBankTitle.GetList()

                If ds.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In ds.Tables(0).Rows
                        list.Add(New BrandModel() With {
                          .ID = row("ID").ToString(),
                          .SegmentCode = row("SegmentCode").ToString(),
                          .SegmentName = row("SegmentName").ToString()
                        })
                    Next
                End If
                objModel.GenericList = list
                objModel.Status = True
            End If



            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function AddEditBusinessSegment(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = False
            If BizSoft.Utilities.HasRights("Business Segment", If(id = "0", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim brand As New BrandModel
                Dim brandModel As New BizSoft.Bridge_SCM.clsBrand()
                If id <> "0" Then
                    Dim ds As DataSet = brandModel.GetById(id)
                    If ds.Tables(0).Rows.Count > 0 Then
                        brand.ID = ds.Tables(0).Rows(0)("SegmentID").ToString()
                        brand.SegmentCode = ds.Tables(0).Rows(0)("SegmentCode").ToString()
                        brand.SegmentName = ds.Tables(0).Rows(0)("SegmentName").ToString()
                    End If
                End If
                objModel.GenericList = brand
                objModel.Status = True
            End If


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSave(model As BrandModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.clsBrand
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Long

            If model.ID = "" Then
                objModel.Message = obj.Add(0, model.SegmentCode, model.SegmentName, intSatatus, msg, NewID)
                objModel.ID = NewID
                objModel.Status = True
            Else
                objModel.Message = obj.Update(model.ID, model.SegmentCode, model.SegmentName, intSatatus, msg)
                objModel.ID = model.ID
                objModel.Status = True
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDelete(ByVal dataID As String, ByVal dataName As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.clsBrand()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Integer

            If dataID <> "" Then
                objModel.Message = obj.Delete(dataID, dataName, intSatatus, msg)
                objModel.ID = dataID
                objModel.Status = True
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

End Class