﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services
Imports System.Reflection

<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class GoodsReceivingNoteService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGrid() As String
        Try
            Dim list As New List(Of SalesOrderModel)
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim ds As DataSet = obj.GetList()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New SalesOrderModel() With {
                      .ID = row("PK_SalesOrder").ToString(),
                      .Fname = row("Firstname").ToString(),
                      .SalesOrder = row("SalesOrder").ToString(),
                      .SalesDate2 = row("SalesDate").ToString(),
                      .FK_CustomerID = row("FK_CustomerID").ToString(),
                      .BillTo = row("BillTo").ToString(),
                      .ShipTo = row("ShipTo").ToString(),
                      .OrderType = row("OrderType").ToString(),
                      .OrderRef = row("OrderRef").ToString(),
                      .OrderRefDate = row("OrderRef").ToString(),
                      .FK_EmployeeID = row("FK_EmployeeID").ToString(),
                      .FK_LocationID = row("FK_LocationID").ToString(),
                      .Tax = row("Tax").ToString(),
                      .Charges = row("Charges").ToString(),
                      .Remarks = row("Remarks").ToString(),
                      .Billing = row("Billing").ToString(),
                      .Assembly = row("Assembly").ToString(),
                      .Delivery = row("Delivery").ToString(),
                      .CompanyID = row("CompanyID").ToString(),
                      .FK_Terms = row("FK_Terms").ToString(),
                      .FK_CurrencyID = row("FK_CurrencyID").ToString(),
                      .ExchangeRate = row("ExchangeRate").ToString(),
                      .FK_StatusID = row("FK_StatusID").ToString(),
                      .StatusDate = row("StatusDate").ToString(),
                      .Status = row("StatusName").ToString(),
                      .SalesPerson = row("SalesPerson").ToString(),
                      .StatusRemarks = row("StatusRemarks").ToString()
                    })
                Next
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetDeliveredFromList() As String

        Dim obj As New BizSoft.Bridge_SCM.clsLogin()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.FillLocation()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Name").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetTypeList() As String

        Dim obj As New BizSoft.Bridge_SCM.GoodReceivingNote()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetTypeList()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("localID").ToString,
                      .Text = row("Description").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetCurrencyList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetCurrency()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Name").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetTermsList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetTerms()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Name").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetSOTypeList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetSOType()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Description").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetCustomerList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetDealerRecord()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Name").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetSalesPersonList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetSalesPerson()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("EmployeeID").ToString,
                      .Text = row("FirstName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetStatusList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetStatus()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("StatusID").ToString,
                      .Text = row("StatusName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnTypeChange(ID As String) As String

        Dim obj As New BizSoft.Bridge_SCM.GoodReceivingNote()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetType2(ID.Trim())
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Description").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnType2Change(ID As String, GRN As String, Type1 As String) As String

        Dim obj As New BizSoft.Bridge_SCM.GoodReceivingNote()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetType3(ID.Trim(), GRN, Type1)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("PK_Reference").ToString,
                      .Text = row("ReferenceNo").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnType3Change(ID As String, GRN As String, Type1 As String, ByVal flag As String) As String

        Dim obj As New BizSoft.Bridge_SCM.GoodReceivingNote()
        Dim list As New List(Of GoodsReceivingNoteModel)
        Dim rowCount As Integer = -1
        Dim quantityDs As DataSet = Nothing
        Dim quantity As String = ""
        Dim ds As DataSet = obj.loadGrid(ID.Trim(), GRN, Type1)

        If ds.Tables(0).Rows.Count > 0 Then
            ds.Tables(0).Columns.Add("row")
            If flag = "edit" Then
                quantityDs = obj.getQuantity(ID)
            End If
            For Each row As DataRow In ds.Tables(0).Rows
                rowCount += 1
                row("row") = rowCount
                If flag = "edit" Then
                    quantity = quantityDs.Tables(0).Rows(rowCount)("quantity").ToString()
                Else
                    quantity = ds.Tables(0).Rows(rowCount)("quantity").ToString()
                End If
                list.Add(New GoodsReceivingNoteModel() With {
                      .Reference = ID,
                      .ModelNumber = row("ModelNumber").ToString(),
                      .PartNumber = row("PartNumber").ToString(),
                      .Description = row("Description").ToString(),
                      .TotalQuantity = row("TotalQuantity").ToString(),
                      .Unit = row("Unit").ToString(),
                      .Received = row("Received").ToString(),
                      .Quantity = quantity,
                      .PK_Item = row("PK_Item").ToString(),
                      .FK_Reference = row("FK_Reference").ToString(),
                      .Stock = 0,
                      .rowID = rowCount
                      })

                Session("GRN") = ds
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSave(model As GoodsReceivingNoteModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Good Receiving Note", If(model.ID = "", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim jsSerializer As New JavaScriptSerializer()
                Dim grnDetailList = jsSerializer.Deserialize(Of List(Of GRNDetailModel))(HttpContext.Current.Request.QueryString("detail"))

                Dim obj As New BizSoft.Bridge_SCM.GoodReceivingNote
                Dim msg As String = ""

                Dim resultSet = obj.Add(model, grnDetailList, msg)
                objModel.ID = resultSet.ID
                objModel.Data = resultSet.Number
                objModel.Status = True
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getGRNById(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.GoodReceivingNote
            Dim GRN As New GoodsReceivingNoteModel
            If id <> "" Then
                Dim ds As DataSet = obj.getById(id)
                If ds.Tables(0).Rows.Count > 0 Then
                    GRN.ID = ds.Tables(0).Rows(0)("PK_GRN")
                    GRN.GRNNumber = ds.Tables(0).Rows(0)("GRNNO")
                    GRN.GRNType = ds.Tables(0).Rows(0)("ReceivedType")
                    GRN.GRNDate = ds.Tables(0).Rows(0)("GRNDate")
                    GRN.GRNSupplier = ds.Tables(0).Rows(0)("ReceivedFrom")
                    GRN.GRNReceived = ds.Tables(0).Rows(0)("FK_Received")
                    GRN.PurchaseOrder = ds.Tables(0).Rows(0)("FK_Received")
                    GRN.Remarks = ds.Tables(0).Rows(0)("Remarks")
                End If
            End If

            Dim list As New List(Of GRNDetailModel)
            Dim detailDs As DataSet = obj.GetDetailsById(id)

            If detailDs.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In detailDs.Tables(0).Rows
                    list.Add(New GRNDetailModel() With {
                      .Reference = row("FK_Reference").ToString(),
                      .Item = row("FK_Item").ToString(),
                      .Description = row("Description").ToString(),
                      .Quantity = row("ReceivedQuantity").ToString()
                    })
                Next
            End If

            Dim resultSet As New GRNResultSet() With
           {
           .GRN = GRN,
           .GRNDetail = list
            }
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(resultSet)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function SaveDetail(ByVal row As String, ByVal qty As String, ByVal masterID As String, DeliverdType As String) As String
        Dim objModel As New MessageModel
        Try


            Dim obj As New BizSoft.Bridge_SCM.GoodReceivingNote
            Dim ds As DataSet = Session("GRN")
            If ds.Tables(0).Rows.Count > 0 Then
                Dim strFilter As String = "row='" & row & "'"
                Dim oRow() As Data.DataRow = ds.Tables(0).Select(strFilter)
                ' Dim RecDate As String = Session("RecDate")
                objModel.Message = obj.SaveReceivedDetail(masterID, DeliverdType, oRow(0)("FK_Reference").ToString, oRow(0)("PK_Item").ToString, oRow(0)("Description").ToString, qty, oRow(0)("id").ToString)
                'objModel.Message = obj.UpdadeReceivedDetail(oRow(0)("FkBookingID").ToString, oRow(0)("Payment").ToString, NewAmount, RecDate)
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDelete(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.GoodReceivingNote()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""

            If dataID <> "" Then
                objModel.Message = obj.Delete(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSaveDetail(model As GoodsReceivingNoteModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.GoodReceivingNote

            Dim intSatatus As Boolean = 0
            Dim msg As String = ""

            If model.ID = "" Then
                objModel.Message = obj.AddDetail(model, intSatatus, msg)
                objModel.ID = 0
            Else
                objModel.Message = obj.UpdateDetail(model, intSatatus, msg)
                objModel.ID = model.ID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridDetail() As String
        Try
            Dim list As New List(Of GoodsReceivingNoteModel)
            Dim obj As New BizSoft.Bridge_SCM.GoodReceivingNote()
            Dim ds As DataSet = obj.ItemList()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New GoodsReceivingNoteModel() With {
                      .ID = row("Id").ToString(),
                      .GRNDate = row("GRNDate").ToString(),
                      .GRNNumber = row("GRNNo").ToString(),
                      .Received = row("ReceivedAt").ToString(),
                    .GRNReceived = row("ReceivedFromName").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridDetail2(ID As String) As String
        Try
            Dim list As New List(Of GoodsReceivingNoteModel)
            Dim obj As New BizSoft.Bridge_SCM.GoodReceivingNote()
            Dim ds As DataSet = obj.GetItemDetail(ID)
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New GoodsReceivingNoteModel() With {
                      .ID = row("ID").ToString(),
                      .FK_GDN = row("FK_GDN").ToString(),
                      .ReceivedType = row("ReceivedType").ToString(),
                      .FK_Refrence = row("FK_Refrence").ToString(),
                      .FK_Item = row("FK_Item").ToString(),
                      .SerialNumber_1 = row("SerialNumber_1").ToString(),
                      .OrderID = row("OrderID").ToString(),
                      .PartNumber = row("PartNumber").ToString(),
                      .ModelNumber = row("ModelNumber").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FindData(ID As String) As String
        Try
            Dim list As New List(Of SalesOrderModel)
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim ds As DataSet = obj.FindData(ID)

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New SalesOrderModel() With {
                      .Unit = row("Unit").ToString(),
                      .BDPrice = row("SalePoint").ToString(),
                      .PartNumber = row("PartNumber").ToString(),
                      .ModelNumber = row("ModelNumber").ToString(),
                      .DefaultDescription = row("Description").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetStock(ID As String, Datee As String, LocationID As String, CurrentID As String) As String
        Try
            Dim list As New List(Of SalesOrderModel)
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim Stock As Integer = obj.GetStkQtyLoc(ID, Datee, LocationID, CurrentID)


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(Stock)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDeleteDetail(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.GoodReceivingNote()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""

            If dataID <> "" Then
                objModel.Message = obj.DeleteDetail(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function generateGRNReport(ByVal GRNId As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Dim Message As String = ""
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Good Receiving Note", "isShow", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim ht As New Hashtable

                Dim report As String = CommonConstant.goodReceivingNoteReport
                Dim objReport As New BizSoft.DBManager.clsReport.oReport


                objReport.strFileName = report
                HttpContext.Current.Session("ReportFileName") = report
                Dim StrList As New Generic.List(Of String)

                ht.Add("User", HttpContext.Current.Session("UserName"))
                ht.Add("GRNID", GRNId)

                HttpContext.Current.Session("ogsReport") = objReport

                objReport.ht = ht
                objReport.StrList = StrList
                Dim iAction As String = ""
                iAction = "Generate Good Receiving Note Report"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Good Receiving Note", "VWRPT_INV_GRN and tblGRNSerialNumbers", "", Environment.MachineName)
                objModel.Status = True
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
            'Return "1"
        Catch ex As Exception
            BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, ex.Message, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Good Receiving Note", "VWRPT_INV_GRN and tblGRNSerialNumbers", "", Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
            Message = ex.Message
            Return Message
        End Try

    End Function

End Class