﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization

<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class UserService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridLocation() As String
        Try
            Dim list As New List(Of UserProfileModel)
            Dim obj As New BizSoft.Bridge_SCM.User()
            Dim ds As DataSet = obj.GetLocation()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New UserProfileModel() With {
                      .LocationID = row("LocationID").ToString,
                      .Location = row("Location").ToString,
                      .LocationCode = row("LocationCode").ToString,
                      .CompanyName = row("CompanyName").ToString
                      })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    ' GetUserRoleList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetUserRoleList() As List(Of DropdownModel)
        Dim objDR As New BizSoft.Bridge_SCM.DefineRole
        Dim dtDR As DataSet = objDR.GetList

        Dim ddlList As New List(Of DropdownModel)

        For Each dr As DataRow In dtDR.Tables(0).Rows
            Dim ddl As New DropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetEmployeeList() As List(Of DropdownModel)
        Dim objDR As New BizSoft.Bridge_SCM.User
        Dim dtDR As DataSet = objDR.GetEmployeeList

        Dim ddlList As New List(Of DropdownModel)

        For Each dr As DataRow In dtDR.Tables(0).Rows
            Dim ddl As New DropdownModel
            ddl.Text = dr("FirstName").ToString
            ddl.Value = dr("EmployeeID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function
    ' GetLocationList
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetLocationList() As List(Of DropdownModel)
        Dim objLoc As New BizSoft.Bridge_SCM.clsLogin
        Dim dtLoc As DataSet = objLoc.FillLocation

        Dim ddlList As New List(Of DropdownModel)

        For Each dr As DataRow In dtLoc.Tables(0).Rows
            Dim ddl As New DropdownModel
            ddl.Text = dr("Name").ToString
            ddl.Value = dr("ID").ToString
            ddlList.Add(ddl)
        Next
        Return ddlList
    End Function


    ' GetDetails
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetDetails() As List(Of String)
        Dim id As String = HttpContext.Current.Request.QueryString("id")
        Dim obj As New BizSoft.Bridge_SCM.User
        Dim ds As DataSet = obj.Edit(id)
        Dim list As New List(Of String)
        Dim pass As String = ""
        list.Add(ds.Tables(0).Rows(0)("UserID").ToString())
        'list.Add(ds.Tables(0).Rows(0)("LocationID"))
        list.Add(ds.Tables(0).Rows(0)("login").ToString())
        list.Add(ds.Tables(0).Rows(0)("RoleID").ToString())
        pass = obj.PerformCrypt(ds.Tables(0).Rows(0)("Password").ToString)
        list.Add(pass)
        list.Add(ds.Tables(0).Rows(0)("UserName").ToString())
        list.Add(ds.Tables(0).Rows(0)("isPowerUser").ToString())
        list.Add(ds.Tables(0).Rows(0)("CompanyID").ToString())
        list.Add(ds.Tables(0).Rows(0)("StatusID").ToString())
        If DBNull.Value.Equals(ds.Tables(0).Rows(0)("FK_EmployeeID").ToString()) Then
            list.Add("")
        Else
            list.Add(ds.Tables(0).Rows(0)("FK_EmployeeID").ToString())
        End If
        list.Add(ds.Tables(0).Rows(0)("LocationID").ToString())
        list.Add(ds.Tables(0).Rows(0)("IsRetrictUser").ToString())

        'list.Add(ds.Tables(0).Rows(0)("Fname").ToString())



        Return list
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetDetailsLocation() As String
        Dim list As New List(Of UserProfileModel)
        Dim id As String = HttpContext.Current.Request.QueryString("id")
        Dim obj As New BizSoft.Bridge_SCM.User
        Dim ds As DataSet = obj.GetUserDetail(id)

        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New UserProfileModel() With {
                  .ID = row("FK_Location").ToString
                  })
            Next
        End If



        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    ' Get View Button List
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetGridList() As String
        Try
            Dim objModel As New MessageModel
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("User Define", "isEdit", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim list As New List(Of DropdownModel)
                Dim obj As New BizSoft.Bridge_SCM.User
                Dim ds As DataSet = obj.GetList
                'Dim ds As DataSet = obj.GetList()

                If ds.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In ds.Tables(0).Rows
                        list.Add(New DropdownModel() With {
                          .Value = row("ID").ToString(),
                          .Text = row("Name").ToString()
                        })
                    Next
                End If
                objModel.GenericList = list
                objModel.Status = True
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
        'Dim obj As New BizSoft.Bridge_SCM.User
        'Dim ds As DataSet = obj.GetList
        'Dim rows As New List(Of Dictionary(Of String, Object))()
        'Dim row As Dictionary(Of String, Object)

        'For Each dr As DataRow In ds.Tables(0).Rows
        '    row = New Dictionary(Of String, Object)
        '    For Each col As DataColumn In ds.Tables(0).Columns
        '        row.Add(col.ColumnName, dr(col))

        '    Next
        '    rows.Add(row)
        'Next

        'Return rows
    End Function
    ' Save/Edit
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function Save() As String
        Dim objModel As New ProjectModel
        Dim objMessageModel As New MessageModel
        Dim action As String = HttpContext.Current.Request.QueryString("action")
        Dim code As String = HttpContext.Current.Request.QueryString("code")
        Dim login As String = HttpContext.Current.Request.QueryString("login")
        Dim role As String = HttpContext.Current.Request.QueryString("role")
        Dim password As String = HttpContext.Current.Request.QueryString("password")
        Dim rePassword As String = HttpContext.Current.Request.QueryString("rePassword")
        Dim userName As String = HttpContext.Current.Request.QueryString("userName")
        Dim IsPower As String = HttpContext.Current.Request.QueryString("IsPower")
        Dim Employee As String = HttpContext.Current.Request.QueryString("Employee")
        Dim Location As String = HttpContext.Current.Request.QueryString("Location")
        Dim IsRest As String = HttpContext.Current.Request.QueryString("IsRest")
        Dim obj As New BizSoft.Bridge_SCM.User

        Dim intSatatus As Boolean = 0
        Dim IsInvoice As Boolean = True
        Dim IsnotDisplay As String = "Null"
        Dim NewID As Long
        Dim strMsg As String = ""
        Dim mPower As String
        Dim mRest As String

        If IsPower = "1" Then
            mPower = "1"
        Else
            mPower = "0"
        End If

        If IsRest = "1" Then
            mRest = "1"
        Else
            mRest = "0"
        End If

        Dim isValidUser As Boolean = True
        If BizSoft.Utilities.HasRights("User Define", "isAdd", "1") = False Then
            objMessageModel.Message = "Sorry, you don't have sufficient rights to perform this action"
            objMessageModel.Status = False
            isValidUser = False
        End If

        If isValidUser Then
            If password.Trim = rePassword.Trim Then

                If action = "add" Then

                    obj.Add(login, password, userName, HttpContext.Current.Session("CompanyID"), "300", role, mPower, Employee, Location, mRest, intSatatus, strMsg, NewID)
                    code = NewID
                    Dim iAction As String = ""
                    iAction = "Add new Recored User"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Users", "tblUsers", "", Environment.MachineName)

                    If intSatatus Then
                        objMessageModel.ID = code
                        objMessageModel.Message = "Record has been saved successfully!"
                        objMessageModel.Status = True
                    End If
                    objModel.Msg = strMsg

                End If

                If action = "edit" Then
                    obj.Update(code, login, password, userName, HttpContext.Current.Session("CompanyID"), role, mPower, Employee, Location, mRest, intSatatus, strMsg)
                    Dim iAction As String = ""
                    iAction = "Edit Recored User"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Users", "tblUsers", "", Environment.MachineName)
                    objMessageModel.Message = "Record has been updated successfully!"
                    objMessageModel.ID = code
                    objMessageModel.Status = True
                End If
            Else
                objMessageModel.Message = "Re-Password must be same.."
            End If
        End If
        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(objMessageModel)

    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function SaveDetail() As ProjectModel
        Dim objModel As New ProjectModel

        Dim UserID As String = HttpContext.Current.Request.QueryString("UserID")
        Dim LocationID As String = HttpContext.Current.Request.QueryString("ID")
        Dim obj As New BizSoft.Bridge_SCM.User

        Dim intSatatus As Boolean = 0
        Dim IsInvoice As Boolean = True
        Dim IsnotDisplay As String = "Null"
        Dim NewID As Long
        Dim strMsg As String = ""
        Dim mPower As String



        obj.AddDetail(UserID, LocationID)

        Dim iAction As String = ""
        iAction = "Add new Recored User Location"
        BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Users", "User_Location", "", Environment.MachineName)

        If intSatatus Then
            objModel.Msg = "Record has been saved successfully!"
        End If
        objModel.Msg = strMsg

        Return objModel
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function DeleteDetail() As ProjectModel
        Dim objModel As New ProjectModel

        Dim UserID As String = HttpContext.Current.Request.QueryString("UserID")
        Dim obj As New BizSoft.Bridge_SCM.User

        Dim intSatatus As Boolean = 0
        Dim IsInvoice As Boolean = True
        Dim IsnotDisplay As String = "Null"
        Dim NewID As Long
        Dim strMsg As String = ""
        Dim mPower As String



        obj.DeleteDetail(UserID)

        Dim iAction As String = ""
        iAction = "Delete Recored User Location"
        BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Users", "User_Location", "", Environment.MachineName)

        If intSatatus Then
            objModel.Msg = "Record has been saved successfully!"
        End If
        objModel.Msg = strMsg

        Return objModel
    End Function
End Class