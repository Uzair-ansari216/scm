﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class UserActivityService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function loadGrid(ByVal FromDate As String, ByVal ToDate As String, ByVal User As String) As String
        Try
            Dim list As New List(Of UserActivityModel)
            Dim objBankTitle As New BizSoft.Bridge_SCM.UserActivity()
            Dim ds As DataSet = objBankTitle.GetList(FromDate, ToDate, User)

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New UserActivityModel() With { _
                      .User = row("iKey").ToString(), _
                      .Datee = row("iDate").ToString(), _
                      .Timee = row("iTime").ToString(), _
                      .Source = row("PCID").ToString(), _
                      .Action = row("iAction").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function loadGrid2() As String
        Try
            Dim list As New List(Of UserActivityModel)
            Dim objBankTitle As New BizSoft.Bridge_SCM.UserActivity()
            Dim ds As DataSet = objBankTitle.GetList2()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New UserActivityModel() With { _
                      .User = row("iKey").ToString(), _
                      .Datee = row("iDate").ToString(), _
                      .Timee = row("iTime").ToString(), _
                      .Source = row("PCID").ToString(), _
                      .Action = row("iAction").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function OnDelete(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.UserActivity()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Integer

            If dataID <> "" Then
                objModel.Message = obj.Delete(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetUsersList() As String

        Dim obj As New BizSoft.Bridge_SCM.UserActivity()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetUsers()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("UserID").ToString, _
                      .Text = row("UserName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
End Class