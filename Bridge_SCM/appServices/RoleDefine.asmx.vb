﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization

<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class RoleDefine
    Inherits System.Web.Services.WebService
    Public objSO As New BizSoft.Bridge_SCM.DefineRole()
    Dim ser As New JavaScriptSerializer()
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function viewRole() As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            Dim styleList As New List(Of DropdownModel)
            Dim DataSet = objSO.GetAllRole()
            If DataSet.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In DataSet.Tables(0).Rows
                    styleList.Add(New DropdownModel() With {
                          .Value = row("RoleID").ToString,
                          .Text = row("RoleName").ToString
                                  })
                Next
            End If
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(styleList)
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getRoleWisePermissions(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            Dim permissionList As New List(Of RoleDefineModel)
            Dim DataSet = objSO.GetPermissions(id)
            If DataSet.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In DataSet.Tables(0).Rows
                    permissionList.Add(New RoleDefineModel() With {
                          .RoleId = row("RoleID").ToString,
                          .RoleName = row("RoleName").ToString,
                          .IsAdd = Convert.ToBoolean(row("isAdd")),
                          .IsEdit = Convert.ToBoolean(row("isEdit")),
                          .IsDelete = Convert.ToBoolean(row("isDelete")),
                          .IsView = Convert.ToBoolean(row("isView")),
                          .IsShow = Convert.ToBoolean(row("isShow")),
                          .MenuId = row("MenuID").ToString
                                  })
                Next
            End If
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(permissionList)
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FillGridList() As List(Of Dictionary(Of String, Object))
        Dim objSO As New BizSoft.Bridge_SCM.DefineRole()
        Dim ds As DataSet = objSO.GetDetailRole()

        HttpContext.Current.Session("Role_Detail") = ds
        Dim rows As New List(Of Dictionary(Of String, Object))()
        Dim row As Dictionary(Of String, Object)

        For Each dr As DataRow In ds.Tables(0).Rows
            row = New Dictionary(Of String, Object)
            For Each col As DataColumn In ds.Tables(0).Columns
                row.Add(col.ColumnName, dr(col))

            Next
            rows.Add(row)
        Next

        Return rows
    End Function
    ' Refresh
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Sub Refresh()
        HttpContext.Current.Session.Add("Role_Detail", SalesOderDetail())

        Dim objSO As New BizSoft.Bridge_SCM.DefineRole()
        Dim ds As DataSet = objSO.GetDetailRole()

        HttpContext.Current.Session("Role_Detail") = ds

        ' GetDetailRole()
    End Sub
    ' View List
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FillViewGridList() As List(Of Dictionary(Of String, Object))
        Dim objSO As New BizSoft.Bridge_SCM.DefineRole()
        Dim ds As DataSet = objSO.GetList(HttpContext.Current.Session("CompanyID"))

        Dim rows As New List(Of Dictionary(Of String, Object))()
        Dim row As Dictionary(Of String, Object)

        For Each dr As DataRow In ds.Tables(0).Rows
            row = New Dictionary(Of String, Object)
            For Each col As DataColumn In ds.Tables(0).Columns
                row.Add(col.ColumnName, dr(col))

            Next
            rows.Add(row)
        Next

        Return rows
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function UpdateRows() As String
        If HttpContext.Current.Request.QueryString.Count > 0 Then

            Dim id As String = HttpContext.Current.Request.QueryString("id")
            If id <> "" Then
                If id = "1" Or id = "0" Then
                    CheckUncheck(id)
                End If
            End If
        End If

        Return ""
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Shared Function UpdateRowByID() As String
        If HttpContext.Current.Request.QueryString.Count > 0 Then
            Dim id As String = HttpContext.Current.Request.QueryString("id")
            Dim status As String = HttpContext.Current.Request.QueryString("status")
            Dim column As String = HttpContext.Current.Request.QueryString("column")
            If id <> "" Then
                RowUpdate(id, column, status)
            End If
        End If

        Return ""
    End Function

    'Save
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function Save(ByVal id As String, ByVal shipTo As String, ByVal action As String, model As List(Of Object)) As ValidateModel
        Dim objModel As New ValidateModel
        Dim intSatatus As Boolean = 0
        Dim strMsg As String = ""

        If action = "add" Then
            If id = "" Then
                id = objSO.Role_Master_Insert(shipTo, shipTo, HttpContext.Current.Session("CompanyID"), "1", intSatatus, strMsg)
                AddDetails(model, id)
                objModel.ID = id
                Dim iAction As String = ""
                iAction = "Add new Recored Role Define"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Role Define New", "Gl_Role_Master", id, Environment.MachineName)
                objModel.Msg = "Recored Saved Succcessfully"
            End If

        ElseIf action = "update" Then
            If id <> "" Then
                objSO.Role_Master_Update(id, shipTo, shipTo, HttpContext.Current.Session("CompanyID"), "1")
                Dim iAction As String = ""
                iAction = "Update Recored Role Define"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Role Define New", "Gl_Role_Master", id, Environment.MachineName)
                objSO.Role_Detail_Delete(id)
                AddDetails(model, id)
                objModel.ID = id
                objModel.Msg = "Recored Updated Succcessfully"
            End If
        End If
        Dim objLogin As New BizSoft.Bridge_SCM.clsLogin()
        Session("AccessRights") = objLogin.GetAllaccessbyUser(Session("UserID"))
        Return objModel
    End Function
    Public Shared Function SalesOderDetail() As DataTable
        Dim objSO As New BizSoft.Bridge_SCM.DefineRole()
        Dim dt As DataTable
        dt = objSO.CreateRoleDefineDatatable()
        Return dt
    End Function
    Public Shared Sub CheckUncheck(ByVal id As Boolean)
        Dim ds As DataSet = HttpContext.Current.Session("Role_Detail")
        For Each dr As DataRow In ds.Tables(0).Rows
            dr("isView") = id
            dr("isAdd") = id
            dr("isEdit") = id
            dr("isDelete") = id
            dr("IsActive") = id
        Next
    End Sub
    Public Shared Sub RowUpdate(ByVal filterID As String, ByVal column As String, ByVal status As String)
        Dim ds As DataSet = HttpContext.Current.Session("Role_Detail")
        Dim strFilter As String = "ID=" & filterID
        Dim oTempRow() As Data.DataRow = ds.Tables(0).Select(strFilter)
        Dim statusval As Boolean = False
        If status = "0" Then
            statusval = False
        Else
            statusval = True
        End If

        oTempRow(0)(column) = statusval
    End Sub
    Public Sub AddDetails(model As List(Of Object), ByVal ID As String)
        Dim mActive As Integer
        Dim mAdd As Integer
        Dim mEdit As Integer
        Dim mDelete As Integer
        Dim mPrint As Integer
        Dim i As Integer = 0
        For Each dr In model
            mActive = Convert.ToInt32(dr("IsShow"))
            mAdd = Convert.ToInt32(dr("IsAdd"))
            mEdit = Convert.ToInt32(dr("IsEdit"))
            mDelete = Convert.ToInt32(dr("IsDelete"))
            mPrint = Convert.ToInt32(dr("IsView"))
            objSO.RoleDetail_Insert(ID, dr("MenuName"), "1", mPrint, mAdd, mEdit, mDelete, mActive, dr("MenuId"))
            i = i + 1
        Next
    End Sub
End Class