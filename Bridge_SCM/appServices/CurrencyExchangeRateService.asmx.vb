﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class CurrencyExchangeRateService
    Inherits System.Web.Services.WebService

    Dim exchangeRateRepository As New BizSoft.Bridge_SCM.clsCurrencyExchangeRate()
    Dim dataSet As DataSet
    Dim ser As New JavaScriptSerializer()
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getAllCurrencies() As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            Dim currencyList As New List(Of DropdownModel)
            dataSet = exchangeRateRepository.GetCurrencies()
            If dataSet.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In dataSet.Tables(0).Rows
                    currencyList.Add(New DropdownModel() With {
                          .Value = row("PK_CurrencyID").ToString,
                          .Text = row("Currency").ToString
                          })
                Next
            End If
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(currencyList)
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function AddEditExchangeRate(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            Dim objModel As New MessageModel
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Currency Exchange Rate", If(id = "0", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim exchangeRate As New CurrencyExchangeRateModel
                If Convert.ToInt32(id) > 0 Then
                    dataSet = exchangeRateRepository.GetById(id)
                    If dataSet.Tables(0).Rows.Count > 0 Then
                        exchangeRate.Id = dataSet.Tables(0).Rows(0)("ID").ToString
                        exchangeRate.Currency = dataSet.Tables(0).Rows(0)("FK_CurrencyID").ToString
                        exchangeRate.ExchangeRate = dataSet.Tables(0).Rows(0)("CurrencyRate").ToString
                    End If
                End If

                Dim maxDate = exchangeRateRepository.GetMaxDate()

                objModel.GenericList = exchangeRate
                objModel.Data = maxDate.Tables(0).Rows(0)("Date").ToString
                objModel.Status = True
            End If
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function saveExchangeRate(rateVm As CurrencyExchangeRateModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            Dim messages As String = ""
            Dim status As Boolean = True
            Dim NewID As Long

            Dim resultSet = exchangeRateRepository.Save(rateVm, messages, status, NewID)
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(resultSet)
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getExchangeRate(ByVal fromDate As String, ByVal toDate As String, ByVal currency As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            Dim messages As String = ""
            Dim status As Boolean = True
            Dim NewID As Long

            dataSet = exchangeRateRepository.GetRatesByDate(fromDate, toDate, currency)
            Dim exchangeRateList As New List(Of CurrencyExchangeRateModel)
            If dataSet.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In dataSet.Tables(0).Rows
                    exchangeRateList.Add(New CurrencyExchangeRateModel() With {
                          .Id = row("ID").ToString,
                          .Currency = row("Currency").ToString,
                          .ExchangeRate = row("CurrencyRate").ToString,
                          .FromDate = row("date").ToString
                          })
                Next
            End If
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(exchangeRateList)
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function updateExchangeRate(ByVal id As String, ByVal rateDate As String, ByVal rate As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            Dim resultSet = exchangeRateRepository.UpdateRate(id, rateDate, rate)
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize("")
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
End Class