﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services
Imports System.Reflection

<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class SalesOrderNewService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGrid() As String
        Try
            Dim list As New List(Of SalesOrderModel)
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim ds As DataSet = obj.GetAll()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New SalesOrderModel() With {
                      .ID = row("PK_SalesOrder").ToString(),
                      .SalesOrder = row("SalesOrder").ToString(),
                      .FK_CustomerID = row("CustomerName").ToString(),
                      .SalesDate = row("SalesDate").ToString(),
                      .Charges = row("Number").ToString(),
                      .Remarks = row("Remarks").ToString()
                    })
                Next
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetLocationList() As String

        Dim obj As New BizSoft.Bridge_SCM.clsLogin()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.FillLocation()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Name").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetCurrencyList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetCurrency()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Name").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetTermsList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetTerms()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Name").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetSOTypeList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetSOType()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Description").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetCustomerList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetDealerRecord()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("ID").ToString,
                      .Text = row("Name").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetSalesPersonList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetSalesPerson()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("EmployeeID").ToString,
                      .Text = row("FirstName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetStatusList() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetStatus()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Value = row("StatusID").ToString,
                      .Text = row("StatusName").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnCustomerChange(ID As String) As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetCustomerDetail(ID)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With {
                      .Text = row("Address").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSave(model As SalesOrderModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Good Receiving Note", If(model.ID = "", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim obj As New BizSoft.Bridge_SCM.SalesOrder
                Dim intSatatus As Boolean = 0
                Dim msg As String = ""
                Dim NewID As Long
                Dim SONumber As String
                Dim resultSet
                If model.ID = "" Then
                    resultSet = obj.Add2(model, intSatatus, msg, NewID, SONumber)
                    If NewID > 0 Then
                        Dim dt As DataTable = Session("Items")
                        obj.DeleteDetail2(NewID)
                        If dt.Rows.Count > 0 Then
                            For Each row As DataRow In dt.Rows
                                objModel.Message = obj.AddDetail2(NewID, row("FK_Item").ToString(), row("DefaultDesc").ToString(), row("CurrentDesc").ToString(), row("Quantity").ToString(), row("UnitPrice").ToString(), row("Remarks").ToString(), row("BD_PRICE").ToString(), row("DisCount_per").ToString())
                            Next
                        End If
                    End If
                    objModel.ID = SONumber
                    objModel.IssueID = NewID
                    objModel.Status = True
                Else
                    resultSet = obj.Update(model, intSatatus, msg)
                    If objModel.Message = "update" Then
                        Dim dt As DataTable = Session("Items")
                        obj.DeleteDetail2(model.ID)
                        If dt.Rows.Count > 0 Then
                            For Each row As DataRow In dt.Rows
                                objModel.Message = obj.AddDetail2(model.ID, row("FK_Item").ToString(), row("DefaultDesc").ToString(), row("CurrentDesc").ToString(), row("Quantity").ToString(), row("UnitPrice").ToString(), row("Remarks").ToString(), row("BD_PRICE").ToString(), row("DisCount_per").ToString())
                            Next
                        End If
                        objModel.Message = "update"
                        objModel.ID = model.ID
                        objModel.Status = True
                    End If
                End If
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function getSaleOrderById(ByVal ID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Try
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder
            Dim SO As New SalesOrderModel
            Dim ds As DataSet = obj.getById(ID)
            If ds.Tables(0).Rows.Count > 0 Then
                SO.ID = ds.Tables(0).Rows(0)("PK_SalesOrder").ToString()
                SO.SalesOrder = ds.Tables(0).Rows(0)("SalesOrder").ToString()
                SO.SalesDate = ds.Tables(0).Rows(0)("SalesDate").ToString()
                SO.FK_CustomerID = ds.Tables(0).Rows(0)("FK_CustomerID").ToString()
                SO.BillTo = ds.Tables(0).Rows(0)("BillTo").ToString()
                SO.ShipTo = ds.Tables(0).Rows(0)("ShipTo").ToString()
                SO.OrderType = ds.Tables(0).Rows(0)("OrderType").ToString()
                SO.FK_EmployeeID = ds.Tables(0).Rows(0)("FK_EmployeeID").ToString()
                SO.FK_LocationID = ds.Tables(0).Rows(0)("FK_LocationID").ToString()
                SO.Tax = ds.Tables(0).Rows(0)("Tax").ToString()
                SO.Charges = ds.Tables(0).Rows(0)("Charges").ToString()
                SO.Remarks = ds.Tables(0).Rows(0)("Remarks").ToString()
                SO.Billing = ds.Tables(0).Rows(0)("Billing").ToString()
                SO.CompanyID = ds.Tables(0).Rows(0)("CompanyID").ToString()
                SO.ISLMR = ds.Tables(0).Rows(0)("ISLMR").ToString()
                SO.isSaleReturn = ds.Tables(0).Rows(0)("isSaleReturn").ToString()
                SO.isgst = ds.Tables(0).Rows(0)("isgst").ToString()
                SO.FK_Terms = ds.Tables(0).Rows(0)("FK_Terms").ToString()
                SO.FK_CurrencyID = ds.Tables(0).Rows(0)("FK_CurrencyID").ToString()
                SO.ExchangeRate = ds.Tables(0).Rows(0)("ExchangeRate").ToString()

            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(SO)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function


    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSaveDetail(model As SalesOrderModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder
            Dim dt As DataTable = Session("Items")



            If model.rowID = "" Then
                Dim dtrow As DataRow = dt.NewRow()
                dtrow("PartNumber") = model.PartNumber
                dtrow("ModelNumber") = model.ModelNumber
                dtrow("DefaultDesc") = model.DefaultDesc
                dtrow("CurrentDesc") = model.CurrentDesc
                dtrow("Quantity") = model.Quantity
                dtrow("Unit") = model.Unit
                dtrow("UnitPrice") = model.UnitPrice
                dtrow("Remarks") = model.Remarks
                dtrow("BD_PRICE") = model.BD_PRICE
                dtrow("FK_Item") = model.FK_Item
                dtrow("DisCount_per") = model.DisCount_per
                dt.Rows.Add(dtrow)
            Else
                Dim rows As DataRow() = dt.Select("rowID = '" & model.rowID & "'")
                rows(0)("PartNumber") = model.PartNumber
                rows(0)("ModelNumber") = model.ModelNumber
                rows(0)("DefaultDesc") = model.DefaultDesc
                rows(0)("CurrentDesc") = model.CurrentDesc
                rows(0)("Quantity") = model.Quantity
                rows(0)("Unit") = model.Unit
                rows(0)("UnitPrice") = model.UnitPrice
                rows(0)("Remarks") = model.Remarks
                rows(0)("BD_PRICE") = model.BD_PRICE
                rows(0)("FK_Item") = model.FK_Item
                rows(0)("DisCount_per") = model.DisCount_per

            End If
            'dt.Rows.Add(model.ID, model.PartNumber, model.ModelNumber, model.DefaultDesc, model.CurrentDesc, model.Quantity, model.Unit, model.UnitPrice, model.Remarks, model.BD_PRICE, model.FK_Item, model.DisCount_per)

            If dt.Rows.Count > 0 Then
                objModel.Message = "success"
            Else
                objModel.Message = "failed"
            End If


            'Dim intSatatus As Boolean = 0
            'Dim msg As String = ""

            'If model.ID = "" Then
            '    objModel.Message = obj.AddDetail(model, intSatatus, msg)
            '    objModel.ID = 0
            'Else
            '    objModel.Message = obj.UpdateDetail(model, intSatatus, msg)
            '    objModel.ID = model.ID
            'End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridDetail() As String
        Try
            Dim list As New List(Of SalesOrderModel)
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim ds As DataSet = obj.ItemList()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New SalesOrderModel() With {
                      .ID = row("PK_Item").ToString(),
                      .PartNumber = row("PartNumber").ToString(),
                      .ModelNumber = row("ModelNumber").ToString(),
                      .ProductName = row("ProductName").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridDetail2(ID As String) As String
        Try
            Dim list As New List(Of SalesOrderModel)
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim ds As DataSet = obj.GetItemDetail(ID)
            Dim a As Integer
            Dim b As Integer
            Dim rowCount As Integer = 0
            If ds.Tables(0).Rows.Count > 0 Then
                ds.Tables(0).Columns.Add("rowID", GetType(Integer))
                For Each row As DataRow In ds.Tables(0).Rows
                    rowCount += 1
                    row("rowID") = rowCount.ToString()
                    a = row("BD_PRICE") - ((row("BD_PRICE") * row("DisCount_per")) / 100)
                    list.Add(New SalesOrderModel() With {
                      .ID = row("ID").ToString(),
                      .PartNumber = row("PartNumber").ToString(),
                      .ModelNumber = row("ModelNumber").ToString(),
                      .DefaultDesc = row("DefaultDesc").ToString(),
                      .CurrentDesc = row("CurrentDesc").ToString(),
                      .Quantity = row("Quantity").ToString(),
                      .Unit = row("Unit").ToString(),
                      .UnitPrice = row("UnitPrice").ToString(),
                      .Remarks = row("Remarks").ToString(),
                      .BD_PRICE = row("BD_PRICE").ToString(),
                      .FK_Item = row("FK_Item").ToString(),
                      .DisCount_per = row("DisCount_per").ToString(),
                      .PriceAfterDiscount = a,
                      .rowID = row("rowID").ToString(),
                      .Amount = a * row("Quantity")
                    })
                Next
            End If
            Session("Items") = ds.Tables(0)
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridItems() As String
        Try
            Dim list As New List(Of SalesOrderModel)
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim dt As New DataTable

            dt.Columns.Add("ID", GetType(String))
            dt.Columns.Add("PartNumber", GetType(String))
            dt.Columns.Add("ModelNumber", GetType(String))
            dt.Columns.Add("DefaultDesc", GetType(String))
            dt.Columns.Add("CurrentDesc", GetType(String))
            dt.Columns.Add("Quantity", GetType(String))
            dt.Columns.Add("Unit", GetType(String))
            dt.Columns.Add("UnitPrice", GetType(String))
            dt.Columns.Add("Remarks", GetType(String))
            dt.Columns.Add("BD_PRICE", GetType(String))
            dt.Columns.Add("FK_Item", GetType(String))
            dt.Columns.Add("DisCount_per", GetType(String))
            dt.Columns.Add("rowID", GetType(Integer))

            'dt.Rows.Add(1, "Test", "data")
            'dt.Rows.Add(15, "Robert", "Wich")
            'dt.Rows.Add(18, "Merry", "Cylon")
            'dt.Rows.Add(30, "Tim", "Burst")
            'Dim ds As DataSet = obj.GetItemDetail(ID)
            Dim a As Integer
            Dim b As Integer
            If dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    a = row("BD_PRICE") - ((row("BD_PRICE") * row("DisCount_per")) / 100)
                    list.Add(New SalesOrderModel() With {
                      .ID = row("ID").ToString(),
                      .PartNumber = row("PartNumber").ToString(),
                      .ModelNumber = row("ModelNumber").ToString(),
                      .DefaultDesc = row("DefaultDesc").ToString(),
                      .CurrentDesc = row("CurrentDesc").ToString(),
                      .Quantity = row("Quantity").ToString(),
                      .Unit = row("Unit").ToString(),
                      .UnitPrice = row("UnitPrice").ToString(),
                      .Remarks = row("Remarks").ToString(),
                      .BD_PRICE = row("BD_PRICE").ToString(),
                      .FK_Item = row("FK_Item").ToString(),
                      .DisCount_per = row("DisCount_per").ToString(),
                      .PriceAfterDiscount = a,
                      .Amount = a * row("Quantity")
                    })
                Next
            End If
            Session("Items") = dt
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGridItems2() As String
        Try
            Dim list As New List(Of SalesOrderModel)
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim dt As DataTable = Session("Items")



            'dt.Rows.Add(1, "Test", "data")
            'dt.Rows.Add(15, "Robert", "Wich")
            'dt.Rows.Add(18, "Merry", "Cylon")
            'dt.Rows.Add(30, "Tim", "Burst")
            'Dim ds As DataSet = obj.GetItemDetail(ID)
            Dim a As Integer
            Dim b As Integer
            Dim rowCount As Integer = 0
            If dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    rowCount += 1
                    row("rowID") = rowCount.ToString()
                    a = row("BD_PRICE") - ((row("BD_PRICE") * row("DisCount_per")) / 100)
                    list.Add(New SalesOrderModel() With {
                      .ID = row("ID").ToString(),
                      .PartNumber = row("PartNumber").ToString(),
                      .ModelNumber = row("ModelNumber").ToString(),
                      .DefaultDesc = row("DefaultDesc").ToString(),
                      .CurrentDesc = row("CurrentDesc").ToString(),
                      .Quantity = row("Quantity").ToString(),
                      .Unit = row("Unit").ToString(),
                      .UnitPrice = row("UnitPrice").ToString(),
                      .Remarks = row("Remarks").ToString(),
                      .BD_PRICE = row("BD_PRICE").ToString(),
                      .FK_Item = row("FK_Item").ToString(),
                      .DisCount_per = row("DisCount_per").ToString(),
                      .PriceAfterDiscount = a,
                      .rowID = rowCount.ToString,
                      .Amount = a * row("Quantity")
                    })
                Next
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function FindData(ID As String) As String
        Try
            Dim list As New List(Of SalesOrderModel)
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim ds As DataSet = obj.FindData(ID)

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    list.Add(New SalesOrderModel() With {
                      .Unit = row("Unit").ToString(),
                      .BDPrice = row("SalePoint").ToString(),
                      .PartNumber = row("PartNumber").ToString(),
                      .ModelNumber = row("ModelNumber").ToString(),
                      .DefaultDescription = row("Description").ToString()
                    })
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetStock(ID As String, Datee As String, LocationID As String, CurrentID As String) As String
        Try
            Dim list As New List(Of SalesOrderModel)
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim Stock As Integer = obj.GetStkQtyLoc(ID, Datee, LocationID, CurrentID)


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(Stock)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDeleteDetail(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""

            If dataID <> "" Then
                objModel.Message = obj.DeleteDetail(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDeleteItem(ByVal ID As String, ByVal Item As String, ByVal DefaultDesc As String, ByVal Qty As String, ByVal UnitPrice As String, ByVal BDPrice As String) As String

        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
            Dim ds As DataTable = Session("Items")
            For Each dr As DataRow In ds.Rows
                If ID = "0" Then
                    If dr("FK_Item").ToString() = Item And dr("DefaultDesc").ToString() = DefaultDesc And dr("Quantity").ToString() = Qty And dr("UnitPrice").ToString() = UnitPrice And dr("BD_PRICE").ToString() = BDPrice Then
                        dr.Delete()
                    End If
                    ds.AcceptChanges()
                Else
                    If dr("ID").ToString() = ID And dr("FK_Item").ToString() = Item And dr("DefaultDesc").ToString() = DefaultDesc And dr("Quantity").ToString() = Qty And dr("UnitPrice").ToString() = UnitPrice And dr("BD_PRICE").ToString() = BDPrice Then
                        dr.Delete()
                    End If
                    ds.AcceptChanges()
                End If

            Next

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            'Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetCurrentLocation() As String
        Try

            Dim LocationId As String = HttpContext.Current.Session("LocationId")

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(LocationId)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetCurrentDate() As String
        Try

            Dim WorkingDate As String = HttpContext.Current.Session("WorkingDate")

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(WorkingDate)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetSelectedCurrency() As String

        Dim obj As New BizSoft.Bridge_SCM.SalesOrder()
        Dim ds As DataSet = obj.GetSelectedCurrency()
        Dim Currency As String = ""
        If ds.Tables(0).Rows.Count > 0 Then
            Currency = ds.Tables(0).Rows(0)(0).ToString()
        End If
        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(Currency)
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function generateSalesOrderReport(ByVal salesOrderId As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Dim Message As String = ""
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Sales Order", "isShow", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim ht As New Hashtable

                Dim report As String = CommonConstant.salesOrderReport
                Dim objReport As New BizSoft.DBManager.clsReport.oReport


                objReport.strFileName = report
                HttpContext.Current.Session("ReportFileName") = report
                Dim StrList As New Generic.List(Of String)

                ht.Add("User", HttpContext.Current.Session("UserName"))
                ht.Add("SOID", salesOrderId)

                HttpContext.Current.Session("ogsReport") = objReport

                objReport.ht = ht
                objReport.StrList = StrList
                Dim iAction As String = ""
                iAction = "Generate Sales Order Report"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Sales Order", "VWRPT_INV_SalesOrder", "", Environment.MachineName)
                objModel.Status = True
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
            'Return "1"
        Catch ex As Exception
            BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, ex.Message, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Sales Order", "VWRPT_INV_SalesOrder", "", Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
            Message = ex.Message
            Return Message
        End Try

    End Function
End Class