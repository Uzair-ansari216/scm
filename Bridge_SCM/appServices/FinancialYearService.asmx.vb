﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class FinancialYearService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function loadGrid() As String
        Try
            Dim objModel As New MessageModel
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Financial Year", "isView", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to view this record"
                objModel.Status = False
                isValidUser = False
            End If

            Dim list As New List(Of FinancialYearModel)
            If isValidUser Then
                Dim obj As New BizSoft.Bridge_SCM.clsFinancialYear()
                Dim ds As DataSet = obj.GetList()

                If ds.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In ds.Tables(0).Rows
                        list.Add(New FinancialYearModel() With {
                      .ID = row("ID").ToString(),
                      .yearTitle = row("yearTitle").ToString(),
                      .dateFrom = row("dateFrom").ToString(),
                      .dateTo = row("dateTo").ToString(),
                      .style = row("style").ToString()
                    })
                    Next
                End If
                objModel.GenericList = list
                objModel.Status = True
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function AddEditFinancialYear(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Financial Year", If(id = "0", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim financialYear As New FinancialYearModel
                Dim financialYearModel As New BizSoft.Bridge_SCM.clsFinancialYear()
                If id <> "0" Then
                    Dim ds As DataSet = financialYearModel.GetById(id)
                    If ds.Tables(0).Rows.Count > 0 Then
                        financialYear.ID = ds.Tables(0).Rows(0)("yearID").ToString()
                        financialYear.yearTitle = ds.Tables(0).Rows(0)("yearTitle").ToString()
                        financialYear.dateFrom = ds.Tables(0).Rows(0)("dateFrom").ToString()
                        financialYear.dateTo = ds.Tables(0).Rows(0)("dateTo").ToString()
                        financialYear.style = ds.Tables(0).Rows(0)("style").ToString()
                    End If
                End If
                objModel.GenericList = financialYear
                objModel.Status = True
            End If


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnSave(model As FinancialYearModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Financial Year", If(model.ID = "", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim obj As New BizSoft.Bridge_SCM.clsFinancialYear
                Dim intSatatus As Boolean = 0
                Dim msg As String = ""
                Dim NewID As Long

                If model.ID = "" Then
                    objModel.Message = obj.Add(0, model.yearTitle, model.dateFrom, model.dateTo, intSatatus, msg, NewID)
                    objModel.ID = NewID
                    objModel.Status = True
                Else
                    objModel.Message = obj.Update(model.ID, model.yearTitle, model.dateFrom, model.dateTo, intSatatus, msg)
                    objModel.ID = model.ID
                    objModel.Status = True
                End If
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function OnDelete(ByVal dataID As String, ByVal dataName As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Financial Year", "IsDelete", "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim obj As New BizSoft.Bridge_SCM.clsFinancialYear()
                Dim intSatatus As Boolean = 0
                Dim msg As String = ""

                If dataID <> "" Then
                    objModel.Message = obj.Delete(dataID, dataName, intSatatus, msg)
                    objModel.ID = dataID
                    objModel.Status = True
                End If
            End If
            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function


End Class