﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class ItemCategoryService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function loadGrid() As String
        Try
            Dim list As New List(Of ItemCategoryModel)
            Dim objBankTitle As New BizSoft.Bridge_SCM.clsItemCategory()
            Dim ds As DataSet = objBankTitle.GetList()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    If row("isActive") = True Then
                        list.Add(New ItemCategoryModel() With { _
                          .ID = row("PK_ItemCategory").ToString(), _
                          .ItemClass = row("ItemClass").ToString(), _
                          .Fk_ItemClass = row("Fk_ItemClass").ToString(), _
                          .ItemCategory = row("ItemCategory").ToString(), _
                          .Description = row("Description").ToString(), _
                          .isActive = "True"
                        })
                    Else
                        list.Add(New ItemCategoryModel() With { _
                         .ID = row("PK_ItemCategory").ToString(), _
                         .ItemClass = row("ItemClass").ToString(), _
                         .Fk_ItemClass = row("Fk_ItemClass").ToString(), _
                         .ItemCategory = row("ItemCategory").ToString(), _
                         .Description = row("Description").ToString(), _
                         .isActive = "False"
                       })
                    End If
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function OnSave(model As ItemCategoryModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.clsItemCategory
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Long

            If model.ID = "" Then
                objModel.Message = obj.Add(0, model.Fk_ItemClass, model.ItemCategory, model.Description, model.isActive, intSatatus, msg, NewID)
                objModel.ID = NewID
            Else
                objModel.Message = obj.Update(model.ID, model.Fk_ItemClass, model.ItemCategory, model.Description, model.isActive, intSatatus, msg)
                objModel.ID = model.ID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function OnDelete(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.clsItemCategory()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Integer

            If dataID <> "" Then
                objModel.Message = obj.Delete(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    '    <System.Web.Services.WebMethod(EnableSession:=True)> _
    '<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    '    Public Function GetResourceType() As String

    '        Dim obj As New BizSoft.Bridge_SCM.clsClassGroup()
    '        Dim list As New List(Of DropdownModel)
    '        Dim ds As DataSet = obj.GetList()
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            For Each row As DataRow In ds.Tables(0).Rows
    '                list.Add(New DropdownModel() With { _
    '                      .Value = row("ID").ToString, _
    '                      .Text = row("Name").ToString
    '                      })
    '            Next
    '        End If

    '        Dim ser As New JavaScriptSerializer()
    '        ser.MaxJsonLength = Integer.MaxValue
    '        Return ser.Serialize(list)
    '    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetGroupList() As String

        Dim obj As New BizSoft.Bridge_SCM.clsItemClass()
        Dim list As New List(Of DropdownModel)
        Dim ds As DataSet = obj.GetItemClass()
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                list.Add(New DropdownModel() With { _
                      .Value = row("PK_ItemClass").ToString, _
                      .Text = row("ItemClass").ToString
                      })
            Next
        End If

        Dim ser As New JavaScriptSerializer()
        ser.MaxJsonLength = Integer.MaxValue
        Return ser.Serialize(list)
    End Function

End Class