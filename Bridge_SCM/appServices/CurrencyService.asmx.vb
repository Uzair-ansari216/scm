﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class CurrencyService
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function loadGrid() As String
        Try
            Dim list As New List(Of CurrencyModel)
            Dim objBankTitle As New BizSoft.Bridge_SCM.clsCurrency()
            Dim ds As DataSet = objBankTitle.GetList()

            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    If row("DefaultCurrency") = True Then
                        list.Add(New CurrencyModel() With { _
                          .ID = row("ID").ToString(), _
                          .Currency = row("Currency").ToString(), _
                          .Symbol = row("Symbol").ToString(), _
                          .Unit = row("Unit").ToString(), _
                          .Description = row("Description").ToString(), _
                          .DefaultCurrency = "True"
                        })
                    Else
                        list.Add(New CurrencyModel() With { _
                         .ID = row("ID").ToString(), _
                         .Currency = row("Currency").ToString(), _
                         .Symbol = row("Symbol").ToString(), _
                         .Unit = row("Unit").ToString(), _
                         .Description = row("Description").ToString(), _
                         .DefaultCurrency = "False"
                       })
                    End If
                Next
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(list)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function AddEditCurrency(ByVal id As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim isValidUser As Boolean = True
            If BizSoft.Utilities.HasRights("Currency", If(id = "0", "isAdd", "isEdit"), "1") = False Then
                objModel.Message = "Sorry, you don't have sufficient rights to perform this action"
                objModel.Status = False
                isValidUser = False
            End If

            If isValidUser Then
                Dim currency As New CurrencyModel
                Dim currencyModel As New BizSoft.Bridge_SCM.clsCurrency()
                If id <> "0" Then
                    Dim ds As DataSet = currencyModel.GetById(id)
                    If ds.Tables(0).Rows.Count > 0 Then
                        currency.ID = ds.Tables(0).Rows(0)("ID").ToString()
                        currency.Currency = ds.Tables(0).Rows(0)("Currency").ToString()
                        currency.Symbol = ds.Tables(0).Rows(0)("Symbol").ToString()
                        currency.Unit = ds.Tables(0).Rows(0)("Unit").ToString()
                        currency.Description = ds.Tables(0).Rows(0)("Description").ToString()
                        currency.DefaultCurrency = ds.Tables(0).Rows(0)("DefaultCurrency").ToString()
                    End If
                End If
                objModel.GenericList = currency
                objModel.Status = True
            End If


            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function OnSave(model As CurrencyModel) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try
            Dim obj As New BizSoft.Bridge_SCM.clsCurrency
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Long

            If model.ID = "" Then
                objModel.Message = obj.Add(0, model.Currency, model.Symbol, model.Unit, model.Description, model.DefaultCurrency, intSatatus, msg, NewID)
                objModel.ID = NewID
            Else
                objModel.Message = obj.Update(model.ID, model.Currency, model.Symbol, model.Unit, model.Description, model.DefaultCurrency, intSatatus, msg)
                objModel.ID = model.ID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function OnDelete(ByVal dataID As String) As String
        Dim sessionStatus As Boolean = AppSession.CheckSession()
        If sessionStatus = False Then GoTo Line1 Else GoTo Line2
Line1:
        Return "sessionNotFound"
Line2:
        Dim objModel As New MessageModel
        Try

            Dim obj As New BizSoft.Bridge_SCM.clsCurrency()
            Dim intSatatus As Boolean = 0
            Dim msg As String = ""
            Dim NewID As Integer

            If dataID <> "" Then
                objModel.Message = obj.Delete(dataID, intSatatus, msg)
                objModel.ID = dataID
            End If

            Dim ser As New JavaScriptSerializer()
            ser.MaxJsonLength = Integer.MaxValue
            Return ser.Serialize(objModel)
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function


End Class