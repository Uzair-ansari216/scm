﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="UserActivity.aspx.vb" Inherits="Bridge_SCM.Brand" %>

<%@ Import Namespace="System.Web.Optimization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/UserActivityScript")%>



    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header card-header-text">
                    <h4 class="card-title">User Activity</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">From Date</label>
                                <input type="text" class="form-control" id="txtFromDate" name="txtFromDate" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">To Date</label>
                                <input type="text" class="form-control" id="txtToDate" name="txtToDate" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">User</label>
                                <select id="ddlUser" class="form-control">
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="btn btn-primary pull-right" onclick="Find()"><i class="fas fa-search"></i></button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <table id='tableCity' class="table table-striped table-bordered display">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>User</th>
                                        <th>Datee</th>
                                        <th>Timee</th>
                                        <th>Source</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <%--</form> --%>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="txtID" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="ValidationSummary" class="new badge"></span>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Description</label>
                                <input type="text" class="form-control" id="txtName" name="txtName" placeholder="" />
                                <span id="rName" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnSave()">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDelete()">Confirm</button>
                </div>
            </div>
        </div>
    </div>
    <script>

        function LoadDataTable(datasource) {
            table = $(tblname).DataTable({
                "searching": true,
                "bLengthChange": false,
                "sSort": true,
                "order": [0, 'desc'],
                "bInfo": false,
                dom: 'Bfrtip',
                buttons: [
                        {
                            extend: 'print',
                            footer: true,
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            },
                            title: 'User Activity',
                            text: '<i class="fas fa-print"></i>  Print'

                        },
                        {
                            extend: 'excelHtml5',
                            footer: true,
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            },
                            title: 'User Activity',
                            text: '<i class="fas fa-file-excel"></i>  Excel',
                        },
                       {
                           extend: 'pdf',
                           footer: true,
                           exportOptions: {
                               columns: [0, 1, 2, 3, 4]
                           },
                           title: 'User Activity',
                           text: '<i class="fas fa-file-pdf"></i>  PDF'
                       },
                ],
                "aaData": JSON.parse(datasource),
                "aoColumns": [

                                { data: "User", sDefaultContent: "" },
                                { data: "Datee", sDefaultContent: "" },
                                { data: "Timee", sDefaultContent: "" },
                                { data: "Source", sDefaultContent: "" },
                                { data: "Action", sDefaultContent: "" }

                ],
                "columnDefs": [
                    {
                        "targets": [1],
                        "type": "date",
                        "render": function (data) {
                            if (data !== null) {
                                var javascriptDate = new Date(data);
                                javascriptDate = javascriptDate.getMonth() + 1 + "/" + javascriptDate.getDate() + "/" + javascriptDate.getFullYear();
                                return javascriptDate;
                            } else {
                                return "";
                            }
                        }
                    }
                ]
            });
        }
    </script>
</asp:Content>
