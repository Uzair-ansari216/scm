﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RoleDefineNew.aspx.vb" Inherits="Bridge_SCM.RoleDefineNew" MasterPageFile="~/Forms/MasterPages/Main.Master" %>

<%@ Register Src="~/Forms/UserControls/RoleDefineList.ascx" TagName="ViewList" TagPrefix="RoleDefineList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- jquery -->
    <script type="text/javascript" src="../../js/JQGridReq/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../../js/JQGridReq/jquery-ui.js"></script>

    <!-- jqgrid -->

    <script type="text/javascript" src="../../js/jquery.jqGrid.js"></script>
    <script src="../../js/JQGridReq/numericInput.min.js"></script>


    <link href="../../js/JQGridReq/CustomCss.css" rel="stylesheet" />

    <script src="../../js/customMethods.js"></script>

    <style type="text/css">
        /*body {
            font-family: 'Slabo 27px', serif;
        }*/

        .parentitem {
            background: rgba(53, 195, 239, 0.32) !important;
        }

        .childitem {
            background-color: #f6fbfc;
        }
    </style>

    <script type="text/javascript">

        var gridname = "rolejqgrid";
        var edituri = "";
        var pagename = "RoleDefineNew.aspx";
        var serviceUrl = "../../appServices/RoleDefine.asmx";
        var _ddlDefaultPage = "#ddlDefaultPage";
        var count = 0;
        var griddata;

        function View() {
            $("#ModalView").dialog({
                title: 'Items List',
                draggable: false,
                resizable: false,
                width: 800,
                dialogClass: 'ui-dialog-osx'
            });

            if (count == 0) {
                $(".ui-dialog-titlebar").addClass('dialogheader');
                $(".ui-dialog-titlebar-close").append('<span class="glyphicon glyphicon-remove" id="removeicon" style="font-size:10px; color:#B96161"></span>');
                $("#ui-id-1").css('margin-top', '10px');
                count = 1;
            }

        }

        $(document).ready(function () {
            bindViewRole();
            bindSelectRole();
            $("#txtRoleID").attr('readonly', 'readonly');
        });
        function FillDefaultPage() {
            $.ajax({
                type: "POST",
                url: '' + pagename + '/fillDefaultPage',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var data = JSON.parse(response.d);
                    $(_ddlDefaultPage).empty();
                    $(_ddlDefaultPage).append("<option value='-1'>--Please Select--</option>");
                    jQuery.each(data, function (index, item) {
                        $(_ddlDefaultPage).append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });
                },
                error: OnErrorCall
            });

        }
        function OnErrorCall() {
            alert("Something went wrong")
        }
        function OnSelectAllUpdate(id) {
            console.log(id);
            if (id != "") {
                $.ajax({
                    type: "POST",
                    url: '' + serviceUrl + '/UpdateRows?id=' + id,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (res) {

                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
            }

        }

        function OnSelectUpdate(id, column, status) {
            console.log(id);
            if (id != "") {
                $.ajax({
                    type: "POST",
                    url: '' + serviceUrl + '/UpdateRowByID?id=' + id + '&column=' + column + '&status=' + status,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (res) {

                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        function selectall() {
            $('#rolejqgrid tbody tr').each(function (i, val) {
                $(val).children().eq(2).find("input[type='checkbox']").prop('checked', true);
                $(val).children().eq(3).find("input[type='checkbox']").prop('checked', true);
                $(val).children().eq(4).find("input[type='checkbox']").prop('checked', true);
                $(val).children().eq(5).find("input[type='checkbox']").prop('checked', true);
                $(val).children().eq(6).find("input[type='checkbox']").prop('checked', true);
            });
            OnSelectAllUpdate("1");
        }

        function gridResponsive() {
            var gridview = "gridview";
            var gridname = "rolejqgrid";
            var gridpagingname = "divpaging";
            $("#" + gridview + " .ui-jqgrid").css("width", "100%");
            $("#gview_" + gridname + "").css("width", "100%");
            $("#gview_" + gridname + "").css("width", "100%");
            $("#gview_" + gridname + " .ui-state-default").css("width", "100%");
            $("#gview_" + gridname + " .ui-jqgrid-htable").css("width", "100%");
            $("#gview_" + gridname + " .ui-jqgrid-bdiv").css("width", "100%");
            $("#" + gridname + "").css("width", "100%");
            $("#" + gridname + " tbody td[role='gridcell']").css("width", "10%");
            $("#" + gridname + " tbody td[role='gridcell']").css("width", "");
            $("#" + gridpagingname + "").css("width", "100%");
            $("#" + gridpagingname + "_left").css("width", "33%");
            $("#" + gridpagingname + "_center").css("width", "33%");
            $("#" + gridpagingname + "_right").css("width", "33%");
        }

        function unselectall() {
            $('#rolejqgrid tbody tr').each(function (i, val) {
                $(val).children().eq(2).find("input[type='checkbox']").prop('checked', false);
                $(val).children().eq(3).find("input[type='checkbox']").prop('checked', false);
                $(val).children().eq(4).find("input[type='checkbox']").prop('checked', false);
                $(val).children().eq(5).find("input[type='checkbox']").prop('checked', false);
                $(val).children().eq(6).find("input[type='checkbox']").prop('checked', false);

            });
            OnSelectAllUpdate("0");
        }

        // txtDate    txtSaleOrder  txtShipTo
        function Save() {
            var id = $("#txtRoleID").val();
            var shipTo = $("#txtShipTo").val();
            var action = $("#action").val();

            if (shipTo == "") {
                $("#FormMsg").empty();
                $("#FormMsg").removeClass("alert alert-success");
                $("#FormMsg").append("<span class='alert alert-danger'> Name required </span>");
                $("#txtShipTo").addClass("haserror");
            }

            else {
                var spFound;
                $.getScript('../../FormScript/ValidationScript.js', function () {
                    spFound = valid();
                    debugger
                    if (spFound == false) {
                        let permissions = new Array();
                        $("#rolejqgrid tbody tr").each(function (index, row) {
                            if (index > 0) {
                                let details = {
                                    MenuId: $(row).find("td:first").text(),
                                    MenuName: $(row).find("td:eq(1)").text(),
                                    IsView: $(row).find("td:eq(2) input:checked").length,
                                    IsAdd: $(row).find("td:eq(3) input:checked").length,
                                    IsEdit: $(row).find("td:eq(4) input:checked").length,
                                    IsDelete: $(row).find("td:eq(5) input:checked").length,
                                    IsShow: $(row).find("td:eq(6) input:checked").length,
                                }
                                permissions.push(details)
                            }
                        })
                        $.ajax({
                            type: "POST",
                            url: '' + serviceUrl + '/Save',
                            data: JSON.stringify({ id: id, shipTo: shipTo, action: action, model: permissions }),
                            contentType: "application/json",
                            dataType: "json",
                            success: function (res) {
                                var data = res.d;
                                if (data != null) {
                                    $("#FormMsg").empty();
                                    $("#FormMsg").append("<span class='alert alert-success'> " + data.Msg + " </span>");
                                    $("#txtRoleID").val(data.ID);
                                    $("#action").val("update");
                                }

                            },
                            failure: function (errMsg) {
                                alert(errMsg);
                            }
                        });
                    }
                    else {
                        alert("Illegal Characters Detected!")
                    }
                });
            }
        }

        function AddNew() {
            $("#txtDate").val("");
            $("#txtRoleID").val("");
            $("#txtShipTo").val("");
            $("#action").val("add");

            $.ajax({
                type: "POST",
                url: '' + serviceUrl + '/Refresh',
                contentType: "application/json",
                dataType: "json",
                success: function (res) {

                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });



        }

        $(document).on("click", "td[aria-describedby='" + gridname + "_isView'] input[type='checkbox']", function (e) {
            var rowId = $(e.target).closest("tr").attr("id")
            rowData = jQuery("#" + gridname + "").getRowData(rowId);
            var status = 0;
            console.log(rowData.isActive);
            if (rowData.isView == "Yes") {
                status = 1;
            } else { status = 0; }
            var id = rowData.ID;
            OnSelectUpdate(id, "isActive", status);

        });

        $(document).on("click", "td[aria-describedby='" + gridname + "_isAdd'] input[type='checkbox']", function (e) {
            var rowId = $(e.target).closest("tr").attr("id")
            rowData = jQuery("#" + gridname + "").getRowData(rowId);
            var status = 0;
            if (rowData.isAdd == "Yes") {
                status = 1;
            } else { status = 0; }
            var id = rowData.ID;
            OnSelectUpdate(id, "isAdd", status);

        });

        $(document).on("click", "td[aria-describedby='" + gridname + "_isDelete'] input[type='checkbox']", function (e) {
            var rowId = $(e.target).closest("tr").attr("id")
            rowData = jQuery("#" + gridname + "").getRowData(rowId);
            var status = 0;
            if (rowData.isDelete == "Yes") {
                status = 1;
            } else { status = 0; }
            var id = rowData.ID;
            OnSelectUpdate(id, "isDelete", status);

        });

        $(document).on("click", "td[aria-describedby='" + gridname + "_isPrint'] input[type='checkbox']", function (e) {
            var rowId = $(e.target).closest("tr").attr("id")
            rowData = jQuery("#" + gridname + "").getRowData(rowId);
            var status = 0;
            if (rowData.isPrint == "Yes") {
                status = 1;
            } else { status = 0; }
            var id = rowData.ID;
            OnSelectUpdate(id, "isView", status);

        });

        function CheckParent() {
            $('#rolejqgrid tbody tr').each(function (i, val) {
                var v = $(val).children().eq(6).text();
                if (v == "0") {
                    $(val).addClass("parentitem");
                } else {
                    $(val).addClass("childitem");
                }
            });
        }


        $(function () {
            $("#" + gridname + "").jqGrid({
                url: '' + serviceUrl + '/FillGridList',
                datatype: 'json',
                mtype: 'POST',

                serializeGridData: function (postData) {
                    return JSON.stringify(postData);
                },

                ajaxGridOptions: { contentType: "application/json" },
                loadonce: true,

                colNames: ['ID', 'Menu Description', 'Is View', 'Is Add', 'Is Edit', 'Is Delete', 'Is Print', ''],
                colModel: [


                                { name: 'ID', index: 'ID', editable: false, sortable: false, width: 10, },

                                {
                                    name: 'MenuName', index: 'Name', editable: true, sortable: false, width: 140,
                                    formoptions: { rowpos: 2, colpos: 1, label: 'Country Name' }
                                },

                                { // isActive 
                                    name: "isView", width: 40, align: "center", sortable: false, formatter: "checkbox", edittype: "checkbox",
                                    formatoptions: { disabled: false },
                                    editoptions: { value: "Yes:No", defaultValue: "Yes" },
                                },

                                 {
                                     name: "isAdd", width: 40, align: "center", formatter: "checkbox", sortable: false, edittype: "checkbox",
                                     formatoptions: { disabled: false },
                                     editoptions: { value: "Yes:No", defaultValue: "Yes" },
                                 },
                                  {
                                      name: "isEdit", width: 40, align: "center", formatter: "checkbox", sortable: false, edittype: "checkbox",
                                      formatoptions: { disabled: false },
                                      editoptions: { value: "Yes:No", defaultValue: "Yes" },
                                  },
                                 {
                                     name: "isDelete", width: 40, align: "center", formatter: "checkbox", sortable: false, edittype: "checkbox",
                                     formatoptions: { disabled: false },
                                     editoptions: { value: "Yes:No", defaultValue: "Yes" },
                                 },
                                   {
                                       name: "isPrint", width: 40, align: "center", formatter: "checkbox", sortable: false, edittype: "checkbox",
                                       formatoptions: { disabled: false },
                                       editoptions: { value: "Yes:No", defaultValue: "Yes" },
                                   },
                                     {
                                         name: 'ParentID', hidden: true, index: 'ParentID', editable: false, sortable: false, width: 20,
                                         editoptions: {
                                             dataInit: function (elem) {

                                             },
                                         }
                                     },

                ],
                rowNum: 100,
                rowList: [10, 20, 30],
                width: '1100',
                height: '900',
                viewrecords: true,
                editurl: '',
                jsonReader: {
                    page: function (obj) { return 1; },
                    total: function (obj) { return 1; },
                    records: function (obj) { return obj.d.length; },
                    root: function (obj) { return obj.d; },
                    repeatitems: false,
                    id: "0"
                },
                loadComplete: function (data) {
                    CheckParent();
                    gridResponsive();
                },
            });


            $("#" + gridname + "").jqGrid('navGrid', '#divpaging',
                  {
                      edit: false,
                      add: false,
                      del: false,
                      search: false
                  }
           );


        });

        function ReportRole() {
            var url = "/Forms/Admin/ReportRoleDefine.aspx";
            window.open(url);
        }

        function bindViewRole() {
            $("body").on("click", "#btnView", function () {
                $.ajax({
                    url: serviceUrl + "/viewRole",
                    type: "Post",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (responce) {
                        let details = JSON.parse(responce.d)
                        MakeDataGridForRole(details)
                        $("#userrolemodal").modal()

                    },
                    error: function (responce) {

                    }, complete: function () {
                        isValueExist("#leathercode", $("#leathercode").val())
                    }
                })
            })
        }

        function bindSelectRole() {
            $("body").on("click", ".selectrole", function () {
                $.ajax({
                    url: serviceUrl + "/getRoleWisePermissions",
                    type: "Post",
                    data: JSON.stringify({ id: $(this).attr("_recordId") }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (responce) {
                        let details = JSON.parse(responce.d)
                        $("#txtRoleID").val(details[0].RoleId)
                        $("#txtShipTo").val(details[0].RoleName)

                        $("#rolejqgrid tbody tr").each(function (index, row) {
                            let id = parseInt($(row).find("td:first").text())
                            $(details).each(function (index, secondRow) {
                                if (parseInt(secondRow.MenuId) == id) {
                                    if (secondRow.IsView == true) {
                                        $(row).find('td:eq(2) input').attr("checked", true)
                                    } else {
                                        $(row).find('td:eq(2) input').attr("checked", false)
                                    }

                                    if (secondRow.IsAdd == true) {
                                        $(row).find('td:eq(3) input').attr("checked", true)
                                    } else {
                                        $(row).find('td:eq(3) input').attr("checked", false)
                                    }

                                    if (secondRow.IsEdit == true) {
                                        $(row).find('td:eq(4) input').attr("checked", true)
                                    } else {
                                        $(row).find('td:eq(4) input').attr("checked", false)
                                    }

                                    if (secondRow.IsDelete == true) {
                                        $(row).find('td:eq(5) input').attr("checked", true)
                                    } else {
                                        $(row).find('td:eq(5) input').attr("checked", false)
                                    }

                                    if (secondRow.IsShow == true) {
                                        $(row).find('td:eq(6) input').attr("checked", true)
                                    } else {
                                        $(row).find('td:eq(6) input').attr("checked", false)
                                    }
                                }
                            })
                        })
                        $("#action").val("update");
                        $("#userrolemodal").modal('hide')

                    },
                    error: function (responce) {

                    }, complete: function () {
                        isValueExist("#txtRoleID", $("#txtRoleID").val())
                        isValueExist("#txtShipTo", $("#txtShipTo").val())
                    }
                })
            })
        }
        function MakeDataGridForRole(dataSource) {
            $("#roleGridContainer").DataTable().destroy();

            $("#roleGridContainer").DataTable({
                "searching": true,
                "bLengthChange": true,
                "sSort": true,
                "order": [0, 'desc'],
                oLanguage: {
                    sEmptyTable: "No results found"
                },
                "lengthMenu": [5, 10, 25],
                "bInfo": false,
                "aaData": dataSource,
                "aoColumns": [

                                { data: "Value", title: "" },
                                { data: "Text", title: "Name" },
                                {
                                    data: null,
                                    width: "5%",
                                    title: "Action",
                                    sDefaultContent: "",
                                    "orderable": false,
                                    render: function (data, type, row) {
                                        if (type === 'display') {
                                            var select = ' <a href="#" _recordId="' + data.Value + '" class="selectrole" title="Select Role"><i class="fas fa-check-circle" ></i></a>';
                                            var html = select;
                                            return html;
                                        }
                                        return data;
                                    }
                                },
                ],
                "columnDefs": [
                    { "targets": [0], "visible": false },

                ]
            });
        }
    </script>


    <div id="ModalView" style="display: none">
        <RoleDefineList:ViewList runat="server" />
    </div>
    <input type="hidden" id="action" value="add" />


    <div class="card">
        <div class="page-content">
            <div class="card-header card-header-text">
                <h4 class="card-title" style="color: #000000">Role Define</h4>
            </div>
            <div class="card-content">
                <div class="panel panel-info">
                    <div class="panel-body">
                        <div class="">
                            <div class="row pad">
                                <div class="col-lg-12">
                                    <div id="FormMsg" class="pull-left"></div>
                                    <div id="divTopButtons" class="pull-right">
                                        <button id="Button3" class="btn btn-primary" type="button" onclick="ReportRole()">
                                            Define Report Role
                                        </button>
                                        <button id="btnNew" class="btn btn-primary" type="button" onclick="AddNew()">
                                            <i class="fas fa-sync"></i>

                                        </button>
                                        <button id="btnSave" onclick="Save()" type="button" class="btn btn-primary">
                                            <i class="fas fa-save"></i>

                                        </button>
                                        <button id="btnView" type="button" class="btn btn-primary">
                                            <i class="fas fa-eye"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Role ID</label>
                                        <input type="text" class="form-control" id="txtRoleID" name="txtRoleID" placeholder="" />
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Role Name</label>
                                        <input type="text" class="form-control" id="txtShipTo" name="txtShipTo" placeholder="" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="pull-right" style="padding-right: 10px;">
                                        <button id="Button1" type="button" class="btn btn-primary" onclick="selectall()">
                                            <i class="fas fa-check"></i>

                                        </button>
                                        <button id="Button2" type="button" class="btn btn-primary" onclick="unselectall()">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <table id="Table1" runat="server" class="table-condensed table-hover">
                                <tr>
                                    <td valign="top" align="left">
                                        <table>
                                            <tr>
                                                <td valign="top" align="left">

                                                    <div id="gridview">
                                                        <table id="rolejqgrid"></table>
                                                        <div id="divpaging"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%-- View Role Modal --%>
    <div class="modal fade" id="userrolemodal" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="">Roles</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <table id='roleGridContainer' class="table table-striped table-bordered display w-100">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
