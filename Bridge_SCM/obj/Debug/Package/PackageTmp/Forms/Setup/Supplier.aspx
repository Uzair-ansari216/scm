﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="Supplier.aspx.vb" Inherits="Bridge_SCM.Supplier" %>

<%@ Import Namespace="System.Web.Optimization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/SupplierScript")%>


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header card-header-text">
                    <h4 class="card-title" style="color: #000000">Supplier</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-primary pull-right" id="btnAdd" _recordId="0" title="Add New Supplier"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-content table-responsive">
                                <table id='tableData' class="table table-striped table-bordered display">
                                    <thead class="header bg-primary">
                                        <tr class="">
                                            <th>ID</th>
                                            <th>Company</th>
                                            <th>Address</th>
                                            <th>Contact</th>
                                            <th>ContactPhone</th>
                                            <th>Phone</th>
                                            <th>Fax</th>
                                            <th>Email</th>
                                            <th>AltContact</th>
                                            <th>AltPhone</th>
                                            <th>Terms</th>
                                            <th>CreditLimit</th>
                                            <th>Notes</th>
                                            <th>LocationID</th>
                                            <th>DebtorAccountCode</th>
                                            <th>DebtorOpBal</th>
                                            <th>CreditorAccountCode</th>
                                            <th>CreditorOpBal</th>
                                            <th>NTN</th>
                                            <th>STN</th>
                                            <th>cityID</th>
                                            <th>countryID</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <%--</form> --%>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalAdd" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body" id="supplierForm">

                    <%--<input type="hidden" id="txtID" value="" />--%>
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="ValidationSummary" class="new badge"></span>
                        </div>
                    </div>

                    <%-- Form Wizard --%>
                    <div id="formwizard_simple" class="form-wizard form-wizard-horizontal">
                        <form class="form floating-label">
                            <div class="form-wizard-nav">
                                <div class="progress" style="width: 50%;">
                                    <div class="progress-bar progress-bar-primary" style="width: 0%;"></div>
                                </div>
                                <ul class="nav nav-justified nav-pills">
                                    <li class="active"><a href="#fws_tab1" data-toggle="tab"><span class="step">1</span> <span class="title">Supplier Information</span></a></li>
                                    <li><a href="#fws_tab2" data-toggle="tab"><span class="step">2</span> <span class="title">Contact Information</span></a></li>
                                    <li><a href="#fws_tab3" data-toggle="tab"><span class="step">3</span> <span class="title">Supplier Account Information</span></a></li>
                                </ul>
                            </div>
                            <!--end .form-wizard-nav -->

                            <div class="tab-content">
                                <div class="tab-pane active" id="fws_tab1">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">ID</label>
                                                <input type="text" class="form-control" id="txtID" name="txtID" placeholder="" />
                                                <span id="rID" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Company</label>
                                                <input type="text" class="form-control" id="txtCompany" name="txtCompany" placeholder="" />
                                                <span id="rCompany" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Country</label>
                                                <select id="ddlCountry" class="form-control"></select>
                                                <span id="rCountry" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">City</label>
                                                <select id="ddlCity" class="form-control"></select>
                                                <span id="rCity" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Location</label>
                                                <select id="ddlLocation" class="form-control"></select>
                                                <span id="rLocation" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end #tab1 -->

                                <div class="tab-pane" id="fws_tab2">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Phone No</label>
                                                <input type="text" class="form-control" id="txtPhone" name="txtPhone" placeholder="" />
                                                <span id="rPhone" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Address</label>
                                                <input type="text" class="form-control" id="txtAddress" name="txtAddress" placeholder="" />
                                                <span id="rAddress" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Email</label>
                                                <input type="text" class="form-control" id="txtEmail" name="txtEmail" placeholder="" />
                                                <span id="rEmail" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Fax</label>
                                                <input type="text" class="form-control" id="txtFax" name="txtFax" placeholder="" />
                                                <span id="rFax" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label">NTN</label>
                                                <input type="text" class="form-control" id="txtNTN" name="txtNTN" placeholder="" />
                                                <span id="rNTN" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label">STN</label>
                                                <input type="text" class="form-control" id="txtSTN" name="txtSTN" placeholder="" />
                                                <span id="rSTN" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Contact Person</label>
                                                <input type="text" class="form-control" id="txtContactPerson" name="txtContactPerson" placeholder="" />
                                                <span id="rContactPerson" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Phone No</label>
                                                <input type="text" class="form-control" id="txtPhone1" name="txtPhone1" placeholder="" />
                                                <span id="rPhone1" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Alt Contact Person</label>
                                                <input type="text" class="form-control" id="txtAltContact" name="txtAltContact" placeholder="" />
                                                <span id="rAltContact" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Alternate Phone No</label>
                                                <input type="text" class="form-control" id="txtAltPhone" name="txtAltPhone" placeholder="" />
                                                <span id="rAltPhone" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end #tab2 -->

                                <div class="tab-pane" id="fws_tab3">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Terms</label>
                                                <input type="text" class="form-control" id="txtTerms" name="txtTerms" placeholder="" />
                                                <span id="rTerms" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Credit Limit</label>
                                                <input type="text" class="form-control" id="txtCreditLimit" name="txtCreditLimit" placeholder="" />
                                                <span id="rCreditLimit" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Debtor Account</label>
                                                <input type="text" class="form-control" id="txtDebtor" name="txtDebtor" placeholder="" />
                                                <span id="rDebtor" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Opening Balance</label>
                                                <input type="text" class="form-control" id="txtOpeningBalanceD" name="txtOpeningBalanceD" placeholder="" />
                                                <span id="rOpeningBalanceD" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Creditor Account</label>
                                                <input type="text" class="form-control" id="txtCreditor" name="txtCreditor" placeholder="" />
                                                <span id="rCreditor" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Opening Balance</label>
                                                <input type="text" class="form-control" id="txtOpeningBalanceC" name="txtOpeningBalanceC" placeholder="" />
                                                <span id="rOpeningBalanceC" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Notes</label>
                                                <input type="text" class="form-control" id="txtNotes" name="txtNotes" placeholder="" />
                                                <span id="rNotes" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>

                                    </div>
                                    <%-- <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">GL Account</label>
                                <input type="text" class="form-control" id="txtGLAccount" name="txtGLAccount" placeholder="" />
                                <span id="rGLAccount" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>--%>
                                </div>
                                <!--end #tab3 -->


                                <ul class="pager wizard ">
                                    <li class="previous first disabled"><a class="btn-raised" href="javascript:void(0);">First</a></li>
                                    <li class="previous disabled"><a class="btn-raised" href="javascript:void(0);">Previous</a></li>
                                    <li class="next last"><a class="btn-raised" href="javascript:void(0);">Last</a></li>
                                    <li class="next"><a class="btn-raised" href="javascript:void(0);">Next</a></li>
                                </ul>
                            </div>
                            <!--end .tab-content -->


                        </form>
                    </div>
                    <%-- Form Wizard end --%>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-fill btn-default" onclick="OnSave()">Save changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDelete()">Confirm</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
