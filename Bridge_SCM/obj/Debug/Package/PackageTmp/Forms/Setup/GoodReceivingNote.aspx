﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="GoodReceivingNote.aspx.vb" Inherits="Bridge_SCM.GoodReceivingNote" %>

<%@ Import Namespace="System.Web.Optimization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/GoodReceivingNoteScript")%>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header row">
                    <div class="col-md-3">
                        <h4 class="card-title" style="color: #000000">Goods Receiving Note</h4>
                    </div>
                    <div class="col-md-9">
                        <button type="button" class="btn btn-primary pull-right" id="btnaddnew" title="Add New GRN"><i class="fas fa-sync-alt"></i></button>
                        <button type="button" class="btn btn-primary pull-right" onclick="bindPrintClick()" title="Print GRN"><i class="fas fa-print"></i></button>
                        <button type="button" class="btn btn-primary pull-right" id="viewgrn" title="View GRN"><i class="fas fa-eye"></i></button>
                    </div>
                </div>
                <div class="card-content" id="GRNForm">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-9">
                            <h4 class="modal-title title-cl" id="GRNNO"></h4>
                        </div>
                    </div>
                    <input type="hidden" id="txtGRN" value="0" />
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Type</label>
                                <select id="ddlType" class="form-control"></select>
                                <span id="rType" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group label-floating">
                                <%--<label class="control-label">Type</label>--%>
                                <select id="ddlType2" class="form-control"></select>
                                <span id="rSupplier" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group label-floating">
                                <%--<label class="control-label">Type</label>--%>
                                <select id="ddlType3" class="form-control"></select>
                                <span id="rPurchaseOrder" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group label-floating">
                                <input type="hidden" id="grnnumber" />
                                <label class="control-label">ID</label>
                                <input type="text" class="form-control" id="txtID" readonly="readonly" name="txtID" placeholder="" />
                                <span id="rtxtID" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Date</label>
                                <input type="text" class="form-control" id="txtDate" name="txtDate" placeholder="" />
                                <span id="rDate" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Received @</label>
                                <select id="ddlDeliveredFrom" class="form-control"></select>
                                <span id="rReceived" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-content table-responsive">
                                <table id='tableData' class="table table-striped table-bordered display">
                                    <thead class="header bg-primary">
                                        <tr class="">
                                            <th>Reference#</th>
                                            <th>Model#</th>
                                            <th>Part#</th>
                                            <th>Description</th>
                                            <th>Total Qty</th>
                                            <th>Unit</th>
                                            <th>Received</th>
                                            <th>Qty</th>
                                            <th>Charges</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Remarks</label>
                                <input type="text" class="form-control" id="txtRemarks" name="txtRemarks" placeholder="" />
                                <span id="rRemarks" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-9"></div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnSave()">Save</button>
                        </div>
                        <%--<button type="button" class="btn btn-fill btn-default" onclick="OnSave()">Save</button>--%>
                    </div>
                </div>
                <%--</form> --%>
            </div>
        </div>
    </div>
    <%--        <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="txtID" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="ValidationSummary" class="new badge"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Doc Date</label>
                                <input type="text" class="form-control" id="txtDate" name="txtDate" placeholder="" />
                                <span id="rDate" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Customer</label>
                                <select id="ddlCustomer" class="form-control">
                                </select>
                                <span id="rCustomer" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Ship To</label>
                                <input type="text" class="form-control" id="txtShipTo" name="txtShipTo" placeholder="" />
                                <span id="rShipTo" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Bill To</label>
                                <input type="text" class="form-control" id="txtBillTo" name="txtBillTo" placeholder="" />
                                <span id="rBillTo" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Location</label>
                                <select id="ddlLocation" class="form-control">
                                </select>
                                <span id="rLocation" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Sales Person</label>
                                <select id="ddlSalesPerson" class="form-control">
                                </select>
                                <span id="rSalesPerson" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Currency</label>
                                <select id="ddlCurrency" class="form-control">
                                </select>
                                <span id="rCurrency" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Rate</label>
                                <input type="text" class="form-control" id="txtCurrency" name="txtCurrency" placeholder="" />
                                <span id="rtxtCurrency" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">SO Type</label>
                                <select id="ddlSOType" class="form-control">
                                </select>
                                <span id="rSOType" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Terms</label>
                                <select id="ddlTerms" class="form-control">
                                </select>
                                <span id="rTerms" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">GST</label>
                                <input type="text" class="form-control" id="txtGST" name="txtGST" placeholder="" />
                                <span id="rGST" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Charges</label>
                                <input type="text" class="form-control" id="txtCharges" name="txtCharges" placeholder="" />
                                <span id="rCharges" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Remarks</label>
                                <input type="text" class="form-control" id="txtRemarks" name="txtRemarks" placeholder="" />
                                <span id="rRemarks" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Billing Remarks</label>
                                <input type="text" class="form-control" id="txtBillings" name="txtBillings" placeholder="" />
                                <span id="rBillings" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Status</label>
                                <select id="ddlStatus" class="form-control">
                                </select>
                                <span id="rStatus" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Status Date</label>
                                <input type="text" class="form-control" id="txtStatusDate" name="txtStatusDate" placeholder="" />
                                <span id="rStatusDate" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Remarks</label>
                                <input type="text" class="form-control" id="txtStatusRemarks" name="txtStatusRemarks" placeholder="" />
                                <span id="rStatusRemarks" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnSave()">Save changes</button>
                </div>
            </div>
        </div>
    </div>--%>

    <div class="modal fade" id="modalView" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="width: 55%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H2"></h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="txtCID" value="" />
                    <input type="hidden" id="txtMID" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="Validation2" class="new badge"></span>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Model #</label>
                                <input type="text" class="form-control" id="txtModel" name="txtModel" placeholder="" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Part #</label>
                                <input type="text" class="form-control" id="txtPart" name="txtPart" placeholder="" />
                                <span id="rPart" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Reference #</label>
                                <input type="text" class="form-control" id="txtReference" name="txtReference" placeholder="" />
                                <span id="rReference" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Serial #</label>
                                <input type="text" class="form-control" id="txtSerial" name="txtSerial" placeholder="" />
                                <span id="rSerial" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Order ID</label>
                                <input type="text" class="form-control" id="txtOrder" name="txtOrder" placeholder="" />
                                <span id="rOrder" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnSaveDetail()">Save changes</button>
                </div>
                <hr />
                <div class="row">
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <table id='tableDetail' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>ID</th>
                                        <th>Model#</th>
                                        <th>PartNumber</th>
                                        <th>Serial#</th>
                                        <th>Order ID</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <%--<div class="modal-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Amount</label>
                                <input type="text" class="form-control" id="txtIAmount" name="txtIAmount" placeholder="" />
                                <span id="Span1" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group label-floating">
                                <label class="control-label">GST %</label>
                                <input type="text" class="form-control" id="txtIGST" name="txtIGST" placeholder="" />
                                <span id="Span3" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">GST Amount</label>
                                <input type="text" class="form-control" id="txtIGSTAmount" name="txtIGSTAmount" placeholder="" />
                                <span id="Span2" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group label-floating">
                                <label class="control-label">Charges</label>
                                <input type="text" class="form-control" id="txtICharges" name="txtICharges" placeholder="" />
                                <span id="Span4" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Total Amount</label>
                                <input type="text" class="form-control" id="txtITotalAmount" name="txtITotalAmount" placeholder="" />
                                <span id="Span5" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                </div>--%>
            </div>
        </div>
    </div>

    <%--    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDelete()">Confirm</button>
                </div>
            </div>
        </div>
    </div>--%>

    <div class="modal fade" id="modalDelete2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H3">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDeleteDetail()">Confirm</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalView2" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="OnClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H4"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <table id='tblItemList' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>Select</th>
                                        <th>GRN No</th>
                                        <th>GRN Date</th>
                                        <th>Received At</th>
                                        <th>Received From</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function bindPrintClick() {
            var reportPath = '<%= System.Configuration.ConfigurationManager.AppSettings("ReportPath").ToString() %>'
            let grnId = $("#txtID").val();
            if (grnId == "") {
                alert("ID is required")
            }
            else {
                $.ajax({
                    type: "POST",
                    url: "" + serviceurl + "/generateGRNReport",
                    data: JSON.stringify({ GRNId: grnId }),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (res) {
                        var data = JSON.parse(res.d);
                        if (data.Status) {
                            window.open(reportPath + '?isGeneratedByBuilder=' + false);
                            $("#FormError").css('display', 'none');
                            $("#Error").empty();
                        } else {
                            $("#Validation").fadeIn(1000);
                            $("#Validation").text(data.Message);
                            $("#Validation").fadeOut(5000);
                        }
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }
    </script>
</asp:Content>
