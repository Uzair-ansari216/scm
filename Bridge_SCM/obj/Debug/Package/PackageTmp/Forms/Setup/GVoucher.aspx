﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="GVoucher.aspx.vb" Inherits="Bridge_SCM.GVoucher" %>

<%@ Import Namespace="System.Web.Optimization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {

            typee = '<%= Request.QueryString("Type")%>';
            $("#voucherType").val(typee)
            voucherform = typee  <%--'<%= HttpContext.Current.Session("VoucherType") %>'--%>
        });

        function isNumberKey(sender, evt) {
            var txt = sender.value;
            var dotcontainer = txt.split('.');
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (!(dotcontainer.length == 1 && charCode == 46) && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function puchDetailToArray(data) {
            voucherDetailArray = [];
            $.each(data, function (key, value) {
                var dataSend = {
                    VoucherID: $(_MID).val(),
                    Account: value.Account,
                    AccountCode: value.AccountCode,
                    Particulars: value.Particulars,
                    LocationID: value.LocationID,
                    LocationName: value.LocationName,
                    DepartmentID: value.DepartmentID,
                    DepartmentName: value.DepartmentName,
                    ProjectID: value.ProjectID,
                    ProjectName: value.ProjectName,
                    EmployeeID: value.EmployeeID,
                    EmployeeName: value.EmployeeName,
                    Debit: value.Debit,
                    Credit: value.Credit,
                    SegmentID: value.SegmentID,
                    SegmentName: value.SegmentName,
                    AccountName: value.AccountName,
                    rowID: '_' + Math.random().toString(36).substr(2, 9)
                }
                voucherDetailArray.push(dataSend)
                console.log(voucherDetailArray)
            })
        }

        function FindData2(ID) {
            debugger
            BlockUI();
            $.ajax({
                type: "POST",
                url: "" + serviceurl + "/FindData2",
                data: JSON.stringify({ ID: ID }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var data = JSON.parse(response.d);
                    $(_BankAccountCode).val(data[0][0].AccountCode)
                    $(_Particulars2).val(data[0][0].Particulars)
                    $(_Creditors).val(data[0][0].Credit)
                    $(_Cheque).val(data[0][0].ChecqueNoOrDepositNo)
                    $(_Location2).val(data[0][0].LocationID)
                    $(_AccountName).val(data[0][0].AccountName)
                    $(tableDetail).DataTable().destroy();
                    puchDetailToArray(data[1])
                    LoadDataTableDetail2(voucherDetailArray)
                    UnblockUI();
                    $("#btnsave").attr("disabled", false)
                },
                error: OnErrorCall,
                complete: function () {
                    totalCalculation()
                }
            });
        }
        function OnSave() {
            debugger
            // FormValidaton();
            var isValid = true
            let type = $("#voucherType").val()//$("#hType").val()
            $("body #gvoucherForm input").each(function (index, row) {
                if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
                    if ($(row).val() == "") {
                        $(row).parent().find("span").eq(2).removeClass("hide")
                        isValid = false
                    } else {
                        $(row).parent().find("span").eq(2).addClass("hide")
                    }
                }
            })

            $("body #gvoucherForm select").each(function (index, row) {
                if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
                    if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                        $(row).parent().find("span").eq(2).removeClass("hide")
                        isValid = false
                    } else {
                        $(row).parent().find("span").eq(2).addClass("hide")
                    }
                }
            })
            let debit = 0
            let credit = 0
            if (type == 'BP' || type == 'CP') {
                debit = parseInt($("#txtDebitAmount").val())
                credit = parseInt($("#txtCreditAmount").val()) //parseInt($("#txtCreditors").val()
            } else if (type == 'BR' || type == 'CR') {
                debit = parseInt($("#txtDebitAmount").val()) //parseInt($("#txtCreditors").val()
                credit = parseInt($("#txtCreditAmount").val())
            }

            if (isValid) {
                if (debit == credit) {
                    if ($("#txtDebitAmount").val() > 0 && $("#txtCreditAmount").val() > 0) {
                        ParticularsGeneral = $(_Particulars2).val()
                        var dataSend = {
                            ID: $(_ID).val(),
                            VoucherDate: $(_Date).val(),
                            PayToOrReceivedFrom: $(_Name).val(),
                            Description: $(_Description).val(),
                            AccountCode: $(_BankAccountCode).val(),
                            Particulars: $(_Particulars2).val(),
                            Credit: $(_Creditors).val(),
                            ChecqueNoOrDepositNo: $(_Cheque).val(),
                            LocationID: $(_Location2).val(),
                            VoucherType: $("#voucherType").val()
                        }

                        console.log(dataSend)
                        BlockUI();
                        //if (msg == "") {
                        $.ajax({
                            type: "POST",
                            url: "" + serviceurl + "/OnSave",
                            data: JSON.stringify({ model: dataSend, detailList: voucherDetailArray }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                if (response.d == "sessionNotFound") {
                                    UnblockUI();
                                    alert("sessionNotFound");
                                    window.location.href = loginUrl;
                                } else {
                                    UnblockUI();
                                    var data = JSON.parse(response.d);
                                    console.log(data);
                                    if (!data.Status) {
                                        $("#Validation").fadeIn(1000);
                                        $("#Validation").text(data.Message);
                                        $("#Validation").fadeOut(5000);
                                    }
                                    if (data.Message == "success") {
                                        UnblockUI();
                                        $("#btnsave").attr("disabled", true)
                                        //ClearFields();
                                        $("#Validation").fadeIn(1000);
                                        $("#Validation").text(success);
                                        $("#Validation").fadeOut(5000);
                                        $("#txtVoucherNo").val(data.VoucherNo)
                                        $("#txtID").val(data.ID)
                                        //OnLoadGrid();
                                        //OnLoadGridItems();
                                        //$("#modalAdd").modal('hide');
                                    } else if (data.Message == "exist") {
                                        UnblockUI();
                                        $("#Validation").fadeIn(1000);
                                        $("#Validation").text(exist);
                                        $("#Validation").fadeOut(5000);
                                        // OnLoadGridItems();
                                        //$("#modalAdd").modal('hide');
                                    } else if (data.Message == "update") {
                                        UnblockUI();
                                        $("#btnsave").attr("disabled", true)
                                        $("#Validation").fadeIn(1000);
                                        $("#Validation").text(update);
                                        $("#Validation").fadeOut(5000);
                                        $("#txtVoucherNo").val(data.VoucherNo)
                                        $("#txtID").val(data.ID)
                                        //OnLoadGrid();
                                        //OnLoadGridItems();
                                        // ClearFields()
                                        //$("#modalAdd").modal('hide');
                                    }
                                }
                                UnblockUI();
                            },
                            error: OnErrorCall
                        });

                    }
                    else {
                        alert("Debit & Credit Amount must be greater than 0")
                    }
                }
                else {
                    alert("Debit & Credit Amount must be equal")
                }
            }

        }



        //myVar = setInterval(check, 300)
        function check() {

            //var debit = $("#txtDebitAmount").val() | 0;
            //var credit = $("#txtCalcCredit").val() | 0;

            //var diff = debit - credit | 0;

            //console.log(diff)
            //$(_Creditors).val(diff)


        }



        var pagename = "GVoucher.aspx";

        function OnPrint() {
            var VoucherID = $("#txtID").val();

            var urlname = "Print";
            if (VoucherID == "") {
                alert("ID is required")
            }
            else {
                $.ajax({
                    type: "POST",
                    url: '' + pagename + '/' + urlname + '?VoucherID=' + VoucherID,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (res) {
                        var data = JSON.parse(res.d);

                        if (data.Status) {
                            window.open("../Ctrls/ReportViewer.aspx/" + '?isGeneratedByBuilder=' + false);
                            $("#FormError").css('display', 'none');
                            $("#Error").empty();
                        } else {
                            $("#Validation").fadeIn(1000);
                            $("#Validation").text(data.Message);
                            $("#Validation").fadeOut(5000);
                        }
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        var creditAmount;
        function LoadDataTableDetail2(datasource) {
            var dbSourse
            $(tableDetail).DataTable().destroy();
            if (jQuery.type(datasource) == "string") {
                dbSourse = JSON.parse(datasource)
            } else {
                dbSourse = datasource
            }
            if (datasource != "[]") {

                table3 = $(tableDetail).DataTable({
                    "searching": false,
                    "bLengthChange": false,
                    "sSort": true,
                    "bPaginate": false,
                    "order": [0, 'desc'],
                    "bInfo": false,
                    "bSort": false,
                    "aaData": dbSourse,
                    "aoColumns": [
                                   { data: "rowID", sDefaultContent: "" },
                                    { data: "VoucherID", sDefaultContent: "" },
                                     { data: "Account", sDefaultContent: "" },
                                    { data: "AccountCode", sDefaultContent: "" },
                                    { data: "AccountName", sDefaultContent: "" },
                                    { data: "LocationID", sDefaultContent: "" },
                                    { data: "LocationName", sDefaultContent: "" },
                                    { data: "DepartmentID", sDefaultContent: "" },
                                    { data: "DepartmentName", sDefaultContent: "" },
                                    { data: "SegmentID", sDefaultContent: "" },
                                    { data: "SegmentName", sDefaultContent: "" },
                                    { data: "ProjectID", sDefaultContent: "" },
                                    { data: "ProjectName", sDefaultContent: "" },
                                    { data: "EmployeeID", sDefaultContent: "" },
                                    { data: "EmployeeName", sDefaultContent: "" },
                                    { data: "Particulars", sDefaultContent: "" },
                                    { data: "Debit", sDefaultContent: "" },
                                    { data: "Credit", sDefaultContent: "" },
                                    {
                                        data: null,
                                        sDefaultContent: "",
                                        "orderable": false,
                                        render: function (data, type, row) {
                                            if (type === 'display') {
                                                //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                                var del = ' <a href="#" class="del"  _recordId = ' + data.rowID + ' title="Delete"><i class="fa fa-trash" ></i></a>';
                                                var edit = ' <a href="#" class="edit" _recordId = ' + data.rowID + ' title="Edit"><i class="fas fa-pencil-alt" ></i></a>';
                                                var html = del + edit;
                                                return html;
                                            }
                                            return data;
                                        }
                                    },
                    ],
                    "columnDefs": [

                        { "targets": [0], "visible": false, "searchable": false },
                        { "targets": [1], "visible": false, "searchable": false },
                        { "targets": [5], "visible": false, "searchable": false },
                        { "targets": [7], "visible": false, "searchable": false },
                        { "targets": [9], "visible": false, "searchable": false },
                        { "targets": [11], "visible": false, "searchable": false },
                        { "targets": [13], "visible": false, "searchable": false },
                        { "targets": [3], "visible": false, "searchable": false },
                        { "targets": [4], "visible": false, "searchable": false },
                        //{ "targets": [3], "width": "25%" },

                    ],
                    "footerCallback": function (row, data, start, end, display) {
                        var api = this.api(), data;

                        // Remove the formatting to get integer data for summation
                        var intVal = function (i) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                                typeof i === 'number' ?
                                i : 0;
                        };
                        // Total over all pages
                        //total = api
                        //    .column(16)
                        //    .data()
                        //    .reduce(function (a, b) {
                        //        return intVal(a) + intVal(b);
                        //    }, 0);

                        //totalCredit = api
                        //    .column(17)
                        //    .data()
                        //    .reduce(function (a, b) {
                        //        return intVal(a) + intVal(b);
                        //    }, 0);
                        // Total over this page
                        //pageTotal = api
                        //    .column(10, { page: 'current' })
                        //    .data()
                        //    .reduce(function (a, b) {
                        //        return intVal(a) + intVal(b);
                        //    }, 0);
                        //alert(total)
                        // Update footer
                        //$(api.column(12).footer()).html(
                        //     total

                        //);
                        debugger
                        //var debitAmount = total | 0;
                        //var creditAmount = totalCredit | 0
                        //$("#txtCalcDebit").val(debitAmount);
                        //$("#txtCalcCredit").val(creditAmount)
                        //var creditAmount = totalCredit | 0;
                        //$("#txtCreditAmount").val(creditAmount);
                    }
                });
            }
        }
        function totalCalculation() {
            let totalDebit = 0
            let totalCredit = 0
            debugger
            $("#tableDetail tbody tr").each(function (index, row) {
                totalDebit += parseFloat($(row).find("td:nth-child(8)").text())
                totalCredit += parseFloat($(row).find("td:nth-child(9)").text())
            })
            $("#txtCalcDebit").val(totalDebit);
            $("#txtCalcCredit").val(totalCredit)

            if (typee == "BR" || typee == "CR") {
                //var debit = totalDebit | 0 //$("#txtCalcDebit").val() | 0;
                //var credit = totalCredit | 0 //$("#txtCalcCredit").val() | 0;

                //var diff = credit - debit | 0;
                $(_Creditors).val(Math.abs(totalCredit - totalDebit))

                //var total = debit + diff | 0;
                //if (total <= 0) {
                //    $("#txtDebitAmount").val(debit)
                //} else {
                //    $("#txtDebitAmount").val(total)
                //}
                $("#txtCreditAmount").val(totalCredit)
                $("#txtDebitAmount").val(totalDebit + parseFloat($(_Creditors).val())) //debit > 0 ? debit : credit
            }
            else if (typee == "CP" || typee == "BP") {
                //var debit = totalDebit | 0 //$("#txtCalcDebit").val() | 0;
                //var credit = totalCredit | 0 //$("#txtCalcCredit").val() | 0;

                //var diff = debit - credit | 0;
                $(_Creditors).val(Math.abs(totalDebit - totalCredit))

                //var total = credit + diff | 0;

                //$("#txtCreditAmount").val(total)
                //$("#txtDebitAmount").val(debit)
                $("#txtCreditAmount").val(totalCredit + parseFloat($(_Creditors).val()))
                $("#txtDebitAmount").val(totalDebit) //debit > 0 ? debit : credit
            }
        }

        function OnSupport() {
            clearAllMessages("voucherSupportForm")

            var service_DeveloperMode = $("#hdnDevMode").val()
            if (service_DeveloperMode == 'on') {
                addFormFields("Voucher Support", "voucherSupportForm")
            }
            //var service_ToolTip = $("#hdnToolTip").val()
            //if (service_ToolTip == 'on') {

            setToolTip("Voucher Support", "voucherSupportForm", service_DeveloperMode)

            //}
            if ($("#txtID").val() != "") {
                $.ajax({
                    type: "POST",
                    url: "" + serviceurl + "/AddEditVoucherSupport",
                    data: JSON.stringify({ id: $("#txtID").val() }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        debugger
                        var data = JSON.parse(response.d)
                        $("#ddlcurrency").find("option").remove().end().append("<option value='0'>-- Select Currency --</option>")
                        $.each(data.CurrencyList, function (index, item) {
                            $("#ddlcurrency").append("<option value=" + item.Value + "> " + item.Text + " </option>");
                        });
                        $("#ddlcurrency").find("option:first").prop("selected", true)

                        $("#ddlexcurrency").find("option").remove().end().append("<option value='0'>-- Select Currency --</option>")
                        $.each(data.CurrencyList, function (index, item) {
                            $("#ddlexcurrency").append("<option value=" + item.Value + "> " + item.Text + " </option>");
                        });
                        $("#ddlexcurrency").find("option:first").prop("selected", true)
                        $("#txtSID").val(data.VoucherSupport.Id)
                        $("#txtvoucherid").val($("#txtID").val())
                        $("#txtaccountcode").val(data.VoucherSupport.AccountCode)
                        $("#txtorderno").val(data.VoucherSupport.OrderId)
                        $("#txtquantity").val(data.VoucherSupport.OrderQuantity)
                        $("#ddlcurrency").val(data.VoucherSupport.Currency)
                        $("#txtcurrencyrate").val(data.VoucherSupport.Rate)
                        $("#txtexmonthdate").val(data.VoucherSupport.ExpenseDate != null ? data.VoucherSupport.ExpenseDate != "" ? data.VoucherSupport.ExpenseDate.split(' ')[0] : "" : "")
                        $("#txtinvoicereceiveddate").val(data.VoucherSupport.InvoiceReceived != null ? data.VoucherSupport.InvoiceReceived != "" ? data.VoucherSupport.InvoiceReceived.split(' ')[0] : "" : "")
                        $("#ddlexcurrency").val(data.VoucherSupport.ExpenseCurrency)
                        $("#supportModalLabel").text("Add Voucher Support");
                        $("#vouchersupportmodal").modal();
                    },
                    error: OnErrorCall
                });
            } else {
                alert("Voucher ID is required")
            }
        }

        function OnSaveDetail() {


            var isValid = true

            $("body #paymentAndReceiptDetailForm input").each(function (index, row) {
                if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
                    if ($(row).val() == "") {
                        $(row).parent().find("span").eq(2).removeClass("hide")
                        isValid = false
                    } else {
                        $(row).parent().find("span").eq(2).addClass("hide")
                    }
                }
            })

            $("body #paymentAndReceiptDetailForm select").each(function (index, row) {
                if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
                    if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                        $(row).parent().find("span").eq(2).removeClass("hide")
                        isValid = false
                    } else {
                        $(row).parent().find("span").eq(2).addClass("hide")
                    }
                }
            })

            if (isValid) {
                if (($(_Debit).val() > 0 || $(_Credit).val() > 0)) {
                    //DetailFormValidaton();
                    ParticularsGeneral = $(_Particulars).val()
                    if ($(_MID).val() == "") {
                        var dataSend = {
                            VoucherID: $(_MID).val(),
                            Account: $(_Account).val() + "-" + $("#txtAccountName2").val(),
                            AccountCode: $(_Account).val(),
                            Particulars: $(_Particulars).val(),
                            LocationID: $(_Location).val(),
                            LocationName: $("" + _Location + " option:selected").text(),
                            DepartmentID: $(_Department).val(),
                            DepartmentName: $("" + _Department + " option:selected").text(),
                            ProjectID: ProjectID,
                            ProjectName: $(_Project).val(),
                            EmployeeID: EmployeeID,
                            EmployeeName: $(_Employee).val(),
                            Debit: $(_Debit).val(),
                            Credit: $(_Credit).val(),
                            SegmentID: $(_Brand).val(),
                            SegmentName: $("" + _Brand + " option:selected").text(),
                            AccountName: $("#txtAccountName2").val(),
                            rowID: '_' + Math.random().toString(36).substr(2, 9)
                        }
                        voucherDetailArray.push(dataSend)
                    } else {
                        $.grep(voucherDetailArray, function (key, value) {
                            if (key != undefined && key.rowID == $(_MID).val()) {
                                key.VoucherID = $(_MID).val(),
                                key.Account = $(_Account).val() + "-" + $("#txtAccountName2").val(),
                                key.AccountCode = $(_Account).val(),
                                key.Particulars = $(_Particulars).val(),
                                key.LocationID = $(_Location).val(),
                                key.LocationName = $("" + _Location + " option:selected").text(),
                                key.DepartmentID = $(_Department).val(),
                                key.DepartmentName = $("" + _Department + " option:selected").text(),
                                key.ProjectID = ProjectID,
                                key.ProjectName = $(_Project).val(),
                                key.EmployeeID = EmployeeID,
                                key.EmployeeName = $(_Employee).val(),
                                key.Debit = $(_Debit).val(),
                                key.Credit = $(_Credit).val(),
                                key.SegmentID = $(_Brand).val(),
                                key.SegmentName = $("" + _Brand + " option:selected").text(),
                                key.AccountName = $("#txtAccountName2").val()
                            }
                        })
                    }
                    LoadDataTableDetail2(voucherDetailArray);
                    totalCalculation()
                    $("#modalAdd").modal('hide');
                    //$.ajax({
                    //    type: "POST",
                    //    url: "" + serviceurl + "/OnSaveDetail",
                    //    data: JSON.stringify({ model: dataSend }),
                    //    contentType: "application/json; charset=utf-8",
                    //    dataType: "json",
                    //    success: function (response) {
                    //        if (response.d == "sessionNotFound") {
                    //            alert("sessionNotFound");
                    //            window.location.href = loginUrl;
                    //        } else {
                    //            var data = JSON.parse(response.d);
                    //            console.log(data);
                    //            if (data.Message == "success") {
                    //                ClearDetailFields();
                    //                $("#modalAdd").modal('hide');
                    //                OnLoadGridItems2();

                    //            } else if (data.Message == "exist") {
                    //                OnLoadGrid3(MasterID);
                    //                $("#Validation2").fadeIn(1000);
                    //                $("#Validation2").text(exist);
                    //                $("#Validation2").fadeOut(5000);
                    //            } else if (data.Message == "update") {
                    //                ClearDetailFields();
                    //                OnLoadGrid3(MasterID);
                    //                $("#Validation2").fadeIn(1000);
                    //                $("#Validation2").text(update);
                    //                $("#Validation2").fadeOut(5000);
                    //            }
                    //        }

                    //    },
                    //    error: OnErrorCall
                    //});


                } else {
                    alert("Debit & Credit Amount must be greater than zero");
                }
            }

        }
    </script>

    <%: Scripts.Render("~/bundles/GVoucherScript")%>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <asp:HiddenField ID="hType" ClientIDMode="static" runat="server" />
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <%-- <div class="card-header card-header-text">
                    <h4 id="heading" runat="server" style="color: #000000">GVoucher</h4>
                </div>--%>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                    </div>
                    <!-- BEGIN FORM WIZARD -->
                    <div id="formwizard_simple" class="form-wizard form-wizard-horizontal">
                        <form class="form floating-label">
                            <div class="row">
                                <div class="col-sm-3">
                                    <h4 id="heading" runat="server" style="color: #000000">GVoucher</h4>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-wizard-nav">
                                        <div class="progress" style="width: 50%;">
                                            <div class="progress-bar progress-bar-primary" style="width: 0%;"></div>
                                        </div>
                                        <ul class="nav nav-justified">
                                            <li class="active"><a href="#fws_tab1" data-toggle="tab" aria-expanded="false"><span class="step">1</span> <span class="title">Voucher</span></a></li>
                                            <li class=""><a href="#fws_tab2" data-toggle="tab" aria-expanded="true"><span class="step">2</span> <span class="title">Detail</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <button type="button" class="btn btn-primary pull-right" id="btndelete" title="Delete"><i class="fas fa-trash"></i></button>
                                    <button type="button" class="btn btn-primary pull-right" onclick="OnPrint()" title="Print"><i class="fas fa-print"></i></button>
                                    <button type="button" class="btn btn-primary pull-right" onclick="AddNew()" title="Refresh"><i class="fas fa-sync-alt"></i></button>
                                    <button type="button" class="btn btn-primary pull-right" onclick="Show()" title="Search">
                                        <i class="fas fa-search"></i>
                                        <div class="ripple-container"></div>
                                    </button>

                                </div>
                            </div>
                            <!--end .form-wizard-nav -->

                            <div class="tab-content clearfix p-30" id="gvoucherForm">
                                <div class="tab-pane active" id="fws_tab1">
                                    <input type="hidden" id="txtCalcCredit" value="" />
                                    <input type="hidden" id="txtCalcDebit" value="" />
                                    <input type="hidden" id="txtID" value="" />
                                    <input type="hidden" id="voucherType" value="" />
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <span id="ValidationSummary" class="new badge"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Date</label>
                                                <input type="text" class="form-control" id="txtDate" name="txtDate" placeholder="" />
                                                <span id="rDate" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>

                                        <%--<div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Voucher Type</label>
                                <select id="ddlVoucherType" class="form-control">
                                </select>
                                <span id="rVoucherType" class="new badge  hide">required</span>
                            </div>
                        </div>--%>
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Voucher #</label>
                                                <input type="text" class="form-control" id="txtVoucherNo" name="txtVoucherNo" placeholder="" />
                                                <span id="rVoucherNo" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Type</label>
                                                <select id="ddlType" class="form-control"></select>
                                                <span id="rType" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Name</label>
                                                <select id="ddlName" class="form-control"></select>
                                                <span id="rName" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label">B: A/C #</label>
                                                <input type="text" class="form-control" id="txtBankAccountCode" name="txtBankAccountCode" placeholder="" />
                                                <span id="rBankAccountCode" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-1" style="margin: 1em 3em 0px -3em !important;">
                                            <button type="button" class="btn btn-primary btn-sm pull-right" onclick="OnSearchBankAccount()"><i class="fas fa-search"></i></button>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Account Name</label>
                                                <input type="text" class="form-control" id="txtAccountName" name="txtAccountName" placeholder="" />
                                                <span id="rAccountName" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <%-- <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Invoice #</label>
                                <input type="text" class="form-control" id="txtInvoiceNo" name="txtInvoiceNo" placeholder="" />
                                <span id="rInvoiceNo" class="new badge  hide">required</span>
                            </div>
                        </div>--%>
                                    </div>
                                    <%--<div class="row">
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Invoice Date</label>
                                <input type="text" class="form-control" id="txtInvoiceDate" name="txtInvoiceDate" placeholder="" />
                                <span id="rInvoiceDate" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group label-floating">
                                <label class="control-label">Due Days</label>
                                <input type="text" class="form-control" id="txtDueDays" name="txtDueDays" placeholder="" />
                                <span id="rDueDays" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Due Date</label>
                                <input type="text" class="form-control" id="txtDueDate" name="txtDueDate" placeholder="" />
                                <span id="rDueDate" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>--%>
                                    <div class="row">
                                        <div class="col-sm-7">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Particulars</label>
                                                <input type="text" class="form-control" id="txtParticulars2" name="txtParticulars2" placeholder="" />
                                                <span id="rParticulars2" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-1" style="margin-top: 27px">
                                            <button type="button" class="btn-primary" title="Paste" onclick="OnPaste2()"><i class="fas fa-paste"></i></button>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group label-floating" style="margin-top: 0px">

                                                <label class="control-label"></label>
                                                <%--<asp:Label ID="lblCredit" runat="server" class="control-label" Text="Credit"></asp:Label>--%>
                                                <input type="text" class="form-control" id="txtCreditors" readonly="readonly" name="txtCreditors" placeholder="" />
                                                <span id="rCreditors" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group label-floating">
                                                <label class="control-label"></label>
                                                <input type="text" class="form-control" id="txtCheque" name="txtCheque" placeholder="" />
                                                <span id="rCheque" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Description</label>
                                                <input type="text" class="form-control" id="txtDescription" name="txtDescription" placeholder="" />
                                                <span id="rDescription" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Location</label>
                                                <select id="ddlLocation2" class="form-control"></select>
                                                <span id="rLocation2" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end #tab1 -->

                                <div class="tab-pane" id="fws_tab2">
                                    <div class="row">

                                        <div class="col-sm-12">
                                            <button type="button" class="btn btn-primary pull-right" onclick="OnAdd()"><i class="fas fa-plus"></i></button>
                                            <button type="button" class="btn btn-primary pull-right" onclick="OnSave()" title="Save" id="btnsave"><i class="fas fa-save"></i></button>
                                            <button type="button" class="btn btn-primary pull-right" onclick="OnSupport()" title="Voucher Support"><i class="fas fa-exchange-alt"></i></button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6"></div>
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-3">
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-content table-responsive">
                                                <table id='tableDetail' class="table table-striped table-bordered display" style="width: 100%">
                                                    <thead class="header bg-primary">
                                                        <tr class="">
                                                            <th>rowID</th>
                                                            <th>VoucherID</th>
                                                            <th>Account</th>
                                                            <th>AccountCode</th>
                                                            <th>AccountName</th>
                                                            <th>LocationID</th>
                                                            <th>Location</th>
                                                            <th>DepartmentID</th>
                                                            <th>Department</th>
                                                            <th>SegmentID</th>
                                                            <th>Segment</th>
                                                            <th>ProjectID</th>
                                                            <th>Project</th>
                                                            <th>EmployeeID</th>
                                                            <th>Employee</th>
                                                            <th>Particulars</th>
                                                            <th>Debit</th>
                                                            <th>Credit</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-sm-1">
                                        </div>
                                        <div class="col-sm-4">
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Debit Amount</label>
                                                <input type="text" class="form-control" readonly="readonly" id="txtDebitAmount" name="txtDebitAmount" placeholder="" />
                                                <span id="Span1" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Credit Amount</label>
                                                <input type="text" class="form-control" readonly="readonly" id="txtCreditAmount" name="txtCreditAmount" placeholder="" />
                                                <span id="Span8" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!--end #tab2 -->


                                <ul class="pager wizard ">
                                    <%--<li class="previous first disabled"><a class="btn-raised" href="javascript:void(0);">First</a></li>--%>
                                    <li class="previous disabled"><a class="btn-raised" href="javascript:void(0);">Previous</a></li>
                                    <%--<li class="next last"><a class="btn-raised" href="javascript:void(0);">Last</a></li>--%>
                                    <li class="next"><a class="btn-raised" href="javascript:void(0);">Next</a></li>
                                </ul>
                            </div>
                            <!--end .tab-content -->


                        </form>
                    </div>
                    <!--end #rootwizard -->
                    <!-- END FORM WIZARD -->



                </div>
                <%--</form> --%>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalAdd" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="width: 55%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body" id="paymentAndReceiptDetailForm">

                    <input type="hidden" id="txtMID" value="" />
                    <input type="hidden" id="Hidden2" value="" />
                    <%--<input type="hidden" id="txtAccount" value="" />--%>
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="Span2" class="new badge"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">A/C Code</label>
                                <input type="text" class="form-control" id="txtAccount" name="txtAccount" placeholder="" />
                                <span id="rAccount" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="form-group label-floating">
                                <label class="control-label">A/C Name</label>
                                <input type="text" class="form-control" id="txtAccountName2" name="txtAccountName" placeholder="" />
                                <span id="rAccountName" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <%-- <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">A/C Name ~ Code</label>
                                <input type="text" class="form-control" id="txtAccountCodeName" readonly="readonly" name="txtAccount" placeholder="" />
                            </div>
                        </div>--%>
                        <%--<input type="hidden" id="txtAccountName2" />--%>
                        <div class="col-sm-1" style="margin-top: 1.3em !important;">
                            <button type="button" class="btn btn-primary btn-sm pull-right" onclick="OnSearch()" title="Search Account Name/Code"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Location</label>
                                <select id="ddlLocation" class="form-control"></select>
                                <span id="rLocation" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Brand</label>
                                <select id="ddlBrand" class="form-control"></select>
                                <span id="rBrand" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Department</label>
                                <select id="ddlDepartment" class="form-control"></select>
                                <span id="rDepartment" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group label-floating">
                                <label class="control-label">Project</label>
                                <input type="text" class="form-control" id="txtProject" readonly="readonly" name="txtProject" placeholder="" />
                                <span id="rProject" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-1" style="margin-top: 1.3em !important;">
                            <button type="button" class="btn btn-primary btn-sm pull-right" onclick="OnSearchProject()" title="Search Project"><i class="fas fa-search"></i></button>
                        </div>

                        <div class="col-sm-5">
                            <div class="form-group label-floating">
                                <label class="control-label">Employee</label>
                                <input type="text" class="form-control" id="txtEmployee" readonly="readonly" name="txtEmployee" placeholder="" />
                                <span id="rEmployee" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-1" style="margin-top: 1.3em !important;">
                            <button type="button" class="btn btn-primary btn-sm pull-right" onclick="OnSearchEmployee()" title="Search Employee"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-11">
                            <div class="form-group label-floating">
                                <label class="control-label">Particulars</label>
                                <input type="text" class="form-control" id="txtParticulars" name="txtParticulars" placeholder="" />
                                <span id="rParticulars" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-1" style="margin-top: 2em !important;">
                            <button type="button" class="btn-primary" title="Paste" onclick="OnPaste()"><i class="fas fa-paste"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Debit</label>
                                <input type="text" class="form-control" id="txtDebit" onkeypress="return isNumberKey(this,event);" name="txtDebit" placeholder="" />
                                <span id="rDebit" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Credit</label>
                                <input type="text" class="form-control" id="txtCredit" onkeypress="return isNumberKey(this,event);" name="txtCredit" placeholder="" />
                                <span id="rCredit" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Exit</button>
                            <button type="button" class="btn btn-fill btn-default pull-right" onclick="OnSaveDetail()">Save </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalViewAccountCode" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="OnClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H3"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id='tblAccountCode' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>Select</th>
                                        <th>A/C Code</th>
                                        <th>A/C Name</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalView2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="OnClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H4"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id='tblItemList' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>Select</th>
                                        <th>A/C Code</th>
                                        <th>A/C Name</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalProject" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="OnClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H2"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id='tblProject' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>Select</th>
                                        <th>ID</th>
                                        <th>Title</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalEmployee" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="OnClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H5"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id='tblEmployee' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>Select</th>
                                        <th>ID</th>
                                        <th>Name</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalShow" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="width: 50%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="OnClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1"></h4>
                </div>
                <div class="modal-body">
                    <h6>Search</h6>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Voucher No.</label>
                                <input type="text" class="form-control" id="txtVoucherNum" name="txtVoucherNum" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Voucher Date</label>
                                <input type="text" class="form-control" id="txtVoucherDate" name="txtVoucherDate" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Location</label>
                                <select id="ddlLocationSearch" class="form-control">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <button type="button" class="btn btn-primary pull-right" onclick="SearchFields()">Search</button>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-content table-responsive">
                                <table id='tableData' class="table table-striped table-bordered display" style="width: 100%">
                                    <thead class="header bg-primary">
                                        <tr class="">
                                            <th>ID</th>
                                            <th>Voucher#</th>
                                            <th>VoucherTypeID</th>
                                            <th>Date</th>
                                            <th>PayToOrReceivedFrom</th>
                                            <th>Description</th>
                                            <th></th>
                                            <th>Select</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="vouchersupportmodal" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="width: 55%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="supportModalLabel"></h4>
                </div>
                <div class="modal-body" id="voucherSupportForm">

                    <input type="hidden" id="txtSID" value="" />

                    <div class="row">
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Voucher ID</label>
                                <input type="text" class="form-control" id="txtvoucherid" name="txtAccount" placeholder="" readonly="readonly" />
                                <span id="rtxtvoucherid" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Account Code</label>
                                <input type="text" class="form-control" id="txtaccountcode" name="txtaccountcode" placeholder="" />
                                <span id="rtxtaccountcode" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Order No</label>
                                <input type="text" class="form-control" id="txtorderno" name="txtorderno" placeholder="" />
                                <span id="rtxtorderno" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Quantity</label>
                                <input type="text" class="form-control" id="txtquantity" name="txtquantity" placeholder="" />
                                <span id="rtxtquantity" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Currency</label>
                                <select id="ddlcurrency" class="form-control"></select>
                                <span id="rtxtcurrency" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Currency Rate</label>
                                <input type="text" class="form-control" id="txtcurrencyrate" name="txtcurrencyrate" placeholder="" />
                                <span id="rtxtcurrencyrate" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Expense Month</label>
                                <input type="text" class="form-control" id="txtexmonthdate" name="txtexmonth" placeholder="" />
                                <span id="rtxtexmonth" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Invoice Received</label>
                                <input type="text" class="form-control" id="txtinvoicereceiveddate" name="txtinvoicereceiveddate" placeholder="" />
                                <span id="rtxtinvoicereceiveddate" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Expense Currency</label>
                                <select id="ddlexcurrency" class="form-control"></select>
                                <span id="rexcurrency" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Exit</button>
                            <button type="button" class="btn btn-fill btn-default pull-right" onclick="OnSaveVoucherSupport()">Save </button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="voucherdeletemodal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record ?</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDelete()">Confirm</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="validusermodal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <%--<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete</h4>
                </div>--%>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <span id="invalidusermsg" class="new badge text-center"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="text-center">Please enter your username and password</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">face</i>
                                </span>
                                <div class="form-group label-floating">
                                    <label class="control-label">User Name</label>
                                    <input type="text" id="txtusername" placeholder="" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock_outline</i>
                                </span>
                                <div class="form-group label-floating">
                                    <label class="control-label">Password</label>
                                    <input type="password" id="txtpassword" placeholder="" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-fill btn-default" id="btncheck">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
