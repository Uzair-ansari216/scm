﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="Dealer.aspx.vb" Inherits="Bridge_SCM.Dealer" %>

<%@ Import Namespace="System.Web.Optimization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/DealerScript")%>



    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header card-header-text">
                    <h4 class="card-title" style="color: #000000">Customer</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-primary pull-right" id="btnAdd" _recordId="0" title="Add New Customer"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-content table-responsive">
                                <table id='tableData' class="table table-striped table-bordered display">
                                    <thead class="header bg-primary">
                                        <tr class="">
                                            <th>ID</th>
                                            <th>Contact Person</th>
                                            <th>Company</th>
                                            <th>Company</th>
                                            <th>Address</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Fax</th>
                                            <th>Login</th>
                                            <th>Email</th>
                                            <th>NTN</th>
                                            <th>STN</th>
                                            <th>Address</th>
                                            <th>Contact Person 1</th>
                                            <th>Contact Email 1</th>
                                            <th>Contact Info 1</th>
                                            <th>Contact Person 2</th>
                                            <th>Contact Email 2</th>
                                            <th>Contact Info 2</th>
                                            <th>Location</th>
                                            <th>Dealer</th>
                                            <th>credit Limit</th>
                                            <th>Debtor</th>
                                            <th>Debtor Opening</th>
                                            <th>Creditor</th>
                                            <th>Creditor Opening</th>
                                            <th>Invoice</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <%--</form> --%>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalAdd" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body" id="customerForm">
                    <div class="row">
                        <div class="col-sm-12">
                            <!--      Wizard container        -->
                            <div class="wizard-container">
                                <div class="card wizard-card" data-color="rose" id="wizardProfile">
                                    <form action="" method="">
                                        <!--        You can switch " data-color="purple" "  with one of the next bright colors: "green", "orange", "red", "blue"       -->

                                        <div class="wizard-navigation">
                                            <ul>
                                                <li class="active">
                                                    <a href="#member" data-toggle="tab">Customer</a>
                                                </li>
                                                <li>
                                                    <a href="#group" data-toggle="tab">Contact</a>
                                                </li>
                                                <li>
                                                    <a href="#committee" data-toggle="tab">Detail</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="member">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span id="ValidationSummary" class="new badge"></span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <%--<div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">ID</label>
                                                            <input type="text" class="form-control" id="txtID" name="txtID" placeholder="" />
                                                            <span id="rID" class="new badge  hide">required</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Customer Code</label>
                                                            <input type="text" class="form-control" id="txtDealerCode" name="txtDealerCode" placeholder="" />
                                                            <span id="rDealerCode" class="new badge  hide">required</span>
                                                        </div>
                                                    </div>--%>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Company Group</label>
                                                            <select id="ddlCustomerGroup" class="form-control"></select>
                                                            <span id="rCustomerGroup" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Company</label>
                                                            <input type="text" class="form-control" id="txtCompany" name="txtCompany" placeholder="" />
                                                            <span id="rCompany" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <%--<div class="col-sm-2">
                                                        <button type="button" class="btn btn-primary pull-right" onclick="OnSearch()"><i class="fa fa-search"></i></button>
                                                    </div>--%>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Billing Name</label>
                                                            <input type="text" class="form-control" id="txtBillingName" name="txtBillingName" placeholder="" />
                                                            <span id="rBillingName" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Address</label>
                                                            <input type="text" class="form-control" id="txtAddress" name="txtAddress" placeholder="" />
                                                            <span id="rAddress" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Country</label>
                                                            <select id="ddlCountry" class="form-control"></select>
                                                            <span id="rCountry" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">City</label>
                                                            <select id="ddlCity" class="form-control"></select>
                                                            <span id="rCity" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Phone</label>
                                                            <input type="text" class="form-control" id="txtPhone" name="txtPhone" placeholder="" />
                                                            <span id="rPhone" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Fax</label>
                                                            <input type="text" class="form-control" id="txtFax" name="txtFax" placeholder="" />
                                                            <span id="rFax" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">E-Mail</label>
                                                            <input type="text" class="form-control" id="txtEmail" name="txtEmail" placeholder="" />
                                                            <span id="rEmail" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Login</label>
                                                            <input type="text" class="form-control" id="txtLogin" name="txtLogin" placeholder="" />
                                                            <span id="rLogin" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">N.T.No</label>
                                                            <input type="text" class="form-control" id="txtNT" name="txtNT" placeholder="" />
                                                            <span id="rNT" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">S.T.No</label>
                                                            <input type="text" class="form-control" id="txtST" name="txtST" placeholder="" />
                                                            <span id="rST" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="group">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Contact Person 1</label>
                                                            <input type="text" class="form-control" id="txtContactPerson1" name="txtContactPerson1" placeholder="" />
                                                            <span id="rContactPerson1" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Contact Info. 1</label>
                                                            <input type="text" class="form-control" id="txtContactInfo1" name="txtContactInfo1" placeholder="" />
                                                            <span id="rContactInfo1" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Email 1</label>
                                                            <input type="text" class="form-control" id="txtEmail1" name="txtEmail1" placeholder="" />
                                                            <span id="rEmail1" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Contact Person 2</label>
                                                            <input type="text" class="form-control" id="txtContactPerson2" name="txtContactPerson2" placeholder="" />
                                                            <span id="rContactPerson2" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Contact Info. 2</label>
                                                            <input type="text" class="form-control" id="txtContactInfo2" name="txtContactInfo2" placeholder="" />
                                                            <span id="rContactInfo2" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Email 2</label>
                                                            <input type="text" class="form-control" id="txtEmail2" name="txtEmail2" placeholder="" />
                                                            <span id="rEmail2" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="tab-pane" id="committee">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Location</label>
                                                            <select id="ddlLocation" class="form-control"></select>
                                                            <span id="rLocation" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Customer Type</label>
                                                            <select id="ddlDealerType" class="form-control"></select>
                                                            <span id="rDealerType" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Credit Limit</label>
                                                            <input type="text" class="form-control" id="txtCreditLimit" name="txtCreditLimit" placeholder="" />
                                                            <span id="rCreditLimit" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Customer Category</label>
                                                            <select id="ddlDealerCategory" class="form-control"></select>
                                                            <span id="rDealerCategory" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Debtor A/C Code</label>
                                                            <input type="text" class="form-control" id="txtDebtorCode" name="txtDebtorCode" placeholder="" />
                                                            <span id="rDebtorCode" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Opening Balance</label>
                                                            <input type="text" class="form-control" id="txtOpeningBalanceD" name="txtOpeningBalanceD" placeholder="" />
                                                            <span id="rOpeningBalanceD" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Creditor A/C Code</label>
                                                            <input type="text" class="form-control" id="txtCreditorCode" name="txtCreditorCode" placeholder="" />
                                                            <span id="rCreditorCode" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Opening Balance</label>
                                                            <input type="text" class="form-control" id="txtOpeningBalanceC" name="txtOpeningBalanceC" placeholder="" />
                                                            <span id="rOpeningBalanceC" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 checkbox-radios">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input id="chkAvailable" type="checkbox" name="optionsCheckboxes" />
                                                                Available for invoice in A/R Module
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Sales Person</label>
                                                            <select id="ddlSalesPerson" class="form-control"></select>
                                                            <span id="rSalesPerson" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Pricing</label>
                                                            <select id="ddlPricing" class="form-control"></select>
                                                            <span id="rPricing" class="new hide"></span>
                                                            <span id="" class="hide"></span>
                                                            <span id="" class="hide requiredValidation"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="wizard-footer">
                                    <div class="pull-right">
                                        <input type='button' class='btn btn-next btn-fill btn-rose btn-wd' name='next' value='Next' />
                                        <input type='button' class='btn btn-finish btn-fill btn-rose btn-wd' name='finish' value='Finish' />
                                    </div>
                                    <div class="pull-left">
                                        <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Previous' />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                </form>
                            </div>
                        </div>
                        <!-- wizard container -->
                    </div>
                </div>
                <input type="hidden" id="txtID" value="" />


                <div class="modal-footer">
                    <button type="button" class="btn btn-fill btn-default" onclick="OnSave()">Save changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalView" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="OnClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H2"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <table id='tableDetail' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>Select</th>
                                        <th>ID</th>
                                        <th>Company Name</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDelete()">Confirm</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
