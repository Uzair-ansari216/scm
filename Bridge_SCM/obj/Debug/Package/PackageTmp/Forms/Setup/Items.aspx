﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="Items.aspx.vb" Inherits="Bridge_SCM.Items" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/ItemScript")%>



    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header card-header-text">
                    <h4 class="card-title" style="color: #000000">Item</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnAdd()" title="Add New Item"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">

                            <table id='tableData' class="table table-striped table-bordered display">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>ID                  </th>
                                        <th>Part Number         </th>
                                        <th>Model Number         </th>
                                        <th>Description         </th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>
                <%--</form> --%>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalAdd" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body" id="itemForm">

                    <input type="hidden" id="txtID" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="ValidationSummary" class="new badge"></span>
                        </div>
                    </div>
                    <%--<div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6">
                            <input type="file" name="imgfile" id="imgfile" class="hidden" />
                            <img id="userimg" src="#" style="height: 150px; width: 170px; margin-left: 25%" class="img-responsive" />
                            <a id="upload" href="#" style="font-weight: bold; color: #526A76; margin-left: 40%;">Upload Photo</a>
                            <span id="imgLabel"></span>
                        </div>
                    </div>--%>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Category</label>
                                <select id="ddlitemcategory" class="form-control"></select>
                                <span id="rItemcategory" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Brand</label>
                                <select id="ddlitembrand" class="form-control"></select>
                                <span id="rItembrand" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Unit</label>
                                <select id="ddlitemunit" class="form-control"></select>
                                <span id="rItemunit" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Part Number</label>
                                <input type="text" class="form-control" id="txtpartnumber" name="txtpartnumber" placeholder="" />
                                <span id="rPartnumber" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Model Number</label>
                                <input type="text" class="form-control" id="txtmodelnumber" name="txtmodelnumber" placeholder="" />
                                <span id="rModelnumber" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Product Name</label>
                                <input type="text" class="form-control" id="txtproductname" name="txtproductname" placeholder="" />
                                <span id="rProductname" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Description</label>
                                <input type="text" class="form-control" id="txtdescription" name="txtdescription" placeholder="" />
                                <span id="rDescription" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Min Qty</label>
                                <input type="text" class="form-control" id="txtminqty" name="txtminqty" placeholder="" />
                                <span id="rMinqty" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Max Qty</label>
                                <input type="text" class="form-control" id="txtmaxqty" name="txtmaxqty" placeholder="" />
                                <span id="rMaxqty" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Min Order Qty</label>
                                <input type="text" class="form-control" id="txtminorderqty" name="txtminorderqty" placeholder="" />
                                <span id="rMinorderqty" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">D.L Time</label>
                                <input type="text" class="form-control" id="txtdltime" name="txtdltime" placeholder="" />
                                <span id="rDltime" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Sales Rate</label>
                                <input type="text" class="form-control" id="txtsalesrate" name="txtsalesrate" placeholder="" />
                                <span id="rSalesrate" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Exp GST</label>
                                <input type="text" class="form-control" id="txtexpgst" name="txtexpgst" placeholder="" />
                                <span id="rExpgst" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Warranty Year</label>
                                <input type="text" class="form-control" id="txtwarrantyyear" name="txtwarrantyyear" placeholder="" />
                                <span id="rWarrantyyear" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Warranty Note</label>
                                <input type="text" class="form-control" id="txtwarrantynote" name="txtwarrantynote" placeholder="" />
                                <span id="rWarrantynote" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>


                    <div class="row">

                        <div class="col-sm-4 checkbox-radios">
                            <div class="checkbox">
                                <label>
                                    <input id="isactive" type="checkbox" name="isactive" />Is Active
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-4 checkbox-radios">
                            <div class="checkbox">
                                <label>
                                    <input id="gstapplicable" type="checkbox" name="gstapplicable" />
                                    GST Applicable
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4 checkbox-radios">
                            <div class="checkbox">
                                <label>
                                    <input id="availableforso" type="checkbox" name="availableforso" />
                                    Available For SO
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-4 checkbox-radios">
                            <div class="checkbox">
                                <label>
                                    <input id="itemtoberepost" type="checkbox" name="itemtoberepost" />
                                    Item to be Repost
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-4 checkbox-radios">
                            <div class="checkbox">
                                <label>
                                    <input id="issupportpart" type="checkbox" name="issupportpart" />Is Support Part
                                </label>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-fill btn-default" onclick="OnSave()">Save changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDelete()">Confirm</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
