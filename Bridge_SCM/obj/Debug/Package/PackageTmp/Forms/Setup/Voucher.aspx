﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="Voucher.aspx.vb" Inherits="Bridge_SCM.Voucher" %>

<%@ Import Namespace="System.Web.Optimization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/VoucherScript")%>
    <script type="text/javascript" language="javascript">
        function isNumberKey(sender, evt) {
            var txt = sender.value;
            var dotcontainer = txt.split('.');
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (!(dotcontainer.length == 1 && charCode == 46) && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <script type="text/javascript" language="javascript">
        var loginUrl = "/Default.aspx";


        var pagename = "Voucher.aspx";

        function OnPrint() {

            var reportPath = '<%= System.Configuration.ConfigurationManager.AppSettings("ReportPath").ToString() %>'
            var VoucherID = $("#txtID").val();

            var urlname = "Print";
            if (VoucherID == "") {
                alert("ID is required")
            }
            else {
                $.ajax({
                    type: "POST",
                    url: '' + pagename + '/' + urlname + '?VoucherID=' + VoucherID,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (res) {
                        var data = JSON.parse(res.d);

                        if (data.Status) {
                            window.open(reportPath + '?isGeneratedByBuilder=' + false);
                            $("#FormError").css('display', 'none');
                            $("#Error").empty();

                        } else {
                            $("#Validation").fadeIn(1000);
                            $("#Validation").text(data.Message);
                            $("#Validation").fadeOut(5000);
                        }
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

    </script>

    <div class="row">
        <div class="col-md-12">
            <div class="card">

                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                    </div>

                    <input type="hidden" id="txtID" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="ValidationSummary" class="new badge"></span>
                        </div>
                    </div>
                    <!-- BEGIN FORM WIZARD -->
                    <div id="formwizard_simple" class="form-wizard form-wizard-horizontal">
                        <form class="form floating-label">
                            <div class="row">
                                <div class="col-sm-2">
                                    <h4 class="card-title" style="color: #000000">General Voucher</h4>
                                </div>
                                <div class="col-sm-7">
                                    <div class="form-wizard-nav">
                                        <div class="progress" style="width: 50%;">
                                            <div class="progress-bar progress-bar-primary" style="width: 0%;"></div>
                                        </div>
                                        <ul class="nav nav-justified">
                                            <li class="active"><a href="#fws_tab1" data-toggle="tab" aria-expanded="false"><span class="step">1</span> <span class="title">Voucher</span></a></li>
                                            <li class=""><a href="#fws_tab2" data-toggle="tab" aria-expanded="true"><span class="step">2</span> <span class="title">Detail</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <button type="button" class="btn btn-primary pull-right" id="btndelete" title="Delete"><i class="fas fa-trash"></i></button>
                                    <button type="button" class="btn btn-primary pull-right" onclick="OnPrint()" title="Print"><i class="fas fa-print"></i></button>
                                    <button type="button" class="btn btn-primary pull-right" onclick="AddNew()" title="Refresh"><i class="fas fa-sync-alt"></i></button>
                                    <button type="button" class="btn btn-primary pull-right" onclick="Show()" title="Search">
                                        <i class="fas fa-search"></i>
                                        <div class="ripple-container"></div>
                                    </button>
                                </div>
                            </div>
                            <!--end .form-wizard-nav -->

                            <div class="tab-content clearfix " id="voucherForm">
                                <div class="tab-pane active" id="fws_tab1">

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Date</label>
                                                <input type="text" class="form-control" id="txtDate" name="txtDate" placeholder="" />
                                                <span id="rDate" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Voucher Type</label>
                                                <select id="ddlVoucherType" class="form-control"></select>
                                                <span id="rVoucherType" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Voucher No</label>
                                                <input type="text" class="form-control" id="txtVoucherNo" name="txtVoucherNo" placeholder="" />
                                                <span id="rVoucherNo" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Type</label>
                                                <select id="ddlType" class="form-control"></select>
                                                <span id="rType" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Name</label>
                                                <select id="ddlName" class="form-control"></select>
                                                <span id="rName" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Invoice No</label>
                                                <input type="text" class="form-control" id="txtInvoiceNo" name="txtInvoiceNo" placeholder="" />
                                                <span id="rInvoiceNo" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Invoice Date</label>
                                                <input type="text" class="form-control" id="txtInvoiceDate" name="txtInvoiceDate" placeholder="" />
                                                <span id="rInvoiceDate" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Due Days</label>
                                                <input type="text" class="form-control" id="txtDueDays" name="txtDueDays" placeholder="" />
                                                <span id="rDueDays" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Due Date</label>
                                                <input type="text" class="form-control" id="txtDueDate" name="txtDueDate" placeholder="" />
                                                <span id="rDueDate" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Description</label>
                                                <input type="text" class="form-control" id="txtDescription" name="txtDescription" placeholder="" />
                                                <span id="rDescription" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end #tab1 -->

                                <div class="tab-pane" id="fws_tab2">

                                    <div class="row">

                                        <div class="col-sm-12">
                                            <button type="button" class="btn btn-primary pull-right" onclick="OnAdd()" title="Add"><i class="fas fa-plus"></i></button>
                                            <button type="button" class="btn btn-primary pull-right" onclick="OnSave()" title="Save" id="btnsave"><i class="fas fa-save"></i></button>
                                            <button type="button" class="btn btn-primary pull-right" onclick="OnSupport()" title="Voucher Support"><i class="fas fa-exchange-alt"></i></button>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-content table-responsive">
                                                <table id='tableDetail' class="table table-striped table-bordered display" style="width: 100%">
                                                    <thead class="header bg-primary">
                                                        <tr class="">
                                                            <th>rowID</th>
                                                            <th>VoucherID</th>
                                                            <th>Account</th>
                                                            <th>AccountCode</th>
                                                            <th>AccountName</th>
                                                            <th>LocationID</th>
                                                            <th>Location</th>
                                                            <th>DepartmentID</th>
                                                            <th>Department</th>
                                                            <th>SegmentID</th>
                                                            <th>Segment</th>
                                                            <th>ProjectID</th>
                                                            <th>Project</th>
                                                            <th>EmployeeID</th>
                                                            <th>Employee</th>
                                                            <th>Particulars</th>
                                                            <th>Debit</th>
                                                            <th>Credit</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-2">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Debit Amount</label>
                                                <input type="text" class="form-control" readonly="readonly" id="txtDebitAmount" name="txtDebitAmount" placeholder="" />
                                                <span id="rDebitAmount" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Credit Amount</label>
                                                <input type="text" class="form-control" readonly="readonly" id="txtCreditAmount" name="txtCreditAmount" placeholder="" />
                                                <span id="rCreditAmount" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!--end #tab2 -->


                                <ul class="pager wizard ">
                                    <li class="previous first disabled"><a class="btn-raised" href="javascript:void(0);">First</a></li>
                                    <li class="previous disabled"><a class="btn-raised" href="javascript:void(0);">Previous</a></li>
                                    <li class="next last"><a class="btn-raised" href="javascript:void(0);">Last</a></li>
                                    <li class="next"><a class="btn-raised" href="javascript:void(0);">Next</a></li>
                                </ul>
                            </div>
                            <!--end .tab-content -->


                        </form>
                    </div>
                    <!--end #rootwizard -->
                    <!-- END FORM WIZARD -->

                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalAdd" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="width: 55%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body" id="voucherDetailForm">

                    <input type="hidden" id="txtMID" value="" />
                    <input type="hidden" id="Hidden2" value="" />
                    <%--<input type="hidden" id="txtAccount" value="" />--%>
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="Span2" class="new badge"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">A/C Code</label>
                                <input type="text" class="form-control" id="txtAccount" name="txtAccount" placeholder="" />
                                <span id="rAccountCode" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="form-group label-floating">
                                <label class="control-label">A/C Name</label>
                                <input type="text" class="form-control" id="txtAccountName" name="txtAccountName" placeholder="" />
                                <span id="rAccountName" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <%--<div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">A/C Name ~ Code</label>
                                <input type="text" class="form-control" id="txtAccountCodeName" name="txtAccount" placeholder="" />
                            </div>
                        </div>--%>
                        <%--<input type="hidden" id="txtAccountName" />--%>
                        <div class="col-sm-1" style="margin-top: 1.3em !important;">
                            <button type="button" class="btn btn-sm btn-primary pull-right" onclick="OnSearch()"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Location</label>
                                <select id="ddlLocation" class="form-control"></select>
                                <span id="rLocation" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Brand</label>
                                <select id="ddlBrand" class="form-control"></select>
                                <span id="rBrand" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Department</label>
                                <select id="ddlDepartment" class="form-control"></select>
                                <span id="rDepartment" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group label-floating">
                                <label class="control-label">Project</label>
                                <input type="text" class="form-control" id="txtProject" readonly="readonly" name="txtProject" placeholder="" />
                                <span id="rProject" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-1" style="margin-top: 1.3em !important;">
                            <button type="button" class="btn btn-sm btn-primary pull-right" onclick="OnSearchProject()"><i class="fas fa-search"></i></button>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group label-floating">
                                <label class="control-label">Employee</label>
                                <input type="text" class="form-control" id="txtEmployee" readonly="readonly" name="txtEmployee" placeholder="" />
                                <span id="rEmployee" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-1" style="margin-top: 1.3em !important;">
                            <button type="button" class="btn btn-sm btn-primary pull-right" onclick="OnSearchEmployee()"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">Particulars</label>
                                <input type="text" class="form-control" id="txtParticulars" name="txtParticulars" placeholder="" />
                                <span id="rParticulars" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="btn-primary" title="Paste" onclick="OnPaste()"><i class="fa fa-retweet"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Debit</label>
                                <input type="text" class="form-control" onkeypress="return isNumberKey(this,event);" id="txtDebit" name="txtDebit" placeholder="" />
                                <span id="rDebit" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Credit</label>
                                <input type="text" class="form-control" onkeypress="return isNumberKey(this,event);" id="txtCredit" name="txtCredit" placeholder="" />
                                <span id="rCredit" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Exit</button>
                            <button type="button" class="btn btn-fill btn-default pull-right" onclick="OnSaveDetail()">Save </button>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalView2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="OnClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H4"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id='tblItemList' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>Select</th>
                                        <th>A/C Code</th>
                                        <th>A/C Name</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalProject" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="OnClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H2"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id='tblProject' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>Select</th>
                                        <th>ID</th>
                                        <th>Title</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalEmployee" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="OnClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H5"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id='tblEmployee' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>Select</th>
                                        <th>ID</th>
                                        <th>Name</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalShow" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="width: 50%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="OnClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1"></h4>
                </div>
                <div class="modal-body">
                    <h6>Search</h6>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Voucher No.</label>
                                <input type="text" class="form-control" id="txtVoucherNum" name="txtVoucherNum" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Voucher Date</label>
                                <input type="text" class="form-control" id="txtVoucherDate" name="txtVoucherDate" placeholder="" />
                            </div>
                        </div>
                        <%--<div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Location</label>
                                <select id="ddlLocationSearch" class="form-control">
                                </select>
                            </div>
                        </div>--%>
                        <button type="button" class="btn btn-primary pull-right" onclick="SearchFields()">Search</button>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-content table-responsive">
                                <table id='tableData' class="table table-striped table-bordered display" style="width: 100%">
                                    <thead class="header bg-primary">
                                        <tr class="">
                                            <th>ID</th>
                                            <th>Voucher#</th>
                                            <th>VoucherTypeID</th>
                                            <th>Date</th>
                                            <th>DueDate</th>
                                            <th>PayToOrReceivedFrom</th>
                                            <th>Description</th>
                                            <th></th>
                                            <th></th>
                                            <th>Select</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="vouchersupportmodal" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="width: 55%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="supportModalLabel"></h4>
                </div>
                <div class="modal-body" id="voucherSupportForm">

                    <input type="hidden" id="txtSID" value="" />

                    <div class="row">
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Voucher ID</label>
                                <input type="text" class="form-control" id="txtvoucherid" name="txtAccount" placeholder="" readonly="readonly" />
                                <span id="rtxtvoucherid" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Account Code</label>
                                <input type="text" class="form-control" id="txtaccountcode" name="txtaccountcode" placeholder="" />
                                <span id="rtxtaccountcode" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Order No</label>
                                <input type="text" class="form-control" id="txtorderno" name="txtorderno" placeholder="" />
                                <span id="rtxtorderno" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Quantity</label>
                                <input type="text" class="form-control" id="txtquantity" name="txtquantity" placeholder="" />
                                <span id="rtxtquantity" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Currency</label>
                                <select id="ddlcurrency" class="form-control"></select>
                                <span id="rtxtcurrency" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Currency Rate</label>
                                <input type="text" class="form-control" id="txtcurrencyrate" name="txtcurrencyrate" placeholder="" />
                                <span id="rtxtcurrencyrate" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Expense Month</label>
                                <input type="text" class="form-control" id="txtexmonthdate" name="txtexmonth" placeholder="" />
                                <span id="rtxtexmonth" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Invoice Received</label>
                                <input type="text" class="form-control" id="txtinvoicereceiveddate" name="txtinvoicereceiveddate" placeholder="" />
                                <span id="rtxtinvoicereceiveddate" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Expense Currency</label>
                                <select id="ddlexcurrency" class="form-control"></select>
                                <span id="rexcurrency" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Exit</button>
                            <button type="button" class="btn btn-fill btn-default pull-right" onclick="OnSaveVoucherSupport()">Save </button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="voucherdeletemodal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record ?</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDelete()">Confirm</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="validusermodal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <%--<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete</h4>
                </div>--%>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <span id="invalidusermsg" class="new badge text-center"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="text-center">Please enter your username and password</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">face</i>
                                </span>
                                <div class="form-group label-floating">
                                    <label class="control-label">User Name</label>
                                    <input type="text" id="txtusername" placeholder="" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock_outline</i>
                                </span>
                                <div class="form-group label-floating">
                                    <label class="control-label">Password</label>
                                    <input type="password" id="txtpassword" placeholder="" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-fill btn-default" id="btncheck">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    
    
    <script>
        $(document).ready(function () {
            $("#txtexmonthdate , #txtinvoicereceiveddate").datepicker({
                autoclose: true, changeMonth: true,
                changeYear: true, yearRange: "-90:+00"
            });
            clearAllMessages("voucherForm")
            debugger
            var service_DeveloperMode = $("#hdnDevMode").val()
            if (service_DeveloperMode == 'on') {
                addFormFields("General Voucher", "voucherForm")
            }
            //var service_ToolTip = $("#hdnToolTip").val()
            //if (service_ToolTip == 'on') {

            setToolTip("General Voucher", "voucherForm", service_DeveloperMode)

            //}
        });

        function OnLoadGridAccountCode() {
            debugger
            $.ajax({
                type: "POST",
                url: "" + serviceurl + "/loadGridDetail",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $(tblDetail).DataTable().destroy();
                    LoadDataTableDetail(response.d);
                },
                error: OnErrorCall
            });
        }
    </script>

</asp:Content>
