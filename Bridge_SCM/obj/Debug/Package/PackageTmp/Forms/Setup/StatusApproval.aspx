﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="StatusApproval.aspx.vb" Inherits="Bridge_SCM.StatusApproval" %>

<%@ Import Namespace="System.Web.Optimization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/StatusApprovalScript")%>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header card-header-text">
                    <h4 class="card-title" style="color:#000000">Status Approval</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                        <%-- <div class="col-sm-3">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnAdd()"><i class="fa fa-plus"></i></button>
                        </div>--%>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">

                            <table id='tableData' class="table table-striped table-bordered display">
                                <thead class="header bg-primary">
                                    <tr class="">
                           <%--             <th><span class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="optionsCheckboxes" id="chkSelectMain"></label></span></th>--%>
                                        <th></th>
                                        <th>Document #</th>
                                        <th>Date</th>
                                        <th>Customer</th>
                                        <th>Sales Person</th>
                                        <th>References</th>
                                        <th>Remarks</th>
                                        <th>PK_SalesOrder</th>
                                        <th>Status2</th>
                                        <th>StatusRemarks</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>
                <%--</form> --%>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Status</label>
                                <select id="ddlStatus" class="form-control">
                                </select>
                                <span id="rStatus" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Status Date</label>
                                <input type="text" class="form-control" id="txtStatusDate" name="txtStatusDate" placeholder="" />
                                <span id="rStatusDate" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Remarks</label>
                                <input type="text" class="form-control" id="txtStatusRemarks" name="txtStatusRemarks" placeholder="" />
                                <span id="rStatusRemarks" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-6"></div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnSave()">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="txtID" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="ValidationSummary" class="new badge"></span>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Fault Type</label>
                                <input type="text" class="form-control" id="txtName" name="txtName" placeholder="" />
                                <span id="rName" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Safety Instructions</label>
                                <textarea class="form-control" id="txtSafetyIns" name="txtSafetyIns"></textarea>
                                <%--<input type="text" class="form-control" id="txtSafetyIns" name="txtSafetyIns" placeholder="" />--%>
                                <span id="rSafety" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnSave()">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDelete()">Confirm</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
