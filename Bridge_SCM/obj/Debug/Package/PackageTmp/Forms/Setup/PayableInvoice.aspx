﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="PayableInvoice.aspx.vb" Inherits="Bridge_SCM.PayableInvoice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-text">
                    <h4 class="card-title" style="color: #000000">Invoice</h4>
                </div>
                <div class="card-content">
                    <div class="modal-body" id="brandForm">
                        <input type="hidden" id="txtID" value="" />
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <span id="Validation" class="new badge"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Purchase Order No</label>
                                    <input type="text" class="form-control" id="txtpo" name="txtpo" placeholder="" />
                                    <span id="rPO" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label class="control-label">Supplier NTN/ CNIC</label>
                                    <input type="text" class="form-control" id="txtsuppliercnic" name="txtsuppliercnic" placeholder="" />
                                    <span id="rSupplierCnic" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label class="control-label">Supplier Name</label>
                                    <input type="text" class="form-control" id="txtsuppliername" name="txtsuppliername" placeholder="" />
                                    <span id="rSupplierName" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label class="control-label">Supplier No</label>
                                    <input type="text" class="form-control" id="txtsupplierno" name="txtsupplierno" placeholder="" />
                                    <span id="rSupplierNo" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Supplier Site Name</label>
                                    <select id="ddlSupplierSiteName" class="form-control"></select>
                                    <span id="rSupplierSiteName" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Currency</label>
                                    <select id="ddlCurrency" class="form-control"></select>
                                    <span id="rCurrency" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label class="control-label">Invoice Date</label>
                                    <input type="text" class="form-control datepicker" id="txtinvoicedate" name="txtinvoicedate" placeholder="" />
                                    <span id="rInvoiceDate" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label class="control-label">Invoice No</label>
                                    <input type="text" class="form-control" id="txtinvoicenumber" name="txtinvoicenumber" placeholder="" />
                                    <span id="rInvoiceNumber" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label class="control-label">Invoice Amount</label>
                                    <input type="text" class="form-control" id="txtinvoiceno" name="txtinvoiceno" placeholder="" />
                                    <span id="rInvoiceNo" class="new hide"></span>
                                    <span id="" class="hide"></span>
                                    <span id="" class="hide requiredValidation"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-fill btn-default" onclick="OnSave()">Save changes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
