﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="SalesOrder.aspx.vb" Inherits="Bridge_SCM.SalesOrder" %>

<%@ Import Namespace="System.Web.Optimization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/SalesOrderScript")%>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header card-header-text">
                    <h4 class="card-title" style="color:#000000">Sales Order</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnAdd()"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-content table-responsive">
                                <table id='tableData' class="table table-striped table-bordered display">
                                    <thead class="header bg-primary">
                                        <tr class="">
                                            <th>ID</th>
                                            <th>SalesOrder</th>
                                            <th>Customer</th>
                                            <th>Date</th>
                                            <th>SalesPerson</th>
                                            <th>Status</th>
                                            <th>End Date</th>
                                            <th>Status</th>
                                            <th></th>
                                            <th>ID</th>
                                            <th>Dated</th>
                                            <th>Title</th>
                                            <th>Reasons</th>
                                            <th>Budget</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Status</th>
                                            <th></th>
                                            <th>ID</th>
                                            <th>Dated</th>
                                            <th>Title</th>
                                            <th>Reasons</th>
                                            <th>Budget</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Status</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <%--</form> --%>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="txtID" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="ValidationSummary" class="new badge"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Doc Date</label>
                                <input type="text" class="form-control" id="txtDate" name="txtDate" placeholder="" />
                                <span id="rDate" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Customer</label>
                                <select id="ddlCustomer" class="form-control">
                                </select>
                                <span id="rCustomer" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Ship To</label>
                                <input type="text" class="form-control" id="txtShipTo" name="txtShipTo" placeholder="" />
                                <span id="rShipTo" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Bill To</label>
                                <input type="text" class="form-control" id="txtBillTo" name="txtBillTo" placeholder="" />
                                <span id="rBillTo" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Location</label>
                                <select id="ddlLocation" class="form-control">
                                </select>
                                <span id="rLocation" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Sales Person</label>
                                <select id="ddlSalesPerson" class="form-control">
                                </select>
                                <span id="rSalesPerson" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Currency</label>
                                <select id="ddlCurrency" class="form-control">
                                </select>
                                <span id="rCurrency" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Rate</label>
                                <input type="text" class="form-control" id="txtCurrency" name="txtCurrency" placeholder="" />
                                <span id="rtxtCurrency" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">SO Type</label>
                                <select id="ddlSOType" class="form-control">
                                </select>
                                <span id="rSOType" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Terms</label>
                                <select id="ddlTerms" class="form-control">
                                </select>
                                <span id="rTerms" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">GST</label>
                                <input type="text" class="form-control" id="txtGST" name="txtGST" placeholder="" />
                                <span id="rGST" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Charges</label>
                                <input type="text" class="form-control" id="txtCharges" name="txtCharges" placeholder="" />
                                <span id="rCharges" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Remarks</label>
                                <input type="text" class="form-control" id="txtRemarks" name="txtRemarks" placeholder="" />
                                <span id="rRemarks" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Billing Remarks</label>
                                <input type="text" class="form-control" id="txtBillings" name="txtBillings" placeholder="" />
                                <span id="rBillings" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Status</label>
                                <select id="ddlStatus" class="form-control">
                                </select>
                                <span id="rStatus" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Status Date</label>
                                <input type="text" class="form-control" id="txtStatusDate" name="txtStatusDate" placeholder="" />
                                <span id="rStatusDate" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Remarks</label>
                                <input type="text" class="form-control" id="txtStatusRemarks" name="txtStatusRemarks" placeholder="" />
                                <span id="rStatusRemarks" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnSave()">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalView" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="width: 55%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H2"></h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="txtCID" value="" />
                    <input type="hidden" id="txtMID" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="Validation2" class="new badge"></span>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">Model #</label>
                                <input type="text" class="form-control" id="txtModel" readonly="readonly" name="txtModel" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnSearch()"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Part #</label>
                                <input type="text" class="form-control" id="txtPart" name="txtPart" placeholder="" />
                                <span id="rPart" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Default Description</label>
                                <input type="text" class="form-control" id="txtDefaultDescription" name="txtDefaultDescription" placeholder="" />
                                <span id="rDefaultDescription" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Amendments Description</label>
                                <input type="text" class="form-control" id="txtAmendmentsDescription" name="txtAmendmentsDescription" placeholder="" />
                                <span id="rAmendmentsDescription" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Unit</label>
                                <input type="text" class="form-control" id="txtUnit" name="txtUnit" placeholder="" />
                                <span id="rUnit" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Stock</label>
                                <input type="text" class="form-control" id="txtStock" name="txtStock" placeholder="" />
                                <span id="rStock" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Quantity</label>
                                <input type="text" class="form-control" id="txtQuantity" name="txtQuantity" placeholder="" />
                                <span id="rQuantity" class="new badge  hide">required</span>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Before Discount Price</label>
                                <input type="text" class="form-control" id="txtBDPrice" name="txtBDPrice" placeholder="" />
                                <span id="rBDPrice" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Discount</label>
                                <input type="text" class="form-control" id="txtDiscount" name="txtDiscount" placeholder="" />
                                <span id="rDiscount" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Unit Price</label>
                                <input type="text" class="form-control" id="txtUnitPrice" name="txtUnitPrice" placeholder="" />
                                <span id="rUnitPrice" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">PKR Amount</label>
                                <input type="text" class="form-control" id="txtAmount" name="txtAmount" placeholder="" />
                                <span id="rAmount" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Remarks</label>
                                <input type="text" class="form-control" id="txtItemRemarks" name="txtItemRemarks" placeholder="" />
                                <span id="rItemRemarks" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnSaveDetail()">Save changes</button>
                </div>
                <hr />
                <div class="row">
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <div class="card-content table-responsive">
                                <table id='tableDetail' class="table table-striped table-bordered display" style="width: 100%">
                                    <thead class="header bg-primary">
                                        <tr class="">
                                            <th>ID</th>
                                            <th>Model#</th>
                                            <th>PartNumber</th>
                                            <th>Description</th>
                                            <th>Amendment</th>
                                            <th>Qty</th>
                                            <th>Unit</th>
                                            <th>U.Price</th>
                                            <th>Remarks</th>
                                            <th>B.D Price</th>
                                            <th>Discount</th>
                                            <th>A.D Price</th>
                                            <th>Amount</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Amount</label>
                                <input type="text" class="form-control" id="txtIAmount" name="txtIAmount" placeholder="" />
                                <span id="Span1" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group label-floating">
                                <label class="control-label">GST %</label>
                                <input type="text" class="form-control" id="txtIGST" name="txtIGST" placeholder="" />
                                <span id="Span3" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">GST Amount</label>
                                <input type="text" class="form-control" id="txtIGSTAmount" name="txtIGSTAmount" placeholder="" />
                                <span id="Span2" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group label-floating">
                                <label class="control-label">Charges</label>
                                <input type="text" class="form-control" id="txtICharges" name="txtICharges" placeholder="" />
                                <span id="Span4" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Total Amount</label>
                                <input type="text" class="form-control" id="txtITotalAmount" name="txtITotalAmount" placeholder="" />
                                <span id="Span5" class="new badge  hide">required</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDelete()">Confirm</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDelete2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H3">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDeleteDetail()">Confirm</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalView2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="OnClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H4"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <table id='tblItemList' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>Select</th>
                                        <th>ID</th>
                                        <th>Part#</th>
                                        <th>Model#</th>
                                        <th>Product</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
