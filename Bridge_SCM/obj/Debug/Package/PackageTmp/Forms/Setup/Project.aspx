﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="Project.aspx.vb" Inherits="Bridge_SCM.Project" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/ProjectScript")%>
    <style>
        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #2196f3;
            border-top: 0px;
            border-left: 0px;
            border-right: 0px;
            border-radius: 1px;
        }
        .select2-container {
    box-sizing: border-box;
    display: inline-block;
    margin: 0px;
    position: relative;
    vertical-align: middle;
    margin-top: 7px;
}
    </style>


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header card-header-text">
                    <h4 class="card-title" style="color: #000000">Project</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-primary pull-right" id="btnAdd" _recordid="0"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">

                            <table id='tableData' class="table table-striped table-bordered display">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>Project Code        </th>
                                        <th>Project Title       </th>
                                        <th>Project Type        </th>
                                        <th>Company               </th>
                                        <th>Clint               </th>
                                        <th>Start Date          </th>
                                        <th>End Date            </th>
                                        <th>Department Code     </th>
                                        <th>Duration Days       </th>
                                        <th>Estimated Cost      </th>
                                        <th>Debotr Account      </th>
                                        <th>Opening Balance     </th>
                                        <th>Creditor Account    </th>
                                        <th>Opening Balance     </th>
                                        <th>Description         </th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>
                <%--</form> --%>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalAdd" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body" id="projectForm">

                    <input type="hidden" id="txtID" value="" />
                    <input type="hidden" id="txtprojectcode" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="ValidationSummary" class="new badge"></span>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label"></label>
                                <input type="text" class="form-control" id="txtprojecttitle" name="txtprojecttitle" placeholder="" />
                                <span id="rProjectTitle" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label"></label>
                                <select id="ddlclient" class="form-control client"></select>
                                <span id="rClient" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label"></label>
                                <select id="ddlprojecttype" class="form-control projectType select2" style="width: 100%;"></select>
                                <span id="rProjectType" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label"></label>
                                <input type="text" class="form-control" id="txtstartdate" name="txtstartdate" placeholder="" />
                                <span id="rStartDate" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label"></label>
                                <input type="text" class="form-control" id="txtenddate" name="txtenddate" placeholder="" />
                                <span id="rEndDate" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label"></label>
                                <select id="ddldepartmentcode" class="form-control departmentCode"></select>
                                <span id="rDepartmentCode" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label"></label>
                                <input type="text" class="form-control" id="txtdurationdays" name="txtdurationdays" placeholder="" />
                                <span id="rDurationDays" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label"></label>
                                <input type="text" class="form-control" id="txtestimatedcost" name="txtestimatedcost" placeholder="" />
                                <span id="rEstimatedCost" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-sm-5">
                            <div class="form-group label-floating">
                                <label class="control-label"></label>
                                <input type="text" class="form-control" id="txtdebotraccount" name="txtdebotraccount" placeholder="" />
                                <span id="rDebortAccount" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="btn btn-primary btn-sm pull-right" onclick="OnSearchBankAccount('debitor')"><i class="fas fa-search"></i></button>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label"></label>
                                <input type="text" class="form-control" id="txtopeningbalance" name="txtopeningbalance" placeholder="" />
                                <span id="rOpeningBalance" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>


                    </div>

                    <div class="row">

                        <div class="col-sm-5">
                            <div class="form-group label-floating">
                                <label class="control-label"></label>
                                <input type="text" class="form-control" id="txtcreditoraccount" name="txtcreditoraccount" placeholder="" />
                                <span id="rCreditorAccount" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="btn btn-primary btn-sm pull-right" onclick="OnSearchBankAccount('creditor')"><i class="fas fa-search"></i></button>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label"></label>
                                <input type="text" class="form-control" id="txtcopeningbalance" name="txtcopeningbalance" placeholder="" />
                                <span id="rCopeningBalance" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label"></label>
                                <input type="text" class="form-control" id="txtdescription" name="txtdescription" placeholder="" />
                                <span id="rDescription" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnSave()">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDelete()">Confirm</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalViewAccountCode" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="OnClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H3"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id='tblAccountCode' class="table table-striped table-bordered display" style="width: 100%">
                                <thead class="header bg-primary">
                                    <tr class="">
                                        <th>Select</th>
                                        <th>A/C Code</th>
                                        <th>A/C Name</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        
    </script>
</asp:Content>
