﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="Location.aspx.vb" Inherits="Bridge_SCM.Location" %>

<%@ Import Namespace="System.Web.Optimization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/LocationScript")%>

    <style>
        #txtSafetyIns {
            width: 100%;
            height: 200px;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header card-header-text">
                    <h4 class="card-title" style="color: #000000">Location</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-primary pull-right" id="btnAdd" _recordid="0"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-content table-responsive">
                                <table id='tableData' class="table table-striped table-bordered display">
                                    <thead class="header bg-primary">
                                        <tr class="">
                                            <th>ID</th>
                                            <th>Location</th>
                                            <th>LocationCode</th>
                                            <th>FK_CityID</th>
                                            <th>CityName</th>
                                            <th>Accounts_Receivable</th>
                                            <th>Sales_Credit</th>
                                            <th>Sales_Tax</th>
                                            <th>Sales_Return</th>
                                            <th>Stock</th>
                                            <th>CostofGoodSold</th>
                                            <th>Purchase</th>
                                            <th>LocalPurchase</th>
                                            <th>Discount</th>
                                            <th>Charges</th>
                                            <th>StockInTransit</th>
                                            <th>SalesReturn</th>
                                            <th>ImportCost</th>
                                            <th>ImportCostDr</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <%--</form> --%>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalAdd" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body" id="locationForm">

                    <input type="hidden" id="txtID" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="ValidationSummary" class="new badge"></span>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label"></label>
                                <input type="text" class="form-control locCode" id="txtLocationCode" name="txtLocationCode" placeholder="" />
                                <span id="rLocationCode" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label"></label>
                                <select id="ddlCity" class="form-control city"></select>
                                <span id="rCity" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group label-floating">
                                <label class="control-label"></label>
                                <input type="text" class="form-control" id="txtLocation" name="txtLocation" placeholder="" />
                                <span id="rLocation" class="new hide"></span>
                                <span id="" class="hide"></span>
                                <span id="" class="hide requiredValidation"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div style="height: 300px; overflow: auto; overflow-x: hidden;">
                                <table id='tblAccount' class="table table-striped table-bordered display" style="width: 100%">
                                    <thead class="header bg-primary">
                                        <tr class="">
                                            <th>ID</th>
                                            <th>Title</th>
                                            <th>Code</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <%--  <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">Accounts Receivable</label>
                                <input type="text" class="form-control" id="txtAccount" name="txtAccount" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnSearch()"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">Sales Credit</label>
                                <input type="text" class="form-control" id="Text1" name="txtAccount" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnSearch()"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">Sales Tax</label>
                                <input type="text" class="form-control" id="Text2" name="txtAccount" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnSearch()"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">Sales Return</label>
                                <input type="text" class="form-control" id="Text3" name="txtAccount" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnSearch()"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">Stock</label>
                                <input type="text" class="form-control" id="Text4" name="txtAccount" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnSearch()"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">Cost of Good Sold</label>
                                <input type="text" class="form-control" id="Text5" name="txtAccount" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnSearch()"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">Purchase</label>
                                <input type="text" class="form-control" id="Text6" name="txtAccount" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnSearch()"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">Local Purchase</label>
                                <input type="text" class="form-control" id="Text7" name="txtAccount" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnSearch()"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">Discount</label>
                                <input type="text" class="form-control" id="Text8" name="txtAccount" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnSearch()"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">Charges</label>
                                <input type="text" class="form-control" id="Text9" name="txtAccount" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnSearch()"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">Stock in Transit</label>
                                <input type="text" class="form-control" id="Text10" name="txtAccount" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnSearch()"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">Sales Return</label>
                                <input type="text" class="form-control" id="Text11" name="txtAccount" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnSearch()"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">Import Cost (Cr)</label>
                                <input type="text" class="form-control" id="Text12" name="txtAccount" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnSearch()"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group label-floating">
                                <label class="control-label">Import Cost (Dr)</label>
                                <input type="text" class="form-control" id="Text13" name="txtAccount" placeholder="" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary pull-right" onclick="OnSearch()"><i class="fa fa-search"></i></button>
                        </div>
                    </div>--%>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnSave()">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDelete()">Confirm</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalView2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="OnClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H4"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-content table-responsive">
                                <table id='tblItemList' class="table table-striped table-bordered display" style="width: 100%">
                                    <thead class="header bg-primary">
                                        <tr class="">
                                            <th>Select</th>
                                            <th>A/C Code</th>
                                            <th>A/C Name</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $("#locationForm").on("focusout", ".locCode", function () {
                var code = $(this).val().length
                if (code < 3) {
                    alert("Location code can not be less then or greater then 3 digits")
                }
            })
        })
        function bindAddEditLocation() {
            $("body").on("click", "#btnAdd , .edit", function () {
                debugger
                let id = $(this).attr("_recordId")
                clearAllMessages("locationForm")

                var service_DeveloperMode = $("#hdnDevMode").val()
                if (service_DeveloperMode == 'on') {
                    addFormFields("Location", "locationForm")
                }
                //var service_ToolTip = $("#hdnToolTip").val()
                //if (service_ToolTip == 'on') {

                setToolTip("Location", "locationForm", service_DeveloperMode)

                //}

                $.ajax({
                    type: "POST",
                    url: "" + serviceurl + "/AddEditLocation",
                    data: JSON.stringify({ id: id }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        if (response.d == "sessionNotFound") {
                            alert("sessionNotFound");
                            window.location.href = loginUrl;
                        } else {
                            var data = JSON.parse(response.d);
                            console.log(data);
                            if (!data.Status) {
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text(data.Message);
                                $("#Validation").fadeOut(5000);
                            } else {
                                if (parseInt(id) > 0) {
                                    debugger
                                    $("#formModalLabel").text("Edit");
                                    $(".label-floating").addClass("is-focused");
                                
                                    $(_ID).val(data.GenericList.ID);
                                    $(_LocationCode).val(data.GenericList.LocationCode);
                                    $(_Location).val(data.GenericList.Location);
                                    $(_City).val(data.GenericList.FK_CityID);

                                    var tab1 = $(tblAccount).DataTable();
                                    var c1 = tab1.data().count();

                                    var Code = [];
                                    var mydata = JSON.parse(dataa)
                                    for (var i = 0; i < mydata.length ; i++) {
                                        if (mydata[i].Title == "Accounts Receivable") {
                                            getAccountName(mydata[i].ID, data.GenericList.Accounts_Receivable)
                                            $("#txtCode" + mydata[i].ID).text(data.GenericList.Accounts_Receivable)
                                        }
                                        else if (mydata[i].Title == "Sales Credit") {
                                            getAccountName(mydata[i].ID, data.GenericList.Sales_Credit)
                                            $("#txtCode" + mydata[i].ID).text(data.GenericList.Sales_Credit)
                                        }
                                        else if (mydata[i].Title == "Sales Tax") {
                                            getAccountName(mydata[i].ID, data.GenericList.Sales_Tax)
                                            $("#txtCode" + mydata[i].ID).text(data.GenericList.Sales_Tax)
                                        }
                                        else if (mydata[i].Title == "Sales Return") {
                                            getAccountName(mydata[i].ID, data.GenericList.Sales_Return)
                                            $("#txtCode" + mydata[i].ID).text(data.GenericList.Sales_Return)
                                        }
                                        else if (mydata[i].Title == "Stock") {
                                            getAccountName(mydata[i].ID, data.GenericList.Stock)
                                            $("#txtCode" + mydata[i].ID).text(data.GenericList.Stock)
                                        }
                                        else if (mydata[i].Title == "Cost of Good Sold") {
                                            getAccountName(mydata[i].ID, data.GenericList.CostofGoodSold)
                                            $("#txtCode" + mydata[i].ID).text(data.GenericList.CostofGoodSold)
                                        }
                                        else if (mydata[i].Title == "Purchase") {
                                            getAccountName(mydata[i].ID, data.GenericList.Purchase)
                                            $("#txtCode" + mydata[i].ID).text(data.GenericList.Purchase)
                                        }
                                        else if (mydata[i].Title == "Local Purchase") {
                                            getAccountName(mydata[i].ID, data.GenericList.LocalPurchase)
                                            $("#txtCode" + mydata[i].ID).text(data.GenericList.LocalPurchase)
                                        }
                                        else if (mydata[i].Title == "Discount") {
                                            getAccountName(mydata[i].ID, data.GenericList.Discount)
                                            $("#txtCode" + mydata[i].ID).text(data.GenericList.Discount)
                                        }
                                        else if (mydata[i].Title == "Charges") {
                                            getAccountName(mydata[i].ID, data.GenericList.Charges)
                                            $("#txtCode" + mydata[i].ID).text(data.GenericList.Charges)
                                        }
                                        else if (mydata[i].Title == "Stock in Transit") {
                                            getAccountName(mydata[i].ID, data.GenericList.StockInTransit)
                                            $("#txtCode" + mydata[i].ID).text(data.GenericList.StockInTransit)
                                        }
                                        else if (mydata[i].Title == "Sale Return") {
                                            getAccountName(mydata[i].ID, data.GenericList.SalesReturn)
                                            $("#txtCode" + mydata[i].ID).text(data.GenericList.SalesReturn)
                                        }
                                        else if (mydata[i].Title == "Import Cost (Cr)") {
                                            getAccountName(mydata[i].ID, data.GenericList.ImportCost)
                                            $("#txtCode" + mydata[i].ID).text(data.GenericList.ImportCost)
                                        }
                                        else if (mydata[i].Title == "Import Cost (Dr)") {
                                            getAccountName(mydata[i].ID, data.GenericList.ImportCostDr)
                                            $("#txtCode" + mydata[i].ID).text(data.GenericList.ImportCostDr)
                                        }

                                    }
                                } else {
                                    OnLoadGridAccount();
                                    ClearFields();
                                    $("#formModalLabel").text("Add");
                                    $(".label-floating").removeClass("is-focused");
                                }
                          $("#modalAdd").modal();

                            }

                        }
                    },
                    error: OnErrorCall,
                    complete: function () {
                        //isValueExist(_SegmentCode, $(_SegmentCode).val())
                        //isValueExist(_SegmentName, $(_SegmentName).val())
                    }
                });
            })
        }

      
    </script>
</asp:Content>
