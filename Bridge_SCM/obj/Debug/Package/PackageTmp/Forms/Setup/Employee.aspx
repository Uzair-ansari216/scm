﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="Employee.aspx.vb" Inherits="Bridge_SCM.Employee" %>

<%@ Import Namespace="System.Web.Optimization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundles/DataTable/css")%>
    <%: Scripts.Render("~/bundles/DataTable/js")%>
    <%: Scripts.Render("~/bundles/EmployeeScript")%>


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header card-header-text">
                    <h4 class="card-title" style="color: #000000">Employee</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-primary pull-right" id="btnAdd" _recordId="0" title="Add New Employee"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-content table-responsive">
                                <table id='tableData' class="table table-striped table-bordered display">
                                    <thead class="header bg-primary">
                                        <tr class="">
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Gender</th>
                                            <th>DOB</th>
                                            <th>Address</th>
                                            <th>Phone</th>
                                            <th>Fax</th>
                                            <th>cityID</th>
                                            <th>ContactReference1</th>
                                            <th>ContactReference2</th>
                                            <th>Joining Date</th>
                                            <th>EndDate</th>
                                            <th>StatusID</th>
                                            <th>Nationality</th>
                                            <th>NIC</th>
                                            <th>CountriesTravelled</th>
                                            <th>LanguagesSpoken</th>
                                            <th>LanguagesWritten</th>
                                            <th>MaritalStatus</th>
                                            <th>PassportNo</th>
                                            <th>LocationID</th>
                                            <th>DesignationID</th>
                                            <th>DepartmentID</th>
                                            <th>Cell No</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <%--</form> --%>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalAdd" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="formModalLabel"></h4>
                </div>
                <div class="modal-body" id="employeeForm">

                    <%--<input type="hidden" id="txtID" value="" />--%>
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="ValidationSummary" class="new badge"></span>
                        </div>
                    </div>

                    <%-- Form Wizard --%>
                    <div id="formwizard_simple" class="form-wizard form-wizard-horizontal">
                        <form class="form floating-label">
                            <div class="form-wizard-nav">
                                <div class="progress" style="width: 50%;">
                                    <div class="progress-bar progress-bar-primary" style="width: 0%;"></div>
                                </div>
                                <ul class="nav nav-justified nav-pills">
                                    <li class="active"><a href="#fws_tab1" data-toggle="tab"><span class="step">1</span> <span class="title">Personal Information</span></a></li>
                                    <li><a href="#fws_tab2" data-toggle="tab"><span class="step">2</span> <span class="title">Contant Information</span></a></li>
                                    <li><a href="#fws_tab3" data-toggle="tab"><span class="step">3</span> <span class="title">Job Description</span></a></li>
                                </ul>
                            </div>
                            <!--end .form-wizard-nav -->

                            <div class="tab-content">
                                <div class="tab-pane active" id="fws_tab1">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Employee ID</label>
                                                <input type="text" class="form-control" id="txtID" name="txtID" placeholder="" />
                                                <span id="rID" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Name</label>
                                                <input type="text" class="form-control" id="txtName" name="txtName" placeholder="" />
                                                <span id="rName" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 checkbox-radios">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" id="male" value="Male" name="optionsRadios" checked="true" />
                                                    Male
                                                </label>
                                            </div>

                                        </div>
                                        <div class="col-sm-2 checkbox-radios">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" id="female" value="Female" name="optionsRadios" />
                                                    Female
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Date of Birth</label>
                                                <input type="text" class="form-control datepicker" id="txtDOB" name="txtDOB" placeholder="" />
                                                <span id="rDOB" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Status</label>
                                                <select id="ddlStatus" class="form-control"></select>
                                                <span id="rStatus" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Marital Status</label>
                                                <select id="ddlMaritial" class="form-control"></select>
                                                <span id="rMaterial" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label">NIC No</label>
                                                <input type="text" class="form-control" id="txtNIC" name="txtNIC" placeholder="" />
                                                <span id="rNIC" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Passport No</label>
                                                <input type="text" class="form-control" id="txtPassport" name="txtPassport" placeholder="" />
                                                <span id="rPassport" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Languages Spoken (Comma Delimeted)</label>
                                                <input type="text" class="form-control" id="txtSpoken" name="txtSpoken" placeholder="" />
                                                <span id="rSpoken" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Languages Written (Comma Delimeted)</label>
                                                <input type="text" class="form-control" id="txtWritten" name="txtWritten" placeholder="" />
                                                <span id="rWritten" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Countries Travelled (Comma)</label>
                                                <input type="text" class="form-control" id="txtCountriesTravelled" name="txtCountriesTravelled" placeholder="" />
                                                <span id="rCountriesTravelled" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Contact Reference 1</label>
                                                <input type="text" class="form-control" id="txtContactPerson1" name="txtContactPerson1" placeholder="" />
                                                <span id="rContactPerson1" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Contact Reference 2</label>
                                                <input type="text" class="form-control" id="txtContactPerson2" name="txtContactPerson2" placeholder="" />
                                                <span id="rContactPerson2" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!--end #tab1 -->

                                <div class="tab-pane" id="fws_tab2">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Address</label>
                                                <input type="text" class="form-control" id="txtAddress" name="txtAddress" placeholder="" />
                                                <span id="rAddress" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label">City</label>
                                                <select id="ddlCity" class="form-control"></select>
                                                <span id="rCity" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Cell No</label>
                                                <input type="text" class="form-control" id="txtCell" name="txtCell" placeholder="" />
                                                <span id="rCell" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Phone No</label>
                                                <input type="text" class="form-control" id="txtPhone" name="txtPhone" placeholder="" />
                                                <span id="rPhone" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Nationality</label>
                                                <input type="text" class="form-control" id="txtNationality" name="txtNationality" placeholder="" />
                                                <span id="rNationality" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end #tab2 -->

                                <div class="tab-pane" id="fws_tab3">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Location</label>
                                                <select id="ddlLocation" class="form-control"></select>
                                                <span id="rLocation" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>

                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Department</label>
                                                <select id="ddlDepartment" class="form-control"></select>
                                                <span id="rDepartment" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Designation</label>
                                                <select id="ddlDesignation" class="form-control"></select>
                                                <span id="rDesignation" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Joining Date</label>
                                                <input type="text" class="form-control datepicker" id="txtJoin" name="txtJoin" placeholder="" />
                                                <span id="rJoin" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">End Date</label>
                                                <input type="text" class="form-control datepicker" id="txtEnd" name="txtEnd" placeholder="" />
                                                <span id="rEnd" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Fax</label>
                                                <input type="text" class="form-control" id="txtFax" name="txtFax" placeholder="" />
                                                <span id="rFax" class="new hide"></span>
                                                <span id="" class="hide"></span>
                                                <span id="" class="hide requiredValidation"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end #tab3 -->

                                <ul class="pager wizard ">
                                    <li class="previous first disabled"><a class="btn-raised" href="javascript:void(0);">First</a></li>
                                    <li class="previous disabled"><a class="btn-raised" href="javascript:void(0);">Previous</a></li>
                                    <li class="next last"><a class="btn-raised" href="javascript:void(0);">Last</a></li>
                                    <li class="next"><a class="btn-raised" href="javascript:void(0);">Next</a></li>
                                </ul>
                            </div>
                            <!--end .tab-content -->


                        </form>
                    </div>
                    <%-- Form Wizard end --%>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-fill btn-default" onclick="OnSave()">Save changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Delete</h4>
                </div>
                <div class="modal-body">

                    <h5><i class="fa fa-warning  text-danger"></i>Do you want to delete this record??</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-fill btn-default" onclick="OnConfirmDelete()">Confirm</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
