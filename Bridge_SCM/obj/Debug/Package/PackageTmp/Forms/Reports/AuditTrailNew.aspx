﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AuditTrailNew.aspx.vb" Inherits="Bridge_SCM.AuditTrailNew" MasterPageFile="~/Forms/MasterPages/Main.Master"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <link href="../../content/bootstrap.css" rel="stylesheet" />
    <link href="../../Js/JQGridReq/CustomCss.css" rel="stylesheet" />
    <link href="../../Js/jquery-ui.css" rel="stylesheet" />
    <link href="../../content/Checkbox/customcss.css" rel="stylesheet" />
    <link href="../../content/Checkbox/customradio.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Scripts/jquery-2.1.1.min.js"></script>
    <script src="../../Js/jquery-ui.js"></script>
    <script src="../../content/Checkbox/customjs.js"></script>

    <script src="../../Js/customMethods.js"></script>

    <link href='https://fonts.googleapis.com/css?family=Slabo+27px|Exo+2' rel='stylesheet' type='text/css'>

    <style type="text/css">
        body {
            font-family: 'Slabo 27px', serif;
        }
    </style>

<script type="text/javascript">

    var pagename = "AuditTrailNew.aspx";
    $(document).ready(function () {
        OnLoad();
    });

    function OnLoad() {

        $("#txtFromDate").datepicker();
        $("#txtToDate").datepicker();
        $("#txtCFromDate").datepicker();
        $("#txtCToDate").datepicker();

        $("#txtFromDate").val(GetToday());
        $("#txtToDate").val(GetToday());
        $("#txtCFromDate").val(GetToday());
        $("#txtCToDate").val(GetToday());

        //  ddlVocType   ddlVocStatus   ddlLocation   ddlSegment  ddlDepartment  ddlProject  ddlCreditor  ddlReports   ddlEmployee   ddlDebtor


        FillDropdown(pagename, "GetStatusList", "ddlVocStatus");




        FillDropdown(pagename, "GetLocationList", "ddlLocation");


        FillDropdown(pagename, "GetDepartmentList", "ddlDepartment");
        FillDropdown(pagename, "GetEmployeeList", "ddlEmployee");
        FillDropdown(pagename, "GeSegmentList", "ddlSegment");
        FillDropdown(pagename, "GeProjectList", "ddlProject");
        FillDropdown(pagename, "GeVoucherTypeList", "ddlVocType");
        FillDropdown(pagename, "GeReportList", "ddlReports");

    }

    function Save() {
       
        //  ddlVocType   ddlVocStatus   ddlLocation   ddlSegment  ddlDepartment  ddlProject  ddlCreditor  ddlReports   ddlEmployee   ddlDebtor

        var ddlVocType = $("#ddlVocType :selected").val();
        var ddlVocStatus = $("#ddlVocStatus :selected").val();
        var ddlLocation = $("#ddlLocation :selected").val();
        var ddlSegment = $("#ddlSegment :selected").val();
        var ddlDepartment = $("#ddlDepartment :selected").val();
        var ddlProject = $("#ddlProject :selected").val();
        var ddlCreditor = $("#ddlCreditor :selected").val();
        var ddlReports = $("#ddlReports :selected").val();
        var ddlEmployee = $("#ddlEmployee :selected").val();
        var ddlDebtor = $("#ddlDebtor :selected").val();

        var FromDate = $("#txtFromDate").val();
        var ToDate = $("#txtToDate").val();
        

        var  ChkSegment  = 0;
        var ChkDepartment = 0;
        var ChkProject = 0;
        var ChkEmployee = 0;
        var ChkVoctype = 0;
        var ChkDebtor = 0;

        if ($("#ChkSegment").is(':checked')) {
            ChkSegment = 1;
        }
        else {
            ChkSegment = 0;
        }

        if ($("#ChkDepartment").is(':checked')) {
            ChkDepartment = 1;
        }
        else {
            ChkDepartment = 0;
        }

        if ($("#ChkProject").is(':checked')) {
            ChkProject = 1;
        }
        else {
            ChkProject = 0;
        }

        if ($("#ChkEmployee").is(':checked')) {
            ChkEmployee = 1;
        }
        else {
            ChkEmployee = 0;
        }

        if ($("#ChkVoctype").is(':checked')) {
            ChkVoctype = 1;
        }
        else {
            ChkVoctype = 0;
        }

        if ($("#ChkDebtor").is(':checked')) {
            ChkDebtor = 1;
        }
        else {
            ChkDebtor = 0;
        }


        //  ddlVocType   ddlVocStatus   ddlLocation   ddlSegment  ddlDepartment  ddlProject 
       // ddlCreditor  ddlReports   ddlEmployee   ddlDebtor
        //var ChkSegment = 0;
        //var ChkDepartment = 0;
        //var ChkProject = 0;
        //var ChkEmployee = 0;
        //var ChkVoctype = 0;
        //var ChkDebtor = 0;

        if (ddlReports != "-1") {

            $.ajax({
                type: "POST",
                url: '' + pagename + '/Save?ddlVocType=' + ddlVocType + '&ddlVocStatus=' + ddlVocStatus + '&ddlLocation=' + ddlLocation +
                     '&ddlSegment=' + ddlSegment + '&ddlDepartment=' + ddlDepartment + '&ddlProject=' + ddlProject +
                     '&ddlCreditor=' + ddlCreditor + '&ddlReports=' + ddlReports + '&ddlEmployee=' + ddlEmployee + '&ddlDebtor=' + ddlDebtor +
                     '&ChkSegment=' + ChkSegment + '&ChkDepartment=' + ChkDepartment + '&ChkProject=' + ChkProject + '&ChkEmployee=' + ChkEmployee +
                     '&ChkVoctype=' + ChkVoctype + '&ChkDebtor=' + ChkDebtor + '&FromDate=' + FromDate + '&ToDate=' + ToDate,
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    if (res.d == "1") {
                        //alert("1");
                        window.open("../Ctrls/ReportViewer.aspx")
                    }
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });

        } else {
            alert("Please Select Report");
        }
        
    }
</script>


     <div class="panel panel-default">
        <div class="panel-heading">
            <h5>Audit Traill </h5>
        </div>
              <div class="panel-body">

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-12 bg-primary">
                            <span>Voucher Period</span>
                        </div>
                    </div>


                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-2 text-right">
                            <span class="cr">From Date</span>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group date">
                                <input type="text" id="txtFromDate" class="form-control" />
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>

                        <div class="col-sm-2 text-right">
                            <span class="cr">To Date</span>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group date">
                                <input type="text" id="txtToDate" class="form-control" />
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-calendar"></i></span>
                            </div>

                        </div>

                    </div>
                </div>
    </div>

            <div class="panel panel-info">
                <div class="panel-body">
                    <%--<h3 class="panel-title">Voucher Creation Period:</h3>--%>


                    <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-12 bg-info">
                                    <span>Voucher Creation Period</span>
                                </div>
                            </div>

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-2 text-right">
                            <span class="cr">From Date</span>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group date">
                                <input type="text" id="txtCFromDate" class="form-control" />
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>

                        <div class="col-sm-2 text-right">
                            <span class="cr">To Date</span>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group date">
                                <input type="text" id="txtCToDate" class="form-control" />
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-calendar"></i></span>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="panel panel-success">
                <div class="panel-body">
                   <%-- <h3 class="panel-title">Options:</h3>--%>


                  
                       <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-12 bg-success">
                                    <span>Options</span>
                                </div>
                            </div>

                
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-2 text-right">
                             <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Voc-Type</button>
                                <input type="checkbox" id="ChkVoctype" />
                            </span>

                        </div>

                        <div class="col-sm-3">
                            <select id="ddlVocType" class="select" style="width: 100%"></select>
                        </div>

                        <div class="col-sm-2 text-right">
                             <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Voc-Status</button>
                                <input type="checkbox" id="Checkbox1" />
                            </span>
                         
                        </div>

                        <div class="col-sm-3">
                            <select id="ddlVocStatus" class="select" style="width: 100%"></select>
                        </div>

                    </div>

                    <div class="row" style="margin-top: 10px;">
                       
                        <div class="col-sm-2 text-right">
                             <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Location</button>
                                <input type="checkbox" id="Checkbox2"/>
                            </span>
                        </div>

                        <div class="col-sm-3">
                            <select id="ddlLocation" class="select" style="width: 100%"></select>
                        </div>

                       
                        <div class="col-sm-2 text-right">
                            <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Segment</button>
                                <input type="checkbox" id="ChkSegment"  />
                            </span>
                        
                        </div>

                        <div class="col-sm-3">
                            <select id="ddlSegment" class="select" style="width: 100%"></select>
                        </div>

                    </div>

                    <div class="row" style="margin-top: 10px;">
                       
                        <div class="col-sm-2 text-right">
                             <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Department</button>
                                <input type="checkbox" id="ChkDepartment" />
                            </span>
                           
                        </div>

                        <div class="col-sm-3">
                            <select id="ddlDepartment" class="select" style="width: 100%"></select>
                        </div>

                        <div class="col-sm-2 text-right">
                             <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Project</button>
                                <input type="checkbox" id="ChkProject" />
                            </span>
                           
                        </div>

                        <div class="col-sm-3">
                            <select id="ddlProject" class="select" style="width: 100%"></select>
                        </div>

                    </div>

                    <div class="row" style="margin-top: 10px;">
                      
                        <div class="col-sm-2 text-right">
                             <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Employee</button>
                                <input type="checkbox" id="ChkEmployee"/>
                            </span>
                     
                        </div>

                        <div class="col-sm-3">
                            <select id="ddlEmployee" class="select" style="width: 100%"></select>
                        </div>

                     
                        <div class="col-sm-2 text-right">
                              <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Debtor</button>
                                <input type="checkbox" id="ChkDebtor"/>
                            </span>
                           
                        </div>

                        <div class="col-sm-3">
                            <select id="ddlDebtor" class="select" style="width: 100%"></select>
                        </div>

                    </div>

                    <div class="row" style="margin-top: 10px;">
                      
                        <div class="col-sm-2 text-right">
                             <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Creditor</button>
                                <input type="checkbox" id="Checkbox3"/>
                            </span>
                           
                        </div>

                        <div class="col-sm-3">
                            <select id="ddlCreditor" class="select" style="width: 100%"></select>
                        </div>

                        <div class="col-sm-1" style="padding:0;">
                            <span>Voucher #</span>
                        </div>

                        <div class="col-sm-2">
                            <input type="text" id="txtVoucherNo" class="form-control" />
                        </div>

                        <div class="col-sm-1" style="padding:0;">
                            <span>Voucher To</span>
                        </div>

                        <div class="col-sm-2">
                            <input type="text" id="txtVoucherTo" class="form-control" />
                        </div>



                    </div>

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-2 text-right">
                            <span class="">Reports</span>
                        </div>

                        <div class="col-sm-3">
                            <select id="ddlReports" class="select" style="width: 100%"></select>
                        </div>

                    </div>

                     <div class="row" style="margin-top: 10px;" >
                        <div class="col-sm-12">
                            <div class="pull-right">
                            <button type="button" class="btn btn-primary btn-md" onclick="Save()">
                                <span class="glyphicon glyphicon-search"></span>Show
                            </button>
                                </div>
                        </div>
                    </div>
                   
                </div>
            </div>

        </div>
    </div>

</asp:Content>