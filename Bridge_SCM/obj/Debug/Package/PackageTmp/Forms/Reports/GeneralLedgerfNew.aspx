﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GeneralLedgerfNew.aspx.vb" Inherits="Bridge_SCM.GeneralLedgerfNew" MasterPageFile="~/Forms/MasterPages/Main.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

     <%: Scripts.Render("~/bundles/CustomeMethodScript")%>

    <script type="text/javascript">

        var pagename = "GeneralLedgerfNew.aspx";
        var serviceUrl = "../../appServices/GeneralLedger.asmx"
        $(document).ready(function () {

            $("#txtFromDate").datepicker();
            $("#txtToDate").datepicker();
            //$(".select").select2({ placeholder: "Please Select", });

            OnLoad();
            OnChange();
        });


        function FillDropdown(pagemane, urlname, ddlname) {
            debugger
            $.ajax({
                type: "POST",
                url: "" + serviceUrl + "/" + urlname, //'' + pagename + '/' + urlname ,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {
                    $("#" + ddlname + "").empty();
                    $("#" + ddlname + "").append("<option value='-1'>--Please Select--</option>");
                    jQuery.each(JSON.parse(res.d), function (index, item) {
                        $("." + ddlname + "").append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });
                    $("." + ddlname + " option:selected").text("-- Please Select --").trigger("change")

                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });

        }

        function FillVoucherStatus() {
            $("#ddlVocStatus").empty();
            $("#ddlVocStatus").append("<option value='-1'>--Please Select--</option>");
            $("#ddlVocStatus").append("<option value='1'>Saved Vouche</option>");
            $("#ddlVocStatus").append("<option value='2'>Approved Voucher</option>");
            $("#ddlVocStatus").append("<option value='3'>Posted Voucher</option>");
        }

        function OnLoad() {

            $("#txtFromDate").val(GetToday());
            $("#txtToDate").val(GetToday());

            //ddlAccountType   ddlVocStatus  ddlLocation ddlDepartment  ddlProject   ddlEmployee  
            //ddlDebtor  ddlCreditor  ddlReports 
            FillVoucherStatus();
            FillDropdown(pagename, "GetCustomerList", "ddlDebtor");
            FillDropdown(pagename, "GetProjectList", "ddlProject");
            FillDropdown(pagename, "GetLocationList", "ddlLocation");
            FillDropdown(pagename, "GetDepartmentList", "ddlDepartment");
            FillDropdown(pagename, "GetEmployeeList", "ddlEmployee");
            FillDropdown(pagename, "GetSegmentList", "ddlSegment");
            FillDropdown(pagename, "GetvoucherTypeList", "ddlAccountType");
            FillDropdown(pagename, "GetDetailAccountList", "ddlDetailAccountAC");
            FillDropdown(pagename, "GetReportList", "ddlReports");

            var id = "6";
            FillDropdownByID(pagename, "GetAccountCodetList", "ddlAccountCode", id);

        }

        function OnChange() {
            $("#ddlDetailAccountAC").on('change', function () {
                //var id = $("#ddlDetailAccountAC :selected").val();
                //FillDropdownByID("GetAccountCodetList", "ddlAccountCode", id);
                //$(".select").select2("val", "");
            });

        }

        function Save() {
            var data = $(".select").select2('data');
            var IDS = [];

            jQuery.each(data, function (index, item) {
                IDS.push(item.id);
            });

            var txtFromDate = $("#txtFromDate").val();
            var txtToDate = $("#txtToDate").val();

            var chkVocType = $("#chkVocType").val();
            var chkVocStatus = $("#chkVocStatus").val();
            var chkLocation = $("#chkLocation").val();
            var chkSegment = $("#chkSegment").val();
            var chkDepartment = $("#chkDepartment").val();
            var chkProject = $("#chkProject").val();
            var chkEmployee = $("#chkEmployee").val();
            var chkDebtor = $("#chkDebtor").val();
            var chkCreditor = $("#chkCreditor").val();


            var cVocType = 0;
            var cVocStatus = 0;
            var cLocation = 0;
            var cSegment = 0;
            var cDepartment = 0;
            var cProject = 0;
            var cEmployee = 0;
            var cDebtor = 0;
            var cCreditor = 0;

            if ($("#chkVocType").is(":Checked")) {
                //alert("1");
                cVocType = 1;
            } else { cVocType = 0; alert("0"); }

            if ($("#chkVocStatus").is(":Checked")) {
                cVocStatus = 1;
            } else { cVocStatus = 0; }

            if ($("#chkLocation").is(":Checked")) {
                cLocation = 1;
            } else { cLocation = 0; }

            if ($("#chkSegment").is(":Checked")) {
                cSegment = 1;
            } else { cSegment = 0; }

            if ($("#chkDepartment").is(":Checked")) {
                cDepartment = 1;
            } else { cDepartment = 0; }

            if ($("#chkProject").is(":Checked")) {
                cProject = 1;
            } else { cProject = 0; }

            if ($("#chkEmployee").is(":Checked")) {
                cEmployee = 1;
            } else { cEmployee = 0; }

            if ($("#chkDebtor").is(":Checked")) {
                cDebtor = 1;
            } else { cDebtor = 0; }

            if ($("#chkCreditor").is(":Checked")) {
                cCreditor = 1;
            } else { cCreditor = 0; }

            var radAccountAC = $("#radAccountAC").val();
            var radAccountAC = $("#radAccountAC").val();

            var ddlAccountType = $("#ddlAccountType :selected").val();
            var ddlVocStatus = $("#ddlVocStatus :selected").val();
            var ddlLocation = $("#ddlLocation :selected").val();
            var ddlSegment = $("#ddlSegment :selected").val();
            var ddlDepartment = $("#ddlDepartment :selected").val();
            var ddlProject = $("#ddlProject :selected").val();
            var ddlEmployee = $("#ddlEmployee :selected").val();
            var ddlDebtor = $("#ddlDebtor :selected").val();
            var ddlCreditor = $("#ddlCreditor :selected").val();

            var ddlReports = $("#ddlReports :selected").val();
            var ddlDetailAccountAC = $("#ddlDetailAccountAC :selected").val();

            urlname = "Save";
            $.ajax({
                type: "POST",
                url: '' + pagename + '/' + urlname + '?id=' + IDS + '&txtFromDate=' + txtFromDate + '&txtToDate=' + txtToDate +
                    '&chkVocType=' + cVocType + '&chkVocStatus=' + cVocStatus + '&chkLocation=' + cLocation +
                    '&chkSegment=' + cSegment + '&chkDepartment=' + cDepartment + '&chkProject=' + cProject +
                    '&chkEmployee=' + cEmployee + '&chkDebtor=' + cDebtor + '&chkCreditor=' + cCreditor +
                    '&radAccountAC=' + radAccountAC + '&ddlAccountType=' + ddlAccountType + '&ddlVocStatus=' + ddlVocStatus +
                    '&ddlLocation=' + ddlLocation + '&ddlSegment=' + ddlSegment + '&ddlDepartment=' + ddlDepartment +
                    '&ddlProject=' + ddlProject + '&ddlEmployee=' + ddlEmployee + '&ddlDebtor=' + ddlDebtor +
                    '&ddlCreditor=' + ddlCreditor + '&ddlReports=' + ddlReports + '&ddlDetailAccountAC=' + ddlDetailAccountAC,
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    var data = res.d;
                    if (data == "1") {
                        window.open("../Ctrls/ReportViewer.aspx");
                        $("#FormError").css('display', 'none');
                        $("#Error").empty();

                    } else {
                        $("#FormError").css('display', '');
                        $("#Error").empty();
                        $("#Error").text(data);
                    }
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        }


        function selectAll(selectBox, selectAll) {
            // have we been passed an ID 
            if (typeof selectBox == "string") {
                selectBox = document.getElementById(selectBox);
            }
            // is the select box a multiple select box? 
            if (selectBox.type == "select-multiple") {
                for (var i = 0; i < selectBox.options.length; i++) {
                    selectBox.options[i].selected = selectAll;
                }
            }
        }


    </script>


    <%-- main Content --%>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <%--<form method="get" action="/" class="form-horizontal">--%>
                <div class="card-header card-header-text">
                    <h4 class="card-title" style="color: #000000">General Ledger</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-sm-9">
                            <span id="Validation" class="new badge"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-9">
                            <h4 class="modal-title" id="SalesOrder"></h4>
                        </div>

                        <%-- <div class="col-sm-3">
                            <button type="button" class="btn btn-primary pull-right" onclick="AddNew()"><i class="fa fa-refresh"></i></button>

                            <button type="button" class="btn btn-primary pull-right" onclick="Show()"><i class="fa fa-eye"></i></button>
                        </div--%>
                    </div>
                    <input type="hidden" id="txtID" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <span id="ValidationSummary" class="new badge"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-12">
                                    <span id="" class="new badge">Period</span>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 3em;">
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">From Date</label>
                                        <input type="text" class="form-control" id="txtFromDate" name="txtDate" placeholder="" />
                                        <span id="rFromDate" class="new badge  hide">required</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">To Date</label>
                                        <input type="text" class="form-control" id="txtToDate" name="txtDate" placeholder="" />
                                        <span id="rToDate" class="new badge  hide">required</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-12">
                                    <span id="" class="new badge">Accounts</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 checkbox-radios">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" id="male" value="Male" name="optionsRadios" checked="true" />
                                            Detail Account List
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6 checkbox-radios">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" id="female" value="Female" name="optionsRadios" />
                                            Control Account List
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label"></label>
                                        <asp:DropDownList ID="ddlDetailAccountAC" class="form-control ddlDetailAccountAC select" runat="server" _type="">
                                        </asp:DropDownList>
                                        <span id="rddlDetailAccountAC" class="new badge  hide">required</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Sample</label>
                                        <input type="text" class="form-control" id="txtFromBillTo" name="txtFromBillTo" placeholder="" />
                                        <span id="rFromBillTo" class="new badge"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <span id="" class="new badge">Options</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-2 checkbox-radios">
                                            <div class="checkbox">
                                                <label>
                                                    <input id="chkVocType" type="checkbox" name="chkVocType" />
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Voc-Type</label>
                                                <asp:DropDownList ID="ddlAccountType" class="form-control ddlAccountType select" runat="server" _type=""></asp:DropDownList>
                                                <span id="rddlAccountType" class="new badge  hide">required</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-2 checkbox-radios">
                                            <div class="checkbox">
                                                <label>
                                                    <input id="chkVocStatus" type="checkbox" name="chkVocStatus" />
                                                </label>

                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Voc-Status</label>
                                                <asp:DropDownList ID="ddlVocStatus" class="form-control ddlVocStatus select" runat="server" _type=""></asp:DropDownList>
                                                <span id="rddlVocStatus" class="new badge  hide">required</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-2 checkbox-radios">
                                            <div class="checkbox">
                                                <label>
                                                    <input id="chkLocation" type="checkbox" name="chkLocation" />
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Location</label>
                                                <asp:DropDownList ID="ddlLocation" class="form-control ddlLocation select" runat="server" _type=""></asp:DropDownList>
                                                <span id="rddlLocation" class="new badge  hide">required</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-2 checkbox-radios">
                                            <div class="checkbox">
                                                <label>
                                                    <input id="chkSegment" type="checkbox" name="chkSegment" />
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Segments</label>
                                                <asp:DropDownList ID="DropDownList1" class="form-control ddlSegment select" runat="server" _type=""></asp:DropDownList>
                                                <span id="rddlSegment" class="new badge  hide">required</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-2 checkbox-radios">
                                            <div class="checkbox">
                                                <label>
                                                    <input id="chkDepartment" type="checkbox" name="chkDepartment" />
                                                </label>

                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Department</label>
                                                <asp:DropDownList ID="DropDownList2" class="form-control ddlDepartment select" runat="server" _type=""></asp:DropDownList>
                                                <span id="rddlDepartment" class="new badge  hide">required</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-2 checkbox-radios">
                                            <div class="checkbox">
                                                <label>
                                                    <input id="chkProject" type="checkbox" name="chkProject" />
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Project</label>
                                                <asp:DropDownList ID="DropDownList3" class="form-control ddlProject select" runat="server" _type=""></asp:DropDownList>
                                                <span id="rddlProject" class="new badge  hide">required</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-2 checkbox-radios">
                                            <div class="checkbox">
                                                <label>
                                                    <input id="chkEmployee" type="checkbox" name="chkEmployee" />
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Employee</label>
                                                <asp:DropDownList ID="DropDownList4" class="form-control ddlEmployee select" runat="server" _type=""></asp:DropDownList>
                                                <span id="rddlEmployee" class="new badge  hide">required</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-2 checkbox-radios">
                                            <div class="checkbox">
                                                <label>
                                                    <input id="chkDebtor" type="checkbox" name="chkDebtor" />
                                                </label>

                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Debtor</label>
                                                <asp:DropDownList ID="DropDownList5" class="form-control ddlDebtor select" runat="server" _type=""></asp:DropDownList>
                                                <span id="rddlDebtor" class="new badge  hide">required</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Report</label>
                                                <asp:DropDownList ID="DropDownList6" class="form-control ddlReports select" runat="server" _type=""></asp:DropDownList>
                                                <span id="rddlReports" class="new badge  hide">required</span>
                                            </div>
                                        </div>
                                        </>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-3">
                            <button id="selectall" type="button" class="btn btn-primary " value="All" onclick="selectAll('ddlAccountCode',true)">
                                <span class="fa fa-check-square"></span>
                                Select All
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button id="unselectall" type="button" class="btn btn-primary" value="All" onclick="selectAll(document.getElementById('ddlAccountCode'),false)">
                                <span class="fa fa-stop"></span>
                                UnSelect All
                            </button>
                        </div>
                        <div class="col-sm-4"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8">
                            <div class="form-group label-floating">
                                <label class="control-label">A/C Code</label>
                                <asp:DropDownList ID="ddlAccountCode" class="form-control ddlAccountCode select" multiple="multiple" runat="server" _type=""></asp:DropDownList>
                                <span id="rddlAccountCode" class="new badge  hide">required</span>
                            </div>
                        </div>
                        <div class="col-sm-2"></div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-5"></div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary btn-md" onclick="Save()">
                                <span class="fa fa-search"></span> Show
                            </button>
                        </div>
                        <div class="col-sm-5"></div>
                    </div>
                    <%--</form> --%>
                </div>
            </div>
        </div>
        <%-- Main content end --%>
</asp:Content>
