﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RPTChartOfAccountNew.aspx.vb" Inherits="Bridge_SCM.RPTChartOfAccountNew" MasterPageFile="~/Forms/MasterPages/Main.Master" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="../../content/bootstrap.css" rel="stylesheet" />
    <link href="../../Js/JQGridReq/CustomCss.css" rel="stylesheet" />
    <link href="../../content/Checkbox/customcss.css" rel="stylesheet" />
    <link href="../../content/Checkbox/customradio.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Scripts/jquery-2.1.1.min.js"></script>
    <script src="../../content/Checkbox/customjs.js"></script>

    <!-- APP OWN DEFINED METHODS -->
    <script src="../../Js/customMethods.js"></script>

    <link href='https://fonts.googleapis.com/css?family=Slabo+27px|Exo+2' rel='stylesheet' type='text/css'>

    <style type="text/css">
        body {
            font-family: 'Slabo 27px', serif;
        }
    </style>

    <script type="text/javascript">

        var pagename = "RPTChartOfAccountNew.aspx";

        $(document).ready(function () {
            OnLoad();
        });

        function OnLoad() {
            FillDropdown(pagename, "GetAccountGroupList", "ddlAcountGroup");
            FillDropdown(pagename, "GetAccountTypeList", "ddlAccountType");

        }

        function Save() {
            var chkTV = 0;
            if ($("#chkTV").is(':checked')) {
                alert('checked');
                chkTV = 1;
            }
            else {
                chkTV = 0;
            }

            $.ajax({
                type: "POST",
                url: '' + pagename + '/Save?chkTV=' + chkTV,
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    if (res.d == "1") {
                        window.open("../Ctrls/ReportViewer.aspx");
                    }
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });

        }

    </script>


   <div class="panel panel-default">
        <div class="panel-heading">
            <h5>Chart Of Account </h5>
        </div>
              <div class="panel-body">
              
                   
                    <div class="row" style="margin-top: 10px;">

                        <div class="col-sm-3 text-right">
                            <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Account Group</button>
                                <input type="checkbox" />
                            </span>
                        </div>
                        <div class="col-sm-3">
                            <select id="ddlAcountGroup" class="select" style="width: 100%"></select>
                        </div>

                        <div class="col-sm-3 text-left">
                            <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Pastable A/C</button>
                                <input type="checkbox"  />
                            </span>
                        </div>

                    </div>

                    <div class="row " style="margin-top: 10px;">

                        <div class="col-sm-3 text-right">
                            <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Account Type</button>
                                <input type="checkbox" />
                            </span>
                        </div>

                        <div class="col-sm-3">
                            <select id="ddlAccountType" class="select" style="width: 100%"></select>
                        </div>
                        <div class="col-sm-3 text-left">
                            <span class="button-checkbox">
                                <button type="button" class="" name="chkAccountLevel" data-color="primary" style="border: 0 #fff">Tree View</button>
                                <input type="checkbox" id="chkTV" />
                            </span>
                        </div>

                    </div>

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-12">
                            <div class="pull-right">
                                <button type="button" class="btn btn-primary btn-md" onclick="Save()">
                                    <span class="glyphicon glyphicon-search"></span>Show
                                </button>

                            </div>
                        </div>
                    </div>

                </div>
       
       </div> 
</asp:Content>



