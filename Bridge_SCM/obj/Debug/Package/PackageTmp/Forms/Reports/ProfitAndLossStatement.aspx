﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Forms/MasterPages/Main.Master" CodeBehind="ProfitAndLossStatement.aspx.vb" Inherits="Bridge_SCM.ProfitAndLossStatement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <script type="text/javascript" src="../../js/customMethods.js"></script>

    
    <script type="text/javascript" src="/Support/calendar.js"></script>


    <asp:HiddenField ID="hdnRecID" runat="server" Value="0" />

    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li><a href="ProfitAndLossStatement.aspx">ProfitAndLossStatement.aspx</a>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="../Admin/Home.aspx">Home</a>
            </li>

            <li>
                <a href="#">GeneralLedgerReports</a>
            </li>
            <li class="active">ProfitAndLossStatement</li>
        </ul>
        <!-- /.breadcrumb -->

        <div class="nav-search" id="nav-search">
            <form class="form-search">
                <span class="input-icon">
                    <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                    <i class="ace-icon fa fa-search nav-search-icon"></i>
                </span>
            </form>
        </div>
        <!-- /.nav-search -->
    </div>

    <div class="page-content">

        <div class="widget-box transparent">
            <div class="widget-header widget-header-large">
                <h3 class="widget-title grey lighter">Profit And Loss Statement
                </h3>
            </div>
        </div>


        <div id="divTable">
            <ul id="Ul1" class="item-list">
                <li class="item-orange clearfix">
                    <div id="forms_div">
                      
                        <asp:Label ID="lblMessage" runat="server" CssClass="Label-Error"></asp:Label>
                        <h5>Time Specification</h5>

                        <table id="Table1" runat="server"  class="table table-detail text-left">
                           
                           <tr>
                                <td>
                                    
                                     <asp:RadioButton ID="radio"  runat="server" />Yearly
                                    <asp:DropDownList ID="DropDownList1" CssClass="form-control" runat="server" style="width:30%;"></asp:DropDownList>
                                     
                                </td> 
                               <td>
                                    <asp:RadioButton ID="RadioButton2"  runat="server" />Monthly
                                    <asp:DropDownList ID="DropDownList3" CssClass="form-control" runat="server" style="width:30%;"></asp:DropDownList>
                                   
                               </td> 
                             
                               <td>Year
                                   <asp:DropDownList ID="DropDownList4"  CssClass="form-control" runat="server" style="width:40%;"></asp:DropDownList>
                               </td>
                               
                  
                            </tr>
                          <tr>
                              <td>
                                  <asp:RadioButton ID="RadioButton1"  runat="server" />Duration
                               
                              </td> 
                              <td>
                                    <asp:CheckBox ID="CheckBox2"  runat="server" />Financials
                              </td>
                              <td>
                                   <button id="btnNew" class="btn btn-primary" type="button" onclick="AddNew()">
                       
                        Exit
                    </button>
                              </td>
                             
                          </tr>
                            <tr>
                                 <td>From
                                   <asp:DropDownList ID="DropDownList2"  CssClass="form-control" runat="server" style="width:30%;"></asp:DropDownList>
                                  </td>
                                <td>To
                                   <asp:DropDownList ID="DropDownList5"  CssClass="form-control" runat="server" style="width:30%;"></asp:DropDownList>
                                  </td>
                            </tr>
                           
                        </table>
                       <div class="row pad">
            <div class="col-lg-12">
                <div id="FormMsg" class="pull-left"></div>
                <div id="divTopButtons" class="pull-left">
                    <button id="Button1" class="btn btn-primary" type="button" onclick="AddNew()" style="max-width: 62%; font-size: 11px; min-width: 33.6%;">
                        Map Sheet
                    </button>
                    <button id="btnSave" onclick="Save()" type="button" class="btn btn-primary"style="max-width: 62%; font-size: 11px; min-width: 33.6%;">
                        Notes
                    </button>
                    <button id="btnView" type="button" onclick="ViewList()" class="btn btn-primary"style="max-width: 62%; font-size: 11px; min-width: 30%;">
                        
                       Show
                    </button><br />
                      <button id="Button2" type="button" onclick="ViewList()" class="btn btn-primary" style="max-width: 62%; font-size: 11px; min-width: 25%;">
                        
                       PnlBreakUp
                    </button>
                         <button id="Button3" type="button" onclick="ViewList()" class="btn btn-primary" style="max-width: 62%; font-size: 11px; min-width: 25%;">
                        
                       BS BreakUp
                    </button>
                         <button id="Button4" type="button" onclick="ViewList()" class="btn btn-primary" style="max-width: 62%; font-size: 11px; min-width: 25%;">
                        
                       Financials
                    </button>
                  
                </div>
            </div>
        </div>
                         
                        


                       
</div>
                               
                </li>
            </ul>
        </div>
         <div id="divTable2">
            <ul id="Ul2" class="item-list">
                <li class="item-orange clearfix">
                    <div id="forms_div2">
                      <h5>Location</h5>

                        <asp:Label ID="Label1" runat="server" CssClass="Label-Error"></asp:Label>
                        <table id="Table2" runat="server"  class="table table-detail text-left">
                        <tr>
                            <td>Location
                                 <asp:DropDownList ID="DropDownList6" CssClass="form-control" style="width:48%;" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                            </table>
                        </div>
                    </li>
                </ul>
             </div>
        
    </div>
   
      <style>
       btn {
     color: #FFF!important; 
     text-shadow: 0 -1px 0 rgba(0,0,0,.25); 
     background-image: none!important; 
     border: 0px!important; 
     border-radius: 0!important; 
     box-shadow:none!important; 
     -webkit-transition: none!important; 
    -o-transition: none!important;
     transition: none!important; 
     vertical-align: 0!important; 
     margin: 0; 
    
        }
        .btn-primary, .btn-primary.focus, .btn-primary:focus {
    background-color: none!important;
     border-color:none!important; 
}
        .btn, .btn-default, .btn-default.focus, .btn-default:focus, .btn.focus, .btn:focus {
    background-color:none!important;
    border-color: #ABBAC3;
}
    </style>


    <script type="text/javascript">

        if (document.getElementById('<%=hdnRecID.ClientID%>').value == "1") {
            window.open("../Ctrls/ReportViewer.aspx")
            document.getElementById('<%=hdnRecID.ClientID%>').value = 0;
        }

    </script>
</asp:Content>
