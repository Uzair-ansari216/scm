﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CustomerList.ascx.vb" Inherits="Bridge_PGM.CustomerList" %>

<script type="text/javascript">

    // Variable Declared
    var gridItemsData;
    var gridnameList = "customergrdList";
    var pagingList = "customerPaging";
    var mydatalist;
    //   $("#" + gridnameList + "")
    var pagename = "CustomerPaymentNew.aspx";
    //    $('#' + pagingList + '')

    $(document).ready(function () {
        gridlistFill();

    });


    $(document).on("click", "td[aria-describedby='" + gridnameList + "_Action'] a", function (e) {
        var rowId = $(e.target).closest("tr").attr("id");
        rowData = jQuery("#" + gridnameList + "").getRowData(rowId);
        getSelectedRowList(rowId);

    });


    function gridlistFill() {
        $("#" + gridnameList + "").jqGrid({
            url: '' + pagename + '/GetGridList',
            datatype: 'json',
            mtype: 'POST',

            serializeGridData: function (postData) {
                return JSON.stringify(postData);
            },

            ajaxGridOptions: { contentType: "application/json" },
            loadonce: true,

            // convert(nvarchar(20),payment_id ) ID, u_name as UName, 
            // convert(nvarchar(20),vender_id)  as VenderID ,convert(nvarchar(20),v_date ) as VenderDate
            colNames: ['', 'ID', 'UName', 'VenderID', 'VenderDate'
            ],
            colModel: [
                 {
                     name: 'Action', index: 'Action', width: 10,
                     formatter: function (cellvalue, options, rowObject) {
                         var myGrid = $("#" + gridnameList + ""),
                          selectedRowId = myGrid.jqGrid('getGridParam', 'selrow'),
                          id = myGrid.jqGrid('getCell', selectedRowId, 'ID');

                         return '<a href="#" class="dialogselectbutton"> Select </a>';
                     }
                 },

                         {

                             name: 'ID', index: 'ID', editable: true, width: 10,
                             formoptions: { label: 'ID' },
                             editoptions: {
                                 dataInit: function (elem) {
                                     $(elem).addClass("disableText");
                                     $(elem).prop("readonly", "readonly");

                                 },
                             }
                         },

                              {

                                  name: 'UName', index: 'UName', editable: true, width: 25,
                                  formoptions: { label: 'UName Number' },
                                  editoptions: {
                                      dataInit: function (elem) {
                                          $(elem).addClass("disableText");
                                          $(elem).prop("readonly", "readonly");

                                      },
                                  }
                              },

                       {
                           name: 'VenderID', index: 'VenderID', editable: false, width: 15,
                           formoptions: { label: 'VenderID' },
                           editoptions: {
                               dataInit: function (elem) {
                                   $(elem).addClass("disableText");
                                   $(elem).prop("readonly", "readonly");

                               },
                           }
                       },
                         {
                             name: 'VenderDate', index: 'VenderDate', formatter: 'date', editable: false, width: 20,
                             formoptions: { label: 'VenderDate' },
                         },

            ],
            rowNum: 10,
            rowList: [5, 10, 20],
            pager: '#' + pagingList + '',
            gridview: true,
            rownumbers: true,
            multiselect: false,
            width: 600,
            sortable: true,
            viewrecords: true,
            jsonReader: {
                page: function (obj) { return 1; },
                total: function (obj) { return 1; },
                records: function (obj) { return obj.d.length; },
                root: function (obj) { return obj.d; },
                repeatitems: false,
                id: "0"
            },
            loadComplete: function (data) {
                $("#gs_Action").css('display', 'none');
            },
        });

        $("#" + gridnameList + "").jqGrid('navGrid', '#' + pagingList + '',
              {
                  edit: false,
                  add: false,
                  del: false,
                  search: false,

              }
       );


        $("#" + gridnameList + "").jqGrid('filterToolbar', { stringResult: true, searchOnEnter: true });

    }

    function getSelectedRowList(id) {
        $(".ui-dialog-titlebar-close").parent().find(".ui-dialog-titlebar-close").trigger('click');
        //var grid = $("#" + gridnameList + "");
        //var id = grid.getGridParam("selrow");


        var cellID = $("#" + gridnameList + " tr[id='" + id + "'] td[aria-describedby='" + gridnameList + "_ID']").text();
        if (id) {

            $("#action").val("edit");

            $.ajax({
                type: "POST",
                url: '' + pagename + '/GetItemsDetails?id=' + cellID,
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    var data = res.d;
                    console.log(data);
                    $("#txtPaymentId").val(data[0]);
                    $("#txtDate").val(data[1]);
                    $("#txtUserName").val(data[2]);
                    $("#ddlVender").val(data[3]);
                    $("#ddlPaymentType").val(data[4]);
                    $("#txtChequeDate").val(data[5]);
                    $("#txtChequeNo").val(data[6]);
                    $("#txtVendorPaidAmount").val(data[7]);
                    $("#txtWHTax").val(data[8]);
                    $("#txtTotalAmount").val(data[9]);
                    $("#txtDescription").val(data[10]);

                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });

        }
        else
            alert("No rows are selected");
    }



</script>

<div id="gridviewList">
    <table id="customergrdList"></table>
    <div id="customerPaging"></div>
</div>
