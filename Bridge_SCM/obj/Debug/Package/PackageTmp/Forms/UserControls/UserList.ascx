﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UserList.ascx.vb" Inherits="Bridge_PGM.UserList" %>
<link href="../../Content/dialog.css" rel="stylesheet" />
<link href="../../dtLayout/jquery-ui.min.css" rel="stylesheet" />
<link href="../../Content/ace.min.css" rel="stylesheet" />
    <script src="../../Content/ace.min.js"></script>
<script src="../../Content/jquery.jqGrid.min.js"></script>
<script type="text/javascript">

    // Variable Declared
    var gridItemsData;
    var gridnameList = "usergrdList";
    var pagingList = "userPaging";
    var mydatalist;
    //   $("#" + gridnameList + "")
    var pagename = "UserNew.aspx";
    //    $('#' + pagingList + '')

    $(document).ready(function () {
        gridlistFill();


    });

    $(document).on("click", "td[aria-describedby='" + gridnameList + "_Action'] a", function (e) {
        var rowId = $(e.target).closest("tr").attr("id")
        rowData = jQuery("#" + gridnameList + "").getRowData(rowId);
        getSelectedRowList(rowId);

    });


    function gridlistFill() {
        $("#" + gridnameList + "").jqGrid({
            url: '' + pagename + '/GetGridList',
            datatype: 'json',
            mtype: 'POST',

            serializeGridData: function (postData) {
                return JSON.stringify(postData);
            },

            ajaxGridOptions: { contentType: "application/json" },
            loadonce: true,

            colNames: ['', 'ID', 'Name'
            ],
            colModel: [

                      {
                          name: 'Action', index: 'Action', width: 10,
                          formatter: function (cellvalue, options, rowObject) {
                              var myGrid = $("#" + gridnameList + ""),
                               selectedRowId = myGrid.jqGrid('getGridParam', 'selrow'),
                               id = myGrid.jqGrid('getCell', selectedRowId, 'ID');

                              return '<a href="#"  class="dialogselectbutton"> Select </a>';
                          }
                      },
                         {

                             name: 'ID', index: 'ID', editable: true, width: 10,
                             formoptions: { label: 'ID' },
                             editoptions: {
                                 dataInit: function (elem) {
                                     $(elem).addClass("disableText");
                                     $(elem).prop("readonly", "readonly");

                                 },
                             }
                         },

                              {

                                  name: 'Name', index: 'Name', editable: true, width: 25,
                                  formoptions: { label: 'User Name' },
                                  editoptions: {
                                      dataInit: function (elem) {
                                          $(elem).addClass("disableText");
                                          $(elem).prop("readonly", "readonly");

                                      },
                                  }
                              },

            ],
            rowNum: 20,
            rowList: [5, 10, 20],
            pager: '#' + pagingList + '',
            gridview: true,
            rownumbers: true,
            multiselect: false,
            sortable: true,
            width: 400,
            viewrecords: true,
            jsonReader: {
                page: function (obj) { return 1; },
                total: function (obj) { return 1; },
                records: function (obj) { return obj.d.length; },
                root: function (obj) { return obj.d; },
                repeatitems: false,
                id: "0"
            },
            loadComplete: function (data) {
                $("#gs_Action").css('display', 'none');
            },
        });

        $("#" + gridnameList + "").jqGrid('navGrid', '#' + pagingList + '',
              {
                  edit: false,
                  add: false,
                  del: false,
                  search: false,

              }
       );


        $("#" + gridnameList + "").jqGrid('filterToolbar', { stringResult: true, searchOnEnter: true });

        //resizeGrid();

    }

    function getSelectedRowList(id) {
        $(".ui-dialog-titlebar-close").parent().find(".ui-dialog-titlebar-close").trigger('click');

        var cellID = $("#" + gridnameList + " tr[id='" + id + "'] td[aria-describedby='" + gridnameList + "_ID']").text();
        //alert(cellID);
        if (id) {

            $("#action").val("edit");

            $.ajax({
                type: "POST",
                url: '' + pagename + '/GetDetails?id=' + cellID,
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    var data = res.d;
                    console.log(data);
                    $("#txtCode").val(data[0]);
                    //$("#ddlLocation").val(data[1]);
                    $("#txtLogin").val(data[1]);
                    $("#ddlRole").val(data[2]);
                    $("#txtPassword").val(data[3]);
                    $("#txtRePassword").val(data[3]);
                    $("#txtUserName").val(data[4]);
                    var ischk = data[5];
                    console.log(ischk);
                    if (ischk == "True") {
                        $("#chkIsPower").prop('checked', true);
                    }
                    else if (ischk == "False") {
                        $("#chkIsPower").prop('checked', false);
                    }


                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });

        }
        else
            alert("No rows are selected");
    }

</script>

<div id="gridviewList">
    <table id="usergrdList"></table>
    <div id="userPaging"></div>
</div>
