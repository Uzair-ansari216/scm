﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="RoleDefineList.ascx.vb" Inherits="Bridge_SCM.RoleDefineList" %>
<%--<%: Scripts.Render("~/bundles/JQuery/js")%>
<%: Scripts.Render("~/bundles/JQuery/js1")%>--%>

<!-- jqgrid -->
<script src="../../Js/JQGridReq/jquery.jqGrid.js" type="text/javascript"></script>
<link href="../../Js/JQGridReq/ui.jqgrid.css" rel="stylesheet" type="text/css" />
<script src="../../Js/JQGridReq/grid.locale-en.js" type="text/javascript"></script>
<%--<script src="../../Layout/assets/vendors/bootstrap.min.js"></script>--%>

<style type="text/css">
    .parentitem {
        background: rgba(53, 195, 239, 0.32) !important;
    }

    .childitem {
        background-color: #f6fbfc;
    }
</style>
<script type="text/javascript">

    // Variable Declared
    var gridItemsData;
    var gridnameList = "relodefinegrdList";
    var pagingList = "relodefinePaging";
    var mydatalist;
    //   $("#" + gridnameList + "")
    var pagename = "RoleDefineNew.aspx";
    //    $('#' + pagingList + '')

    $(document).ready(function () {
        OnViewLoad();
        gridlistFill();


    });


    function OnViewLoad() {
        //  GetViewVoucherTypeList
        //$.ajax({
        //    type: "POST",
        //    url: '' + pagename + '/GetViewVoucherTypeList',
        //    contentType: "application/json",
        //    dataType: "json",
        //    success: function (res) {
        //        $("#ddlViewVoucherType").empty();
        //        $("#ddlViewVoucherType").append("<option value='-1'>--Please Select--</option>");
        //        jQuery.each(res.d, function (index, item) {
        //            $("#ddlViewVoucherType").append("<option value=" + item.Value + "> " + item.Text + " </option>");
        //        });
        //    },
        //    failure: function (errMsg) {
        //        alert(errMsg);
        //    }
        //});
    }

    $(document).on("click", "td[aria-describedby='" + gridnameList + "_Action'] a", function (e) {
        var rowId = $(e.target).closest("tr").attr("id")
        rowData = jQuery("#" + gridnameList + "").getRowData(rowId);
        $("#txtRoleID").val(rowData.ID);
        $("#txtShipTo").val(rowData.Name);
        //document.getElementById("ddlDefaultPage").value = rowData.DefaultPage;
        console.log(rowData);
        getSelectedRowViewList(rowData.ID);

    });

    function CheckParent2() {
        $('#rolejqgrid tbody tr').each(function (i, val) {
            var v = $(val).children().eq(6).text();
            if (v == "0") {
                $(val).addClass("parentitem");
            } else {
                $(val).addClass("childitem");
            }
        });
    }
    function gridlistFill() {
        $("#" + gridnameList + "").jqGrid({
            url: '' + pagename + '/FillViewGridList',
            datatype: 'json',
            mtype: 'POST',

            serializeGridData: function (postData) {
                return JSON.stringify(postData);
            },

            ajaxGridOptions: { contentType: "application/json" },
            loadonce: true,

            colNames: ['', 'ID', 'Name'],
            colModel: [

                      {
                          name: 'Action', index: 'Action', width: 10,
                          formatter: function (cellvalue, options, rowObject) {

                              return '<a href="#" onclick="getSelectedRowViewList() class="dialogselectbutton"> Select </a>';
                          }
                      },
                         {

                             name: 'ID', index: 'ID', editable: true, width: 10,
                             formoptions: { label: 'ID' },
                             editoptions: {
                                 dataInit: function (elem) {
                                     $(elem).addClass("disableText");
                                     $(elem).prop("readonly", "readonly");

                                 },
                             }
                         },

                              {

                                  name: 'Name', index: 'Name', editable: true, width: 25,
                                  formoptions: { label: 'Name' },
                                  editoptions: {
                                      dataInit: function (elem) {
                                          $(elem).addClass("disableText");
                                          $(elem).prop("readonly", "readonly");

                                      },
                                  }
                              },
                              
            ],
            rowNum: 10,
            rowList: [5, 10, 20],
            pager: '#' + pagingList + '',
            gridview: true,
            multiselect: false,
            sortable: true,
            width: 750,
            viewrecords: true,
            jsonReader: {
                page: function (obj) { return 1; },
                total: function (obj) { return 1; },
                records: function (obj) { return obj.d.length; },
                root: function (obj) { return obj.d; },
                repeatitems: false,
                id: "0"
            },
            loadComplete: function (data) {
                $("#gs_Action").css('display', 'none');
                //CheckParent2();
            },
        });

        $("#" + gridnameList + "").jqGrid('navGrid', '#' + pagingList + '',
              {
                  edit: false,
                  add: false,
                  del: false,
                  search: false,

              }
       );

        $("#" + gridnameList + "").jqGrid('filterToolbar', { defaultSearch: 'cn', stringResult: true, searchOnEnter: true });

    }

    function getSelectedRowViewList(id) {
        $(".ui-dialog-titlebar-close").parent().find(".ui-dialog-titlebar-close").trigger('click');
        if (id) {
            $("#action").val("edit");
            if (id != "") {
                $.ajax({
                    type: "POST",
                    url: '' + pagename + '/GetViewGridDetailsList?id=' + id,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (res) {
                        griddata = res.d;
                        var trf = $("#rolejqgrid tbody:first tr:first")[0];
                        $("#rolejqgrid tbody:first").empty().append(trf);

                        for (var i = 0; i <= griddata.length; i++) {
                            $("#rolejqgrid").jqGrid('addRowData', i + 1, griddata[i]);
                        }
                        $('#rolejqgrid tbody tr').each(function (i, val) {
                            var v = $(val).children().eq(6).text();

                            if (v == "0") {
                                $(val).addClass("parentitem");
                            } else {
                                $(val).addClass("childitem");
                            }
                        });
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });

            }

        }
        else
            alert("No rows are selected");
    }



</script>

<div id="gridviewList">
    <table id="relodefinegrdList"></table>
    <div id="relodefinePaging"></div>
</div>


