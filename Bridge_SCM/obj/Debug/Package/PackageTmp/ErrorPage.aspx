﻿<%@ Page Title="" Language="vb" AutoEventWireup="false"  CodeBehind="ErrorPage.aspx.vb" Inherits="Bridge_SCM.ErrorPage" %>


<!DOCTYPE html>
<html lang="en">
    <head>
        <title>POMS</title>

        <!-- BEGIN META -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="your,keywords">
        <meta name="description" content="">
        <!-- END META -->

        <!-- BEGIN STYLESHEETS -->
        <link type="text/css" rel="stylesheet" href="/Layout/assets/css/theme/bootstrap.css?1422792965" />
        <link type="text/css" rel="stylesheet" href="/Layout/assets/css/theme/materialadmin.css?1425466319" />
        <link type="text/css" rel="stylesheet" href="/Layout/assets/css/theme/font-awesome.min.css?1422529194" />
        <link type="text/css" rel="stylesheet" href="/Layout/assets/css/theme/material-design-iconic-font.min.css?1421434286" />

        <link type="text/css" rel="stylesheet" href="/Layout/assets/css/theme/libs/Data<a href="ErrorPage.aspx">ErrorPage.aspx</a>Tables/extensions/dataTables.colVis.css?1423553990" />
        <link type="text/css" rel="stylesheet" href="/Layout/assets/css/theme/libs/DataTables/extensions/dataTables.tableTools.css?1423553990" />
        <!-- END STYLESHEETS -->
        <!--my css-->
        <link href="assets/css/theme/libs/dropify/dropify.min.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="/Layout/assets/css/theme/libs/jquery-ui/jquery-ui-theme.css?1423393666" />
        <link type="text/css" rel="stylesheet" href="/Layout/assets/css/theme/libs/select2/select2.css?1424887856" />
        <link type="text/css" rel="stylesheet" href="/Layout/assets/css/theme/libs/bootstrap-datepicker/datepicker3.css?1424887858" />
        <link type="text/css" rel="stylesheet" href="/Layout/assets/css/theme/libs/bootstrap-colorpicker/bootstrap-colorpicker.css?1424887860" />
     
        <link href="Layout/assets/style.css" rel="stylesheet" />
        <link href="/Layout/assets/css/theme/responsive.css" rel="stylesheet" type="text/css"/>
        
    </head>
    <body class="header-fixed menubar-pin menubar-first">

       

        <!-- BEGIN BASE-->
        <div>


            <!-- BEGIN CONTENT-->
            <div id="content">

                <!-- BEGIN LIST SAMPLES -->
                <section class="text-center error-page">
                
                    <h1>ERROR 4<img src="/Layout/assets/images/error-404.png" />4</h1>
                    
                  
                    <h3>Sorry :( , Applicaton session has been expired. Please Login again</h3>

                    <h5>Go to Login Page <a href="Default.aspx">click here</a></h5>

                </section>
            </div><!--end #content-->
            <!-- END CONTENT -->




        </div><!--end #base-->
        <!-- END BASE -->

       


        <!-- BEGIN JAVASCRIPT -->
        <script src="/Layout/assets/js/libs/jquery/jquery-1.11.2.min.js"></script>
        <script src="/Layout/assets/js/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="/Layout/assets/js/libs/bootstrap/bootstrap.min.js"></script>
        <script src="/Layout/assets/js/libs/autosize/jquery.autosize.min.js"></script>
        <script src="/Layout/assets/js/libs/spin.js/spin.min.js"></script>
        <script src="/Layout/assets/js/libs/jquery-ui/jquery-ui.min.js"></script>
        <script src="/Layout/assets/js/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
        <script src="/Layout/assets/js/core/source/App.js"></script>
        <script src="/Layout/assets/js/core/source/AppNavigation.js"></script>
        <script src="/Layout/assets/js/core/source/AppForm.js"></script>
        <script src="/Layout/assets/js/libs/moment/moment.min.js"></script>
        <script src="/Layout/assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
        <script src="/Layout/assets/js/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
        <script src="/Layout/assets/js/libs/select2/select2.min.js"></script>
        <script src="/Layout/assets/js/libs/dropify/dropify.min.js" type="text/javascript"></script>
        <script src="/Layout/assets/js/core/demo/Demo.js"></script>
        <script src="/Layout/assets/js/core/demo/DemoFormComponents.js"></script>
        <script src="/Layout/assets/js/libs/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
        <script src="/Layout/assets/js/validation.js" type="text/javascript"></script>
        <script src="/Layout/assets/js/custom-script.js" type="text/javascript"></script>

    </body>
</html>
