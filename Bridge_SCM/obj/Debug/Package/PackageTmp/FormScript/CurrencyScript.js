﻿var commonService = "../../appServices/CommonService.asmx";
var serviceurl = "../../appServices/CurrencyService.asmx";
var tblname = "#tableCity";
var table;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _ID = "#txtID";
var _Currency = "#txtcurrency";
var _Symbol = "#txtsymbol";
var _Unit = "#txtunit";
var _Description = "#txtdescription";
var _DefaultCurrency = "#isdefault";



var loginUrl = "/Default.aspx";

$(document).ready(function () {

    $("body #currencyForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #currencyForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #currencyForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })

    OnLoadGrid();
    bindAddEditCurrency();
    OnDelete();

});

function OnLoadGrid() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblname).DataTable().destroy();
            LoadDataTable(response.d);
        },
        error: OnErrorCall
    });
}

function bindAddEditCurrency() {
    $("body").on("click", "#btnAdd , .edit", function () {

        let id = $(this).attr("_recordId")
        clearAllMessages("currencyForm")

        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("Currency", "currencyForm")
        }
        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("Currency", "currencyForm", service_DeveloperMode)

        //}

        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/AddEditCurrency",
            data: JSON.stringify({ id: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        if (parseInt(id) > 0) {
                            $("#formModalLabel").text("Edit");
                            $(".label-floating").addClass("is-focused");
                            if (data.GenericList.DefaultCurrency == "True") {
                                $('input[id=isdefault]').attr('checked', true);
                                $('input[id=isdefault]').prop('checked', true);
                            }
                            else {
                                $('input[id=isdefault]').attr('checked', false);
                                $('input[id=isdefault]').prop('checked', false);
                            }
                        } else {
                            $("#formModalLabel").text("Add");
                            $(".label-floating").removeClass("is-focused");
                        }
                        $(_ID).val(data.GenericList.ID);
                        $(_Currency).val(data.GenericList.Currency);
                        $(_Symbol).val(data.GenericList.Symbol);
                        $(_Unit).val(data.GenericList.Unit);
                        $(_Description).val(data.GenericList.Description);

                        $("#modalAdd").modal();
                    }

                }
            },
            error: OnErrorCall,
            complete: function () {
                isValueExist(_Currency, $(_Currency).val())
                isValueExist(_Symbol, $(_Symbol).val())
                isValueExist(_Unit, $(_Unit).val())
                isValueExist(_Description, $(_Description).val())
            }
        });
    })
}

function OnSave() {
    var isValid = true

    $("body #currencyForm input").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    $("body #currencyForm select").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    if (isValid) {
        if ($(_DefaultCurrency).is(':checked')) {

            isdefault = 1;
        }
        else {
            isdefault = 0;
        }

        var dataSend = {
            ID: $(_ID).val(),
            Currency: $(_Currency).val(),
            Symbol: $(_Symbol).val(),
            Unit: $(_Unit).val(),
            Description: $(_Description).val(),
            DefaultCurrency: isdefault
        }

        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnSave",
            data: JSON.stringify({ model: dataSend }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        ClearFields();
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(success);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "exist") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(exist);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "update") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(update);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    }
                }
            },
            error: OnErrorCall
        });
    }
}

function FormValidaton() {
    msg = "";

    $("#rCurrency").addClass("hide");

    if ($(_Currency).val() == "") {
        msg += "required";
        $("#rCurrency").removeClass("hide");
    } else {
        $("#rCurrency").addClass("hide");
    }



    $("#rSymbol").addClass("hide");

    if ($(_Symbol).val() == "") {
        msg += "required";
        $("#rSymbol").removeClass("hide");
    } else {
        $("#rSymbol").addClass("hide");
    }


    $("#rUnit").addClass("hide");

    if ($(_Unit).val() == "") {
        msg += "required";
        $("#rUnit").removeClass("hide");
    } else {
        $("#rUnit").addClass("hide");
    }


    $("#rDescription").addClass("hide");

    if ($(_Description).val() == "") {
        msg += "required";
        $("#rDescription").removeClass("hide");
    } else {
        $("#rDescription").addClass("hide");
    }


    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}

function ClearFields() {

    $(_ID).val("");
    $(_Currency).val("");
    $(_Symbol).val("");
    $(_Unit).val("");
    $(_Description).val("");


    $('input[id=isdefault]').attr('checked', false);
    $('input[id=isdefault]').prop('checked', false);

    $("#ValidationSummary").text("");
    $("#rCurrency").addClass("hide");
    $("#rSymbol").addClass("hide");
    $("#rUnit").addClass("hide");
    $("#rDescription").addClass("hide");

}

var DeleteID = "";
function OnDelete() {
    $(tblname).on('click', '.del', function () {
        let id = $(this).attr('_recordId');
        DeleteID = id;
        $.ajax({
            type: "POST",
            url: "" + commonService + "/isAuthenticatedForDelete",
            data: JSON.stringify({ form: "Currency" }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        $("#modalDelete").modal();
                    }
                }
            },
            error: OnErrorCall
        });
    });
}

function OnConfirmDelete() {
    Delete(DeleteID);
}

function Delete(id) {
    if (id != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDelete",
            data: JSON.stringify({ dataID: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(delt);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalDelete").modal('hide');

                    } else if (data.Message == "failed") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(failedmsg);
                        $("#Validation").fadeOut(5000);
                    } else if (data.Message == "use") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("It is used by another process, Please try again!");
                        $("#Validation").fadeOut(5000);
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}


function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        dom: 'Bfrtip',
        buttons: [
             {
                 extend: 'print',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Currency',
                 text: '<i class="fa fa-print"></i>  Print'

             },
             {
                 extend: 'excelHtml5',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Currency',
                 text: '<i class="fa fa-file-excel"></i>  Excel',
             },
            {
                extend: 'pdf',
                footer: true,
                exportOptions: {
                    columns: [0, 1, 3]
                },
                title: 'Currency',
                text: '<i class="fa fa-file-pdf"></i>  PDF'
            },
        ],
        "aaData": JSON.parse(datasource),
        "aoColumns": [

                        { data: "ID", sDefaultContent: "" },
                        { data: "Currency", sDefaultContent: "" },
                        { data: "Symbol", sDefaultContent: "" },
                        { data: "Unit", sDefaultContent: "" },
                        { data: "Description", sDefaultContent: "" },
                        { data: "DefaultCurrency", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" _recordId = ' + data.ID + ' class="del" title="del"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#" _recordId = ' + data.ID + ' class="edit" title="Edit"><i class="fas fa-pencil-alt" ></i></a>';
                                    var html = del + edit;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [

            { "targets": [0], "width": "5%" },
            { "targets": [1], "width": "15%" },
            { "targets": [2], "width": "15%" },
            { "targets": [3], "width": "30%" },
            { "targets": [4], "width": "10%" },
            { "targets": [5], "width": "10%" },
            { "targets": [6], "width": "10%" }
        ]
    });
}

function OnErrorCall() {
    alert("Something went wrong");
}




