﻿
var serviceurl = "../../appServices/ReportRoleDefineService.asmx";
var tblname = "#tblReport";
var tblDetail = "#tableDetail";
var table, table2;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _RoleID = "#txtRoleID";
var _RoleName = "#txtRoleName";

var loginUrl = "/Default.aspx";


$(document).ready(function () {
    OnLoadGrid();
    $('#btnCheckAll').click(function () {
        var checked = !$(this).data('checked');
        $('input:checkbox').prop('checked', checked);
        $(this).val(checked ? 'uncheck all' : 'check all')
        $(this).data('checked', checked);
    });

    OnSelect();
});

function OnView() {
    OnLoadGridView();
    $("#modalView").modal();
}

function OnLoadGridView() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + '/GetGridList',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblDetail).DataTable().destroy();
            LoadDataTableView(response.d);
        },
        error: OnErrorCall
    });
}

function LoadDataTableView(datasource) {
    table2 = $(tblDetail).DataTable({
        "searching": false,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [
             {
                 data: null,
                 sDefaultContent: "",
                 "orderable": false,
                 render: function (data, type, row) {
                     if (type === 'display') {
                         var del = ' <a href="#" class="select" title="Select">Select</a>';
                         var html = del; return html;
                     }
                     return data;
                 }
             },
                        { data: "Value", sDefaultContent: "" },
                        { data: "Text", sDefaultContent: "" }

        ]
    });
}


function FillEventCategory() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + '/GetEventCategoryList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_EventCategory).empty();
            $(_EventCategory).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_EventCategory).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}

function OnSave() {
    if ($(_RoleID).val() > 0) {
        DeleteDetail($(_RoleID).val())
    }
}



function DeleteDetail(ID) {
    if (ID > 0) {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/DeleteDetail",
            data: JSON.stringify({ ID: ID }),
            contentType: "application/json",
            dataType: "json",
            success: function (res) {
                var data = res.d;
                SaveDetail(ID)
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    }
}

function SaveDetail(ID) {
    if (ID > 0) {
        var isView;
        $('#tblReport tbody tr').each(function () {
            if ($(this).find("td:nth-child(1) input:checkbox").is(':checked')) {
                isView = true;
            } else if ($(this).find("td:nth-child(1) input:checkbox").not(':checked')) {
                isView = false;
            }

            var row = $(this).closest('tr');
            var rowIndex = table.row(row).index();
            var rowData = table.row(rowIndex).data();

            SaveGrid(ID, rowData.ReportID, isView)
        });
        alert("Data Saved Successfully")
        OnLoadGrid();
        ClearFields();
    }
}

function SaveGrid(ID, ReportID, isView) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/SaveDetail",
        data: JSON.stringify({ ID: ID, ReportID: ReportID, isView: isView }),
        contentType: "application/json",
        dataType: "json",
        success: function (res) {
            var data = res.d;
        },
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
}

function FormValidaton() {
    msg = "";


    $("#rRoleName").addClass("hide");

    if ($(_RoleName).val() == "") {
        msg += "Role Name is required";
        $("#rRoleName").removeClass("hide");
    } else {
        $("#rRoleName").addClass("hide");
    }


    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}

function ClearFields() {
    $(_RoleID).val("");
    $(_RoleName).val("");

    $("#ValidationSummary").text("");
    $("#rRoleName").addClass("hide");
}

function OnLoadGrid() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblname).DataTable().destroy();
            LoadDataTable(response.d);
        },
        error: OnErrorCall
    });


}


function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": false,
        "bLengthChange": false,
        "sSort": false,
        "bSort": false,
        "bInfo": false,
        "bPaginate": false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [

                        { data: "ReportID", sDefaultContent: "" },
                         {
                             data: null,
                             sDefaultContent: "",
                             "width": "10%",
                             "orderable": false,
                             render: function (data, type, row) {
                                 if (type === 'display') {
                                     var select = '<div class="checkbox"><label><input type="checkbox" name="optionsCheckboxes" id="' + data.ReportID.substr(0, data.ReportID.indexOf('.')) + '" ><span class="checkbox-material"><span class="check"></span></span></label></div>';
                                     var html = select;
                                     return html;
                                 }
                                 return data;
                             }
                         },
                         { data: "ReportModule", sDefaultContent: "" },
                         { data: "ReportCategory", sDefaultContent: "" },
                         { data: "ReportName", sDefaultContent: "" }

        ],
        "columnDefs": [
             { "targets": [0], "visible": false },
        ]
    });
    $('input:checkbox').prop('checked', true);
    $('#btnCheckAll').data('checked', true);
}

function OnAdd() {
    ClearFields();
    $("#formModalLabel").text("Add");
    $("#modalAdd").modal();
}


function OnErrorCall() {
    alert("Something went wrong");
}

function OnSelect() {
    $(tblDetail).on('click', '.select', function () {
        var row = $(this).closest('tr');
        var rowIndex = table2.row(row).index();
        var rowData = table2.row(rowIndex).data();
        $(_RoleID).val(rowData.Value)
        $(_RoleName).val(rowData.Text)
        $("#modalView").modal('hide');
        console.log(rowData);
        OnSelectRole(rowData.Value)
    });
}

function OnSelectRole(ID) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/GetDetailsRole",
        data: JSON.stringify({ ID: ID }),
        contentType: "application/json",
        dataType: "json",
        success: function (res) {
            var data = JSON.parse(res.d);

            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].isView == "True") {
                        $('input[id=' + data[i].ReportID.substr(0, data[i].ReportID.indexOf('.')) + ']').attr('checked', true);
                        $('input[id=' + data[i].ReportID.substr(0, data[i].ReportID.indexOf('.')) + ']').prop('checked', true);
                    }
                    else {
                        $('input[id=' + data[i].ReportID.substr(0, data[i].ReportID.indexOf('.')) + ']').attr('checked', false);
                        $('input[id=' + data[i].ReportID.substr(0, data[i].ReportID.indexOf('.')) + ']').prop('checked', false);
                    }
                }
            } else {
                $('input[name="optionsCheckboxes"]').attr('checked', false);
                $('input[name="optionsCheckboxes"]').prop('checked', false);
            }


        },
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
}