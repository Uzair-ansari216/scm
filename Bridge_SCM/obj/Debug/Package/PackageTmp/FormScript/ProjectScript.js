﻿var serviceurl = "../../appServices/ProjectService.asmx";
var commonService = "../../appServices/CommonService.asmx";
var tblname = "#tableData";
var table;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _ID = "#txtprojectcode";
var _ProjectTitle = "#txtprojecttitle";
var _Client = "#ddlclient";
var _ProjectType = "#ddlprojecttype";
var _StartDate = "#txtstartdate";
var _EndDate = "#txtenddate";
var _DepartmentCode = "#ddldepartmentcode";
var _DurationDays = "#txtdurationdays";
var _EstimatedCost = "#txtestimatedcost";
var _DebotrAccount = "#txtdebotraccount";
var _OpeningBalance = "#txtopeningbalance";
var _CreditorAccount = "#txtcreditoraccount";
var _CopeningBalance = "#txtcopeningbalance";
var _Description = "#txtdescription";
var tblAccountCode = "#tblAccountCode"
var fieldType;
var loginUrl = "/Default.aspx";


$(document).ready(function () {

    $("body #projectForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #projectForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #projectForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })

    OnLoadGrid();
    bindAddEditProject();
    OnDelete();

    FillClient();
    FillGetProjecType();
    FillDepartmentCode();
    OnLoadGridAccountCode2();
    OnSelectAccountCode2();
    $(_StartDate).datepicker({ autoclose: true });
    $(_EndDate).datepicker({ autoclose: true });


});

function bindAddEditProject() {
    $("body").on("click", "#btnAdd , .edit", function () {

        let id = $(this).attr("_recordId")
        clearAllMessages("projectForm")

        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("Project", "projectForm")
        }
        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("Project", "projectForm", service_DeveloperMode)

        //}

        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/AddEditProject",
            data: JSON.stringify({ id: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        if (parseInt(id) > 0) {

                            $("#formModalLabel").text("Edit");
                            $("#ValidationSummary").text("");
                            $(".label-floating").addClass("is-focused");

                            var javascriptDate2 = new Date(data.GenericList.StartDate);
                            javascriptDate2 = javascriptDate2.getMonth() + 1 + "/" + javascriptDate2.getDate() + "/" + javascriptDate2.getFullYear();
                            $(_StartDate).val(javascriptDate2);

                            var javascriptDate3 = new Date(data.GenericList.EndDate);
                            javascriptDate3 = javascriptDate3.getMonth() + 1 + "/" + javascriptDate3.getDate() + "/" + javascriptDate3.getFullYear();
                            $(_EndDate).val(javascriptDate3);
                        } else {
                            $("#formModalLabel").text("Add");
                            $(".label-floating").removeClass("is-focused");
                        }
                        $(_ID).val(data.GenericList.ID);
                        $(_ProjectTitle).val(data.GenericList.ProjectTitle);
                        $(_Client).val(data.GenericList.ClientID);
                        $("#ddlprojecttype").find("option:contains('" + data.GenericList.ProjectType + "')").prop("selected", 'selected')
                        $(".select2-selection__rendered").text(data.GenericList.ProjectType)
                        //$(_ProjectType).val(data.GenericList.ProjectType);


                        $(_DepartmentCode).val(data.GenericList.DepartmentCode);
                        $(_DurationDays).val(data.GenericList.EstimatedDuration);
                        $(_EstimatedCost).val(data.GenericList.EstimatedCost);
                        $(_DebotrAccount).val(data.GenericList.DebtorAccountCode);


                        $(_OpeningBalance).val(data.GenericList.DebtorOpBal);
                        $(_CreditorAccount).val(data.GenericList.CreditorAccountCode);
                        $(_CopeningBalance).val(data.GenericList.CreditorOpBal);
                        $(_Description).val(data.GenericList.Description);

                        $(_ProjectType).select2({
                            tags: true,
                            dropdownParent: $("#modalAdd")
                        });
                        $("#modalAdd").modal();
                    }

                }
            },
            error: OnErrorCall,
            complete: function () {
                isValueExist(_ProjectTitle, $(_ProjectTitle).val())
                isValueExist(_Client, $(_Client).val())
                isValueExist(_ProjectType, $(_ProjectType).val())
                isValueExist(_DepartmentCode, $(_DepartmentCode).val())
                isValueExist(_DurationDays, $(_DurationDays).val())
                isValueExist(_EstimatedCost, $(_EstimatedCost).val())
                isValueExist(_DebotrAccount, $(_DebotrAccount).val())
                isValueExist(_OpeningBalance, $(_OpeningBalance).val())
                isValueExist(_CreditorAccount, $(_CreditorAccount).val())
                isValueExist(_CopeningBalance, $(_CopeningBalance).val())
                isValueExist(_Description, $(_Description).val())

            }
        });
    })
}


function OnSave() {

    var isValid = true

    $("body #projectForm input").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    $("body #projectForm select").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    if (isValid) {
        var dataSend = {

            ID: $(_ID).val(),
            ProjectTitle: $(_ProjectTitle).val(),

            ClientID: $(_Client).val(),


            ProjectType: $(_ProjectType).find('option:selected').text(),
            StartDate: $(_StartDate).val(),
            EndDate: $(_EndDate).val(),


            DepartmentCode: $(_DepartmentCode).val(),
            EstimatedDuration: $(_DurationDays).val(),
            EstimatedCost: $(_EstimatedCost).val(),
            DebtorAccountCode: $(_DebotrAccount).val(),


            DebtorOpBal: $(_OpeningBalance).val(),
            CreditorAccountCode: $(_CreditorAccount).val(),
            CreditorOpBal: $(_CopeningBalance).val(),
            Description: $(_Description).val(),
        }

        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnSave",
            data: JSON.stringify({ model: dataSend }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        ClearFields();
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                        $("#modalAdd").modal('hide');
                    }
                    if (data.Message == "success") {
                        ClearFields();
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(success);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "exist") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(exist);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "update") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(update);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        ClearFields();
                        $("#modalAdd").modal('hide');
                    }
                }
            },
            error: OnErrorCall
        });
    }

}

function FormValidaton() {
    msg = "";

    $("#rProjectTitle").addClass("hide");

    if ($(_ProjectTitle).val() == "") {
        msg += "required";
        $("#rProjectTitle").removeClass("hide");
    } else {
        $("#rProjectTitle").addClass("hide");
    }

    $("#rStartDate").addClass("hide");

    if ($(_StartDate).val() == "") {
        msg += "required";
        $("#rStartDate").removeClass("hide");
    } else {
        $("#rStartDate").addClass("hide");
    }

    $("#rEndDate").addClass("hide");

    if ($(_EndDate).val() == "") {
        msg += "required";
        $("#rEndDate").removeClass("hide");
    } else {
        $("#rEndDate").addClass("hide");
    }

    $("#rDurationDays").addClass("hide");

    if ($(_DurationDays).val() == "") {
        msg += "required";
        $("#rDurationDays").removeClass("hide");
    } else {
        $("#rDurationDays").addClass("hide");
    }

    $("#rEstimatedCost").addClass("hide");

    if ($(_EstimatedCost).val() == "") {
        msg += "required";
        $("#rEstimatedCost").removeClass("hide");
    } else {
        $("#rEstimatedCost").addClass("hide");
    }

    //$("#rMaxqty").addClass("hide");

    //if ($(_Maxqty).val() == "") {
    //    msg += "Max Qty is required";
    //    $("#rMaxqty").removeClass("hide");
    //} else {
    //    $("#rMaxqty").addClass("hide");
    //}



    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}

function ClearFields() {

    $(_ID).val("");
    $(_ProjectTitle).val("");
    $(_StartDate).val("");
    $(_EndDate).val("");


    $(_DurationDays).val("");
    $(_EstimatedCost).val("");
    $(_DebotrAccount).val("");
    $(_OpeningBalance).val("");


    $(_CreditorAccount).val("");
    $(_CopeningBalance).val("");
    $(_Description).val("");

    $(_Client).val("-1");
    $(_ProjectType).val("-1");
    $(_DepartmentCode).val("-1");




    $("#ValidationSummary").text("");
    $("#rProjectTitle").addClass("hide");
    $("#rStartDate").addClass("hide");
    $("#rEndDate").addClass("hide");
    $("#rDurationDays").addClass("hide");
    $("#rEstimatedCost").addClass("hide");



}

function OnLoadGrid() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            let data = JSON.parse(response.d)
            if (!data.Status) {
                $("#Validation").fadeIn(1000);
                $("#Validation").text(data.Message);
                $("#Validation").fadeOut(5000);
            } else {
                $(tblname).DataTable().destroy();
                LoadDataTable(data.GenericList);
            }
        },
        error: OnErrorCall
    });


}

function OnConfirmDelete() {
    Delete(DeleteID, DeleteName);
}

var DeleteID = "";
var DeleteName = ""
function OnDelete() {
    $(tblname).on('click', '.del', function () {
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData);
        DeleteID = rowData.ID;
        DeleteName = rowData.ProjectTitle
        $.ajax({
            type: "POST",
            url: "" + commonService + "/isAuthenticatedForDelete",
            data: JSON.stringify({ form: "Projects" }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        $("#modalDelete").modal();
                    }
                }
            },
            error: OnErrorCall
        });

    });
}

function Delete(id, Name) {
    if (id != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDelete",
            data: JSON.stringify({ dataID: id, dataName: Name }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                        $("#modalDelete").modal('hide');
                    }
                    if (data.Message == "success") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(delt);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalDelete").modal('hide');

                    } else if (data.Message == "failed") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(failedmsg);
                        $("#Validation").fadeOut(5000);
                    } else if (data.Message == "use") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("It is used by another process, Please try again!");
                        $("#Validation").fadeOut(5000);
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}


function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        dom: 'Bfrtip',
        buttons: [
             {
                 extend: 'print',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Project',
                 text: '<i class="fas fa-print"></i>  Print'

             },
             {
                 extend: 'excelHtml5',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Project',
                 text: '<i class="fas fa-file-excel"></i>  Excel',
             },
            {
                extend: 'pdf',
                footer: true,
                exportOptions: {
                    columns: [0, 1, 3]
                },
                title: 'Project',
                text: '<i class="fas fa-file-pdf"></i>  PDF'
            },
        ],
        "aaData": datasource,
        "aoColumns": [

                        { data: "ID", sDefaultContent: "" },
                        { data: "ProjectTitle", sDefaultContent: "" },
                        { data: "ProjectType", sDefaultContent: "" },
                        { data: "ClientID", sDefaultContent: "" },
                        { data: "Company", sDefaultContent: "" },
                        { data: "StartDate", sDefaultContent: "" },
                        { data: "EndDate", sDefaultContent: "" },
                        { data: "DepartmentCode", sDefaultContent: "" },
                        { data: "EstimatedDuration", sDefaultContent: "" },
                        { data: "EstimatedCost", sDefaultContent: "" },
                        { data: "DebtorAccountCode", sDefaultContent: "" },
                        { data: "DebtorOpBal", sDefaultContent: "" },
                        { data: "CreditorAccountCode", sDefaultContent: "" },
                        { data: "CreditorOpBal", sDefaultContent: "" },
                        { data: "Description", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" _recordId = ' + data.ID + ' class="del" title="del"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#" _recordId = ' + data.ID + ' class="edit" title="Edit"><i class="fas fa-pencil-alt" ></i></a>';
                                    var html = del + edit;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [
             //{
             //    "targets": [4],
             //    "type": "date",
             //    "render": function (data) {
             //        if (data !== null) {
             //            var javascriptDate = new Date(data);
             //            console.log(data);
             //            javascriptDate = javascriptDate.getMonth() + 1 + "/" + javascriptDate.getDate() + "/" + javascriptDate.getFullYear();
             //            return javascriptDate;
             //        } else {
             //            return "";
             //        }
             //    }
             //},
            {
                "targets": [5],
                "type": "date",
                "render": function (data) {
                    if (data !== null) {
                        var javascriptDate = new Date(data);
                        console.log(data);
                        javascriptDate = javascriptDate.getMonth() + 1 + "/" + javascriptDate.getDate() + "/" + javascriptDate.getFullYear();
                        return javascriptDate;
                    } else {
                        return "";
                    }
                }
            },
            {
                "targets": [6],
                "type": "date",
                "render": function (data) {
                    if (data !== null && data !== "") {
                        var javascriptDate = new Date(data);
                        console.log(data);
                        javascriptDate = javascriptDate.getMonth() + 1 + "/" + javascriptDate.getDate() + "/" + javascriptDate.getFullYear();
                        return javascriptDate;
                    } else {
                        return "";
                    }
                }
            },
            { "targets": [0], "width": "3%" },
            { "targets": [1], "width": "10%" },
            { "targets": [2], "width": "10%" },
            { "targets": [3], "visible": false },
            { "targets": [6], "width": "10%" },
            { "targets": [7], "width": "10%" },
            { "targets": [8], "visible": false },
            { "targets": [9], "visible": false },
            { "targets": [10], "width": "20%" },
            { "targets": [11], "visible": false },
            { "targets": [12], "visible": false },
            { "targets": [13], "width": "10%" },
            { "targets": [14], "width": "15%" },
            { "targets": [15], "width": "15%" }

        ]
    });
}

function OnErrorCall() {
    alert("Something went wrong");
}

function FillClient() {

    //alert('3')
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetClient',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Client).empty();
            $(_Client).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Client).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
            //$(_dependent).select2('val', index);

        }, complete: function () {
            isValueExist(_Client, $(_Client).val())
        }
        //failure: function (errMsg) {
        //    alert(errMsg);
        //}
    });

}

function FillGetProjecType() {

    //alert('3')
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetProjecType',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_ProjectType).empty();
            $(_ProjectType).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_ProjectType).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
            //$(_dependent).select2('val', index);

        }, complete: function () {
            isValueExist(_ProjectType, $(_ProjectType).val())
        }
        //failure: function (errMsg) {
        //    alert(errMsg);
        //}
    });

}

function FillDepartmentCode() {

    //alert('3')
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetDepartmentCode',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_DepartmentCode).empty();
            $(_DepartmentCode).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_DepartmentCode).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
            //$(_dependent).select2('val', index);

        }, complete: function () {
            isValueExist(_DepartmentCode, $(_DepartmentCode).val())
        }
        //failure: function (errMsg) {
        //    alert(errMsg);
        //}
    });

}

function OnSearchBankAccount(type) {
    fieldType = type
    $("#H3").text("Account Name / Code");
    $("#modalViewAccountCode").modal();
}

function OnSelectAccountCode2() {
    $(tblAccountCode).on('click', '.select', function () {
        var row = $(this).closest('tr');
        var rowIndex = table6.row(row).index();
        var rowData = table6.row(rowIndex).data();
        if (fieldType == 'debitor') {
            $("#txtdebotraccount").val(rowData.AccountCode)
            isValueExist("#txtdebotraccount", $("#txtdebotraccount").val())
        } else {
            $("#txtcreditoraccount").val(rowData.AccountCode)
            isValueExist("#txtcreditoraccount", $("#txtcreditoraccount").val())
        }
        $("#modalViewAccountCode").modal('hide');
    });
}


function OnLoadGridAccountCode2() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGridDetailAccountCode",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblAccountCode).DataTable().destroy();
            LoadDataTableDetailAccountCode(response.d);
        },
        error: OnErrorCall
    });
}

function LoadDataTableDetailAccountCode(datasource) {
    table6 = $(tblAccountCode).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [
                    {
                        data: null,
                        sDefaultContent: "",
                        "orderable": false,
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var select = ' <a href="#" class="select"><b>Select</b></a>';
                                var html = select;
                                return html;
                            }
                            return data;
                        }
                    },
                        { data: "AccountCode", sDefaultContent: "" },
                        { data: "AccountName", sDefaultContent: "" }

        ],
        "columnDefs": [
             //{ "targets": [1], "visible": false, "searchable": false },
        ]
    });
}