﻿var serviceurl = "../../appServices/SalesOrderService.asmx";
var tblname = "#tableData";
var tblDetail = "#tblItemList";
var tableDetail = "#tableDetail";
var table, table2, table3;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _ID = "#txtID";
var _Date = "#txtDate";
var _Customer = "#ddlCustomer";
var _Currency = "#ddlCurrency";
var _txtCurrency = "#txtCurrency";
var _Location = "#ddlLocation";
var _ShipTo = "#txtShipTo";
var _SalesPerson = "#ddlSalesPerson";
var _BillTo = "#txtBillTo";
var _SOType = "#ddlSOType";
var _Terms = "#ddlTerms";
var _Remarks = "#txtRemarks";
var _Billings = "#txtBillings";
var _Status = "#ddlStatus";
var _StatusDate = "#txtStatusDate";
var _StatusRemarks = "#txtStatusRemarks";
var _Charges = "#txtCharges";
var _GST = "#txtGST";

//Item Modal
var _Model = "#txtModel";
var _Part = "#txtPart";
var _DefaultDescription = "#txtDefaultDescription";
var _AmendmentsDescription = "#txtAmendmentsDescription";
var _Quantity = "#txtQuantity";
var _Unit = "#txtUnit";
var _BDPrice = "#txtBDPrice";
var _Discount = "#txtDiscount";
var _UnitPrice = "#txtUnitPrice";
var _Amount = "#txtAmount";
var _ItemRemarks = "#txtItemRemarks";
var _Stock = "#txtStock";
var _IAmount = "#txtIAmount";
var _IGST = "#txtIGST";
var _IGSTAmount = "#txtIGSTAmount";
var _ICharges = "#txtICharges";
var _ITotalAmount = "#txtITotalAmount";

var _MID = "#txtMID";
var loginUrl = "/Default.aspx";


$(document).ready(function () {
    OnLoadGrid();
    $(_StatusDate).datepicker({ autoclose: true });
    $(_Date).datepicker({ autoclose: true });
    FillSalesPerson();
    FillLocation();
    FillCurrency();
    FillTerms();
    FillCustomer();
    FillSOType();
    OnCustomerChange();
    FillStatus();
    OnLoadGrid2();
    OnSelectDetail();
    //OnDelete();
    OnDeleteDetail();
    OnEdit();
    OnView();
    CalculateAmount();
    OnEditDetail();
});
var myVar = setInterval(myTimer, 1000);
function myTimer() {
    var Amount = $(_IAmount).val() | 0;
    var GST = $(_IGST).val() | 0;
    var Charges = $(_ICharges).val() | 0;
    var total = (Amount * GST) / 100 | 0;
    $(_IGSTAmount).val(total);
    var gTotal = Charges + total + Amount | 0;
    $(_ITotalAmount).val(gTotal);
}
function CalculateAmount() {
    $("" + _BDPrice + "," + _Discount + "," + _Quantity + "").keyup(function () {
        GetTotalAmount()
    });
}

function GetTotalAmount() {
    var BDPrice = $(_BDPrice).val() | 0;
    var Discount = $(_Discount).val() | 0;
    var Quantity = $(_Quantity).val() | 0;


    var BeforeDiscount = (BDPrice * Discount) / 100 | 0;
    var Amount = BDPrice - BeforeDiscount | 0;
    $(_UnitPrice).val(Amount);

    var totalAmount = Amount * Quantity | 0;
    $(_Amount).val(totalAmount);
}

var ItemID;
function OnSelectDetail() {
    $(tblDetail).on('click', '.select', function () {
        var row = $(this).closest('tr');
        var rowIndex = table2.row(row).index();
        var rowData = table2.row(rowIndex).data();
        ItemID = rowData.ID;
        FindData(rowData.ID)
        $("#modalView2").modal('hide');
    });
}


function FindData(ID) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/FindData",
        data: JSON.stringify({ ID: ID }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Unit).val(data[0].Unit)
            $(_BDPrice).val(data[0].BDPrice)
            $(_Part).val(data[0].PartNumber)
            $(_Model).val(data[0].ModelNumber)
            $(_DefaultDescription).val(data[0].DefaultDescription)

            GetStock(ID)
        },
        error: OnErrorCall
    });
}

function GetStock(ID) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/GetStock",
        data: JSON.stringify({ ID: ID, Datee: SalesDate, LocationID: LocationID, CurrentID: MasterID }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            console.log(response.d)
            var data = JSON.parse(response.d);
            console.log(data)
            $(_Stock).val(response.d)
        },
        error: OnErrorCall
    });
}

var MasterID, LocationID, SalesDate;
function OnView() {
    $(tblname).on('click', '.view', function () {
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData);
        MasterID = rowData.ID;
        LocationID = rowData.FK_LocationID;
        SalesDate = rowData.SalesDate2;
        $(_ICharges).val(rowData.Charges)
        $(_IGST).val(rowData.Tax)

        //$(_CID).val(rowData.ID);
        //var javascriptDate = new Date(rowData.Dated);
        //javascriptDate = javascriptDate.getMonth() + 1 + "/" + javascriptDate.getDate() + "/" + javascriptDate.getFullYear();
        //$(_CDate).val(javascriptDate);
        //$(_CTitle).val(rowData.Title);
        OnLoadGrid3(rowData.ID);
        ClearDetailFields();
        $("#H2").text("Items");
        $("#modalView").modal();
    });
}

function OnLoadGrid3(ID) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGridDetail2",
        data: JSON.stringify({ ID: ID }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tableDetail).DataTable().destroy();
            LoadDataTableDetail2(response.d);
        },
        error: OnErrorCall
    });
}

function LoadDataTableDetail2(datasource) {
    table3 = $(tableDetail).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [
                        { data: "ID", sDefaultContent: "" },
                        { data: "ModelNumber", sDefaultContent: "" },
                        { data: "PartNumber", sDefaultContent: "" },
                        { data: "DefaultDesc", sDefaultContent: "" },
                        { data: "CurrentDesc", sDefaultContent: "" },
                        { data: "Quantity", sDefaultContent: "" },
                        { data: "Unit", sDefaultContent: "" },
                        { data: "UnitPrice", sDefaultContent: "" },
                        { data: "Remarks", sDefaultContent: "" },
                        { data: "BD_PRICE", sDefaultContent: "" },
                        { data: "DisCount_per", sDefaultContent: "" },
                        { data: "PriceAfterDiscount", sDefaultContent: "" },
                        { data: "Amount", sDefaultContent: "" },
                        { data: "FK_Item", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" class="del" title="Delete"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#" class="edit" title="Edit"><i class="fa fa-pencil" ></i></a>';
                                    var html = del + edit;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [

            { "targets": [2], "visible": false, "searchable": false },
            { "targets": [6], "visible": false, "searchable": false },
            { "targets": [8], "visible": false, "searchable": false },
            { "targets": [0], "visible": false, "searchable": false },
            { "targets": [13], "visible": false, "searchable": false },
            { "targets": [9], "visible": false, "searchable": false },
            { "targets": [3], "width": "25%" },

        ],
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };
            // Total over all pages
            total = api
                .column(12)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Total over this page
            pageTotal = api
                .column(10, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            //alert(total)
            // Update footer
            //$(api.column(12).footer()).html(
            //     total

            //);

            $("#txtIAmount").val(total);
        }
    });
}

function OnLoadGrid2() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGridDetail",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblDetail).DataTable().destroy();
            LoadDataTableDetail(response.d);
        },
        error: OnErrorCall
    });
}

function OnCustomerChange() {
    $(_Customer).on('change', function () {
        var id = $(_Customer).val();

        if (id != "") {
            $.ajax({
                type: "POST",
                url: "" + serviceurl + "/OnCustomerChange",
                data: JSON.stringify({ ID: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var data = JSON.parse(response.d);
                    $(_BillTo).val(data[0].Text)
                    $(_ShipTo).val(data[0].Text)

                },
                error: OnErrorCall
            });
        }
    });
}
function FillCurrency() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetCurrencyList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Currency).empty();
            $(_Currency).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Currency).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}
function FillTerms() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetTermsList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Terms).empty();
            $(_Terms).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Terms).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}
function FillCustomer() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetCustomerList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Customer).empty();
            $(_Customer).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Customer).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}

function FillStatus() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetStatusList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Status).empty();
            $(_Status).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Status).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}

function FillSOType() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetSOTypeList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_SOType).empty();
            $(_SOType).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_SOType).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}

function FillLocation() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetLocationList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Location).empty();
            $(_Location).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Location).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}
function FillSalesPerson() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetSalesPersonList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_SalesPerson).empty();
            $(_SalesPerson).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_SalesPerson).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}

function OnSave() {
    // FormValidaton();

    var dataSend = {
        ID: $(_ID).val(),
        SalesDate: $(_Date).val(),
        FK_CustomerID: $(_Customer).val(),
        BillTo: $(_BillTo).val(),
        ShipTo: $(_ShipTo).val(),
        OrderType: $(_SOType).val(),
        FK_EmployeeID: $(_SalesPerson).val(),
        FK_LocationID: $(_Location).val(),
        Remarks: $(_Remarks).val(),
        Billing: $(_Billings).val(),
        FK_Terms: $(_Terms).val(),
        FK_CurrencyID: $(_Currency).val(),
        ExchangeRate: $(_txtCurrency).val(),
        FK_StatusID: $(_Status).val(),
        StatusDate: $(_StatusDate).val(),
        StatusRemarks: $(_StatusRemarks).val(),
        Tax: $(_GST).val(),
        Charges: $(_Charges).val()
    }

    //if (msg == "") {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/OnSave",
        data: JSON.stringify({ model: dataSend }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.d == "sessionNotFound") {
                alert("sessionNotFound");
                window.location.href = loginUrl;
            } else {
                var data = JSON.parse(response.d);
                console.log(data);
                if (data.Message == "success") {
                    ClearFields();
                    $("#Validation").fadeIn(1000);
                    $("#Validation").text(success);
                    $("#Validation").fadeOut(5000);
                    OnLoadGrid();
                    $("#modalAdd").modal('hide');
                } else if (data.Message == "exist") {
                    $("#Validation").fadeIn(1000);
                    $("#Validation").text(exist);
                    $("#Validation").fadeOut(5000);
                    OnLoadGrid();
                    $("#modalAdd").modal('hide');
                } else if (data.Message == "update") {
                    $("#Validation").fadeIn(1000);
                    $("#Validation").text(update);
                    $("#Validation").fadeOut(5000);
                    OnLoadGrid();
                    $("#modalAdd").modal('hide');
                }
            }
        },
        error: OnErrorCall
    });
    //} else {

    //}
}

function FormValidaton() {
    msg = "";

    $("#rDate").addClass("hide");

    if ($(_Date).val() == "") {
        msg += "Date is required";
        $("#rDate").removeClass("hide");
    } else {
        $("#rDate").addClass("hide");
    }

    $("#rTitle").addClass("hide");

    if ($(_Title).val() == "") {
        msg += "Title is required";
        $("#rTitle").removeClass("hide");
    } else {
        $("#rTitle").addClass("hide");
    }

    $("#rReasons").addClass("hide");

    if ($(_Reasons).val() == "") {
        msg += "Reason is required";
        $("#rReasons").removeClass("hide");
    } else {
        $("#rReasons").addClass("hide");
    }

    $("#rStartDate").addClass("hide");

    if ($(_StartDate).val() == "") {
        msg += "Start Date is required";
        $("#rStartDate").removeClass("hide");
    } else {
        $("#rStartDate").addClass("hide");
    }

    $("#rEndDate").addClass("hide");

    if ($(_EndDate).val() == "") {
        msg += "End Date is required";
        $("#rEndDate").removeClass("hide");
    } else {
        $("#rEndDate").addClass("hide");
    }

    $("#rBudget").addClass("hide");

    if ($(_Budget).val() == "") {
        msg += "Budget is required";
        $("#rBudget").removeClass("hide");
    } else {
        $("#rBudget").addClass("hide");
    }
    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}


function ClearFields() {
    $(_ID).val("");
    $(_Date).val("");
    $(_StatusDate).val("");


    $(_Customer).val("-1");
    $(_BillTo).val("");
    $(_ShipTo).val("");
    $(_SOType).val("-1");
    $(_SalesPerson).val("-1");
    $(_Location).val("-1");
    $(_Remarks).val("");
    $(_Billings).val("");
    $(_Terms).val("-1");
    $(_Currency).val("-1");
    $(_txtCurrency).val("");
    $(_Status).val("-1");
    $(_StatusRemarks).val("");


    $(_GST).val("");
    $(_Charges).val("");

    $("#ValidationSummary").text("");
    $("#rDate").addClass("hide");
    $("#rCustomer").addClass("hide");
    $("#rShipTo").addClass("hide");
    $("#rBillTo").addClass("hide");
    $("#rLocation").addClass("hide");
    $("#rSalesPerson").addClass("hide");
    $("#rCurrency").addClass("hide");
    $("#rtxtCurrency").addClass("hide");
    $("#rSOType").addClass("hide");
    $("#rTerms").addClass("hide");
    $("#rRemarks").addClass("hide");
    $("#rBillings").addClass("hide");
    $("#rStatus").addClass("hide");
    $("#rStatusDate").addClass("hide");
    $("#rStatusRemarks").addClass("hide");
}

function OnLoadGrid() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblname).DataTable().destroy();
            LoadDataTable(response.d);
        },
        error: OnErrorCall
    });


}

function OnConfirmDelete() {
    Delete(DeleteID);
}

var DeleteID = "";
function OnDelete() {
    $(tblname).on('click', '.del', function () {
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData);
        DeleteID = rowData.ID;

        if (DeleteID != "") {
            $("#modalDelete").modal();
        }

    });
}

function OnEdit() {
    $(tblname).on('click', '.edit', function () {
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData);

        $(_ID).val(rowData.ID);
        var javascriptDate = new Date(rowData.SalesDate2);
        javascriptDate = javascriptDate.getMonth() + 1 + "/" + javascriptDate.getDate() + "/" + javascriptDate.getFullYear();
        $(_Date).val(javascriptDate);

        var javascriptDate2 = new Date(rowData.StatusDate);
        javascriptDate2 = javascriptDate2.getMonth() + 1 + "/" + javascriptDate2.getDate() + "/" + javascriptDate2.getFullYear();
        $(_StatusDate).val(javascriptDate2);


        $(_Customer).val(rowData.FK_CustomerID);
        $(_BillTo).val(rowData.BillTo);
        $(_ShipTo).val(rowData.ShipTo);
        $(_SOType).val(rowData.OrderType);
        $(_SalesPerson).val(rowData.FK_EmployeeID);
        $(_Location).val(rowData.FK_LocationID);
        $(_Remarks).val(rowData.Remarks);
        $(_Billings).val(rowData.Billing);
        $(_Terms).val(rowData.FK_Terms);
        $(_Currency).val(rowData.FK_CurrencyID);
        $(_txtCurrency).val(rowData.ExchangeRate);
        $(_Status).val(rowData.FK_StatusID);
        $(_StatusRemarks).val(rowData.StatusRemarks);

        $(_GST).val(rowData.Tax);
        $(_Charges).val(rowData.Charges);

        $("#formModalLabel").text("Edit");
        $("#modalAdd").modal();
        $(".label-floating").addClass("is-focused");
    });
}


function OnSearch() {
    $("#H4").text("Search Items");
    $("#modalView2").modal();
}

function Delete(id) {
    if (id != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDelete",
            data: JSON.stringify({ dataID: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(delt);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalDelete").modal('hide');

                    } else if (data.Message == "failed") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(failedmsg);
                        $("#Validation").fadeOut(5000);
                    } else if (data.Message == "use") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("It is used by another process, Please try again!");
                        $("#Validation").fadeOut(5000);
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}


function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [

                        { data: "ID", sDefaultContent: "" },
                        { data: "SalesOrder", sDefaultContent: "" },
                        { data: "Fname", sDefaultContent: "" },
                        { data: "SalesDate2", sDefaultContent: "" },
                        { data: "SalesPerson", sDefaultContent: "" },
                        { data: "Status", sDefaultContent: "" },
                        { data: "FK_CustomerID", sDefaultContent: "" },
                        { data: "BillTo", sDefaultContent: "" },
                        { data: "ShipTo", sDefaultContent: "" },
                        { data: "OrderType", sDefaultContent: "" },
                        { data: "OrderRef", sDefaultContent: "" },
                        { data: "OrderRefDate", sDefaultContent: "" },
                        { data: "FK_EmployeeID", sDefaultContent: "" },
                        { data: "FK_LocationID", sDefaultContent: "" },
                        { data: "Tax", sDefaultContent: "" },
                        { data: "Charges", sDefaultContent: "" },
                        { data: "Remarks", sDefaultContent: "" },
                        { data: "Billing", sDefaultContent: "" },
                        { data: "Assembly", sDefaultContent: "" },
                        { data: "Delivery", sDefaultContent: "" },
                        { data: "CompanyID", sDefaultContent: "" },
                        { data: "FK_Terms", sDefaultContent: "" },
                        { data: "FK_CurrencyID", sDefaultContent: "" },
                        { data: "ExchangeRate", sDefaultContent: "" },
                        { data: "FK_StatusID", sDefaultContent: "" },
                        { data: "StatusDate", sDefaultContent: "" },
                        { data: "StatusRemarks", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" class="del" title="Delete"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#" class="edit" title="Edit"><i class="fa fa-pencil" ></i></a>';
                                    var view = ' <a href="#" class="view" title="Items"><i class="fa fa-eye" ></i></a>';
                                    var html = del + edit + view;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [
            {
                "targets": [3],
                "type": "date",
                "render": function (data) {
                    if (data !== null) {
                        var javascriptDate = new Date(data);
                        console.log(data);
                        javascriptDate = javascriptDate.getMonth() + 1 + "/" + javascriptDate.getDate() + "/" + javascriptDate.getFullYear();
                        return javascriptDate;
                    } else {
                        return "";
                    }
                }
            },

            { "targets": [0], "visible": false },
            { "targets": [6], "visible": false },
            { "targets": [7], "visible": false },
            { "targets": [8], "visible": false },
            { "targets": [9], "visible": false },
            { "targets": [10], "visible": false },
            { "targets": [11], "visible": false },
            { "targets": [12], "visible": false },
            { "targets": [13], "visible": false },
            { "targets": [14], "visible": false },
            { "targets": [15], "visible": false },
            { "targets": [16], "visible": false },
            { "targets": [17], "visible": false },
            { "targets": [18], "visible": false },
            { "targets": [19], "visible": false },
            { "targets": [20], "visible": false },
            { "targets": [21], "visible": false },
            { "targets": [22], "visible": false },
            { "targets": [23], "visible": false },
            { "targets": [24], "visible": false },
            { "targets": [25], "visible": false },
            { "targets": [26], "visible": false }

        ]
    });
}

function OnAdd() {
        
    $(".label-floating").removeClass("is-focused");
    ClearFields();
    $("#formModalLabel").text("Add");
    $("#modalAdd").modal();
}


function OnErrorCall() {
    alert("Something went wrong");
}

function FillMember() {

    $.ajax({
        type: "POST",
        url: serviceurl + '/GetMemberList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Member).empty();
            $(_Member).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Member).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
            //$(_dependent).select2('val', index);

        }
        //failure: function (errMsg) {
        //    alert(errMsg);
        //}
    });

}

function FillMemberType() {

    $.ajax({
        type: "POST",
        url: serviceurl + '/GetMemberTypeList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_MemberType).empty();
            $(_MemberType).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_MemberType).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
            //$(_dependent).select2('val', index);

        }
        //failure: function (errMsg) {
        //    alert(errMsg);
        //}
    });

}

function OnSaveDetail() {
    //DetailFormValidaton();

    var dataSend = {
        ID: $(_MID).val(),
        FK_SalesOrder: MasterID,
        FK_Item: ItemID,
        DefaultDesc: $(_DefaultDescription).val(),
        CurrentDesc: $(_AmendmentsDescription).val(),
        Quantity: $(_Quantity).val(),
        UnitPrice: $(_UnitPrice).val(),
        Remarks: $(_ItemRemarks).val(),
        BD_PRICE: $(_BDPrice).val(),
        DisCount_per: $(_Discount).val()
    }

    //if (msg == "") {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/OnSaveDetail",
        data: JSON.stringify({ model: dataSend }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.d == "sessionNotFound") {
                alert("sessionNotFound");
                window.location.href = loginUrl;
            } else {
                var data = JSON.parse(response.d);
                console.log(data);
                if (data.Message == "success") {
                    ClearDetailFields();
                    OnLoadGrid3(MasterID);
                    $("#Validation2").fadeIn(1000);
                    $("#Validation2").text(success);
                    $("#Validation2").fadeOut(5000);
                } else if (data.Message == "exist") {
                    OnLoadGrid3(MasterID);
                    $("#Validation2").fadeIn(1000);
                    $("#Validation2").text(exist);
                    $("#Validation2").fadeOut(5000);
                } else if (data.Message == "update") {
                    ClearDetailFields();
                    OnLoadGrid3(MasterID);
                    $("#Validation2").fadeIn(1000);
                    $("#Validation2").text(update);
                    $("#Validation2").fadeOut(5000);
                }
            }
        },
        error: OnErrorCall
    });
    //} else {

    //}
}


function OnLoadGrid2() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGridDetail",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblDetail).DataTable().destroy();
            LoadDataTableDetail(response.d);
        },
        error: OnErrorCall
    });
}

function LoadDataTableDetail(datasource) {
    table2 = $(tblDetail).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [
                    {
                        data: null,
                        sDefaultContent: "",
                        "orderable": false,
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var select = ' <a href="#" class="select"><b>Select</b></a>';
                                var html = select;
                                return html;
                            }
                            return data;
                        }
                    },
                        { data: "ID", sDefaultContent: "" },
                        { data: "PartNumber", sDefaultContent: "" },
                        { data: "ModelNumber", sDefaultContent: "" },
                        { data: "ProductName", sDefaultContent: "" },

        ],
        "columnDefs": [
             { "targets": [1], "visible": false, "searchable": false },
        ]
    });
}


function OnEditDetail() {
    $(tableDetail).on('click', '.edit', function () {
        var row = $(this).closest('tr');
        var rowIndex = table3.row(row).index();
        var rowData = table3.row(rowIndex).data();
        console.log(rowData);

        $(_MID).val(rowData.ID);
        $(_Model).val(rowData.ModelNumber);
        $(_Part).val(rowData.PartNumber);
        $(_DefaultDescription).val(rowData.DefaultDesc);
        $(_AmendmentsDescription).val(rowData.CurrentDesc);
        $(_Unit).val(rowData.Unit);
        $(_Quantity).val(rowData.Quantity);
        $(_BDPrice).val(rowData.BD_PRICE);
        $(_Discount).val(rowData.DisCount_per);
        $(_UnitPrice).val(rowData.UnitPrice);
        $(_ItemRemarks).val(rowData.Remarks);
        ItemID = rowData.FK_Item;
        GetStock(rowData.FK_Item)

        var BDPrice = $(_BDPrice).val() | 0;
        var Discount = $(_Discount).val() | 0;
        var Quantity = $(_Quantity).val() | 0;


        var BeforeDiscount = (BDPrice * Discount) / 100 | 0;
        var Amount = BDPrice - BeforeDiscount | 0;
        $(_UnitPrice).val(Amount);

        var totalAmount = Amount * Quantity | 0;
        $(_Amount).val(totalAmount);
        ///$("#formModalEditMember").text("Edit Member");
        //$("#modalEditDetail").modal();
    });
}



var DeleteDetailID = ""
function OnDeleteDetail() {
    $(tableDetail).on('click', '.del', function () {
        var row = $(this).closest('tr');
        var rowIndex = table3.row(row).index();
        var rowData = table3.row(rowIndex).data();
        console.log(rowData);
        DeleteDetailID = rowData.ID;

        if (DeleteDetailID != "") {
            $("#modalDelete2").modal();
        }

    });
}

function OnConfirmDeleteDetail() {
    DeleteDetail(DeleteDetailID);
}
function DeleteDetail(id) {
    if (id != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDeleteDetail",
            data: JSON.stringify({ dataID: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        $("#Validation2").fadeIn(1000);
                        $("#Validation2").text(delt);
                        $("#Validation2").fadeOut(5000);
                        OnLoadGrid3(MasterID);
                        $("#modalDelete2").modal('hide');
                    } else if (data.Message == "failed") {
                        $("#Validation2").fadeIn(1000);
                        $("#Validation2").text(failedmsg);
                        $("#Validation2").fadeOut(5000);
                    } else if (data.Message == "use") {
                        $("#Validation2").fadeIn(1000);
                        $("#Validation2").text("It is used by another process, Please try again!");
                        $("#Validation2").fadeOut(5000);
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}
function DetailFormValidaton() {
    msg = "";

    $("#rMember").addClass("hide");

    if ($(_Member).val() == "-1") {
        msg += "Member is required";
        $("#rMember").removeClass("hide");
    } else {
        $("#rMember").addClass("hide");
    }


    $("#rMember2").addClass("hide");

    if ($(_Member2).val() == "-1") {
        msg += "Member is required";
        $("#rMember2").removeClass("hide");
    } else {
        $("#rMember2").addClass("hide");
    }


    $("#rMemberType").addClass("hide");

    if ($(_MemberType).val() == "-1") {
        msg += "Title is required";
        $("#rMemberType").removeClass("hide");
    } else {
        $("#rMemberType").addClass("hide");
    }


    if (msg == "") {
        $("#ValidationSummary2").text("");
    } else {
        $("#ValidationSummary2").text("Please fill required values.");
        alert("Please fill required values.");
    }
}
function ClearDetailFields() {
    $(_MID).val("");
    $(_Model).val("");
    $(_Part).val("");
    $(_DefaultDescription).val("");
    $(_AmendmentsDescription).val("");
    $(_Unit).val("");
    $(_Quantity).val("");
    $(_BDPrice).val("");
    $(_Discount).val("");
    $(_UnitPrice).val("");
    $(_ItemRemarks).val("");
    ItemID = "";
    $(_Stock).val("")
    $(_UnitPrice).val("");
    $(_Amount).val("");

    $("#Validation2").text("");
    $("#rPart").addClass("hide");
    $("#rDefaultDescription").addClass("hide");
    $("#rAmendmentsDescription").addClass("hide");
    $("#rUnit").addClass("hide");
    $("#rStock").addClass("hide");
    $("#rQuantity").addClass("hide");
    $("#rBDPrice").addClass("hide");
    $("#rDiscount").addClass("hide");
    $("#rAmount").addClass("hide");
    $("#rItemRemarks").addClass("hide");
}