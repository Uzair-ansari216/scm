﻿
var name_regex = /^[ A-Za-z0-9_$%^*=()@./#&+-:\u0600-\u06FF]*$/;

var spFound;
var valid = function () {
    spFound = false;
    var inputTags = document.getElementsByTagName('input');
    var checkboxCount = 0;
    for (var i = 0, length = inputTags.length; i < length; i++) {
        if (inputTags[i].type == 'text') {
            if (!inputTags[i].value.match(name_regex)) {
                spFound = true;
            }
        }
    }
    return spFound;
}
