﻿var serviceurl = "../../appServices/BrandService.asmx";
var commonService = "../../appServices/CommonService.asmx";
var tblname = "#tableCity";
var table;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _ID = "#txtID";
var _SegmentCode = "#txtcode";
var _SegmentName = "#txtbrand";

var loginUrl = "/Default.aspx";


$(document).ready(function () {

    $("body #brandForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #brandForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #brandForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })

    OnLoadGrid();
    bindAddEditBrand();
    OnDelete();
});

function OnLoadGrid() {
    //alert('5')
    BlockUI();
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            debugger
            let data = JSON.parse(response.d)
            if (!data.Status) {
                $("#Validation").fadeIn(1000);
                $("#Validation").text(data.Message);
                $("#Validation").fadeOut(5000);
            } else {
                $(tblname).DataTable().destroy();
                LoadDataTable(data.GenericList);
                UnblockUI();
            }
        },
        error: OnErrorCall
    });
}

function bindAddEditBrand() {
    $("body").on("click", "#btnAdd , .edit", function () {

        let id = $(this).attr("_recordId")
        clearAllMessages("brandForm")

        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("Brand", "brandForm")
        }
        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("Brand", "brandForm", service_DeveloperMode)

        //}

        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/AddEditBusinessSegment",
            data: JSON.stringify({ id: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        if (parseInt(id) > 0) {
                            $("#formModalLabel").text("Edit");
                            $(".label-floating").addClass("is-focused");
                        } else {
                            $("#formModalLabel").text("Add");
                            $(".label-floating").removeClass("is-focused");
                        }
                        $(_ID).val(data.GenericList.ID);
                        $(_SegmentCode).val(data.GenericList.SegmentCode);
                        $(_SegmentName).val(data.GenericList.SegmentName);

                        $("#modalAdd").modal();
                    }

                }
            },
            error: OnErrorCall,
            complete: function () {
                isValueExist(_SegmentCode, $(_SegmentCode).val())
                isValueExist(_SegmentName, $(_SegmentName).val())
            }
        });
    })
}

function OnSave() {

    var isValid = true

    $("body #brandForm input").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    $("body #brandForm select").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    if (isValid) {
        var dataSend = {
            ID: $(_ID).val(),
            SegmentCode: $(_SegmentCode).val(),
            SegmentName: $(_SegmentName).val()
        }

        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnSave",
            data: JSON.stringify({ model: dataSend }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        ClearFields();
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                        $("#modalAdd").modal('hide');
                    }
                    if (data.Message == "success") {
                        ClearFields();
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(success);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "exist") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(exist);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "update") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(update);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    }
                }
            },
            error: OnErrorCall
        });
    }

}


function FormValidaton() {
    msg = "";

    $("#rCode").addClass("hide");

    if ($(_SegmentCode).val() == "") {
        msg += "Code is required";
        $("#rCode").removeClass("hide");
    } else {
        $("#rCode").addClass("hide");
    }

    //if (!$(_SegmentCode).val().match(name_regex) || $(_SegmentCode).val().length == 0) {
    //    // alert("OK")
    //    msg += "Code is required";
    //    $("#rCode").removeClass("hide");
    //}
    //else {
    //    $("#rCode").addClass("hide");
    //}


    $("#rBrand").addClass("hide");

    if ($(_SegmentName).val() == "") {
        msg += "Brand is required";
        $("#rBrand").removeClass("hide");
    } else {
        $("#rBrand").addClass("hide");
    }


    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}

function ClearFields() {
    $(_ID).val("");
    $(_SegmentCode).val("");
    $(_SegmentName).val("");

    $("#ValidationSummary").text("");
    $("#rBrand").addClass("hide");
    $("#rCode").addClass("hide");
}

var DeleteID = "";
var DeleteName = ""
function OnDelete() {
    $(tblname).on('click', '.del', function () {
        let id = $(this).attr('_recordId');
        DeleteID = id;
        $.ajax({
            type: "POST",
            url: "" + commonService + "/isAuthenticatedForDelete",
            data: JSON.stringify({ form: "Business Segment" }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        $("#modalDelete").modal();
                    }
                }
            },
            error: OnErrorCall
        });

    });
}

function OnConfirmDelete() {
    Delete(DeleteID, DeleteName);
}

function Delete(id, Name) {
    if (id != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDelete",
            data: JSON.stringify({ dataID: id, dataName: Name }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                        $("#modalDelete").modal('hide');
                    }
                    if (data.Message == "success") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(delt);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalDelete").modal('hide');

                    } else if (data.Message == "failed") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(failedmsg);
                        $("#Validation").fadeOut(5000);
                    } else if (data.Message == "use") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("It is used by another process, Please try again!");
                        $("#Validation").fadeOut(5000);
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}


function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        dom: 'Bfrtip',
        buttons: [
             {
                 extend: 'print',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Brand',
                 text: '<i class="fa fa-print"></i>  Print'

             },
             {
                 extend: 'excelHtml5',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Brand',
                 text: '<i class="fa fa-file-excel"></i>  Excel',
             },
            {
                extend: 'pdf',
                footer: true,
                exportOptions: {
                    columns: [0, 1, 3]
                },
                title: 'Brand',
                text: '<i class="fa fa-file-pdf"></i>  PDF'
            },
        ],
        "aaData": datasource,
        "aoColumns": [

                        { data: "ID", sDefaultContent: "" },
                        { data: "SegmentCode", sDefaultContent: "" },
                        { data: "SegmentName", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" _recordId = ' + data.ID + ' class="del" title="del"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#" _recordId = ' + data.ID + ' class="edit" title="Edit"><i class="fas fa-pencil-alt" ></i></a>';
                                    var html = del + edit;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [
             { "targets": [0], "width": "3%" },
             { "targets": [1], "width": "25%" },
             { "targets": [2], "width": "25%" },
             { "targets": [3], "width": "3%" }
        ]
    });
}

function OnErrorCall() {
    alert("Something went wrong");
}
