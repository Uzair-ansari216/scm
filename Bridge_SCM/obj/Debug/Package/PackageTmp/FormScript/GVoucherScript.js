﻿var serviceurl = "../../appServices/GVoucherService.asmx";
var commonService = "../../appServices/CommonService.asmx";
var tblname = "#tableData";
var tblDetail = "#tblItemList";
var tableDetail = "#tableDetail";
var tblProject = "#tblProject";
var tblEmployee = "#tblEmployee";
var tblAccountCode = "#tblAccountCode"
var typee;
var table, table2, table3, table4, table5, table6;
var voucherDetailArray = new Array();
var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _ID = "#txtID";
var _Date = "#txtDate";
var _VoucherType = "#ddlVoucherType";
var _VoucherNo = "#txtVoucherNo";
var _Type = "#ddlType";
var _Name = "#ddlName";
var _InvoiceNo = "#txtInvoiceNo";
var _InvoiceDate = "#txtInvoiceDate";
var _Amount = "#txtAmount";
var _DueDate = "#txtDueDate";
var _Description = "#txtDescription";
var _BankAccountCode = "#txtBankAccountCode";
var _AccountName = "#txtAccountName";
var _Location2 = "#ddlLocation2";
var _Particulars2 = "#txtParticulars2";
var _Creditors = "#txtCreditors";
var _Cheque = "#txtCheque";
var _LoactionSeacch = "#ddlLocationSearch";
var _VoucherNum = "#txtVoucherNum";
//var _AccountCodeName = "#txtAccountCodeName";

//Modal
var _Account = "#txtAccount";
var _Location = "#ddlLocation";
var _Brand = "#ddlBrand";
var _Department = "#ddlDepartment";
var _Project = "#txtProject";
var _Employee = "#txtEmployee";
var _Particulars = "#txtParticulars";
var _Debit = "#txtDebit";
var _Credit = "#txtCredit";
var _VoucherDate = "#txtVoucherDate";

var ParticularsGeneral = "";

var _MID = "#txtMID";
var loginUrl = "/Default.aspx";


$(document).ready(function () {

    var service_DeveloperMode = $("#hdnDevMode").val()
    if (service_DeveloperMode == 'on') {
        addFormFields("G-Voucher", "gvoucherForm")
    }
    //var service_ToolTip = $("#hdnToolTip").val()
    //if (service_ToolTip == 'on') {

    setToolTip("G-Voucher", "gvoucherForm", service_DeveloperMode)

    //}

    $("body #gvoucherForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #gvoucherForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #gvoucherForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })

    $("body #paymentAndReceiptDetailForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #paymentAndReceiptDetailForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #paymentAndReceiptDetailForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })


    $("body #voucherSupportForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #voucherSupportForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #voucherSupportForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })


    $(".label-floating").addClass("is-focused");
    $.material.options.autofill = true;
    $.material.init();
    $('.modal').on('hidden.bs.modal', function (e) {
        if ($('.modal').hasClass('in')) {
            $('body').addClass('modal-open');
        }
    });
    $(_Date).val(GetToday())

    SPAccountCode();
    SPAccountName();
    GetLocation();
    //GetWorkingDate();
    FillBankAccount();
    FillLocation();
    DueDays();
    OnDebitCredit();
    OnLoadGrid();
    OnSelect();
    $(_VoucherNo).attr('readonly', 'readonly');
    $(_Date).datepicker({
        autoclose: true, changeMonth: true,
        changeYear: true, yearRange: "-90:+00"
    });
    $(_VoucherDate).datepicker({
        autoclose: true, changeMonth: true,
        changeYear: true, yearRange: "-90:+00"
    });
    OnLoadGridItems();
    FillVoucherType();
    FillType();
    FillDepartment();
    FillBrand();
    OnTypeChange();
    OnLoadGridAccountCode();
    OnLoadGridAccountCode2();
    OnLoadGridProject();
    OnLoadGridEmployee();
    OnSelectAccountCode();
    OnSelectProject();
    OnSelectEmployee();
    OnSelectAccountCode2();
    OnDeleteItem();
    OnEditItem();
    bindDeleteVoucher();
    setTimeout(function () {
        setLabel()
    }, 10000)

});



function AddNew() {
    ClearFields();
    //OnLoadGridItems()
    ClearDetailFields();
    //OnLoadGridItems2();
    voucherDetailArray = [];
    $("#btnsave").attr("disabled", false)
    //$.ajax({
    //    type: "POST",
    //    url: "" + serviceurl + "/clearItem",
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",
    //    success: function (response) {
    //    },
    //    error: OnErrorCall
    //});
}


function GetLocation() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/GetCurrentLocation",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var data = response.d;
            var data2 = data.replace(/^"(.*)"$/, '$1');
            $(_LoactionSeacch).val(data2)
            $(_Location).val(data2)
            $(_Location2).val(data2)
        },
        error: OnErrorCall
    });
}

function SearchFields() {
    var dataSend = {
        VoucherNo: $(_VoucherNum).val(),
        VoucherDate: $(_VoucherDate).val(),
        LocationID: $(_LoactionSeacch).val(),
        VoucherType: $("#voucherType").val(),

    }
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGridSearch",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ model: dataSend }),
        dataType: "json",
        success: function (response) {
            $(tblname).DataTable().destroy();
            LoadDataTable(response.d);
        },
        error: OnErrorCall
    });
}

function GetWorkingDate() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/FillWorkingDate",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            var data = response.d;
            var data2 = data.replace(/^"(.*)"$/, '$1');

            var javascriptDate2 = new Date(data2);
            javascriptDate2 = javascriptDate2.getMonth() + 1 + "/" + javascriptDate2.getDate() + "/" + javascriptDate2.getFullYear();
            $(_VoucherDate).val(javascriptDate2)
        },
        error: OnErrorCall
    });
}

//myVar = setInterval(check, 300)

//function check() {

//    var debit = $("#txtDebitAmount").val() | 0;
//    var credit = creditAmount;

//    var diff = debit - credit | 0;

//    console.log(diff)
//    $(_Creditors).val(diff)

//    var total = credit + diff | 0;

//    $("#txtCreditAmount").val(total)
//}

function FillBankAccount() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/FillBankAccount",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ type: typee }),
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d)
            $(_BankAccountCode).val(data[0].BankAccountCode)
            $(_AccountName).val(data[0].AccountName)
        },
        error: OnErrorCall
    });
}

function FillType() {
    $(_Type).empty();
    $(_Type).append("<option value='-1'>--Please Select--</option>");
    $(_Type).append("<option value='Debtor'> Debtor </option>");
    $(_Type).append("<option value='Creditor'> Creditor </option>");
    $(_Type).append("<option value='Other'> Other </option>");
}
function DueDays() {
    $("#txtDueDays").keyup(function () {
        $(_DueDate).val(addDays($(_Date).val(), $("#txtDueDays").val()))
    });
}

function OnDebitCredit() {
    $(_Debit).keyup(function () {
        if ($(_Debit).val() > 0) {
            $(_Credit).val("0")
        }
    });

    $(_Credit).keyup(function () {
        if ($(_Credit).val() > 0) {
            $(_Debit).val("0")
        }
    });
}


function addDays(date, days) {
    var date = new Date(date);
    var newdate = new Date(date);
    var adddays = days | 0;
    newdate.setDate(newdate.getDate() + adddays);

    var dd = newdate.getDate();
    var mm = newdate.getMonth() + 1;
    var y = newdate.getFullYear();

    var someFormattedDate = mm + '/' + dd + '/' + y;

    return someFormattedDate
}

function GetToday() {
    let selectedWorkingDate = workingDate
    //var today = new Date();
    var dd = selectedWorkingDate.split('-')[2]//today.getDate();
    var mm = selectedWorkingDate.split('-')[1]//today.getMonth() + 1; //January is 0!

    var yyyy = selectedWorkingDate.split('-')[0]//today.getFullYear();
    //if (dd < 10) {
    //    dd = '0' + dd
    //}
    //if (mm < 10) {
    //    mm = '0' + mm
    //}
    var today = mm + '/' + dd + '/' + yyyy;

    return today;
}

var ProjectID, EmployeeID;
function OnSelectProject() {
    $(tblProject).on('click', '.select', function () {
        var row = $(this).closest('tr');
        var rowIndex = table4.row(row).index();
        var rowData = table4.row(rowIndex).data();
        ProjectID = rowData.ProjectID
        $(_Project).val(rowData.ProjectTitle)
        $("#modalProject").modal('hide');
    });
}

function OnSelectEmployee() {
    $(tblEmployee).on('click', '.select', function () {
        var row = $(this).closest('tr');
        var rowIndex = table5.row(row).index();
        var rowData = table5.row(rowIndex).data();
        EmployeeID = rowData.EmployeeID
        $(_Employee).val(rowData.EmployeeName)
        $("#modalEmployee").modal('hide');
    });
}

var rowID;
function OnEditItem() {
    $(tableDetail).on('click', '.edit', function () {


        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("G-Voucher Detail", "paymentAndReceiptDetailForm")
        }
        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("G-Voucher Detail", "paymentAndReceiptDetailForm", service_DeveloperMode)

        //}
        let id = $(this).attr("_recordId")
        var row = $(this).closest('tr');
        var rowIndex = table3.row(row).index();
        var rowData = table3.row(rowIndex).data();
        console.log(rowData);
        //if (rowData.VoucherID == "") {
        //    alert("Sorry");
        //}
        //else {
        rowID = id//rowData.rowID;
        $(_MID).val(id);
        $(_Account).val(rowData.AccountCode);
        //$(_AccountCodeName).val(rowData.AccountCode + " - " + rowData.AccountName)
        $("#txtAccountName2").val(rowData.AccountName)
        $(_Location).val(rowData.LocationID);
        $(_Brand).val(rowData.SegmentID);
        $(_Department).val(rowData.DepartmentID);
        ProjectID = rowData.ProjectID
        $(_Project).val(rowData.ProjectName);
        $(_Employee).val(rowData.EmployeeName);
        EmployeeID = rowData.EmployeeID
        $(_Particulars).val(rowData.Particulars);
        $(_Debit).val(rowData.Debit);
        $(_Credit).val(rowData.Credit);
        $("#formModalLabel").text("Edit");
        $("#modalAdd").modal();
        $(".label-floating").addClass("is-focused");

        //}
    });
}

function OnDeleteItem() {
    $(tableDetail).on('click', '.del', function () {
        let id = $(this).attr('_recordId')
        $.grep(voucherDetailArray, function (key, value) {
            if (key != undefined && key.rowID == id) {
                voucherDetailArray.splice(value, 1)
            }
        })

        LoadDataTableDetail2(voucherDetailArray);
        totalCalculation();
        //var row = $(this).parent().parent();
        //var rowIndex = table3.row(row).index();
        //var rowData = table3.row(rowIndex).data();
        //console.log(rowData);
        //if (rowData.ID == "") {
        //    DeleteItem(0, rowData.AccountCode, rowData.Particulars, rowData.LocationID, rowData.DepartmentID, rowData.EmployeeID, rowData.Debit, rowData.Credit);
        //}
        //else {
        //    DeleteItem(rowData.VoucherID, rowData.AccountCode, rowData.Particulars, rowData.LocationID, rowData.DepartmentID, rowData.EmployeeID, rowData.Debit, rowData.Credit);
        //}


    });
}

function DeleteItem(ID, AccountCode, Particulars, LocationID, DepartmentID, EmployeeID, Debit, Credit) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/OnDeleteItem",
        data: JSON.stringify({ ID: ID, AccountCode: AccountCode, Particulars: Particulars, LocationID: LocationID, DepartmentID: DepartmentID, EmployeeID: EmployeeID, Debit: Debit, Credit: Credit }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.d == "sessionNotFound") {
                //alert("sessionNotFound");
                //window.location.href = "<%= ResolveUrl("~/ErrorPage.aspx").ToString%>";
            } else {
                OnLoadGridItems2();
            }
            totalCalculation();
        },
        error: OnErrorCall
    });
}

function Show() {
    $(".label-floating").addClass("is-focused");
    GetWorkingDate();
    OnLoadGrid();
    $(_VoucherNum).val("")
    $("#H1").text("Voucher List");
    $("#modalShow").modal();
}



function OnLoadGridItems2() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGridItems2",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tableDetail).DataTable().destroy();
            LoadDataTableDetail2(response.d);
            totalCalculation()
        },
        error: OnErrorCall
    });
}
function OnSelect() {
    $(tblname).on('click', '.select', function () {
        debugger

        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();

        $(_ID).val(rowData.ID);
        var javascriptDate = new Date(rowData.VoucherDate);
        javascriptDate = javascriptDate.getMonth() + 1 + "/" + javascriptDate.getDate() + "/" + javascriptDate.getFullYear();
        $(_Date).prop("disabled", true).val(javascriptDate);
        //$('#txtDate').datepicker("option", "minDate", -1);
        //$('#txtDate').datepicker("option", "maxDate", -2);

        var javascriptDate2 = new Date(rowData.DueDate);
        javascriptDate2 = javascriptDate2.getMonth() + 1 + "/" + javascriptDate2.getDate() + "/" + javascriptDate2.getFullYear();
        $(_DueDate).val(javascriptDate2);


        $(_VoucherType).val(rowData.VoucherTypeID);
        $(_VoucherNo).val(rowData.VoucherNo);
        $(_Description).val(rowData.Description);
        $(_InvoiceNo).val(rowData.ReferenceNo);
        var javascriptDate3 = new Date(rowData.ReferenceDate);
        javascriptDate3 = javascriptDate3.getMonth() + 1 + "/" + javascriptDate3.getDate() + "/" + javascriptDate3.getFullYear();
        $(_InvoiceDate).val(javascriptDate3);

        FindData(rowData.ID)

        FindData2(rowData.ID)

        //OnLoadGrid3(rowData.ID);
        $("#modalShow").modal('hide');
    });
}


function OnLoadGridItems() {
    debugger
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGridItems",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tableDetail).DataTable().destroy();
            LoadDataTableDetail2(response.d);

        },
        error: OnErrorCall
    });
}



//////////////////

var ItemID;
function OnSelectAccountCode() {
    $(tblDetail).on('click', '.select', function () {
        var row = $(this).closest('tr');
        var rowIndex = table2.row(row).index();
        var rowData = table2.row(rowIndex).data();
        $(_Account).val(rowData.AccountCode)
        //$(_AccountCodeName).val(rowData.AccountCode + " - " + rowData.AccountName)
        $("#txtAccountName2").val(rowData.AccountName)
        $("#modalView2").modal('hide');
        $(".label-floating").addClass("is-focused");
    });
}

function OnSelectAccountCode2() {
    $(tblAccountCode).on('click', '.select', function () {
        var row = $(this).closest('tr');
        var rowIndex = table6.row(row).index();
        var rowData = table6.row(rowIndex).data();
        $(_BankAccountCode).val(rowData.AccountCode)
        $(_AccountName).val(rowData.AccountName)
        $("#modalViewAccountCode").modal('hide');
    });
}


function FindData(ID) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/FindData",
        data: JSON.stringify({ ID: ID }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            var data = JSON.parse(response.d);
            if (data[0].SupplierID != "") {
                $(_Type).val("Creditor")
                FillNameCreditor2(data[0].SupplierID)
            } else if (data[0].CustomerID != "") {
                $(_Type).val("Debtor")
                FillNameDebtor2(data[0].CustomerID)
            } else {
                $(_Type).val("Other")
                $(_Name).empty();
                $(_Name).append("<option value='-1'>--Please Select--</option>");
            }
        },
        error: OnErrorCall
    });
}



function OnLoadGrid3(ID) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGridDetail2",
        data: JSON.stringify({ ID: ID }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tableDetail).DataTable().destroy();
            LoadDataTableDetail2(response.d);
        },
        error: OnErrorCall
    });
}





function OnLoadGridAccountCode() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGridDetail",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblDetail).DataTable().destroy();
            LoadDataTableDetail(response.d);
        },
        error: OnErrorCall
    });
}

function OnLoadGridAccountCode2() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGridDetailAccountCode",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ type: typee }),
        dataType: "json",
        success: function (response) {
            $(tblAccountCode).DataTable().destroy();
            LoadDataTableDetailAccountCode(response.d);
        },
        error: OnErrorCall
    });
}

function LoadDataTableDetailAccountCode(datasource) {
    table6 = $(tblAccountCode).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [
                    {
                        data: null,
                        sDefaultContent: "",
                        "orderable": false,
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var select = ' <a href="#" class="select"><b>Select</b></a>';
                                var html = select;
                                return html;
                            }
                            return data;
                        }
                    },
                        { data: "AccountCode", sDefaultContent: "" },
                        { data: "AccountName", sDefaultContent: "" }

        ],
        "columnDefs": [
             //{ "targets": [1], "visible": false, "searchable": false },
        ]
    });
}

function OnTypeChange() {
    $(_Type).on('change', function () {
        var id = $(_Type).val();

        if (id == "Debtor") {
            FillNameDebtor()
        } else if (id == "Creditor") {
            FillNameCreditor()
        } else {
            $(_Name).empty();
            $(_Name).append("<option value='-1'>--Please Select--</option>");
        }
    });
}

function FillNameDebtor() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/FillNameDebtor",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Name).empty();
            $(_Name).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Name).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        },
        error: OnErrorCall
    });
}
function FillNameDebtor2(ID) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/FillNameDebtor",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Name).empty();
            $(_Name).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Name).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
            $(_Name).val(ID)
        },
        error: OnErrorCall
    });
}
function FillNameCreditor() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/FillNameCreditor",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Name).empty();
            $(_Name).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Name).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        },
        error: OnErrorCall
    });
}
function FillNameCreditor2(ID) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/FillNameCreditor",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Name).empty();
            $(_Name).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Name).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });

            $(_Name).val(ID)
        },
        error: OnErrorCall
    });
}
function FillDepartment() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetDepartmentList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Department).empty();
            $(_Department).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Department).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}
function FillBrand() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetBrandList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Brand).empty();
            $(_Brand).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Brand).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}


function FillLocation() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetLocationList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Location).empty();
            $(_Location).append("<option value='-1'>--Please Select--</option>");
            $(_Location2).empty();
            $(_Location2).append("<option value='-1'>--Please Select--</option>");
            $(_LoactionSeacch).empty();
            $(_LoactionSeacch).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Location).append("<option value=" + item.Value + "> " + item.Text + " </option>");
                $(_Location2).append("<option value=" + item.Value + "> " + item.Text + " </option>");
                $(_LoactionSeacch).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
            GetLocation();
        }
    });
}
function FillVoucherType() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetVoucherTypeList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_VoucherType).empty();
            $(_VoucherType).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_VoucherType).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}


function FormValidaton() {
    msg = "";

    $("#rDate").addClass("hide");

    if ($(_Date).val() == "") {
        msg += "Date is required";
        $("#rDate").removeClass("hide");
    } else {
        $("#rDate").addClass("hide");
    }

    $("#rTitle").addClass("hide");

    if ($(_Title).val() == "") {
        msg += "Title is required";
        $("#rTitle").removeClass("hide");
    } else {
        $("#rTitle").addClass("hide");
    }

    $("#rReasons").addClass("hide");

    if ($(_Reasons).val() == "") {
        msg += "Reason is required";
        $("#rReasons").removeClass("hide");
    } else {
        $("#rReasons").addClass("hide");
    }

    $("#rStartDate").addClass("hide");

    if ($(_StartDate).val() == "") {
        msg += "Start Date is required";
        $("#rStartDate").removeClass("hide");
    } else {
        $("#rStartDate").addClass("hide");
    }

    $("#rEndDate").addClass("hide");

    if ($(_EndDate).val() == "") {
        msg += "End Date is required";
        $("#rEndDate").removeClass("hide");
    } else {
        $("#rEndDate").addClass("hide");
    }

    $("#rBudget").addClass("hide");

    if ($(_Budget).val() == "") {
        msg += "Budget is required";
        $("#rBudget").removeClass("hide");
    } else {
        $("#rBudget").addClass("hide");
    }
    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}


function ClearFields() {
    $(_ID).val("")
    $(_Type).val("-1")
    $(_Date).val(GetToday())
    $(_Date).prop("disabled", false)
    //$(_Date).datepicker("option", "minDate", null);
    //$(_Date).datepicker("option", "maxDate", null);
    $(_Name).val("-1")
    $(_Description).val("")
    $(_Particulars2).val("")
    $(_Creditors).val("")
    $(_Cheque).val("")
    $(_Description).val("")
    GetLocation()//$(_Location2).val("-1")
    $("#txtVoucherNo").val("")

    FillBankAccount()
    $("#ValidationSummary").text("");
    $("#rDate").addClass("hide");
    $("#txtBankAccountCode").val("")
    $("#txtAccountName").val("")
    $("#rVoucherType").addClass("hide");
    $("#rVoucherNo").addClass("hide");
    $("#rType").addClass("hide");
    $("#rName").addClass("hide");
    $("#rInvoiceNo").addClass("hide");
    $("#rInvoiceDate").addClass("hide");
    $("#rDueDays").addClass("hide");
    $("#rDescription").addClass("hide");
    $("#tableDetail tbody").html("")
    $("#txtDebitAmount , #txtCreditAmount").val("")
}

function OnLoadGrid() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ type: typee }),
        dataType: "json",
        success: function (response) {
            $(tblname).DataTable().destroy();
            LoadDataTable(response.d);
        },
        error: OnErrorCall
    });


}


function OnSearch() {
    $("#H4").text("Account Name / Code");
    $($($("#tblItemList_wrapper .row")[0]).find("div")[0]).html("<div class='' style='margin-top: 1em !important;'><i class='fas fa-eraser btn-primary btn-sm' id='clear' title='Clear Account Code' onclick='clearAccountCode()'></i></div>")
    $("#modalView2").modal();
}

function OnSearchProject() {
    $("#H2").text("Project");
    $($($("#tblProject_wrapper .row")[0]).find("div")[0]).html("<div class='' style='margin-top: 1em !important;'><i class='fas fa-eraser btn-primary btn-sm' id='clear' title='Clear Project' onclick='clearProject()'></i></div>")
    $("#modalProject").modal();
}

function OnSearchEmployee() {
    $("#H5").text("Employee");
    $($($("#tblEmployee_wrapper .row")[0]).find("div")[0]).html("<div class='' style='margin-top: 1em !important;'><i class='fas fa-eraser btn-primary btn-sm' id='clear' title='Clear Employee' onclick='clearEmployee()'></i></div>")
    $("#modalEmployee").modal();
}

function clearProject() {

    if ($("#txtProject").val() != "") {
        $("#txtProject").val("")
        $("#modalProject").modal('hide');
    }
}

function clearAccountCode() {
    if ($("#txtAccountName2").val() != "") {
        $("#txtAccountName2").val("")
        $("#modalView2").modal('hide');
    }
}

function clearEmployee() {

    if ($("#txtEmployee").val() != "") {
        $("#txtEmployee").val("")
        $("#modalEmployee").modal('hide');
    }
}

function OnSearchBankAccount() {
    $("#H3").text("Account Name / Code");
    $("#modalViewAccountCode").modal();
}



function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [

                        { data: "ID", sDefaultContent: "" },
                        { data: "VoucherNo", sDefaultContent: "" },
                        { data: "VoucherTypeID", sDefaultContent: "", title: "Voucher Type" },
                        { data: "VoucherDate", sDefaultContent: "" },
                        { data: "PayToOrReceivedFrom", sDefaultContent: "" },
                        { data: "Description", sDefaultContent: "" },
                        { data: "ReferenceNo", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    var select = ' <a href="#" class="select"><b>Select</b></a>';
                                    var html = select;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [
            {
                "targets": [3],
                "type": "date",
                "render": function (data) {
                    if (data !== null && data != "") {
                        var javascriptDate = new Date(data);
                        console.log(data);
                        javascriptDate = javascriptDate.getMonth() + 1 + "/" + javascriptDate.getDate() + "/" + javascriptDate.getFullYear();
                        return javascriptDate;
                    } else {
                        return "";
                    }
                }
            },
            {
                "targets": [4],
                "type": "date",
                "render": function (data) {
                    if (data !== null && data != "") {
                        var javascriptDate = new Date(data);
                        console.log(data);
                        javascriptDate = javascriptDate.getMonth() + 1 + "/" + javascriptDate.getDate() + "/" + javascriptDate.getFullYear();
                        return javascriptDate;
                    } else {
                        return "";
                    }
                }
            },
            { "targets": [0], "visible": false },
            { "targets": [1], "visible": true },
            { "targets": [5], "visible": true },
            { "targets": [4], "visible": false },
            { "targets": [6], "visible": false },
        ]
    });
}

function OnAdd() {
    //$(".label-floating").addClass("is-focused");
    //ClearFields();
    ClearDetailFields();
    $("#formModalLabel").text("Add");
    $("#modalAdd").modal();

    var service_DeveloperMode = $("#hdnDevMode").val()
    if (service_DeveloperMode == 'on') {
        addFormFields("G-Voucher Detail", "paymentAndReceiptDetailForm")
    }
    //var service_ToolTip = $("#hdnToolTip").val()
    //if (service_ToolTip == 'on') {

    setToolTip("G-Voucher Detail", "paymentAndReceiptDetailForm", service_DeveloperMode)

    //}
}


function OnErrorCall() {
    alert("Something went wrong");
}

function FillMember() {

    $.ajax({
        type: "POST",
        url: serviceurl + '/GetMemberList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Member).empty();
            $(_Member).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Member).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
            //$(_dependent).select2('val', index);

        }
        //failure: function (errMsg) {
        //    alert(errMsg);
        //}
    });

}

function FillMemberType() {

    $.ajax({
        type: "POST",
        url: serviceurl + '/GetMemberTypeList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_MemberType).empty();
            $(_MemberType).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_MemberType).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
            //$(_dependent).select2('val', index);

        }
        //failure: function (errMsg) {
        //    alert(errMsg);
        //}
    });

}







function OnLoadGridProject() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGridProject",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblProject).DataTable().destroy();
            LoadDataTableProject(response.d);
        },
        error: OnErrorCall
    });
}

function OnLoadGridEmployee() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGridEmployee",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblEmployee).DataTable().destroy();
            LoadDataTableEmployee(response.d);
        },
        error: OnErrorCall
    });
}

function LoadDataTableEmployee(datasource) {
    table5 = $(tblEmployee).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [
                    {
                        data: null,
                        sDefaultContent: "",
                        "orderable": false,
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var select = ' <a href="#" class="select"><b>Select</b></a>';
                                var html = select;
                                return html;
                            }
                            return data;
                        }
                    },
                        { data: "EmployeeID", sDefaultContent: "" },
                        { data: "EmployeeName", sDefaultContent: "" }

        ],
        "columnDefs": [
             //{ "targets": [1], "visible": false, "searchable": false },
        ]
    });
}
function LoadDataTableProject(datasource) {
    table4 = $(tblProject).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [
                    {
                        data: null,
                        sDefaultContent: "",
                        "orderable": false,
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var select = ' <a href="#" class="select"><b>Select</b></a>';
                                var html = select;
                                return html;
                            }
                            return data;
                        }
                    },
                        { data: "ProjectID", sDefaultContent: "" },
                        { data: "ProjectTitle", sDefaultContent: "" }

        ],
        "columnDefs": [
             //{ "targets": [1], "visible": false, "searchable": false },
        ]
    });
}

function LoadDataTableDetail(datasource) {
    table2 = $(tblDetail).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [
                    {
                        data: null,
                        sDefaultContent: "",
                        "orderable": false,
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var select = ' <a href="#" class="select"><b>Select</b></a>';
                                var html = select;
                                return html;
                            }
                            return data;
                        }
                    },
                        { data: "AccountCode", sDefaultContent: "" },
                        { data: "AccountName", sDefaultContent: "" }

        ],
        "columnDefs": [
             //{ "targets": [1], "visible": false, "searchable": false },
        ]
    });
}


function DetailFormValidaton() {
    msg = "";

    $("#rParticulars").addClass("hide");

    if ($(_Particulars).val() == "") {
        msg += "Particulars is required";
        $("#rParticulars").removeClass("hide");
    } else {
        $("#rParticulars").addClass("hide");
    }


    $("#rLocation").addClass("hide");

    if ($(_Location).val() == "-1") {
        msg += "Location is required";
        $("#rLocation").removeClass("hide");
    } else {
        $("#rLocation").addClass("hide");
    }


    $("#rDepartment").addClass("hide");

    if ($(_Department).val() == "-1") {
        msg += "Department is required";
        $("#rDepartment").removeClass("hide");
    } else {
        $("#rDepartment").addClass("hide");
    }

    $("#rBrand").addClass("hide");

    if ($(_Brand).val() == "-1") {
        msg += "Brand is required";
        $("#rBrand").removeClass("hide");
    } else {
        $("#rBrand").addClass("hide");
    }


    if (msg == "") {
        $("#Span2").text("");
    } else {
        $("#Span2").text("Please fill required values.");
        alert("Please fill required values.");
    }
}
function ClearDetailFields() {
    $(_MID).val("")
    $(_Account).val("")
    //$(_AccountCodeName).val("")
    rowID = ""
    $("#txtAccountName2").val("")
    $(_Particulars).val("")
    //$(_Location).val("-1")
    $(_Department).val("-1")
    $(_Project).val("")
    ProjectID = ""
    EmployeeID = ""
    $(_Employee).val("")
    $(_Debit).val("")
    $(_Credit).val("")
    $(_Brand).val("-1")

    $("#Validation2").text("");
    $("#rLocation").addClass("hide");
    $("#rBrand").addClass("hide");
    $("#rDepartment").addClass("hide");
    $("#rParticulars").addClass("hide");
    $("#rDebit").addClass("hide");
    $("#rCredit").addClass("hide");
}

function OnPaste() {
    if (ParticularsGeneral == "") {
        ParticularsGeneral = $(_Particulars2).val();
        $(_Particulars).val(ParticularsGeneral)
    } else {
        $(_Particulars).val(ParticularsGeneral)
    }
}

function OnPaste2() {
    $(_Particulars2).val(ParticularsGeneral)
}


function SPAccountCode() {
    $(_Account).blur(function () {
        if ($(_Account).val() != "") {
            GetAccountName($(_Account).val())
        }
    });
}

function GetAccountName(AccountCode) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/GetAccountName",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ AccountCode: AccountCode }),
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d)
            $("#txtAccountName2").val(data[0].AccountName)
            $(_Account).val(data[0].AccountCode)
        },
        error: OnErrorCall
    });
}


function SPAccountName() {
    $("#txtAccountName2").blur(function () {
        if ($("#txtAccountName2").val() != "") {
            GetAccountCode($("#txtAccountName2").val())
        }
    });
}

function GetAccountCode(AccountName) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/GetAccountCode",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ AccountName: AccountName }),
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d)
            $("#txtAccountName2").val(data[0].AccountName)
            $(_Account).val(data[0].AccountCode)
        },
        error: OnErrorCall
    });
}

function setLabel() {
    debugger
    let type = voucherType
    if (type == "BP" || typee == "CP") {
        $("#txtCreditors").parent().find('label').text("Credit")
        $("#txtCheque").parent().find('label').text("Cheque #")
    } if (type == "BR" || typee == "CR") {
        $("#txtCreditors").parent().find('label').text("Debit")
        $("#txtCheque").parent().find('label').text("Deposit Slip #")
    }

}



function OnSaveVoucherSupport() {
    var isValid = true

    $("body #voucherSupportForm input").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    $("body #voucherSupportForm select").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    if (isValid) {
        var formData = {
            Id: $("#txtSID").val(),
            VoucherId: $("#txtvoucherid").val(),
            AccountCode: $("#txtaccountcode").val(),
            Currency: $("#ddlcurrency").val(),
            Rate: $("#txtcurrencyrate").val(),
            OrderId: $("#txtorderno").val(),
            OrderQuantity: $("#txtquantity").val(),
            OrderRate: $("#txtcurrencyrate").val(),
            ExpenseDate: $("#txtexmonthdate").val(),
            InvoiceReceived: $("#txtinvoicereceiveddate").val(),
            ExpenseCurrency: $("#ddlexcurrency").val(),

        }
        $.ajax({
            type: "POST",
            url: serviceurl + "/saveVoucherSupport",
            data: JSON.stringify({ supportVm: formData }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var data = JSON.parse(response.d);
                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Item1 == "success") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("Voucher support save successfully");
                        $("#Validation").fadeOut(5000);
                        //$("#txtfromdate , #txttodate, #txtexchangerate").val("")
                        //$("#ddlcurrency").find("option:first").prop("selected", true)
                        $("#vouchersupportmodal").modal('hide');
                    } else if (data.Item1 == "exist") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(exist);
                        $("#Validation").fadeOut(5000);
                        $("#vouchersupportmodal").modal('hide');
                    } else if (data.Item1 == "update") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(update);
                        $("#Validation").fadeOut(5000);
                        $("#vouchersupportmodal").modal('hide');
                    }
                }
            },
            error: function (response) {

            }
        });
    }
}


let generalVoucherId;
function bindDeleteVoucher() {
    $("body").on("click", "#btndelete", function () {
        var VoucherID = $("#txtID").val();
        if (VoucherID == "") {
            alert("ID is required")
        }
        else {
            $.ajax({
                type: "POST",
                url: "" + commonService + "/isAuthenticatedForDelete",
                data: JSON.stringify({ form: voucherform }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    if (response.d == "sessionNotFound") {
                        alert("sessionNotFound");
                        window.location.href = loginUrl;
                    } else {
                        var data = JSON.parse(response.d);
                        console.log(data);
                        if (!data.Status) {
                            $("#Validation").fadeIn(1000);
                            $("#Validation").text(data.Message);
                            $("#Validation").fadeOut(5000);
                        } else {
                            generalVoucherId = VoucherID
                            $('#voucherdeletemodal').modal();
                        }
                    }
                },
                error: OnErrorCall
            });
        }
    })
}

function OnConfirmDelete(generalVoucherId) {
    $("#txtusername , #txtpassword").val("")
    $("#validusermodal").modal();
}

$("body").on("click", "#btncheck", function () {
    let username = $("#txtusername").val()
    let password = $("#txtpassword").val()
    if (username != "" && password != "") {
        $.ajax({
            type: "POST",
            url: "" + commonService + "/isValidUserForDelete",
            data: JSON.stringify({ username: username, password: password }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#invalidusermsg").fadeIn(1000);
                        $("#invalidusermsg").text(data.Message);
                        $("#invalidusermsg").fadeOut(5000);
                    } else {
                        deleteVoucher(generalVoucherId)
                        $('#voucherdeletemodal').modal();
                    }
                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Username and password is required")
    }

})

function deleteVoucher(generalVoucherId) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/deleteGVoucher",
        data: JSON.stringify({ id: generalVoucherId }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            if (response.d == "sessionNotFound") {
                alert("sessionNotFound");
                window.location.href = loginUrl;
            } else {
                var data = JSON.parse(response.d);
                $("#validusermodal").modal('hide');
                $('#voucherdeletemodal').modal('hide');
                $("#Validation").fadeIn(1000);
                $("#Validation").text("Voucher deleted successfully");
                $("#Validation").fadeOut(5000);
                AddNew();
            }
        },
        error: OnErrorCall
    });
}