﻿var serviceurl = "../../appServices/UserActivityService.asmx";
var tblname = "#tableCity";
var table;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _FromDate = "#txtFromDate";
var _ToDate = "#txtToDate";
var _Users = "#ddlUser";

var loginUrl = "/Default.aspx";


$(document).ready(function () {
    FillUser();
    OnLoadGrid2();
    $(_ToDate).datepicker({ autoclose: true });
    $(_FromDate).datepicker({ autoclose: true });

    $("#ddlUser").find("option:selected").trigger('change')
});

function OnLoadGrid2() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid2",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblname).DataTable().destroy();
            LoadDataTable(response.d);
        },
        error: OnErrorCall
    });
}

function FillUser() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetUsersList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Users).empty();
            $(_Users).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Users).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}

function OnLoadGrid(FromDate, ToDate, User) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        data: JSON.stringify({ FromDate: $(_FromDate).val(), ToDate: $(_ToDate).val(), User: $(_Users).val() }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblname).DataTable().destroy();
            LoadDataTable(response.d);
        },
        error: OnErrorCall
    });


}


function OnErrorCall() {
    alert("Something went wrong");
}

function Find() {
    if ($(_FromDate).val() == "" || $(_ToDate).val() == "") {
        alert("Please Fill All Values")
    }
    else {
        OnLoadGrid($(_FromDate).val(), $(_ToDate).val(), $(_Users).val());
    }
}