﻿const serviceUrl = "../../appServices/AssetBookService.asmx";
const commonService = "../../appServices/CommonService.asmx";

$(function () {

    $("body #assetBookForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #assetBookForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #assetBookForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })

    bindGetAssetBook();
    bindAddEditAssetBook();
    bindSaveAssetBook();
    bindDeleteAssetBook();
})


function bindGetAssetBook() {
    $.ajax({
        url: serviceUrl + "/getAllAssetBook",
        type: "Post",
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            let data = JSON.parse(response.d)
            MakeDataGridForAssetBook(data)
        },
        error: function (response) {

        }
    })
}

function bindAddEditAssetBook() {
    $("body").on("click", "#btnAdd , .btnedit", function () {
        let id = $(this).attr("_recordid")

        clearAllMessages("assetBookForm")

        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("Asset Book", "assetBookForm")
        }
        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("Asset Book", "assetBookForm", service_DeveloperMode)

        //}

        $.ajax({
            type: "POST",
            url: serviceUrl + "/AddEditAssetBook",
            data: JSON.stringify({ id: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        if (parseInt(id) > 0) {
                            $("#formModalLabel").text("Edit");
                        } else {
                            $("#formModalLabel").text("Add");
                        }
                        $("#txtID").val(data.GenericList.Id);
                        $("#title").val(data.GenericList.Title);
                        $("#description").val(data.GenericList.Description);
                        data.GenericList.IsDefault == true ? $("#isdefault").attr("checked", true) : $("#isdefault").attr("checked", false)

                        $("#assetbookmodal").modal();
                    }
                }
            },
            error: function (response) {

            },
            complete: function () {
                isValueExist("#title", $("#title").val())
                isValueExist("#description", $("#description").val())
            }
        });
    })
}

function bindSaveAssetBook() {
    $("body").on("click", "#btnsave", function () {

        var isValid = true

        $("body #assetBookForm input").each(function (index, row) {
            if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
                if ($(row).val() == "") {
                    $(row).parent().find("span").eq(2).removeClass("hide")
                    isValid = false
                } else {
                    $(row).parent().find("span").eq(2).addClass("hide")
                }
            }
        })

        $("body #assetBookForm select").each(function (index, row) {
            if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
                if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                    $(row).parent().find("span").eq(2).removeClass("hide")
                    isValid = false
                } else {
                    $(row).parent().find("span").eq(2).addClass("hide")
                }
            }
        })

        if (isValid) {
            var formData = {
                Id: parseInt($("#txtID").val()),
                Title: $("#title").val(),
                Description: $("#description").val(),
                IsDefault: $("#isdefault:checked").length,
            }

            $.ajax({
                type: "POST",
                url: serviceUrl + "/saveAssetBook",
                data: JSON.stringify({ assetBookVm: formData }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d == "sessionNotFound") {
                        alert("sessionNotFound");
                        window.location.href = loginUrl;
                    } else {
                        var data = JSON.parse(response.d);

                        if (data.Status) {
                            $("#Validation").fadeIn(1000);
                            $("#Validation").text(data.Message);
                            $("#Validation").fadeOut(5000);
                            MakeDataGridForAssetBook(data.GenericList);
                            $("#assetbookmodal").modal('hide');
                        } else if (data.Message == "exist") {
                            $("#Validation").fadeIn(1000);
                            $("#Validation").text(exist);
                            $("#Validation").fadeOut(5000);
                            $("#assetbookmodal").modal('hide');
                        } else if (data.Message == "update") {
                            $("#Validation").fadeIn(1000);
                            $("#Validation").text(update);
                            $("#Validation").fadeOut(5000);
                            MakeDataGridForAssetBook();
                            $("#assetbookmodal").modal('hide');
                        }
                    }
                },
                error: function (response) {
                }
            });
        }
    })
}

var currentElement;
function bindDeleteAssetBook() {
    $("body").on("click", ".btndelete", function () {
        currentElement = $(this)

        $.ajax({
            type: "POST",
            url: "" + commonService + "/isAuthenticatedForDelete",
            data: JSON.stringify({ form: "Asset Book" }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        $("#assetbookdeletemodal").modal();
                    }
                }
            },
            error: function (response) {

            }
        });
    })
}

function deleteAssetBook() {
    $.ajax({
        type: "POST",
        url: serviceUrl + "/deleteAssetBook",
        data: JSON.stringify({ id: currentElement.attr("_recordId"), title: currentElement.attr("_title") }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            if (response.d == "sessionNotFound") {
                alert("sessionNotFound");
                window.location.href = loginUrl;
            } else {
                var data = JSON.parse(response.d);
                if (data.Status) {
                    $("#assetbookdeletemodal").modal('hide');
                    currentElement.closest('tr').remove()
                    $("#Validation").fadeIn(1000);
                    $("#Validation").text(data.Message);
                    $("#Validation").fadeOut(5000);
                } else if (data.Message == "failed") {
                    $("#Validation").fadeIn(1000);
                    $("#Validation").text(failedmsg);
                    $("#Validation").fadeOut(5000);
                } else if (data.Message == "use") {
                    $("#Validation").fadeIn(1000);
                    $("#Validation").text("It is used by another process, Please try again!");
                    $("#Validation").fadeOut(5000);
                }
            }
        },
        error: function (response) {

        }
    });
}

function MakeDataGridForAssetBook(datasource) {
    $("#assetbookcontainer").DataTable().destroy();

    $("#assetbookcontainer").DataTable({
        "searching": true,
        "bLengthChange": true,
        "scrollY": 400,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        dom: 'Bfrtip',
        buttons: [
             {
                 extend: 'print',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Asset Book',
                 text: '<i class="fa fa-lg fa-print"></i>  Print'

             },
             {
                 extend: 'excelHtml5',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Asset Book',
                 text: '<i class="fa fa-lg fa-file-excel"></i>  Excel',
             },
            {
                extend: 'pdf',
                footer: true,
                exportOptions: {
                    columns: [0, 1, 3]
                },
                title: 'Asset Book',
                text: '<i class="fa fa-lg fa-file-pdf"></i>  PDF'
            },
        ],
        "aaData": datasource,
        "aoColumns": [

                        { data: "Title", width: "20%" },
                        { data: "Description", width: "70%" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "width": "5%",
                            "orderable": false,
                            render: function (data, type, row) {
                                let checkbox = '<div class="col-sm-3 checkbox-radios"><div class="checkbox"><label><input id="' + data.Id + '" type="checkbox" name="selector" disabled><span class="checkbox-material"><span class="check"></span></span></label></div></div>'
                                return checkbox;
                            }
                        },
                         {
                             data: null,
                             width: "5%",
                             sDefaultContent: "",
                             "orderable": false,
                             render: function (data, type, row) {
                                 if (type === 'display') {
                                     var del = ' <a href="#" _recordId="' + data.Id + '" _title="' + data.Title + '" class="btndelete" title="Delete Asset Book"><i class="fas fa-trash"></i></a>';
                                     var edit = ' <a href="#" _recordId="' + data.Id + '" class="btnedit" title="Edit Asset Book"><i class="fas fa-pencil-alt" ></i></a>';
                                     var html = edit + " | " + del;
                                     return html;
                                 }
                                 return data;
                             }
                         },
        ], rowCallback: function (row, data) {
            data.IsDefault == true ? $(row).find("input").attr("checked", true) : $(row).find("input").attr("checked", false)

        }
    });
}