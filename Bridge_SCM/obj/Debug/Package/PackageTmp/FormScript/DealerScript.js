﻿var serviceurl = "../../appServices/DealerService.asmx";
var commonService = "../../appServices/CommonService.asmx";

var tblname = "#tableData";
var tblDetail = "#tableDetail";
var table, table2;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _ID = "#txtID";
var _DealerCode = "#txtDealerCode";
var _Company = "#txtCompany";
var _BillingName = "#txtBillingName";
var _Address = "#txtAddress";
var _Country = "#ddlCountry";
var _City = "#ddlCity";
var _Phone = "#txtPhone";
var _Fax = "#txtFax";
var _Email = "#txtEmail";
var _Login = "#txtLogin";
var _NT = "#txtNT";
var _ST = "#txtST";
var _ContactPerson1 = "#txtContactPerson1";
var _ContactInfo1 = "#txtContactInfo1";
var _Email1 = "#txtEmail1";

var _ContactPerson2 = "#txtContactPerson2";
var _ContactInfo2 = "#txtContactInfo2";
var _Email2 = "#txtEmail2";
var _Location = "#ddlLocation";
var _DealerType = "#ddlDealerType";
var _CreditLimit = "#txtCreditLimit";
var _DealerCategory = "#ddlDealerCategory";
var _DebtorCode = "#txtDebtorCode";

var _OpeningBalanceD = "#txtOpeningBalanceD";
var _CreditorCode = "#txtCreditorCode";
var _OpeningBalanceC = "#txtOpeningBalanceC";
var _Available = "#chkAvailable";
var _SalesPerson = "#ddlSalesPerson";
var _Pricing = "#ddlPricing";
var _CustomerGroup = "#ddlCustomerGroup";

var loginUrl = "/Default.aspx";


$(document).ready(function () {
    //$("#userimg").attr("src", "../../Layout/Images/user.jpg");

    $("body #customerForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #customerForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #customerForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })

    wizard();
    wizard2();
    OnLoadGrid();
    FillCategory();
    FillPricing();
    CustomerType();
    FillSalesPerson();
    FillCity();
    FillCountry();
    FillLocation()
    // OnLoadGrid2();
    FillCustomerGroup();
    //OnSelectDetail()
    bindAddEditCustomer();
    OnDelete();
    OnEdit();


});

function wizard() {
    //Add blue animated border and remove with condition when focus and blur
    if ($('.fg-line')[0]) {
        $('body').on('focus', '.form-control', function () {
            $(this).closest('.fg-line').addClass('fg-toggled');
        })

        $('body').on('blur', '.form-control', function () {
            var p = $(this).closest('.form-group');
            var i = p.find('.form-control').val();

            if (p.hasClass('fg-float')) {
                if (i.length == 0) {
                    $(this).closest('.fg-line').removeClass('fg-toggled');
                }
            }
            else {
                $(this).closest('.fg-line').removeClass('fg-toggled');
            }
        });
    }

    //Add blue border for pre-valued fg-flot text feilds
    if ($('.fg-float')[0]) {
        $('.fg-float .form-control').each(function () {
            var i = $(this).val();

            if (!i.length == 0) {
                $(this).closest('.fg-line').addClass('fg-toggled');
            }

        });
    }


    /*   Form Wizard Functions  */
    /*--------------------------*/
    _handleTabShow = function (tab, navigation, index, wizard) {
        var total = navigation.find('li').length;
        var current = index + 0;
        var percent = (current / (total - 1)) * 100;
        var percentWidth = 100 - (100 / total) + '%';
        navigation.find('li').removeClass('done');
        navigation.find('li.active').prevAll().addClass('done');

        wizard.find('.progress-bar').css({ width: percent + '%' });
        $('.form-wizard-horizontal').find('.progress').css({ 'width': percentWidth });
    };

    _updateHorizontalProgressBar = function (tab, navigation, index, wizard) {
        var total = navigation.find('li').length;
        var current = index + 0;
        var percent = (current / (total - 1)) * 100;
        var percentWidth = 100 - (100 / total) + '%';

        wizard.find('.progress-bar').css({ width: percent + '%' });
        wizard.find('.progress').css({ 'width': percentWidth });
    };

    /* Form Wizard - Example 1  */
    /*--------------------------*/
    $('#formwizard_simple').bootstrapWizard({
        onTabShow: function (tab, navigation, index) {
            _updateHorizontalProgressBar(tab, navigation, index, $('#formwizard_simple'));
        }
    });

    /* Form Wizard - Example 2  */
    /*--------------------------*/

    $('#formwizard_validation').bootstrapWizard({
        onNext: function (tab, navigation, index) {
            var form = $('#formwizard_validation').find("form");
            var valid = true;

            if (index == 1) {
                var fname = form.find('#firstname');
                var lastname = form.find('#lastname');

                if (!fname.val()) {
                    swal("You must enter your first name!");
                    fname.focus();
                    return false;
                }

                if (!lastname.val()) {
                    swal("You must enter your last name!");
                    lastname.focus();
                    return false;
                }
            }

            if (!valid) {
                return false;
            }
        },
        onTabShow: function (tab, navigation, index) {
            _updateHorizontalProgressBar(tab, navigation, index, $('#formwizard_validation'));
        }
    });

}

function wizard2() {
    // Code for the Validator


    // Wizard Initialization
    $('.wizard-card').bootstrapWizard({
        'tabClass': 'nav nav-pills',
        'nextSelector': '.btn-next',
        'previousSelector': '.btn-previous',

        onNext: function (tab, navigation, index) {
            var $valid = $('.wizard-card form').valid();
            if (!$valid) {
                $validator.focusInvalid();
                return false;
            }
        },

        onInit: function (tab, navigation, index) {

            //check number of tabs and fill the entire row
            var $total = navigation.find('li').length;
            var $width = 100 / $total;
            var $wizard = navigation.closest('.wizard-card');

            var $display_width = $(document).width();

            if ($display_width < 600 && $total > 3) {
                $width = 50;
            }

            navigation.find('li').css('width', $width + '%');
            var $first_li = navigation.find('li:first-child a').html();
            var $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
            $('.wizard-card .wizard-navigation').append($moving_div);
            refreshAnimation($wizard, index);
            $('.moving-tab').css('transition', 'transform 0s');
        },

        //onTabClick: function (tab, navigation, index) {
        //    var $valid = $('.wizard-card form').valid();

        //    if (!$valid) {
        //        return false;
        //    } else {
        //        return true;
        //    }
        //},

        onTabShow: function (tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index + 1;

            var $wizard = navigation.closest('.wizard-card');

            // If it's the last tab then hide the last button and show the finish instead
            if ($current >= $total) {
                $($wizard).find('.btn-next').hide();
                $($wizard).find('.btn-finish').show();
            } else {
                $($wizard).find('.btn-next').show();
                $($wizard).find('.btn-finish').hide();
            }

            var button_text = navigation.find('li:nth-child(' + $current + ') a').html();

            setTimeout(function () {
                $('.moving-tab').text(button_text);
            }, 150);

            var checkbox = $('.footer-checkbox');

            if (!index == 0) {
                $(checkbox).css({
                    'opacity': '0',
                    'visibility': 'hidden',
                    'position': 'absolute'
                });
            } else {
                $(checkbox).css({
                    'opacity': '1',
                    'visibility': 'visible'
                });
            }

            refreshAnimation($wizard, index);
        }
    });


    // Prepare the preview for profile picture
    $("#wizard-picture").change(function () {
        readURL(this);
    });

    $('[data-toggle="wizard-radio"]').on('click', function () {
        wizard = $(this).closest('.wizard-card');
        wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
        $(this).addClass('active');
        $(wizard).find('[type="radio"]').removeAttr('checked');
        $(this).find('[type="radio"]').attr('checked', 'true');
    });

    $('[data-toggle="wizard-checkbox"]').on('click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).find('[type="checkbox"]').removeAttr('checked');
        } else {
            $(this).addClass('active');
            $(this).find('[type="checkbox"]').attr('checked', 'true');
        }
    });

    $('.set-full-height').css('height', 'auto');

    //Function to show image before upload

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(window).resize(function () {
        $('.wizard-card').each(function () {
            $wizard = $(this);
            index = $wizard.bootstrapWizard('currentIndex');
            refreshAnimation($wizard, index);

            $('.moving-tab').css({
                'transition': 'transform 0s',
                'width': '186'
            });
        });
    });

    function refreshAnimation($wizard, index) {

        var total_steps = $wizard.find('li').length;
        var move_distance = $wizard.width() / total_steps;
        var step_width = move_distance;
        move_distance *= index;

        var $current = index + 1;

        if ($current == 1) {
            move_distance -= 8;
        } else if ($current == total_steps) {
            move_distance += 8;
        }

        $wizard.find('.moving-tab').css('width', step_width);
        $('.moving-tab').css({
            'transform': 'translate3d(' + move_distance + 'px, 0, 0)',
            'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

        });
    }

}

function OnLoadGrid() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblname).DataTable().destroy();
            LoadDataTable(response.d);
        },
        error: OnErrorCall
    });
}


function OnSelectDetail() {
    $(tblDetail).on('click', '.select', function () {
        var row = $(this).closest('tr');
        var rowIndex = table2.row(row).index();
        var rowData = table2.row(rowIndex).data();

        FindData(rowData.ID)
        $("#formModalLabel").text("Add");
        $("#modalAdd").modal();
        $("#modalView").modal('hide');
    });
}

function FindData(ID) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/FindData",
        data: JSON.stringify({ ID: ID }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_ID).val(data[0].ID)
            $(_Company).val(data[0].Company)
            $(_ContactPerson1).val(data[0].FirstName)
            $(_Address).val(data[0].Address)
            $(_DebtorCode).val(data[0].DebtorAccountCode)
            $(_CreditorCode).val(data[0].CreditorAccountCode)
            $(_Country).val(data[0].countryName)
            $(_City).val(data[0].cityName)
            $(_BillingName).val(data[0].CBillingName)
            $(_Location).val(data[0].Location)
            $(_Email).val(data[0].Email)
            $(_Phone).val(data[0].Phone)

            $(_Fax).val(data[0].Fax)
            $(_OpeningBalanceD).val(data[0].DebtorOpBal)
            $(_OpeningBalanceC).val(data[0].CreditorOpBal)
            $(_NT).val(data[0].NTN)
            $(_ST).val(data[0].STN)
            $(_ContactInfo1).val(data[0].CellNo1)
            $(_ContactInfo2).val(data[0].CellNo2)
            $(_Email1).val(data[0].Email1)
            $(_Email2).val(data[0].Email2)
        },
        error: OnErrorCall
    });
}

function OnSearch() {
    $("#H2").text("Search");
    $("#modalAdd").modal('hide');
    $("#modalView").modal();
}

function OnLoadGrid2() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGridDetail",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblDetail).DataTable().destroy();
            LoadDataTableDetail(response.d);
        },
        error: OnErrorCall
    });
}

function LoadDataTableDetail(datasource) {
    table2 = $(tblDetail).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [
                    {
                        data: null,
                        sDefaultContent: "",
                        "orderable": false,
                        render: function (data, type, row) {
                            if (type === 'display') {
                                //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                var select = ' <a href="#" class="select"><b>Select</b></a>';
                                var html = select;
                                return html;
                            }
                            return data;
                        }
                    },

                        { data: "ID", sDefaultContent: "" },
                        { data: "Company", sDefaultContent: "" },


        ],
        "columnDefs": [

            { "targets": [1], "visible": false, "searchable": false },


        ]
    });
}
function FillLocation() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetLocationList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Location).empty();
            $(_Location).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Location).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_Location, $(_Location).val())
        }
    });
}

function FillCity() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetCityList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_City).empty();
            $(_City).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_City).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_City, $(_City).val())
        }
    });
}

function FillCategory() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetCategoryList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_DealerCategory).empty();
            $(_DealerCategory).append("<option>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_DealerCategory).append("<option> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_DealerCategory, $(_DealerCategory).val())
        }
    });
}
function FillCountry() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetCountryList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Country).empty();
            $(_Country).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Country).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_Country, $(_Country).val())
        }
    });
}

function FillCustomerGroup() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetCustomerGroupList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_CustomerGroup).empty();
            $(_CustomerGroup).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_CustomerGroup).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_CustomerGroup, $(_CustomerGroup).val())
        }
    });
}

function FillPricing() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetPricingList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Pricing).empty();
            $(_Pricing).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Pricing).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_Pricing, $(_Pricing).val())
        }
    });
}

function CustomerType() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetCustomerTypeList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_DealerType).empty();
            $(_DealerType).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_DealerType).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_DealerType, $(_DealerType).val())
        }
    });
}

function OnClose() {
    console.log('Close')
    //$(_ContactPerson1).focus();
    // $('#modalAdd').focus();
}

function FillSalesPerson() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetSalesPersonList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_SalesPerson).empty();
            $(_SalesPerson).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_SalesPerson).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_SalesPerson, $(_SalesPerson).val())
        }
    });
}

function bindAddEditCustomer() {
    $("body").on("click", "#btnAdd , .edit", function () {

        let id = $(this).attr("_recordId")
        clearAllMessages("customerForm")

        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("Customer", "customerForm")
        }
        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("Customer", "customerForm", service_DeveloperMode)

        //}

        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/AddEditCustomer",
            data: JSON.stringify({ id: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        if (parseInt(id) > 0) {
                            $("#formModalLabel").text("Edit");
                            $(".label-floating").addClass("is-focused");
                        } else {
                            $("#formModalLabel").text("Add");
                            $(".label-floating").removeClass("is-focused");
                        }
                        $(_ID).val(data.GenericList.ID);
                        $(_Company).val(data.GenericList.Company);
                        $(_ContactPerson1).val(data.GenericList.FirstName);
                        $(_ContactPerson2).val(data.GenericList.LastName);
                        $(_Address).val(data.GenericList.Address);
                        $(_DebtorCode).val(data.GenericList.DebtorAccountCode);
                        $(_CreditorCode).val(data.GenericList.CreditorAccountCode);
                        $(_Country).val(data.GenericList.countryName);
                        $(_City).val(data.GenericList.cityName);
                        $(_Location).val(data.GenericList.Location);
                        $(_Phone).val(data.GenericList.Phone);
                        $(_Email).val(data.GenericList.Email);
                        $(_BillingName).val(data.GenericList.CBillingName);
                        $(_Fax).val(data.GenericList.Fax);
                        $(_OpeningBalanceD).val(data.GenericList.DebtorOpBal);
                        $(_OpeningBalanceC).val(data.GenericList.CreditorOpBal);
                        $(_NT).val(data.GenericList.NTN);
                        $(_ST).val(data.GenericList.STN);

                        $(_ContactInfo1).val(data.GenericList.CellNo1);
                        $(_ContactInfo2).val(data.GenericList.CellNo2);
                        $(_Email1).val(data.GenericList.Email1);
                        $(_Email2).val(data.GenericList.Email2);
                        $(_DealerType).val(data.GenericList.CustomerTypeID);
                        $(_DealerCategory).val(data.GenericList.Terms);
                        $(_CreditLimit).val(data.GenericList.CreditLimit);
                        $(_Login).val(data.GenericList.CustomerLogin);
                        $(_CustomerGroup).val(data.GenericList.CustomerGroupID);
                        $(_SalesPerson).val(data.GenericList.EmployeeID);
                        $(_Pricing).val(data.GenericList.FK_RateMaster);

                        $("#modalAdd").modal();
                    }

                }
            },
            error: OnErrorCall,
            complete: function () {
                isValueExist(_Company, $(_Company).val())
                isValueExist(_ContactPerson1, $(_ContactPerson1).val())
                isValueExist(_ContactPerson2, $(_ContactPerson2).val())
                isValueExist(_Address, $(_Address).val())
                isValueExist(_DebtorCode, $(_DebtorCode).val())
                isValueExist(_CreditorCode, $(_CreditorCode).val())
                isValueExist(_Country, $(_Country).val())
                isValueExist(_City, $(_City).val())
                isValueExist(_Location, $(_Location).val())
                isValueExist(_Phone, $(_Phone).val())
                isValueExist(_Email, $(_Email).val())
                isValueExist(_BillingName, $(_BillingName).val())
                isValueExist(_Fax, $(_Fax).val())
                isValueExist(_OpeningBalanceD, $(_OpeningBalanceD).val())
                isValueExist(_OpeningBalanceC, $(_OpeningBalanceC).val())
                isValueExist(_NT, $(_NT).val())
                isValueExist(_ST, $(_ST).val())
                isValueExist(_ContactInfo1, $(_ContactInfo1).val())
                isValueExist(_ContactInfo2, $(_ContactInfo2).val())
                isValueExist(_Email1, $(_Email1).val())
                isValueExist(_Email2, $(_Email2).val())
                isValueExist(_DealerType, $(_DealerType).val())
                isValueExist(_DealerCategory, $(_DealerCategory).val())
                isValueExist(_CreditLimit, $(_CreditLimit).val())
                isValueExist(_Login, $(_Login).val())
                isValueExist(_CustomerGroup, $(_CustomerGroup).val())
                isValueExist(_SalesPerson, $(_SalesPerson).val())
                isValueExist(_Pricing, $(_Pricing).val())
            }
        });
    })
}


function OnSave() {


    var isValid = true

    $("body #customerForm input").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    $("body #customerForm select").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    if (isValid) {
        var dataSend = {
            ID: $(_ID).val(),
            FirstName: $(_ContactPerson1).val(),
            Company: $(_Company).val(),
            Address: $(_Address).val(),
            Phone: $(_Phone).val(),
            Fax: $(_Fax).val(),
            Email: $(_Email).val(),
            CreditLimit: $(_CreditLimit).val(),
            LocationID: $(_Location).val(),
            DebtorAccountCode: $(_DebtorCode).val(),
            DebtorOpBal: $(_OpeningBalanceD).val(),
            CreditorAccountCode: $(_CreditorCode).val(),
            CreditorOpBal: $(_OpeningBalanceC).val(),
            NTN: $(_NT).val(),
            STN: $(_ST).val(),
            CellNo1: $(_ContactInfo1).val(),
            CellNo2: $(_ContactInfo2).val(),
            Email1: $(_Email1).val(),
            Email2: $(_Email2).val(),
            cityID: $(_City).val(),
            countryID: $(_Country).val(),
            CustomerLogin: $(_Login).val(),
            CBillingName: $(_BillingName).val(),
            CustomerGroupID: $(_CustomerGroup).val(),
            CustomerTypeID: $(_DealerType).val(),
            EmployeeID: $(_SalesPerson).val(),
            FK_RateMaster: $(_Pricing).val(),
            LastName: $(_ContactPerson2).val(),
            Terms: $(_DealerCategory).val()
        }

        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnSave",
            data: JSON.stringify({ model: dataSend }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        ClearFields();
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(success);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "exist") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(exist);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "update") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(update);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    }
                }
            },
            error: OnErrorCall
        });
    }
}

function FormValidaton() {
    msg = "";

    //$("#rFName").addClass("hide");

    //if ($(_FName).val() == "") {
    //    msg += "First Name is required";
    //    $("#rFName").removeClass("hide");
    //} else {
    //    $("#rFName").addClass("hide");
    //}

    //$("#rCNIC").addClass("hide");

    //if ($(_CNIC).val() == "") {
    //    msg += "CNIC is required";
    //    $("#rCNIC").removeClass("hide");
    //} else {
    //    $("#rCNIC").addClass("hide");
    //}

    //$("#rAddress").addClass("hide");

    //if ($(_Address).val() == "") {
    //    msg += "Address is required";
    //    $("#rAddress").removeClass("hide");
    //} else {
    //    $("#rAddress").addClass("hide");
    //}

    //$("#rEmail").addClass("hide");

    //if ($(_Email).val() == "") {
    //    msg += "Email is required";
    //    $("#rEmail").removeClass("hide");
    //} else {
    //    $("#rEmail").addClass("hide");
    //}

    //$("#rMob").addClass("hide");

    //if ($(_Mob).val() == "") {
    //    msg += "Mobile is required";
    //    $("#rMob").removeClass("hide");
    //} else {
    //    $("#rMob").addClass("hide");
    //}

    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}

function ClearFields() {

    $(_ID).val("");
    $(_Company).val("");
    $(_ContactPerson1).val("");
    $(_ContactPerson2).val("");
    $(_Address).val("");
    $(_DebtorCode).val("");
    $(_CreditorCode).val("");
    $(_Country).val("");
    $(_City).val("");
    $(_Location).val("");
    $(_Phone).val("");
    $(_Email).val("");
    $(_BillingName).val("");
    $(_Fax).val("");
    $(_OpeningBalanceD).val("");
    $(_OpeningBalanceC).val("");
    $(_NT).val("");
    $(_ST).val("");

    $(_ContactInfo1).val("");
    $(_ContactInfo2).val("");
    $(_Email1).val("");
    $(_Email2).val("");
    $(_DealerType).val("");
    $(_DealerCategory).val("");
    $(_CreditLimit).val("");
    $(_Login).val("");
    $(_CustomerGroup).val("");
    $(_SalesPerson).val("");
    $(_Pricing).val("");

    $("#ValidationSummary").text("");
    $("#rCustomerGroup").addClass("hide");
    $("#rCompany").addClass("hide");
    $("#rBillingName").addClass("hide");
    $("#rAddress").addClass("hide");
    $("#rCountry").addClass("hide");
    $("#rCity").addClass("hide");
    $("#rPhone").addClass("hide");
    $("#rFax").addClass("hide");
    $("#rEmail").addClass("hide");
    $("#rLogin").addClass("hide");
    $("#rNT").addClass("hide");
    $("#rST").addClass("hide");
    $("#rContactPerson1").addClass("hide");
    $("#rContactInfo1").addClass("hide");
    $("#rEmail1").addClass("hide");
    $("#rContactPerson2").addClass("hide");
    $("#rContactInfo2").addClass("hide");
    $("#rEmail2").addClass("hide");
    $("#rLocation").addClass("hide");
    $("#rDealerType").addClass("hide");
    $("#rCreditLimit").addClass("hide");
    $("#rDealerCategory").addClass("hide");
    $("#rDebtorCode").addClass("hide");
    $("#rOpeningBalanceD").addClass("hide");
    $("#rCreditorCode").addClass("hide");
    $("#rOpeningBalanceC").addClass("hide");
    $("#rSalesPerson").addClass("hide");
    $("#rPricing").addClass("hide");
}

var DeleteID = "";
function OnDelete() {
    $(tblname).on('click', '.del', function () {
        let id = $(this).attr('_recordId');
        DeleteID = id;
        $.ajax({
            type: "POST",
            url: "" + commonService + "/isAuthenticatedForDelete",
            data: JSON.stringify({ form: "Customer" }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        $("#modalDelete").modal();
                    }
                }
            },
            error: OnErrorCall
        });
    });
}

function OnConfirmDelete() {
    Delete(DeleteID);
}

function Delete(id) {
    if (id != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDelete",
            data: JSON.stringify({ dataID: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(delt);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalDelete").modal('hide');

                    } else if (data.Message == "failed") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(failedmsg);
                        $("#Validation").fadeOut(5000);
                    } else if (data.Message == "use") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("It is used by another process, Please try again!");
                        $("#Validation").fadeOut(5000);
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}


function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        dom: 'Bfrtip',
        buttons: [
             {
                 extend: 'print',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Customer',
                 text: '<i class="fas fa-print"></i>  Print'

             },
             {
                 extend: 'excelHtml5',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Customer',
                 text: '<i class="fas fa-file-excel"></i>  Excel',
             },
            {
                extend: 'pdf',
                footer: true,
                exportOptions: {
                    columns: [0, 1, 3]
                },
                title: 'Customer',
                text: '<i class="fas fa-file-pdf"></i>  PDF'
            },
        ],
        "aaData": JSON.parse(datasource),
        "aoColumns": [

                        { data: "ID", sDefaultContent: "" },
                        { data: "FirstName", sDefaultContent: "" },
                        { data: "LastName", sDefaultContent: "" },
                        { data: "Company", sDefaultContent: "" },
                        { data: "Address", sDefaultContent: "" },
                        { data: "Phone", sDefaultContent: "" },
                        { data: "Email", sDefaultContent: "" },
                        { data: "DebtorAccountCode", sDefaultContent: "" },
                        { data: "CreditorAccountCode", sDefaultContent: "" },
                        { data: "countryName", sDefaultContent: "" },
                        { data: "cityName", sDefaultContent: "" },
                        { data: "Location", sDefaultContent: "" },
                        { data: "CBillingName", sDefaultContent: "" },
                        { data: "Fax", sDefaultContent: "" },
                        { data: "DebtorOpBal", sDefaultContent: "" },
                        { data: "CreditorOpBal", sDefaultContent: "" },
                        { data: "NTN", sDefaultContent: "" },
                        { data: "STN", sDefaultContent: "" },
                        { data: "CellNo1", sDefaultContent: "" },
                        { data: "CellNo2", sDefaultContent: "" },
                        { data: "Email1", sDefaultContent: "" },
                        { data: "Email2", sDefaultContent: "" },
                        { data: "CustomerTypeID", sDefaultContent: "" },
                        { data: "Terms", sDefaultContent: "" },
                        { data: "CreditLimit", sDefaultContent: "" },
                        { data: "CustomerLogin", sDefaultContent: "" },
                        { data: "CustomerGroupID", sDefaultContent: "" },
                        { data: "EmployeeID", sDefaultContent: "" },
                        { data: "FK_RateMaster", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" _recordId = ' + data.ID + ' class="del" title="del"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#" _recordId = ' + data.ID + ' class="edit" title="Edit"><i class="fas fa-pencil-alt" ></i></a>';
                                    var html = del + edit;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [
            { "targets": [1], "width": "20%" },
            { "targets": [3], "width": "25%" },
            { "targets": [4], "width": "25%" },
            { "targets": [5], "width": "10%" },
            { "targets": [6], "width": "10%" },
            { "targets": [0], "visible": false, "searchable": false },
            { "targets": [2], "visible": false, "searchable": false },
            { "targets": [7], "visible": false, "searchable": false },
            { "targets": [8], "visible": false, "searchable": false },
            { "targets": [9], "visible": false, "searchable": false },
            { "targets": [10], "visible": false, "searchable": false },
            { "targets": [11], "visible": false, "searchable": false },
            { "targets": [12], "visible": false, "searchable": false },
            { "targets": [13], "visible": false, "searchable": false },
            { "targets": [14], "visible": false, "searchable": false },
            { "targets": [15], "visible": false, "searchable": false },
            { "targets": [16], "visible": false, "searchable": false },
            { "targets": [17], "visible": false, "searchable": false },
            { "targets": [18], "visible": false, "searchable": false },
            { "targets": [19], "visible": false, "searchable": false },
            { "targets": [20], "visible": false, "searchable": false },
            { "targets": [21], "visible": false, "searchable": false },
            { "targets": [22], "visible": false, "searchable": false },
            { "targets": [23], "visible": false, "searchable": false },
            { "targets": [24], "visible": false, "searchable": false },
            { "targets": [25], "visible": false, "searchable": false },
            { "targets": [26], "visible": false, "searchable": false },
            { "targets": [27], "visible": false, "searchable": false },
            { "targets": [28], "visible": false, "searchable": false },
        ]
    });
}

function OnErrorCall() {
    alert("Something went wrong");
}
