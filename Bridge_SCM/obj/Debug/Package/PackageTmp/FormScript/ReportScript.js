﻿var pagename = "Report.aspx";
var ServiceUrl = "../../appServices/ReportService.asmx";


var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";

var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";


var _VoucherType = "#ddlVoucherType";
var _VoucherStatus = "#ddlVoucherStatus";
var _Location = "#ddlLocation";
var _Segment = "#ddlSegment";
var _Department = "#ddlDepartment";
var _Project = "#ddlProject";
var _Employee = "#ddlEmployee";
var _Customer = "#ddlCustomer";
var _Supplier = "#ddlSupplier";
var _AccountCode = "#ddlAccountCode";
var _ddlReports = "#ddlReports";
var _ToDate = "#txtToDate";
var _FromDate = "#txtFromDate";
//var _FromTime = "#txtFromTime";
//var _ToTime = "#txtToTime";
var _ddlReportCategory = "#ddlReportCategory";

$(document).ready(function () {
    $(_ToDate).datepicker({
        autoclose: true
    });
    $(_FromDate).datepicker({
        autoclose: true
    });
    $(_ToDate).val(GetToday());
    $(_FromDate).val(GetToday());

    //$(_FromTime).val("00:00:00")
    //$(_ToTime).val("23:59:59")

    FillAccountCode();
    FillVoucherType();
    FillVoucherStatus();
    FillLocation()
    FillSegment()
    FillDepartment()
    FillProject()
    FillEmployee()
    FillCustomer()
    FillSupplier()
    FillReportsCategory();
    OnChangeReportCategory();

    $(".cmb1").hide()
    $(".cmb2").hide()
    $(".cmb3").hide()
    $(".cmb4").hide()

    $(".txt1").hide()
    $(".txt2").hide()
    $(".txt3").hide()
    $(".txt4").hide()
});


function GetToday() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var today = mm + '/' + dd + '/' + yyyy;
    return today;
}

function FillVoucherType() {
    $.ajax({
        type: "POST",
        url: ServiceUrl + '/FillVoucherType',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_VoucherType).empty();
            $(_VoucherType).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_VoucherType).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}


function FillVoucherStatus() {
    $.ajax({
        type: "POST",
        url: ServiceUrl + '/FillVoucherStatus',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_VoucherStatus).empty();
            $(_VoucherStatus).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_VoucherStatus).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}

function FillLocation() {
    $.ajax({
        type: "POST",
        url: ServiceUrl + '/FillLocation',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Location).empty();
            $(_Location).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Location).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}


function FillSegment() {
    $.ajax({
        type: "POST",
        url: ServiceUrl + '/FillSegment',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Segment).empty();
            $(_Segment).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Segment).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}


function FillDepartment() {
    $.ajax({
        type: "POST",
        url: ServiceUrl + '/FillDepartment',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Department).empty();
            $(_Department).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Department).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}


function FillProject() {
    $.ajax({
        type: "POST",
        url: ServiceUrl + '/FillProject',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Project).empty();
            $(_Project).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Project).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}


function FillEmployee() {
    $.ajax({
        type: "POST",
        url: ServiceUrl + '/FillEmployee',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Employee).empty();
            $(_Employee).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Employee).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}


function FillCustomer() {
    $.ajax({
        type: "POST",
        url: ServiceUrl + '/FillCustomer',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Customer).empty();
            $(_Customer).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Customer).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}

function FillSupplier() {
    $.ajax({
        type: "POST",
        url: ServiceUrl + '/FillSupplier',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Supplier).empty();
            $(_Supplier).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Supplier).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}

function FillReportsCategory() {
    $.ajax({
        type: "POST",
        url: ServiceUrl + '/FillReportsCategory',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_ddlReportCategory).empty();
            $(_ddlReportCategory).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_ddlReportCategory).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}

function OnChangeReportCategory() {
    $(_ddlReportCategory).on('change', function () {
        var id = $(_ddlReportCategory).val();

        if (id != "") {
            $.ajax({
                type: "POST",
                url: "" + ServiceUrl + "/FillReports",
                data: JSON.stringify({ ID: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var data = JSON.parse(response.d);
                    $(_ddlReports).empty();
                    $(_ddlReports).append("<option value='-1'>--Please Select--</option>");
                    jQuery.each(data, function (index, item) {
                        $(_ddlReports).append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });
                },
                error: OnErrorCall
            });
        }
    });
}

function OnErrorCall() {
    alert("Something went wrong")
}


function FillAccountCode() {
    $.getScript('../../Layout/multiplelist/select2/js/select2.js', function () {
        $.getScript('../../Layout/multiplelist/select/select2-init.js', function () {

            $.ajax({
                type: "POST",
                url: ServiceUrl + '/FillAccountCode',
                contentType: "application/json",
                dataType: "json",
                success: function (response) {
                    var data = JSON.parse(response.d);
                    $(_AccountCode).empty();
                    $(_AccountCode).append('<label>A/C Code</label>');
                    $(_AccountCode).append('<select class="form-control select2-multiple" id="ProductDropdown" multiple >')
                    jQuery.each(data, function (index, item) {
                        $("#ProductDropdown").append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });
                    var placeholder = "--Please Select--";
                    $('.select2, .select2-multiple').select2({
                        theme: "bootstrap",
                        placeholder: placeholder,
                    });
                }
            });
        })
    })

}


function selectAll(selectBox, selectAll) {
    // have we been passed an ID 
    if (typeof selectBox == "string") {
        selectBox = document.getElementById(selectBox);
    }
    // is the select box a multiple select box? 
    if (selectBox.type == "select-multiple") {
        for (var i = 0; i < selectBox.options.length; i++) {
            selectBox.options[i].selected = selectAll;
        }
    }
}