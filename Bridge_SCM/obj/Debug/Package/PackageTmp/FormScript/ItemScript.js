﻿var serviceurl = "../../appServices/ItemsService.asmx";
var tblname = "#tableData";
var table;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _ID = "#txtID";
var _Category = "#ddlitemcategory";
var _Brand = "#ddlitembrand";
var _Unit = "#ddlitemunit";
var _Partnumber = "#txtpartnumber";
var _Modelnumber = "#txtmodelnumber";
var _Productname = "#txtproductname";
var _Description = "#txtdescription";
var _Minqty = "#txtminqty";
var _Maxqty = "#txtmaxqty";
var _Minorderqty = "#txtminorderqty";
var _Dltime = "#txtdltime";
var _Salesrate = "#txtsalesrate";
var _Expgst = "#txtexpgst";
var _Warrantyyear = "#txtwarrantyyear";
var _Warrantynote = "#txtwarrantynote";
var _Isactive = "#isactive";
var _Gstapplicable = "#gstapplicable";
var _Availableforso = "#availableforso";
var _Itemtoberepost = "#itemtoberepost";
var _Issupportpart = "#issupportpart";


var loginUrl = "/Default.aspx";


$(document).ready(function () {

    $("body #itemForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #itemForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #itemForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })


    OnLoadGrid();
    OnDelete();
    OnEdit();
    FillItemCat();
    FillBrand();
    FillUnitList();
});

function OnSave() {

    var isValid = true

    $("body #itemForm input").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    $("body #itemForm select").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    if (isValid) {
        var gst;
        var active;
        var availablefor
        var itemtoberepost
        var issupportpart

        if ($(_Isactive).is(':checked')) {

            active = 1;
        }
        else {
            active = 0;
        }

        if ($(_Gstapplicable).is(':checked')) {
            gst = 1;
        }
        else {
            gst = 0;
        }

        if ($(_Availableforso).is(':checked')) {
            availablefor = 1;
        }
        else {
            availablefor = 0;
        }

        if ($(_Itemtoberepost).is(':checked')) {
            itemtoberepost = 1;
        }
        else {
            itemtoberepost = 0;
        }

        if ($(_Issupportpart).is(':checked')) {
            issupportpart = 1;
        }
        else {
            issupportpart = 0;
        }

        var dataSend = {
            ID: $(_ID).val(),
            FK_ItemCategory: $(_Category).val(),
            FK_BrandID: $(_Brand).val(),
            FK_Unit: $(_Unit).val(),
            PartNumber: $(_Partnumber).val(),

            ModelNumber: $(_Modelnumber).val(),
            ProductName: $(_Productname).val(),
            Description: $(_Description).val(),
            MinQuantity: $(_Minqty).val(),

            MaxQuantity: $(_Maxqty).val(),
            ReOrderQuantity: $(_Minorderqty).val(),
            DeliveryLandTime: $(_Dltime).val(),

            SalePoint: $(_Salesrate).val(),
            ExpGST: $(_Expgst).val(),
            WarrantyYear: $(_Warrantyyear).val(),
            WarrantyNote: $(_Warrantynote).val(),

            isActive: active,
            GST: gst,
            AvailableForSO: availablefor,

            ItemToBeRepost: itemtoberepost,
            isSerialNo: issupportpart

        }
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnSave",
            data: JSON.stringify({ model: dataSend }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        ClearFields();
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(success);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "exist") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(exist);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "update") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(update);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    }
                }
            },
            error: OnErrorCall
        });
    }

}

function FormValidaton() {
    msg = "";

    $("#rPartnumber").addClass("hide");

    if ($(_Partnumber).val() == "") {
        msg += "required";
        $("#rPartnumber").removeClass("hide");
    } else {
        $("#rPartnumber").addClass("hide");
    }

    $("#rModelnumber").addClass("hide");

    if ($(_Modelnumber).val() == "") {
        msg += "required";
        $("#rModelnumber").removeClass("hide");
    } else {
        $("#rModelnumber").addClass("hide");
    }

    //$("#rProductname").addClass("hide");

    //if ($(_Productname).val() == "") {
    //    msg += "Product Name is required";
    //    $("#rProductname").removeClass("hide");
    //} else {
    //    $("#rProductname").addClass("hide");
    //}

    //$("#rDescription").addClass("hide");

    //if ($(_Description).val() == "") {
    //    msg += "Description is required";
    //    $("#rDescription").removeClass("hide");
    //} else {
    //    $("#rDescription").addClass("hide");
    //}

    //$("#rMinqty").addClass("hide");

    //if ($(_Minqty).val() == "") {
    //    msg += "Min Qty is required";
    //    $("#rMinqty").removeClass("hide");
    //} else {
    //    $("#rMinqty").addClass("hide");
    //}

    //$("#rMaxqty").addClass("hide");

    //if ($(_Maxqty).val() == "") {
    //    msg += "Max Qty is required";
    //    $("#rMaxqty").removeClass("hide");
    //} else {
    //    $("#rMaxqty").addClass("hide");
    //}

    //$("#rMinorderqty").addClass("hide");

    //if ($(_Minorderqty).val() == "") {
    //    msg += "Min Order Qty is required";
    //    $("#rMinorderqty").removeClass("hide");
    //} else {
    //    $("#rMinorderqty").addClass("hide");
    //}


    //$("#rDltime").addClass("hide");

    //if ($(_Dltime).val() == "") {
    //    msg += "D.l Time is required";
    //    $("#rDltime").removeClass("hide");
    //} else {
    //    $("#rDltime").addClass("hide");
    //}



    //$("#rSalesrate").addClass("hide");

    //if ($(_Salesrate).val() == "") {
    //    msg += "Salse Rate is required";
    //    $("#rSalesrate").removeClass("hide");
    //} else {
    //    $("#rSalesrate").addClass("hide");
    //}

    //$("#rExpgst").addClass("hide");

    //if ($(_Expgst).val() == "") {
    //    msg += "Exp GST is required";
    //    $("#rExpgst").removeClass("hide");
    //} else {
    //    $("#rExpgst").addClass("hide");
    //}

    //$("#rWarrantyyear").addClass("hide");

    //if ($(_Warrantyyear).val() == "") {
    //    msg += "Warranty Year is required";
    //    $("#rWarrantyyear").removeClass("hide");
    //} else {
    //    $("#rWarrantyyear").addClass("hide");
    //}


    //$("#rWarrantynote").addClass("hide");

    //if ($(_Warrantynote).val() == "") {
    //    msg += "Warranty Note is required";
    //    $("#rWarrantynote").removeClass("hide");
    //} else {
    //    $("#rWarrantynote").addClass("hide");
    //}






    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}

function ClearFields() {

    $(_ID).val("");
    $(_Partnumber).val("");
    $(_Modelnumber).val("");
    $(_Productname).val("");
    $(_Description).val("");

    $(_Minqty).val("");
    $(_Maxqty).val("");
    $(_Minorderqty).val("");
    $(_Dltime).val("");

    $(_Salesrate).val("");
    $(_Expgst).val("");
    $(_Warrantyyear).val("");
    $(_Warrantynote).val("");


    $(_Category).val("-1");
    isValueExist(_Category, $(_Category).val())
    $(_Unit).val("-1");
    isValueExist(_Unit, $(_Unit).val())
    $(_Brand).val("-1");
    isValueExist(_Brand, $(_Brand).val())

    $('input[id=isactive]').attr('checked', false);
    $('input[id=isactive]').prop('checked', false);

    $('input[id=gstapplicable]').attr('checked', false);
    $('input[id=gstapplicable]').prop('checked', false);

    $('input[id=availableforso]').attr('checked', false);
    $('input[id=availableforso]').prop('checked', false);

    $('input[id=itemtoberepost]').attr('checked', false);
    $('input[id=itemtoberepost]').prop('checked', false);

    $('input[id=issupportpart]').attr('checked', false);
    $('input[id=issupportpart]').prop('checked', false);


    $("#ValidationSummary").text("");
    $("#rPartnumber").addClass("hide");
    $("#rModelnumber").addClass("hide");

    //$("#rProductname").addClass("hide");
    //$("#rDescription").addClass("hide");
    //$("#rMinqty").addClass("hide");

    //$("#rMaxqty").addClass("hide");
    //$("#rMinorderqty").addClass("hide");
    //$("#rDltime").addClass("hide");

    //$("#rSalesrate").addClass("hide");
    //$("#rExpgst").addClass("hide");

    //$("#rWarrantyyear").addClass("hide");
    //$("#rWarrantynote").addClass("hide");


}

function OnLoadGrid() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/GetAll",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblname).DataTable().destroy();
            LoadDataTable(response.d);
        },
        error: OnErrorCall
    });


}

function OnConfirmDelete() {
    Delete(DeleteID);
}

var DeleteID = "";
function OnDelete() {
    $(tblname).on('click', '.del', function () {
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData);
        DeleteID = rowData.ID;

        if (DeleteID != "") {
            $("#modalDelete").modal();
        }

    });
}

function OnEdit() {
    $(tblname).on('click', '.edit', function () {
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/getItemById",
            data: JSON.stringify({ id: rowData.ID }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var data = JSON.parse(response.d)
                $(_ID).val(data.ID);
                $(_Category).val(data.FK_ItemCategory);
                $(_Brand).val(data.FK_BrandID);
                $(_Unit).val(data.FK_Unit);
                $(_Partnumber).val(data.PartNumber);

                $(_Modelnumber).val(data.ModelNumber);
                $(_Productname).val(data.ProductName);
                $(_Description).val(data.Description);
                $(_Minqty).val(data.MinQuantity);

                $(_Maxqty).val(data.MaxQuantity);
                $(_Minorderqty).val(data.ReOrderQuantity);
                $(_Dltime).val(data.DeliveryLandTime);
                $(_Salesrate).val(data.SalePoint);

                $(_Expgst).val(data.ExpGST);
                $(_Warrantyyear).val(data.WarrantyYear);
                $(_Warrantynote).val(data.WarrantyNote);



                if (data.isActive == "True") {
                    $('input[id=isactive]').attr('checked', true);
                    $('input[id=isactive]').prop('checked', true);
                }
                else {
                    $('input[id=isactive]').attr('checked', false);
                    $('input[id=isactive]').prop('checked', false);
                }


                if (data.GST == "True") {
                    $('input[id=gstapplicable]').attr('checked', true);
                    $('input[id=gstapplicable]').prop('checked', true);
                }
                else {
                    $('input[id=gstapplicable]').attr('checked', false);
                    $('input[id=gstapplicable]').prop('checked', false);

                }

                if (data.AvailableForSO == "True") {
                    $('input[id=availableforso]').attr('checked', true);
                    $('input[id=availableforso]').prop('checked', true);
                }
                else {
                    $('input[id=availableforso]').attr('checked', false);
                    $('input[id=availableforso]').prop('checked', false);
                }

                if (data.ItemToBeRepost == "1") {
                    $('input[id=itemtoberepost]').attr('checked', 1);
                    $('input[id=itemtoberepost]').prop('checked', 1);
                }
                else {
                    $('input[id=itemtoberepost]').attr('checked', 0);
                    $('input[id=itemtoberepost]').prop('checked', 0);
                }
                // alert(rowData.ItemToBeRepost == "1");

                if (data.isSerialNo == "True") {
                    $('input[id=issupportpart]').attr('checked', true);
                    $('input[id=issupportpart]').prop('checked', true);
                }
                else {
                    $('input[id=issupportpart]').attr('checked', false);
                    $('input[id=issupportpart]').prop('checked', false);
                }


                $("#formModalLabel").text("Edit");
                $("#modalAdd").modal();

                $("#ValidationSummary").text("");
                $("#rPartnumber").addClass("hide");
                $("#rModelnumber").addClass("hide");
                $(".label-floating").addClass("is-focused");
            },
            error: OnErrorCall
        });

        clearAllMessages("itemForm")

        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("Item", "itemForm")
        }
        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("Item", "itemForm", service_DeveloperMode)

        //}
    });
}

function Delete(id) {
    if (id != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDelete",
            data: JSON.stringify({ dataID: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(delt);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalDelete").modal('hide');

                    } else if (data.Message == "failed") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(failedmsg);
                        $("#Validation").fadeOut(5000);
                    } else if (data.Message == "use") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("It is used by another process, Please try again!");
                        $("#Validation").fadeOut(5000);
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}


function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        dom: 'Bfrtip',
        buttons: [
             {
                 extend: 'print',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Item',
                 text: '<i class="fa fa-lg fa-print"></i>  Print'

             },
             {
                 extend: 'excelHtml5',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Item',
                 text: '<i class="fa fa-lg fa-file-excel"></i>  Excel',
             },
            {
                extend: 'pdf',
                footer: true,
                exportOptions: {
                    columns: [0, 1, 3]
                },
                title: 'Item',
                text: '<i class="fa fa-lg fa-file-pdf"></i>  PDF'
            },
        ],
        "aaData": JSON.parse(datasource),
        "aoColumns": [

                        { data: "ID", sDefaultContent: "" },
                        { data: "PartNumber", sDefaultContent: "" },
                        { data: "ModelNumber", sDefaultContent: "" },
                        { data: "Description", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" class="del" title="del"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#" class="edit" title="Edit"><i class="fas fa-pencil-alt" ></i></a>';
                                    var html = del + " | " + edit;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [
            //{ "targets": [0], "width": "3%" },

        ]
    });
}
//"visible": false
function OnAdd() {
    $(".label-floating").removeClass("is-focused");
    ClearFields();
    $("#formModalLabel").text("Add");
    $("#modalAdd").modal();

    clearAllMessages("itemForm")

    var service_DeveloperMode = $("#hdnDevMode").val()
    if (service_DeveloperMode == 'on') {
        addFormFields("Item", "itemForm")
    }
    //var service_ToolTip = $("#hdnToolTip").val()
    //if (service_ToolTip == 'on') {

    setToolTip("Item", "itemForm", service_DeveloperMode)

    //}
}


function OnErrorCall() {
    alert("Something went wrong");
}

function FillItemCat() {

    //alert('3')
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetItem',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Category).empty();
            $(_Category).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Category).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
            //$(_dependent).select2('val', index);

        }
        //failure: function (errMsg) {
        //    alert(errMsg);
        //}
    });

}

function FillBrand() {

    //alert('3')
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetBrand',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Brand).empty();
            $(_Brand).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Brand).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
            //$(_dependent).select2('val', index);

        }
        //failure: function (errMsg) {
        //    alert(errMsg);
        //}
    });

}

function FillUnitList() {

    //alert('3')
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetUnit',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Unit).empty();
            $(_Unit).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Unit).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
            //$(_dependent).select2('val', index);

        }
        //failure: function (errMsg) {
        //    alert(errMsg);
        //}
    });

}


