﻿var serviceurl = "../../appServices/EmployeeService.asmx";
var commonService = "../../appServices/CommonService.asmx";
var tblname = "#tableData";
var table;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _EmployeeID = "#txtID";
var _Name = "#txtName";
var _Male = "#male";
var _Female = "#female"
var _DOB = "#txtDOB";
var _Address = "#txtAddress";
var _Phone = "#txtPhone";
var _Fax = "#txtFax";
var _cityID = "#ddlCity";
var _ContactReference1 = "#txtContactPerson1";
var _ContactReference2 = "#txtContactPerson2";
var _JoiningDate = "#txtJoin";
var _EndDate = "#txtEnd";
var _Status = "#ddlStatus";
var _Nationality = "#txtNationality";
var _NIC = "#txtNIC";
var _CountriesTravelled = "#txtCountriesTravelled";
var _LanguagesSpoken = "#txtSpoken";
var _LanguagesWritten = "#txtWritten";
var _MaritalStatus = "#ddlMaritial";
var _LocationID = "#ddlLocation";
var _DesignationID = "#ddlDesignation";
var _DepartmentID = "#ddlDepartment";
var _CellNo = "#txtCell";
var _PassportNo = "#txtPassport";
var _Mode;
//var _Signature = "#txtSignature"

var loginUrl = "/Default.aspx";


$(document).ready(function () {

    // Wizard style
    //Add blue animated border and remove with condition when focus and blur
    if ($('.fg-line')[0]) {
        $('body').on('focus', '.form-control', function () {
            $(this).closest('.fg-line').addClass('fg-toggled');
        })

        $('body').on('blur', '.form-control', function () {
            var p = $(this).closest('.form-group');
            var i = p.find('.form-control').val();

            if (p.hasClass('fg-float')) {
                if (i.length == 0) {
                    $(this).closest('.fg-line').removeClass('fg-toggled');
                }
            }
            else {
                $(this).closest('.fg-line').removeClass('fg-toggled');
            }
        });
    }

    //Add blue border for pre-valued fg-flot text feilds
    if ($('.fg-float')[0]) {
        $('.fg-float .form-control').each(function () {
            var i = $(this).val();

            if (!i.length == 0) {
                $(this).closest('.fg-line').addClass('fg-toggled');
            }

        });
    }


    /*   Form Wizard Functions  */
    /*--------------------------*/
    _handleTabShow = function (tab, navigation, index, wizard) {
        var total = navigation.find('li').length;
        var current = index + 0;
        var percent = (current / (total - 1)) * 100;
        var percentWidth = 100 - (100 / total) + '%';

        navigation.find('li').removeClass('done');
        navigation.find('li.active').prevAll().addClass('done');

        wizard.find('.progress-bar').css({ width: percent + '%' });
        $('.form-wizard-horizontal').find('.progress').css({ 'width': percentWidth });
    };

    _updateHorizontalProgressBar = function (tab, navigation, index, wizard) {
        var total = navigation.find('li').length;
        var current = index + 0;
        var percent = (current / (total - 1)) * 100;
        var percentWidth = 100 - (100 / total) + '%';

        wizard.find('.progress-bar').css({ width: percent + '%' });
        wizard.find('.progress').css({ 'width': percentWidth });
    };

    /* Form Wizard - Example 1  */
    /*--------------------------*/
    $('#formwizard_simple').bootstrapWizard({
        onTabShow: function (tab, navigation, index) {
            _updateHorizontalProgressBar(tab, navigation, index, $('#formwizard_simple'));
        }
    });

    /* Form Wizard - Example 2  */
    /*--------------------------*/

    $('#formwizard_validation').bootstrapWizard({
        onNext: function (tab, navigation, index) {
            var form = $('#formwizard_validation').find("form");
            var valid = true;

            if (index == 1) {
                var fname = form.find('#firstname');
                var lastname = form.find('#lastname');

                if (!fname.val()) {
                    swal("You must enter your first name!");
                    fname.focus();
                    return false;
                }

                if (!lastname.val()) {
                    swal("You must enter your last name!");
                    lastname.focus();
                    return false;
                }
            }

            if (!valid) {
                return false;
            }
        },
        onTabShow: function (tab, navigation, index) {
            _updateHorizontalProgressBar(tab, navigation, index, $('#formwizard_validation'));
        }
    });
    $(".form-wizard-nav ul").removeClass("nav-pills")
    // Wizard style end


    $("body #employeeForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #employeeForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #employeeForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })

    FillDesignation();
    FillDepartment();
    FillStatus();
    FillMaritalStatus();
    FillCity();
    FillLocation();
    demo.initFormExtendedDatetimepickers();
    //$(_DOB).datepicker({ autoclose: true });
    //$(_JoiningDate).datepicker({ autoclose: true });
    //$(_EndDate).datepicker({ autoclose: true });
    OnLoadGrid();
    bindAddEditEmployee();
    OnDelete();
});

function FillDesignation() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetDesignationList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_DesignationID).empty();
            $(_DesignationID).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_DesignationID).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_DesignationID, $(_DesignationID).val())
        }
    });
}

function FillDepartment() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetDepartmentList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_DepartmentID).empty();
            $(_DepartmentID).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_DepartmentID).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_DepartmentID, $(_DepartmentID).val())
        }
    });
}

function FillStatus() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetStatusList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Status).empty();
            $(_Status).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Status).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_Status, $(_Status).val())
        }
    });
}

function FillMaritalStatus() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetMaritalStatusList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_MaritalStatus).empty();
            $(_MaritalStatus).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_MaritalStatus).append("<option> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_MaritalStatus, $(_MaritalStatus).val())
        }
    });
}

function FillLocation() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetLocationList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_LocationID).empty();
            $(_LocationID).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_LocationID).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_LocationID, $(_LocationID).val())
        }
    });
}

function FillCity() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetCityList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_cityID).empty();
            $(_cityID).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_cityID).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_cityID, $(_cityID).val())
        }
    });
}

function bindAddEditEmployee() {
    $("body").on("click", "#btnAdd , .edit", function () {

        let id = $(this).attr("_recordId")
        clearAllMessages("employeeForm")

        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("Employee", "employeeForm")
        }
        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("Employee", "employeeForm", service_DeveloperMode)

        //}

        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/AddEditEmployee",
            data: JSON.stringify({ id: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                debugger
                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        if (id != "0") {
                            _Mode = "Edit";
                            $("#formModalLabel").text("Edit");
                            $(".label-floating").addClass("is-focused");
                            if (data.GenericList.Gender == "M") {
                                $("#male").prop("checked", true)
                            } else {
                                $("#female").prop("checked", true)
                            }
                        } else {
                            _Mode = "Add";
                            ClearFields();
                            $("#formModalLabel").text("Add");
                            $(".label-floating").removeClass("is-focused");
                        }

                        $(_EmployeeID).val(data.GenericList.ID)
                        //$(_EmployeeID).attr('readonly', true);
                        $(_Name).val(data.GenericList.FirstName)

                        if (data.GenericList.DOB != "") {
                            var javascriptDate = new Date(data.GenericList.DOB);
                            javascriptDate = javascriptDate.getMonth() + 1 + "/" + javascriptDate.getDate() + "/" + javascriptDate.getFullYear();
                            $(_DOB).val(javascriptDate)
                        } else {
                            $(_DOB).val("")
                        }
                        $(_Address).val(data.GenericList.Address)
                        $(_Phone).val(data.GenericList.Phone)
                        $(_Fax).val(data.GenericList.Fax)
                        $(_cityID).val(data.GenericList.cityID),
                        $(_ContactReference1).val(data.GenericList.ContactReference1)
                        $(_ContactReference2).val(data.GenericList.ContactReference2)
                        if (data.GenericList.JoiningDate != "") {
                            var javascriptDate2 = new Date(data.GenericList.JoiningDate);
                            javascriptDate2 = javascriptDate2.getMonth() + 1 + "/" + javascriptDate2.getDate() + "/" + javascriptDate2.getFullYear();

                            $(_JoiningDate).val(javascriptDate2)
                        } else {
                            $(_JoiningDate).val("")
                        }
                        if (data.GenericList.EndDate != "") {
                            var javascriptDate3 = new Date(data.GenericList.EndDate);
                            javascriptDate3 = javascriptDate3.getMonth() + 1 + "/" + javascriptDate3.getDate() + "/" + javascriptDate3.getFullYear();

                            $(_EndDate).val(javascriptDate3)
                        } else {
                            $(_EndDate).val("")
                        }
                        $(_Status).val(data.GenericList.StatusID)
                        $(_Nationality).val(data.GenericList.StatusID)
                        $(_NIC).val(data.GenericList.NIC)
                        $(_CountriesTravelled).val(data.GenericList.CountriesTravelled)
                        $(_LanguagesSpoken).val(data.GenericList.LanguagesSpoken)
                        $(_LanguagesWritten).val(data.GenericList.LanguagesWritten)
                        $(_MaritalStatus).val(data.GenericList.MaritalStatus)
                        $(_PassportNo).val(data.GenericList.PassportNo)
                        $(_LocationID).val(data.GenericList.LocationID)
                        $(_DesignationID).val(data.GenericList.DesignationID)
                        $(_DepartmentID).val(data.GenericList.DepartmentID)
                        $(_CellNo).val(data.GenericList.CellNo)

                        $("#modalAdd").modal();
                    }

                }
            },
            error: OnErrorCall,
            complete: function () {
                isValueExist(_Name, $(_Name).val())
                isValueExist(_DOB, $(_DOB).val())
                isValueExist(_Address, $(_Address).val())
                isValueExist(_Phone, $(_Phone).val())
                isValueExist(_Fax, $(_Fax).val())
                isValueExist(_cityID, $(_cityID).val())
                isValueExist(_ContactReference1, $(_ContactReference1).val())
                isValueExist(_ContactReference2, $(_ContactReference2).val())
                isValueExist(_JoiningDate, $(_JoiningDate).val())
                isValueExist(_EndDate, $(_EndDate).val())
                isValueExist(_Status, $(_Status).val())
                isValueExist(_Nationality, $(_Nationality).val())
                isValueExist(_NIC, $(_NIC).val())
                isValueExist(_CountriesTravelled, $(_CountriesTravelled).val())
                isValueExist(_LanguagesSpoken, $(_LanguagesSpoken).val())
                isValueExist(_LanguagesWritten, $(_LanguagesWritten).val())
                isValueExist(_MaritalStatus, $(_MaritalStatus).val())
                isValueExist(_PassportNo, $(_PassportNo).val())
                isValueExist(_DesignationID, $(_DesignationID).val())
                isValueExist(_DepartmentID, $(_DepartmentID).val())
                isValueExist(_CellNo, $(_CellNo).val())
            }
        });
    })
}

function OnSave() {

    var isValid = true

    $("body #employeeForm input").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    $("body #employeeForm select").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    if (isValid) {
        var gender;
        if ($("input[name=optionsRadios]:checked").val() == "Male") {
            gender = "M"
        } else {
            gender = "F"
        }

        var dataSend = {
            Mode: _Mode,
            ID: $(_EmployeeID).val(),
            FirstName: $(_Name).val(),
            Gender: gender,
            DOB: $(_DOB).val(),
            Address: $(_Address).val(),
            Phone: $(_Phone).val(),
            Fax: $(_Fax).val(),
            cityID: $(_cityID).val(),
            ContactReference1: $(_ContactReference1).val(),
            ContactReference2: $(_ContactReference2).val(),
            JoiningDate: $(_JoiningDate).val(),
            EndDate: $(_EndDate).val(),
            StatusID: $(_Status).val(),
            Nationality: $(_Nationality).val(),
            NIC: $(_NIC).val(),
            CountriesTravelled: $(_CountriesTravelled).val(),
            LanguagesSpoken: $(_LanguagesSpoken).val(),
            LanguagesWritten: $(_LanguagesWritten).val(),
            MaritalStatus: $("#ddlMaritial option:selected").text(),
            PassportNo: $(_PassportNo).val(),
            LocationID: $(_LocationID).val(),
            DesignationID: $(_DesignationID).val(),
            DepartmentID: $(_DepartmentID).val(),
            CellNo: $(_CellNo).val()
        }

        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnSave",
            data: JSON.stringify({ model: dataSend }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        ClearFields();
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                        $("#modalAdd").modal('hide');
                    }
                    if (data.Message == "success") {
                        ClearFields();
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(success);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "exist") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(exist);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "update") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(update);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    }
                }
            },
            error: OnErrorCall
        });
    }

}

function FormValidaton() {
    msg = "";

    $("#rName").addClass("hide");


    if ($(_Name).val() == "") {
        msg += "Type is required";
        $("#rName").removeClass("hide");
    } else {
        $("#rName").addClass("hide");
    }

    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}

function ClearFields() {
    $(_EmployeeID).val("")
    $(_EmployeeID).attr('readonly', false)
    $(_Name).val("")
    $(_DOB).val("")
    $(_Address).val("")
    $(_Phone).val("")
    $(_Fax).val("")
    $(_cityID).val("-1"),
    $(_ContactReference1).val("")
    $(_ContactReference2).val("")
    $(_JoiningDate).val("")
    $(_EndDate).val("")
    $(_Status).val("-1")
    $(_Nationality).val("")
    $(_NIC).val("")
    $(_CountriesTravelled).val("")
    $(_LanguagesSpoken).val("")
    $(_LanguagesWritten).val("")
    $(_MaritalStatus).val("-1")
    $(_PassportNo).val("")
    $(_LocationID).val("-1")
    $(_DesignationID).val("-1")
    $(_DepartmentID).val("-1")
    $(_CellNo).val("")
    $("#male").prop("checked", true)


    $("#ValidationSummary").text("");
    $("#rID").addClass("hide");
    $("#rName").addClass("hide");
    $("#rContactPerson1").addClass("hide");
    $("#rContactPerson2").addClass("hide");
    $("#rDesignation").addClass("hide");
    $("#rJoin").addClass("hide");
    $("#rEnd").addClass("hide");
    $("#rDOB").addClass("hide");
    $("#rPhone").addClass("hide");
    $("#rCell").addClass("hide");
    $("#rPassport").addClass("hide");
    $("#rNIC").addClass("hide");
    $("#rFax").addClass("hide");
    $("#rNationality").addClass("hide");
    $("#rLocation").addClass("hide");
    $("#rDepartment").addClass("hide");
    $("#rCity").addClass("hide");
    $("#rStatus").addClass("hide");
    $("#rMaritial").addClass("hide");
    $("#rSpoken").addClass("hide");
    $("#rWritten").addClass("hide");
    $("#rSignature").addClass("hide");
    $("#rAddress").addClass("hide");
}

function OnLoadGrid() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            let data = JSON.parse(response.d)
            if (!data.Status) {
                $("#Validation").fadeIn(1000);
                $("#Validation").text(data.Message);
                $("#Validation").fadeOut(5000);
            } else {
                $(tblname).DataTable().destroy();
                LoadDataTable(data.GenericList);
            }

        },
        error: OnErrorCall
    });


}

function OnConfirmDelete() {
    Delete(DeleteID, DeleteName);
}

var DeleteID = "";
var DeleteName = "";
function OnDelete() {
    $(tblname).on('click', '.del', function () {
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData);
        DeleteID = rowData.ID;
        DeleteName = rowData.FirstName
        $.ajax({
            type: "POST",
            url: "" + commonService + "/isAuthenticatedForDelete",
            data: JSON.stringify({ form: "Employee" }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        $("#modalDelete").modal();
                    }
                }
            },
            error: OnErrorCall
        });

    });
}

function Delete(id, Name) {
    if (id != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDelete",
            data: JSON.stringify({ dataID: id, dataName: Name }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                        $("#modalDelete").modal('hide');
                    }
                    if (data.Message == "success") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(delt);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalDelete").modal('hide');

                    } else if (data.Message == "failed") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(failedmsg);
                        $("#Validation").fadeOut(5000);
                    } else if (data.Message == "use") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("It is used by another process, Please try again!");
                        $("#Validation").fadeOut(5000);
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}


function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        dom: 'Bfrtip',
        buttons: [
             {
                 extend: 'print',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Employee',
                 text: '<i class="fas fa-print"></i>  Print'

             },
             {
                 extend: 'excelHtml5',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Employee',
                 text: '<i class="fas fa-file-excel"></i>  Excel',
             },
            {
                extend: 'pdf',
                footer: true,
                exportOptions: {
                    columns: [0, 1, 3]
                },
                title: 'Employee',
                text: '<i class="fas fa-file-pdf"></i>  PDF'
            },
        ],
        "aaData": datasource,
        "aoColumns": [

                        { data: "ID", sDefaultContent: "" },
                        { data: "FirstName", sDefaultContent: "" },
                        { data: "Gender", sDefaultContent: "" },
                        { data: "DOB", sDefaultContent: "" },
                        { data: "Address", sDefaultContent: "" },
                        { data: "Phone", sDefaultContent: "" },
                        { data: "Fax", sDefaultContent: "" },
                        { data: "cityID", sDefaultContent: "" },
                        { data: "ContactReference1", sDefaultContent: "" },
                        { data: "ContactReference2", sDefaultContent: "" },
                        { data: "JoiningDate", sDefaultContent: "" },
                        { data: "EndDate", sDefaultContent: "" },
                        { data: "StatusID", sDefaultContent: "" },
                        { data: "Nationality", sDefaultContent: "" },
                        { data: "NIC", sDefaultContent: "" },
                        { data: "CountriesTravelled", sDefaultContent: "" },
                        { data: "LanguagesSpoken", sDefaultContent: "" },
                        { data: "LanguagesWritten", sDefaultContent: "" },
                        { data: "MaritalStatus", sDefaultContent: "" },
                        { data: "PassportNo", sDefaultContent: "" },
                        { data: "LocationID", sDefaultContent: "" },
                        { data: "DesignationID", sDefaultContent: "" },
                        { data: "DepartmentID", sDefaultContent: "" },
                        { data: "CellNo", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" _recordId = ' + data.ID + '  class="del" title="del"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#" _recordId = ' + data.ID + '  class="edit" title="Edit"><i class="fas fa-pencil-alt" ></i></a>';
                                    var html = del + edit;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [
             { "targets": [2], "searchable": false, "visible": false },
             { "targets": [3], "searchable": false, "visible": false },
             { "targets": [5], "searchable": false, "visible": false },
             { "targets": [6], "searchable": false, "visible": false },
             { "targets": [7], "searchable": false, "visible": false },
             { "targets": [8], "searchable": false, "visible": false },
             { "targets": [9], "searchable": false, "visible": false },
             { "targets": [11], "searchable": false, "visible": false },
             { "targets": [12], "searchable": false, "visible": false },
             { "targets": [13], "searchable": false, "visible": false },
             { "targets": [15], "searchable": false, "visible": false },
             { "targets": [16], "searchable": false, "visible": false },
             { "targets": [17], "searchable": false, "visible": false },
             { "targets": [18], "searchable": false, "visible": false },
             { "targets": [19], "searchable": false, "visible": false },
             { "targets": [20], "searchable": false, "visible": false },
             { "targets": [21], "searchable": false, "visible": false },
             { "targets": [22], "searchable": false, "visible": false },
             {
                 "targets": [10],
                 "type": "date",
                 "render": function (data) {
                     if (data !== null && data != "") {
                         var javascriptDate = new Date(data);
                         // console.log(data);
                         javascriptDate = javascriptDate.getMonth() + 1 + "/" + javascriptDate.getDate() + "/" + javascriptDate.getFullYear();
                         return javascriptDate;
                     } else {
                         return "";
                     }
                 }
             },
        ]
    });
}

function OnErrorCall() {
    alert("Something went wrong");
}
