﻿var serviceurl = "../../appServices/LocationService.asmx";
var commonService = "../../appServices/CommonService.asmx";
var tblname = "#tableData";
var tblAccount = "#tblAccount";
var tblDetail = "#tblItemList";
var table, table2, table3;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _ID = "#txtID";
var _LocationCode = "#txtLocationCode";
var _City = "#ddlCity";
var _Location = "#txtLocation";


var loginUrl = "/Default.aspx";


$(document).ready(function () {

    $("body #locationForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #locationForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #locationForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })

    OnLoadGrid();
    bindAddEditLocation();
    OnLoadGridAccount();
    OnDelete();
    FillCity();
    OnLoadGridAccountCodeList();
    OnSelectAccountCode();
});

/////////////adas

function OnLoadGridAccountCodeList() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGridDetail",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblDetail).DataTable().destroy();
            LoadDataTableDetail(response.d);
        },
        error: OnErrorCall
    });
}

function LoadDataTableDetail(datasource) {
    table3 = $(tblDetail).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [
                    {
                        data: null,
                        sDefaultContent: "",
                        "orderable": false,
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var select = ' <a href="#" class="select"><b>Select</b></a>';
                                var html = select;
                                return html;
                            }
                            return data;
                        }
                    },
                        { data: "AccountCode", sDefaultContent: "" },
                        { data: "AccountName", sDefaultContent: "" }

        ],
        "columnDefs": [
             //{ "targets": [1], "visible": false, "searchable": false },
        ]
    });
}

var ClickID;
function OnSearch(ID) {
    ClickID = ID;
    $("#H4").text("Account Name / Code");
    $("#modalView2").modal();
}
function OnSelectAccountCode() {
    $(tblDetail).on('click', '.select', function () {
        var row = $(this).closest('tr');
        var rowIndex = table3.row(row).index();
        var rowData = table3.row(rowIndex).data();

        $("#txtCode" + ClickID).text(rowData.AccountCode)
        $("#txtName" + ClickID).text(rowData.AccountName)
        $("#modalView2").modal('hide');
    });
}

function OnSave() {

    var isValid = true

    $("body #locationForm input").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    $("body #locationForm select").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    if (isValid) {

        var found = false;
        var tab1 = $(tblAccount).DataTable();
        var c1 = tab1.data().count();

        for (var i = 0; i < c1 ; i++) {
            var setRowData = $("#tblAccount tbody tr:eq(" + i + ")");
            var a = $(setRowData).find(".txtCode").text();
            if (a == "") {
                found = true
            }
            //Code.push(a.trim())
            // ReceivedDetail(i, a, data.ID);
        }

        if (found == false) {

            var Code = [];
            for (var i = 0; i < c1 ; i++) {
                var setRowData = $("#tblAccount tbody tr:eq(" + i + ")");
                var a = $(setRowData).find(".txtCode").text();
                Code.push(a.trim())
                // ReceivedDetail(i, a, data.ID);
            }
            console.log("Code", Code)
            var dataSend = {
                ID: $(_ID).val(),
                LocationCode: $(_LocationCode).val(),
                FK_CityID: $(_City).val(),
                Location: $(_Location).val(),
                Accounts_Receivable: Code[0],
                Sales_Credit: Code[1],
                Sales_Tax: Code[2],
                Sales_Return: Code[3],
                Stock: Code[4],
                CostofGoodSold: Code[5],
                Purchase: Code[6],
                LocalPurchase: Code[7],
                Discount: Code[8],
                Charges: Code[9],
                StockInTransit: Code[10],
                SalesReturn: Code[11],
                ImportCost: Code[12],
                ImportCostDr: Code[13]
            }


            $.ajax({
                type: "POST",
                url: "" + serviceurl + "/OnSave",
                data: JSON.stringify({ model: dataSend }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d == "sessionNotFound") {
                        alert("sessionNotFound");
                        window.location.href = loginUrl;
                    } else {
                        var data = JSON.parse(response.d);
                        console.log(data);
                        if (!data.Status) {
                            ClearFields();
                            $("#Validation").fadeIn(1000);
                            $("#Validation").text(data.Message);
                            $("#Validation").fadeOut(5000);
                            $("#modalAdd").modal('hide');
                        }
                        if (data.Message == "success") {
                            ClearFields();
                            $("#Validation").fadeIn(1000);
                            $("#Validation").text(success);
                            $("#Validation").fadeOut(5000);
                            OnLoadGrid();
                            $("#modalAdd").modal('hide');
                        } else if (data.Message == "exist") {
                            $("#Validation").fadeIn(1000);
                            $("#Validation").text(exist);
                            $("#Validation").fadeOut(5000);
                            OnLoadGrid();
                            $("#modalAdd").modal('hide');
                        } else if (data.Message == "update") {
                            $("#Validation").fadeIn(1000);
                            $("#Validation").text(update);
                            $("#Validation").fadeOut(5000);
                            OnLoadGrid();
                            $("#modalAdd").modal('hide');
                        }
                    }
                },
                error: OnErrorCall
            });


        } else {
            alert("Please Fill Codes")
        }
    }
}

function FormValidaton() {
    msg = "";

    $("#rLocationCode").addClass("hide");

    if ($(_LocationCode).val() == "") {
        msg += "Type is required";
        $("#rLocationCode").removeClass("hide");
    } else {
        $("#rLocationCode").addClass("hide");
    }

    $("#rLocation").addClass("hide");

    if ($(_Location).val() == "") {
        msg += "Type is required";
        $("#rLocation").removeClass("hide");
    } else {
        $("#rLocation").addClass("hide");
    }


    $("#rCity").addClass("hide");

    if ($(_City).val() == "-1") {
        msg += "City is required";
        $("#rCity").removeClass("hide");
    } else {
        $("#rCity").addClass("hide");
    }


    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}

function ClearFields() {
    $(_LocationCode).val("");
    $(_Location).val("");
    $(_City).val("-1")
    $(_ID).val("");

    $("#ValidationSummary").text("");

    $("#rLocationCode").addClass("hide");
    $("#rLocation").addClass("hide");
    $("#rCity").addClass("hide");
}

function OnLoadGrid() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            let data = JSON.parse(response.d)
            if (!data.Status) {
                $("#Validation").fadeIn(1000);
                $("#Validation").text(data.Message);
                $("#Validation").fadeOut(5000);
                $("#modalDelete").modal('hide');
            } else {
                $(tblname).DataTable().destroy();
                LoadDataTable(data.GenericList);
            }
        },
        error: OnErrorCall
    });
}
var dataa;

function OnLoadGridAccount() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGridAccount",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblAccount).DataTable().destroy();
            dataa = response.d;
            LoadDataTableAccount(response.d);
        },
        error: OnErrorCall
    });
}

function LoadDataTableAccount(datasource) {
    table2 = $(tblAccount).DataTable({
        "searching": false,
        "bLengthChange": false,
        "sSort": true,
        "bInfo": false,
        "bPaginate": false,
        dom: 'Bfrtip',
        buttons: [
             {
                 extend: 'print',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Location',
                 text: '<i class="fas fa-print"></i>  Print'

             },
             {
                 extend: 'excelHtml5',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Location',
                 text: '<i class="fas fa-file-excel"></i>  Excel',
             },
            {
                extend: 'pdf',
                footer: true,
                exportOptions: {
                    columns: [0, 1, 3]
                },
                title: 'Location',
                text: '<i class="fas fa-file-pdf"></i>  PDF'
            },
        ],
        "aaData": JSON.parse(datasource),
        "aoColumns": [

                        { data: null, sDefaultContent: "" },
                        { data: null, sDefaultContent: "" },
                        { data: null, sDefaultContent: "" }
        ],
        "columnDefs": [
            {
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '<p id="lblID" value="' + data.ID + '">' + data.ID + '</p>';
                }
            },
            {
                'targets': 1,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '<p>' + data.Title + '</p><button style="padding : 5px 5px !important;" type="button" class="btn btn-primary" onclick="OnSearch(' + data.ID + ')"><i class="fa fa-search"></i></button>';
                }
            },
            {
                'targets': 2,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '<p id="txtCode' + data.ID + '" class="txtCode" ></p><p class="txtName" id="txtName' + data.ID + '" ></p>';
                }
            },

            { "targets": [2], "width": "80%" },
           { "targets": [0], "searchable": false, "visible": false }

        ]
    });
}

function OnConfirmDelete() {
    Delete(DeleteID, DeleteName);
}

var DeleteID = "";
var DeleteName = "";
function OnDelete() {
    $(tblname).on('click', '.del', function () {
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData);
        DeleteID = rowData.ID;
        DeleteName = rowData.Location;
        $.ajax({
            type: "POST",
            url: "" + commonService + "/isAuthenticatedForDelete",
            data: JSON.stringify({ form: "Locations" }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        $("#modalDelete").modal();
                    }
                }
            },
            error: OnErrorCall
        });
    });
}

function getAccountName(ID, AccountCode) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/GetAccountName",
        data: JSON.stringify({ AccountCode: AccountCode }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d)
            $("#txtName" + ID).text(data[0].AccountName)
        },
        error: OnErrorCall
    });
}

function Delete(id, Name) {
    if (id != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDelete",
            data: JSON.stringify({ dataID: id, dataName: Name }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                        $("#modalDelete").modal('hide');
                    }
                    if (data.Message == "success") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(delt);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalDelete").modal('hide');

                    } else if (data.Message == "failed") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(failedmsg);
                        $("#Validation").fadeOut(5000);
                    } else if (data.Message == "use") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("It is used by another process, Please try again!");
                        $("#Validation").fadeOut(5000);
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}


function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        dom: 'Bfrtip',
        buttons: [
             {
                 extend: 'print',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Location',
                 text: '<i class="fas fa-print"></i>  Print'

             },
             {
                 extend: 'excelHtml5',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Location',
                 text: '<i class="fas fa-file-excel"></i>  Excel',
             },
            {
                extend: 'pdf',
                footer: true,
                exportOptions: {
                    columns: [0, 1, 3]
                },
                title: 'Location',
                text: '<i class="fas fa-file-pdf"></i>  PDF'
            },
        ],
        "aaData": datasource,
        "aoColumns": [

                        { data: "ID", sDefaultContent: "" },
                        { data: "Location", sDefaultContent: "" },
                        { data: "LocationCode", sDefaultContent: "" },
                        { data: "FK_CityID", sDefaultContent: "" },
                        { data: "cityName", sDefaultContent: "" },
                        { data: "Accounts_Receivable", sDefaultContent: "" },
                        { data: "Sales_Credit", sDefaultContent: "" },
                        { data: "Sales_Tax", sDefaultContent: "" },
                        { data: "Sales_Return", sDefaultContent: "" },
                        { data: "Stock", sDefaultContent: "" },
                        { data: "CostofGoodSold", sDefaultContent: "" },
                        { data: "Purchase", sDefaultContent: "" },
                        { data: "LocalPurchase", sDefaultContent: "" },
                        { data: "Discount", sDefaultContent: "" },
                        { data: "Charges", sDefaultContent: "" },
                        { data: "StockInTransit", sDefaultContent: "" },
                        { data: "SalesReturn", sDefaultContent: "" },
                        { data: "ImportCost", sDefaultContent: "" },
                        { data: "ImportCostDr", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" _recordId = ' + data.ID + ' class="del" title="del"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#" _recordId = ' + data.ID + ' class="edit" title="Edit"><i class="fas fa-pencil-alt" ></i></a>';
                                    var html = del + edit;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [
            { "targets": [3], "searchable": false, "visible": false },
            { "targets": [5], "searchable": false, "visible": false },
            { "targets": [6], "searchable": false, "visible": false },
            { "targets": [7], "searchable": false, "visible": false },
            { "targets": [8], "searchable": false, "visible": false },
            { "targets": [9], "searchable": false, "visible": false },
            { "targets": [10], "searchable": false, "visible": false },
            { "targets": [11], "searchable": false, "visible": false },
            { "targets": [12], "searchable": false, "visible": false },
            { "targets": [13], "searchable": false, "visible": false },
            { "targets": [14], "searchable": false, "visible": false },
            { "targets": [15], "searchable": false, "visible": false },
            { "targets": [16], "searchable": false, "visible": false },
            { "targets": [17], "searchable": false, "visible": false },
            { "targets": [18], "searchable": false, "visible": false },
          //  { "targets": [4], "searchable": false, "visible": false }

        ]
    });
}

function OnErrorCall() {
    alert("Something went wrong");
}


function FillCity() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetCityList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_City).empty();
            $(_City).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_City).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_City, $(_City).val())
        }
    });
}