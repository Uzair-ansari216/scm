﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Optimization


Public Class BundleConfig
    ' For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254726
    Public Shared Sub RegisterBundles(ByVal bundles As BundleCollection)
        bundles.Add(New ScriptBundle("~/bundles/ReportRoleDefineScript").Include(
            "~/FormScript/ReportRoleDefineScript.js"
      ))

        bundles.Add(New ScriptBundle("~/bundles/ReportScript").Include(
             "~/FormScript/ReportScript.js"
       ))
        bundles.Add(New ScriptBundle("~/bundles/ReportBuilderScript").Include(
             "~/FormScript/ReportBuilderScript.js"
       ))
        bundles.Add(New ScriptBundle("~/bundles/ReportSetupScript").Include(
             "~/FormScript/ReportSetupScript.js"
       ))


        bundles.Add(New ScriptBundle("~/bundles/ReportCategoryScript").Include(
             "~/FormScript/ReportCategoryScript.js"
       ))


        bundles.Add(New ScriptBundle("~/bundles/ProjectTypeScript").Include(
              "~/FormScript/ProjectTypeScript.js"
        ))
        bundles.Add(New ScriptBundle("~/bundles/ProjectTaskTypeScript").Include(
              "~/FormScript/ProjectTaskTypeScript.js"
        ))

        bundles.Add(New ScriptBundle("~/bundles/ChartOfAccountScript").Include(
             "~/FormScript/ChartOfAccountScript.js"
       ))

        bundles.Add(New ScriptBundle("~/bundles/Select2Script").Include(
             "~/Layout/assets/js/select2.min.js"
       ))

        bundles.Add(New ScriptBundle("~/bundles/LoginScript").Include(
            "~/FormScript/LoginScript.js"
      ))

        bundles.Add(New ScriptBundle("~/bundles/VoucherScript").Include(
           "~/FormScript/VoucherScript.js"
     ))

        bundles.Add(New ScriptBundle("~/bundles/GVoucherScript").Include(
          "~/FormScript/GVoucherScript.js"
    ))

        bundles.Add(New ScriptBundle("~/bundles/StatusApprovalScript").Include(
             "~/FormScript/StatusApprovalScript.js"
       ))

        bundles.Add(New ScriptBundle("~/bundles/SalesOrderScript").Include(
              "~/FormScript/SalesOrderScript.js"
        ))

        bundles.Add(New ScriptBundle("~/bundles/LocationScript").Include(
              "~/FormScript/LocationScript.js"
        ))

        bundles.Add(New ScriptBundle("~/bundles/SalesOrderNewScript").Include(
              "~/FormScript/SalesOrderNewScript.js"
        ))

        bundles.Add(New ScriptBundle("~/bundles/CustomeMethodScript").Include(
              "~/FormScript/customMethods.js"
        ))

        bundles.Add(New ScriptBundle("~/bundles/InvoiceScript").Include(
              "~/FormScript/InvoiceScript.js"
        ))
        bundles.Add(New ScriptBundle("~/bundles/GoodsDeliveryNoteScript").Include(
             "~/FormScript/GoodsDeliveryNoteScript.js"
       ))

        bundles.Add(New ScriptBundle("~/bundles/GoodReceivingNoteScript").Include(
             "~/FormScript/GoodReceivingNoteScript.js"
       ))

        bundles.Add(New ScriptBundle("~/bundles/MemberScript").Include(
              "~/FormScript/MemberScript.js"
        ))

        bundles.Add(New ScriptBundle("~/bundles/EmployeeScript").Include(
              "~/FormScript/EmployeeScript.js"
        ))
        bundles.Add(New ScriptBundle("~/bundles/SupplierScript").Include(
              "~/FormScript/SupplierScript.js"
        ))
        bundles.Add(New ScriptBundle("~/bundles/CommitteeScript").Include(
              "~/FormScript/CommitteeScript.js"
        ))
        bundles.Add(New ScriptBundle("~/bundles/GroupScript").Include(
              "~/FormScript/GroupScript.js"
        ))
        bundles.Add(New ScriptBundle("~/bundles/AccomodationScript").Include(
             "~/FormScript/AccomodationScript.js"
       ))
        bundles.Add(New ScriptBundle("~/bundles/AccommodationTypeScript").Include(
           "~/FormScript/AccommodationTypeScript.js"
     ))
        bundles.Add(New ScriptBundle("~/bundles/RoomFacilityScript").Include(
        "~/FormScript/RoomFacilityScript.js"
        ))

        bundles.Add(New ScriptBundle("~/bundles/RoomTypeScript").Include(
       "~/FormScript/RoomTypeScript.js"
       ))

        bundles.Add(New ScriptBundle("~/bundles/DealerScript").Include(
     "~/FormScript/DealerScript.js"
     ))

        bundles.Add(New ScriptBundle("~/bundles/RoomScript").Include(
    "~/FormScript/RoomScript.js"
        ))
        bundles.Add(New ScriptBundle("~/bundles/ManualEntryFormScript").Include(
   "~/FormScript/ManualEntryFormScript.js"
       ))

        bundles.Add(New ScriptBundle("~/bundles/RoomBookingRequestScript").Include(
  "~/FormScript/RoomBookingRequestScript.js"
))

        bundles.Add(New ScriptBundle("~/bundles/FaultTypeScript").Include(
           "~/FormScript/FaultTypeScript.js"
     ))

        bundles.Add(New ScriptBundle("~/bundles/CampResidenceFaultScript").Include(
           "~/FormScript/CampResidenceFaultScript.js"
     ))

        bundles.Add(New ScriptBundle("~/bundles/ComplainAssignScript").Include(
                   "~/FormScript/ComplainAssignScript.js"
             ))
        bundles.Add(New ScriptBundle("~/bundles/ComplainClosingScript").Include(
                   "~/FormScript/ComplainClosingScript.js"
             ))


        bundles.Add(New ScriptBundle("~/bundles/DataTable/js").Include(
              "~/dtLayout/DataTables-1.10.12/media/js/jquery.dataTables.min.js",
               "~/ dtLayout/DataTables-1.10.12/media/js/dataTables.bootstrap.min.js"
        ))

        bundles.Add(New ScriptBundle("~/bundles/DataTable/css").Include(
              "~/dtLayout/DataTables-1.10.12/media/css/dataTables.bootstrap.min.css"
        ))

        bundles.Add(New ScriptBundle("~/bundles/ClassGroupScript").Include(
             "~/FormScript/ClassGroupScript.js"
       ))

        bundles.Add(New ScriptBundle("~/bundles/PurchaseOrderScript").Include(
             "~/FormScript/PurchaseOrderScript.js"
       ))

        bundles.Add(New ScriptBundle("~/bundles/ItemClassScript").Include(
          "~/FormScript/ItemClassScript.js"
      ))
        bundles.Add(New ScriptBundle("~/bundles/ItemCategoryScript").Include(
            "~/FormScript/ItemCategoryScript.js"
        ))

        bundles.Add(New ScriptBundle("~/bundles/ItemScript").Include(
          "~/FormScript/ItemScript.js"
       ))

        bundles.Add(New ScriptBundle("~/bundles/CountryScript").Include(
          "~/FormScript/CountryScript.js"
        ))
        bundles.Add(New ScriptBundle("~/bundles/CityScript").Include(
         "~/FormScript/CityScript.js"
       ))
        bundles.Add(New ScriptBundle("~/bundles/DepartmentScript").Include(
           "~/FormScript/DepartmentScript.js"
       ))

        bundles.Add(New ScriptBundle("~/bundles/AssetBook").Include(
           "~/FormScript/AssetBook.js"
       ))


        bundles.Add(New ScriptBundle("~/bundles/AssetLocation").Include(
           "~/FormScript/AssetLocation.js"
       ))

        bundles.Add(New ScriptBundle("~/bundles/BrandScript").Include(
           "~/FormScript/BrandScript.js"
       ))
        bundles.Add(New ScriptBundle("~/bundles/CustomerGroupScript").Include(
            "~/FormScript/CustomerGroupScript.js"
       ))
        bundles.Add(New ScriptBundle("~/bundles/DisposalScript").Include(
            "~/FormScript/DisposalScript.js"
       ))
        bundles.Add(New ScriptBundle("~/bundles/CurrencyScript").Include(
           "~/FormScript/CurrencyScript.js"
      ))
        bundles.Add(New ScriptBundle("~/bundles/CompanyProfileScript").Include(
          "~/FormScript/CompanyProfileScript.js"
     ))
        bundles.Add(New ScriptBundle("~/bundles/FinancialYearScript").Include(
          "~/FormScript/FinancialYearScript.js"
     ))
        bundles.Add(New ScriptBundle("~/bundles/ProjectScript").Include(
          "~/FormScript/ProjectScript.js"
     ))
        bundles.Add(New ScriptBundle("~/bundles/AssetsClassScript").Include(
          "~/FormScript/AssetsClassScript.js"
     ))
        bundles.Add(New ScriptBundle("~/bundles/DealerScript").Include(
     "~/FormScript/DealerScript.js"
     ))
        bundles.Add(New ScriptBundle("~/bundles/UserActivityScript").Include(
     "~/FormScript/UserActivityScript.js"
     ))


        BundleTable.EnableOptimizations = True

    End Sub
End Class
