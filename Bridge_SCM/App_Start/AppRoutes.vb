﻿
Public Class AppRoutes

    Public Shared Sub Main(routes As System.Web.Routing.RouteCollection)
        '    routes.MapPageRoute(
        '    "HomeRoute",
        '    "Main",
        '    "~/Forms/Admin/Home.aspx"
        ')

        routes.MapPageRoute(
           "DefaultRoute",
           "Login",
           "~/Default.aspx"
       )
    End Sub

    Public Shared Sub Setups(routes As System.Web.Routing.RouteCollection)
        ' Setups
        
        'routes.MapPageRoute(
        '    "SupplierInformationRoute",
        '    "Setup/SupplierInformations",
        '    "~/Forms/Setup/SupplierInformation.aspx"
        ')

        routes.MapPageRoute(
           "Brand",
           "B@|r@|a|@n|d@2bf9bdbdb9378215462be9efc185c006",
           "~/Forms/Setup/Brand.aspx"
       )
        routes.MapPageRoute(
          "Country",
          "C|~o|!u|n$|t@|r^|y59716c97497eb9694541f7c3d37b1a4d",
          "~/Forms/Setup/Country.aspx"
      )
        routes.MapPageRoute(
          "City",
          "C|$i|@t|!y~57d056ed0984166336b7879c2af3657f",
          "~/Forms/Setup/City.aspx"
      )
        routes.MapPageRoute(
          "Supplier",
          "S|!u|$p|~p|l!|i$|e^|rec136b444eede3bc85639fac0dd06229",
          "~/Forms/Setup/Supplier.aspx"
      )
        routes.MapPageRoute(
          "CustomerGroup",
          "C|!u|$s|!t|o^|m@|e!|r$|G~|r$|o!|u~|p2c5176146d938e15c9f0e3d5203c19bf",
          "~/Forms/Setup/CustomerGroup.aspx"
      )
        routes.MapPageRoute(
          "Dealer",
          "D|!e|a$|l~|e@|rf01d4c0d85ece9b196ab4b8efc16c92b",
          "~/Forms/Setup/Dealer.aspx"
      )
        routes.MapPageRoute(
          "Currency",
          "C|!u|~r|r@|e$|n^|c$|y386c339d37e737a436499d423a77df0c",
          "~/Forms/Setup/Currency.aspx"
      )
        routes.MapPageRoute(
          "ClassGroup",
          "C|!l|a~|s|@s|G$|r^|o!|u~|p484b601fd2c5d6f5d901a0e0b544d60a",
          "~/Forms/Setup/ClassGroup.aspx"
      )
        routes.MapPageRoute(
          "ItemCategory",
          "I|~t|e$|m|@C|a^|t12|!e|g~|o|r@|y94c1dae2f7642a50f1aa990b799ca4a6",
          "~/Forms/Setup/ItemCategory.aspx"
      )
        routes.MapPageRoute(
          "ItemsClass",
          "I|$t|e~|m|!s|C$|l!|a~|s^|s1aa88813e709983b610c28cbae6a853f",
          "~/Forms/Setup/ItemsClass.aspx"
      )
        routes.MapPageRoute(
          "Items",
          "I|$t!|e~|m$|s9dea4016dbcc290b773ab2fae678aaa8",
          "~/Forms/Setup/Items.aspx"
      )
        routes.MapPageRoute(
          "PurchaseOrder",
          "P|!u|$r|c@|h|^a|s~|e4|O!|r$|d|@e|rc3190fc37651b2005302d53b58f0891f",
          "~/Forms/Setup/PurchaseOrder.aspx"
      )
        routes.MapPageRoute(
          "POStatus",
          "P|!O|S~|t$|a^|t$|u!|s3efd97c72e21afd28d7ebb567a80d7d3",
          "~/Forms/Setup/POStatus.aspx"
      )
        routes.MapPageRoute(
          "GoodReceivingNote",
          "G|1o~|o!|d$|R|e@|c~|e!|i@|v$|i~|n!|g$|N^|o|$t|~e3c18f1c8925b4c5c87e125b6cb302269",
          "~/Forms/Setup/GoodReceivingNote.aspx"
      )
        '   routes.MapPageRoute(
        '    "GRNCancel",
        '    "G|~R|N!|C|$a|2n|4c!|e~|l7307b463b12d24ae6173761b5c485dea",
        '    "~/Forms/Setup/GRNCancel.aspx"
        ')
        '   routes.MapPageRoute(
        '    "Bills",
        '    "B|~i|l!|l3|s5f1324ada7a2bf7adef6160762e21fc0f",
        '    "~/Forms/Setup/Bills.aspx"
        ')
        routes.MapPageRoute(
         "SalesOrderNew",
         "S|$a|@l|!e|s~|O$|r9|d~|e6|r^|N@|e|!wb91b3c4effbf1e6e2f9cebb8c1e4f897",
         "~/Forms/Setup/SalesOrderNew.aspx"
     )
        routes.MapPageRoute(
         "StatusApproval",
         "S|!t|$a|t~|u|s^|A3|p|4p|~r|o!|v|a@|l6fed3e15868a08be901f3a6e86009c4c",
         "~/Forms/Setup/StatusApproval.aspx"
     )
        routes.MapPageRoute(
         "GoodsDeliveryNote",
         "G|o!|o~|d@|s$|D^|e~|l@|i$|v~|e|r^|y$|N!|o|t~|ec388a680c72f1518a3199e9dfb3b48d7",
         "~/Forms/Setup/GoodsDeliveryNote.aspx"
     )
        routes.MapPageRoute(
         "Invoice",
         "I|!n|~v3|o5|$i|@c|!e466eadd40b3c10580e3ab4e8061161ce",
         "~/Forms/Setup/Invoice.aspx"
     )
        '   routes.MapPageRoute(
        '    "PropertiesList",
        '    "P|!r|~o|p$|e^|r2|t0|!i|e~|s|@L|i!|s~|t5971cfadee0367b30665428ef8e56af7",
        '    "~/Forms/Setup/PropertiesList.aspx"
        ')
        '   routes.MapPageRoute(
        '    "Report",
        '    "R|e~|p!|o$|r|^t4b1b4dc8cf38b3c64b1d657da8f5ac8c",
        '    "~/Forms/Setup/Report.aspx"
        ')
        '   routes.MapPageRoute(
        '    "BookingReportList",
        '    "B|$o|o@|k|!i|n~|g3|R|4e|p8|o$|r|!t|L~|i|^s|!td257cd24b62a32fa81079b505c9d7731",
        '    "~/Forms/Setup/BookingReportList.aspx"
        ')
        routes.MapPageRoute(
         "Department",
         "D|!e|~p|a$|r|^t|m$|e|!n|te2e4d5dc3ae70454b4e0d8d6a75d47d0",
         "~/Forms/Setup/Department.aspx"
     )
        routes.MapPageRoute(
         "Location",
         "L|$o|c!|a|~t|i3|o$|nce5bf551379459c1c61d2a204061c455",
         "~/Forms/Setup/Location.aspx"
     )
        routes.MapPageRoute(
         "Project",
         "P|~r|o!|j|$e|@c|1t9e727fdd3aec8274f46685441900280d",
         "~/Forms/Setup/Project.aspx"
     )
        routes.MapPageRoute(
         "Employee",
         "E|@m|!p|~l|o^|y4|e$|ef8c8b903cb2e4f297e4b96d4b9c1e98a",
         "~/Forms/Setup/Employee.aspx"
     )
        routes.MapPageRoute(
         "FinancialYear",
         "F|!i|~n|a$|n^|c$|i!|a|l~|Y4|e!|a~|rdb443c32d5fc86d44b05646b0f0fd995",
         "~/Forms/Setup/FinancialYear.aspx"
     )
        routes.MapPageRoute(
         "ChartOfAccount",
         "C|~h|a$|r!|t|@O|f$|A!|c~|c3!|o~|u$|n!|tebbe9a42836427f714bd6570f087ea87",
         "~/Forms/Setup/ChartOfAccount.aspx"
     )


        routes.MapPageRoute(
         "GVoucher",
         "G|$V|~o|u!|c|^h|e2|rdab6a724ee62a038eb513f68d54c8ca4",
         "~/Forms/Setup/GVoucher.aspx"
     )
        routes.MapPageRoute(
         "Voucher",
         "V|!o|~u|c$|h|^e|rbe686376cddb23d0227444ccc3c4b5b7",
         "~/Forms/Setup/Voucher.aspx"
     )
        routes.MapPageRoute(
         "ProfitAndLossStatement",
         "T%7C!r%7C$i%7C@a%7C~l%7CB%5E%7Ca2%7Cl0%7Ca4%7Cn@%7Cc!%7Ce%7C~N%7C%5Ee%7Cw4909f40405889e3adc08d3542aecc254",
         "~/Forms/Reports/ProfitAndLossStatement.aspx"
     )

        routes.MapPageRoute(
         "rptchartofaccountnew",
         "R^|P!|T!|C^|h^|a$|r|$t|O~|f|~A|4c|c3|o|5u|^n|!t|N^|e!|wfee9a63210607c231bac89a744a09aea",
         "~/Forms/Reports/rptchartofaccountnew.aspx"
     )
        routes.MapPageRoute(
         "GeneralLedgerfNew",
         "G|~e|!n|$e|@r|^a|!l|L|~e|d$|g!|e~|r!|f^|N1|e0|w36ad8987729e5d3f20c087113dc1c1b4",
         "~/Forms/Reports/GeneralLedgerfNew.aspx"
     )
        routes.MapPageRoute(
         "VendorAgeingNew",
         "V|1e|!n|~d|$o|r@|A^|g4|e6|i!|n~|!g|N$|e^|w3d43d5d477717dd405b0b10b7895c0a7",
         "~/Forms/Reports/VendorAgeingNew.aspx"
     )
        routes.MapPageRoute(
         "AuditTrailNew",
         "A|~u|!d|$i|@t|^T|~r|$a|!i|l^|N5|e2|w359e822654ba2c575065b7fa2b6d7886",
         "~/Forms/Reports/AuditTrailNew.aspx"
     )
        routes.MapPageRoute(
         "TrialBalanceNew",
         "T|!r|~i|a$|l!|B~|a$|l^|a@|n!|c~|e^|N$|e!|w4909f40405889e3adc08d3542aecc254",
         "~/Forms/Reports/TrialBalanceNew.aspx"
     )
        '   routes.MapPageRoute(
        '    "TrialBalanceNew",
        '    "T|!r|$i|@a|~l|B^|a2|l0|a4|n@|c!|e|~N|^e|w4909f40405889e3adc08d3542aecc254",
        '    "~/Forms/Setup/TrialBalanceNew.aspx"
        ')
        routes.MapPageRoute(
         "RoleDefineNew",
         "R|!o|~l|e$|D@|e!|f|3i|n0|e!|N^|e!|w9156937d65befb9b0bc17d24dda835a6",
         "~/Forms/Admin/RoleDefineNew.aspx"
     )
        routes.MapPageRoute(
         "UserNew",
         "U|!s|$e|@r|~N|e3|w0e4857c01443bdf909a25de26b0dcb6d",
         "~/Forms/Admin/UserNew.aspx"
     )
        routes.MapPageRoute(
         "FormControl",
         "Forms/Setup/FormControl.aspx",
         "~/Forms/Setup/FormControl.aspx"
     )
        routes.MapPageRoute(
         "ReportBuilder",
         "Forms/Setup/ReportBuilder.aspx",
         "~/Forms/Setup/ReportBuilder.aspx"
     )
        routes.MapPageRoute(
         "Trail Balance Mapping",
         "Forms/Setup/Financial.aspx",
         "~/Forms/Setup/Financial.aspx"
     )
        routes.MapPageRoute(
         "ChangePassword",
         "C|~h|a!|n|$g|e@|P|a~|s|^s|w5|o|3r|~d|N@|e|$w3646e111a9b8767e64aeee7028090f42",
         "~/Forms/Admin/ChangePassword.aspx"
     )
        routes.MapPageRoute(
          "Currency Exchange Rate",
          "C|!u|~r|r@|e$|n^|c$|Exy386c339d37e737Chga436499d423aRa77df0cte",
          "~/Forms/Setup/CurrencyExchangeRate.aspx"
      )

        routes.MapPageRoute(
       "Asset Book",
       "A|!s|$s|t~|u|s^|A3|E|4p|~r|o!|t|a@B|l6fed3e15868a0o8be901f3oa6e86009c4ck",
       "~/Forms/Setup/AssetBook.aspx"
   )

        routes.MapPageRoute(
  "Asset Location",
  "A|!s|$s|t~|u|s^|A3|E|4p|~r|o!|t|a@B|l6fed3eL5868a0o8be9O1f3Ca6e86AT009icocN",
  "~/Forms/Setup/AssetLocation.aspx"
  )



    End Sub

    
    Public Shared Sub Transaction(routes As System.Web.Routing.RouteCollection)
        routes.MapPageRoute(
        "ProductionOrderRoute",
        "Transaction/ProductionOrders",
        "~/Forms/Transaction/ProductionOrder.aspx"
    )
    

    End Sub

    


End Class
