﻿Imports System.Xml
Imports System.Data.SqlClient

Namespace BizSoft.DBManager
    Public Module Connection
        Dim strConnectionString As String = ""
        Public strTBLSQL As String = ""
        Public iLastRow As Long
        Public strTable As String
        Dim oDA As New SqlDataAdapter

        Dim oCB As SqlCommandBuilder
        Public lUpdateIDFieldOnInsert As Boolean = False
        Private lDisplayOnly As Boolean = False
        Public strSQLOrderBy As String

        Function SetConnect(ByVal ConfigurationName As String) As Data.SqlClient.SqlConnection

            If HttpContext.Current.Session("ConfigurationName") Is Nothing Then
                Dim objCnn As New Data.SqlClient.SqlConnection()
                Dim _Constr As String = ""
                Dim xDoc As New XmlDocument()
                Dim strConnectionString As String = System.Configuration.ConfigurationManager.ConnectionStrings(ConfigurationName).ConnectionString
                objCnn.ConnectionString = strConnectionString
                objCnn.Open()
                HttpContext.Current.Session("ConfigurationName") = objCnn
            End If
            Return CType(HttpContext.Current.Session("ConfigurationName"), Data.SqlClient.SqlConnection)

        End Function
        Function GetDataSet(ByVal Qry As String, Optional ByVal DataBase As String = "Default") As DataSet
            Dim objCmd As New Data.SqlClient.SqlCommand()
            Dim objAdb As New Data.SqlClient.SqlDataAdapter()
            Dim objDs As New DataSet()

            objCmd.Connection = SetConnect(DataBase)
            objCmd.CommandType = CommandType.Text
            objCmd.CommandText = Qry
            objAdb.SelectCommand = objCmd
            objAdb.Fill(objDs)
            Return objDs

        End Function

        Function ExecuteQry(ByVal Qry As String, Optional ByVal DataBase As String = "Default") As Integer
            Dim objCmd As New Data.SqlClient.SqlCommand()
            Dim objAdb As New Data.SqlClient.SqlDataAdapter()
            Dim objDs As New DataSet()

            objCmd.Connection = SetConnect(DataBase)
            objCmd.CommandType = CommandType.Text
            objCmd.CommandText = Qry
            objAdb.SelectCommand = objCmd
            Return objCmd.ExecuteNonQuery()
        End Function
        Function ExecuteSqlQuery(ByVal Qry As String, Optional ByVal DataBase As String = "Default") As String
            Dim objCmd As New Data.SqlClient.SqlCommand()
            Dim objAdb As New Data.SqlClient.SqlDataAdapter()

            objCmd.Connection = SetConnect(DataBase)
            objCmd.CommandType = CommandType.Text
            objCmd.CommandText = Qry
            objAdb.SelectCommand = objCmd
            Return objCmd.ExecuteScalar()
        End Function
        Function ExecuteQry1(ByVal Qry As String, Optional ByVal DataBase As String = "Default") As Integer
            Dim objCmd As New Data.SqlClient.SqlCommand()
            Dim objAdb As New Data.SqlClient.SqlDataAdapter()
            Dim objDs As New DataSet()

            objCmd.Connection = SetConnect(DataBase)
            objCmd.CommandType = CommandType.Text
            objCmd.CommandText = Qry
            objAdb.SelectCommand = objCmd
            Return objCmd.ExecuteScalar()


        End Function
        Function ExecuteQry2(ByVal Qry As String, Optional ByVal DataBase As String = "Default") As Integer
            Dim objCmd As New Data.SqlClient.SqlCommand()
            Dim objAdb As New Data.SqlClient.SqlDataAdapter()
            Dim objDs As New DataSet()

            objCmd.Connection = SetConnect(DataBase)
            objCmd.CommandType = CommandType.Text
            objCmd.CommandText = Qry
            objAdb.SelectCommand = objCmd
            Return objCmd.ExecuteScalar()

        End Function

        Function ExecuteQryDB2(ByVal Qry As String) As Integer

            Dim con As String = System.Configuration.ConfigurationManager.ConnectionStrings("Default2").ConnectionString()
            Dim objCnn As New Data.SqlClient.SqlConnection(con)
            Dim objCmd As New Data.SqlClient.SqlCommand()
            Dim objAdb As New Data.SqlClient.SqlDataAdapter()
            Dim objDs As New DataSet()
            objCnn.Open()
            objCmd.Connection = objCnn
            objCmd.CommandType = CommandType.Text
            objCmd.CommandText = Qry
            objAdb.SelectCommand = objCmd
            Dim status As Integer = objCmd.ExecuteNonQuery()
            objCnn.Close()
            Return status


        End Function

        Function GetDataSetDB2(ByVal Qry As String, Optional ByVal DataBase As String = "Default") As DataSet
            Dim con As String = System.Configuration.ConfigurationManager.ConnectionStrings("Default2").ConnectionString()
            Dim objCnn As New Data.SqlClient.SqlConnection(con)
            Dim objCmd As New Data.SqlClient.SqlCommand()
            Dim objAdb As New Data.SqlClient.SqlDataAdapter()
            Dim objDs As New DataSet()
            objCnn.Open()
            objCmd.Connection = objCnn
            objCmd.CommandType = CommandType.Text
            objCmd.CommandText = Qry
            objAdb.SelectCommand = objCmd
            objAdb.Fill(objDs)
            objCnn.Close()
            Return objDs

        End Function

        Private Function ExecuteSQL(ByVal strTable As String, ByRef pcSQL As String, ByRef oTable As DataTable, Optional ByVal lIndependent As Boolean = False, Optional ByVal DataBase As String = "Default") As String

            Dim objCmd As New Data.SqlClient.SqlCommand()
            Dim objAdb As New Data.SqlClient.SqlDataAdapter()
            Dim oDATable As Data.SqlClient.SqlDataAdapter 'OracleDataAdapter
            Dim objDs As New DataSet()
            Dim strSQL As String

            strSQL = ""
            Try
                objCmd.Connection = SetConnect(DataBase)
                'strSQL = MergeSQLSelect(lIndependent)
                strSQL = strSQL & pcSQL

                oDATable = New SqlDataAdapter(strSQL, objCmd.Connection)
                If strTable <> "" Then
                    oTable.TableName = strTable
                End If
                oDATable.Fill(oTable)
                ExecuteSQL = ""

            Catch e As Exception
                ExecuteSQL = e.Message
            End Try

        End Function
        Function ExecuteSP(ByVal SPName As String, ByVal Parameters As SqlClient.SqlParameter(), Optional ByVal DataBase As String = "Default") As DataSet
            Dim objCmd As New Data.SqlClient.SqlCommand()
            Dim objAdb As New Data.SqlClient.SqlDataAdapter()
            Dim objDs As New DataSet()

            objCmd.Connection = SetConnect(DataBase)
            objCmd.CommandType = CommandType.StoredProcedure
            objCmd.CommandText = SPName
            objCmd.Parameters.AddRange(Parameters)
            objAdb.SelectCommand = objCmd
            objAdb.Fill(objDs)
            objCmd.Parameters.Clear()
            Return objDs
        End Function
        Function ExecuteSPForInsertion(ByVal SPName As String, ByVal Parameters As Hashtable, Optional ByVal DataBase As String = "Default") As SqlCommand
            Dim objAdb As New Data.SqlClient.SqlDataAdapter()
            Dim objCnn As New Data.SqlClient.SqlConnection()
            Dim objDs As New DataSet()
            Dim strConnectionString As String = System.Configuration.ConfigurationManager.ConnectionStrings("Default").ConnectionString
            objCnn.ConnectionString = strConnectionString

            Dim comm As SqlCommand = New SqlCommand(SPName, objCnn)
            comm.CommandType = CommandType.StoredProcedure
            comm.CommandText = SPName
            For Each item As DictionaryEntry In Parameters
                comm.Parameters.AddWithValue(item.Key, item.Value)
            Next
            'comm.Parameters.AddRange(Parameters)
            objCnn.Open()
            comm.ExecuteNonQuery()
            'comm.Parameters.Clear()

            'Dim objCmd As New Data.SqlClient.SqlCommand()
            'objCmd.Connection = SetConnect(DataBase)

            ''objCmd.Connection.BeginTransaction()
            'objCmd.CommandType = CommandType.StoredProcedure
            'objCmd.CommandText = SPName

            'objCmd.Parameters.AddRange(Parameters)

            'objCmd.ExecuteNonQuery()
            'objCmd.Parameters.Clear()
            Return comm
        End Function
        Function GETMAXValue(ByVal TableName As String, ByVal FieldName As String, Optional ByVal DataBase As String = "Default") As Long
            Dim objCmd As New Data.SqlClient.SqlCommand()
            Dim objAdb As New Data.SqlClient.SqlDataAdapter()
            Dim objDs As New DataSet()
            Try
                objCmd.Connection = SetConnect(DataBase)
                objCmd.CommandType = CommandType.Text
                objCmd.CommandText = "Select isnull(Max(" & FieldName & "),0) from " & TableName
                objAdb.SelectCommand = objCmd
                objAdb.Fill(objDs)
                Dim id As Long = Convert.ToInt64(objDs.Tables(0).Rows(0)(0)) + 1
                Return Convert.ToInt64(objDs.Tables(0).Rows(0)(0)) + 1
            Catch ex As Exception
                Return 0
            End Try

        End Function



        Function GETMAXValueItem(ByVal FK_MainCategory As String, ByVal FK_Category As String, Optional ByVal DataBase As String = "Default") As String
            Dim objCmd As New Data.SqlClient.SqlCommand()
            Dim objAdb As New Data.SqlClient.SqlDataAdapter()
            Dim objDs As New DataSet()
            Try
                objCmd.Connection = SetConnect(DataBase)
                objCmd.CommandType = CommandType.Text
                objCmd.CommandText = "select  isnull(MAX(right(Code,6)),0) from  GS_Item where FK_MainCategory = " & FK_MainCategory & " AND FK_Category = " & FK_Category & ""
                objAdb.SelectCommand = objCmd
                objAdb.Fill(objDs)
                If Convert.ToInt64(objDs.Tables(0).Rows(0)(0)) = 0 Then
                    Return "000001"
                Else
                    Dim strVal As String = ("00000" & Convert.ToInt64(objDs.Tables(0).Rows(0)(0)) + 1).ToString()
                    Return strVal
                End If


            Catch ex As Exception
                Return 0
            End Try

        End Function
        'Public Function GetConnectionForReports(ByRef strServer As String, ByRef strUserID As String, ByRef strPassword As String, ByRef strDatabase As String, ByRef lIntegratedSecurity As Boolean) As Boolean
        '    strServer = System.Configuration.ConfigurationSettings.AppSettings("DBSERVER")
        '    strUserID = System.Configuration.ConfigurationSettings.AppSettings("DBUSERID")
        '    strPassword = System.Configuration.ConfigurationSettings.AppSettings("DBPASSWORD")
        '    strDatabase = System.Configuration.ConfigurationSettings.AppSettings("DBDATABASE")
        '    lIntegratedSecurity = False
        'End Function

        Public Function GetRowCount(ByVal strTable As String, ByRef oDS As DataSet) As Long
            'To find no. of rows --- should be optimized
            oDS.Clear()
            If strTable <> "" Then
                oDA.Fill(oDS, strTable)
            Else
                oDA.Fill(oDS)
            End If
            Return oDS.Tables(0).Rows.Count
        End Function

        Public Function GetRows(ByVal strTable As String, ByRef oDS As DataSet) As String

            GetRows = ""
            iLastRow = GetRowCount(strTable, oDS) - 1

        End Function


        Public Function GetRows(ByVal strTable As String, ByRef oTable As DataTable) As String
            Dim strSQL As String

            Try
                GetRows = ""
                If strTBLSQL <> "" Then
                    strSQL = ExecuteSQL(strTable, strTBLSQL, oTable, True)
                    If strSQL = "-1" Then
                        strSQL = ""
                    End If
                    GetRows = (Not IsNothing(strSQL) Or strSQL = "")
                    If GetRows Then
                        iLastRow = oTable.Rows.Count - 1
                        GetRows = ""
                    End If
                Else
                    oTable.Rows.Clear()
                    iLastRow = -1
                End If
            Catch e As Exception
                GetRows = e.Message
            End Try
        End Function

        Public Function Update(ByRef ods As DataSet, ByVal strTableName As String, ByRef mField As String) As String
            strTable = strTableName
            Update = Update(ods, mField)
        End Function


        Public Sub InitConnectionObjects(ByVal strDataBase As String, ByVal lCommandBuilder As Boolean, Optional ByVal DataBase As String = "Default")
            Dim objCmd As New Data.SqlClient.SqlCommand()

            Try


                'Fill SQL properties/commands in Data Adapter
                objCmd.Connection = SetConnect(DataBase)
                oDA = New SqlDataAdapter(strTBLSQL, objCmd.Connection)

                If lCommandBuilder Then
                    oCB = New SqlCommandBuilder(oDA)
                    oDA.DeleteCommand = oCB.GetDeleteCommand
                    oDA.InsertCommand = oCB.GetInsertCommand
                    oDA.UpdateCommand = oCB.GetUpdateCommand
                    oDA.SelectCommand.CommandTimeout = 0

                End If
                ' Include an event to fill in the Autonumber value.

            Catch e As Exception
                'Ignore error, which might be returned if proper SQL is sent by the programmer
                strTBLSQL = e.Message
            End Try

        End Sub

        Public Function Update(ByRef oDS As DataSet, ByVal mField As String) As String
            Dim oTempDs As New DataSet
            Update = ""

            If oDS.HasChanges Then
                oTempDs = oDS.GetChanges(DataRowState.Modified + DataRowState.Added + DataRowState.Deleted)
                If oDS.Tables.Contains(strTable) Then
                    oDA.Update(oTempDs, strTable)

                    'If Not Me.CallerID Is Nothing Then
                    '    MaintainLog(oDS, strTable)  'Log Old and New values
                    'End If
                    oDS.AcceptChanges()             'Call after logging old values
                End If

                '                Dim isAttempt As Boolean
                '                If strTable Is Nothing Then strTable = oDS.Tables(0).TableName
                '                Try
                'ReEntryPoint:
                '                    ' Include an event to fill in the Autonumber value.
                '                    'If Connection.SetConnect("Default") Then
                '                    If lUpdateIDFieldOnInsert Then
                '                        oDS.Tables(strTable).Columns("" & mField & "").AutoIncrement = True
                '                        oDS.Tables(strTable).Columns("" & mField & "").AutoIncrementSeed = -1
                '                        oDS.Tables(strTable).Columns("" & mField & "").AutoIncrementStep = -1
                '                        oDA.InsertCommand.CommandText += "; " & oDA.SelectCommand.CommandText & " WHERE " & mField & "=scope_identity()"
                '                        'oDA.InsertCommand.CommandText += "; " & oDA.SelectCommand.CommandText
                '                        oDA.InsertCommand.UpdatedRowSource = UpdateRowSource.FirstReturnedRecord

                '                        'Update dataset with the default table
                '                        If oDS.Tables.Contains(strTable) Then
                '                            Dim oTempDA As New SqlDataAdapter
                '                            oTempDA.DeleteCommand = oDA.DeleteCommand
                '                            oTempDA.UpdateCommand = oDA.UpdateCommand
                '                            oTempDA.InsertCommand = oDA.InsertCommand
                '                            'AddHandler oTempDA.RowUpdated, New SqlRowUpdatedEventHandler(AddressOf OnRowUpdated)

                '                            oTempDA.Update(oDS, strTable)

                '                            'If Not Me.CallerID Is Nothing Then
                '                            'MaintainLog(oDS, strTable)  'Log Old and New values
                '                            'End If
                '                            oDS.AcceptChanges()             'Call after logging old values
                '                        End If

                '                    Else
                '                        ''Update dataset with the default table
                '                        If oDS.Tables.Contains(strTable) Then
                '                            oDA.Update(oTempDs, strTable)

                '                            'If Not Me.CallerID Is Nothing Then
                '                            '    MaintainLog(oDS, strTable)  'Log Old and New values
                '                            'End If
                '                            oDS.AcceptChanges()             'Call after logging old values
                '                        End If
                '                    End If
                '                    ' End If

                '                Catch de As DBConcurrencyException
                '                    If Not lUpdateIDFieldOnInsert Then
                '                        Update = de.Message & "  Retrying Concurrency..."
                '                    End If
                '                    Update = ""
                '                    GoTo ReEntryPoint       'Handle concurrency problem, and overwrite changes
                '                    'MsgBox(de.Message)
                '                Catch ode As SqlException
                '                    Update = ode.Message
                '                    'MsgBox(ode.Message)
                '                Catch IOE As System.InvalidOperationException
                '                    Update = IOE.Message
                '                    'MsgBox(IOE.Message)
                '                Catch e As Exception
                '                    If isAttempt = False Then
                '                        Update = e.Message
                '                        Dim rowIndex As Integer
                '                        Dim colIndex As Integer
                '                        Dim i As Integer
                '                        For i = 0 To oDS.Tables(strTable).Rows.Count - 1
                '                            For colIndex = 0 To oDS.Tables(strTable).Columns.Count - 1
                '                                If CStr(oDS.Tables(strTable).Rows(rowIndex).Item(colIndex)) = "" Then
                '                                    oDS.Tables(strTable).Rows(rowIndex).Item(colIndex) = 0
                '                                End If
                '                            Next
                '                        Next
                '                        isAttempt = True
                '                        GoTo ReEntryPoint
                '                        'MsgBox(e.Message)
                '                    End If
                '                Finally

                '                End Try
            End If

        End Function

        Private Function MergeSQLSelect(ByVal strTable As String, ByVal lIndependent As Boolean) As String
            If Not lIndependent Then
                If oDA.SelectCommand.CommandText <> "" Then
                    Return oDA.SelectCommand.CommandText &
                        IIf(InStr(UCase(oDA.SelectCommand.CommandText), "WHERE") <> 0, " and ", " where ")
                Else
                    Return "Select * from " & strTable & " where "
                End If
            Else
                If Not lDisplayOnly Then
                    Return "Select * from " & strTable & " where "
                End If
            End If
            Return ""
        End Function

        Public Function GetData(ByVal strTable As String, ByVal strFilter As String, ByRef oDS As DataSet, Optional ByVal lIndependent As Boolean = False) As Boolean
            Dim strTempSQL As String
            Dim strSQL As String

            strSQL = MergeSQLSelect(strTable, lIndependent)
            strSQL = strSQL & strFilter
            If oDA.SelectCommand.CommandText = "" Then
                strTempSQL = oDA.SelectCommand.CommandText      'Save previous command text
                If Len(strSQLOrderBy) > 0 Then
                    strSQL += " Order by " & strSQLOrderBy
                End If
            Else
                strTempSQL = "Select * from " & strTable & ""
            End If
            oDA.SelectCommand.CommandText = strSQL
            strSQL = GetRows(strTable, oDS)
            oDA.SelectCommand.CommandText = strTempSQL      'Restore previous command text

            GetData = (Not IsNothing(strSQL) Or strSQL = "")

        End Function

        'Returns a DATASET of valid STRING values as matching:
        '<cField> = <cValue>
        Public Function GetData(ByVal strTable As String, ByVal cField As String, ByVal cValue As String, ByRef oDS As DataSet, Optional ByVal lIndependent As Boolean = False) As Boolean
            Dim strTempSQL As String
            Dim strSQL As String
            strSQL = MergeSQLSelect(strTable, lIndependent)
            strSQL = strSQL & cField & " = '" & cValue & "'"

            strTempSQL = oDA.SelectCommand.CommandText      'Save previous command text
            oDA.SelectCommand.CommandText = strSQL
            strSQL = GetRows(strTable, oDS)
            oDA.SelectCommand.CommandText = strTempSQL      'Restore previous command text

            GetData = (Not IsNothing(strSQL) Or strSQL = "")

        End Function

        Public Function GetData(ByVal strTable As String, ByVal strFilter As String, ByRef oDS As DataSet, ByRef lDeleted As Boolean, ByVal lIndependent As Boolean, Optional ByVal strOrderBy As String = "") As Boolean
            Dim strTemp As String

            strTemp = strSQLOrderBy
            strSQLOrderBy = strOrderBy
            GetData(strTable, strFilter, oDS, lIndependent)       'Update Dataset if found
            GetData = False
            strSQLOrderBy = strTemp                     'Change back old value

            If oDS.Tables.Count > 0 Then
                If oDS.Tables(0).Rows.Count > 0 Then
                    GetData = True
                End If
            End If
            lDeleted = False    'Not oTempRow.IsNull("dDeleted")
        End Function
        Public Function GetConnectionForReports(ByRef strServer As String, ByRef strUserID As String, ByRef strPassword As String, ByRef strDatabase As String, ByRef lIntegratedSecurity As Boolean) As ReportCredentials 'previously returning bolean
            Dim obj As New ReportCredentials
            With obj
                .Server = strServer
                .UserId = PerformCrypt(strUserID)
                .Password = PerformCrypt(strPassword.Trim()) '"$S" & PerformCrypt(strPassword.Trim()) & "#e"
                .Database = PerformCrypt(strDatabase)
                .lIntegratedSecurity = False
            End With
            'obj.Password = "$S" & obj.Password & "#e"
            'Dim pass = PerformCrypt("$S" & obj.Password & "#e")
            'Dim pass2 = PerformCrypt(pass)
            Return obj

            'Dim Server = strServer 'System.Configuration.ConfigurationSettings.AppSettings("DBSERVER")
            'Dim UserID = PerformCrypt(strUserID) 'PerformCrypt(System.Configuration.ConfigurationSettings.AppSettings("DBUSERID"))
            ' strPassword = System.Configuration.ConfigurationSettings.AppSettings("DBPASSWORD")
            'Dim Password = "$S" & PerformCrypt(strPassword.Trim()) & "#e"  'PerformCrypt((System.Configuration.ConfigurationSettings.AppSettings("DBPASSWORD")).Trim)
            'Dim Database = PerformCrypt(strDatabase) 'PerformCrypt(System.Configuration.ConfigurationSettings.AppSettings("DBDATABASE"))
            'lIntegratedSecurity = False
        End Function
        Public Function PerformCrypt(ByVal strText As String) As String
            'This function will perform the functionality of Encryption and Decryption as well
            Dim i As Integer
            Dim strTemp As String = ""
            For i = 1 To Len(strText)
                If Asc(Mid$(strText, i, 1)) < 128 Then
                    strTemp = Asc(Mid$(strText, i, 1)) + 128
                ElseIf Asc(Mid$(strText, i, 1)) > 128 Then
                    strTemp = Asc(Mid$(strText, i, 1)) - 128
                End If
                Mid$(strText, i, 1) = Chr(strTemp)
            Next i
            Return strText

        End Function
        Function GetDataSetForModuleReportParameter(ByVal Qry As String, server As String, userId As String, password As String, database As String) As DataSet
            Dim objCmd As New Data.SqlClient.SqlCommand()
            Dim objAdb As New Data.SqlClient.SqlDataAdapter()
            Dim objDs As New DataSet()

            objCmd.Connection = SetConnectForModuleReportParameter(server, userId, password, database)
            objCmd.CommandType = CommandType.Text
            objCmd.CommandText = Qry
            objAdb.SelectCommand = objCmd
            objAdb.Fill(objDs)
            Return objDs

        End Function
        Function SetConnectForModuleReportParameter(server As String, userId As String, password As String, database As String) As Data.SqlClient.SqlConnection
            Dim strServerC As String = server 'System.Configuration.ConfigurationSettings.AppSettings("DBSERVER")
            Dim strUserIDC As String = PerformCrypt(userId) 'PerformCrypt(System.Configuration.ConfigurationSettings.AppSettings("DBUSERID"))
            ' Dim strPasswordC As String = System.Configuration.ConfigurationSettings.AppSettings("DBPASSWORD")
            Dim strPasswordC As String = PerformCrypt((password).Trim) '"$S" & PerformCrypt((System.Configuration.ConfigurationSettings.AppSettings("DBPASSWORD")).Trim) & "#e"
            Dim strDatabaseC As String = PerformCrypt(database) 'PerformCrypt(System.Configuration.ConfigurationSettings.AppSettings("DBDATABASE"))

            'If HttpContext.Current.Session("ConfigurationName") Is Nothing Then
            Dim objCnn As New Data.SqlClient.SqlConnection()
            Dim _Constr As String = ""
            Dim xDoc As New XmlDocument()
            'Dim strConnectionString As String = System.Configuration.ConfigurationManager.ConnectionStrings(ConfigurationName).ConnectionString
            Dim strConnectionString As String = "Data Source=" & strServerC & ";Initial Catalog= " & strDatabaseC & " ;User ID=" & strUserIDC & ";Password=" & strPasswordC & ";"
            objCnn.ConnectionString = strConnectionString
            objCnn.Open()
            HttpContext.Current.Session("ConfigurationName") = objCnn

            'End If
            Return CType(HttpContext.Current.Session("ConfigurationName"), Data.SqlClient.SqlConnection)

        End Function

        Public Function GetDS(ByVal strTable As String, ByRef oDS As DataSet, ByRef oRow As DataRow) As String
            Try
                GetDS = ""
                oDS.Clear()
                oDA.FillSchema(oDS, SchemaType.Mapped, strTable)
                oRow = oDS.Tables(strTable).NewRow
            Catch e As Exception
                GetDS = e.Message
            End Try
        End Function


        Public Sub CallNew(ByVal strDataBase As String, ByVal strSql As String, ByVal lDisplay As Boolean)
            lDisplayOnly = lDisplay

            'Use specified query
            strTBLSQL = strSql

            InitConnectionObjects(strDataBase, False)
        End Sub

        Public Sub CallNew(ByVal strDataBase As String, ByVal strTbl As String, Optional ByVal strSql As String = "")
            strTable = strTbl

            If IsNothing(strSql) Or strSql = "" Then
                'Use default query for the specified table
                strTBLSQL = "SELECT * FROM " & strTable & "  "
            Else
                'Use specified query
                strTBLSQL = strSql
            End If

            If strTable <> "" Then
                InitConnectionObjects(strDataBase, True)
            End If
        End Sub

    End Module


End Namespace
