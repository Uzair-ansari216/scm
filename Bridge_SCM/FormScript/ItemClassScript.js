﻿
var serviceurl = "../../appServices/ItemClassService.asmx";
var tblname = "#tableCity";
var table;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _ID = "#txtID";
var _Group = "#ddlgroup";
var _Name = "#txtname";
var _Description = "#txtdescription";
var _Invoice = "#invoice";
var _Active = "#active";

var loginUrl = "/Default.aspx";

$(document).ready(function () {

    $("body #itemClassForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #itemClassForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #itemClassForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })

    OnLoadGrid();
    OnDelete();
    OnEdit();
    FillClassGroup();

});


function OnSave() {

    var isValid = true

    $("body #itemClassForm input").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    $("body #itemClassForm select").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    if (isValid) {
        var invoice;
        var active;

        if ($(_Active).is(':checked')) {

            active = 1;
        }
        else {
            active = 0;
        }

        if ($(_Invoice).is(':checked')) {
            invoice = 1;
        }
        else {
            invoice = 0;
        }

        var dataSend = {
            ID: $(_ID).val(),
            ItemClass: $(_Name).val(),
            FK_ClassGroup: $(_Group).val(),
            Description: $(_Description).val(),
            isInvoice: invoice,
            isActive: active

        }
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnSave",
            data: JSON.stringify({ model: dataSend }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        ClearFields();
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(success);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "exist") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(exist);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "update") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(update);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    }
                }
            },
            error: OnErrorCall
        });
    }
}

function FormValidaton() {
    msg = "";

    $("#rname").addClass("hide");

    if ($(_Name).val() == "") {
        msg += "required";
        $("#rname").removeClass("hide");
    } else {
        $("#rname").addClass("hide");
    }


    $("#rdescription").addClass("hide");

    if ($(_Description).val() == "") {
        msg += "required";
        $("#rdescription").removeClass("hide");
    } else {
        $("#rdescription").addClass("hide");
    }


    $("#rGroup").addClass("hide");

    if ($(_Group).val() == "-1") {
        msg += "required";
        $("#rGroup").removeClass("hide");
    } else {
        $("#rGroup").addClass("hide");
    }

    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}

function ClearFields() {

    $(_Name).val("");
    $(_Description).val("");
    $(_ID).val("");

    $(_Group).val("-1");

    $('input[id=active]').attr('checked', false);
    $('input[id=active]').prop('checked', false);

    $('input[id=invoice]').attr('checked', false);
    $('input[id=invoice]').prop('checked', false);


    $("#ValidationSummary").text("");
    $("#rdescription").addClass("hide");
    $("#rname").addClass("hide");
}

function OnLoadGrid() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblname).DataTable().destroy();
            LoadDataTable(response.d);
        },
        error: OnErrorCall
    });


}

function OnConfirmDelete() {
    Delete(DeleteID);
}

var DeleteID = "";
function OnDelete() {
    $(tblname).on('click', '.del', function () {
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData);
        DeleteID = rowData.ID;

        if (DeleteID != "") {
            $("#modalDelete").modal();
        }

    });
}

function OnEdit() {
    $(tblname).on('click', '.edit', function () {
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData);

        $(_ID).val(rowData.ID);
        $(_Group).val(rowData.Fk_ClassGroup);
        $(_Name).val(rowData.ItemClass);
        $(_Description).val(rowData.Description);



        if (rowData.isActive == "True") {
            $('input[id=active]').attr('checked', true);
            $('input[id=active]').prop('checked', true);
        }
        else {
            $('input[id=active]').attr('checked', false);
            $('input[id=active]').prop('checked', false);
        }

        if (rowData.isInvoice == "True") {
            $('input[id=invoice]').attr('checked', true);
            $('input[id=invoice]').prop('checked', true);
        }
        else {
            $('input[id=invoice]').attr('checked', false);
            $('input[id=invoice]').prop('checked', false);
        }


        $("#formModalLabel").text("Edit");
        $("#modalAdd").modal();


        $("#ValidationSummary").text("");
        $("#rdescription").addClass("hide");
        $("#rname").addClass("hide");
        $("#rGroup").addClass("hide");
        $(".label-floating").addClass("is-focused");

        clearAllMessages("itemClassForm")

        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("Item Class", "itemClassForm")
        }
        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("Item Class", "itemClassForm", service_DeveloperMode)

        //}
    });
}

function Delete(id) {
    if (id != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDelete",
            data: JSON.stringify({ dataID: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(delt);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalDelete").modal('hide');

                    } else if (data.Message == "failed") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(failedmsg);
                        $("#Validation").fadeOut(5000);
                    } else if (data.Message == "exist") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("It is used by another process, Please try again!");
                        $("#Validation").fadeOut(5000);
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}


function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        dom: 'Bfrtip',
        buttons: [
             {
                 extend: 'print',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Item Class',
                 text: '<i class="fa fa-lg fa-file-text-o"></i>  Print'

             },
             {
                 extend: 'excelHtml5',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Item Class',
                 text: '<i class="fa fa-lg fa-file-excel-o"></i>  Excel',
             },
            {
                extend: 'pdf',
                footer: true,
                exportOptions: {
                    columns: [0, 1, 3]
                },
                title: 'Item Class',
                text: '<i class="fa fa-lg fa-file-pdf-o"></i>  PDF'
            },
        ],
        "aaData": JSON.parse(datasource),
        "aoColumns": [

                        { data: "ID", sDefaultContent: "" },
                        { data: "ItemClass", sDefaultContent: "" },
                        { data: "ClassGroup", sDefaultContent: "" },
                        { data: "Description", sDefaultContent: "" },
                        { data: "isActive", sDefaultContent: "" },
                        { data: "isInvoice", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" class="del" title="del"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#" class="edit" title="Edit"><i class="fas fa-pencil-alt" ></i></a>';
                                    var html = del + edit;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [

            { "targets": [0], "width": "5%" },
            { "targets": [1], "width": "15%" },
            { "targets": [2], "width": "20%" },
            { "targets": [3], "width": "10%" },
            { "targets": [4], "width": "10%" },
            { "targets": [5], "width": "10%" },
            { "targets": [6], "width": "10%" }
        ]
    });
}

function OnAdd() {
    $(".label-floating").removeClass("is-focused");
    ClearFields();
    $("#formModalLabel").text("Add");
    $("#modalAdd").modal();

    clearAllMessages("itemClassForm")

    var service_DeveloperMode = $("#hdnDevMode").val()
    if (service_DeveloperMode == 'on') {
        addFormFields("Item Class", "itemClassForm")
    }
    //var service_ToolTip = $("#hdnToolTip").val()
    //if (service_ToolTip == 'on') {

    setToolTip("Item Class", "itemClassForm", service_DeveloperMode)

    //}
}


function OnErrorCall() {
    alert("Something went wrong");
}





function FillClassGroup() {

    $.ajax({
        type: "POST",
        url: serviceurl + '/GetClassGroup',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Group).empty();
            $(_Group).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Group).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
            //$(_dependent).select2('val', index);

        }
        //failure: function (errMsg) {
        //    alert(errMsg);
        //}
    });

}

