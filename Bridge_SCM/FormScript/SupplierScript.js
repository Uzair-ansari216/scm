﻿var serviceurl = "../../appServices/SupplierService.asmx";
var commonService = "../../appServices/CommonService.asmx";

var tblname = "#tableData";
var table;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _ID = "#txtID";
var _Company = "#txtCompany";
var _Address = "#txtAddress";
var _Phone = "#txtPhone";
var _Fax = "#txtFax";
var _Country = "#ddlCountry";
var _City = "#ddlCity";
var _LocationID = "#ddlLocation";
var _Email = "#txtEmail";
var _Terms = "#txtTerms";
var _CreditLimit = "#txtCreditLimit";
var _NTN = "#txtNTN";
var _STN = "#txtSTN";
var _ContactPerson = "#txtContactPerson";
var _Phone1 = "#txtPhone1";
var _AltContact = "#txtAltContact";
var _AltPhone = "#txtAltPhone";
var _Debtor = "#txtDebtor";
var _OpeningBalanceD = "#txtOpeningBalanceD";
var _Creditor = "#txtCreditor";
var _OpeningBalanceC = "#txtOpeningBalanceC";
var _Notes = "#txtNotes";
//var _GLAccount = "#txtGLAccount";
var _Mode;

var loginUrl = "/Default.aspx";


$(document).ready(function () {

    // Wizard style
    //Add blue animated border and remove with condition when focus and blur
    if ($('.fg-line')[0]) {
        $('body').on('focus', '.form-control', function () {
            $(this).closest('.fg-line').addClass('fg-toggled');
        })

        $('body').on('blur', '.form-control', function () {
            var p = $(this).closest('.form-group');
            var i = p.find('.form-control').val();

            if (p.hasClass('fg-float')) {
                if (i.length == 0) {
                    $(this).closest('.fg-line').removeClass('fg-toggled');
                }
            }
            else {
                $(this).closest('.fg-line').removeClass('fg-toggled');
            }
        });
    }

    //Add blue border for pre-valued fg-flot text feilds
    if ($('.fg-float')[0]) {
        $('.fg-float .form-control').each(function () {
            var i = $(this).val();

            if (!i.length == 0) {
                $(this).closest('.fg-line').addClass('fg-toggled');
            }

        });
    }


    /*   Form Wizard Functions  */
    /*--------------------------*/
    _handleTabShow = function (tab, navigation, index, wizard) {
        var total = navigation.find('li').length;
        var current = index + 0;
        var percent = (current / (total - 1)) * 100;
        var percentWidth = 100 - (100 / total) + '%';

        navigation.find('li').removeClass('done');
        navigation.find('li.active').prevAll().addClass('done');

        wizard.find('.progress-bar').css({ width: percent + '%' });
        $('.form-wizard-horizontal').find('.progress').css({ 'width': percentWidth });
    };

    _updateHorizontalProgressBar = function (tab, navigation, index, wizard) {
        var total = navigation.find('li').length;
        var current = index + 0;
        var percent = (current / (total - 1)) * 100;
        var percentWidth = 100 - (100 / total) + '%';

        wizard.find('.progress-bar').css({ width: percent + '%' });
        wizard.find('.progress').css({ 'width': percentWidth });
    };

    /* Form Wizard - Example 1  */
    /*--------------------------*/
    $('#formwizard_simple').bootstrapWizard({
        onTabShow: function (tab, navigation, index) {
            _updateHorizontalProgressBar(tab, navigation, index, $('#formwizard_simple'));
        }
    });

    /* Form Wizard - Example 2  */
    /*--------------------------*/

    $('#formwizard_validation').bootstrapWizard({
        onNext: function (tab, navigation, index) {
            var form = $('#formwizard_validation').find("form");
            var valid = true;

            if (index == 1) {
                var fname = form.find('#firstname');
                var lastname = form.find('#lastname');

                if (!fname.val()) {
                    swal("You must enter your first name!");
                    fname.focus();
                    return false;
                }

                if (!lastname.val()) {
                    swal("You must enter your last name!");
                    lastname.focus();
                    return false;
                }
            }

            if (!valid) {
                return false;
            }
        },
        onTabShow: function (tab, navigation, index) {
            _updateHorizontalProgressBar(tab, navigation, index, $('#formwizard_validation'));
        }
    });
    $(".form-wizard-nav ul").removeClass("nav-pills")
    // Wizard style end

    $("body #supplierForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #supplierForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #supplierForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })

    FillCountry();
    FillCity();
    FillLocation();
    OnLoadGrid();
    bindAddEditSupplier();
    OnDelete();
});


function FillCountry() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetCountryList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Country).empty();
            $(_Country).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Country).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_Country, $(_Country).val())
        }
    });
}

function FillLocation() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetLocationList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_LocationID).empty();
            $(_LocationID).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_LocationID).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_LocationID, $(_LocationID).val())
        }
    });
}

function FillCity() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetCityList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_City).empty();
            $(_City).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_City).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_City, $(_City).val())
        }
    });
}

function OnLoadGrid() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblname).DataTable().destroy();
            LoadDataTable(response.d);
        },
        error: OnErrorCall
    });
}

function bindAddEditSupplier() {
    $("body").on("click", "#btnAdd , .edit", function () {

        let id = $(this).attr("_recordId")
        clearAllMessages("supplierForm")

        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("Supplier", "supplierForm")
        }
        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("Supplier", "supplierForm", service_DeveloperMode)

        //}

        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/AddEditSupplier",
            data: JSON.stringify({ id: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        if (id != "0") {
                            _Mode = "Edit";
                            $("#formModalLabel").text("Edit");
                            $(".label-floating").addClass("is-focused");
                        } else {
                            _Mode = "Add";
                            $("#formModalLabel").text("Add");
                            $(".label-floating").removeClass("is-focused");
                        }
                        $(_ID).val(data.GenericList.SupplierID)
                        $(_ID).attr('readonly', true);
                        $(_Company).val(data.GenericList.Company)
                        $(_Address).val(data.GenericList.Address)
                        $(_ContactPerson).val(data.GenericList.Contact)
                        $(_Phone1).val(data.GenericList.ContactPhone)
                        $(_Phone).val(data.GenericList.Phone)
                        $(_Fax).val(data.GenericList.Fax)
                        $(_Email).val(data.GenericList.Email)
                        $(_AltContact).val(data.GenericList.AltContact)
                        $(_AltPhone).val(data.GenericList.AltPhone)
                        $(_Terms).val(data.GenericList.Terms)
                        $(_CreditLimit).val(data.GenericList.CreditLimit)
                        $(_Notes).val(data.GenericList.Notes)
                        $(_LocationID).val(data.GenericList.LocationID)
                        $(_Debtor).val(data.GenericList.DebtorAccountCode)
                        $(_OpeningBalanceD).val(data.GenericList.DebtorOpBal)
                        $(_Creditor).val(data.GenericList.CreditorAccountCode)
                        $(_OpeningBalanceC).val(data.GenericList.CreditorOpBal)
                        $(_NTN).val(data.GenericList.NTN)
                        $(_STN).val(data.GenericList.STN)
                        $(_City).val(data.GenericList.cityID)
                        $(_Country).val(data.GenericList.countryID)

                        $("#modalAdd").modal();
                    }

                }
            },
            error: OnErrorCall,
            complete: function () {
                isValueExist(_Company, $(_Company).val())
                isValueExist(_ContactPerson, $(_ContactPerson).val())
                isValueExist(_Address, $(_Address).val())
                isValueExist(_Phone1, $(_Phone1).val())
                isValueExist(_Phone, $(_Phone).val())
                isValueExist(_Fax, $(_Fax).val())
                isValueExist(_Email, $(_Email).val())
                isValueExist(_AltContact, $(_AltContact).val())
                isValueExist(_AltPhone, $(_AltPhone).val())
                isValueExist(_Terms, $(_Terms).val())
                isValueExist(_CreditLimit, $(_CreditLimit).val())
                isValueExist(_Notes, $(_Notes).val())
                isValueExist(_LocationID, $(_LocationID).val())
                isValueExist(_Debtor, $(_Debtor).val())
                isValueExist(_OpeningBalanceD, $(_OpeningBalanceD).val())
                isValueExist(_Creditor, $(_Creditor).val())
                isValueExist(_NTN, $(_NTN).val())
                isValueExist(_OpeningBalanceC, $(_OpeningBalanceC).val())
                isValueExist(_STN, $(_STN).val())
                isValueExist(_City, $(_City).val())
                isValueExist(_Country, $(_Country).val())
            }
        });
    })
}


function OnSave() {


    var isValid = true

    $("body #supplierForm input").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    $("body #supplierForm select").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    if (isValid) {
        var dataSend = {
            Mode: _Mode,
            SupplierID: $(_ID).val(),
            Company: $(_Company).val(),
            Address: $(_Address).val(),
            Contact: $(_ContactPerson).val(),
            ContactPhone: $(_Phone1).val(),
            Phone: $(_Phone).val(),
            Fax: $(_Fax).val(),
            Email: $(_Email).val(),
            AltContact: $(_AltContact).val(),
            AltPhone: $(_AltPhone).val(),
            Terms: $(_Terms).val(),
            CreditLimit: $(_CreditLimit).val(),
            Notes: $(_Notes).val(),
            LocationID: $(_LocationID).val(),
            DebtorAccountCode: $(_Debtor).val(),
            DebtorOpBal: $(_OpeningBalanceD).val(),
            CreditorAccountCode: $(_Creditor).val(),
            CreditorOpBal: $(_OpeningBalanceC).val(),
            NTN: $(_NTN).val(),
            STN: $(_STN).val(),
            cityID: $(_City).val(),
            countryID: $(_Country).val()
        }
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnSave",
            data: JSON.stringify({ model: dataSend }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        ClearFields();
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(success);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "exist") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(exist);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "update") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(update);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    }
                }
            },
            error: OnErrorCall
        });
    }
}


function FormValidaton() {
    msg = "";

    $("#rName").addClass("hide");


    if ($(_Name).val() == "") {
        msg += "Type is required";
        $("#rName").removeClass("hide");
    } else {
        $("#rName").addClass("hide");
    }

    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}

function ClearFields() {
    $(_ID).val("")
    $(_ID).attr('readonly', false)
    $(_Company).val("")
    $(_Address).val("")
    $(_ContactPerson).val("")
    $(_Phone1).val("")
    $(_Phone).val("")
    $(_Fax).val("")
    $(_Email).val("")
    $(_AltContact).val("")
    $(_AltPhone).val("")
    $(_Terms).val("")
    $(_CreditLimit).val("")
    $(_Notes).val("")
    $(_LocationID).val("-1")
    $(_Debtor).val("")
    $(_OpeningBalanceD).val("")
    $(_Creditor).val("")
    $(_OpeningBalanceC).val("")
    $(_NTN).val("")
    $(_STN).val("")
    $(_City).val("-1")
    $(_Country).val("-1")

    $("#ValidationSummary").text("");
    $("#rID").addClass("hide");
    $("#rCompany").addClass("hide");
    $("#rAddress").addClass("hide");
    $("#rPhone").addClass("hide");
    $("#rFax").addClass("hide");
    $("#rCountry").addClass("hide");
    $("#rCity").addClass("hide");
    $("#rLocation").addClass("hide");
    $("#rEmail").addClass("hide");
    $("#rTerms").addClass("hide");
    $("#rCreditLimit").addClass("hide");
    $("#rNTN").addClass("hide");
    $("#rSTN").addClass("hide");
    $("#rContactPerson").addClass("hide");
    $("#rPhone1").addClass("hide");
    $("#rAltContact").addClass("hide");
    $("#rAltPhone").addClass("hide");
    $("#rDebtor").addClass("hide");
    $("#rOpeningBalanceD").addClass("hide");
    $("#rCreditor").addClass("hide");
    $("#rOpeningBalanceC").addClass("hide");
    $("#rNotes").addClass("hide");
    //$("#rGLAccount").addClass("hide");
}


var DeleteID = "";
function OnDelete() {
    $(tblname).on('click', '.del', function () {
        let id = $(this).attr('_recordId');
        DeleteID = id;
        $.ajax({
            type: "POST",
            url: "" + commonService + "/isAuthenticatedForDelete",
            data: JSON.stringify({ form: "Supplier" }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        $("#modalDelete").modal();
                    }
                }
            },
            error: OnErrorCall
        });
    });
}

function OnConfirmDelete() {
    Delete(DeleteID);
}

function Delete(id) {
    if (id != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDelete",
            data: JSON.stringify({ dataID: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(delt);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalDelete").modal('hide');

                    } else if (data.Message == "failed") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(failedmsg);
                        $("#Validation").fadeOut(5000);
                    } else if (data.Message == "use") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("It is used by another process, Please try again!");
                        $("#Validation").fadeOut(5000);
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}


function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        dom: 'Bfrtip',
        buttons: [
             {
                 extend: 'print',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Supplier',
                 text: '<i class="fa fa-print"></i>  Print'

             },
             {
                 extend: 'excelHtml5',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Supplier',
                 text: '<i class="fa fa-file-excel"></i>  Excel',
             },
            {
                extend: 'pdf',
                footer: true,
                exportOptions: {
                    columns: [0, 1, 3]
                },
                title: 'Supplier',
                text: '<i class="fa fa-file-pdf"></i>  PDF'
            },
        ],
        "aaData": JSON.parse(datasource),
        "aoColumns": [

                        { data: "SupplierID", sDefaultContent: "" },
                        { data: "Company", sDefaultContent: "" },
                        { data: "Address", sDefaultContent: "" },
                        { data: "Contact", sDefaultContent: "" },
                        { data: "ContactPhone", sDefaultContent: "" },
                        { data: "Phone", sDefaultContent: "" },
                        { data: "Fax", sDefaultContent: "" },
                        { data: "Email", sDefaultContent: "" },
                        { data: "AltContact", sDefaultContent: "" },
                        { data: "AltPhone", sDefaultContent: "" },
                        { data: "Terms", sDefaultContent: "" },
                        { data: "CreditLimit", sDefaultContent: "" },
                        { data: "Notes", sDefaultContent: "" },
                        { data: "LocationID", sDefaultContent: "" },
                        { data: "DebtorAccountCode", sDefaultContent: "" },
                        { data: "DebtorOpBal", sDefaultContent: "" },
                        { data: "CreditorAccountCode", sDefaultContent: "" },
                        { data: "CreditorOpBal", sDefaultContent: "" },
                        { data: "NTN", sDefaultContent: "" },
                        { data: "STN", sDefaultContent: "" },
                        { data: "cityID", sDefaultContent: "" },
                        { data: "countryID", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" _recordId = ' + data.SupplierID + ' class="del" title="del"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#" _recordId = ' + data.SupplierID + ' class="edit" title="Edit"><i class="fas fa-pencil-alt" ></i></a>';
                                    var html = del + edit;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [
             //{ "targets": [2], "searchable": false, "visible": false },
             { "targets": [3], "searchable": false, "visible": false },
             { "targets": [4], "searchable": false, "visible": false },
             { "targets": [6], "searchable": false, "visible": false },
             { "targets": [8], "searchable": false, "visible": false },
             { "targets": [9], "searchable": false, "visible": false },
             { "targets": [10], "searchable": false, "visible": false },
             { "targets": [11], "searchable": false, "visible": false },
             { "targets": [12], "searchable": false, "visible": false },
             { "targets": [13], "searchable": false, "visible": false },
             { "targets": [14], "searchable": false, "visible": false },
             { "targets": [15], "searchable": false, "visible": false },
             { "targets": [16], "searchable": false, "visible": false },
             { "targets": [17], "searchable": false, "visible": false },
             { "targets": [18], "searchable": false, "visible": false },
             { "targets": [19], "searchable": false, "visible": false },
             { "targets": [20], "searchable": false, "visible": false },
             { "targets": [21], "searchable": false, "visible": false },

        ]
    });
}

function OnErrorCall() {
    alert("Something went wrong");
}
