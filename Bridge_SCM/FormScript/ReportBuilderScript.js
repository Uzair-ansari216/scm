﻿var pagename = "Report.aspx";
var ServiceUrl = "../../appServices/ReportService.asmx";


var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";

var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";


var _ddlNA = "#ddlNA";
var _ddlPA = "#ddlPA";
var _ddlUC = "#ddlUC";
var _ddlWard = "#ddlWard";
var _ddlBlock = "#ddlBlock";
var _ddlArea = "#ddlArea";
var _ddlStreet = "#ddlStreet";
var _ddlReports = "#ddlReports";
var _ToDate = "#txtToDate";
var _FromDate = "#txtFromDate";
var _FromTime = "#txtFromTime";
var _ToTime = "#txtToTime";
var _ddlReportCategory = "#ddlReportCategory";
var _ddlReportModule = "#ddlReportModule";

$(document).ready(function () {
    GetReportType()
    OnChangeNA();
    NationalAssembly();
    //FillsReports();
    OnChangePA();
    OnChangeUC();
    OnChangeWard();
    OnChangeBlock();
    OnChangeArea();
    $(_ToDate).datepicker({
        autoclose: true
    });
    $(_FromDate).datepicker({
        autoclose: true
    });
    fillReportModule();
    //FillReportsCategory();
    OnChangeReportCategory();
    OnChangeReportModule();
    $(_FromTime).timepicker({
        'scrollDefaultNow': true,
        'timeFormat': 'H:i:s'
    })

    $(_ToTime).timepicker({
        'scrollDefaultNow': true,
        'timeFormat': 'H:i:s'
    })
    //$(_ToDate).val(GetToday());
    //$(_FromDate).val(GetToday());
    $(_FromDate).val($(".datefrom").val().replace("00:00:00 AM", ""));
    $(_ToDate).val($(".dateto").val().replace("23:59:59 AM", ""));

    $(_FromTime).val("00:00:00")
    $(_ToTime).val("23:59:59")
    Find()
    $(".cmb1").hide()
    $(".cmb2").hide()
    $(".cmb3").hide()
    $(".cmb4").hide()

    $(".txt1").hide()
    $(".txt2").hide()
    $(".txt3").hide()
    $(".txt4").hide()

    $("#xlsx").css({ "display": "none" })

    $(".multi-select").select2({
        multiple: true,
    });
});

function GetReportType() {
    $.ajax({
        type: "POST",
        url: ServiceUrl + '/GetReportType',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $("#ReportType").val(data)
        }
    });

}
function GetToday() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var today = mm + '/' + dd + '/' + yyyy;
    return today;
}
//Report Module
function fillReportModule() {
    $.ajax({
        type: "POST",
        url: ServiceUrl + '/FillReportModule',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_ddlReportModule).empty();
            $(_ddlReportModule).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_ddlReportModule).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_ddlReportModule, $(_ddlReportModule).val())
        }
    });
}

function OnChangeReportModule() {
    $(_ddlReportModule).on('change', function () {
        var id = $(_ddlReportModule).val();
        //alert(id)
        if (id != "") {
            $.ajax({
                type: "POST",
                url: "" + ServiceUrl + "/FillReportCategories",
                data: JSON.stringify({ ID: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var data = JSON.parse(response.d);
                    $(_ddlReportCategory).empty();
                    $(_ddlReports).empty();
                    $(_ddlReportCategory).append("<option value='-1'>--Please Select--</option>");
                    jQuery.each(data.DropDown, function (index, item) {
                        $(_ddlReportCategory).append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });
                    $("#hdnmoduleName").val(data.Name)
                    $("#hdnReportServer").val(data.ReportServer)
                    $("#hdnReportDatabase").val(data.ReportDatabase)
                    $("#hdnReportDbUserId").val(data.ReportDbUserId)
                    $("#hdnmoduleDbPassword").val(data.ReportDbPassord)
                    $("#hdnReportPath").val(data.ReportPath)
                    $("#hdnConnectionType").val(data.ReportConnectionType)
                    $("#hdnConnectionName").val(data.ConnectionName)
                    $("#hdnConnectionUserId").val(data.ConnectionUserId)
                    $("#hdnConnectionPassword").val(data.ConnectionPassword)
                },
                error: OnErrorCall,
                complete: function () {
                    isValueExist(_ddlReportCategory, $(_ddlReportCategory).val())
                }
            });
        }
    });
}
// End Report Module

function FillReportsCategory() {
    $.ajax({
        type: "POST",
        url: ServiceUrl + '/FillReportsCategory',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_ddlReportCategory).empty();
            $(_ddlReportCategory).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_ddlReportCategory).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}

function OnChangeReportCategory() {
    $(_ddlReportCategory).on('change', function () {
        var id = $(_ddlReportCategory).val();

        if (id != "") {
            $.ajax({
                type: "POST",
                url: "" + ServiceUrl + "/FillReports",
                data: JSON.stringify({ ID: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var data = JSON.parse(response.d);
                    $(_ddlReports).empty();
                    $(_ddlReports).append("<option value='-1'>--Please Select--</option>");
                    jQuery.each(data, function (index, item) {
                        $(_ddlReports).append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });
                },
                error: OnErrorCall,
                complete: function () {
                    isValueExist(_ddlReports, $(_ddlReports).val())
                }
            });
        }
    });
}

function OnErrorCall() {
    alert("Something went wrong");
}


function NationalAssembly() {
    $.ajax({
        type: "POST",
        url: ServiceUrl + '/NAFill',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_ddlNA).empty();
            $(_ddlNA).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_ddlNA).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}


function FillsReports() {
    $.ajax({
        type: "POST",
        url: ServiceUrl + '/FillReports',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_ddlReports).empty();
            $(_ddlReports).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_ddlReports).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}


function OnChangeNA() {
    $(_ddlNA).on('change', function () {
        var id = $(_ddlNA).val();

        if (id != "") {
            $.ajax({
                type: "POST",
                url: "" + ServiceUrl + "/OnChangeNA",
                data: JSON.stringify({ ID: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var data = JSON.parse(response.d);
                    $(_ddlPA).empty();
                    $(_ddlPA).append("<option value='-1'>--Please Select--</option>");
                    jQuery.each(data, function (index, item) {
                        $(_ddlPA).append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });

                    $(_ddlUC).empty();
                    $(_ddlWard).empty();
                    $(_ddlBlock).empty();
                    $(_ddlArea).empty();
                    $(_ddlStreet).empty();
                },
                error: OnErrorCall
            });
        }
    });
}


function OnChangeUC() {
    $(_ddlUC).on('change', function () {
        var id = $(_ddlUC).val();

        if (id != "") {
            $.ajax({
                type: "POST",
                url: "" + ServiceUrl + "/OnChangeUC",
                data: JSON.stringify({ ID: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var data = JSON.parse(response.d);
                    $(_ddlWard).empty();
                    $(_ddlWard).append("<option value='-1'>--Please Select--</option>");
                    jQuery.each(data, function (index, item) {
                        $(_ddlWard).append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });
                    $(_ddlBlock).empty();
                    $(_ddlArea).empty();
                    $(_ddlStreet).empty();
                },
                error: OnErrorCall
            });
        }
    });
}


function OnChangeWard() {
    $(_ddlWard).on('change', function () {
        var id = $(_ddlWard).val();

        if (id != "") {
            $.ajax({
                type: "POST",
                url: "" + ServiceUrl + "/OnChangeWard",
                data: JSON.stringify({ ID: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var data = JSON.parse(response.d);
                    $(_ddlBlock).empty();
                    $(_ddlBlock).append("<option value='-1'>--Please Select--</option>");
                    jQuery.each(data, function (index, item) {
                        $(_ddlBlock).append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });
                    $(_ddlArea).empty();
                    $(_ddlStreet).empty();
                },
                error: OnErrorCall
            });
        }
    });
}


function OnChangeBlock() {
    $(_ddlBlock).on('change', function () {
        var id = $(_ddlBlock).val();

        if (id != "") {
            $.ajax({
                type: "POST",
                url: "" + ServiceUrl + "/OnChangeBlock",
                data: JSON.stringify({ ID: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var data = JSON.parse(response.d);
                    $(_ddlArea).empty();
                    $(_ddlArea).append("<option value='-1'>--Please Select--</option>");
                    jQuery.each(data, function (index, item) {
                        $(_ddlArea).append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });
                    $(_ddlStreet).empty();
                },
                error: OnErrorCall
            });
        }
    });
}

function OnChangeArea() {
    $(_ddlArea).on('change', function () {
        var id = $(_ddlArea).val();

        if (id != "") {
            $.ajax({
                type: "POST",
                url: "" + ServiceUrl + "/OnChangeArea",
                data: JSON.stringify({ ID: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var data = JSON.parse(response.d);
                    $(_ddlStreet).empty();
                    $(_ddlStreet).append("<option value='-1'>--Please Select--</option>");
                    jQuery.each(data, function (index, item) {
                        $(_ddlStreet).append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });
                },
                error: OnErrorCall
            });
        }
    });
}


function OnChangePA() {
    $(_ddlPA).on('change', function () {
        var id = $(_ddlPA).val();

        if (id != "") {
            $.ajax({
                type: "POST",
                url: "" + ServiceUrl + "/OnChangePA",
                data: JSON.stringify({ ID: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var data = JSON.parse(response.d);
                    $(_ddlUC).empty();
                    $(_ddlUC).append("<option value='-1'>--Please Select--</option>");
                    jQuery.each(data, function (index, item) {
                        $(_ddlUC).append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });
                    $(_ddlWard).empty();
                    $(_ddlBlock).empty();
                    $(_ddlArea).empty();
                    $(_ddlStreet).empty();
                },
                error: OnErrorCall
            });
        }
    });
}


function OnChangeBlock() {
    $(_ddlBlock).on('change', function () {
        var id = $(_ddlBlock).val();

        if (id != "") {
            $.ajax({
                type: "POST",
                url: "" + ServiceUrl + "/OnChangeBlock",
                data: JSON.stringify({ ID: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var data = JSON.parse(response.d);
                    $(_ddlArea).empty();
                    $(_ddlArea).append("<option value='-1'>--Please Select--</option>");
                    jQuery.each(data, function (index, item) {
                        $(_ddlArea).append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });

                },
                error: OnErrorCall
            });
        }
    });
}




