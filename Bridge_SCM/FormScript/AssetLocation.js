﻿const serviceUrl = "../../appServices/AssetLocationService.asmx";
const commonService = "../../appServices/CommonService.asmx";

$(function () {

    $("body #assetLocationForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #assetLocationForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #assetLocationForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })

    bindGetAssetLocation();

    bindAddEditAssetLocation();
    bindSaveAssetLocation();
    bindDeleteAssetLocation();
})


function bindGetAssetLocation() {
    $.ajax({
        url: serviceUrl + "/getAllAssetLocation",
        type: "Post",
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            let data = JSON.parse(response.d)
            MakeDataGridForAssetBook(data)
        },
        error: function (response) {

        }
    })
}

function bindAddEditAssetLocation() {
    $("body").on("click", "#btnAdd , .btnedit", function () {
        let id = $(this).attr("_recordid")

        clearAllMessages("assetLocationForm")

        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("Asset Location", "assetLocationForm")
        }
        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("Asset Location", "assetLocationForm", service_DeveloperMode)

        //}

        $.ajax({
            type: "POST",
            url: serviceUrl + "/AddEditAssetLocation",
            data: JSON.stringify({ id: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        if (parseInt(id) > 0) {
                            $("#formModalLabel").text("Edit");
                        } else {
                            $("#formModalLabel").text("Add");
                        }
                        $("#txtID").val(data.GenericList.Id);
                        $("#assetlocation").val(data.GenericList.AssetLocation);
                        $("#description").val(data.GenericList.Description);
                        $("#location").val(data.GenericList.Location);
                        $("#department").val(data.GenericList.Department);
                        
                        $("#assetlocationmodal").modal();
                    }
                }
            },
            error: function (response) {

            },
            complete: function () {
                isValueExist("#assetlocation", $("#assetlocation").val())
                isValueExist("#description", $("#description").val())
                isValueExist("#location", $("#location").val())
                isValueExist("#department", $("#department").val())
            }
        });
    })
}

function bindSaveAssetLocation() {
    $("body").on("click", "#btnsave", function () {

        var isValid = true

        $("body #assetLocationForm input").each(function (index, row) {
            if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
                if ($(row).val() == "") {
                    $(row).parent().find("span").eq(2).removeClass("hide")
                    isValid = false
                } else {
                    $(row).parent().find("span").eq(2).addClass("hide")
                }
            }
        })

        $("body #assetLocationForm select").each(function (index, row) {
            if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
                if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                    $(row).parent().find("span").eq(2).removeClass("hide")
                    isValid = false
                } else {
                    $(row).parent().find("span").eq(2).addClass("hide")
                }
            }
        })

        if (isValid) {
            var formData = {
                Id: parseInt($("#txtID").val()),
                AssetLocation: $("#assetlocation").val(),
                Description: $("#description").val(),
                Location: $("#location").val(),
                Department: $("#department").val(),
            }

            $.ajax({
                type: "POST",
                url: serviceUrl + "/saveAssetLocation",
                data: JSON.stringify({ assetLocationVm: formData }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d == "sessionNotFound") {
                        alert("sessionNotFound");
                        window.location.href = loginUrl;
                    } else {
                        var data = JSON.parse(response.d);

                        if (data.Status) {
                            $("#Validation").fadeIn(1000);
                            $("#Validation").text(data.Message);
                            $("#Validation").fadeOut(5000);
                            MakeDataGridForAssetLocation(data.GenericList);
                            $("#assetlocationmodal").modal('hide');
                        } else if (data.Message == "exist") {
                            $("#Validation").fadeIn(1000);
                            $("#Validation").text(exist);
                            $("#Validation").fadeOut(5000);
                            $("#assetlocationmodal").modal('hide');
                        } else if (data.Message == "update") {
                            $("#Validation").fadeIn(1000);
                            $("#Validation").text(update);
                            $("#Validation").fadeOut(5000);
                            MakeDataGridForAssetLocation(data.GenericList);
                            $("#assetlocationmodal").modal('hide');
                        }
                    }
                },
                error: function (response) {
                }
            });
        }
    })
}

var currentElement;
function bindDeleteAssetLocation() {
    $("body").on("click", ".btndelete", function () {
        currentElement = $(this)

        $.ajax({
            type: "POST",
            url: "" + commonService + "/isAuthenticatedForDelete",
            data: JSON.stringify({ form: "Asset Location" }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        $("#assetbookdeletemodal").modal();
                    }
                }
            },
            error: function (response) {

            }
        });
    })
}

function deleteAssetBook() {
    $.ajax({
        type: "POST",
        url: serviceUrl + "/deleteAssetLocation",
        data: JSON.stringify({ id: currentElement.attr("_recordId"), title: currentElement.attr("_title") }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            if (response.d == "sessionNotFound") {
                alert("sessionNotFound");
                window.location.href = loginUrl;
            } else {
                var data = JSON.parse(response.d);
                if (data.Status) {
                    $("#assetbookdeletemodal").modal('hide');
                    currentElement.closest('tr').remove()
                    $("#Validation").fadeIn(1000);
                    $("#Validation").text(data.Message);
                    $("#Validation").fadeOut(5000);
                } else if (data.Message == "failed") {
                    $("#Validation").fadeIn(1000);
                    $("#Validation").text(failedmsg);
                    $("#Validation").fadeOut(5000);
                } else if (data.Message == "use") {
                    $("#Validation").fadeIn(1000);
                    $("#Validation").text("It is used by another process, Please try again!");
                    $("#Validation").fadeOut(5000);
                }
            }
        },
        error: function (response) {

        }
    });
}

function MakeDataGridForAssetLocation(datasource) {
    $("#assetlocationcontainer").DataTable().destroy();

    $("#assetlocationcontainer").DataTable({
        "searching": true,
        "bLengthChange": true,
        "scrollY": 400,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        dom: 'Bfrtip',
        buttons: [
             {
                 extend: 'print',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Asset Location',
                 text: '<i class="fa fa-lg fa-print"></i>  Print'

             },
             {
                 extend: 'excelHtml5',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Asset Location',
                 text: '<i class="fa fa-lg fa-file-excel"></i>  Excel',
             },
            {
                extend: 'pdf',
                footer: true,
                exportOptions: {
                    columns: [0, 1, 3]
                },
                title: 'Asset Location',
                text: '<i class="fa fa-lg fa-file-pdf"></i>  PDF'
            },
        ],
        "aaData": datasource,
        "aoColumns": [

                        { data: "AssetLocation", width: "50%" },
                        { data: "Description", width: "50%" },
                        { data: "Location", width: "20%" },
                        { data: "Department", width: "20%" },
                         {
                             data: null,
                             width: "5%",
                             sDefaultContent: "",
                             "orderable": false,
                             render: function (data, type, row) {
                                 if (type === 'display') {
                                     var del = ' <a href="#" _recordId="' + data.Id + '" _title="' + data.AssetLocation + '" class="btndelete" title="Delete Asset Location"><i class="fas fa-trash"></i></a>';
                                     var edit = ' <a href="#" _recordId="' + data.Id + '" class="btnedit" title="Edit Asset Location"><i class="fas fa-pencil-alt"></i></a>';
                                     var html = edit + " | " + del;
                                     return html;
                                 }
                                 return data;
                             }
                         },
        ]
    });
}