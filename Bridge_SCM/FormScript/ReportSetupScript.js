﻿var serviceurl = "../../appServices/ReportSetupService.asmx";
var tblname = "#tableCity";
var tblDetail = "#tableDetail";
var tblDetail2 = "#tblDetail";
var tblDetail4 = "#tableselectionformula";
var table, table2, table3, table4;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _ID = "#txtID";
var _Name = "#txtName";
var _ReportModule = "#ddlReportModule";
var _ReportCategory = "#ddlReportCategory";
var _ReportPath = "#hdnreportpath";
var _ConnectionType = "#hndConnectionType";
var _ReportID = "#txtReportID";
var _chkActive = "#chkActive";
var _chkAutoCalling = "#chkAutoCalling";
var _chkSubReport = "#chkSubReport";
var _chkSelectionFormula = "#chkSelectionFormula";
var _DataType = "#txtDataType";
var _ParameterName = "#txtParameterName";
var _ObjectName = "#ddlObjectName";
var _Msg = "#txtMsg";
var _Mode = "#txtMode";
var _MasterID = "#txtMasterID";
var _DetailID = "#txtDetailID";
var _Master = "#txtMaster";
var _ObjectName2 = "#txtObjectName2";
var _ObjectType = "#txtObjectType";
var _Query = "#txtQuery";
var _Label = "#txtLabel";
var _Report = "#txtReport";
var loginUrl = "/Default.aspx";


$(document).ready(function () {
    $("#imgfile").show()
    OnParameter();
    ReportModule();
    //ReportCategory();
    OnLoadGrid();
    OnDelete();
    OnEdit();
    OnDeleteDetail();
    OnDetail();
    OnDeleteDetail2();
    bindAddSelectionFormula();
    bindSaveSelectionFormula();
    bindAddStoreProcedure();
    bindSaveStoreProcedure();
    OnDeleteStoreProcedure();
    //OnLoadGridForSF();
    bindOnChangeEvent();
    OnChangeReportModule();

    $("body").on("change", "#ddlParameterName", function () {
        $("#txtParameterName").val($(this).find("option:selected").val())
    })

    $("body").on("change", "#ddlObjectType, #ddlObjectName", function () {
        var currentEle = $(this).attr('_type');
        if (currentEle == "objectType") {
            $("#ddlObjectType").val($(this).find("option:selected").val())
        } else {
            $("#ddlObjectName").val($(this).find("option:selected").val())
        }
    })

    $(".chkRportConType").on("change", function () {
        var type = $(this).attr("_type")
        $("#hndConnectionType").val(type)
    })
    //OnEditDetail();

    $(".multi-select").select2({
        multiple: true,
        placeholder: "-- Select Variable --"
    });
});


function ReportModule() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + '/FillReportModule',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_ReportModule).empty();
            $(_ReportModule).append("<option value='-1'>--Please Select--</option>");
            $.each(data, function (index, item) {
                $(_ReportModule).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
            $(_ReportModule).val("-1");
        }
    });
}

function OnChangeReportModule() {
    $(_ReportModule).on('change', function () {
        var id = $(_ReportModule).val();
        //alert(id)
        if (id != "") {
            $.ajax({
                type: "POST",
                url: "" + serviceurl + "/FillReportCategories",
                data: JSON.stringify({ ID: id }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var data = JSON.parse(response.d);
                    $(_ReportCategory).empty();
                    $(_ReportCategory).append("<option value='-1'>--Please Select--</option>");
                    jQuery.each(data.DropDown, function (index, item) {
                        $(_ReportCategory).append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });
                    $(_ReportCategory).val("-1");
                    $(_ReportPath).val(data.ReportPath)
                },
                error: OnErrorCall
            });
        }
    });
}

//function ReportCategory() {
//    $.ajax({
//        type: "POST",
//        url: "" + serviceurl + '/ReportCategoryFill',
//        contentType: "application/json",
//        dataType: "json",
//        success: function (response) {
//            var data = JSON.parse(response.d);
//            $(_ReportCategory).empty();
//            $(_ReportCategory).append("<option value='-1'>--Please Select--</option>");
//            jQuery.each(data, function (index, item) {
//                $(_ReportCategory).append("<option value=" + item.Value + "> " + item.Text + " </option>");
//            });
//            $(_ReportCategory).val("-1");
//        }
//    });
//}
//OnSave setup
//function OnSave() {
//    var spFound;
//    $.getScript('../../FormScript/ValidationScript.js', function () {
//        spFound = valid();

//        FormValidaton();
//        var isActive, autoCalling, isSubReport, isSelectionFormula;
//        if ($(_chkActive).is(':checked')) {
//            isActive = 1;
//        }
//        else {
//            isActive = 0
//        }

//        if ($(_chkAutoCalling).is(':checked')) {
//            autoCalling = 1;
//        }
//        else {
//            autoCalling = 0
//        }

//        if ($(_chkSubReport).is(':checked')) {
//            isSubReport = 1;
//        }
//        else {
//            isSubReport = 0
//        }

//        if ($(_chkSelectionFormula).is(':checked')) {
//            isSelectionFormula = 1;
//        }
//        else {
//            isSelectionFormula = 0
//        }

//        var dataSend = {
//            ID: $(_ID).val(),
//            ReportName: $(_Name).val(),
//            IsActive: isActive,
//            ReportID: $(_ReportID).val(),
//            AutoCalling: autoCalling,
//            IsSubReport: isSubReport,
//            IsSelectionFormula: isSelectionFormula,
//            FKReportCategory: $(_ReportCategory).val(),
//            FKReportModule: $(_ReportModule).val(),
//            ReportConnectionType: $(_ConnectionType).val(),
//            ReportPath: $(_ReportPath).val()
//        }

//        if (msg == "") {
//            if (spFound == false) {
//                $.ajax({
//                    type: "POST",
//                    url: "" + serviceurl + "/OnSave",
//                    data: JSON.stringify({ model: dataSend }),
//                    contentType: "application/json; charset=utf-8",
//                    dataType: "json",
//                    success: function (response) {
//                        if (response.d == "sessionNotFound") {
//                            alert("sessionNotFound");
//                            window.location.href = loginUrl;
//                        } else {
//                            var data = JSON.parse(response.d);
//                            console.log(data);
//                            if (data.Message == "success") {
//                                var data = new FormData()
//                                var files = $("#imgfile").get(0).files;
//                                if (files.length > 0) {
//                                    data.append("UploadedImage", files[0])
//                                }

//                                sendFile(data);

//                                ClearFields();
//                                $("#Validation").fadeIn(1000);
//                                $("#Validation").text(success);
//                                $("#Validation").fadeOut(5000);
//                                OnLoadGrid();
//                                $("#modalAdd").modal('hide');
//                            } else if (data.Message == "exist") {
//                                $("#Validation").fadeIn(1000);
//                                $("#Validation").text(exist);
//                                $("#Validation").fadeOut(5000);
//                                OnLoadGrid();
//                                $("#modalAdd").modal('hide');
//                            } else if (data.Message == "update") {
//                                $("#Validation").fadeIn(1000);
//                                $("#Validation").text(update);
//                                $("#Validation").fadeOut(5000);
//                                OnLoadGrid();
//                                $("#modalAdd").modal('hide');
//                            }
//                            else if (data.Message == "failed") {
//                                $("#Validation").fadeIn(1000);
//                                $("#Validation").text(failedmsg);
//                                $("#Validation").fadeOut(5000);
//                                OnLoadGrid();
//                                $("#modalAdd").modal('hide');
//                            }
//                        }
//                    },
//                    error: OnErrorCall
//                });
//            }
//            else {
//                alert("Illegal Characters Detected!")
//            }
//        } else {

//        }
//    });
//}
function showname() {
    var name = document.getElementById('imgfile');
    $(_ReportID).val(name.files.item(0).name);

};
//function sendFile(data) {
//    $.ajax({
//        type: 'post',
//        url: "" + serviceurl + "/uploadImage",
//        data: data,
//        success: function (response) {
//            console.log(response)
//            //ClearFields();
//            $("#imgfile").val("")
//            $("#Validation").fadeIn(1000);
//            $("#Validation").text(success);
//            $("#Validation").fadeOut(5000);
//        },
//        processData: false,
//        contentType: false,
//        error: OnErrorCall
//    });
//}

function FormValidaton() {
    msg = "";

    $("#rName").addClass("hide");


    if ($(_Name).val() == "") {
        msg += "Type is required";
        $("#rName").removeClass("hide");
    } else {
        $("#rName").addClass("hide");
    }

    $("#rReportCategory").addClass("hide");


    if ($(_ReportCategory).val() == "-1") {
        msg += "Report Category is required";
        $("#rReportCategory").removeClass("hide");
    } else {
        $("#rReportCategory").addClass("hide");
    }

    $("#rReportID").addClass("hide");


    if ($(_ReportID).val() == "") {
        msg += "Report ID is required";
        $("#rReportID").removeClass("hide");
    } else {
        $("#rReportID").addClass("hide");
    }

    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}

function ClearFields() {
    $(_Name).val("");
    $(_ID).val("");
    $(_ReportCategory).val("-1");
    $(_ReportID).val("");
    $("input[id=chkActive]").prop("checked", false);
    $("input[id=chkAutoCalling]").prop("checked", true);
    $("input[id=chkSubReport]").prop("checked", false);
    $("input[id=chkSelectionFormula]").prop("checked", false);

    $("#ValidationSummary").text("");
    $("#rName").addClass("hide");
    $("#rReportCategory").addClass("hide");
    $("#rReportID").addClass("hide");
}

function OnLoadGrid() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblname).DataTable().destroy();
            LoadDataTable(response.d);
        },
        error: OnErrorCall
    });


}






function OnEdit() {
    $(tblname).on('click', '.edit', function () {
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData);

        $(_ID).val(1);
        $(_Name).val(rowData.ReportName);
        $(_ReportModule).val(rowData.FKReportModule);
        $(_ReportCategory).val(rowData.FKReportCategory);
        $(_ReportID).val(rowData.ReportID);

        if (rowData.IsActive == 1 || rowData.IsActive == "1") {
            $('input[id=chkActive]').attr('checked', true);
            $('input[id=chkActive]').prop('checked', true);
        }
        else if (rowData.IsActive == 0 || rowData.IsActive == "0") {
            $('input[id=chkActive]').attr('checked', false);
            $('input[id=chkActive]').prop('checked', false);
        }

        if (rowData.AutoCalling == 1 || rowData.AutoCalling == "1") {
            $('input[id=chkAutoCalling]').attr('checked', true);
            $('input[id=chkAutoCalling]').prop('checked', true);
        }
        else if (rowData.AutoCalling == 0 || rowData.AutoCalling == "0") {
            $('input[id=chkAutoCalling]').attr('checked', false);
            $('input[id=chkAutoCalling]').prop('checked', false);
        }

        if (rowData.IsSubReport == 1 || rowData.IsSubReport == "1") {
            $('input[id=chkSubReport]').attr('checked', true);
            $('input[id=chkSubReport]').prop('checked', true);
        }
        else if (rowData.IsSubReport == 0 || rowData.IsSubReport == "0") {
            $('input[id=chkSubReport]').attr('checked', false);
            $('input[id=chkSubReport]').prop('checked', false);
        }

        if (rowData.IsSelectionFormula == 1 || rowData.IsSelectionFormula == "1") {
            $('input[id=chkSelectionFormula]').attr('checked', true);
            $('input[id=chkSelectionFormula]').prop('checked', true);
        }
        else if (rowData.IsSelectionFormula == 0 || rowData.IsSelectionFormula == "0") {
            $('input[id=chkSelectionFormula]').attr('checked', false);
            $('input[id=chkSelectionFormula]').prop('checked', false);
        }
        // set report connnection type
        if (rowData.ReportConnectionType == "odbc") {
            $('input[_type=odbc]').attr('checked', true);
            $('input[id=hndConnectionType]').val("odbc");
        }
        else if (rowData.ReportConnectionType == "oledb") {
            $('input[_type=oledb]').attr('checked', true);
            $('input[id=hndConnectionType]').val("oledb");
        }
        $("#imgfile").hide()

        $("#formModalLabel").text("Edit");
        $("#modalAdd").modal();

        $("#ValidationSummary").text("");
        $("#rName").addClass("hide");
        $("#rReportCategory").addClass("hide");
        $("#rReportID").addClass("hide");
    });
}




function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        dom: 'Bfrtip',
        buttons: [
                {
                    extend: 'print',
                    footer: true,
                    exportOptions: {
                        columns: [0, 1, 3]
                    },
                    title: 'Report Setup',
                    text: '<i class="fa fa-lg fa-file-text-o"></i>  Print'

                },
                {
                    extend: 'excelHtml5',
                    footer: true,
                    exportOptions: {
                        columns: [0, 1, 3]
                    },
                    title: 'Report Setup',
                    text: '<i class="fa fa-lg fa-file-excel-o"></i>  Excel',
                },
               {
                   extend: 'pdf',
                   footer: true,
                   exportOptions: {
                       columns: [0, 1, 3]
                   },
                   title: 'Report Setup',
                   text: '<i class="fa fa-lg fa-file-pdf-o"></i>  PDF'
               },
        ],
        "aaData": JSON.parse(datasource),
        "aoColumns": [
                        { data: "ReportModule", sDefaultContent: "" },
                        { data: "ReportCategory", sDefaultContent: "" },
                        { data: "ReportName", sDefaultContent: "" },
                        { data: "IsActive", sDefaultContent: "" },
                        { data: "ReportID", sDefaultContent: "" },
                        { data: "AutoCalling", sDefaultContent: "" },
                        { data: "IsSubReport", sDefaultContent: "" },
                        { data: "FKReportCategory", sDefaultContent: "" },
                        { data: "IsSelectionFormula", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            width: "10%",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" class="del" title="del"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#" class="edit" title="Edit"><i class="fas fa-pencil-alt" ></i></a>';
                                    var view = ' <a href="#" class="view" title="Parameters"><i class="fa fa-eye" ></i></a>';
                                    var view2 = ' <a href="#" class="view2" title="Detail"><i class="fa fa-eye" ></i></a>';
                                    var formula = ' <a href="javascript:;" class="selectionformula" title="Selection Formula"><i class="fa fa-eye" ></i></a>';
                                    var storedProcedure = ' <a href="javascript:;" class="storeProcedure" title="Store Procedure"><i class="fa fa-eye" ></i></a>';
                                    var html = del + edit + view2 + view + formula + storedProcedure;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [
            { "targets": [3], "visible": false },
            { "targets": [5], "visible": false },
            { "targets": [6], "visible": false },
            { "targets": [7], "visible": false },
            { "targets": [8], "visible": false },

        ]
    });
}

function OnAdd() {
    $("#imgfile").show()
    ClearFields();
    $("#formModalLabel").text("Add");
    $("#modalAdd").modal(); 
    $("#ddlReportModule option:selected").text("-- Please Select --").trigger("")
    $("#ddlReportCategory option:selected").text("-- Please Select --").trigger("change")
}


function OnErrorCall() {
    alert("Something went wrong");
}



function ClearDetailFields() {
    $(_Mode).val("Add");
    $(_DataType).val("");
    $(_ParameterName).val("");
    $(_ObjectName).val("");
    $(_Msg).val("");

    $("#ValidationSummary2").text("");
    $("#rDataType").addClass("hide");
    $("#rDataType").addClass("hide");
    $("#rObjectName").addClass("hide");
    $("#rMsg").addClass("hide");
}






function OnLoadGrid2(ID) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGridDetail",
        data: JSON.stringify({ id: ID }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblDetail).DataTable().destroy();
            LoadDataTableDetail(response.d);
        },
        error: OnErrorCall
    });
}

function GetParameterByReport(ReportID) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/getReportParameterByReportName",
        data: JSON.stringify({ id: ReportID }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d)
            $("#ddlParameterName").empty();
            $("#ddlParameterName").append("<option value=''>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $("#ddlParameterName").append("<option value=" + item + "> " + item + " </option>");
            });

        },
        error: OnErrorCall
    });
}

function LoadDataTableDetail(datasource) {
    table2 = $(tblDetail).DataTable({
        "searching": false,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [

                        { data: "ReportID", sDefaultContent: "" },
                        { data: "DataType", sDefaultContent: "" },
                        { data: "ParameterName", sDefaultContent: "" },
                        { data: "ObjectName", sDefaultContent: "" },
                        { data: "Msg", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" class="del" title="Delete"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#" class="edit" title="Edit"><i class="fa fa-pencil" ></i></a>';
                                    var html = del;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [
            { "targets": [0], "visible": false },

        ]
    });
}

function OnEditDetail() {
    $(tblDetail).on('click', '.edit', function () {
        debugger
        var row = $(this).closest('tr');
        var rowIndex = table2.row(row).index();
        var rowData = table2.row(rowIndex).data();
        console.log(rowData);

        $(_ParameterName).val(rowData.ParameterName)
        $(_ObjectName).val(rowData.ObjectName);
        $(_DataType).val(rowData.DataType);
        $(_Msg).val(rowData.Msg);
        $(_Mode).val("Edit")
    });
}

var Del = ""
var ParameterNamee = "";
function OnDeleteDetail() {
    $(tblDetail).on('click', '.del', function () {
        debugger
        var row = $(this).closest('tr');
        var rowIndex = table2.row(row).index();
        var rowData = table2.row(rowIndex).data();
        Del = rowData.ReportID;
        ParameterNamee = rowData.ParameterName;

        if (Del != "" && ParameterNamee != "") {
            $("#modalDelete2").modal();
        }

    });
}

function OnConfirmDeleteDetail() {
    DeleteDetail(Del, ParameterNamee);
}

function DeleteDetail(id, ParameterName) {
    if (id != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDeleteDetail",
            data: JSON.stringify({ dataID: id, ParameterName: ParameterName }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        $("#Validation2").fadeIn(1000);
                        $("#Validation2").text(delt);
                        $("#Validation2").fadeOut(5000);
                        OnLoadGrid2(MasterID);
                        $("#modalDelete2").modal('hide');
                    } else if (data.Message == "failed") {
                        $("#Validation2").fadeIn(1000);
                        $("#Validation2").text(failedmsg);
                        $("#Validation2").fadeOut(5000);
                    } else if (data.Message == "use") {
                        $("#Validation2").fadeIn(1000);
                        $("#Validation2").text("It is used by another process, Please try again!");
                        $("#Validation2").fadeOut(5000);
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}




function ClearDetailFields2() {
    $(_ObjectName2).val("");
    $(_ObjectType).val("");
    $(_Query).val("");
    $(_Label).val("");

    $("#Span1").text("");
    $("#rObjectName2").addClass("hide");
    $("#rObjectType").addClass("hide");
    $("#rQuery").addClass("hide");
    $("#rLabel").addClass("hide");
}


function DetailFormValidaton2() {
    msg = "";

    $("#rObjectName2").addClass("hide");

    if ($("#detailddlObjectName").val() == "" || $("#detailddlObjectName").val() == 0) {
        msg += "Object Name is required";
        $("#rObjectName2").removeClass("hide");
    } else {
        $("#rObjectName2").addClass("hide");
    }

    $("#rObjectType").addClass("hide");

    if ($("#detailddlObjectType").val() == "" || $("#detailddlObjectType").val() == 0) {
        msg += "Object Type is required";
        $("#rObjectType").removeClass("hide");
    } else {
        $("#rObjectType").addClass("hide");
    }

    $("#rQuery").addClass("hide");

    if ($("#detailddlObjectType").val() == "Dropdown" && $(_Query).val() == "") {
        msg += "Query is required";
        $("#rQuery").removeClass("hide");
    } else {
        $("#rQuery").addClass("hide");
    }

    $("#rMsg").addClass("hide");

    if ($(_Label).val() == "") {
        msg += "Label is required";
        $("#rLabel").removeClass("hide");
    } else {
        $("#rLabel").addClass("hide");
    }

    if (msg == "") {
        $("#Span1").text("");
    } else {
        $("#Span1").text("Please fill required values.");
        alert("Please fill required values.");
    }
}


function OnLoadGrid3(ID) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGridDetail2",
        data: JSON.stringify({ id: ID }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblDetail2).DataTable().destroy();
            LoadDataTableDetail2(response.d);
        },
        error: OnErrorCall
    });
}


function LoadDataTableDetail2(datasource) {
    table3 = $(tblDetail2).DataTable({
        "searching": false,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [

                        { data: "ReportID", sDefaultContent: "" },
                        { data: "ObjectName", sDefaultContent: "" },
                        { data: "ObjectType", sDefaultContent: "" },
                        { data: "Query", sDefaultContent: "" },
                        { data: "Label", sDefaultContent: "" },
                        { data: "DefaultValue", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" _recordId=' + data.ReportID + ' class="del" title="Delete"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#" class="edit" title="Edit"><i class="fa fa-pencil" ></i></a>';
                                    var html = del;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [
            { "targets": [0], "visible": false },

        ]
    });
}


// pop up add selection formula modal
function bindAddSelectionFormula() {
    $("body").on('click', '.selectionformula', function () {
        debugger
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        //console.log(rowData);
        $(_MasterID).val(rowData.ReportID);
        $(_Report).val(rowData.ReportID);
        MasterID = rowData.ReportID;
        //alert(MasterID);
        //alert(serviceurl);
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/loadGridForSF",
            data: JSON.stringify({ id: MasterID }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                debugger
                var result = JSON.parse(response.d)
                $(tblDetail4).DataTable().destroy();
                LoadSFDataTable(result);
                $("#title").text("Selection Formula");
                $("#txtSelectionFormula").val("")
                $("#modal-body-SF #ddlObjectType").find('option').remove().end().append("<option value ='0'>-- Select --</option>")
                .append("<option value='Dropdown'>ComboBox Control</option>")
                $("#modal-body-SF #ddlObjectName").find('option').remove().end().append("<option value ='0'>-- Select --</option>")
                $("#modalSelectionFormula").modal();
            },
            error: OnErrorCall
        });

    });
}


// save selection formula
function bindSaveSelectionFormula() {
    $("body").on("click", "#saveSelectionFormula", function () {
        var spFound;
        var formdata = $("#frmselectionformula").serialize();
        //  $.getScript('../../FormScript/ValidationScript.js', function () {
        //spFound = valid(); // Commented due to illegal characters validation in selection formula  >> ValidationScript.js 

        SelectionFormulaFormValidaton();

        var dataSend = {
            ReportID: $("#txtReport").val(),
            ObjectName: $("#ddlObjectName").find("option:selected").text(),
            SelectionFormula: $("#txtSelectionFormula").val(),

        }

        if (msg == "") {
            if (spFound == false) {
                $.ajax({
                    type: "POST",
                    url: "" + serviceurl + "/SaveSelectionFormula",
                    data: JSON.stringify({ model: dataSend }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d == "sessionNotFound") {
                            alert("sessionNotFound");
                            window.location.href = loginUrl;
                        } else {
                            var data = JSON.parse(response.d);
                            console.log(data);
                            if (data.Message == "success") {

                                ClearFields();
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text(success);
                                $("#Validation").fadeOut(5000);
                                //OnLoadGrid();
                                $("#modalSelectionFormula").modal('hide');
                            } else if (data.Message == "exist") {
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text(exist);
                                $("#Validation").fadeOut(5000);
                                // OnLoadGrid();
                                $("#modalAdd").modal('hide');
                            } else if (data.Message == "update") {
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text(update);
                                $("#Validation").fadeOut(5000);
                                // OnLoadGrid();
                                $("#modalSelectionFormula").modal('hide');
                            }
                            else if (data.Message == "failed") {
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text(failedmsg);
                                $("#Validation").fadeOut(5000);
                                // OnLoadGrid();
                                $("#modalSelectionFormula").modal('hide');
                            }
                        }
                    },
                    error: OnErrorCall
                });
            }
            else {
                alert("Illegal Characters Detected!")
            }
        } else {

        }
        //});
    })
}

// validate selection formula Form
function SelectionFormulaFormValidaton() {
    msg = "";

    $("#rReport").addClass("hide");


    if ($("#txtReport").val() == "") {
        msg += "Report is required";
        $("#rReport").removeClass("hide");
    } else {
        $("#rReport").addClass("hide");
    }

    $("#rObjectName").addClass("hide");


    if ($("#txtObjectName").val() == "-1") {
        msg += "Object name is required";
        $("#rObjectName").removeClass("hide");
    } else {
        $("#rObjectName").addClass("hide");
    }

    $("#rSelectionFormula").addClass("hide");


    if ($("#txtSelectionFormula").val() == "") {
        msg += "Selection formula is required";
        $("#rSelectionFormula").removeClass("hide");
    } else {
        $("#rSelectionFormula").addClass("hide");
    }

    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}

// load selection formula Grid
//function OnLoadGridForSF(ID) {
//    $.ajax({
//        type: "GET",
//        url: "" + serviceurl + "/loadGridForSF",
//        data: JSON.stringify({ id: ID }),
//        async:false,
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (response) {
//            $(tblDetail).DataTable().destroy();
//            LoadSFDataTable(response.d);
//        },
//        error: OnErrorCall
//    });
//}

function LoadSFDataTable(datasource) {
    debugger

    table4 = $(tblDetail4).DataTable({
        "searching": false,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": datasource,
        "aoColumns": [
                        { data: "ID", sDefaultContent: "" },
                        { data: "ReportID", sDefaultContent: "" },
                        { data: "ObjectName", sDefaultContent: "" },
                        { data: "SelectionFormula", sDefaultContent: "" },

                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" class="delSF" title="Delete"><i class="fa fa-trash" ></i></a>';
                                    //var edit = ' <a href="#" class="edit" title="Edit"><i class="fa fa-pencil" ></i></a>';
                                    var html = del;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [
           //{ "targets": [0], "visible": false },

        ]
    });
}
// Delete Pop Up Modal For Selection Formula
var Del2 = ""
var ObjectNamee = "";
//function OnDeleteDetail2() {
//    $(tblDetail2).on('click', '.delSF', function () {
//        var row = $(this).closest('tr');
//        var rowIndex = table3.row(row).index();
//        var rowData = table3.row(rowIndex).data();
//        Del2 = rowData.ID;
//        ObjectNamee = rowData.ObjectName;

//        if (Del2 != "" && ObjectNamee != "") {
//            $("#modalDeleteSF").modal();
//        }

//    });
//}



function OnConfirmDeleteSF() {
    DeleteSelectionFormula(Del2);
}


// pop up add store procedure modal
function bindAddStoreProcedure() {
    $("body").on('click', '.storeProcedure', function () {
        debugger
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData.ReportID);
        $(_MasterID).val(rowData.ReportID);
        $("#txtReportsp").val(rowData.ReportID);
        MasterID = rowData.ReportID;
        //alert(MasterID);
        //alert(serviceurl);
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/loadGridForSP",
            data: JSON.stringify({ id: MasterID }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                debugger
                var result = JSON.parse(response.d)
                $("#tablestoreprocedure").DataTable().destroy();
                LoadSPDataTable(result);
                $("#titleSP").text("Store Procedure");
                $("#txtSP").val("")
               $("#modalStoreProcedure").modal();
            },
            error: OnErrorCall
        });

    });
}


function LoadSPDataTable(datasource) {
    debugger

    table4 = $("#tablestoreprocedure").DataTable({
        "searching": false,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": datasource,
        "aoColumns": [
                        { data: "ID", sDefaultContent: "" },
                        { data: "ReportID", sDefaultContent: "" },
                        { data: "StoreProcedure", sDefaultContent: "" },
                        { data: "ExecutionOrder", sDefaultContent: "" },

                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" _recordId="' + data.ID + '" class="delSP" title="Delete"><i class="fa fa-trash" ></i></a>';
                                    //var edit = ' <a href="#" class="edit" title="Edit"><i class="fa fa-pencil" ></i></a>';
                                    var html = del;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [
           { "targets": [0], "visible": false },

        ]
    });
}

// Report Detail section

