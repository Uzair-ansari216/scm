﻿var serviceurl = "../../appServices/FinancialYearService.asmx";
var commonService = "../../appServices/CommonService.asmx";
var tblname = "#tableData";
var tblDetail = "#tableDetail";
var table, table2;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _ID = "#txtID";

var _YearStart = "#txtyearstart";
var _YearEnd = "#txtyearend";

var _FromtDate = "#txtfromdate";
var _ToDate = "#txttodate";

//var _Year = $(_YearStart).val() + "-" + $(_YearEnd).val();

var loginUrl = "/Default.aspx";


$(document).ready(function () {

    $("body #financialYearForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #financialYearForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #financialYearForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })
    //$(_YearStartDate).datepicker({ autoclose: true });
    //$(_YearEndtDate).datepicker({ autoclose: true });

    //$(_FromtDate).datepicker({ autoclose: true });
    //$(_ToDate).datepicker({ autoclose: true });

    demo.initFormExtendedDatetimepickers();

    OnLoadGrid();
    bindAddEditFinancialYear();
    OnDelete();
    OnEdit();

});

function bindAddEditFinancialYear() {
    $("body").on("click", "#btnAdd , .edit", function () {

        let id = $(this).attr("_recordId")
        clearAllMessages("financialYearForm")
        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("Financial Year", "financialYearForm")
        }
        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("Financial Year", "financialYearForm", service_DeveloperMode)

        //}

        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/AddEditFinancialYear",
            data: JSON.stringify({ id: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        if (parseInt(id) > 0) {
                            $("#formModalLabel").text("Edit");
                            $(".label-floating").addClass("is-focused");

                            var str = data.GenericList.yearTitle;
                            var res = str.split("-");
                            $(_YearStart).val(res[0])
                            $(_YearEnd).val(res[1])

                            var javascriptDate2 = new Date(data.GenericList.dateTo);
                            javascriptDate2 = javascriptDate2.getMonth() + 1 + "/" + javascriptDate2.getDate() + "/" + javascriptDate2.getFullYear();
                            $(_ToDate).val(javascriptDate2);

                            var javascriptDate3 = new Date(data.GenericList.dateFrom);
                            javascriptDate3 = javascriptDate3.getMonth() + 1 + "/" + javascriptDate3.getDate() + "/" + javascriptDate3.getFullYear();
                            $(_FromtDate).val(javascriptDate3);
                        } else {
                            $("#formModalLabel").text("Add");
                            $(".label-floating").removeClass("is-focused");
                        }
                        $(_ID).val(data.GenericList.ID);
                        $("#modalAdd").modal();
                    }

                }
            },
            error: OnErrorCall,
            complete: function () {
                isValueExist(_YearStart, $(_YearStart).val())
                isValueExist(_YearEnd, $(_YearEnd).val())
                isValueExist(_FromtDate, $(_FromtDate).val())
                isValueExist(_ToDate, $(_ToDate).val())
            }
        });
    })
}

function OnSave() {

    var isValid = true

    $("body #financialYearForm input").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    $("body #financialYearForm select").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    if (isValid) {

        _Year = $(_YearStart).val() + "-" + $(_YearEnd).val()

        var dataSend = {
            ID: $(_ID).val(),
            yearTitle: _Year,
            dateFrom: $(_FromtDate).val(),
            dateTo: $(_ToDate).val()
        }

        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnSave",
            data: JSON.stringify({ model: dataSend }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        ClearFields();
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                        $("#modalAdd").modal('hide');
                    }
                    if (data.Message == "success") {
                        ClearFields();
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(success);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "exist") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(exist);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "update") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(update);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    }
                }
            },
            error: OnErrorCall
        });
    }

}

function FormValidaton() {
    msg = "";

    $("#rYearStartDate").addClass("hide");

    if ($(_YearStart).val() == "") {
        msg += "required";
        $("#rYearStartDate").removeClass("hide");
    } else {
        $("#rYearStartDate").addClass("hide");
    }


    $("#rYearEnd").addClass("hide");

    if ($(_YearEnd).val() == "") {
        msg += "required";
        $("#rYearEnd").removeClass("hide");
    } else {
        $("#rYearEnd").addClass("hide");
    }

    $("#rFromDate").addClass("hide");

    if ($(_FromtDate).val() == "") {
        msg += "required";
        $("#rFromDate").removeClass("hide");
    } else {
        $("#rFromDate").addClass("hide");
    }

    $("#rToDate").addClass("hide");

    if ($(_ToDate).val() == "") {
        msg += "required";
        $("#rToDate").removeClass("hide");
    } else {
        $("#rToDate").addClass("hide");
    }


    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}


function ClearFields() {

    $(_ID).val("");
    $(_FromtDate).val("");
    $(_ToDate).val("");
    $(_YearStart).val("");
    $(_YearEnd).val("");

    $("#ValidationSummary").text("");

    $("#rFromDate").addClass("hide");
    $("#rToDate").addClass("hide");
    $("#rYearStartDate").addClass("hide");
    $("#rYearEnd").addClass("hide");

}

function OnLoadGrid() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            let data = JSON.parse(response.d)
            if (!data.Status) {
                $("#Validation").fadeIn(1000);
                $("#Validation").text(data.Message);
                $("#Validation").fadeOut(5000);
            } else {
                $(tblname).DataTable().destroy();
                LoadDataTable(data.GenericList);
            }
        },
        error: OnErrorCall
    });


}

function OnConfirmDelete() {
    Delete(DeleteID, DeleteName);
}

var DeleteID = "";
var DeleteName = "";
function OnDelete() {
    $(tblname).on('click', '.del', function () {
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData);
        DeleteID = rowData.ID;
        DeleteName = rowData.yearTitle;

        $.ajax({
            type: "POST",
            url: "" + commonService + "/isAuthenticatedForDelete",
            data: JSON.stringify({ form: "Financial Year" }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        $("#modalDelete").modal();
                    }
                }
            },
            error: OnErrorCall
        });

    });
}

function Delete(id, Name) {
    if (id != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDelete",
            data: JSON.stringify({ dataID: id, dataName: Name }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                        $("#modalDelete").modal('hide');
                    }
                    if (data.Message == "success") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(delt);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalDelete").modal('hide');

                    } else if (data.Message == "failed") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(failedmsg);
                        $("#Validation").fadeOut(5000);
                    } else if (data.Message == "exist") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("It is used by another process, Please try again!");
                        $("#Validation").fadeOut(5000);
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}


function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        dom: 'Bfrtip',
        buttons: [
             {
                 extend: 'print',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Financial Year',
                 text: '<i class="fas fa-print"></i>  Print'

             },
             {
                 extend: 'excelHtml5',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Financial Year',
                 text: '<i class="fas fa-file-excel"></i>  Excel',
             },
            {
                extend: 'pdf',
                footer: true,
                exportOptions: {
                    columns: [0, 1, 3]
                },
                title: 'Financial Year',
                text: '<i class="fas fa-file-pdf"></i>  PDF'
            },
        ],
        "aaData": datasource,
        "aoColumns": [

                        { data: "ID", sDefaultContent: "" },
                        { data: "yearTitle", sDefaultContent: "" },
                        { data: "dateFrom", sDefaultContent: "" },
                        { data: "dateTo", sDefaultContent: "" },
                        { data: "style", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    var del = ' <a href="#" _recordId = ' + data.ID + ' class="del" title="Delete"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#" _recordId = ' + data.ID + ' class="edit" title="Edit"><i class="fas fa-pencil-alt" ></i></a>';
                                    var html = del + edit;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [

            {
                "targets": [2],
                "type": "date",
                "render": function (data) {
                    if (data !== null) {
                        var javascriptDate = new Date(data);
                        console.log(data);
                        javascriptDate = javascriptDate.getMonth() + 1 + "/" + javascriptDate.getDate() + "/" + javascriptDate.getFullYear();
                        return javascriptDate;
                    } else {
                        return "";
                    }
                }
            },
            {
                "targets": [3],
                "type": "date",
                "render": function (data) {
                    if (data !== null) {
                        var javascriptDate = new Date(data);
                        console.log(data);
                        javascriptDate = javascriptDate.getMonth() + 1 + "/" + javascriptDate.getDate() + "/" + javascriptDate.getFullYear();
                        return javascriptDate;
                    } else {
                        return "";
                    }
                }
            },
             { "targets": [1], "width": "20%" },
            { "targets": [0], "width": "4%" },
            { "targets": [4], "visible": false }
        ]
    });
}

function OnErrorCall() {
    alert("Something went wrong");
}