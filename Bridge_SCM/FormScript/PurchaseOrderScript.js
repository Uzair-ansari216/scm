﻿const serviceUrl = "../../appServices/PurchaseOrder.asmx"
var commonService = "../../appServices/CommonService.asmx";
var tableName = "#purchaseInvoicesTable"
let rowId = ""
let PONumber = ""
$(function () {

    $("body #PurchaseOrderForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #PurchaseOrderForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #PurchaseOrderForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })



    $("body #modal-product-info").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #modal-product-info").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #modal-product-info").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })

    bindGetAllPurchaseOrder();
    wizard();
    wizard2();
    bindNextPrevious();
    bindAddPurchaseOrder();
    bindEditPurchaseOrder()
    bindAddEditProductInfo();
    bindOnSearchProduct();
    bindOnProductSelection();
    bindShowPurchaseInvoices();
    bindOnChangeEvent();
    bindSavePurchaseOrder();
    bindCalculateAmount();
    bindAppendSelectedProductDetail();
    bindDeleteDetail();
    bindDeleteClick();
    bindUpdatePurchaseOrderStatus()
    var date = '<%=Session("WorkingDate") %>'
    $(".datePicker").datepicker({ autoclose: true });


})

function bindNextPrevious() {
    $("#btnNext").click(function (e) {
        e.preventDefault();
        $("#btnPrevious").css({ "display": "block" })
        $(".nav-pills li").each(function (index, row) {
            if ($(row).hasClass("active")) {
                if (index == 0) {
                    var isValid = formValidation();
                    if (isValid == "") {
                        $(row).removeClass('active')

                        var nextEle = $(row).next()
                        var id = $(nextEle).find('a').attr('href')
                        $('.nav-pills a[href="' + id + '"]').tab('show')
                        $(row).next().addClass("active")
                        $(".moving-tab").html($(nextEle).text().trim())
                        hideNext()
                        return false
                    }
                } else {
                    $(row).removeClass('active')

                    var nextEle = $(row).next()
                    var id = $(nextEle).find('a').attr('href')
                    $('.nav-pills a[href="' + id + '"]').tab('show')
                    $(row).next().addClass("active")
                    $(".moving-tab").html($(nextEle).text().trim())
                    hideNext()
                    return false
                }

            }
        })
    })
    $("#btnPrevious").click(function (e) {
        e.preventDefault();
        $(".nav-pills li").each(function (index, row) {
            if ($(row).hasClass("active")) {
                $(row).removeClass('active')

                var previousEle = $(row).prev()
                var id = $(previousEle).find('a').attr('href')
                $('.nav-pills a[href="' + id + '"]').tab('show')
                $(row).prev().addClass("active")
                $(".moving-tab").html($(previousEle).text().trim())
                hidePrevious()
                return false
            }
        })
    })
}
function hidePrevious() {
    $(".nav-pills li").each(function (index, row) {
        if ($(row).hasClass("active") && index == 0) {
            $("#btnPrevious").css({ "display": "none" })
        }
        if ($(row).hasClass("active") && index == 1) {
            $("#btnSave").css({ "display": "none" })
            $("#btnNext").css({ "display": "" })
        }
    })
}
function hideNext() {
    $(".nav-pills li").each(function (index, row) {
        if ($(row).hasClass("active") && index == 2) {
            $("#btnNext").css({ "display": "none" })
            $("#btnSave").css({ "display": "" })
        }
    })
}
function formValidation() {
    var msg = "";

    if ($(".cboCurrency").val() <= 0) {
        msg += "Please select currency"
        $("#rCboCurrency").text("Required")
    } if ($("#txtExchange").val() == "") {
        msg += "Please enter exchange rate"
        $("#rExchange").text("Required")
    }
    if ($(".cboFromBillTo").val() <= 0) {
        msg += "Please select From/Bill To, Company"
        $("#rFromBillTo").text("Required")
    } if ($(".cboBillTo").val() <= 0) {
        msg += "Please select To, Company"
        $("#rBillTo").text("Required")
    } if ($(".cboShipTo").val() <= 0) {
        msg += "Please select Ship To, Company"
        $("#rShipTo").text("Required")
    } if (true) {

    }
    return msg
}

function bindGetAllPurchaseOrder() {
    $.ajax({
        type: "POST",
        url: "" + serviceUrl + "/GetAllPurchaseOrder",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            MakeDataGridForPurchaseOrder(response.d)
        },
        error: OnErrorCall
    });
}

function wizard() {
    //Add blue animated border and remove with condition when focus and blur
    if ($('.fg-line')[0]) {
        $('body').on('focus', '.form-control', function () {
            $(this).closest('.fg-line').addClass('fg-toggled');
        })

        $('body').on('blur', '.form-control', function () {
            var p = $(this).closest('.form-group');
            var i = p.find('.form-control').val();

            if (p.hasClass('fg-float')) {
                if (i.length == 0) {
                    $(this).closest('.fg-line').removeClass('fg-toggled');
                }
            }
            else {
                $(this).closest('.fg-line').removeClass('fg-toggled');
            }
        });
    }

    //Add blue border for pre-valued fg-flot text feilds
    if ($('.fg-float')[0]) {
        $('.fg-float .form-control').each(function () {
            var i = $(this).val();

            if (!i.length == 0) {
                $(this).closest('.fg-line').addClass('fg-toggled');
            }

        });
    }


    /*   Form Wizard Functions  */
    /*--------------------------*/
    _handleTabShow = function (tab, navigation, index, wizard) {
        var total = navigation.find('li').length;
        var current = index + 0;
        var percent = (current / (total - 1)) * 100;
        var percentWidth = 100 - (100 / total) + '%';
        navigation.find('li').removeClass('done');
        navigation.find('li.active').prevAll().addClass('done');

        wizard.find('.progress-bar').css({ width: percent + '%' });
        $('.form-wizard-horizontal').find('.progress').css({ 'width': percentWidth });
    };

    _updateHorizontalProgressBar = function (tab, navigation, index, wizard) {
        var total = navigation.find('li').length;
        var current = index + 0;
        var percent = (current / (total - 1)) * 100;
        var percentWidth = 100 - (100 / total) + '%';

        wizard.find('.progress-bar').css({ width: percent + '%' });
        wizard.find('.progress').css({ 'width': percentWidth });
    };

    /* Form Wizard - Example 1  */
    /*--------------------------*/
    $('#formwizard_simple').bootstrapWizard({
        onTabShow: function (tab, navigation, index) {
            _updateHorizontalProgressBar(tab, navigation, index, $('#formwizard_simple'));
        }
    });

    /* Form Wizard - Example 2  */
    /*--------------------------*/

    $('#formwizard_validation').bootstrapWizard({
        onNext: function (tab, navigation, index) {
            var form = $('#formwizard_validation').find("form");
            var valid = true;

            if (index == 1) {
                var fname = form.find('#firstname');
                var lastname = form.find('#lastname');

                if (!fname.val()) {
                    swal("You must enter your first name!");
                    fname.focus();
                    return false;
                }

                if (!lastname.val()) {
                    swal("You must enter your last name!");
                    lastname.focus();
                    return false;
                }
            }

            if (!valid) {
                return false;
            }
        },
        onTabShow: function (tab, navigation, index) {
            _updateHorizontalProgressBar(tab, navigation, index, $('#formwizard_validation'));
        }
    });

}

function wizard2() {
    // Code for the Validator


    // Wizard Initialization
    $('.wizard-card').bootstrapWizard({
        'tabClass': 'nav nav-pills',
        'nextSelector': '.btn-next',
        'previousSelector': '.btn-previous',

        onNext: function (tab, navigation, index) {
            var $valid = $('.wizard-card form').valid();
            if (!$valid) {
                $validator.focusInvalid();
                return false;
            }
        },

        onInit: function (tab, navigation, index) {

            //check number of tabs and fill the entire row
            var $total = navigation.find('li').length;
            var $width = 100 / $total;
            var $wizard = navigation.closest('.wizard-card');

            var $display_width = $(document).width();

            if ($display_width < 600 && $total > 3) {
                $width = 50;
            }

            navigation.find('li').css('width', $width + '%');
            var $first_li = navigation.find('li:first-child a').html();
            var $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
            $('.wizard-card .wizard-navigation').append($moving_div);
            refreshAnimation($wizard, index);
            $('.moving-tab').css('transition', 'transform 0s');
        },

        //onTabClick: function (tab, navigation, index) {
        //    var $valid = $('.wizard-card form').valid();

        //    if (!$valid) {
        //        return false;
        //    } else {
        //        return true;
        //    }
        //},

        onTabShow: function (tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index + 1;

            var $wizard = navigation.closest('.wizard-card');

            // If it's the last tab then hide the last button and show the finish instead
            if ($current >= $total) {
                $($wizard).find('.btn-next').hide();
                $($wizard).find('.btn-finish').show();
            } else {
                $($wizard).find('.btn-next').show();
                $($wizard).find('.btn-finish').hide();
            }

            var button_text = navigation.find('li:nth-child(' + $current + ') a').html();

            setTimeout(function () {
                $('.moving-tab').text(button_text);
            }, 150);

            var checkbox = $('.footer-checkbox');

            if (!index == 0) {
                $(checkbox).css({
                    'opacity': '0',
                    'visibility': 'hidden',
                    'position': 'absolute'
                });
            } else {
                $(checkbox).css({
                    'opacity': '1',
                    'visibility': 'visible'
                });
            }

            refreshAnimation($wizard, index);
        }
    });


    // Prepare the preview for profile picture
    $("#wizard-picture").change(function () {
        readURL(this);
    });

    $('[data-toggle="wizard-radio"]').on('click', function () {
        wizard = $(this).closest('.wizard-card');
        wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
        $(this).addClass('active');
        $(wizard).find('[type="radio"]').removeAttr('checked');
        $(this).find('[type="radio"]').attr('checked', 'true');
    });

    $('[data-toggle="wizard-checkbox"]').on('click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).find('[type="checkbox"]').removeAttr('checked');
        } else {
            $(this).addClass('active');
            $(this).find('[type="checkbox"]').attr('checked', 'true');
        }
    });

    $('.set-full-height').css('height', 'auto');

    //Function to show image before upload

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(window).resize(function () {
        $('.wizard-card').each(function () {
            $wizard = $(this);
            index = $wizard.bootstrapWizard('currentIndex');
            refreshAnimation($wizard, index);

            $('.moving-tab').css({
                'transition': 'transform 0s',
                'width': '286'
            });
        });
    });

    function refreshAnimation($wizard, index) {

        var total_steps = $wizard.find('li').length;
        var move_distance = $wizard.width() / total_steps;
        var step_width = move_distance;
        move_distance *= index;

        var $current = index + 1;

        if ($current == 1) {
            move_distance -= 8;
        } else if ($current == total_steps) {
            move_distance += 8;
        }

        $wizard.find('.moving-tab').css('width', step_width);
        $('.moving-tab').css({
            'transform': 'translate3d(' + move_distance + 'px, 0, 0)',
            'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

        });
    }

}

function bindAddPurchaseOrder() {
    $("body").on("click", "#addPO", function () {

        clearAllMessages("PurchaseOrderForm")

        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("Purchase Order", "PurchaseOrderForm")
        }
        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("Purchase Order", "PurchaseOrderForm", service_DeveloperMode)

        $.ajax({
            type: "POST",
            url: "" + serviceUrl + "/AddPO",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {

                        $("#formModalLabelPO").text("Add");
                        $(".nav-pills li:nth-child(1)").addClass('active')
                        $("#btnPrevious").css({ "display": "none" })
                        $("#btnSave").css({ "display": "none" })
                        $("#btnPrint").css({ "display": "none" })
                        $("#btneditstatus").css({ "display": "none" })
                        clearFields()
                        setDateAndLocation("dtPODate", ".cboLocation")
                        isValueExist("#dtPODate", $("#dtPODate").val())
                        $("#modalAddPO").modal();
                        getAddress($(".cboFromBillTo").attr("_type"), $(".cboFromBillTo").val())
                        getAddress($(".cboBillTo").attr("_type"), $(".cboBillTo").val())
                        getAddress($(".cboShipTo").attr("_type"), $(".cboShipTo").val())
                    }

                }
            },
            error: OnErrorCall

        });

    })
}


function bindEditPurchaseOrder() {
    $("body").on("click", "#edit", function () {
        debugger

        clearAllMessages("PurchaseOrderForm")

        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("Purchase Order", "PurchaseOrderForm")
        }
        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("Purchase Order", "PurchaseOrderForm", service_DeveloperMode)


        var id = $(this).attr("_record")

        $.ajax({
            type: "POST",
            url: "" + serviceUrl + "/getPurchaseOrderById",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ id: id }),
            dataType: "json",
            success: function (response) {
                clearFields()
                let totalAmount = 0
                let PKRTotal = 0
                var result = JSON.parse(response.d)
                if (!result.Status) {
                    $("#Validation").fadeIn(1000);
                    $("#Validation").text(data.Message);
                    $("#Validation").fadeOut(5000);
                } else {

                    $("#POID").val(result.PurchaseOrder.PurchaseOrderID)
                    $("#PONumber").val(result.PurchaseOrder.PurchaseOrderNumber)
                    $(".cboFromBillTo").val(result.PurchaseOrder.FromBillTo)
                    $(".cboBillTo").val(result.PurchaseOrder._To)
                    $(".cboShipTo").val(result.PurchaseOrder.ShipTo)
                    $("#txtFromBillTo").val(result.PurchaseOrder.FromBillToAddress)
                    $("#txtBillTo").val(result.PurchaseOrder.ToAddress)
                    $("#txtShipTo").val(result.PurchaseOrder.ShipToAddress)
                    $("#dtPODate").val(getFormatedDate(result.PurchaseOrder.PurchaseOrderDate))
                    $(".cboCurrency").val(result.PurchaseOrder.Currency)
                    $("#txtExchange").val(result.PurchaseOrder.ExchangeRate.slice(0, 6))
                    $("#DTBillDate").val(result.PurchaseOrder.BillDate.split(' ')[0])
                    $("#txtSPOTNumber").val(result.PurchaseOrder.SpotNumber)
                    $(".cboPOStatus").val(result.PurchaseOrder.Status)
                    $("#txtPORef").val(result.PurchaseOrder.PurchaseOrderReference)
                    $(".cboSignature").val(result.PurchaseOrder.Signature)
                    $("#txtNotes").val(result.PurchaseOrder.Notes)
                    $("#txtComments").val(result.PurchaseOrder.Comments)
                    $(".cboRegion").val(result.PurchaseOrder.Reason)
                    $("#txtReasonDetail").val(result.PurchaseOrder.ReasonDetail)
                    $(".cboLocation").val(1)    //when location save to db

                    $(result.GenericList).each(function (index, row) {
                        var GUID = Math.random().toString(36).substr(2, 9);
                        var tr = ""
                        tr += "<tr id='" + GUID + "'>" +
                              "<td>" + row.ModalNumber + "</td>" +
                        "<td>" + row.ModalNumber + "</td>" +
                        "<td>" + row.PartNumber + "</td>" +
                        "<td>" + row.Unit + "</td>" +
                        "<td>" + row.Quantity + "</td>" +
                        "<td>" + parseInt(row.UnitPrice) + "</td>" +
                        "<td>" + row.Quantity * parseInt(row.UnitPrice) + "</td>" +
                        "<td>" + $("#txtExchange").val() * (row.Quantity * parseInt(row.UnitPrice)) + "</td>" +
                        "<td>" + row.OtherCost + "</td>" +
                        "<td style='display:none'>" + row.Remarks + "</td>" +
                        "<td style='display:none'>" + row.ItemId + "</td>" +
                        "<td><a href='#' class='deleteDetail' title='Delete'><i class='fa fa-trash'></i></a> <a href='#' class='editDetail' _action='edit' title='Edit'><i class='fa fa-pencil'></i></a></td>" +
                        "</tr>"
                        $("#tbldetailbody").append(tr)
                        totalAmount += row.Quantity * parseInt(row.UnitPrice)
                        PKRTotal += $("#txtExchange").val() * (row.Quantity * parseInt(row.UnitPrice))
                    })


                    //$("#tbldetailbody tr").each(function (index, row) {
                    //    totalAmount = totalAmount + parseInt($(row).find("td:nth-child(7)").text())
                    //    PKRTotal = PKRTotal + parseInt($(row).find("td:nth-child(8)").text())
                    //})
                    $("#txtTotalAmount").val(totalAmount)
                    $("#txtPKRTotalAmount").val(PKRTotal)

                    $("#formModalLabelPO").text("Edit");
                    $(".nav-pills li:nth-child(1)").addClass('active')
                    $("#btnPrevious").css({ "display": "none" })
                    $("#btnSave").css({ "display": "none" })
                    $("#btnPrint").css({ "display": "" })
                    $("#btneditstatus").css({ "display": "" })
                    $("#modalAddPO").modal();
                }
            }, error: function (responce) {
                alert(responce)
            }, complete: function () {
                isValueExist(".cboFromBillTo", $(".cboFromBillTo").val())
                isValueExist(".cboBillTo", $(".cboBillTo").val())
                isValueExist(".cboShipTo", $(".cboShipTo").val())
                isValueExist("#txtFromBillTo", $("#txtFromBillTo").val())
                isValueExist("#txtBillTo", $("#txtBillTo").val())
                isValueExist("#txtShipTo", $("#txtShipTo").val())
                isValueExist("#dtPODate", $("#dtPODate").val())
                isValueExist("#txtExchange", $("#txtExchange").val())
                isValueExist("#DTBillDate", $("#DTBillDate").val())
                isValueExist("#txtSPOTNumber", $("#txtSPOTNumber").val())
                isValueExist("#txtPORef", $("#txtPORef").val())
                isValueExist("#txtNotes", $("#txtNotes").val())
                isValueExist("#txtComments", $("#txtComments").val())
                isValueExist("#txtReasonDetail", $("#txtReasonDetail").val())

            }

        })


        //getAddress($(".cboFromBillTo").attr("_type"), $(".cboFromBillTo").val())
        //getAddress($(".cboBillTo").attr("_type"), $(".cboBillTo").val())
        //getAddress($(".cboShipTo").attr("_type"), $(".cboShipTo").val())

    })
}

function bindOnChangeEvent() {
    $("body").on("change", ".cboFromBillTo, .cboBillTo, .cboShipTo", function () {
        var type = $(this).attr('_type')
        var selectedItem = $(this).val()
        getAddress(type, selectedItem)
    })
}

function getAddress(type, selectedItem) {
    $.ajax({
        type: "POST",
        url: "" + serviceUrl + "/GetAddress",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ item: selectedItem, type: type }),
        dataType: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (type == "From") {
                $("#txtFromBillTo").val(result)
            } else if (type == "To") {
                $("#txtBillTo").val(result)
            } else {
                $("#txtShipTo").val(result)
            }

        },
        error: OnErrorCall
    })
}


function bindAddEditProductInfo() {
    $("body").on("click", "#addProductinfo, .editDetail", function () {

        clearAllMessages("modal-product-info")

        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("Purchase Order Detail", "modal-product-info")
        }
        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("Purchase Order Detail", "modal-product-info", service_DeveloperMode)

        if ($(this).attr("_action") == "add") {
            ClearDetailFields();
            $("#formModalLabel").text("Add");
        } else {
            var currentElement = $(this).closest('tr')
            $("#GUID").val($(currentElement).attr('id'))
            $("#txtModel").val($(currentElement).find('td:nth-child(1)').text())
            $("#txtPart").val($(currentElement).find('td:nth-child(2)').text())
            $("#txtDefaultDescription").val($(currentElement).find('td:nth-child(3)').text())
            $("#txtUnit").val($(currentElement).find('td:nth-child(4)').text())
            $("#txtQuantity").val($(currentElement).find('td:nth-child(5)').text())
            $("#txtUnitPrice").val($(currentElement).find('td:nth-child(6)').text())
            $("#txtAmount").val($(currentElement).find('td:nth-child(7)').text())
            $("#txtPKRAmount").val($(currentElement).find('td:nth-child(8)').text())
            $("#txtOtherCost").val($(currentElement).find('td:nth-child(9)').text())
            $("#txtItemRemarks").val($(currentElement).find('td:nth-child(10)').text())
            $("#formModalLabel").text("Edit");
        }
        $("#modalAdd").modal();
    })
}

function ClearDetailFields() {
    $("#modal-product-info input").each(function (index, row) {
        $(row).val("")
    })
}

function bindOnSearchProduct() {
    $("body").on("click", "#btnsearchproduct", function () {
        debugger
        $.ajax({
            type: "POST",
            url: "" + serviceUrl + "/GetProducts",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $("#tblProductList").DataTable().destroy();
                LoadDataTableForProductDetail(response.d);
                $("#H4").text("Search Items");
                $("#modalView2").modal();

            },
            error: OnErrorCall
        });
    })
}


function LoadDataTableForProductDetail(datasource) {
    table2 = $("#tblProductList").DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [
                    {
                        data: null,
                        sDefaultContent: "",
                        "orderable": false,
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var select = ' <a href="#" class="select"><i class="fa fa-check-circle"></i></a>';
                                var html = select;
                                return html;
                            }
                            return data;
                        }
                    },
                        { data: "ID", sDefaultContent: "" },
                        { data: "PartNumber", sDefaultContent: "" },
                        { data: "ModelNumber", sDefaultContent: "" },
                        { data: "ProductName", sDefaultContent: "" },
                        { data: "ItemClass", sDefaultContent: "" },

        ],
        "columnDefs": [
             { "targets": [1], "visible": false, "searchable": false },
        ]
    });
}

function bindOnProductSelection() {
    $("body").on("click", ".select", function () {
        var row = $(this).closest('tr');
        var rowIndex = table2.row(row).index();
        var rowData = table2.row(rowIndex).data();
        findProductDetail(rowData.ID)
        $("#modalView2").modal('hide');
    })
}

function findProductDetail(ID) {
    $.ajax({
        type: "POST",
        url: "" + serviceUrl + "/FindProductDetail",
        data: JSON.stringify({ ID: ID }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $("#itemid").val(data[0].ID)
            $("#txtUnit").val(data[0].Unit)
            $("#txtPart").val(data[0].PartNumber)
            $("#txtModel").val(data[0].ModelNumber)
            $("#txtDefaultDescription").val(data[0].DefaultDescription)
            $("#txtQuantity").focus()
        },
        error: OnErrorCall
    });
}

function bindCalculateAmount() {
    $("body").on("focusout", "#txtUnitPrice", function () {
        var currrency = $(".cboCurrency").val()
        var exchangeRate = $("#txtExchange").val()
        if (currrency <= 0 || exchangeRate == "") {
            alert("Please select and enter exchange rate")
            $("#modalAdd").modal('hide');
        } else {
            var unitPrice = $(this).val()
            var quantity = $("#txtQuantity").val()
            if (unitPrice == "" || quantity == "") {
                alert("Please enter quantity and unit price")
            } else {
                $("#txtAmount").val(unitPrice * quantity)
                $("#txtPKRAmount").val((unitPrice * quantity) * exchangeRate)
            }
        }
    })
}

function bindAppendSelectedProductDetail() {
    $("body").on("click", "#btnSaveDetail", function () {
        var isValid = true
        $("body #modal-product-info input").each(function (index, row) {
            if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
                if ($(row).val() == "") {
                    $(row).parent().find("span").eq(2).removeClass("hide")
                    isValid = false
                } else {
                    $(row).parent().find("span").eq(2).addClass("hide")
                }
            }
        })

        $("body #modal-product-info select").each(function (index, row) {
            if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
                if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                    $(row).parent().find("span").eq(2).removeClass("hide")
                    isValid = false
                } else {
                    $(row).parent().find("span").eq(2).addClass("hide")
                }
            }
        })

        if (isValid) {

            var id = $("#GUID").val()
            if (id != "") {
                $("#tbldetailbody tr").each(function (index, row) {
                    if ($(row).attr('id') == id) {
                        $(row).find('td:nth-child(1)').text($("#txtModel").val())
                        $(row).find('td:nth-child(2)').text($("#txtPart").val())
                        $(row).find('td:nth-child(3)').text($("#txtDefaultDescription").val())
                        $(row).find('td:nth-child(4)').text($("#txtUnit").val())
                        $(row).find('td:nth-child(5)').text($("#txtQuantity").val())
                        $(row).find('td:nth-child(6)').text($("#txtUnitPrice").val())
                        $(row).find('td:nth-child(7)').text($("#txtAmount").val())
                        $(row).find('td:nth-child(8)').text($("#txtPKRAmount").val())
                        $(row).find('td:nth-child(9)').text($("#txtOtherCost").val())
                        $(row).find('td:nth-child(10)').text($("#txtItemRemarks").val())
                    }
                })
            } else {
                var GUID = Math.random().toString(36).substr(2, 9);
                var tr = ""
                tr += "<tr id='" + GUID + "'>" +
                      "<td>" + $("#txtModel").val() + "</td>" +
                "<td>" + $("#txtPart").val() + "</td>" +
                "<td>" + $("#txtDefaultDescription").val() + "</td>" +
                "<td>" + $("#txtUnit").val() + "</td>" +
                "<td>" + $("#txtQuantity").val() + "</td>" +
                "<td>" + $("#txtUnitPrice").val() + "</td>" +
                "<td>" + $("#txtAmount").val() + "</td>" +
                "<td>" + $("#txtPKRAmount").val() + "</td>" +
                "<td>" + $("#txtOtherCost").val() + "</td>" +
                "<td style='display:none'>" + $("#txtItemRemarks").val() + "</td>" +
    "<td style='display:none'>" + $("#itemid").val() + "</td>" +
                "<td><a href='#' class='deleteDetail' title='Delete'><i class='fa fa-trash'></i></a> <a href='#' class='editDetail' _action='edit' title='Edit'><i class='fa fa-pencil'></i></a></td>" +
                "</tr>"
                $("#tbldetailbody").append(tr)
            }
            var totalAmount = 0
            var PKRTotal = 0
            $("#tbldetailbody tr").each(function (index, row) {
                totalAmount = totalAmount + parseInt($(row).find("td:nth-child(7)").text())
                PKRTotal = PKRTotal + parseInt($(row).find("td:nth-child(8)").text())
            })
            $("#txtTotalAmount").val(totalAmount)
            $("#txtPKRTotalAmount").val(PKRTotal)
            $("#modalAdd").modal('hide');
        }
    })
}

function bindDeleteDetail() {
    $("body").on("click", ".deleteDetail", function () {
        $(this).closest('tr').remove()
        var totalAmount = 0
        var PKRTotal = 0
        $("#tbldetailbody tr").each(function (index, row) {
            totalAmount = totalAmount + parseInt($(row).find("td:nth-child(7)").text())
            PKRTotal = PKRTotal + parseInt($(row).find("td:nth-child(8)").text())
        })
        $("#txtTotalAmount").val(totalAmount)
        $("#txtPKRTotalAmount").val(PKRTotal)
    })
}
//save Purchase Order 



function bindShowPurchaseInvoices() {
    $("body").on("click", "#displayPurchaseInvoices", function () {
        $.ajax({
            type: "POST",
            url: "" + serviceUrl + "/GetPuchaseInvoices",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $(tableName).DataTable().destroy();
                LoadDataTable(response.d);
                $("#H1").text("Show");
                $("#modalShow").modal();

            },
            error: OnErrorCall
        });
    })
}

function LoadDataTable(datasource) {
    table = $(tableName).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [

                        { data: "PurchaseOrderDate", sDefaultContent: "" },
                        { data: "PurchaseOrderNumber", sDefaultContent: "" },
                        { data: "ToName", sDefaultContent: "" },
                        { data: "Number", sDefaultContent: "" },
                        { data: "PurchaseOrder", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    var select = ' <a href="#" class="select"><b>Select</b></a>';
                                    var html = select;
                                    return html;
                                }
                                return data;
                            }
                        },

        ],
        "columnDefs": [

            { "width": "30%", "targets": 1 }
        ]
    });
}

//Delete Purchase Order 
function bindDeleteClick() {
    $("body").on("click", "#delete", function () {
        var id = $(this).attr('_record');
        rowId = id;
        PONumber = $(this).closest('tr').find('td:nth-child(1)').text()

        $.ajax({
            type: "POST",
            url: "" + commonService + "/isAuthenticatedForDelete",
            data: JSON.stringify({ form: "Purchase Order" }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        $("#POdeletemodal").modal();
                    }
                }
            },
            error: OnErrorCall
        });
    })
}


function OnConfirmDelete() {
    if (rowId != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceUrl + "/deletePurchaseOrder",
            data: JSON.stringify({ dataID: rowId, name: PONumber }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("Purchase Order deleted successfully");
                        $("#Validation").fadeOut(5000);
                        bindGetAllPurchaseOrder()
                        $("#POdeletemodal").modal('hide');

                    } else if (data.Message == "failed") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(failedmsg);
                        $("#Validation").fadeOut(5000);
                        $("#POdeletemodal").modal('hide');
                    } else if (data.Message == "exist") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("It is used by another process, Please try again!");
                        $("#Validation").fadeOut(5000);
                        $("#POdeletemodal").modal('hide');
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}

// All Purchase Order

function MakeDataGridForPurchaseOrder(datasource) {
    let data = JSON.parse(datasource)
    $(data).each(function (index, row) {
        row.PurchaseOrderDate = getFormatedDate(row.PurchaseOrderDate)
    })
    $("#purchaseorders").DataTable().destroy()
    $("#purchaseorders").DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        dom: 'Bfrtip',
        buttons: [
             {
                 extend: 'print',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Purchase Order',
                 text: '<i class="fa fa-lg fa-print"></i>  Print'

             },
             {
                 extend: 'excelHtml5',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Purchase Order',
                 text: '<i class="fa fa-lg fa-file-excel"></i>  Excel',
             },
            {
                extend: 'pdf',
                footer: true,
                exportOptions: {
                    columns: [0, 1, 3]
                },
                title: 'Purchase Order',
                text: '<i class="fa fa-lg fa-file-pdf"></i>  PDF'
            },
        ],
        "aaData": data,
        "aoColumns": [
                        { data: "PurchaseOrderID", sDefaultContent: "" },
                        { data: "PurchaseOrderNumber", sDefaultContent: "", width: "150" },
                        { data: "PurchaseOrderDate", sDefaultContent: "", width: "80" },
                        { data: "ToAddress", sDefaultContent: "" },
                      {
                          data: null,
                          width: "50",
                          sDefaultContent: "",
                          "orderable": false,
                          render: function (data, type, row) {
                              if (type === 'display') {
                                  //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                  var del = ' <a href="#" _record= "' + data.PurchaseOrderID + '" class="del" id="delete" title="Delete"><i class="fa fa-trash"></i></a>';
                                  var seperator = ' | '
                                  var edit = ' <a href="#" _record= "' + data.PurchaseOrderID + '" class="edit" id="edit" title="Edit"><i class="fa fa-pencil-alt"></i></a>';
                                  var html = del + seperator + edit;
                                  return html;
                              }
                              return data;
                          }
                      },

        ],
        "columnDefs": [

           { "targets": [0], "searchable": false, "visible": false },
        ]
    })
}

function bindUpdatePurchaseOrderStatus() {
    $("#btneditstatus").on("click", function () {
        let reason = $(".cboRegion").val()
        let reasonDetail = $("#txtReasonDetail").val()
        let poId = $("#POID").val()
        if (reason != "") {
            $.ajax({
                type: "POST",
                url: "" + serviceUrl + "/UpdatePurchaseOrderStatus",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ "reason": reason, "detail": reasonDetail, "id": poId }),
                dataType: "json",
                success: function (response) {
                    $("#Validation").fadeIn(1000);
                    $("#Validation").text(response.d);
                    $("#Validation").fadeOut(5000);
                },
                error: OnErrorCall
            });
        }
    })
}




function clearFields() {

    $("#orderInfo , #productInfo").find('input , textarea').val(" ")
    $("#orderInfo select").find('option:first').prop("selected", true)
    $("#productInfo #tbldetailbody").html(" ")

    $("#btnNext").css({ "display": "" })

}

function OnErrorCall() {
    alert("Something went wrong");
}
//Remove special characters from Json

function removecharacters(data) {

    jsondata = data.replace(/\\n/g, "\\n")
               .replace(/\\'/g, "\\'")
               .replace(/\\"/g, '\\"')
               .replace(/\\&/g, "\\&")
               .replace(/\\r/g, "\\r")
               .replace(/\\t/g, "\\t")
               .replace(/\\b/g, "\\b")
               .replace(/\\f/g, "\\f");
    // remove non-printable and other non-valid JSON chars
    return json = jsondata.replace(/[\u0000-\u0019]+/g, "");
}