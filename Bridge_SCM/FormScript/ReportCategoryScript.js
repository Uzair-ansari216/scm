﻿var serviceurl = "../../appServices/ReportCategoryService.asmx";
var tblname = "#tableCity";
var table;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _ID = "#txtID";
var _Name = "#txtName";

var loginUrl = "/Default.aspx";


$(document).ready(function () {
    OnLoadGrid();
    OnDelete();
    OnEdit();
});

function OnSave() {
    var spFound;
    $.getScript('../../FormScript/ValidationScript.js', function () {
        spFound = valid();

        FormValidaton();

        var dataSend = {
            ID: $(_ID).val(),
            Description: $(_Name).val()
        }

        if (msg == "") {
            if (spFound == false) {
                $.ajax({
                    type: "POST",
                    url: "" + serviceurl + "/OnSave",
                    data: JSON.stringify({ model: dataSend }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d == "sessionNotFound") {
                            alert("sessionNotFound");
                            window.location.href = loginUrl;
                        } else {
                            var data = JSON.parse(response.d);
                            console.log(data);
                            if (data.Message == "success") {
                                ClearFields();
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text(success);
                                $("#Validation").fadeOut(5000);
                                OnLoadGrid();
                                $("#modalAdd").modal('hide');
                            } else if (data.Message == "exist") {
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text(exist);
                                $("#Validation").fadeOut(5000);
                                OnLoadGrid();
                                $("#modalAdd").modal('hide');
                            } else if (data.Message == "update") {
                                $("#Validation").fadeIn(1000);
                                $("#Validation").text(update);
                                $("#Validation").fadeOut(5000);
                                OnLoadGrid();
                                $("#modalAdd").modal('hide');
                            }
                        }
                    },
                    error: OnErrorCall
                });
            }
            else {
                alert("Illegal Characters Detected!")
            }
        } else {

        }
    });
}

function FormValidaton() {
    msg = "";

    $("#rName").addClass("hide");


    if ($(_Name).val() == "") {
        msg += "Type is required";
        $("#rName").removeClass("hide");
    } else {
        $("#rName").addClass("hide");
    }

    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}

function ClearFields() {
    $(_Name).val("");
    $(_ID).val("");
    $("#ValidationSummary").text("");
    $("#rName").addClass("hide");

}

function OnLoadGrid() {
    //alert('5')
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblname).DataTable().destroy();
            LoadDataTable(response.d);
        },
        error: OnErrorCall
    });


}

function OnConfirmDelete() {
    Delete(DeleteID);
}

var DeleteID = "";
function OnDelete() {
    $(tblname).on('click', '.del', function () {
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData);
        DeleteID = rowData.ID;

        if (DeleteID != "") {
            $("#modalDelete").modal();
        }

    });
}

function OnEdit() {
    $(tblname).on('click', '.edit', function () {
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData);

        $(_ID).val(rowData.ID);
        $(_Name).val(rowData.Description);
        $("#formModalLabel").text("Edit");
        $("#modalAdd").modal();

        $("#ValidationSummary").text("");
        $("#rName").addClass("hide");
    });
}

function Delete(id) {
    if (id != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDelete",
            data: JSON.stringify({ dataID: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(delt);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalDelete").modal('hide');

                    } else if (data.Message == "failed") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(failedmsg);
                        $("#Validation").fadeOut(5000);
                    } else if (data.Message == "use") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("It is used by another process, Please try again!");
                        $("#Validation").fadeOut(5000);
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}


function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        dom: 'Bfrtip',
        buttons: [
                {
                    extend: 'print',
                    footer: true,
                    exportOptions: {
                        columns: [0, 1]
                    },
                    title: 'Report Category',
                    text: '<i class="fa fa-lg fa-file-text-o"></i>  Print'

                },
                {
                    extend: 'excelHtml5',
                    footer: true,
                    exportOptions: {
                        columns: [0, 1]
                    },
                    title: 'Report Category',
                    text: '<i class="fa fa-lg fa-file-excel-o"></i>  Excel',
                },
               {
                   extend: 'pdf',
                   footer: true,
                   exportOptions: {
                       columns: [0, 1]
                   },
                   title: 'Report Category',
                   text: '<i class="fa fa-lg fa-file-pdf-o"></i>  PDF'
               },
        ],
        "aaData": JSON.parse(datasource),
        "aoColumns": [

                        { data: "ID", sDefaultContent: "" },
                        { data: "Description", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" class="del" title="del"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#" class="edit" title="Edit"><i class="fa fa-pencil" ></i></a>';
                                    var html = del + edit;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [
          //  { "targets": [0], "searchable": false, "visible": false },
          //  { "targets": [4], "searchable": false, "visible": false }

        ]
    });
}

function OnAdd() {
    ClearFields();
    $("#formModalLabel").text("Add");
    $("#modalAdd").modal();
}


function OnErrorCall() {
    alert("Something went wrong");
}
