﻿function GetToday() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var today = mm + '/' + dd + '/' + yyyy;
    return today;
}

function FillDropdown(pagemane, urlname, ddlname) {
    debugger
    $.ajax({
        type: "POST",
        url: '' + pagename + '/' + urlname + '',
        contentType: "application/json",
        dataType: "json",
        success: function (res) {
            $("#" + ddlname + "").empty();
            $("#" + ddlname + "").append("<option value='-1'>--Please Select--</option>");
            jQuery.each(res.d, function (index, item) {
                $("#" + ddlname + "").append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });

        },
        failure: function (errMsg) {
            alert(errMsg);
        }
    });

}

function FillDropdownByID(pagename, urlname, ddlname, id) {

    if (id != "-1") {
        $.ajax({
            type: "POST",
            url: '' + pagename + '/' + urlname + '?id=' + id,
            contentType: "application/json",
            dataType: "json",
            success: function (res) {
                $("#" + ddlname + "").empty();
                $("#" + ddlname + "").append("<option value='-1'>--Please Select--</option>");
                jQuery.each(res.d, function (index, item) {
                    $("#" + ddlname + "").append("<option value=" + item.Value + "> " + item.Text + " </option>");
                });

            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    }

}
