﻿var serviceurl = "../../appServices/StatusApprovalService.asmx";
var tblname = "#tableData";
var table;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _ID = "#txtID";
var _Name = "#txtName";
var _SafetyIns = "#txtSafetyIns";

var _Status = "#ddlStatus";
var _StatusDate = "#txtStatusDate";
var _StatusRemarks = "#txtStatusRemarks";

var loginUrl = "/Default.aspx";


$(document).ready(function () {
    $(_StatusDate).datepicker({ autoclose: true });
    OnLoadGrid();
    FillStatus();
    OnDelete();
    OnEdit();
    OnSelectCheckBox();
   // OnMainCheckBox();
});
//function OnMainCheckBox() {
//    $('#chkSelectMain').change(function () {
//        var chk = 0;
//        if ($(this).is(":checked")) {
//            chk = 1;
//            $(tblname).find('input[type="checkbox"]').each(function () {
//                //this is the current checkbox
//                $('input[type="checkbox"]').prop('checked', true);
//            });
           
//        } else {
//            chk = 0;
//            $(tblname).find('input[type="checkbox"]').each(function () {
//                //this is the current checkbox
//                $('input[type="checkbox"]').prop('checked', false);
//            });
//            //$('input[type="checkbox"]').prop('checked', false);
//        }
//        var table = $(tblname).DataTable();
//        var table_length = table.data().count();

//        //for (var i = 1; i <= table_length; i++) {
//            isSelected2(chk);
//        //}
//    });
//}
function isSelected2(chk) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/OnCheckedChange2",
        data: JSON.stringify({ chk: chk }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

        },
        error: OnErrorCall
    });
}
function OnSelectCheckBox() {
    $(tblname).on('change', '.checkbox', function () {

        var chkSelect = $(this).find("#chkSelect");

        var chk = 0;
        if ($(chkSelect).is(":checked")) {
            chk = 1;
        } else { chk = 0; }

        var row = $(this).parent().parent();
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData);
        isSelected(rowData.rowID, chk);

    });
}

function isSelected(rowID, chk) {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/OnCheckedChange",
        data: JSON.stringify({ rowID: rowID, chk: chk }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

        },
        error: OnErrorCall
    });
}

function FillStatus() {
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetStatusList',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Status).empty();
            $(_Status).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Status).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }
    });
}

function OnSave() {
    var spFound;
    $.getScript('../../FormScript/ValidationScript.js', function () {
        spFound = valid();
    //FormValidaton();

    var dataSend = {
        Status: $(_Status).val(),
        StatusDate: $(_StatusDate).val(),
        Remarks: $(_StatusRemarks).val()
    }

     //  if (msg == "") {
    if (spFound == false) {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnSave",
            data: JSON.stringify({ model: dataSend }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        ClearFields();
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(success);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        // $("#modalAdd").modal('hide');
                    } else if (data.Message == "exist") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(exist);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        //$("#modalAdd").modal('hide');
                    } else if (data.Message == "update") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(update);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        //$("#modalAdd").modal('hide');
                    }
                }
            },
            error: OnErrorCall
        });
        } else {
        alert("Illegal Characters Detected!");
        }
    //} else {
            
    //}
    });
}

function FormValidaton() {
    msg = "";

    $("#rName").addClass("hide");


    if ($(_Name).val() == "") {
        msg += "Type is required";
        $("#rName").removeClass("hide");
    } else {
        $("#rName").addClass("hide");
    }

    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}

function ClearFields() {
    $(_Status).val("-1");
    $(_StatusDate).val("");
    $(_StatusRemarks).val("");
    $("#ValidationSummary").text("");
   // $("#rName").addClass("hide");

}

function OnLoadGrid() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblname).DataTable().destroy();
            LoadDataTable(response.d);
        },
        error: OnErrorCall
    });


}

function OnConfirmDelete() {
    Delete(DeleteID);
}

var DeleteID = "";
function OnDelete() {
    $(tblname).on('click', '.del', function () {
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData);
        DeleteID = rowData.ID;

        if (DeleteID != "") {
            $("#modalDelete").modal();
        }

    });
}

function OnEdit() {
    $(tblname).on('click', '.edit', function () {
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData);

        $(_ID).val(rowData.ID);
        $(_Name).val(rowData.Description);
        $(_SafetyIns).val(rowData.Safty_ins);
        $("#formModalLabel").text("Edit");
        $("#modalAdd").modal();
        $(".label-floating").addClass("is-focused");

    });
}

function Delete(id) {
    if (id != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDelete",
            data: JSON.stringify({ dataID: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(delt);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalDelete").modal('hide');

                    } else if (data.Message == "failed") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(failedmsg);
                        $("#Validation").fadeOut(5000);
                    } else if (data.Message == "use") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("It is used by another process, Please try again!");
                        $("#Validation").fadeOut(5000);
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}


function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "bInfo": false,
       // "bPaginate" :false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [
                       {
                           data: null,
                           sDefaultContent: "",
                           "orderable": false,
                           render: function (data, type, row) {
                               if (type === 'display') {
                                   var select = '<div class="checkbox"><label><input type="checkbox" name="optionsCheckboxes" id="chkSelect" ><span class="checkbox-material"><span class="check"></span></span></label></div>';
                                   var html = select;
                                   return html;
                               }
                               return data;
                           }
                       },
                        { data: "OrderNo", sDefaultContent: "" },
                        { data: "SalesDate", sDefaultContent: "" },
                        { data: "CustomerName", sDefaultContent: "" },
                        { data: "EmployeeName", sDefaultContent: "" },
                        { data: "Refrences", sDefaultContent: "" },
                        { data: "Remarks", sDefaultContent: "" },
                        { data: "PK_SalesOrder", sDefaultContent: "" },
                        { data: "Status2", sDefaultContent: "" },
                        { data: "StatusRemarks", sDefaultContent: "" },
                        { data: "rowID", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    var view = ' <a href="#" class="view" title="Items"><i class="fa fa-eye" ></i></a>';

                                    return view;
                                }
                                return data;
                            }
                        }

        ],
        "columnDefs": [
            { "targets": [7], "searchable": false, "visible": false },
            { "targets": [8], "searchable": false, "visible": false },
            { "targets": [9], "searchable": false, "visible": false },
            { "targets": [10], "searchable": false, "visible": false }
          //  { "targets": [4], "searchable": false, "visible": false }

        ]
    });
}

function OnAdd() {
    $(".label-floating").removeClass("is-focused");
    ClearFields();
    $("#formModalLabel").text("Add");
    $("#modalAdd").modal();
}


function OnErrorCall() {
    alert("Something went wrong");
}
