﻿var commonService = "../../appServices/CommonService.asmx";
var serviceurl = "../../appServices/DepartmentService.asmx";
var tblname = "#tableCity";
var table;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _ID = "#txtdepartmentcode";
var _Name = "#txtdepartmentname";
var _DepartmentHead = "#ddlheadperson";
var _Description = "#txtdescription";
var _Action = "#txtAction";

var loginUrl = "/Default.aspx";

$(document).ready(function () {


    $("body #departmentForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #departmentForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #departmentForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })


    OnLoadGrid();
    bindAddEditDepartment();
    OnDelete();
    FillHeadPerson();

});

function OnLoadGrid() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            let data = JSON.parse(response.d)
            if (!data.Status) {
                $("#Validation").fadeIn(1000);
                $("#Validation").text(data.Message);
                $("#Validation").fadeOut(5000);
                $("#modalDelete").modal('hide');
            } else {
                $(tblname).DataTable().destroy();
                LoadDataTable(data.GenericList);
            }

        },
        error: OnErrorCall
    });
}

function bindAddEditDepartment() {
    $("body").on("click", "#btnAdd , .edit", function () {

        let id = $(this).attr("_recordId")
        clearAllMessages("departmentForm")

        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("Department", "departmentForm")
        }

        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("Department", "departmentForm", service_DeveloperMode)

        //}

        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/AddEditDepartment",
            data: JSON.stringify({ id: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        if (parseInt(id) > 0) {
                            $('input[id=txtdepartmentcode]').attr("readonly", true);
                            $('input[id=txtdepartmentcode]').prop("readonly", true);
                            $("#formModalLabel").text("Edit");
                            $(".label-floating").addClass("is-focused");
                            $(_Action).val("1");
                        } else {
                            $("#formModalLabel").text("Add");
                            $(".label-floating").removeClass("is-focused");
                            $("#ddlheadperson option:selected").text("-- Please Select --").trigger("change")
                        }

                        $(_ID).val(data.GenericList.ID);
                        $(_Name).val(data.GenericList.DepartmentName);
                        $(_DepartmentHead).val(data.GenericList.DepartmentHead) // $(this).attr("_recordId") rowData.DepartmentHead);

                        $("#modalAdd").modal();
                    }

                }
            },
            error: OnErrorCall,
            complete: function () {
                isValueExist(_Name, $(_Name).val())
                isValueExist(_DepartmentHead, $(_DepartmentHead).val())
            }
        });
    })
}

function OnSave() {
    debugger
    var isValid = true

    $("body #departmentForm input").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    $("body #departmentForm select").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    if (isValid) {
        var dataSend = {

            ID: $(_Action).val(),
            Code: $(_ID).val(),
            DepartmentName: $(_Name).val(),
            DepartmentHead: $(_DepartmentHead).val(),
            Description: $(_Description).val(),
        }
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnSave",
            data: JSON.stringify({ model: dataSend }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        ClearFields();
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                        $("#modalAdd").modal('hide');
                    }
                    if (data.Message == "success") {
                        ClearFields();
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(success);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "exist") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(exist);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "update") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(update);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    }
                }
            },
            error: OnErrorCall
        });

    }
    //var spFound;
    //$.getScript('../../FormScript/ValidationScript.js', function () {
    //    spFound = valid();

    //    FormValidaton();


    //    if (msg == "") {
    //        if (spFound == false) {

    //        }
    //        else {
    //            alert("Illegal Characters Detected!")
    //        }
    //    } else {

    //    }
    //});

}


function FormValidaton() {
    msg = "";

    $("#rDepartmentcode").addClass("hide");

    if ($(_ID).val() == "") {
        msg += "required";
        $("#rDepartmentcode").removeClass("hide");
    } else {
        $("#rDepartmentcode").addClass("hide");
    }

    $("#rDepartmentname").addClass("hide");

    if ($(_Name).val() == "") {
        msg += "required";
        $("#rDepartmentname").removeClass("hide");
    } else {
        $("#rDepartmentname").addClass("hide");
    }

    $("#rDescription").addClass("hide");

    if ($(_Description).val() == "") {
        msg += "required";
        $("#rDescription").removeClass("hide");
    } else {
        $("#rDescription").addClass("hide");
    }

    $("#rHeadperson").addClass("hide");

    if ($(_DepartmentHead).val() == "-1") {
        msg += "required";
        $("#rHeadperson").removeClass("hide");
    } else {
        $("#rHeadperson").addClass("hide");
    }


    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}

function ClearFields() {

    $(_Name).val("");
    $(_ID).val("");
    $(_Action).val("")
    $(_Description).val("");
    $(_DepartmentHead).val("-1");


    $('input[id=txtdepartmentcode]').attr('readonly', false);
    $('input[id=txtdepartmentcode]').prop('readonly', false);



    $("#ValidationSummary").text("");

    $("#rHeadperson").addClass("hide");
    $("#rDepartmentname").addClass("hide");
    $("#rDepartmentcode").addClass("hide");
    $("#rDescription").addClass("hide");
}

function OnConfirmDelete() {
    Delete(DeleteID, DeleteName);
}

var DeleteID = "";
var DeleteName = "";
function OnDelete() {
    $(tblname).on('click', '.del', function () {
        let id = $(this).attr('_recordId');
        DeleteID = id;
        $.ajax({
            type: "POST",
            url: "" + commonService + "/isAuthenticatedForDelete",
            data: JSON.stringify({ form: "Department" }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        $("#modalDelete").modal();
                    }
                }
            },
            error: OnErrorCall
        });
    });
}

function Delete(id, Name) {
    if (id != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDelete",
            data: JSON.stringify({ dataID: id, dataName: Name }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                        $("#modalDelete").modal('hide');
                    }
                    if (data.Message == "success") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(delt);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalDelete").modal('hide');

                    } else if (data.Message == "failed") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(failedmsg);
                        $("#Validation").fadeOut(5000);
                    } else if (data.Message == "use") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("It is used by another process, Please try again!");
                        $("#Validation").fadeOut(5000);
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}


function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        dom: 'Bfrtip',
        buttons: [
             {
                 extend: 'print',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Department',
                 text: '<i class="fas fa-print"></i>  Print'

             },
             {
                 extend: 'excelHtml5',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'Department',
                 text: '<i class="fas fa-file-excel"></i>  Excel',
             },
            {
                extend: 'pdf',
                footer: true,
                exportOptions: {
                    columns: [0, 1, 3]
                },
                title: 'Department',
                text: '<i class="fas fa-file-pdf"></i>  PDF'
            },
        ],
        "aaData": datasource,
        "aoColumns": [

                        { data: "ID", sDefaultContent: "" },
                        { data: "DepartmentName", sDefaultContent: "" },
                        { data: "Description", sDefaultContent: "" },
                        { data: "DepartmentHead", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#"  _recordId = ' + data.ID + ' class="del" title="del"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#"  _recordId = ' + data.ID + ' class="edit" title="Edit"><i class="fas fa-pencil-alt" ></i></a>';
                                    var html = del + edit;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [

            { "targets": [0], "width": "3%" },
            { "targets": [1], "width": "6%" },
            { "targets": [2], "width": "5%" },
            { "targets": [3], "width": "5%" },
            { "targets": [4], "width": "2%" }

        ]
    });
}

function OnErrorCall() {
    alert("Something went wrong");
}



function FillHeadPerson() {

    $.ajax({
        type: "POST",
        url: serviceurl + '/GetHeadPerson',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_DepartmentHead).empty();
            $(_DepartmentHead).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_DepartmentHead).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
            //$(_dependent).select2('val', index);

        },
        complete: function () {
            isValueExist(_DepartmentHead, $(_DepartmentHead).val())
        }
        //failure: function (errMsg) {
        //    alert(errMsg);
        //}
    });

}