﻿var commonService = "../../appServices/CommonService.asmx";
var serviceurl = "../../appServices/CityService.asmx";
var tblname = "#tableCity";
var table;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _ID = "#txtID";
var _Country = "#ddlcountry";
var _CityCode = "#txtcitycode";
var _CityName = "#txtcityname";


var loginUrl = "/Default.aspx";

$(document).ready(function () {

    $("body #cityForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #cityForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #cityForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })
    OnLoadGrid();
    FillCountry()
    bindAddEditCity();
    OnDelete();
    OnEdit();

});

function OnLoadGrid() {

    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblname).DataTable().destroy();
            LoadDataTable(response.d);
        },
        error: OnErrorCall
    });
}

function FillCountry() {

    $.ajax({
        type: "POST",
        url: serviceurl + '/GetCountryName',
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Country).empty();
            $(_Country).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Country).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
        }, complete: function () {
            isValueExist(_Country, $(_Country).val())
        }
    });
}

function bindAddEditCity() {
    $("body").on("click", "#btnAdd , .edit", function () {

        let id = $(this).attr("_recordId")
        clearAllMessages("cityForm")

        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("City", "cityForm")
        }
        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("City", "cityForm", service_DeveloperMode)

        //}

        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/AddEditCity",
            data: JSON.stringify({ id: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        if (parseInt(id) > 0) {
                            $("#formModalLabel").text("Edit");
                            $(".label-floating").addClass("is-focused");
                        } else {
                            $(".label-floating").removeClass("is-focused");
                            $("#formModalLabel").text("Add");
                        }
                        $(_ID).val(data.GenericList.ID);
                        $(_CityCode).val(data.GenericList.cityCode);
                        $(_CityName).val(data.GenericList.cityName);
                        $(_Country).val(data.GenericList.countryID);

                        $("#modalAdd").modal();
                    }

                }
            },
            error: OnErrorCall,
            complete: function () {
                isValueExist(_CityCode, $(_CityCode).val())
                isValueExist(_CityName, $(_CityName).val())
                isValueExist(_Country, $(_Country).val())
            }
        });
    })
}

function OnSave() {

    var isValid = true

    $("body #cityForm input").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    $("body #cityForm select").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('_ismandatory') != "false") {
            if ($(row).val() == "" || $(row).val() == "0" || $(row).val() == "-1") {
                $(row).parent().find("span").eq(2).removeClass("hide")
                isValid = false
            } else {
                $(row).parent().find("span").eq(2).addClass("hide")
            }
        }
    })

    if (isValid) {
        var dataSend = {
            ID: $(_ID).val(),
            countryID: $(_Country).val(),
            cityCode: $(_CityCode).val(),
            cityName: $(_CityName).val(),

        }
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnSave",
            data: JSON.stringify({ model: dataSend }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        ClearFields();
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(success);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "exist") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(exist);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "update") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(update);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalAdd").modal('hide');
                    }
                }
            },
            error: OnErrorCall
        });
    }
}

function FormValidaton() {
    msg = "";

    $("#rCitycode").addClass("hide");

    if ($(_CityCode).val() == "") {
        msg += "required";
        $("#rCitycode").removeClass("hide");
    } else {
        $("#rCitycode").addClass("hide");
    }



    $("#rCityname").addClass("hide");

    if ($(_CityName).val() == "") {
        msg += "required";
        $("#rCityname").removeClass("hide");
    } else {
        $("#rCityname").addClass("hide");
    }


    $("#rCountry").addClass("hide");

    if ($(_Country).val() == "-1") {
        msg += "required";
        $("#rCountry").removeClass("hide");
    } else {
        $("#rCountry").addClass("hide");
    }


    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}

function ClearFields() {
    $(_CityName).val("");
    $(_CityCode).val("");
    $(_ID).val("");

    $(_Country).val("-1");



    $("#ValidationSummary").text("");
    $("#rCitycode").addClass("hide");
    $("#rCityname").addClass("hide");
    $("#rCountry").addClass("hide");
}


var DeleteID = "";
function OnDelete() {
    $(tblname).on('click', '.del', function () {
        let id = $(this).attr('_recordId');
        DeleteID = id;
        $.ajax({
            type: "POST",
            url: "" + commonService + "/isAuthenticatedForDelete",
            data: JSON.stringify({ form: "City" }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (!data.Status) {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(data.Message);
                        $("#Validation").fadeOut(5000);
                    } else {
                        $("#modalDelete").modal();
                    }
                }
            },
            error: OnErrorCall
        });
    });
}

function OnConfirmDelete() {
    Delete(DeleteID);
}

function Delete(id) {
    if (id != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDelete",
            data: JSON.stringify({ dataID: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(delt);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalDelete").modal('hide');

                    } else if (data.Message == "failed") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(failedmsg);
                        $("#Validation").fadeOut(5000);
                    } else if (data.Message == "use") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("It is used by another process, Please try again!");
                        $("#Validation").fadeOut(5000);
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}


function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        dom: 'Bfrtip',
        buttons: [
             {
                 extend: 'print',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'City',
                 text: '<i class="fa fa-print"></i>  Print'

             },
             {
                 extend: 'excelHtml5',
                 footer: true,
                 exportOptions: {
                     columns: [0, 1, 3]
                 },
                 title: 'City',
                 text: '<i class="fa fa-file-excel"></i>  Excel',
             },
            {
                extend: 'pdf',
                footer: true,
                exportOptions: {
                    columns: [0, 1, 3]
                },
                title: 'City',
                text: '<i class="fa fa-file-pdf"></i>  PDF'
            },
        ],
        "aaData": JSON.parse(datasource),
        "aoColumns": [

                        { data: "ID", sDefaultContent: "" },
                        { data: "cityCode", sDefaultContent: "" },
                        { data: "cityName", sDefaultContent: "" },
                        { data: "countryID", sDefaultContent: "" },
                        { data: "countryName", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" _recordId = ' + data.ID + ' class="del" title="del"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#" _recordId = ' + data.ID + ' class="edit" title="Edit"><i class="fas fa-pencil-alt" ></i></a>';
                                    var html = del + edit;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [

            { "targets": [0], "width": "2%" },
            { "targets": [1], "width": "6%" },
            { "targets": [2], "width": "5%" },
            { "targets": [3], "visible": false },
            { "targets": [4], "width": "10%" },
            { "targets": [5], "width": "2%" }

        ]
    });
}

function OnErrorCall() {
    alert("Something went wrong");
}
