﻿var serviceurl = "../../appServices/LoginService.asmx";
var tblname = "#tableData";
var table;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _UserName = "#txtCode";
var _Password = "#txtpasswd";
var _Company = "#cmbCompany";
var _Location = "#cmbLocation"

var loginUrl = "/Default.aspx";


$(document).ready(function () {
    document.getElementById("txtWorkingDate").valueAsDate = new Date()
    //OnLoadGrid();
    //OnDelete();
    //OnEdit();
    //FillCompany();
    
    OnKeyCheck();
  
});

function OnKeyCheck() {
    
    $(_UserName).focusout(function () {
    
        //if ($(_UserName).val() != "" && $(_Password).val() != "") {
        if ($(_UserName).val() != "") {
            FillCompany()
        }
    });

    $(_Password).focusout(function () {        
        if ($(_UserName).val() != "" && $(_Password).val() != "") {
            FillCompany()
        }
      
    });

    $(_Company).on("change", function () {
        if ($(_Company).val() != "-1" && $(_UserName).val() != "") {
            var company = $(this).val()
            var user = $(_UserName).val()
            $.ajax({
                type: "POST",
                url: serviceurl + '/GetLocationListByCompany',
                data: JSON.stringify({ company: company, user: user }),
                contentType: "application/json",
                dataType: "json",
                success: function (response) {
                    debugger
                    var data = JSON.parse(response.d);
                    $(_Location).empty();
                    $(_Location).append("<option value='-1'>--Please Select--</option>");
                    jQuery.each(data, function (index, item) {
                        $(_Location).append("<option value=" + item.Value + "> " + item.Text + " </option>");
                    });
                }
            });

        }
    })
 
    //$(_Company).focusout(function () {       
    //    if ($(_Company).val() != "-1" && $(_UserName).val() != "") {
    //        FillLocation()
    //    }
    //});
}


function FillCompany() {   
    var dataSend = {
        User: $(_UserName).val()
        //Password: $(_Password).val()
    }
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetCompanyList',
        data: JSON.stringify({ model: dataSend }),
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Company).empty();
            $(_Company).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Company).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
            $('#cmbCompany option')[1].selected = true;
            //$(_Company).val("1")
            FillLocation()
        }


    });

  
}



function FillLocation() {
    var dataSend = {
        User: $(_UserName).val()
     
    }
    $.ajax({
        type: "POST",
        url: serviceurl + '/GetLocationList',
        data: JSON.stringify({ model: dataSend }),
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var data = JSON.parse(response.d);
            $(_Location).empty();
            $(_Location).append("<option value='-1'>--Please Select--</option>");
            jQuery.each(data, function (index, item) {
                $(_Location).append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
            $('#cmbLocation option')[1].selected = true;
            //$(_Location).val("1")
        }


    });
}

function OnSave() {
   
    FormValidaton();

    var dataSend = {
        User: $(_UserName).val(),
        Password: $(_Password).val(),
        Location: $(_Location).val(),
        Company: $(_Company).val(),
        WorkingDate: $("#txtWorkingDate").val(),
        FinancialYear: $("#cmbFinancial").val()
    }

    if (msg == "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/Login",
            data: JSON.stringify({ model: dataSend }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data[0] == "Welcome") {
                        window.location.href = data[1];
                        //ClearFields();
                        //$("#Validation").fadeIn(1000);
                        //$("#Validation").text(success);
                        //$("#Validation").fadeOut(5000);
                        //OnLoadGrid();
                        //$("#modalAdd").modal('hide');
                    } else {
                        $("#lblAllError").text(data[0])
                    }
            },
            error: OnErrorCall
        });
    } else {

    }
}

function FormValidaton() {
    msg = "";

   // $("#rName").addClass("hide");


    if ($(_Location).val() == "" || $(_Location).val() == null) {
        msg += "Location is required";
        //$("#rName").removeClass("hide");
    } else {
       // $("#rName").addClass("hide");
    }

    if ($(_Company).val() == "" || $(_Company).val() == null) {
        msg += "Company is required";
        //$("#rName").removeClass("hide");
    } else {
        // $("#rName").addClass("hide");
    }

    if (msg == "") {
        $("#lblAllError").text("");
    } else {
        $("#lblAllError").text("Please fill required values.");
       // alert("Please fill required values.");
    }
}

function ClearFields() {
    $(_Name).val("");
    $(_SafetyIns).val("");
    $(_ID).val("");
    $("#ValidationSummary").text("");
    $("#rName").addClass("hide");

}

function OnLoadGrid() {
    $.ajax({
        type: "POST",
        url: "" + serviceurl + "/loadGrid",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(tblname).DataTable().destroy();
            LoadDataTable(response.d);
        },
        error: OnErrorCall
    });


}

function OnConfirmDelete() {
    Delete(DeleteID);
}

var DeleteID = "";
function OnDelete() {
    $(tblname).on('click', '.del', function () {
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData);
        DeleteID = rowData.ID;

        if (DeleteID != "") {
            $("#modalDelete").modal();
        }

    });
}

function OnEdit() {
    $(tblname).on('click', '.edit', function () {
        var row = $(this).closest('tr');
        var rowIndex = table.row(row).index();
        var rowData = table.row(rowIndex).data();
        console.log(rowData);

        $(_ID).val(rowData.ID);
        $(_Name).val(rowData.Description);
        $(_SafetyIns).val(rowData.Safty_ins);
        $("#formModalLabel").text("Edit");
        $("#modalAdd").modal();
    });
}

function Delete(id) {
    if (id != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDelete",
            data: JSON.stringify({ dataID: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data);
                    if (data.Message == "success") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(delt);
                        $("#Validation").fadeOut(5000);
                        OnLoadGrid();
                        $("#modalDelete").modal('hide');

                    } else if (data.Message == "failed") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(failedmsg);
                        $("#Validation").fadeOut(5000);
                    } else if (data.Message == "use") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("It is used by another process, Please try again!");
                        $("#Validation").fadeOut(5000);
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}


function LoadDataTable(datasource) {
    table = $(tblname).DataTable({
        "searching": true,
        "bLengthChange": false,
        "sSort": true,
        "order": [0, 'desc'],
        "bInfo": false,
        "aaData": JSON.parse(datasource),
        "aoColumns": [

                        { data: "ID", sDefaultContent: "" },
                        { data: "Description", sDefaultContent: "" },
                        { data: "Safty_ins", sDefaultContent: "" },
                        {
                            data: null,
                            sDefaultContent: "",
                            "orderable": false,
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    //  var edit = ' <a data-toggle="modal" class="edit"><i class="md md-visibility"></i></a>';
                                    var del = ' <a href="#" class="del" title="del"><i class="fa fa-trash" ></i></a>';
                                    var edit = ' <a href="#" class="edit" title="Edit"><i class="fa fa-pencil" ></i></a>';
                                    var html = del + edit;
                                    return html;
                                }
                                return data;
                            }
                        },
        ],
        "columnDefs": [
            { "targets": [2], "searchable": false, "visible": false }
          //  { "targets": [4], "searchable": false, "visible": false }

        ]
    });
}

function OnAdd() {
    ClearFields();
    $("#formModalLabel").text("Add");
    $("#modalAdd").modal();
}


function OnErrorCall() {
    alert("Something went wrong");
}
