﻿
var serviceurl = "../../appServices/ChartOfAccountService.asmx";
var pagename = "ChartOfAccount.aspx";
var tblname = "#treeview";
var table;

var _msg = $("#txtmsg").selector;
var delt = "Record has been deleted Successfully!";
var update = "Record has been updated Successfully!";
var usemsg = "Record already is in use, could not delete this record!";
var success = "Record has been saved Successfully!";
var failedmsg = "Could not proccess your request, Please try again!";
var exist = "Sorry this record already exists , Please try any other!";


var successclass = "label label-success msgtext";
var failedclass = "label label-danger msgtext";
var infoclass = "label label-info msgtext";

var _txtAccountCode = "#txtAccountCode";
var _txtAccountName = "#txtAccountName";
var _txtDescription = "#txtDescription";
var _ddlAccounType = "#ddlAccounType";

var _txtOpeningDate = "#txtOpeningDate";
var _txtOpeningBalance = "#txtOpeningBalance";

var _txtOldAccountCode = "#txtOldAccountCode";
var _ddlLocation = "#ddlLocation";
var _ddlOldAccountCode = "#ddlOldAccountCode";


var loginUrl = "/Default.aspx";

$(document).ready(function () {
    OnLoad();
    OnChange();
    //OnDelete();
    $("#txtOpeningDate").datepicker({ autoclose: true });
    $("#txtOpeningDate").val(GetToday());
    $('#scrolling').slimscroll({ height: '400' });
    $(".select2").select2();

    $("body #chartOfAccountForm").on("focus", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            showCurrentToopTip(this)
        }
    })

    $("body #chartOfAccountForm").on("focusout", "input, select", function () {
        var service_ToolTip = $("#hdnToolTip").val()
        if (service_ToolTip == 'on') {
            hideToopTipExceptCurrent(this)
        } else {
            hideValidationMessage(this)
        }
    })

    $("body #chartOfAccountForm").on("keypress paste", "input", function (key) {
        var currentElement = $(this)
        var type = $(currentElement).attr("_fieldtype")
        if (type != "") {
            return showHideFieldValidation(key, currentElement, type, $("#hdnToolTip").val())
        }
    })

});


/////////////save


function Delete() {

    var txtAccountC = $("#txtAccountCode").val();
    var accountName = $("#txtAccountName").val();
    if (txtAccountC != "") {
        $.ajax({
            type: "POST",
            url: "" + serviceurl + "/OnDelete",
            data: JSON.stringify({ dataID: txtAccountC, dataName: accountName }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                debugger
                if (response.d == "sessionNotFound") {
                    alert("sessionNotFound");
                    window.location.href = loginUrl;
                } else {
                    var data = JSON.parse(response.d);
                    console.log(data.Message);
                    if (!data.Status) {
                        $("#validation").fadeIn(1000);
                        $("#validation").text(data.Message);
                        $("#validation").fadeOut(5000);
                        $("#modalAdd").modal('hide');
                    }
                    if (data.Message == "success") {
                        getTreeView(TID)
                        $("#validation").fadeIn(1000);
                        $("#validation").text(delt);
                        $("#validation").fadeOut(5000);

                        OnLoad();
                        $("#modalAdd").modal('hide');

                    } else if (data.Message == "failed") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text(failedmsg);
                        $("#Validation").fadeOut(5000);
                        $("#modalAdd").modal('hide');
                    } else if (data.Message == "exist") {
                        $("#Validation").fadeIn(1000);
                        $("#Validation").text("It is used by another process, Please try again!");
                        $("#Validation").fadeOut(5000);
                        $("#modalAdd").modal('hide');
                    }

                }
            },
            error: OnErrorCall
        });
    } else {
        alert("Please select a record to delete!")
    }
}


function GetToday() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var today = mm + '/' + dd + '/' + yyyy;
    return today;
}

function OnLoad() {
    //alert('2');

    $.ajax({
        type: "POST",
        url: '' + serviceurl + '/GetAccountsList',
        contentType: "application/json",
        dataType: "json",
        success: function (res) {
            $("#ddlMain").empty();
            $("#ddlMain").append("<option value='0' selected>-- Select Account Type--</option>");
            jQuery.each(res.d, function (index, item) {
                $("#ddlMain").append("<option value=" + item.Value + "> " + item.Text + " </option>");
            });
            $("#ddlMain option:selected").text("-- Select Account Type --").trigger("change")
        },
        failure: function (errMsg) {
            alert(errMsg);
        }
    });


    $('#btn-expand-all').on('click', function (e) {
        var levels = $('#select-expand-all-levels').val();
        $('#treeview').treeview('expandAll', { levels: levels, silent: $('#chk-expand-silent').is(':checked') });
    });

    $('#btn-collapse-all').on('click', function (e) {
        $('#treeview').treeview('collapseAll', { silent: $('#chk-expand-silent').is(':checked') });
    });

    $('#btnAdd').on('click', function () {
        if (editId != "") {

            clearAllMessages("chartOfAccountForm")

            var action = $("#action").val();
            var LeavesId = $("#ddlMain :selected").val();
            //  GetAccountsList
            $.ajax({
                type: "POST",
                url: '' + serviceurl + '/GetCode?action=' + action + '&editId=' + editId + '&LeavesId=' + LeavesId,
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    var data = res.d[0];
                    console.log(data);
                    if (data == "failed") {
                        //alert("1");
                        var error = "Chart of Account Level must be less then or Equal to 5";
                        $("#FormError").empty();
                        $("#FormError").append("<span class='alert alert-danger'> " + error + " </span>");
                        // 
                    } else {
                        //alert("2");
                        $(".accountcode").empty();
                        $(".accountcode").append("<option value='0' selected>-- Please Select --</option>");
                        jQuery.each(JSON.parse(res.d[1]), function (index, item) {
                            $(".accountcode").append("<option value=" + item.Value + "> " + item.Text + " </option>");
                        });
                        $(".accountcode option:selected").text("-- Please Select --").trigger("change")

                        $("#FormError").empty();
                        ClearDialogFields();
                        $("#txtAccountCode").val(data).trigger("change");
                        $("#formModalLabel").text("Add");
                        $("#modalAdd").modal();

                        //$("#modalAdd").dialog({
                        //    title: "Addkjjkj",
                        //    width: 800,
                        //});
                    }

                    //$("#formModalLabel").text("Add");
                    //$("#modalAdd").modal();


                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        } else {
            var error = "Please Select Account";
            $("#FormError").empty();
            $("#FormError").append("<span class='alert alert-danger'> " + error + " </span>");
        }
        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("Chart of account", "chartOfAccountForm")
        }
        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("Chart of account", "chartOfAccountForm", service_DeveloperMode)

        //}


    });

    $('#btnEdit').on('click', function () {
        if (editId != "") {

            clearAllMessages("chartOfAccountForm")

            var action = $("#action").val("edit");
            var action = $("#action").val();
            var LeavesId = $("#ddlMain :selected").val();
            //  GetEditDetails
            $.ajax({
                type: "POST",
                url: '' + serviceurl + '/GetEditDetails?action=' + action + '&editId=' + editId + '&LeavesId=' + LeavesId,
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    var data = res.d;
                    console.log(data);
                    if (data == "failed") {
                        //alert("1");
                        var error = "Chart of Account Level must be less then or Equal to 5";
                        $("#FormError").empty();
                        $("#FormError").append("<span class='alert alert-danger'> " + error + " </span>");
                        // 
                    } else {
                        //alert("2");
                        $(".accountcode").empty();
                        $(".accountcode").append("<option value='0' selected>-- Please Select --</option>");
                        jQuery.each(JSON.parse(data[9]), function (index, item) {
                            $(".accountcode").append("<option value=" + item.Value + "> " + item.Text + " </option>");
                        });
                        $(".accountcode option:selected").text("-- Please Select --").trigger("change")

                        $("#btnSave").text("Update");
                        $("#FormError").empty();
                        ClearDialogFields();

                        $("#txtAccountCode").val(data[0]).trigger("change");
                        $("#txtAccountName").val(data[1]).trigger("change");
                        $("#txtDescription").val(data[2]).trigger("change");
                        $("#txtOldAccountCode").val(data[3]).trigger("change");
                        $("#txtOpeningBalance").val(data[4]).trigger("change");


                        $("#ddlAccounType").val(data[5]).trigger("change");
                        $("#ddlLocation").val(data[6]).trigger("change");
                        $("#txtOpeningDate").val(data[8]).trigger("change");
                        //$("#ddlOldAccountCode").val();
                        var pastableAccount = 0;
                        var mustSubLedger = 0;

                        if ($("#chk").is(":Checked")) {
                            pastableAccount = 1;
                        } else {
                            pastableAccount = 0;
                        }

                        if ($("#ChkMustSubLedger").is(":Checked")) {
                            mustSubLedger = 1;
                        } else {
                            mustSubLedger = 0;
                        }

                        $("#formModalLabel").text("Edit");
                        $("#modalAdd").modal();
                        //$("#txtAccountCode").val(data[0]).trigger("change");
                        //$("#modalAdd").dialog({
                        //    title: "Edit",
                        //    width: 800,
                        //});
                    }

                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        } else {
            var error = "Please Select Account";
            $("#FormError").empty();
            $("#FormError").append("<span class='alert alert-danger'> " + error + " </span>");
        }

        var service_DeveloperMode = $("#hdnDevMode").val()
        if (service_DeveloperMode == 'on') {
            addFormFields("Chart of account", "chartOfAccountForm")
        }
        //var service_ToolTip = $("#hdnToolTip").val()
        //if (service_ToolTip == 'on') {

        setToolTip("Chart of account", "chartOfAccountForm", service_DeveloperMode)

        //}

    });



}


function ClearDialogFields() {
    $("#txtAccountCode").val("");
    $("#txtAccountName").val("");
    $("#txtDescription").val("");
    $("#txtOldAccountCode").val("");
    $("#txtOpeningDate").val("");
    $("#ddlAccounType").val("-1");
    $("#ddlOldAccountCode").val("-1");
    $("#ddlLocation").val("-1");
    $("#chk").prop('checked', false);
    $("#ChkMustSubLedger").prop('checked', false);
}

var TID = 0;
function OnChange() {
    $("#ddlMain").on('change', function () {
        var id = $("#ddlMain :selected").val();
        //alert(id);
        if (id != "-1") {
            TID = id;
            getTreeView(id);
        }

    });
}


function getTreeView(id) {

    $.ajax({
        type: "POST",
        url: '' + serviceurl + '/GetList?id=' + id,
        contentType: "application/json",
        dataType: "json",
        success: function (res) {
            let data = JSON.parse(res.d)
            if (!data.Status) {
                $("#validation").fadeIn(1000);
                $("#validation").text(data.Message);
                $("#validation").fadeOut(5000);
            } else {
                var options = {
                    backColor: "",
                    onhoverColor: "rgb(189, 228, 255)",
                    showBorder: false,
                    showTags: true,
                    highlightSelected: true,
                    selectedColor: '#fff',
                    selectedBackColor: "rgb(189, 228, 255)",
                    expandIcon: "fas fa-folder-plus",
                    collapseIcon: "fas fa-folder-open",
                    emptyIcon: "fa fa-folder-minus",
                    bootstrap2: false,
                    showTags: true,
                    levels: 5,  //4
                    data: data.GenericList,// res.d,
                    onNodeSelected: function (event, node) {
                        editId = "";
                        editId = node.id;
                        backColor = "rgb(152, 186, 208)"
                    },
                };
                //alert('OUT');
                $('#treeview').treeview(options);
            }
        },
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
}

function FormValidaton() {
    msg = "";
    $("#rAccountCode").addClass("hide");
    $("#rAccountName").addClass("hide");
    $("#rDescription").addClass("hide");
    $("#rOpeningDate").addClass("hide");

    if ($(_txtAccountCode).val() == "") {
        msg += "Account Code is required";
        $(rAccountCode).removeClass("hide");
    } else {
        $("#rAccountCode").addClass("hide");
    }

    if ($(_txtAccountName).val() == "") {
        msg += "Account Name is required";
        $("#rAccountName").removeClass("hide");
    } else {
        $("#rAccountName").addClass("hide");
    }

    if ($(_txtDescription).val() == "") {
        msg += "Description is required";
        $("#rDescription").removeClass("hide");
    } else {
        $("#rDescription").addClass("hide");
    }

    if ($(_txtOpeningDate).val() == "") {
        msg += "Opening Date is required";
        $("#rOpeningDate").removeClass("hide");
    } else {
        $("#rOpeningDate").addClass("hide");
    }



    if (msg == "") {
        $("#ValidationSummary").text("");
    } else {
        $("#ValidationSummary").text("Please fill required values.");
        alert("Please fill required values.");
    }
}

function OnErrorCall() {
    alert("Something went wrong");
}
