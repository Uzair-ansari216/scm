﻿Public Class VoucherModel
    Public ID As String
    Public SalesOrder As String
    Public SalesDate As DateTime
    Public SalesDate2 As String
    Public FK_CustomerID As String
    Public BillTo As String
    Public ShipTo As String
    Public OrderType As String
    Public OrderRef As String
    Public OrderRefDate As String
    Public FK_EmployeeID As String
    Public FK_LocationID As String
    Public Tax As String
    Public Charges As String
    Public Remarks As String
    Public Billing As String
    Public Assembly As String
    Public Delivery As String
    Public CompanyID As String
    Public LastUpdatedBy As String
    Public LastUpdatedOn As String
    Public ISLMR As String
    Public isSaleReturn As String
    Public Location_Tid As String
    Public LocationEntry As String
    Public isgst As String
    Public FK_Terms As String
    Public FK_CurrencyID As String
    Public ExchangeRate As String

    Public FK_StatusID As String
    Public StatusDate As String
    Public StatusRemarks As String
    Public Fname As String
    Public Status As String
    Public SalesPerson As String

    Public PartNumber As String
    Public ModelNumber As String
    Public ProductName As String
    Public Unit As String
    Public BDPrice As String
    Public DefaultDescription As String

    Public FK_SalesOrder As String
    Public FK_Item As String
    Public DefaultDesc As String
    Public CurrentDesc As String
    Public Quantity As String
    Public UnitPrice As String
    Public BD_PRICE As String
    Public DisCount_per As String

    Public PriceAfterDiscount As Integer
    Public Amount As Integer

    Public AccountName As String
    Public AccountCode As String
    Public ProjectID As String
    Public ProjectTitle As String
    Public EmployeeID As String
    Public EmployeeName As String

    Public VoucherID As String
    Public Particulars As String
    Public LocationID As String
    Public LocationName As String
    Public DepartmentID As String
    Public DepartmentName As String
    Public ProjectName As String
    Public SegmentID As String
    Public SegmentName As String
    Public Debit As String
    Public Credit As String

    Public VoucherTypeID As String
    Public VoucherDate As String
    Public DueDate As String
    Public PayToOrReceivedFrom As String
    Public Description As String
    Public VoucherNo As String
    Public SupplierID As String
    Public SupplierCompany As String
    Public CustomerID As String
    Public CustomerCompany As String
    Public ReferenceNo As String
    Public ReferenceDate As String
    Public Account As String
    Public rowID As String
End Class
