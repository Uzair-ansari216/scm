﻿Public Class ReportSetupModel
    Public ID As String
    Public Description As String
    Public ReportName As String
    Public IsActive As String
    Public ReportID As String
    Public AutoCalling As String
    Public IsSubReport As String
    Public IsSelectionFormula As String
    Public FKReportCategory As String
    Public ReportCategory As String
    Public ReportModule As String
    Public DataType As String
    Public ParameterName As String
    Public ObjectName As String
    Public Msg As String
    Public Mode As String
    Public ObjectType As String
    Public Query As String
    Public Label As String
    Public SelectionFormula As String
    Public FKReportModule As Int32
    Public ReportConnectionType As String
    Public imgfile As String
    Public ReportPath As String
    Public SessionVariable() As String
    Public StoreProcedure As String
    Public ExecutionOrder As String
    Public DefaultValue As String
End Class


Public Class ReportCredentials
    Public Server As String
    Public UserId As String
    Public Password As String
    Public Database As String
    Public lIntegratedSecurity As Boolean
End Class

Public Class ReportParameters
    Public Name As String
    Public Msg As String
End Class