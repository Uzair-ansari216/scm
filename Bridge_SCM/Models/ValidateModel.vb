﻿Public Class ValidateModel

    Public Property ID() As String
        Get
            Return m_ID
        End Get
        Set(value As String)
            m_ID = value
        End Set
    End Property
    Private m_ID As String

    Public Property Msg() As String
        Get
            Return m_msg
        End Get
        Set(value As String)
            m_msg = value
        End Set
    End Property
    Private m_msg As String


    Public Property Other() As String
        Get
            Return _other
        End Get
        Set(value As String)
            _other = value
        End Set
    End Property
    Private _other As String

End Class
