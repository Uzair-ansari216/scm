﻿Public Class DropdownModel

    Public Text As String
    Public Value As String
End Class

Public Class ReportModule
    Public Name As String
    Public ReportServer As String
    Public ReportDatabase As String
    Public ReportDbUserId As String
    Public ReportDbPassord As String
    Public ReportPath As String
    Public ReportConnectionType As String
    Public ConnectionName As String
    Public ConnectionUserId As String
    Public ConnectionPassword As String
    Public DropDown As List(Of DropdownModel)
End Class