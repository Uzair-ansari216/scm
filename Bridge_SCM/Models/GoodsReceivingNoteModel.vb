﻿Public Class GoodsReceivingNoteModel
    Public ID As String
    Public SalesOrder As String
    Public SalesDate As DateTime
    Public SalesDate2 As String
    Public FK_CustomerID As String
    Public BillTo As String
    Public ShipTo As String
    Public OrderType As String
    Public OrderRef As String
    Public OrderRefDate As String
    Public FK_EmployeeID As String
    Public FK_LocationID As String
    Public Tax As String
    Public Charges As String
    Public Remarks As String
    Public Billing As String
    Public Assembly As String
    Public Delivery As String
    Public CompanyID As String
    Public LastUpdatedBy As String
    Public LastUpdatedOn As String
    Public ISLMR As String
    Public isSaleReturn As String
    Public Location_Tid As String
    Public LocationEntry As String
    Public isgst As String
    Public FK_Terms As String
    Public FK_CurrencyID As String
    Public ExchangeRate As String

    Public FK_StatusID As String
    Public StatusDate As String
    Public StatusRemarks As String
    Public Fname As String
    Public Status As String
    Public SalesPerson As String

    Public PartNumber As String
    Public ModelNumber As String
    Public ProductName As String
    Public Unit As String
    Public BDPrice As String
    Public DefaultDescription As String

    Public FK_SalesOrder As String
    Public FK_Item As String
    Public DefaultDesc As String
    Public CurrentDesc As String
    Public Quantity As String
    Public UnitPrice As String
    Public BD_PRICE As String
    Public DisCount_per As String

    Public PriceAfterDiscount As Integer
    Public Amount As Integer

    Public Reference As String
    Public Description As String
    Public TotalQuantity As String
    Public Issued As String
    Public PK_Item As String
    Public FK_Reference As String
    Public Stock As String

    Public PK_GDN As String
    Public GDNDate As String
    Public FK_DeliverdFrom As String
    Public DeliverdType As String
    Public FK_DeliverdTo As String
    Public rowID As String

    Public FK_GDN As String
    Public ReceivedType As String
    Public FK_Refrence As String
    Public SerialNumber_1 As String
    Public OrderID As String
    Public Received As String


    Public GRNNumber As String
    Public GRNType As String
    Public GRNDate As String
    Public GRNSupplier As String
    Public GRNReceived As String
    Public PurchaseOrder As String
End Class
Public Class GRNDetailModel
    Public Reference As Int32
    Public Item As Int32
    Public Description As String
    Public Quantity As Int32
    Public Charges As Decimal
End Class

Public Class GRNResultSet
    Public GRN As GoodsReceivingNoteModel
    Public GRNDetail As List(Of GRNDetailModel)
End Class