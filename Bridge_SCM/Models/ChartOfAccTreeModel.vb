﻿Public Class ChartOfAccTreeModel

    Public AccountCode As String
    Public AccountName As String
    Public Description As String
    Public ParentAccountCode As String
    Public AccountGroupID As String
    Public OpeningBalance As String
    Public AccountTypeID As String
    Public AccountLevel As String
    Public AccountCodeOld As String
    Public LocationID As String
    Public BalanceSheetAccountHead As String
    Public ISSubLedger As String
    Public pastableAccount As String
    Public FK_BSID As String




    Public Property text() As String
        Get
            Return _text
        End Get
        Set(value As String)
            _text = value
        End Set
    End Property
    Private _text As String

    Public Property id() As String
        Get
            Return _id
        End Get
        Set(value As String)
            _id = value
        End Set
    End Property
    Private _id As String

    Public Property nodes() As List(Of ChartOfAccTreeModel)
        Get
            Return _nodes
        End Get
        Set(value As List(Of ChartOfAccTreeModel))
            _nodes = value
        End Set
    End Property
    Private _nodes As List(Of ChartOfAccTreeModel)

End Class
