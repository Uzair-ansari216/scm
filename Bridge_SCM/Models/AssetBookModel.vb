﻿Public Class AssetBookModel
    Public Id As Int32
    Public Title As String
    Public Description As String
    Public CreatedBy As Int32
    Public CreatedDate As Date
    Public IsDefault As Int16
End Class
