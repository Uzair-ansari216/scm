﻿Public Class VoucherSupportModel
    Public Id As String
    Public VoucherId As String
    Public AccountCode As String
    Public Currency As String
    Public Rate As String
    Public OrderId As String
    Public OrderQuantity As String
    Public OrderRate As String
    Public ExpenseDate As String
    Public InvoiceReceived As String
    Public ExpenseCurrency As String

End Class

Public Class VoucherSupportResultSet
    Public VoucherSupport As VoucherSupportModel
    Public CurrencyList As List(Of DropdownModel)
End Class