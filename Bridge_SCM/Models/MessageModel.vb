﻿Public Class MessageModel

    Public ID As String
    Public INVOICEID As String
    Public IssueID As String
    Public Message As String
    Public SessionStatus As String
    Public VoucherNo As Int32
    Public Status As Boolean
    Public Data As String
    Dim g_DataList
    Public Property GenericList()
        Get
            Return g_DataList
        End Get
        Set(ByVal value)
            g_DataList = value
        End Set
    End Property
    Public PurchaseOrder As PurchaseOrderModel
End Class

Public Class ResultSetModel
    Public ID As String
    Public Number As String
    Public Message As String
End Class