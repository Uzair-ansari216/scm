﻿Public Class LocationModel
    Public ID As String
    Public Location As String
    Public FK_CityID As String
    Public LocationCode As String
    Public cityName As String
    Public Title As String
    Public AccountName As String
    Public AccountCode As String
    Public Accounts_Receivable As String
    Public Sales_Credit As String
    Public Sales_Tax As String
    Public Sales_Return As String
    Public Stock As String
    Public CostofGoodSold As String
    Public Purchase As String
    Public LocalPurchase As String
    Public Discount As String
    Public Charges As String
    Public StockInTransit As String
    Public SalesReturn As String
    Public ImportCost As String
    Public ImportCostDr As String
End Class
