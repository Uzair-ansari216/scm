﻿Public Class FinancialModel
    Public ID As String
    Public Financial As String
    Public Document As String
    Public code As String
    Public ParentCode As String
    Public Heading As String
    Public Impact As String
    Public IsActivity As String
    Public IsBalanceSheetItem As String
    Public Amount As String
    Public IsShowInReport As String
    Public DebitAmount As String
    Public CreditAmount As String
    Public NetAmount As String
    Public FixedValue As String
    Public NotesCode As String
    Public AddToCode As String
    Public IsBold As Boolean
    Public RowColour As String
    Public headingList As List(Of DropdownModel)
End Class
Public Class TrialBalanceModel
    Public Heading As String
    Public AccountType As String
    Public UserID As String
    Public CompanyID As String
    Public AcountCode As String
    Public AccountName As String
    Public ODabit As String
    Public OCredit As String
    Public Debit As String
    Public Credit As String
    Public CDebit As String
    Public CCredit As String
    Public SAccount As String
    Public ParentCode As String
End Class

Public Class FinancialResultSet
    Public TrialBalanceList As List(Of TrialBalanceModel)
    Public FinancialDetailList As List(Of FinancialModel)
    Public FinancialHeadingList As List(Of DropdownModel)
End Class
