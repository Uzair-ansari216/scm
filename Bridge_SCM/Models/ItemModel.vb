﻿Public Class ItemModel
    Public ID As String
    Public FK_ItemCategory As String
    Public FK_BrandID As String
    Public FK_Unit As String
    Public PartNumber As String
    Public ModelNumber As String
    Public ProductName As String
    Public Description As String
    Public MinQuantity As String
    Public MaxQuantity As String
    Public ReOrderQuantity As String
    Public DeliveryLandTime As String
    Public SalePoint As String
    Public ExpGST As String
    Public WarrantyYear As String
    Public WarrantyNote As String
    Public isActive As String
    Public GST As String
    Public AvailableForSO As String
    Public ItemToBeRepost As String
    Public isSerialNo As String
    Public ItemCategory As String
    Public SegmentName As String
    Public Unit As String

End Class
