﻿Public Class ProjectModel
    Public ID As String
    Public ProjectTitle As String
    Public Description As String
    Public ProjectType As String
    Public ClientID As String
    Public StartDate As String
    Public EndDate As String
    Public DepartmentCode As String
    Public EstimatedDuration As String
    Public EstimatedCost As String
    Public DebtorAccountCode As String
    Public DebtorOpBal As String
    Public CreditorAccountCode As String
    Public CreditorOpBal As String
    Public Company As String
    Public Msg As String
End Class
