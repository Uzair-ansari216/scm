﻿
Public Class CurrencyExchangeRateModel
    Public Id As String
    Public Currency As String
    Public ExchangeRate As String
    Public FromDate As String
    Public ToDate As String
End Class

Public Class CurrencyExchangeRateResultSet
    Public CurrencyExchangeRate As CurrencyExchangeRateModel
    Public MaxDate As String
End Class

