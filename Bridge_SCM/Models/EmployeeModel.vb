﻿Public Class EmployeeModel
    Public ID As String
    Public FirstName As String
    Public Gender As String
    Public DOB As String
    Public Address As String
    Public Phone As String
    Public Fax As String
    Public cityID As String
    Public ContactReference1 As String
    Public ContactReference2 As String
    Public JoiningDate As String
    Public EndDate As String
    Public StatusID As String
    Public Nationality As String
    Public NIC As String
    Public CountriesTravelled As String
    Public LanguagesSpoken As String
    Public LanguagesWritten As String
    Public MaritalStatus As String
    Public PassportNo As String
    Public LocationID As String
    Public DesignationID As String
    Public DepartmentID As String
    Public CellNo As String
    Public Mode As String
End Class
