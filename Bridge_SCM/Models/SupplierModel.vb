﻿Public Class SupplierModel
    Public SupplierID As String
    Public Address As String
    Public Phone As String
    Public Fax As String
    Public cityID As String
    Public LocationID As String
    Public Company As String
    Public Contact As String
    Public ContactPhone As String
    Public Email As String
    Public AltContact As String
    Public AltPhone As String
    Public Terms As String
    Public CreditLimit As String
    Public Notes As String
    Public DebtorAccountCode As String
    Public DebtorOpBal As String
    Public CreditorAccountCode As String
    Public CreditorOpBal As String
    Public NTN As String
    Public STN As String
    Public countryID As String
    Public Mode As String
End Class
