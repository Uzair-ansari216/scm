﻿Public Class PurchaseOrderModel
    Public PurchaseOrderID As String
    Public PurchaseOrderNumber As String
    Public PurchaseOrderDate As DateTime
    Public Currency As String
    Public ExchangeRate As String
    Public FromBillTo As String
    Public FromBillToAddress As String
    Public _To As String
    Public ToAddress As String
    Public ShipTo As String
    Public ShipToAddress As String
    Public Status As String
    Public Signature As String
    Public Notes As String
    Public Comments As String
    Public SpotNumber As String
    Public Company As String
    Public LastUpdated As String
    Public PurchaseOrderReference As String
    Public BillDate As String
    Public ToName As String
    Public NUmber As String
    Public PurchaseOrder As String
    Public Reason As String
    Public ReasonDetail As String
    Public TotalAmount As String
    Public TotalPKR As String
    Public Location As String
    Public FromFinancialYearDate As DateTime
    Public ToFinancialYearDate As DateTime
End Class

Public Class PurchaseOrderProduct
    Public ID As String
    Public PartNumber As String
    Public ModelNumber As String
    Public ProductName As String
    Public ItemClass As String
    Public Unit As String
    Public DefaultDescription As String

End Class
Public Class PurchaseOrderDetail
    Public ItemId As Integer
    Public ModalNumber As String
    Public PartNumber As String
    Public Description As String
    Public Unit As String
    Public Quantity As String
    Public UnitPrice As String
    Public Amount As String
    Public PKRAmount As String
    Public OtherCost As String
    Public Remarks As String
End Class

Public Class ResultSet
    Public PurchaseOrder As PurchaseOrderModel
    Public PODetail As List(Of PurchaseOrderDetail)
End Class