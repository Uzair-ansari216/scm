﻿Public Class CommonModel
    Public MenuName As String
    Public ObjectType As String
    Public ObjectName As String
    Public Label As String
    Public ToolTip As String
    Public IsMendatory As Boolean
    Public MendatoryMessage As String
    Public MendatoryVoiceMsgPath As String
    Public TrainingVoiceMsgPath As String
    Public FieldType As String
    Public FieldTypeValidationMessage As String
    Public MaximumLength As String
    Public FieldTypeValidMessage As String
End Class

