﻿Public Class DealerModel
    Public ID As String
    Public Company As String
    Public FirstName As String
    Public Address As String
    Public DebtorAccountCode As String
    Public CreditorAccountCode As String
    Public countryName As String
    Public cityName As String
    Public CBillingName As String
    Public Location As String
    Public Phone As String
    Public Email As String
    Public Fax As String
    Public DebtorOpBal As String
    Public CreditorOpBal As String
    Public NTN As String
    Public STN As String
    Public CellNo1 As String
    Public CellNo2 As String
    Public Email1 As String
    Public Email2 As String

    Public Terms As String
    Public LastName As String
    Public FK_RateMaster As String
    Public EmployeeID As String
    Public CustomerGroupID As String
    Public CustomerTypeID As String
    Public cityID As String
    Public countryID As String
    Public CustomerLogin As String
    Public CreditLimit As String
    Public LocationID As String
End Class
