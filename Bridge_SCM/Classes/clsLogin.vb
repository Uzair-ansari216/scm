﻿Imports System.Reflection

Namespace BizSoft.Bridge_SCM

    Public Class clsLogin

        Public Function VerifyUser(ByVal mLoginName As String) As Boolean
            Dim StrQry As String = ""

            Dim ds As New DataSet()
            StrQry = "Select * from Users Where login='" & mLoginName & "'"
            Try
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    HttpContext.Current.Session("mIsPowerUsers") = Convert.ToString(ds.Tables(0).Rows(0)("isPowerUser"))
                    HttpContext.Current.Session("mCompanyID") = Convert.ToString(ds.Tables(0).Rows(0)("CompanyID"))
                    HttpContext.Current.Session("mLocationID") = Convert.ToString(ds.Tables(0).Rows(0)("LocationID"))
                    HttpContext.Current.Session("mUserID") = Convert.ToString(ds.Tables(0).Rows(0)("UserID"))
                    Return HttpContext.Current.Session("mIsPowerUsers")
                Else
                    Return False
                End If

            Catch ex As Exception

                Return False
            End Try

        End Function

        Public Function Login(ByVal mLoginName As String, ByVal mPassword As String, ByRef Success As Boolean, ByRef Message As String) As DataSet
            Dim StrQry As String = ""

            Dim ds As New DataSet()
            'StrQry = "Select * from Users Where login='" & mLoginName & "' and Password = '" & PerformCrypt(mPassword) & "'"


            StrQry = " SELECT CS.CompanyName,Users.*, getDate() as sysDate  FROM Users INNER JOIN CompanySetup CS on CS.CompanyID = Users.CompanyID  WHERE Login= '" & (mLoginName).Trim _
                    & "' AND Password = '" & PerformCrypt((mPassword).Trim) & "'" _
                    & ""


            Try
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then

                    BizSoft.Utilities.mUserId = Convert.ToString(ds.Tables(0).Rows(0)("UserID"))
                    BizSoft.Utilities.mUserName = Convert.ToString(ds.Tables(0).Rows(0)("UserName"))
                    BizSoft.Utilities.mCompanyId = Convert.ToString(ds.Tables(0).Rows(0)("CompanyID"))
                    'BizSoft.Utilities.mLocationID = Convert.ToString(ds.Tables(0).Rows(0)("LocationID"))
                    'BizSoft.Utilities.mLocationName = Convert.ToString(ds.Tables(0).Rows(0)("Location"))

                    BizSoft.Utilities.mIsPowerUsers = Convert.ToString(ds.Tables(0).Rows(0)("isPowerUser"))
                    BizSoft.Utilities.mStatusID = Convert.ToString(ds.Tables(0).Rows(0)("StatusID"))

                    BizSoft.Utilities.mCompanyName = Convert.ToString(ds.Tables(0).Rows(0)("CompanyName"))



                    Success = True
                    Message = "Welcome" & "---" & PerformCrypt((mPassword).Trim)

                Else
                    Success = False
                    'Message = "Invalid UserName or Password" & "---" & PerformCrypt((mPassword).Trim)
                    Return Nothing
                End If

            Catch ex As Exception
                Success = False
                Message = ex.Message
                Return Nothing
            End Try
            'Return ds.Tables(0).Rows(0)
            Return ds
        End Function
        Public Function GetLocationNameById(ByVal id As String) As String
            Dim StrQry As String = ""

            Dim ds As New DataSet()
            'StrQry = "Select * from Users Where login='" & mLoginName & "' and Password = '" & PerformCrypt(mPassword) & "'"


            StrQry = "select Location from Locations where LocationID = " & id


            Try
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then

                    Dim name As String = ds.Tables(0).Rows(0)("Location")

                    Return name
                End If

            Catch ex As Exception

                Return Nothing
            End Try
        End Function
        Public Function GetFY(ByVal yearID As String) As Boolean
            Dim StrQry As String = ""

            Dim ds As New DataSet()
            StrQry = "Select * from GL_FINANCIAL_YEAR Where yearID=" & yearID & ""
            Try
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then

                    BizSoft.Utilities.GL_FinancialID = Convert.ToString(ds.Tables(0).Rows(0)("yearID"))
                    BizSoft.Utilities.GL_FinancialYear = Convert.ToString(ds.Tables(0).Rows(0)("yearTitle"))
                    BizSoft.Utilities.GL_FinancialDateF = Convert.ToString(ds.Tables(0).Rows(0)("Datefrom"))
                    BizSoft.Utilities.GL_FinancialDateT = Convert.ToString(ds.Tables(0).Rows(0)("dateTo"))

                    HttpContext.Current.Session("yearID") = Convert.ToString(ds.Tables(0).Rows(0)("yearID"))
                    HttpContext.Current.Session("yearTitle") = Convert.ToString(ds.Tables(0).Rows(0)("yearTitle"))
                    HttpContext.Current.Session("Datefrom") = Convert.ToString(ds.Tables(0).Rows(0)("Datefrom"))
                    HttpContext.Current.Session("dateTo") = Convert.ToString(ds.Tables(0).Rows(0)("dateTo"))
                    Return True
                Else
                    BizSoft.Utilities.GL_FinancialID = 1
                    BizSoft.Utilities.GL_FinancialYear = "2012-2020"
                    BizSoft.Utilities.GL_FinancialDateF = "2012-07-01 00:00:00.000"
                    BizSoft.Utilities.GL_FinancialDateT = "2020-06-30 00:00:00.000"
                    Return False
                End If

            Catch ex As Exception
                Return False
            End Try

        End Function


        Public Function PerformCrypt(ByVal strText As String) As String
            'This function will perform the functionality of Encryption and Decryption as well
            Dim i As Integer
            Dim strTemp As String = ""
            For i = 1 To Len(strText)
                If Asc(Mid$(strText, i, 1)) < 128 Then
                    strTemp = Asc(Mid$(strText, i, 1)) + 128
                ElseIf Asc(Mid$(strText, i, 1)) > 128 Then
                    strTemp = Asc(Mid$(strText, i, 1)) - 128
                End If
                Mid$(strText, i, 1) = Chr(strTemp)
            Next i
            Return strText

        End Function
        Public Function GetCompany() As System.Data.DataSet
            Dim blnStatus As Boolean = 0
            Dim strMsg As String = ""

            Dim StrQry As String = ""
            StrQry = "SELECT * FROM CompanySetup ORDER BY CompanyID"
            Dim ds2 As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds2


        End Function


        Public Function GetLocation() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),LocationID) ID,LocationCode, Location as Name  from Locations  Order by Location  "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetFinancialYear() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT * FROM GL_FINANCIAL_YEAR ORDER BY dateto desc "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function IsAdminUser(UserName As String) As Boolean
            Dim blnStatus As Boolean = 0
            Dim strMsg As String = ""
            Dim IsAdmin As Boolean = VerifyUser(UserName)

            Return IsAdmin

        End Function

        Public Function GetCompany(UserName As String) As System.Data.DataSet
            Dim blnStatus As Boolean = 0
            Dim strMsg As String = ""
            Dim StrQry As String = ""
            Dim CompanyID As String
            Dim LocationID As String

            If IsAdminUser(UserName) = True Then
                StrQry = "SELECT * FROM CompanySetup ORDER BY CompanyID"
                Dim ds2 As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
                Return ds2
            Else
                CompanyID = HttpContext.Current.Session("mCompanyID")
                LocationID = HttpContext.Current.Session("mLocationID")

                StrQry = "SELECT * FROM CompanySetup where companyID = " & HttpContext.Current.Session("mCompanyID") & " ORDER BY CompanyID"
                Dim ds2 As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
                Return ds2
            End If


        End Function

        Public Function GetLocation(UserName As String) As System.Data.DataSet
            Dim StrQry As String = ""
            Dim IPowerUser = HttpContext.Current.Session("mIsPowerUsers")
            Dim CompanyID As String = HttpContext.Current.Session("mCompanyID")
            Dim UserID As String = HttpContext.Current.Session("mUserID")
            If IPowerUser = True Then
                StrQry = "SELECT LocationID as ID, Location as Name FROM Locations WHERE CompanyID = " & CompanyID
            Else
                StrQry = "SELECT LocationID as ID, Location as Name FROM Locations WHERE CompanyID = " & CompanyID & " and LocationID in (   select FK_Location  From User_Location where USERID = " & UserID & ")"

            End If

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
            'Dim CompanyID As String = ""
            'Dim StrQry As String = ""
            'Dim blnStatus As Boolean = 0
            'Dim strMsg As String = ""

            'Dim ds As DataSet

            'If IsAdminUser(UserName) = True Then
            '    StrQry = "Select * from Locations where CompanyID = " & HttpContext.Current.Session("mCompanyID") & ""
            '    ds = BizSoft.DBManager.GetDataSet(StrQry)
            '    Return ds
            'Else
            '    StrQry = "Select * from Users Inner Join Locations on Users.LocationID = Locations.LocationID  where Users.CompanyID = " & HttpContext.Current.Session("mCompanyID") & " AND Users.Login = '" & UserName & "'"
            '    ds = BizSoft.DBManager.GetDataSet(StrQry)
            '    Return ds
            'End If

        End Function
        Public Function GetLocationByCompanyId(CompanyID As String, UserID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            Dim IPowerUser = HttpContext.Current.Session("mIsPowerUsers")
            If IPowerUser = True Then
                StrQry = "SELECT LocationID as ID, Location as Name FROM Locations WHERE CompanyID = " & CompanyID
            Else
                StrQry = "SELECT LocationID as ID, Location as Name FROM Locations WHERE CompanyID = " & CompanyID & " and LocationID in (   select FK_Location  From User_Location where USERID = " & UserID & ")"

            End If

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function FillLocation() As System.Data.DataSet

            Dim StrQry As String = ""
            Dim IPowerUser = HttpContext.Current.Session("isPowerUser")
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            Dim UserID As String = HttpContext.Current.Session("UserID")
            If IPowerUser = True Then
                StrQry = "SELECT LocationID as ID, Location as Name FROM Locations WHERE CompanyID = " & CompanyID & " and LocationID in (   select FK_Location  From User_Location where USERID = " & UserID & ")"
            Else
                StrQry = "SELECT LocationID as ID, Location as Name FROM Locations WHERE CompanyID = " & CompanyID & " and LocationID in (   select FK_Location  From User_Location where USERID = " & UserID & ")"

            End If

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
        Public Function GetAllaccessbyUser(ByVal UserID As Long) As System.Data.DataSet
            Dim strSQL As String = ""
            strSQL = " Select m.ID,m.ParentID,m.MenuName,m.DT_MenuObjectName , D.RoleID,D.isAdd,D.isEdit ,IsDelete,isView,isShow " &
" From GL_Menu m left Outer Join GL_ROLE_DETAIL D on M.MenuName = D.MnueName " &
" Inner join Users U on D.RoleID = U.RoleID AND M.Appid = 1 And M.IsActive = 0 and U.UserID = " & UserID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(strSQL)
            Return ds

        End Function
        Public Function isOldPassword(ByVal currentPassword As String, ByVal newPassword As String, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As Tuple(Of String, Boolean)
            Dim StrQry As String = ""
            Dim voucherNo As String = ""
            Dim ds As New DataSet
            Try

                StrQry = "select * from Users where userid = " & HttpContext.Current.Session("UserID") & " and password = '" & PerformCrypt((currentPassword).Trim) & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    StrQry = "update Users set password = '" & PerformCrypt((newPassword).Trim) & "' where userid = " & HttpContext.Current.Session("UserID") & ""
                    ds = BizSoft.DBManager.GetDataSet(StrQry)
                    Message = "Password change successfully"
                    Success = True

                    Dim iAction As String = ""
                    iAction = "Change Password"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Change Password", "Users", HttpContext.Current.Session("UserID"), Environment.MachineName)

                Else
                    Message = "Incorrect current password"
                    Success = False
                End If

                Return Tuple.Create(CStr(Message), CBool(Success))
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Change Password", "Users", "", Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Message = ex.Message
                Return Tuple.Create(CStr(Message), CBool(Success))
            End Try
        End Function
    End Class
End Namespace
