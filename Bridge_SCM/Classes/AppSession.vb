﻿Public Class AppSession
    Public Shared Function CheckSession() As Boolean
        Dim status As Boolean = True
        If HttpContext.Current.Session.Count > 0 Then
            If IsNothing(HttpContext.Current.Session("UserID")) Then
                status = False
            End If
        Else
            status = False
        End If
        Return status
    End Function
End Class
