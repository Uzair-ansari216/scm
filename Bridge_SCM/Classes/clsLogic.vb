﻿Namespace BizSoft.Bridge_SCM
    Public Class Logic
        Public Overridable Function GetList() As DataSet
            Return New DataSet()
        End Function


        Public Overridable Function GetVisitorList() As DataSet
            Return New DataSet()
        End Function

        Public Overridable Function GetList(ByVal String1 As String) As DataSet
            Return New DataSet()
        End Function
        Public Overridable Function GetRoomReq() As DataSet
            Return New DataSet()
        End Function
        Public Overridable Function GetRoomReq(ByVal FromDate As Date, ByVal ToDate As Date) As DataSet
            Return New DataSet()
        End Function
        Public Overridable Function GetRoomReqs() As DataSet
            Return New DataSet()
        End Function

        Public Overridable Function GetRoomReq(ByVal FromDate As Date, ByVal ToDate As Date, ByVal RoomID As String, ByVal BedID As String) As DataSet
            Return New DataSet()
        End Function
        Public Overridable Function GetListNew(ByVal DBSERVER As String, Optional ByVal Criteria As String = "", Optional ByVal DateFrom As String = "", Optional ByVal DateTo As String = "") As DataSet
            Return New DataSet()
        End Function

    End Class
End Namespace