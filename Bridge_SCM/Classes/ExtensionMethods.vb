﻿Imports System.Runtime.CompilerServices
Module Extensions
    <Extension()>
    Public Function ToShortDate(ByVal d As String) As String
        If Not IsNothing(d) Then
            If d <> "" Then
                d = Convert.ToDateTime(d).ToShortDateString()
                Return d
            Else
                Return ""
            End If
        Else
            Return ""
        End If
    End Function


    Public Sub CheckSession()
        If HttpContext.Current.Session.Count > 0 Then
            If IsNothing(HttpContext.Current.Session("UserID")) Then
                HttpContext.Current.Response.Redirect("~/Default.aspx")
            End If
        Else
            HttpContext.Current.Response.Redirect("~/Default.aspx")
        End If
    End Sub


    Public Function AddFieldValue(row As DataRow,
                          fieldName As String) As String
        If Not DBNull.Value.Equals(row.Item(fieldName)) Then
            Return CStr(row.Item(fieldName))
        Else
            Return Nothing
        End If
    End Function

End Module





