﻿
Namespace BizSoft.Bridge_SCM
    Public Class AccessRight

        Public Sub New()

        End Sub

        Public Sub New(ByVal strTable As String, ByVal strObjectID As String, Optional ByVal strSQL As String = "")
            'MainTableName = strTable
            'ObjectID = strTable
            'LAST_NO_FIELD_NAME = "LAST_CURRENCY_ID"
            'NewForOverriding(strTable, strObjectID, strSQL)
        End Sub

        Public Function ShowAll(ByRef oDS As DataSet) As Boolean
            Dim strSQL As String
            Dim ds As DataSet

            strSQL = "Select id,Func_id,Object,Parent from PS_Function"
            oDS = BizSoft.DBManager.GetDataSet(strSQL)
            If oDS.Tables(0).Rows.Count > 0 Then
                ShowAll = True
            End If
        End Function
        Public Function GetParent(ByVal id As Long) As String
            Dim strSQL As String
            Dim ods As New DataSet

            strSQL = "select * from GS_Function where id = (select Parent_id from GS_Function where id=" & id & ")"
            ods = BizSoft.DBManager.GetDataSet(strSQL)
            If ods.Tables(0).Rows.Count > 0 Then
                GetParent = ods.Tables(0).Rows(0).Item("Name")
            Else
                GetParent = ""
            End If

        End Function
        Public Overloads Function GetFunctions(ByVal Parent_Code As Long, ByRef ods As DataSet)
            Dim strSQL As String
            Dim ds As DataSet

            strSQL = "select * from GS_Function where Parent_id =" & Parent_Code
            ds = BizSoft.DBManager.GetDataSet(strSQL)
            ods = ds

        End Function
        Public Overloads Function GetFunctions(ByRef ods As DataTable)
            Dim strSQL As String
            Dim ds As DataSet
            strSQL = "select * from GS_Function"
            ds = BizSoft.DBManager.GetDataSet(strSQL)
            ods = ds.Tables(0)

        End Function
        Public Function GetSchema(ByRef ods As DataSet) As Boolean

            Return False

        End Function

        Public Function Access_Save(ByRef ods As DataSet, ByVal strTable As String) As String

        End Function
        Public Function ResetRights(ByVal GroupID As Long)
            Dim strSQL As String
            Try
                strSQL = " Delete from PS_GroupRights where USERGROUP_ID=" & GroupID
                If BizSoft.DBManager.ExecuteQry(strSQL) = "" Then
                    Return True
                End If
                Return False
            Catch exp As Exception
                ' MsgBox(exp.Message)
            Finally
            End Try
        End Function

        Public Function GetAllaccess(ByVal GroupID As Long, ByRef ods As DataSet)
            Dim strSQL As String
            Dim ds As DataSet

            Try
                strSQL = " select *  from PS_GroupRights Gr inner join GS_Function FNC on FNC.ID= GR.FUNCTION_ID  where USERGROUP_ID=" & GroupID
                ds = BizSoft.DBManager.GetDataSet(strSQL)
                ods = ds
            Catch exp As Exception

            Finally
            End Try
        End Function

        Public Function GetAllaccessbyUser(ByVal UserID As Long, ByRef ods As DataSet)
            Dim strSQL As String
            Dim ds As DataSet
            Try
                'strSQL = " select *,(select Name from GS_Function FP where FP.id=FNC.Parent_id) as ParentName  from PS_GroupRights Gr inner join GS_Function FNC on FNC.ID= GR.FUNCTION_ID  where USERGROUP_ID=(select top 1 USERGROUP_ID from PS_USERGROUP where user_id=  " & UserID & ")"
                strSQL = " Select m.ID,m.ParentID,m.MenuName,m.DT_MenuObjectName , D.RoleID,D.isAdd ,IsDelete,isView " & _
                         " From GL_Menu m left Outer Join GL_ROLE_DETAIL  D on M.DT_MenuObjectName  = D.MnueName  " & _
                         " Inner join Users U on D.RoleID = U.RoleID AND M.Appid = 1 And M.IsActive = 0 and U.UserID = " & UserID & ""

                ds = BizSoft.DBManager.GetDataSet(strSQL)
                ods = ds
            Catch exp As Exception

            Finally
            End Try
        End Function
        Public Function RemoveFunction() As Integer
            Dim strSQL As String
            Dim ods As New DataSet

            Try
                strSQL = " Delete from GS_Function "
                ods = BizSoft.DBManager.GetDataSet(strSQL)
                Return ods.Tables(0).Rows(0)(0)
            Catch exp As Exception

            Finally
            End Try
            Return 0
        End Function
        Public Function AddFunction(ByVal Name As String, ByVal Parentid As Integer) As Integer
            Dim strSQL As String
            Dim ods As New DataSet

            Try
                strSQL = " insert into GS_Function (Name,Parent_ID) values  ('" & Name & "'," & Parentid & ") select SCOPE_IDENTITY() "
                ods = BizSoft.DBManager.GetDataSet(strSQL)
                Return ods.Tables(0).Rows(0)(0)
            Catch exp As Exception

            Finally
            End Try
            Return 0
        End Function


    End Class

End Namespace


