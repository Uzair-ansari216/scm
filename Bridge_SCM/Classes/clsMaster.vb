﻿Namespace BizSoft.Bridge_SCM
    Public Class Master


        Public Function GetEvents() As System.Data.DataSet
            Dim UserID As String = HttpContext.Current.Session("UserID")
            Dim StrQry As String = ""
            StrQry = "  SELECT       CAST(StartDate AS time) AS time, dbo.Users.UserID, dbo.TblEvent.Id, dbo.TblEvent.MemberID, dbo.TblEvent.StartDate, dbo.TblEvent.Description, dbo.TblEvent.EndDate, dbo.TblEvent.priority, " &
           " dbo.TblEvent.Status, dbo.TblEvent.Closing_Remarks, dbo.TblEvent.CreateBy, dbo.TblEvent.CreateDateTime" &
" FROM            dbo.TblEvent INNER JOIN" &
                        " dbo.GS_Member ON dbo.TblEvent.MemberID = dbo.GS_Member.ID INNER JOIN" &
                        " dbo.Users ON dbo.GS_Member.Email = dbo.Users.Login" &
" WHERE        (dbo.Users.UserID = " & UserID & ") AND (dbo.TblEvent.Notification = 0) order by ID desc"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetUserName() As System.Data.DataSet
            Dim UserID As String = HttpContext.Current.Session("UserID")
            Dim StrQry As String = ""
            StrQry = "select UserName from Users where UserID = " & UserID & " "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function Update(ID As String) As String
            Dim StrQry As String = ""
            Dim Message As String = ""
            Dim datee As System.DateTime = System.DateTime.Now
            StrQry = "Update TblEvent set Notification = '1',NotifyDate ='" & datee & "' where ID = '" & ID & "'"

            Dim returnValue As Integer = BizSoft.DBManager.ExecuteQry(StrQry)
            If returnValue = 1 Then
                Message = "success"
            Else
                Message = "failed"
            End If
            Return Message
        End Function
        Public Function GF_codeDiscription(ByVal tableParameter As String) As String
            Dim fParameter, gl_query
            fParameter = Split(tableParameter, "±")
            gl_query = "SELECT " & fParameter(0) & " FROM " & fParameter(1)

            If fParameter(2) <> "" Then
                gl_query = gl_query & " WHERE " & fParameter(2)
            End If
            Dim returnValue As String = BizSoft.DBManager.ExecuteSqlQuery(gl_query)
            Return returnValue
        End Function
    End Class
End Namespace
