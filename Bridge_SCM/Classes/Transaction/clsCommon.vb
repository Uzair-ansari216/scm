﻿
Public Class clsCommon

    Public Shared Function GenerateID(ByVal ColumnName As String, ByVal TableName As String) As Integer
        Dim query As String = "Select ISNULL(MAX(CAST(" & ColumnName & " as numeric(18,0))), 0) from " & TableName & " "
        Dim ID As Integer
        Dim ds As DataSet = BizSoft.DBManager.GetDataSet(query)
        If ds.Tables(0).Rows.Count > 0 Then
            ID = Convert.ToInt32(ds.Tables(0).Rows(0)(0)) + 1
        Else
            ID = 1
        End If

        Return ID
    End Function

    Public Shared Function GenerateSampleInvoice(ByVal mfrst As String, ByVal mLast As String, ByVal tableName As String, ByVal FieldName As String) As String
        Dim query As String = "Select ISNULL(MAX(CAST(" & FieldName & " as numeric(18,0))), 0) from " & tableName & " "
        Dim ID As Integer
        Dim InvoiceNumber As String
        Dim ds As DataSet = BizSoft.DBManager.GetDataSet(query)
        If ds.Tables(0).Rows.Count > 0 Then

            ID = Convert.ToInt32(ds.Tables(0).Rows(0)(0)) + 1
            InvoiceNumber = mfrst & "/" & ID & "/" & mLast
        Else
            InvoiceNumber = mfrst & "/" & "1" & "/" & mLast
        End If

        Return InvoiceNumber
    End Function


    Public Shared Sub ValidSession()
        If (HttpContext.Current.Session("LoginUserInfo") Is Nothing) Then
            HttpContext.Current.Response.Redirect("../../Default.aspx?Login=0")
        End If

    End Sub

  

End Class
