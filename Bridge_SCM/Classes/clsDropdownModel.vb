﻿Public Class clsDropdownModel

    Public Property Text() As String
        Get
            Return _text
        End Get
        Set(value As String)
            _text = value
        End Set
    End Property
    Private _text As String

    Public Property Value() As String
        Get
            Return _value

        End Get
        Set(value As String)
            _value = value
        End Set
    End Property
    Private _value As String


End Class
