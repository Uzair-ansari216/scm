﻿Namespace BizSoft.Bridge_SCM

    Public Class GeneralLedger

        Inherits Logic


        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),EmployeeID) ID, FirstName as Name  from Employees where FirstName  is not null Order by EmployeeID"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetListAccountCode(ByVal CompanyID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT convert(nvarchar(10),AccountCode  ) ID,AccountName as Name FROM vChartOfAccount where CompanyID  = " & CompanyID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
        Public Function GetListVoucherType() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select convert(nvarchar(10),VoucherTypeID)ID, VoucherName as Name  from VoucherTypes order by sorton"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function


        Public Function GetListDetailAccountList(ByVal CompanyID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select Levels as ID, Levels as Name  from Vchartofaccount where CompanyID = " & CompanyID & "   GRoup by Levels Order by Levels"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetAccountLevelList(ByVal Level As String, ByVal CompanyID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select AccountCode as ID, AccountName as Name, * from Vchartofaccount where levels = " & Level & " and CompanyID  = " & CompanyID & " Order by AccountName"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
        Public Function GetReportDetails(ByVal FrmName As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select ReportID as ID, ReportName as Name  from GS_Report_Setup where FormName =  '" & FrmName & "' AND IsActive =1"


            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
        Public Function GetListStatus() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),StatusID   ) ID, StatusName    as Name  from Status    Order by StatusName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetListSegment(ByVal CompanyID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT convert(nvarchar(10),SegmentID ) ID, SegmentName as Name FROM BusinessSegment where CompanyID  = " & CompanyID & " Order by SegmentName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
        Public Function GetListproject(ByVal CompanyID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT  convert(nvarchar(10),ProjectId ) ID,ProjectTitle as Name FROM projects where CompanyID = " & CompanyID & " ORDER BY ProjectTitle"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function Edit(ByVal RecID As Long, ByVal CompanyID As String) As System.Data.DataSet

            Dim StrQry As String = ""
            StrQry = "Select * from Locations where  DeptID = " & RecID & " and CompanyID  = " & CompanyID & " "

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
        Public Sub PopulateAccountCodes(ByVal id As String, ByRef Success As Boolean, ByRef Message As String)
            Dim StrQry As String = ""
            StrQry = "GUI_GetDetailAccountCodes" & id & ""

            Try
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then

                    Success = True
                    Message = "Employee Hoildays record modified successfully "
                Else
                    Success = False
                    Message = " Employee record Are Not Existed in This Category  "
                End If
            Catch ex As Exception
                Success = False
                Message = ex.Message
            End Try

        End Sub
        Public Sub check(ByVal cmbControlAccountList As System.Web.UI.WebControls.DropDownList, ByVal selectedvalue As String, ByVal CompanyID As String)
            Dim StrQry As String = ""
            StrQry = "SELECT convert(nvarchar(10),AccountCode  ) ID,AccountName as Name FROM vChartOfAccount where  ID = " & selectedvalue & " and CompanyID  = " & CompanyID & ""

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Dim Str1 As String = ""

            Dim col1 As String = Convert.ToString(ds.Tables(0).Rows(0)("FkTableID"))
            Dim col2 As String = Convert.ToString(ds.Tables(0).Rows(0)("description"))
            Dim table As String = Convert.ToString(ds.Tables(0).Rows(0)("Tablename"))

            If table = "tbllocation" Then
                col2 = "location"
            End If

            Str1 = "select " & col1 & ", " & col2 & " from " & table & " Order by " & col2


            Dim ds1 As DataSet = BizSoft.DBManager.GetDataSet(Str1)

            ' Employee Selection
            'cmbEmployeerec.DataTextField = col2
            'cmbEmployeerec.DataValueField = col1
            'cmbEmployeerec.DataSource = ds1.Tables(0)
            'cmbEmployeerec.DataBind()

            Dim objAccountCode As New BizSoft.Bridge_SCM.GeneralLedger
            Dim dtAccountCode As DataSet = objAccountCode.GetListAccountCode(HttpContext.Current.Session("CompanyID"))

            cmbControlAccountList.DataTextField = "Name"
            cmbControlAccountList.DataValueField = "ID"
            cmbControlAccountList.DataSource = dtAccountCode.Tables(0)
            cmbControlAccountList.DataBind()

        End Sub

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub

        'Public Function GetMyDepartments() As System.Data.DataTable

        '    Dim StrQry As String = ""
        '    StrQry = " Select * from TH_Department where  ID  in (Select DepartmentID from TD_Department where  UserID = " & BizSoft.Utilities.GetLoginUserId & ")"
        '    Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
        '    Return ds.Tables(0)

        'End Function
        'Public Function GetTypesWithAny() As System.Data.DataTable

        '    Dim StrQry As String = ""
        '    StrQry = "Select 0 as ID,'Any' as Name union  select ID,Name from Gs_Type"
        '    Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
        '    Return ds.Tables(0)

        'End Function
        'Public Function GetTypes() As System.Data.DataTable

        '    Dim StrQry As String = ""
        '    StrQry = "select ID,Name from Gs_Type"
        '    Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
        '    Return ds.Tables(0)

        'End Function
    End Class

End Namespace