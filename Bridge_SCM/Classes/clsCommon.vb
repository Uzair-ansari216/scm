﻿Namespace BizSoft.Bridge_SCM
    Public Class Common
        Inherits Logic
        Public Function SaveFormControls(controlList As List(Of CommonModel)) As String
            Dim strQry As String = ""
            For Each item As CommonModel In controlList
                strQry = "select * from GL_MenuDetail where MenuName ='" & item.MenuName & "' and Object_Name ='" & item.ObjectName & "'"
                Dim ds As DataSet = BizSoft.DBManager.GetDataSet(strQry)
                If ds.Tables(0).Rows.Count = 0 Then
                    strQry = "insert into GL_MenuDetail (MenuName,Object_Type,Object_Name,Label,isMandatory) values('" & item.MenuName & "','" & item.ObjectType & "','" & item.ObjectName & "','" & item.Label & "','" & item.IsMendatory & "')"
                    BizSoft.DBManager.ExecuteQry(strQry)
                End If
            Next
            Return "asds"
        End Function

        Public Function GetRequiredFields(formName As String) As System.Data.DataSet
            Dim strQry As String = ""
            strQry = "select * from gl_menudetail where MenuName = '" & formName & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(strQry)
            Return ds
        End Function

        Public Function GetMenuList() As System.Data.DataSet
            Dim strQry As String = ""
            strQry = "select count(ID) as ID ,MenuName from GL_MenuDetail group by MenuName "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(strQry)
            Return ds
        End Function
        Public Function GetControlsByForm(menuName As String) As System.Data.DataSet
            Dim strQry As String = ""
            strQry = "select * from GL_MenuDetail where MenuName ='" & menuName & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(strQry)
            Return ds
        End Function

        Public Function UpdateControlsContent(controlList As List(Of CommonModel), ByRef Success As Boolean, ByRef Message As String) As String
            Dim strQry As String = ""
            Dim ds As DataSet
            For Each item As CommonModel In controlList
                Dim maxLength As String
                If Val(item.MaximumLength) > 0 Then
                    maxLength = item.MaximumLength
                Else
                    maxLength = ""
                End If
                strQry = "update GL_MenuDetail set Label = '" & item.Label & "', ToolTip = '" & item.ToolTip & "', isMandatory = '" & item.IsMendatory & "', Mandatory_msg = '" & item.MendatoryMessage & "', FieldType = '" & item.FieldType & "', MaxLength = '" & maxLength & "',FieldTypeValidationMsg = '" & item.FieldTypeValidationMessage & "' where Object_Name = '" & item.ObjectName.Trim() & "' and MenuName='" & item.MenuName.Trim() & "'"
                BizSoft.DBManager.ExecuteQry(strQry)
                Dim iAction As String = ""
                iAction = "Edit Recored Form Control " & item.MenuName.Trim() & " Object " & item.ObjectName
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Form Control", "BusinessSegment", 0, Environment.MachineName)

            Next
            Success = True
            Message = "success"
            Return Message
        End Function

        Public Function HasRights(ByVal strRight As String, ByVal strAction As String, ByVal strEntity As String) As Boolean
            Dim flag As Boolean = False
            Dim dt As DataTable = CType(HttpContext.Current.Session("AccessRights"), DataTable)
            Dim dv As New DataView(dt)
            dv.RowFilter = "MenuName='" & strRight.Replace("'", "''") & "' and " & strAction & "=" & strEntity
            If dv.ToTable().Rows.Count > 0 Then
                flag = True
            Else
                flag = False
            End If
            Return flag
        End Function

        Public Function validUser(ByVal username As String, ByVal password As String) As System.Data.DataSet
            Dim login As New clsLogin
            Dim strQry As String = ""
            strQry = "select * from Users where Login ='" & username & "' and Password = '" & login.PerformCrypt((password).Trim) & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(strQry)
            Return ds
        End Function
    End Class

End Namespace
