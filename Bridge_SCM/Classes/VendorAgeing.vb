﻿Namespace BizSoft.Bridge_SCM
    Public Class VendorAgeing


        Inherits Logic

        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),EmployeeID) ID, FirstName as Name  from Employees where FirstName  is not null Order by EmployeeID"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetReportDetails() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select ReportID as ID, ReportName as Name  from GS_Report_Setup"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
        Public Function Edit(ByVal RecID As Long) As System.Data.DataSet

            Dim StrQry As String = ""
            StrQry = "Select * from Locations where  DeptID = " & RecID

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Sub check(ByVal cmbControlAccountList As System.Web.UI.WebControls.DropDownList, ByVal selectedvalue As String)
            Dim StrQry As String = ""
            StrQry = "SELECT convert(nvarchar(10),AccountCode  ) ID,AccountName as Name FROM vChartOfAccount where  ID = " & selectedvalue

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Dim Str1 As String = ""

            Dim col1 As String = Convert.ToString(ds.Tables(0).Rows(0)("FkTableID"))
            Dim col2 As String = Convert.ToString(ds.Tables(0).Rows(0)("description"))
            Dim table As String = Convert.ToString(ds.Tables(0).Rows(0)("Tablename"))

            If table = "tbllocation" Then
                col2 = "location"
            End If

            Str1 = "select " & col1 & ", " & col2 & " from " & table & " Order by " & col2


            Dim ds1 As DataSet = BizSoft.DBManager.GetDataSet(Str1)

            ' Employee Selection
            'cmbEmployeerec.DataTextField = col2
            'cmbEmployeerec.DataValueField = col1
            'cmbEmployeerec.DataSource = ds1.Tables(0)
            'cmbEmployeerec.DataBind()

            Dim objAccountCode As New BizSoft.Bridge_SCM.GeneralLedger
            Dim dtAccountCode As DataSet = objAccountCode.GetListAccountCode(HttpContext.Current.Session("CompanyID"))

            cmbControlAccountList.DataTextField = "Name"
            cmbControlAccountList.DataValueField = "ID"
            cmbControlAccountList.DataSource = dtAccountCode.Tables(0)
            cmbControlAccountList.DataBind()

        End Sub



    End Class
End Namespace

