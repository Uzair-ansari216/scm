﻿Namespace BizSoft.Bridge_SCM

    Public Class UserActivity
        Inherits Logic
        Public Function GetList(ByVal FromDate As String, ByVal ToDate As String, ByVal User As String) As System.Data.DataSet
            Dim StrQry As String = ""
            If User = "" Or User = "-1" Then
                User = 0
            End If
            StrQry = "SPUserActivity '" & FromDate & "','" & ToDate & "'," & User & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetList2() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select top 100 * from GL_TransactionLog order by transid desc"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function Edit(ByVal RecID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from GS_MainCast where  ID = " & RecID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        ' New Methods
        Public Function Update(ByVal ID As String, ByVal Description As String, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            StrQry = "Select * from GS_MainCast Where Description='" & Description & "' and ID <>  '" & ID & "'"
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "exist"
                Return Message
            End If
            StrQry = "Update GS_MainCast set Description = '" & Description & "' where ID = " & ID & ""
            Try
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Success = True
                    Dim iAction As String = ""
                    iAction = "Edit Recored Main Caste"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Main Cast", "GS_MainCast", ID, Environment.MachineName)
                    Message = "update"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function

        Public Function Delete(ByVal ID As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            Dim ds As DataSet
            StrQry = "select * from GS_Cast where FkMainCast = '" & ID & "'"
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "use"
                Return Message
            End If
            StrQry = "Delete from GS_MainCast where ID = " & ID & ""
            If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                Success = True
                Dim iAction As String = ""
                iAction = "Delete Recored Main Caste"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Main Cast", "GS_MainCast", ID, Environment.MachineName)
                Message = "success"
                Return Message
            Else
                Success = False
                Message = "failed"
                Return Message
            End If
            Return Message = ""
        End Function

        Public Function Add(ByVal ID As String, ByVal Description As String, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            StrQry = "Select * from GS_MainCast Where Description='" & Description & "'"
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "exist"
                Return Message
            End If
            StrQry = "Insert into GS_MainCast(Description) values('" & Description & "');Select @@IDENTITY"

            Try
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    NewID = ds.Tables(0).Rows(0)(0).ToString()
                    Success = True
                    Dim iAction As String = ""
                    iAction = "Add new Recored Main Caste"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Main Cast", "GS_MainCast", NewID, Environment.MachineName)
                    Message = "success"
                    Return Message
                End If
            Catch ex As Exception
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function
        Public Function GetUsers() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from Users order by UserID"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
    End Class
End Namespace
