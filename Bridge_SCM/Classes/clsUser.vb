﻿Namespace BizSoft.Bridge_SCM

    Public Class User

        Inherits Logic

        'Public Sub Add(ByVal Login As String, ByVal Password As String, ByVal UserName As String, ByVal CompanyID As String, ByVal Status As String, ByVal RoleID As String, ByVal isPowerUser As String, ByVal Fb As String, ByVal Twitter As String, ByVal Member As String, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long)
        '    Dim StrQry As String = ""

        '    Dim mPassWord As String = ""
        '    mPassWord = PerformCrypt(Password)
        '    StrQry = "Select * from Users Where Login='" & Login & "'"
        '    Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
        '    If ds.Tables(0).Rows.Count > 0 Then
        '        Success = False
        '        Message = "Sorry Login Name is already exists , Please try any other"
        '        Exit Sub
        '    End If
        '    StrQry = "Insert into Users(Login,Password,UserName ,CompanyID,StatusID,RoleID , LocationID,isPowerUser,FbPage,TwitterAccount,FkMemberID) values('" & Login & "','" & mPassWord & "','" & UserName & "'," & CompanyID & "," & Status & ", " & RoleID & ", 1 , " & isPowerUser & ",'" & Fb & "','" & Twitter & "','" & Member & "')"

        '    Try
        '        BizSoft.DBManager.GetDataSet(StrQry)
        '        Success = True
        '        Message = "User record added successfully "
        '    Catch ex As Exception
        '        Success = False
        '        Message = ex.Message
        '    End Try

        'End Sub
        Public Function PerformCrypt(ByVal strText As String) As String
            'This function will perform the functionality of Encryption and Decryption as well
            Dim i As Integer
            Dim strTemp As String = ""
            For i = 1 To Len(strText)
                If Asc(Mid$(strText, i, 1)) < 128 Then
                    strTemp = Asc(Mid$(strText, i, 1)) + 128
                ElseIf Asc(Mid$(strText, i, 1)) > 128 Then
                    strTemp = Asc(Mid$(strText, i, 1)) - 128
                End If
                Mid$(strText, i, 1) = Chr(strTemp)
            Next i
            Return strText

        End Function
        Public Sub Update(ByVal UserID As String, ByVal Login As String, ByVal Password As String, ByVal UserName As String, ByVal CompanyID As String, ByVal RoleID As String, ByVal isPowerUser As String, ByVal Employee As String, ByVal Location As String, ByVal Rest As String, ByRef Success As Boolean, ByRef Message As String)


            Dim StrQry As String = ""
            Dim mPassWord As String = ""
            mPassWord = PerformCrypt(Password)
            StrQry = "Select * from Users Where Login='" & Login & "' and UserID <>  '" & UserID & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "Sorry User is already exists , Please try any other"
                Exit Sub
            End If
            If Employee = "-1" Then
                StrQry = "Update Users set UserName = '" & UserName & "', Password = '" & mPassWord & "' ,CompanyID = " & CompanyID & " , isPowerUser = " & isPowerUser & ", RoleID = " & RoleID & ", FK_EmployeeID = NULL, LocationID = '" & Location & "',IsRetrictUser = '" & Rest & "' where UserID  = " & UserID & ""
            Else
                StrQry = "Update Users set UserName = '" & UserName & "', Password = '" & mPassWord & "' ,CompanyID = " & CompanyID & " , isPowerUser = " & isPowerUser & ", RoleID = " & RoleID & ", FK_EmployeeID = '" & Employee & "', LocationID = '" & Location & "',IsRetrictUser = '" & Rest & "' where UserID  = " & UserID & ""
            End If

            Try
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then

                    Success = True
                    Message = "User record modified successfully "
                Else
                    Success = False
                    Message = "There is some problem in saving new record "
                End If
            Catch ex As Exception
                Success = False
                Message = ex.Message
            End Try


        End Sub

        Public Sub ChangePassword(ByVal UserID As String, ByVal Password As String)


            Dim StrQry As String = ""
            Dim mPassWord As String = ""
            mPassWord = PerformCrypt(Password)

            StrQry = "Update Users set Password = '" & mPassWord & "' where UserID  = " & UserID & ""
            BizSoft.DBManager.ExecuteQry(StrQry)

        End Sub
        Public Sub ChangeGeneralPassword(ByVal Password As String)


            Dim StrQry As String = ""
            Dim mPassWord As String = ""
            mPassWord = PerformCrypt(Password)

            StrQry = "Update tblPassword set password = '" & mPassWord & "' "
            BizSoft.DBManager.ExecuteQry(StrQry)

        End Sub
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),UserID ) ID, login  as Name  from Users   Order by login"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Overrides Function GetList(ByVal locationId As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),UserID ) ID, login  as Name  from Users   Order by login"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetListEmployee() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),EmployeeID) ID, FirstName as Name  from Employees where FirstName  is not null Order by EmployeeID"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Sub Delete(ByVal CityID As String, ByVal CompanyID As String, ByRef retCode As Integer, ByRef Success As Boolean, ByRef Message As String)
        End Sub
        Public Function Edit(ByVal RecID As String) As System.Data.DataSet

            Dim StrQry As String = ""
            StrQry = "Select * from Users where  UserID = " & RecID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function



        Public Sub Add(ByVal Login As String, ByVal Password As String, ByVal UserName As String, ByVal CompanyID As String, ByVal Status As String, ByVal RoleID As String, ByVal isPowerUser As String, ByVal Employee As String, ByVal Location As String, ByVal Rest As String, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long)
            Dim StrQry As String = ""

            Dim mPassWord As String = ""
            mPassWord = PerformCrypt(Password)
            StrQry = "Select * from Users Where Login='" & Login & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "Sorry Login Name is already exists , Please try any other"
                Exit Sub
            End If

            If Employee = "-1" Then
                StrQry = "Insert into Users(Login,Password,UserName ,CompanyID,StatusID,RoleID , LocationID,isPowerUser,FK_EmployeeID,IsRetrictUser) values('" & Login & "','" & mPassWord & "','" & UserName & "'," & CompanyID & "," & Status & ", " & RoleID & ", '" & Location & "' , " & isPowerUser & ",NULL,'" & Rest & "');Select @@IDENTITY"
            Else
                StrQry = "Insert into Users(Login,Password,UserName ,CompanyID,StatusID,RoleID , LocationID,isPowerUser,FK_EmployeeID,IsRetrictUser) values('" & Login & "','" & mPassWord & "','" & UserName & "'," & CompanyID & "," & Status & ", " & RoleID & ", '" & Location & "' , " & isPowerUser & ",'" & Employee & "','" & Rest & "');Select @@IDENTITY"
            End If



            Try
                Dim ds2 As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
                If ds2.Tables(0).Rows.Count > 0 Then
                    NewID = ds2.Tables(0).Rows(0)(0).ToString()
                End If
                Success = True
                Message = "User record added successfully "
            Catch ex As Exception
                Success = False
                Message = ex.Message
            End Try

        End Sub
        
        'Public Sub Update(ByVal UserID As String, ByVal Login As String, ByVal Password As String, ByVal UserName As String, ByVal CompanyID As String, ByVal RoleID As String, ByVal isPowerUser As String, ByRef Success As Boolean, ByRef Message As String)


        '    Dim StrQry As String = ""
        '    Dim mPassWord As String = ""
        '    mPassWord = PerformCrypt(Password)
        '    StrQry = "Select * from Users Where Login='" & Login & "' and UserID <>  '" & UserID & "'"
        '    Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
        '    If ds.Tables(0).Rows.Count > 0 Then
        '        Success = False
        '        Message = "Sorry City is already exists , Please try any other"
        '        Exit Sub
        '    End If
        '    StrQry = "Update Users set UserName = '" & UserName & "', Password = '" & mPassWord & "' ,CompanyID = " & CompanyID & " , LocationID = 1,isPowerUser = " & isPowerUser & ", RoleID = " & RoleID & " where UserID  = " & UserID & ""
        '    Try
        '        If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then

        '            Success = True
        '            Message = "User record modified successfully "
        '        Else
        '            Success = False
        '            Message = "There is some problem in saving new record "
        '        End If
        '    Catch ex As Exception
        '        Success = False
        '        Message = ex.Message
        '    End Try


        'End Sub

      
      

       
      
        Public Function GetMembers() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT        dbo.GS_Member.ID, dbo.GS_Member_Image.Pic, dbo.GS_Member.Fname, dbo.GS_Member.CNICNo, dbo.GS_Member.Email, " & _
            " dbo.GS_Member.MobileNo" & _
" FROM            dbo.GS_Member LEFT OUTER JOIN" & _
                         " dbo.GS_Member_Image ON dbo.GS_Member.ID = dbo.GS_Member_Image.FKMemberID" & _
" ORDER BY dbo.GS_Member.Fname"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetEmployeeList() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "SELECT FirstName, EmployeeID FROM Employees WHERE Companyid = '" & CompanyID & "' and StatusID <> 109 ORDER BY FirstName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetLocation() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "SELECT      dbo.Locations.LocationID, dbo.Locations.Location, dbo.Locations.LocationCode , dbo.CompanySetup.CompanyName" & _
          " FROM         dbo.CompanySetup INNER JOIN dbo.Locations ON dbo.CompanySetup.CompanyID = dbo.Locations.CompanyID Where  dbo.Locations.CompanyID = '" & CompanyID & "' Order By dbo.CompanySetup.CompanyName ,dbo.Locations.LocationID"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Sub DeleteDetail(ByVal UserID As String)
            Dim StrQry As String = ""
            Dim Success As String = ""
            Dim Message As String = ""

            StrQry = "Delete from User_Location where UserID = '" & UserID & "'"

            Try
                BizSoft.DBManager.GetDataSet(StrQry)
                Success = True
                Message = "User Detail record deleted successfully "
            Catch ex As Exception
                Success = False
                Message = ex.Message
            End Try

        End Sub
        Public Sub AddDetail(ByVal UserID As String, ByVal FK_Location As String)
            Dim StrQry As String = ""
            Dim Success As String = ""
            Dim Message As String = ""

            StrQry = "Insert into User_Location(UserID,FK_Location) values('" & UserID & "','" & FK_Location & "')"

            Try
                BizSoft.DBManager.GetDataSet(StrQry)
                Success = True
                Message = "User Detail record added successfully "
            Catch ex As Exception
                Success = False
                Message = ex.Message
            End Try

        End Sub
        Public Function GetUserDetail(ByVal RecID As String) As System.Data.DataSet

            Dim StrQry As String = ""
            StrQry = "  select * from User_Location where userid = " & RecID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
    End Class

End Namespace