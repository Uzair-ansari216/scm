﻿Namespace BizSoft.Bridge_SCM
    Public Class Home
        Public Function GetList() As System.Data.DataSet
            Dim UserID As String = HttpContext.Current.Session("UserID")
            Dim StrQry As String = ""
            StrQry = "SELECT        dbo.Users.UserID, dbo.Users.Login, dbo.GS_Member.Email, dbo.Td_Task.FkMemberID, GS_Member_1.Fname, dbo.Th_Task.*" & _
" FROM            dbo.Td_Task INNER JOIN" & _
                        " dbo.Th_Task ON dbo.Td_Task.TaskID = dbo.Th_Task.ID INNER JOIN" & _
                        " dbo.GS_Member ON dbo.Td_Task.FkMemberID = dbo.GS_Member.ID INNER JOIN" & _
                        " dbo.Users ON dbo.GS_Member.Email = dbo.Users.Login INNER JOIN" & _
                        " dbo.GS_Member AS GS_Member_1 ON dbo.Th_Task.TaskBy = GS_Member_1.ID" & _
" WHERE        (dbo.Users.UserID = " & UserID & ") AND (dbo.Td_Task.FkGroupID = 0) AND (dbo.Td_Task.FkCommitteeID = 0) AND (dbo.Td_Task.ReviewBy = 0)"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetGroupList() As System.Data.DataSet
            Dim UserID As String = HttpContext.Current.Session("UserID")
            Dim StrQry As String = ""
            StrQry = "SELECT        dbo.Gs_Committee.*, dbo.Td_Task.FkGroupID,dbo.Td_Task.TaskID ,Td_Task.FkMemberID , Users.Login ,UserID ,dbo.Gs_Committee.Title + ' > ' + dbo.Th_Task.TaskDescription As TaskDescription" & _
            " FROM dbo.Gs_Committee" & _
" INNER JOIN  dbo.Td_Task ON dbo.Gs_Committee.ID = dbo.Td_Task.FkGroupID" & _
" INNER JOIN  dbo.Th_Task ON dbo.Td_Task.TaskID = dbo.Th_Task.ID" & _
" Inner Join dbo.GS_Member on dbo.GS_Member.ID = dbo.Td_Task.FkMemberID" & _
" Inner Join dbo.Users on dbo.Users.Login = dbo.GS_Member.Email" & _
    " where Users.UserID  = " & UserID & " and dbo.Td_Task.FkCommitteeID = 0 and Gs_Committee.Type = 'Group' and Td_Task.ReviewBy =  0"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetHistory(ID As String) As System.Data.DataSet
            Dim UserID As String = HttpContext.Current.Session("UserID")
            Dim StrQry As String = ""
            StrQry = "SELECT        dbo.Th_Task.TaskDescription, dbo.TaskReply.Message,dbo.TaskReply.ReplyDate, dbo.GS_Member.Fname, dbo.Td_Task.TaskID" & _
" FROM            dbo.Th_Task INNER JOIN" & _
                         " dbo.Td_Task ON dbo.Th_Task.ID = dbo.Td_Task.TaskID INNER JOIN" & _
                         " dbo.TaskReply ON dbo.Td_Task.ID = dbo.TaskReply.Fk_TD_Task INNER JOIN" & _
                         " dbo.GS_Member ON dbo.TaskReply.Fk_MemberID = dbo.GS_Member.ID" & _
                         " where dbo.Th_Task.ID = " & ID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetCommitteeList() As System.Data.DataSet
            Dim UserID As String = HttpContext.Current.Session("UserID")
            Dim StrQry As String = ""
            StrQry = "SELECT        dbo.Users.Login, dbo.Users.UserID, dbo.Th_Task.TaskDescription, dbo.Gs_Committee.*, dbo.Td_Task.*" & _
" FROM            dbo.Th_Task INNER JOIN" & _
                        " dbo.Td_Task ON dbo.Th_Task.ID = dbo.Td_Task.TaskID INNER JOIN" & _
                        " dbo.GS_Member ON dbo.GS_Member.ID = dbo.Td_Task.FkMemberID INNER JOIN" & _
                        " dbo.Users ON dbo.Users.Login = dbo.GS_Member.Email INNER JOIN" & _
                        " dbo.Gs_Committee ON dbo.Td_Task.FkCommitteeID = dbo.Gs_Committee.ID" & _
" WHERE        (dbo.Users.UserID = " & UserID & ") AND (dbo.Gs_Committee.Type = 'Committee') AND (dbo.Td_Task.FkGroupID = 0) and (Td_Task.ReviewBy =  0)"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetTaskList(ID As String) As System.Data.DataSet
            Dim UserID As String = HttpContext.Current.Session("UserID")
            Dim StrQry As String = ""
            StrQry = "SELECT        dbo.Users.UserID, dbo.Users.Login, dbo.GS_Member.Email, dbo.Td_Task.FkMemberID, GS_Member_1.Fname AS GivenBy, GS_Member_2.Fname AS FollowupBy, " & _
                         " dbo.Th_Task.*,dbo.Td_Task.ID AS TD_ID" & _
" FROM            dbo.Td_Task INNER JOIN" & _
                         " dbo.Th_Task ON dbo.Td_Task.TaskID = dbo.Th_Task.ID INNER JOIN" & _
                         " dbo.GS_Member ON dbo.Td_Task.FkMemberID = dbo.GS_Member.ID INNER JOIN" & _
                         " dbo.Users ON dbo.GS_Member.Email = dbo.Users.Login INNER JOIN" & _
                         " dbo.GS_Member AS GS_Member_1 ON dbo.Th_Task.TaskBy = GS_Member_1.ID INNER JOIN" & _
                         " dbo.GS_Member AS GS_Member_2 ON dbo.Th_Task.TaskFollowup = GS_Member_2.ID" & _
" WHERE        (dbo.Users.UserID = " & UserID & ") AND (dbo.Td_Task.FkGroupID = 0) AND (dbo.Td_Task.FkCommitteeID = 0) AND (dbo.Td_Task.TaskID = " & ID & ")"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetGroupList(ID As String) As System.Data.DataSet
            Dim UserID As String = HttpContext.Current.Session("UserID")
            Dim StrQry As String = ""
            StrQry = "SELECT        dbo.Td_Task.FkGroupID, dbo.Td_Task.TaskID, dbo.Td_Task.FkMemberID, dbo.Users.Login, dbo.Users.UserID, GS_Member_1.Fname AS GivenBy, " & _
                         " GS_Member_2.Fname AS FollowupBy, dbo.Gs_Committee.*, dbo.Th_Task.*,dbo.Td_Task.ID AS TD_ID" & _
" FROM            dbo.Gs_Committee INNER JOIN" & _
                        " dbo.Td_Task ON dbo.Gs_Committee.ID = dbo.Td_Task.FkGroupID INNER JOIN" & _
                        " dbo.Th_Task ON dbo.Td_Task.TaskID = dbo.Th_Task.ID INNER JOIN" & _
                        " dbo.GS_Member ON dbo.GS_Member.ID = dbo.Td_Task.FkMemberID INNER JOIN" & _
                        " dbo.Users ON dbo.Users.Login = dbo.GS_Member.Email INNER JOIN" & _
                        " dbo.GS_Member AS GS_Member_1 ON dbo.Th_Task.TaskBy = GS_Member_1.ID INNER JOIN" & _
                        " dbo.GS_Member AS GS_Member_2 ON dbo.Th_Task.TaskFollowup = GS_Member_2.ID" & _
" WHERE        (dbo.Users.UserID = " & UserID & ") AND (dbo.Td_Task.FkCommitteeID = 0) AND (dbo.Gs_Committee.Type = 'Group') AND (dbo.Td_Task.TaskID = " & ID & ")"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetCommitteeList(ID As String) As System.Data.DataSet
            Dim UserID As String = HttpContext.Current.Session("UserID")
            Dim StrQry As String = ""
            StrQry = "SELECT        dbo.Td_Task.TaskID, dbo.Td_Task.FkMemberID, dbo.Users.Login, dbo.Users.UserID, GS_Member_1.Fname AS GivenBy, GS_Member_2.Fname AS FollowupBy, " & _
                         " dbo.Gs_Committee.Type AS Expr10, dbo.Td_Task.FkCommitteeID, dbo.Gs_Committee.*, dbo.Th_Task.*,dbo.Td_Task.ID AS TD_ID" & _
" FROM            dbo.Th_Task INNER JOIN" & _
                        " dbo.Td_Task ON dbo.Th_Task.ID = dbo.Td_Task.TaskID INNER JOIN" & _
                        " dbo.GS_Member ON dbo.GS_Member.ID = dbo.Td_Task.FkMemberID INNER JOIN" & _
                        " dbo.Users ON dbo.Users.Login = dbo.GS_Member.Email INNER JOIN" & _
                        " dbo.GS_Member AS GS_Member_1 ON dbo.Th_Task.TaskBy = GS_Member_1.ID INNER JOIN" & _
                        " dbo.GS_Member AS GS_Member_2 ON dbo.Th_Task.TaskFollowup = GS_Member_2.ID INNER JOIN" & _
                        " dbo.Gs_Committee ON dbo.Td_Task.FkCommitteeID = dbo.Gs_Committee.ID" & _
" WHERE        (dbo.Users.UserID = " & UserID & ") AND (dbo.Gs_Committee.Type = 'Committee') AND (dbo.Td_Task.TaskID = " & ID & ") AND (dbo.Td_Task.FkGroupID = 0)"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function Add(model As HomeModel, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            If model.Reply = "" Then
                Return Message
            End If
            Dim datee As System.DateTime = System.DateTime.Now
            'StrQry = "Update Td_Task set Reply = '" & model.Reply & "', ReviewBy = '1' where TaskID = '" & model.ID & "' and FkMemberID = '" & model.FkMemberID & "' and fkGroupID = '" & model.FkGroupID & "'"
            StrQry = "insert into TaskReply(FK_TD_Task,Fk_MemberID,Message,ReplyDate) values ('" & model.ID & "','" & model.FkMemberID & "','" & model.Reply & "','" & datee & "')"
            Dim returnValue As Integer = BizSoft.DBManager.ExecuteQry(StrQry)
            If returnValue = 1 Then
                Message = "success"
            Else
                Message = "failed"
            End If
            Return Message
        End Function
        Public Function AddCommittee(model As HomeModel, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""

            If model.Reply = "" Then
                Return Message
            End If
            Dim datee As System.DateTime = System.DateTime.Now
            'StrQry = "Update Td_Task set Reply = '" & model.Reply & "', ReviewBy = '1' where TaskID = '" & model.ID & "' and FkMemberID = '" & model.FkMemberID & "' and fkCommitteeID = '" & model.FkCommitteeID & "'"
            StrQry = "insert into TaskReply(FK_TD_Task,Fk_MemberID,Message,ReplyDate) values ('" & model.ID & "','" & model.FkMemberID & "','" & model.Reply & "','" & datee & "')"

            Dim returnValue As Integer = BizSoft.DBManager.ExecuteQry(StrQry)
            If returnValue = 1 Then
                Message = "success"
            Else
                Message = "failed"
            End If
            Return Message
        End Function
        Public Function AddTask(model As HomeModel, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            'StrQry = "Update Td_Task set Reply = '" & model.Reply & "', ReviewBy = '1' where TaskID = '" & model.ID & "' and FkMemberID = '" & model.FkMemberID & "' and fkCommitteeID = '0' and fkGroupID = '0' "
            If model.Reply = "" Then
                Return Message
            End If
            Dim datee As System.DateTime = System.DateTime.Now
            StrQry = "insert into TaskReply(FK_TD_Task,Fk_MemberID,Message,ReplyDate) values ('" & model.ID & "','" & model.FkMemberID & "','" & model.Reply & "','" & datee & "')"

            Dim returnValue As Integer = BizSoft.DBManager.ExecuteQry(StrQry)
            If returnValue = 1 Then
                Message = "success"
            Else
                Message = "failed"
            End If
            Return Message
        End Function
        Public Function GetEvents() As System.Data.DataSet
            Dim UserID As String = HttpContext.Current.Session("UserID")
            Dim StrQry As String = ""
            StrQry = "  SELECT       CAST(StartDate AS time) AS time, dbo.Users.UserID, dbo.TblEvent.Id, dbo.TblEvent.MemberID, dbo.TblEvent.StartDate, dbo.TblEvent.Description, dbo.TblEvent.EndDate, dbo.TblEvent.priority, " & _
           " dbo.TblEvent.Status, dbo.TblEvent.Closing_Remarks, dbo.TblEvent.CreateBy, dbo.TblEvent.CreateDateTime" & _
" FROM            dbo.TblEvent INNER JOIN" & _
                        " dbo.GS_Member ON dbo.TblEvent.MemberID = dbo.GS_Member.ID INNER JOIN" & _
                        " dbo.Users ON dbo.GS_Member.Email = dbo.Users.Login" & _
" WHERE        (dbo.Users.UserID = " & UserID & ")"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetLocation() As System.Data.DataSet
            Dim UserID As String = HttpContext.Current.Session("UserID")
            Dim StrQry As String = ""
            StrQry = "  SELECT        dbo.Users.UserName, dbo.TblMemberLocation.*" & _
" FROM            dbo.TblMemberLocation INNER JOIN" & _
                        " dbo.Users ON dbo.TblMemberLocation.UserID = dbo.Users.UserID" & _
" WHERE        (dbo.TblMemberLocation.Date BETWEEN getdate()-15 AND getdate()) AND (dbo.TblMemberLocation.ID IN" & _
                            " (SELECT        (SELECT        MAX(ID) AS Expr1" & _
                                                         " FROM            dbo.TblMemberLocation AS TblMemberLocation_1" & _
                                                         " WHERE        (UserID = ML.UserID)) AS maxID" & _
                              " FROM            dbo.TblMemberLocation AS ML" & _
                              " GROUP BY UserID))"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function AddEvent(model As DateModel, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim NewID As Long
            Dim UserID As String = HttpContext.Current.Session("UserID")
            Dim datee As System.DateTime = System.DateTime.Now

            If model.members = "" Then

                Dim StrQry2 As String = ""
                Dim ds As DataSet
                StrQry2 = "select ID from GS_Member where Email in" & _
                " (" & _
                    " select Login from users where UserID = " & UserID & "" & _
                " )"
                ds = BizSoft.DBManager.GetDataSet(StrQry2)
                If ds.Tables(0).Rows.Count > 0 Then
                    NewID = ds.Tables(0).Rows(0)(0).ToString()
                End If
                StrQry = "Insert into tblEvent(MemberID,StartDate,Description,EndDate,priority,status,CreateBy,CreateDateTime) values('" & NewID & "','" & model.start & "','" & model.title & "','" & model.end & "','" & model.priority & "','0','" & UserID & "','" & datee & "') "

                Dim returnValue As Integer = BizSoft.DBManager.ExecuteQry(StrQry)
                If returnValue = 1 Then
                    Message = "success"
                Else
                    Message = "failed"
                End If
            Else
                StrQry = "Insert into tblEvent(MemberID,StartDate,Description,EndDate,priority,status,CreateBy,CreateDateTime) values('" & model.members & "','" & model.start & "','" & model.title & "','" & model.end & "','" & model.priority & "','0','" & UserID & "','" & datee & "') "

                Dim returnValue As Integer = BizSoft.DBManager.ExecuteQry(StrQry)
                If returnValue = 1 Then
                    Message = "success"
                Else
                    Message = "failed"
                End If
            End If


            Return Message
        End Function

        Public Function AddLocation(Longitude As String, Lat As String) As String
            Dim StrQry As String = ""
            Dim UserID As String = HttpContext.Current.Session("UserID")
            StrQry = "Insert into TblMemberLocation(Latitude,Longitude,Date,UserID) values('" & Lat & "','" & Longitude & "',GETDATE(),'" & UserID & "') "
            Dim Message As String
            Dim returnValue As Integer = BizSoft.DBManager.ExecuteQry(StrQry)
            If returnValue = 1 Then
                Message = "success"
            Else
                Message = "failed"
            End If
            Return Message
        End Function
        Public Function GetMembersList() As System.Data.DataSet
            Dim UserID As String = HttpContext.Current.Session("UserID")
            Dim StrQry As String = ""
            StrQry = " exec sp_DB_TopMembers"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetMeetings() As System.Data.DataSet
            Dim UserID As String = HttpContext.Current.Session("UserID")
            Dim StrQry As String = ""
            StrQry = "SP_DB_Meetings " & UserID & ""
            'StrQry = "SP_DB_MemberVisits 1027 "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetVisits() As System.Data.DataSet
            Dim UserID As String = HttpContext.Current.Session("UserID")
            Dim StrQry As String = ""
            StrQry = "SP_DB_MemberVisits " & UserID & ""
            'StrQry = "SP_DB_MemberVisits 1027"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetFbPage() As System.Data.DataSet
            Dim UserID As String = HttpContext.Current.Session("UserID")
            Dim StrQry As String = ""
            StrQry = "select FbPage from Users where UserID = " & UserID & " "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetTwitter() As System.Data.DataSet
            Dim UserID As String = HttpContext.Current.Session("UserID")
            Dim StrQry As String = ""
            StrQry = "select TwitterAccount from Users where UserID = " & UserID & " "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
    End Class
End Namespace
