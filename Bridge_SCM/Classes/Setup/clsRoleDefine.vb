﻿
Namespace BizSoft.Bridge_SCM

    Public Class DefineRole
        Inherits Logic
        Public Function GetAllRole() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from Gl_Role_Master order by rolename "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
        Public Function GetStatus() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT  StatusID as ID, StatusName As Name FROM Status WHERE StatusID BETWEEN 800 AND 803 ORDER BY StatusID"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetListSalesPerson(ByVal CompanyID As Integer) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT FirstName As Name, EmployeeID As ID FROM Employees WHERE  companyID = " & CompanyID & " And StatusID <> 109 AND LastName IN (Select Designation From Designations Where Category = 1) ORDER BY FirstName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Overrides Function GetList() As System.Data.DataSet

            Dim StrQry As String = ""
            StrQry = "SELECT  RoleID  AS ID ,RoleName As Name , Description  FROM GL_ROLE_MASTER "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
        Public Function GetMembers() As System.Data.DataSet

            Dim StrQry As String = ""
            StrQry = "SELECT  ID  AS ID ,Fname FROM GS_Member order by Fname "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        'Public Function FillDefaultPage() As System.Data.DataSet

        '    Dim StrQry As String = ""
        '    StrQry = "select MenuName as Name,WP_FormName as ID from [dbo].[GL_Menu] where WP_FormName is not null"
        '    Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
        '    Return ds

        'End Function

        Public Overrides Function GetList(CompanyId As String) As System.Data.DataSet

            Dim StrQry As String = ""
            StrQry = "SELECT  RoleID  AS ID ,RoleName As Name , Description  FROM GL_ROLE_MASTER where companyID = " & CompanyId & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetTerms() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = " SELECT  PK_Terms As ID, TermsNo + ' ~ ' + TermsDescription As Name from tblTerms  ORDER BY TermsNo"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetListSalesOrderType() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select ID as ID, Description as Name From tblsalesOrderType"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
        Public Function GetSalesOrderInfo(ByVal RecID As Long) As System.Data.DataSet

            Dim StrQry As String = ""
            StrQry = "Select e.* ,D.ID as DeptId , D.Description As Name ,S.ID As ID ,S.Description as Designation1 ,G.ID As gid , G.Description As GroupName , T.ID As Tid , T.Description as Emptype from TblHREmployees E Inner Join TblDepartments D on d.ID  = E.FkCurdep Inner Join tblHrdesignation  S on S.ID  = E.FkCurDesignation Inner Join TblHrEmpType   T on T.ID  = E.Type Inner Join TblHrGroup G on G.ID = E.FkCurgroup  where  EmployeeID = " & RecID

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetGDNInfo(ByVal RecID As Long) As System.Data.DataTable

            Dim StrQry As String = ""
            StrQry = " Select So.PK_SalesOrder  ,G.PK_GDN,G.GDNNO,G.GDNDate,G.FK_DeliverdFrom ,G.FK_DeliverdTo ,G.Fk_LocationId ,SO.BillTo,SO.Billing,So.Charges,So.Delivery ,So.ExchangeRate,So.FK_CurrencyID ,So.FK_CustomerID,So.FK_EmployeeID ," &
                     " So.FK_LocationID,So.FK_Terms ,So.ShipTo ,G.Remarks,I.PK_Item ,I.ModelNumber,I.partNumber,I.Description,I.FK_Unit As Unit, Gi.IssueQuantity As Quantity, SI.BD_PRICE As BDPrice ,Si.DisCount_per As Discount ,Si.UnitPrice As UnitPrice ,SI.Quantity * Si.UnitPrice As Amount , Si.Remarks " &
                     " from tblGDNItems " &
                     " GI Inner Join tblGDN G on GI.FK_GDN  = G.PK_GDN Inner Join tblSalesOrderItems SI on GI.FK_Item = SI.FK_Item AND GI.FK_Reference = Si.FK_SalesOrder Inner Join tblsalesOrder SO on Si.FK_SalesOrder = So.PK_SalesOrder Inner Join tblItems I on SI.FK_Item = I.PK_Item  " &
                     " where G.PK_GDN = " & RecID & ""

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds.Tables(0)

        End Function

        Public Function Edit(ByVal RecID As Long) As System.Data.DataSet

            Dim StrQry As String = ""
            StrQry = "Select * from GL_ROLE_MASTER where  RoleID = " & RecID
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function


        Public Function EditList(ByVal RecID As Long) As System.Data.DataTable

            Dim StrQry As String = ""
            StrQry = "Select * from GL_ROLE_MASTER where  RoleID = " & RecID
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds.Tables(0)

        End Function


        Public Function AutoGeneratedKEY(ByVal l_location As String, ByVal dtSODate As String, ByVal companyID As Integer, ByVal LocationID As Integer) As String

            'KHI-0001/06


            Dim DateF As Date
            Dim DateT As Date
            Dim tempRs As New DataSet
            Dim strSQL As String = ""
            Dim gl_query As String = ""


            strSQL = "select * from GL_FINANCIAL_YEAR where DateFrom <  '" & dtSODate & "' and DateTo > '" & dtSODate & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(strSQL)

            DateF = ds.Tables(0).Rows(0)("DateFrom")
            DateT = ds.Tables(0).Rows(0)("DateTo")
            'DateF = Format(tempRs("DateFrom").value, "dd-MMM-YYYY")
            'DateT = Format(tempRs("DateTo").value, "dd-MMM-YYYY")


            gl_query = "SELECT isNull(max(subString(SalesOrder, 12, 4)), 0) + 1 as maxNO FROM tblSalesOrder WHERE SalesDate BETWEEN '" & FormatDateTime(DateF, DateFormat.ShortDate) & "' AND '" & FormatDateTime(DateT, DateFormat.ShortDate) & " 11:59:59 PM ' and Companyid = " & companyID & " and FK_LocationID = " & LocationID & ""

            'If GL_FinancialID > 3 Then
            '    gl_query = gl_query & " and Companyid = " & GL_companyID & " and FK_LocationID = " & cboLocation.ItemData(cboLocation.ListIndex)
            'End If
            Dim rs As DataSet = BizSoft.DBManager.GetDataSet(gl_query)
            'rs.Open(gl_query, objConn)


            gl_query = " SELECT     dbo.tblCity.cityCode + '-' + dbo.Locations.LocationCode AS LocCode FROM         dbo.Locations INNER JOIN   dbo.tblCity ON dbo.Locations.FK_CityID = dbo.tblCity.cityID where Locations.LocationID =  " & LocationID & ""
            Dim DsLocCode As DataSet = BizSoft.DBManager.GetDataSet(gl_query)

            l_location = l_location & Convert.ToString(DsLocCode.Tables(0).Rows(0)("LocCode"))

            AutoGeneratedKEY = l_location & Format(rs.Tables(0).Rows(0)("maxNO"), "0000") & "/" & Year(DateF) & "-" & Right(Year(DateT), 2)
            Exit Function

            Resume Next
        End Function

        Public Function AutoGeneratedKEYGDN(ByVal l_location As String, ByVal CompanyId As String) As String
            Dim StrQry As String = ""
            Dim mKeyNumber As String = ""

            StrQry = "SELECT isNull(max(subString(GDNNO ,14,5)), 0) + 1 FROM tblgdn where Companyid = " & CompanyId & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                mKeyNumber = l_location & Format(ds.Tables(0).Rows(0)(0), "00000")
            Else
                Return "0"
            End If
            Return Convert.ToString(mKeyNumber)

        End Function

#Region "Data Base Queries"
        Public Function Getdata(ByVal StrQry As String) As DataSet

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Sub RoleDetail_Insert(ByVal RoleID As String, ByVal MnueName As String, ByVal GroupID As String, ByVal isView As String, ByVal isAdd As String, ByVal isEdit As String, ByVal isDelete As String, ByVal IsShow As String, ByVal MenuID As String)

            Dim StrQry As String = ""
            Dim StrQryDel As String = ""
            Dim ds As New DataSet


            StrQry = "Insert into Gl_Role_Detail (RoleID, MnueName ,GroupID ,isView ,isAdd ,isEdit ,isDelete,IsShow,MenuID) Values(" & RoleID & ",'" & MnueName & "'," & GroupID & "," & isView & "," & isAdd & "," & isEdit & "," & isDelete & "," & IsShow & "," & MenuID & ")"
            ds = BizSoft.DBManager.GetDataSet(StrQry)

        End Sub

        Public Function Role_Master_Insert(ByVal RoleName As String, ByVal Description As String, ByVal CompanyID As String, ByVal isAdmin As String, ByRef Success As Boolean, ByRef Message As String) As Integer
            Dim Insert As Integer
            Dim StrQry As String = ""
            Dim ds As New DataSet
            StrQry = "Insert into Gl_Role_Master(RoleName , Description,CompanyID ,isAdmin) Values('" & RoleName & "','" & Description & "'," & CompanyID & "," & isAdmin & ") Select @@IDENTITY "
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Insert = ds.Tables(0).Rows(0)(0).ToString
                Return Insert
            End If
        End Function

        Public Sub Role_Master_Update(ByVal RoleID As String, ByVal RoleName As String, ByVal Description As String, ByVal CompanyID As String, ByVal isAdmin As String)

            Dim StrQry As String = ""
            StrQry = "Update Gl_Role_Master set RoleName ='" & RoleName & "', Description='" & RoleName & "' ,CompanyID = " & CompanyID & " ,isAdmin= 1 where RoleID = " & RoleID & ""
            BizSoft.DBManager.ExecuteQry(StrQry)

        End Sub

        Public Sub Role_Detail_Delete(ByVal RoleID As String)

            Dim StrQry As String = ""
            StrQry = "Delete from Gl_Role_Detail where RoleID = " & RoleID & ""
            BizSoft.DBManager.ExecuteQry(StrQry)

        End Sub
#End Region

        Public Function GetDetailRole(ByVal FK_RoleID As Integer) As System.Data.DataSet

            Dim StrQry As String = ""
            Dim ds As New DataSet
            StrQry = " Select M.ID , M.DT_MenuObjectName, M.MenuName, GroupID, isView,isAdd, isEdit,isDelete ,D.IsShow As IsActive " &
                     " from Gl_Role_Detail D " &
                     " Inner Join GL_Menu M on D.MnueName = M.DT_MenuObjectName where D.RoleID  = " & FK_RoleID & ""
            Try
                ds = BizSoft.DBManager.GetDataSet(StrQry)
            Catch ex As Exception
            End Try
            Return ds

        End Function

        Public Function GetDetailRole() As System.Data.DataSet

            Dim StrQry As String = ""
            Dim ds As New DataSet
            StrQry = " Select ParentID,ID,MenuName , DT_MenuObjectName,0 As GroupID, 0 As isView,0 As isAdd, 0 As isEdit,0 As isDelete,IsActive   from GL_Menu where IsActive = 0 Order by Code "
            Try
                ds = BizSoft.DBManager.GetDataSet(StrQry)
            Catch ex As Exception
            End Try
            Return ds

        End Function

        Public Function GetDetailRole_New(ByVal FK_RoleID As Double) As System.Data.DataTable

            Dim StrQry As String = ""
            Dim ds As New DataSet
            'StrQry = " Select M.ID , M.DT_MenuObjectName, M.MenuName, GroupID, isView,isAdd, isEdit,isDelete ,M.IsActive,M.ParentID " & _
            '         " from Gl_Role_Detail D " & _
            '         " Inner Join GL_Menu M on D.MnueName = M.DT_MenuObjectName where D.RoleID  = " & FK_RoleID & " ORDER BY M.MenuOrder"

            StrQry = " Select M.ID , M.DT_MenuObjectName, M.MenuName, GroupID, isView,isAdd, isEdit,isDelete ,IsShow as IsActive,M.ParentID " &
                  " from Gl_Role_Detail D " &
                  " Inner Join GL_Menu M on D.MnueName = M.DT_MenuObjectName where D.RoleID  = " & FK_RoleID & " ORDER BY M.MenuOrder"

            Try
                ds = BizSoft.DBManager.GetDataSet(StrQry)
            Catch ex As Exception
            End Try
            Return ds.Tables(0)

        End Function

        Public Function GetDetailRole_New() As System.Data.DataTable

            Dim StrQry As String = ""
            Dim ds As New DataSet
            StrQry = " Select ID,MenuName , DT_MenuObjectName,0 As GroupID, 0 As isView,0 As isAdd, 0 As isEdit,0 As isDelete,IsActive   from GL_Menu where IsActive = 0 Order by Code"
            Try
                ds = BizSoft.DBManager.GetDataSet(StrQry)
            Catch ex As Exception
            End Try
            Return ds.Tables(0)

        End Function
        Public Function GetPermissions(ByVal id As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select Gl_Role_Master.RoleID,Gl_Role_Master.RoleName, Gl_Role_Detail.* from Gl_Role_Master inner join  Gl_Role_Detail" &
" on Gl_Role_Master.RoleID = Gl_Role_Detail.RoleID" &
      " where Gl_Role_Detail.RoleID = " & id & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

#Region "Data Table"


        Public Function CreateRoleDefineDatatable() As DataTable


            Dim dtDetails As New DataTable("Role_Detail")
            dtDetails.Columns.Add("RoleID", GetType(Integer))
            dtDetails.Columns.Add("MnueName", GetType(String))
            dtDetails.Columns.Add("GroupID", GetType(Integer))
            dtDetails.Columns.Add("isView", GetType(Integer))
            dtDetails.Columns.Add("isAdd", GetType(Integer))
            dtDetails.Columns.Add("isEdit", GetType(Integer))
            dtDetails.Columns.Add("isDelete", GetType(Integer))
            Return dtDetails

        End Function


        Sub AddList_Details(ByVal RoleID As Integer, ByVal MnueName As String, ByVal GroupID As Integer,
                   ByVal isView As Integer, ByVal isAdd As Integer, ByVal isEdit As Integer,
                  ByVal isDelete As Integer)
            Dim List_Details As DataTable = HttpContext.Current.Session("Role_Detail")
            Dim LdrList_Details As DataRow
            Dim Sno As Integer
            Sno = List_Details.Rows.Count + 1

            LdrList_Details = List_Details.NewRow()
            LdrList_Details("Fk_item") = RoleID
            LdrList_Details("ModelNumber") = MnueName
            LdrList_Details("partNumber") = GroupID
            LdrList_Details("Description") = isView
            LdrList_Details("SalesTax") = isAdd
            LdrList_Details("Quantity") = isEdit
            LdrList_Details("Unit") = isDelete
            List_Details.Rows.Add(LdrList_Details)
            List_Details.AcceptChanges()
            HttpContext.Current.Session("Role_Detail") = List_Details

        End Sub

#End Region


    End Class

End Namespace
