﻿Imports System.Reflection

Namespace BizSoft.Bridge_SCM
    Public Class clsAssetBook
        Dim dataSet As DataSet
        Dim queryString As String = ""
        Public Function GetAssetBooks() As DataSet
            queryString = "SELECT * FROM GS_FA_Book where company_Id = " & HttpContext.Current.Session("CompanyID") & "ORDER BY Book_Title "
            dataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return dataSet
        End Function
        Public Function GetById(Optional ByVal id As String = "0") As DataSet
            queryString = "select * from GS_FA_Book where ID = " & id & ""
            dataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return dataSet
        End Function
        Public Function Add(ByVal assetBookVm As AssetBookModel, ByRef status As Boolean, ByRef message As String, ByRef newId As Long) As String
            Try
                queryString = "Insert into GS_FA_Book(Book_Title,book_Decs,Created_By,Creation_Date,Is_Default,company_Id) values('" & assetBookVm.Title & "','" & assetBookVm.Description & "'," & HttpContext.Current.Session("UserID") & ",convert(date,getdate())," & assetBookVm.IsDefault & "," & HttpContext.Current.Session("CompanyID") & ")Select @@IDENTITY"
                dataSet = BizSoft.DBManager.GetDataSet(queryString)
                If dataSet.Tables(0).Rows.Count > 0 Then
                    newId = dataSet.Tables(0).Rows(0)(0).ToString()
                    status = True
                    message = "Asset book save successfully"
                    Dim iAction As String = ""
                    iAction = "Add new Recored Asset Book, Title = " & assetBookVm.Title
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Asset Book", "GS_FA_Book", newId, Environment.MachineName)
                    Return message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, queryString, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Asset Book", "GS_FA_Book", "", Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                status = False
                message = ex.Message
                Return message
            End Try
        End Function

        Public Function Update(ByVal assetBookVm As AssetBookModel, ByRef status As Boolean, ByRef Message As String) As String
            Try

                queryString = "Update GS_FA_Book set Book_Title = '" & assetBookVm.Title & "', book_Decs='" & assetBookVm.Description & "', Updated_By = " & HttpContext.Current.Session("UserID") & ", Updation_date = convert(date,getdate()), Is_Default = " & assetBookVm.IsDefault & " where ID = " & assetBookVm.Id & ""
                If BizSoft.DBManager.ExecuteQry(queryString) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Edit Recored Asset Book, Title = " & assetBookVm.Title
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Asset Book", "GS_FA_Book", assetBookVm.Id, Environment.MachineName)

                    status = True
                    Message = "Asset book save successfully"
                    Return Message
                Else
                    status = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, queryString, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Asset Book", "GS_FA_Book", assetBookVm.Id, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                status = False
                Message = ex.Message
                Return Message
            End Try

        End Function
        Public Function Delete(ByVal id As Int32, ByVal title As String, ByRef status As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            Dim ds As DataSet
            Try

                StrQry = "Delete from GS_FA_Book where ID = " & id & ""
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Delete Recored Asset Book Title = " & title
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Asset Book", "GS_FA_Book", id, Environment.MachineName)

                    status = True
                    Message = "Asset book delete successfully"
                    Return Message
                Else
                    status = False
                    Message = "failed"
                    Return Message
                End If

            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Asset Book", "GS_FA_Book", id, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                status = False
                Message = ex.Message
            End Try
        End Function

    End Class
End Namespace

