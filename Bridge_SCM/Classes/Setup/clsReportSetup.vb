﻿Imports System.IO
Imports Bridge_SCM.BizSoft.Bridge_SCM
Imports CrystalDecisions.CrystalReports.Engine

Namespace BizSoft.Bridge_SCM

    Public Class ReportSetup
        Inherits Logic
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT dbo.GS_Report_Setup.*, dbo.GS_Report_Category.Description,dbo.GS_Report_Module.Name" &
" FROM     dbo.GS_Report_Category INNER JOIN" &
                  " dbo.GS_Report_Setup ON dbo.GS_Report_Category.ID = dbo.GS_Report_Setup.FKReportCategory" &
                  " INNER JOIN dbo.GS_Report_Module ON dbo.GS_Report_Setup.FKReportModule = dbo.GS_Report_Module.Id" &
                  " where CompanyID = " + HttpContext.Current.Session("CompanyID")
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetSessionVariables() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from Gs_SessionVariable"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function Edit(ByVal RecID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from Gs_ComplainType where  ID = " & RecID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        ' New Methods
        Public Function Update(model As ReportSetupModel, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            StrQry = "Select * from GS_Report_Setup Where ReportName = '" & model.ReportName & "' and IsActive = '" & model.IsActive & "' and AutoCalling = '" & model.AutoCalling & "' and IsSubReport = '" & model.IsSubReport & "' and FKReportCategory = '" & model.FKReportCategory & "' and IsSelectionFormula = '" & model.IsSelectionFormula & "'  and ReportID =  '" & model.ReportID & "'"
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "exist"
                Return Message
            End If
            StrQry = "Update GS_Report_Setup set ReportName = '" & model.ReportName & "',IsActive = '" & model.IsActive & "',AutoCalling = '" & model.AutoCalling & "',IsSubReport = '" & model.IsSubReport & "',FKReportCategory = '" & model.FKReportCategory & "',FKReportModule = '" & model.FKReportModule & "',ReportConnectionType = '" & model.ReportConnectionType & "',IsSelectionFormula = '" & model.IsSelectionFormula & "' where ReportID = '" & model.ReportID & "'"
            Try
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Success = True
                    Dim iAction As String = ""
                    iAction = "Edit Recored Report Setup"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Report Setup", "GS_Report_Setup", "", Environment.MachineName)
                    Message = "update"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function

        Public Function Delete(ByVal ID As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            Dim ds As DataSet
            StrQry = "select * from GS_ReportParameter where ReportID = '" & ID & "'"
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "use"
                Return Message
            End If

            StrQry = "select * from GS_ReportDetail where ReportID = '" & ID & "'"
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "use"
                Return Message
            End If

            StrQry = "Delete from GS_Report_Setup where ReportID = '" & ID & "'"
            If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                Success = True
                Dim iAction As String = ""
                iAction = "Delete Recored Report Setup"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Report Setup", "GS_Report_Setup", "", Environment.MachineName)
                Message = "success"
                Return Message
            Else
                Success = False
                Message = "failed"
                Return Message
            End If
            Return Message = ""
        End Function

        Public Function Add(model As ReportSetupModel, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            StrQry = "Select * from GS_Report_Setup Where ReportID='" & model.ReportID & "'"
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "exist"
                Return Message
            End If
            Dim company = HttpContext.Current.Session("CompanyID")
            StrQry = "Insert into GS_Report_Setup(ReportName,FormName,UserID,IsActive,ReportID,AutoCalling,IsSubReport,FKReportCategory,IsSelectionFormula,FKReportModule,ReportConnectionType,CompanyID) values('" & model.ReportName & "','Reports'," & HttpContext.Current.Session("UserID") & ",'" & model.IsActive & "','" & model.ReportID & "','" & model.AutoCalling & "','" & model.IsSubReport & "','" & model.FKReportCategory & "','" & model.IsSelectionFormula & "'," & model.FKReportModule & ",'" & model.ReportConnectionType & "'," & company & ")Select @@IDENTITY"

            Try
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    'NewID = ds.Tables(0).Rows(0)(0).ToString
                    Dim iAction As String = ""
                    iAction = "Add new Recored Report Setup"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Report Setup", "GS_Report_Setup", "", Environment.MachineName)
                    Message = "success"
                    Return Message
                End If
            Catch ex As Exception
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function
        Public Function FillReportModule() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select Id, Name from GS_Report_Module"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)

            Return ds
        End Function
        Public Function FillReportCategory() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select ID, Description from GS_Report_Category"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)

            Return ds
        End Function
        Public Function AddDetail(model As ReportSetupModel, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim NewID As Integer
            Dim ds As DataSet
            StrQry = "Select * from GS_ReportParameter Where ReportID='" & model.ReportID & "' and ParameterName = '" & model.ParameterName & "'"
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "exist"
                Return Message
            End If
            StrQry = "Insert into GS_ReportParameter(ReportID,DataType,ParameterName,ObjectName,Msg) values('" & model.ReportID & "','" & model.DataType & "','" & model.ParameterName & "','" & model.ObjectName & "','" & model.Msg & "');Select @@IDENTITY"

            ds = BizSoft.DBManager.GetDataSet(StrQry)

            If ds.Tables(0).Rows.Count > 0 Then
                'NewID = ds.Tables(0).Rows(0)(0).ToString
                Dim iAction As String = ""
                iAction = "Add new Recored Report Parameter"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Report Setup", "GS_ReportParameter", NewID, Environment.MachineName)
                Message = "success"
            Else
                Message = "failed"
            End If

            Return Message
        End Function
        Public Function GetDetail(ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from GS_ReportParameter where ReportID = '" & ID & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetParameterByReportName(name As String) As List(Of String)
            Dim StrQry As String = ""
            StrQry = "SELECT dbo.GS_Report_Module.ReportPath FROM dbo.GS_Report_Module INNER JOIN dbo.GS_Report_Category ON dbo.GS_Report_Module.Id = dbo.GS_Report_Category.ReportModuleId INNER JOIN dbo.GS_Report_Setup ON dbo.GS_Report_Category.ID = dbo.GS_Report_Setup.FKReportCategory WHERE(dbo.GS_Report_Setup.ReportID = '" & name & "')"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Dim path = ds.Tables(0).Rows(0)("reportpath")
            Dim parameterList = New List(Of String)
            Dim reportDocument = New ReportDocument()
            Dim strPath As String
            Dim strNewPath As String

            strPath = HttpContext.Current.Server.MapPath("")
            strNewPath = strPath.Replace("\appServices", "")
            If strNewPath.IndexOf("\Forms\Ctrls") > 0 Then
                strNewPath = strNewPath.Replace("\Forms\Ctrls", "")
            Else
                strNewPath = strNewPath.Replace("\Forms\Ctrls", "")
            End If
            reportDocument.Load(strNewPath & path & "\" & name)

            If reportDocument.DataDefinition.ParameterFields.Count > 0 Then
                For Each parameter As ParameterFieldDefinition In reportDocument.DataDefinition.ParameterFields

                    If String.IsNullOrEmpty(parameter.ReportName) Then
                        parameterList.Add(parameter.ParameterFieldName)
                    End If
                Next
            End If
            Return parameterList
        End Function

        Public Function DeleteDetail(ByVal ID As String, ByVal ParameterName As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""

            StrQry = "Delete from GS_ReportParameter where ReportID = '" & ID & "' and ParameterName = '" & ParameterName & "'"
            If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                Success = True
                Dim iAction As String = ""
                iAction = "Delete Recored Report Parameter"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Report Setup", "GS_ReportParameter", "", Environment.MachineName)
                Message = "success"
                Return Message
            Else
                Success = False
                Message = "failed"
                Return Message
            End If
            Return Message
        End Function
        Public Function DeleteReportDetail(ByVal ID As String, objectName As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            Dim ds As DataSet
            StrQry = "select * from GS_ReportParameter where ReportID = '" & ID & "' and ObjectName= '" & objectName & "'"
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "use"
                Return Message
            End If

            StrQry = "Delete from GS_ReportDetail where ReportID = '" & ID & "' and objectName= '" & objectName & "'"
            If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                Success = True
                Dim iAction As String = ""
                iAction = "Delete Recored Report Detail"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Report Setup", "GS_Report_Setup", "", Environment.MachineName)
                Message = "success"
                Return Message
            Else
                Success = False
                Message = "failed"
                Return Message
            End If
            Return Message = ""
        End Function
        Public Function AddDetail2(model As ReportSetupModel, variables As String, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim NewID As Integer
            Dim ds As DataSet
            StrQry = "Select * from GS_ReportDetail Where ReportID='" & model.ReportID & "' and ObjectName = '" & model.ObjectName & "'"
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "exist"
                Return Message
            End If
            StrQry = "Insert into GS_ReportDetail(ReportID,ObjectName,ObjectType,Query,Label,SessionVariable,DefaultValue) values('" & model.ReportID & "','" & model.ObjectName & "','" & model.ObjectType & "','" & model.Query & "','" & model.Label & "','" & variables & "','" & model.DefaultValue & "');Select @@IDENTITY"

            ds = BizSoft.DBManager.GetDataSet(StrQry)

            If ds.Tables(0).Rows.Count > 0 Then
                'NewID = ds.Tables(0).Rows(0)(0).ToString
                Dim iAction As String = ""
                iAction = "Add new Recored Report Detail"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Report Setup", "GS_ReportDetail", NewID, Environment.MachineName)
                Message = "success"
            Else
                Message = "failed"
            End If

            Return Message
        End Function

        Public Function GetDetail2(ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from GS_ReportDetail where ReportID = '" & ID & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function DeleteDetail2(ByVal ID As String, ByVal ObjectName As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""

            StrQry = "Delete from GS_ReportDetail where ReportID = '" & ID & "' and ObjectName = '" & ObjectName & "'"
            If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                Success = True
                Dim iAction As String = ""
                iAction = "Delete Recored Report Detail"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Report Setup", "GS_ReportDetail", "", Environment.MachineName)
                Message = "success"
                Return Message
            Else
                Success = False
                Message = "failed"
                Return Message
            End If
            Return Message
        End Function

#Region "Selection Formula"
        Public Function GetDetailForSF(ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from GS_Report_SelectionFormula where ReportID = '" & ID & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function AddSelectionFormula(model As ReportSetupModel, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            StrQry = "Select * from GS_Report_SelectionFormula Where ReportID='" & model.ReportID & "'"
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "exist"
                Return Message
            End If
            StrQry = "Insert into GS_Report_SelectionFormula(SelectionFormula,ReportID,ObjectName) values('" & model.SelectionFormula & "', '" & model.ReportID & "','" & model.ObjectName & "')Select @@IDENTITY"

            Try
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    'NewID = ds.Tables(0).Rows(0)(0).ToString
                    Dim iAction As String = ""
                    iAction = "Add new Recored Report Selection Formula"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Report Setup", "GS_Report_Selection_Formula", "", Environment.MachineName)
                    Message = "success"
                    Return Message
                End If
            Catch ex As Exception
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function

        Public Function DeleteSelectionFormula(ByVal ID As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            Dim ds As DataSet
            StrQry = "select * from GS_ReportParameter where ReportID = '" & ID & "'"
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "use"
                Return Message
            End If

            StrQry = "select * from GS_ReportDetail where ReportID = '" & ID & "'"
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "use"
                Return Message
            End If

            StrQry = "Delete from GS_Report_SelectionFormula where ID = '" & ID & "'"
            If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                Success = True
                Dim iAction As String = ""
                iAction = "Delete Recored Report Selection Formula"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Report Setup", "GS_Report_SelectionFormula", "", Environment.MachineName)
                Message = "success"
                Return Message
            Else
                Success = False
                Message = "failed"
                Return Message
            End If
            Return Message = ""
        End Function
#End Region
        Public Function GetDetailForSP(ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from GS_Report_SP where ReportID = '" & ID & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function AddStoreProcedure(model As ReportSetupModel, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            'StrQry = "Select * from GS_Report_SelectionFormula Where ReportID='" & model.ReportID & "'"
            'ds = BizSoft.DBManager.GetDataSet(StrQry)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    Success = False
            '    Message = "exist"
            '    Return Message
            'End If
            StrQry = "Insert into GS_Report_SP(ReportID,SP_Name,Sorton) values('" & model.ReportID & "', '" & model.StoreProcedure & "','" & model.ExecutionOrder & "') Select @@IDENTITY "

            Try
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    'NewID = ds.Tables(0).Rows(0)(0).ToString
                    Dim iAction As String = ""
                    iAction = "Add new Recored Report Store Procedure"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Report Setup", "GS_Report_SP", "", Environment.MachineName)
                    Message = "success"
                    Return Message
                End If
            Catch ex As Exception
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function
        Public Function DeleteStoreProcedure(ByVal ID As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            Dim ds As DataSet


            StrQry = "Delete from GS_Report_SP where ID = '" & ID & "'"
            If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                Success = True
                Dim iAction As String = ""
                iAction = "Delete Recored Report Store Procedure"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Report Setup", "GS_Report_SP", "", Environment.MachineName)
                Message = "success"
                Return Message
            Else
                Success = False
                Message = "failed"
                Return Message
            End If
            Return Message = ""
        End Function
    End Class

End Namespace

