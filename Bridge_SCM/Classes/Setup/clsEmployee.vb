﻿Imports System.Reflection

Namespace BizSoft.Bridge_SCM

    Public Class Employee
        Inherits Logic


        Public Function GetListStatus(ByVal CompanyID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select EmployeeID As ID, FirstName As Name ,* from Employees where StatusID <> 109 and CompanyID  = " & CompanyID & " order by EmployeeID desc"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from Employees where CompanyID = " + HttpContext.Current.Session("CompanyID")
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetById(ByVal id As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from Employees where EmployeeID = '" & id & "' and CompanyID = " + HttpContext.Current.Session("CompanyID") + ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetDesignation() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT * FROM Designations ORDER BY Designation"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetDepartment() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "select * from Departments where CompanyID = " & CompanyID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetStatus() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT * FROM  STATUS WHERE StatusID BETWEEN 100 AND 149"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetCity() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select CONVERT(nvarchar(10),cityID)as ID , cityName as Name from tblCity order by cityID"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetMaritalStatus() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from GS_MaritalStatus"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetLocation() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            Dim LocationID As String = HttpContext.Current.Session("LocationId")
            Dim isPowerUser As Boolean = HttpContext.Current.Session("isPowerUser")
            If isPowerUser = True Then

                StrQry = "SELECT LocationID, Location + ' - ' + CityName LocationName  FROM VWGUI_Location_CityCountry where CompanyID =  " & CompanyID & " ORDER BY CityName, Location"
            Else

                StrQry = "SELECT LocationID, Location + ' - ' + CityName LocationName  FROM VWGUI_Location_CityCountry where Locationid = " & LocationID & " and CompanyID =  " & CompanyID & " ORDER BY CityName, Location"

                'CboLocation.Enabled = False
            End If

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function Edit(ByVal RecID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from GS_Fault_Type where  ID = " & RecID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        ' New Methods
        Public Function Update(model As EmployeeModel, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            'Dim ds As DataSet
            'StrQry = "Select * from GS_Fault_Type Where Description='" & Description & "' and ID <>  '" & ID & "'"
            'ds = BizSoft.DBManager.GetDataSet(StrQry)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    Success = False
            '    Message = "exist"
            '    Return Message
            'End If
            Try
                StrQry = "Update Employees set FirstName = '" & model.FirstName & "',Gender = '" & model.Gender & "',DOB = '" & model.DOB & "',Address = '" & model.Address & "',Phone = '" & model.Phone & "',Fax = '" & model.Fax & "',cityID = '" & model.cityID & "',ContactReference1 = '" & model.ContactReference1 & "',ContactReference2 = '" & model.ContactReference2 & "',JoiningDate = '" & model.JoiningDate & "',EndDate = '" & model.EndDate & "',StatusID = '" & model.StatusID & "',Nationality = '" & model.Nationality & "',NIC = '" & model.NIC & "',CountriesTravelled = '" & model.CountriesTravelled & "',LanguagesSpoken = '" & model.LanguagesSpoken & "',LanguagesWritten = '" & model.LanguagesWritten & "',MaritalStatus = '" & model.MaritalStatus.Trim() & "',PassportNo = '" & model.PassportNo & "',LocationID = '" & model.LocationID & "',DesignationID = '" & model.DesignationID & "',DepartmentID = '" & model.DepartmentID & "',CellNo = '" & model.CellNo & "' where EmployeeID = '" & model.ID & "'"
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Edit Recored Employee Name = " & model.FirstName
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Employee", "Employees", 0, Environment.MachineName)

                    Success = True
                    Message = "update"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Employee", "Employees", model.ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try

        End Function

        Public Function Delete(ByVal ID As String, ByVal Name As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            Try
                StrQry = "Delete from Employees where EmployeeID = '" & ID & "'"
                Dim d = BizSoft.DBManager.ExecuteQry(StrQry)
                If d > 0 Then
                    Dim iAction As String = ""
                    iAction = "Delete Recored Employee Name = " & Name
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Employee", "Employees", d, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If

            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Employee", "Employees", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
            End Try
        End Function

        Public Function Add(model As EmployeeModel, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "Select * from Employees Where EmployeeID='" & model.ID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
                Dim UserID As String = HttpContext.Current.Session("UserID")
                Dim datee As System.DateTime = System.DateTime.Now
                StrQry = "Insert into Employees(EmployeeID,FirstName,Gender,DOB,Address,Phone,Fax,cityID,ContactReference1,ContactReference2,JoiningDate,EndDate,StatusID,Nationality,NIC,CountriesTravelled,LanguagesSpoken,LanguagesWritten,MaritalStatus,PassportNo,LocationID,UserID,DateCreated,DesignationID,CompanyID,DepartmentID,CellNo) values('" & model.ID & "','" & model.FirstName & "','" & model.Gender & "','" & model.DOB & "','" & model.Address & "','" & model.Phone & "','" & model.Fax & "','" & model.cityID & "','" & model.ContactReference1 & "','" & model.ContactReference2 & "','" & model.JoiningDate & "','" & model.EndDate & "','" & model.StatusID & "','" & model.Nationality & "','" & model.NIC & "','" & model.CountriesTravelled & "','" & model.LanguagesSpoken & "','" & model.LanguagesWritten & "','" & model.MaritalStatus.Trim() & "','" & model.PassportNo & "','" & model.LocationID & "'," & UserID & ",getDate(),'" & model.DesignationID & "'," & CompanyID & ",'" & model.DepartmentID & "','" & model.CellNo & "')Select @@IDENTITY"

                Dim returnValue As Integer = BizSoft.DBManager.ExecuteQry(StrQry)
                If returnValue = 1 Then
                    Dim iAction As String = ""
                    iAction = "Add New Recored Employee Name = " & model.FirstName
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Employee", "Employees", NewID, Environment.MachineName)

                    Message = "success"
                Else
                    Message = "failed"
                End If
                Return Message
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Employee", "Employees", NewID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try

        End Function

    End Class

End Namespace
