﻿Imports System.Reflection

Namespace BizSoft.Bridge_SCM
    Public Class Department
        Inherits Logic
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT TOP (100) PERCENT CONVERT(nvarchar(10), dbo.Departments.DepartmentID) AS ID, dbo.Departments.DepartmentName AS Name, dbo.Departments.Description, dbo.Departments.DepartmentHead," &
                        " dbo.Employees.FirstName FROM dbo.Departments left outer JOIN dbo.Employees ON dbo.Departments.DepartmentHead = dbo.Employees.EmployeeID" &
" WHERE(dbo.Departments.CompanyID = " + HttpContext.Current.Session("CompanyID") + ") ORDER BY Name" '"Select convert(nvarchar(10),DepartmentID) ID, DepartmentName  as Name ,Description,DepartmentHead from Departments where CompanyID = " + HttpContext.Current.Session("CompanyID") + "  Order by DepartmentName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetById(ByVal id As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from Departments where DepartmentID = '" & id & "' and CompanyID = " + HttpContext.Current.Session("CompanyID") + ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Overrides Function GetList(companyId As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),DepartmentID) ID, DepartmentName  as Name from Departments where companyID ='" & companyId & "'  Order by DepartmentName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetList2() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT * , FirstName + '~' + EmployeeID as Emp  FROM vGetEmployees WHERE StatusID <> 109 AND CompanyID = 1 ORDER BY FirstName, LastName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        'Public Function GetList3() As System.Data.DataSet
        '    Dim StrQry As String = ""
        '    StrQry = "Select convert(nvarchar(10),DepartmentID) ID, DepartmentType from Departments  Order by DepartmentType"
        '    Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
        '    Return ds
        'End Function


        'Public Function Edit(ByVal ID As String) As System.Data.DataSet
        '    Dim StrQry As String = ""
        '    StrQry = "Select * from tblItemClass where  ID = " & ID & ""
        '    Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
        '    Return ds
        'End Function
        'New Methods

        Public Function Update(ByVal ID As String, ByVal DepartmentName As String, ByVal DepartmentHead As String, ByVal Description As String, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "Select * from Departments Where DepartmentName='" & DepartmentName & "' and DepartmentID <>  '" & ID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                If ID <> "-1" Then

                    StrQry = "Update Departments set DepartmentName = '" & DepartmentName & "', DepartmentHead = '" & DepartmentHead & "', Description = '" & Description & "' where DepartmentID = '" & ID & "'"
                Else
                    '  StrQry = "Update tblCity set cityCode = '" & cityCode & "', cityName = '" & cityName & "', countryID = '" & countryID & "' where cityID = " & ID & ""
                End If

                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Edit Recored Department Name = " & DepartmentName
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Department", "Departments", 0, Environment.MachineName)

                    Success = True
                    Message = "update"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Department", "Departments", "", Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function

        Public Function Delete(ByVal ID As String, ByVal Name As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            'Dim ds As DataSet
            'StrQry = "select * from Departments where DepartmentID = '" & ID & "'"
            'ds = BizSoft.DBManager.GetDataSet(StrQry)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    Success = False
            '    Message = "exist"
            '    Return Message
            'End If
            Try
                StrQry = "Delete from Departments where DepartmentID = '" & ID & "'"
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Delete Recored Department Name = " & Name
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Department", "Departments", 0, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
                Return Message = ""
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Department", "Departments", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
            End Try

        End Function

        Public Function Add(ByVal ID As String, ByVal DepartmentName As String, ByVal DepartmentHead As String, ByVal Description As String, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As String) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
                StrQry = "Select * from Departments Where DepartmentID = '" & ID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                If ID <> "-1" Then
                    StrQry = "Insert into Departments(DepartmentID,DepartmentName,Description,DepartmentHead,CompanyID) values('" & ID & "','" & DepartmentName & "','" & Description & "','" & DepartmentHead & "','" & CompanyID & "')Select @@IDENTITY"
                    'Else
                    '    StrQry = "Insert into GS_Resource(Description,Fk_ResourceType) values ('" & Description & "','" & Fk_ResourceType & "')Select @@IDENTITY"
                End If

                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    NewID = ds.Tables(0).Rows(0)(0).ToString()

                    Dim iAction As String = ""
                    iAction = "Add new Recored Department, Name = " & DepartmentName
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Department", "Departments", NewID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Business Segment", "BusinessSegment", "", Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function
    End Class
End Namespace