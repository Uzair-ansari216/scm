﻿Imports Bridge_SCM.BizSoft.Bridge_SCM

Namespace BizSoft.Bridge_SCM

    Public Class ReportCategory
        Inherits Logic
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),ID ) ID, Description  as Name  from GS_Report_Category   Order by Description"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function Edit(ByVal RecID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from GS_Report_Category where  ID = " & RecID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        ' New Methods
        Public Function Update(ByVal ID As String, ByVal Description As String, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            StrQry = "Select * from GS_Report_Category Where Description='" & Description & "' and ID <>  '" & ID & "'"
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "exist"
                Return Message
            End If
            StrQry = "Update GS_Report_Category set Description = '" & Description & "' where ID = " & ID & ""
            Try
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Success = True
                    Dim iAction As String = ""
                    iAction = "Edit Recored Report Category"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Report Category", "GS_Report_Category", ID, Environment.MachineName)
                    Message = "update"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function

        Public Function Delete(ByVal ID As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            Dim ds As DataSet
            StrQry = "select * from GS_Report_Setup where FKReportCategory = '" & ID & "'"
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "use"
                Return Message
            End If
            StrQry = "Delete from GS_Report_Category where ID = " & ID & ""
            If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                Success = True
                Dim iAction As String = ""
                iAction = "Delete Recored Report Category"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Report Category", "GS_Report_Category", ID, Environment.MachineName)
                Message = "success"
                Return Message
            Else
                Success = False
                Message = "failed"
                Return Message
            End If
            Return Message = ""
        End Function

        Public Function Add(ByVal ID As String, ByVal Description As String, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            StrQry = "Select * from GS_Report_Category Where Description='" & Description & "'"
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "exist"
                Return Message
            End If
            StrQry = "Insert into GS_Report_Category(Description) values('" & Description & "')Select @@IDENTITY"

            Try
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    NewID = ds.Tables(0).Rows(0)(0).ToString()
                    Dim iAction As String = ""
                    iAction = "Add new Recored Report Category"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Report Category", "GS_Report_Category", NewID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                End If
            Catch ex As Exception
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function

    End Class

End Namespace
