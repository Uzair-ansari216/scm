﻿Imports System.Reflection

Namespace BizSoft.Bridge_SCM
    Public Class clsItemClass
        Inherits Logic
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT   dbo.tblItemClass.*, dbo.tblClassGroup.ClassGroup" &
                     " FROM   dbo.tblClassGroup " &
                     "INNER JOIN dbo.tblItemClass ON dbo.tblClassGroup.PK_ClassGroup = dbo.tblItemClass.FK_ClassGroup where dbo.tblItemClass.CompanyID = " + HttpContext.Current.Session("CompanyID")
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetGroupList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from tblClassGroup"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetItemClass() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT PK_ItemClass, ItemClass FROM INVGUI_GroupWithClass WHERE CompanyID = 1 Group by PK_ItemClass , ItemClass"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        'Public Function Edit(ByVal ID As String) As System.Data.DataSet
        '    Dim StrQry As String = ""
        '    StrQry = "Select * from tblItemClass where  ID = " & ID & ""
        '    Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
        '    Return ds
        'End Function
        'New Methods
        Public Function Update(ByVal ID As String, ByVal ItemClass As String, ByVal Description As String, ByVal Fk_ClassGroup As String, ByVal Active As String, ByVal Invoice As String, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "Select * from tblItemClass Where Description='" & Description & "' and PK_ItemClass <>  '" & ID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                If ID <> "-1" Then

                    StrQry = "Update tblItemClass set Fk_ClassGroup = " & Fk_ClassGroup & " ,ItemClass = '" & ItemClass & "', Description = '" & Description & "',isActive = '" & Active & "' ,isInvoice = '" & Invoice & "' where PK_ItemClass = " & ID & ""
                Else
                    StrQry = "Update tblItemClass set Fk_ClassGroup = " & Fk_ClassGroup & " ,ItemClass = '" & ItemClass & "', Description = '" & Description & "',isActive = '" & Active & "' ,isInvoice = '" & Invoice & "' where PK_ItemClass = " & ID & ""
                End If

                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Edit Recored Item Class"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Item Class", "tblItemClass", ID, Environment.MachineName)

                    Success = True
                    Message = "update"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Item Class", "tblItemClass", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function

        Public Function Delete(ByVal ID As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "select * from tblItemCategory where FK_ItemClass = '" & ID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If

                StrQry = "Delete from tblItemClass where PK_ItemClass = " & ID & ""
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Delete Recored Item Class"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Item Class", "tblItemClass", ID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If

            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Item Class", "tblItemClass", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
            End Try

        End Function

        Public Function Add(ByVal ID As String, ByVal ItemClass As String, ByVal Description As String, ByVal Fk_ClassGroup As String, ByVal Active As String, ByVal Invoice As String, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            Try
                StrQry = "Select * from tblItemClass Where Description = '" & Description & "' AND FK_ClassGroup = '" & Fk_ClassGroup & "' "
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                If ID <> "-1" Then
                    StrQry = "Insert into tblItemClass(ItemClass,Description,FK_ClassGroup,isActive,isInvoice,CompanyID) values('" & ItemClass & "','" & Description & "','" & Fk_ClassGroup & "','" & Active & "','" & Invoice & "','" & CompanyID & "')Select @@IDENTITY"
                    'Else
                    '    StrQry = "Insert into GS_Resource(Description,Fk_ResourceType) values ('" & Description & "','" & Fk_ResourceType & "')Select @@IDENTITY"
                End If

                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    NewID = ds.Tables(0).Rows(0)(0).ToString()
                    Dim iAction As String = ""
                    iAction = "Add New Recored Item Class"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Item Class", "tblItemClass", NewID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Item Class", "tblItemClass", NewID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function

    End Class
End Namespace