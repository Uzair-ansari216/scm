﻿Namespace BizSoft.DBManager
    Public Class clsReportBuilder
        Public Function GetConnection(ByRef strServer As String, ByRef strUserID As String, ByRef strPassword As String, ByRef strDatabase As String, ByRef lIntegratedSecurity As Boolean) As ReportCredentials 'previously returning boolean
            Return BizSoft.DBManager.GetConnectionForReports(strServer, strUserID, strPassword, strDatabase, lIntegratedSecurity)
        End Function
        Public Class oReport
            Public strFileName As String
            Public ht As Hashtable
            Public StrList As Generic.List(Of String)
            Public strQueryString As Boolean = False
            Public strCompanyName As String
            Public strSqlString As String
            Public rptTitle As String
            Public rptCreteria As String
            Public rptIsSubLoad As Boolean = False
            Public IsDisplayToolbar As Boolean = True
            Public lShowTitle As Boolean = True
            Public rptProduct As String
            Public mainTable As DataTable
            Public rptSubReportCollection As New Collection
            Public Formula As String
        End Class
    End Class
End Namespace