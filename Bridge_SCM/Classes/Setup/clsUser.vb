﻿Namespace BizSoft.Bridge_SCM

    Public Class User

        Inherits Logic

        Public Sub Add(ByVal Login As String, ByVal Password As String, ByVal UserName As String, ByVal CompanyID As String, ByVal Status As String, ByVal RoleID As String, ByVal isPowerUser As String, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long)
            Dim StrQry As String = ""

            Dim mPassWord As String = ""
            mPassWord = PerformCrypt(Password)
            StrQry = "Select * from Users Where Login='" & Login & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "Sorry Login Name is already exists , Please try any other"
                Exit Sub
            End If
            StrQry = "Insert into Users(Login,Password,UserName ,CompanyID,StatusID,RoleID , LocationID,isPowerUser) values('" & Login & "','" & mPassWord & "','" & UserName & "'," & CompanyID & "," & Status & ", " & RoleID & ", 1 , " & isPowerUser & ")"

            Try
                BizSoft.DBManager.GetDataSet(StrQry)
                Success = True
                Message = "User record added successfully "
            Catch ex As Exception
                Success = False
                Message = ex.Message
            End Try

        End Sub
        Public Function PerformCrypt(ByVal strText As String) As String
            'This function will perform the functionality of Encryption and Decryption as well
            Dim i As Integer
            Dim strTemp As String = ""
            For i = 1 To Len(strText)
                If Asc(Mid$(strText, i, 1)) < 128 Then
                    strTemp = Asc(Mid$(strText, i, 1)) + 128
                ElseIf Asc(Mid$(strText, i, 1)) > 128 Then
                    strTemp = Asc(Mid$(strText, i, 1)) - 128
                End If
                Mid$(strText, i, 1) = Chr(strTemp)
            Next i
            Return strText

        End Function
        Public Sub Update(ByVal UserID As String, ByVal Login As String, ByVal Password As String, ByVal UserName As String, ByVal CompanyID As String, ByVal RoleID As String, ByVal isPowerUser As String, ByRef Success As Boolean, ByRef Message As String)


            Dim StrQry As String = ""
            Dim mPassWord As String = ""
            mPassWord = PerformCrypt(Password)
            StrQry = "Select * from Users Where Login='" & Login & "' and UserID <>  '" & UserID & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "Sorry City is already exists , Please try any other"
                Exit Sub
            End If
            StrQry = "Update Users set UserName = '" & UserName & "', Password = '" & mPassWord & "' ,CompanyID = " & CompanyID & " , LocationID = 1,isPowerUser = " & isPowerUser & ", RoleID = " & RoleID & " where UserID  = " & UserID & ""
            Try
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then

                    Success = True
                    Message = "User record modified successfully "
                Else
                    Success = False
                    Message = "There is some problem in saving new record "
                End If
            Catch ex As Exception
                Success = False
                Message = ex.Message
            End Try


        End Sub

        Public Sub ChangePassword(ByVal UserID As String, ByVal Password As String)


            Dim StrQry As String = ""
            Dim mPassWord As String = ""
            mPassWord = PerformCrypt(Password)

            StrQry = "Update Users set Password = '" & mPassWord & "' where UserID  = " & UserID & ""
            BizSoft.DBManager.ExecuteQry(StrQry)

        End Sub
        Public Sub ChangeGeneralPassword(ByVal Password As String)


            Dim StrQry As String = ""
            Dim mPassWord As String = ""
            mPassWord = PerformCrypt(Password)

            StrQry = "Update tblPassword set password = '" & mPassWord & "' "
            BizSoft.DBManager.ExecuteQry(StrQry)

        End Sub
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),UserID ) ID, login  as Name  from Users   Order by login"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Overrides Function GetList(ByVal locationId As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),UserID ) ID, login  as Name  from Users   Order by login"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetListEmployee() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),EmployeeID) ID, FirstName as Name  from Employees where FirstName  is not null Order by EmployeeID"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Sub Delete(ByVal CityID As String, ByVal CompanyID As String, ByRef retCode As Integer, ByRef Success As Boolean, ByRef Message As String)
        End Sub
        Public Function Edit(ByVal RecID As String) As System.Data.DataSet

            Dim StrQry As String = ""
            StrQry = "SELECT * FROM     dbo.Users where  UserID = " & RecID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
        Public Function GetMembers() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT        dbo.GS_Member.ID, dbo.GS_Member_Image.Pic, dbo.GS_Member.Fname, dbo.GS_Member.CNICNo, dbo.GS_Member.Email, " & _
            " dbo.GS_Member.MobileNo" & _
" FROM            dbo.GS_Member LEFT OUTER JOIN" & _
                         " dbo.GS_Member_Image ON dbo.GS_Member.ID = dbo.GS_Member_Image.FKMemberID" & _
" ORDER BY dbo.GS_Member.Fname"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

    End Class

End Namespace
