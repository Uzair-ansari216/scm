﻿Imports System.Data.SqlClient
Imports System.Reflection

Namespace BizSoft.Bridge_SCM
    Public Class PurchaseOrder
        Inherits Logic

        Public Overrides Function GetList() As System.Data.DataSet
            Dim queryString As String = ""
            queryString = "SELECT DISTINCT convert(varchar(20), PODate, 06) As PODate, PONumber, ToName,subString(PONumber, 12, 4) AS Number , PK_PurchaseOrder " _
                 & " FROM VWGUI_INV_PurchaseOrder where CompanyId = " & HttpContext.Current.Session("CompanyID") &
                  " ORDER BY PODate "

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return ds
        End Function

        Public Function GetAllPurchaseOrder() As System.Data.DataSet
            Dim queryString As String = ""

            'queryString = "  SELECT * FROM tblPurchaseOrder where companyID = " & HttpContext.Current.Session("CompanyID") & ""

            queryString = "SELECT DISTINCT convert(varchar(20), PODate, 06) As PODate, PONumber, ToName, PK_PurchaseOrder 
                  FROM VWGUI_INV_PurchaseOrder where CompanyId = " & HttpContext.Current.Session("CompanyID") & " ORDER BY PODate"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return ds
        End Function
        Public Function GetFromToOrBillTo() As System.Data.DataSet

            Dim queryString As String = ""
            queryString = "SELECT CompanyID, CompanyName FROM CompanySetup where CompanyID = " & HttpContext.Current.Session("CompanyID") & " ORDER BY CompanyName"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return ds

        End Function
        Public Function GetSuppliers() As System.Data.DataSet

            Dim queryString As String = ""
            queryString = "SELECT  SupplierID, SupplierName FROM Supplier WHERE  CompanyID = " & HttpContext.Current.Session("CompanyID") & " and StatusID <> 155 ORDER BY SupplierName"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return ds

        End Function
        Public Function GetCustomer() As System.Data.DataSet

            Dim queryString As String = ""
            queryString = " SELECT CompanyID, CompanyName, 0 As IndexID FROM CompanySetup where CompanyID = " & HttpContext.Current.Session("CompanyID") & "UNION ALL SELECT CustomerID, Company, 1 FROM Customer WHERE  CompanyID = " & HttpContext.Current.Session("CompanyID") & " and StatusID <> 205 ORDER BY IndexID, CompanyName"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return ds

        End Function
        Public Function GetCurrencies() As System.Data.DataSet

            Dim queryString As String = ""
            queryString = "SELECT PK_CurrencyID, Currency + '(' + Symbol + ')' As Currency FROM tblCurrency ORDER BY DefaultCurrency desc"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return ds

        End Function
        Public Function GetPOStatus() As System.Data.DataSet

            Dim queryString As String = ""
            queryString = "SELECT  StatusID, StatusName FROM Status WHERE StatusID BETWEEN 800 AND 801 ORDER BY StatusID"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return ds

        End Function
        Public Function GetReason() As System.Data.DataSet

            Dim queryString As String = ""
            queryString = "SELECT  StatusID, StatusName FROM Status WHERE StatusID BETWEEN 400 AND 499 ORDER BY StatusID"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return ds

        End Function
        Public Function GetSignatures() As System.Data.DataSet

            Dim queryString As String = ""
            queryString = "SELECT  PK_Signature, PersonName FROM tblSignatures ORDER BY PersonName"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return ds

        End Function
        Public Function GetLocations() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim IPowerUser = HttpContext.Current.Session("isPowerUser")
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            Dim UserID As String = HttpContext.Current.Session("UserID")
            If IPowerUser = True Then
                StrQry = "SELECT LocationID as ID, Location as Name FROM Locations WHERE CompanyID = " & CompanyID & " and LocationID in (   select FK_Location  From User_Location where USERID = " & UserID & ")"
            Else
                StrQry = "SELECT LocationID as ID, Location as Name FROM Locations WHERE CompanyID = " & CompanyID & " and LocationID in (   select FK_Location  From User_Location where USERID = " & UserID & ")"

            End If

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetAllProducts() As System.Data.DataSet
            Dim queryString As String = ""

            queryString = "  SELECT * FROM INVGUI_ProductInformation where companyID = " & HttpContext.Current.Session("CompanyID") & " ORDER BY ModelNumber, PartNumber"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return ds
        End Function
        Public Function GetDetailById(ID As String) As System.Data.DataSet
            Dim queryString As String = ""
            queryString = "SELECT * FROM INVGUI_ProductInformation where companyID = " & HttpContext.Current.Session("CompanyID") & "and Pk_Item = " & ID & " ORDER BY ModelNumber, PartNumber "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return ds
        End Function
        Public Function AddPurchaseOrder(model As PurchaseOrderModel, productInformationList As List(Of PurchaseOrderDetail)) As String
            Try
                Dim queryStr As String = ""
                Dim POId As Int32 = 0
                Dim outPutParam As Int16 = 0
                Dim PONum As String = ""
                Dim ds As DataSet
                If model.PurchaseOrderNumber.Trim = "" Then
                    queryStr = "exec AutoGeneratedKEY 'PO','" & model.PurchaseOrderDate & "'," & HttpContext.Current.Session("CompanyID") & "," & model.Location & ""
                    ds = BizSoft.DBManager.GetDataSet(queryStr)
                    PONum = ds.Tables(0).Rows(0)(0).ToString()
                    'PONum = AutoGeneratedKEY("PO/", model.PurchaseOrderDate + " " + Now.ToShortTimeString, HttpContext.Current.Session("CompanyID"), model.Location)
                Else
                    PONum = model.PurchaseOrderNumber
                End If
                If model.PurchaseOrderID.Trim = "" Then
                    POId = 0
                    outPutParam = 0
                Else
                    POId = model.PurchaseOrderID
                    outPutParam = 1
                End If
                queryStr = "exec Transaction_PurchaseOrder " & POId & ",'" & PONum & "','" & model.PurchaseOrderDate & "'," & model.Currency & "," & model.ExchangeRate & "," & model.FromBillTo & ",'" & model.FromBillToAddress & "','" & model._To & "','" & model.ToAddress & "','" & model.ShipTo & "','" & model.ShipToAddress & "'," & model.Status & "," & model.Signature & ",'" & model.Notes & "','" & model.Comments & "','" & model.SpotNumber & "'," & HttpContext.Current.Session("CompanyID") & ",'" & String.Empty & "'," & outPutParam & ",'" & model.PurchaseOrderReference & "','" & model.BillDate & "'"

                ds = BizSoft.DBManager.GetDataSet(queryStr)
                If ds.Tables(0).Rows.Count > 0 Then
                    model.PurchaseOrderID = ds.Tables(0).Rows(0)(0).ToString()
                End If


                ' Insert Purchase Detail

                queryStr = "DELETE FROM tblPurchaseOrderItems WHERE FK_PurchaseOrder = " & model.PurchaseOrderID
                Dim returnValue As Integer = BizSoft.DBManager.ExecuteQry(queryStr)

                'For Each item As IEnumerable(Of PurchaseOrderDetail) In productInformationList.ToList()
                '    'queryStr = "exec Transaction_PurchaseOrderItems " & model.PurchaseOrderID & "," & Index + 1 & "," & productInformationList(Index).ItemId & ",'" & productInformationList(Index).Description & "'," & productInformationList(Index).Quantity & "," & productInformationList(Index).UnitPrice & "," & productInformationList(Index).OtherCost & ""
                '    ds = BizSoft.DBManager.GetDataSet(queryStr)
                'Next

                For index = 0 To productInformationList.Count - 1
                    queryStr = "exec Transaction_PurchaseOrderItems " & model.PurchaseOrderID & "," & index + 1 & "," & productInformationList(index).ItemId & ",'" & productInformationList(index).Description & "'," & productInformationList(index).Quantity & "," & productInformationList(index).UnitPrice & "," & productInformationList(index).OtherCost & ""
                    ds = BizSoft.DBManager.GetDataSet(queryStr)

                Next


                queryStr = "insert into tblPurchaseOrderStatus (FK_PurchaseOrder,FK_Reason,Detail)values(" & model.PurchaseOrderID & "," & model.Reason & ",'" & model.ReasonDetail & "')"
                ds = BizSoft.DBManager.GetDataSet(queryStr)

                Return model.PurchaseOrderID
                Exit Function

            Catch ex As Exception
                Return ex.Message
            End Try

            Return model.PurchaseOrderID
        End Function

        Public Function UpdatePOStatus(ByVal reason As String, ByVal detail As String, ByVal id As String) As String
            Dim queryStr As String = ""
            Dim message As String = ""
            Dim ds As DataSet
            queryStr = "update tblPurchaseOrderStatus set FK_Reason =" & reason & ", Detail = '" & detail & "' where FK_PurchaseOrder = " & id & ""
            ds = BizSoft.DBManager.GetDataSet(queryStr)
            'If ds.Tables(0).Rows.Count > 0 Then
            message = "Purchase order status updated successfully"
            'End If
            Return message
        End Function
        Public Function Delete(ByVal ID As String, ByVal name As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "exec sp_DeletePurchaseOrder " & ID & "," & HttpContext.Current.Session("CompanyID") & ""
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 And ds.Tables(0).Rows(0)("deletedId") <> -1 Then
                    Dim iAction As String = ""
                    iAction = "Delete Recored Purchase Order Detail PO ID = " & ds.Tables(0).Rows(0)("deletedId") & " and PO No = " & name
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Business Segment", "BusinessSegment", ID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                Else
                    Success = False
                    Message = "exist"
                    Return Message
                End If

            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Business Segment", "BusinessSegment", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
            End Try
        End Function

        Public Function getById(ByVal ID As String) As DataSet

            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "select * from tblpurchaseorder where PK_PurchaseOrder =" & ID & ""
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Return ds
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Business Segment", "BusinessSegment", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
            End Try
        End Function
        Public Function GetDetailsById(ByVal ID As String) As DataSet

            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "exec sp_getPurchaseOrderAndDetailsById " & ID & "," & HttpContext.Current.Session("CompanyID") & ""
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Return ds
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Business Segment", "BusinessSegment", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
            End Try
        End Function
        Public Function GetPurchaseOrderStatus(ByVal ID As String) As DataSet

            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "select FK_Reason,Detail from tblPurchaseOrderStatus where FK_PurchaseOrder =" & ID & ""
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Return ds
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Business Segment", "BusinessSegment", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
            End Try
        End Function

        Public Function AutoGeneratedKEY(ByVal l_location As String, ByVal dtSODate As String, ByVal companyID As Integer, ByVal LocationID As Integer) As String

            'KHI-0001/06


            Dim DateF As Date
            Dim DateT As Date
            Dim tempRs As New DataSet
            Dim strSQL As String = ""
            Dim gl_query As String = ""


            strSQL = "select * from GL_FINANCIAL_YEAR where DateFrom <  '" & dtSODate & "' and DateTo > '" & dtSODate & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(strSQL)

            DateF = ds.Tables(0).Rows(0)("DateFrom")
            DateT = ds.Tables(0).Rows(0)("DateTo")
            'DateF = Format(tempRs("DateFrom").value, "dd-MMM-YYYY")
            'DateT = Format(tempRs("DateTo").value, "dd-MMM-YYYY")


            gl_query = "SELECT isNull(max(subString(SalesOrder, 12, 4)), 0) + 1 as maxNO FROM tblSalesOrder WHERE SalesDate BETWEEN '" & FormatDateTime(DateF, DateFormat.ShortDate) & "' AND '" & FormatDateTime(DateT, DateFormat.ShortDate) & " 11:59:59 PM ' and Companyid = " & companyID & " and FK_LocationID = " & LocationID & ""

            'If GL_FinancialID > 3 Then
            '    gl_query = gl_query & " and Companyid = " & GL_companyID & " and FK_LocationID = " & cboLocation.ItemData(cboLocation.ListIndex)
            'End If
            Dim rs As DataSet = BizSoft.DBManager.GetDataSet(gl_query)
            'rs.Open(gl_query, objConn)


            gl_query = " SELECT     dbo.tblCity.cityCode + '-' + dbo.Locations.LocationCode AS LocCode FROM         dbo.Locations INNER JOIN   dbo.tblCity ON dbo.Locations.FK_CityID = dbo.tblCity.cityID where Locations.LocationID =  " & LocationID & ""
            Dim DsLocCode As DataSet = BizSoft.DBManager.GetDataSet(gl_query)

            l_location = l_location & Convert.ToString(DsLocCode.Tables(0).Rows(0)("LocCode"))

            AutoGeneratedKEY = l_location & "-" & Format(rs.Tables(0).Rows(0)("maxNO"), "0000") & "/" & Year(DateF) & "-" & Right(Year(DateT), 2)
            Exit Function

            Resume Next
        End Function

    End Class
End Namespace
