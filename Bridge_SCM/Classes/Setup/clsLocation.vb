﻿Imports System.Reflection

Namespace BizSoft.Bridge_SCM

    Public Class Location
        Inherits Logic
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT dbo.Locations.*, dbo.tblCity.cityName" &
            " FROM  dbo.Locations INNER JOIN" &
                         " dbo.tblCity ON dbo.Locations.FK_CityID = dbo.tblCity.cityID where dbo.Locations.CompanyID = " + HttpContext.Current.Session("CompanyID")
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Overrides Function GetList(ByVal CompanyID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),LocationID) ID,LocationCode, Location as Name  from Locations where CompanyID = " & CompanyID & " Order by Location  "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetById(ByVal id As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from Locations where LocationID = " & id & " and CompanyID = " + HttpContext.Current.Session("CompanyID") + ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function Update(model As LocationModel, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Try
                'Dim ds As DataSet
                'StrQry = "Select * from GS_Fault_Type Where Description='" & Description & "' and ID <>  '" & ID & "'"
                'ds = BizSoft.DBManager.GetDataSet(StrQry)
                'If ds.Tables(0).Rows.Count > 0 Then
                '    Success = False
                '    Message = "exist"
                '    Return Message
                'End If
                StrQry = "Update Locations set Location = '" & model.Location & "',FK_CityID='" & model.FK_CityID & "',LocationCode='" & model.LocationCode & "',Accounts_Receivable='" & model.Accounts_Receivable & "',Sales_Credit='" & model.Sales_Credit & "',Sales_Tax='" & model.Sales_Tax & "',Sales_Return='" & model.Sales_Return & "',Stock='" & model.Stock & "',CostofGoodSold='" & model.CostofGoodSold & "',Purchase='" & model.Purchase & "',LocalPurchase='" & model.LocalPurchase & "',Discount='" & model.Discount & "',Charges='" & model.Charges & "',StockInTransit='" & model.StockInTransit & "',SalesReturn='" & model.SalesReturn & "',ImportCost='" & model.ImportCost & "',ImportCostDr='" & model.ImportCostDr & "' where LocationID = " & model.ID & ""
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Edit Recored Location Name = " & model.Location
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Location", "Locations", model.ID, Environment.MachineName)

                    Success = True
                    Message = "update"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Location", "Locations", model.ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try

        End Function

        Public Function Delete(ByVal ID As String, ByVal Name As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            Try
                StrQry = "Delete from Locations where LocationID = " & ID & ""
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Delete Recored Location Name = " & Name
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Location", "Locations", ID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
                Return Message = ""
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Location", "Locations", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
            End Try

        End Function
        Public Function AccountCode() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "  exec WgetCharofAccount " & CompanyID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function Add(model As LocationModel, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As String
            Dim StrQry As String = ""
            'Dim ds As DataSet
            'StrQry = "Select * from Locations Where Description='" & Description & "'"
            'ds = BizSoft.DBManager.GetDataSet(StrQry)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    Success = False
            '    Message = "exist"
            '    Return Message
            'End If
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            ' Dim datee As System.DateTime = System.DateTime.Now
            Try
                StrQry = "Insert into Locations(Location,FK_CityID,LocationCode,CompanyID,Accounts_Receivable,Sales_Credit,Sales_Tax,Sales_Return,Stock,CostofGoodSold,Purchase,LocalPurchase,Discount,Charges,StockInTransit,SalesReturn,ImportCost,ImportCostDr) values('" & model.Location & "','" & model.FK_CityID & "','" & model.LocationCode & "','" & CompanyID & "','" & model.Accounts_Receivable & "','" & model.Sales_Credit & "','" & model.Sales_Tax & "','" & model.Sales_Return & "','" & model.Stock & "','" & model.CostofGoodSold & "','" & model.Purchase & "','" & model.LocalPurchase & "','" & model.Discount & "','" & model.Charges & "','" & model.StockInTransit & "','" & model.SalesReturn & "','" & model.ImportCost & "','" & model.ImportCostDr & "')"

                Dim returnValue As Integer = BizSoft.DBManager.ExecuteQry(StrQry)
                If returnValue = 1 Then
                    Dim iAction As String = ""
                    iAction = "Add New Recored Location Name = " & model.Location
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Location", "Locations", NewID, Environment.MachineName)

                    Message = "success"
                Else
                    Message = "failed"
                End If
                Return Message
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Location", "Locations", NewID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try

        End Function
        Public Function GetCity() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select CONVERT(nvarchar(10),cityID)as ID , cityName as Name from tblCity order by cityID"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetAccountList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from tblAccountTitles"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function AccountName(AccountCode As String) As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "  	Select AccountName FROM vChartOfAccount  " &
" WHERE levels = 5 and CompanyID = " & CompanyID & " And AccountCode = " & AccountCode & " AND AccountCode NOT IN (SELECT DISTINCT ParentAccountCode FROM vChartOfAccount WHERE CompanyID = " & CompanyID & " )" &
" ORDER BY AccountName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
    End Class

End Namespace
