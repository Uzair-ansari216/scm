﻿Imports System.Reflection

Namespace BizSoft.Bridge_SCM
    Public Class clsCity
        Inherits Logic
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT   dbo.tblCity.*, dbo.tblCountry.countryName" &
                     " FROM    dbo.tblCountry " &
                     "INNER JOIN dbo.tblCity ON dbo.tblCountry.countryID = dbo.tblCity.countryID where dbo.tblCity.CompanyID = " + HttpContext.Current.Session("CompanyID")
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetById(ByVal id As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from tblCity where cityid = " & id & " and CompanyID = " + HttpContext.Current.Session("CompanyID") + ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetList2() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),countryID ) ID, countryName  from tblCountry Order by countryName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function


        'Public Function Edit(ByVal ID As String) As System.Data.DataSet
        '    Dim StrQry As String = ""
        '    StrQry = "Select * from tblItemClass where  ID = " & ID & ""
        '    Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
        '    Return ds
        'End Function
        'New Methods
        Public Function Update(ByVal ID As String, ByVal cityCode As String, ByVal cityName As String, ByVal countryID As String, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "Select * from tblCity Where cityCode = '" & cityCode & "' AND  cityID <>  '" & ID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                If ID <> "-1" Then

                    StrQry = "Update tblCity set cityCode = '" & cityCode & "', cityName = '" & cityName & "', countryID = '" & countryID & "' where cityID = " & ID & ""
                Else
                    StrQry = "Update tblCity set cityCode = '" & cityCode & "', cityName = '" & cityName & "', countryID = '" & countryID & "' where cityID = " & ID & ""
                End If

                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Edit Recored City"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "City", "tblCity", ID, Environment.MachineName)

                    Success = True
                    Message = "update"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "City", "tblCity", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function

        Public Function Delete(ByVal ID As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "select * from tblCity where countryID = '" & ID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If

                StrQry = "Delete from tblCity where cityID = " & ID & ""
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Delete Recored City"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "City", "tblCity", ID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "City", "tblCity", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
            End Try

        End Function

        Public Function Add(ByVal ID As String, ByVal cityCode As String, ByVal cityName As String, ByVal countryID As String, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "Select * from tblCity Where cityCode = '" & cityCode & "' AND  countryID = '" & countryID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                If ID <> "-1" Then
                    Dim company = HttpContext.Current.Session("CompanyID")
                    StrQry = "Insert into tblCity(cityCode,cityName,countryID,CompanyID) values('" & cityCode & "','" & cityName & "','" & countryID & "'," & company & ")Select @@IDENTITY"
                    'Else
                    '    StrQry = "Insert into GS_Resource(Description,Fk_ResourceType) values ('" & Description & "','" & Fk_ResourceType & "')Select @@IDENTITY"
                End If

                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    NewID = ds.Tables(0).Rows(0)(0).ToString()
                    Dim iAction As String = ""
                    iAction = "Add New Recored City"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "City", "tblCity", NewID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "City", "tblCity", NewID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try

        End Function
    End Class

End Namespace
