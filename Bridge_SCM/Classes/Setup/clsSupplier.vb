﻿Imports System.Reflection

Namespace BizSoft.Bridge_SCM

    Public Class Supplier
        Inherits Logic
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "  select * from Supplier where CompanyID = " + HttpContext.Current.Session("CompanyID")
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetById(ByVal id As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from Supplier where SupplierID = '" & id & "' and CompanyID = " + HttpContext.Current.Session("CompanyID") + ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetCity() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select CONVERT(nvarchar(10),cityID)as ID , cityName as Name from tblCity order by cityID"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetCountry() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "  Select convert(nvarchar(10),countryID ) ID, countryName  as Name, countryCode  from tblCountry   Order by countryName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetLocation() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            Dim LocationID As String = HttpContext.Current.Session("LocationId")
            Dim isPowerUser As Boolean = HttpContext.Current.Session("isPowerUser")
            If isPowerUser = True Then

                StrQry = "SELECT LocationID, Location + ' - ' + CityName LocationName  FROM VWGUI_Location_CityCountry where CompanyID =  " & CompanyID & " ORDER BY CityName, Location"
            Else

                StrQry = "SELECT LocationID, Location + ' - ' + CityName LocationName  FROM VWGUI_Location_CityCountry where Locationid = " & LocationID & " and CompanyID =  " & CompanyID & " ORDER BY CityName, Location"

                'CboLocation.Enabled = False
            End If

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function Edit(ByVal RecID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from GS_Fault_Type where  ID = " & RecID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        ' New Methods
        Public Function Update(model As SupplierModel, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            'Dim ds As DataSet
            'StrQry = "Select * from Supplier Where  SupplierID =  '" & model.SupplierID & "'"
            'ds = BizSoft.DBManager.GetDataSet(StrQry)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    Success = False
            '    Message = "exist"
            '    Return Message
            'End If
            Try
                StrQry = "Update Supplier set Company='" & model.Company & "',Address = '" & model.Address & "',Contact = '" & model.Contact & "',ContactPhone = '" & model.ContactPhone & "',Phone = '" & model.Phone & "',Fax = '" & model.Fax & "',Email = '" & model.Email & "',AltContact = '" & model.AltContact & "',AltPhone = '" & model.AltPhone & "',Terms = '" & model.Terms & "',CreditLimit = '" & model.CreditLimit & "',Notes = '" & model.Notes & "',LocationID = '" & model.LocationID & "',DebtorAccountCode = '" & model.DebtorAccountCode & "',DebtorOpBal = '" & model.DebtorOpBal & "',CreditorAccountCode = '" & model.CreditorAccountCode & "',CreditorOpBal = '" & model.CreditorOpBal & "',NTN = '" & model.NTN & "',STN = '" & model.STN & "',cityID = '" & model.cityID & "',countryID = '" & model.countryID & "' where SupplierID = '" & model.SupplierID & "' "

                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Edit Recored Supplier"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Supplier", "Supplier", 0, Environment.MachineName)

                    Success = True
                    Message = "update"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Supplier", "Supplier", model.SupplierID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function

        Public Function Delete(ByVal ID As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            Try
                StrQry = "Delete from Supplier where SupplierID = '" & ID & "'"
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Delete Recored Supplier"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Supplier", "Supplier", 0, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
                Return Message = ""
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Supplier", "Supplier", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
            End Try

        End Function

        Public Function Add(model As SupplierModel, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Try

                StrQry = "select isNull(max(subString(SupplierID, 6, 2)), 0) + 1 as maxNO FROM Supplier"

                Dim rs As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
                model.SupplierID = "SPS-" & Format(rs.Tables(0).Rows(0)("maxNO"), "000") & "/" & Date.Now.Year
                StrQry = "Select * from Supplier Where  SupplierID ='" & model.SupplierID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
                Dim UserID As String = HttpContext.Current.Session("UserID")
                ' Dim datee As System.DateTime = System.DateTime.Now
                StrQry = "Insert into Supplier(SupplierID,Company,Address,Contact,ContactPhone,Phone,Fax,Email,AltContact,AltPhone,Terms,CreditLimit,Notes,StatusID,LocationID,DebtorAccountCode,DebtorOpBal,CreditorAccountCode,CreditorOpBal,CompanyID,NTN,STN,cityID,countryID) values('" & model.SupplierID & "','" & model.Company & "','" & model.Address & "','" & model.Contact & "','" & model.ContactPhone & "','" & model.Phone & "','" & model.Fax & "','" & model.Email & "','" & model.AltContact & "','" & model.AltPhone & "','" & model.Terms & "','" & model.CreditLimit & "','" & model.Notes & "',150,'" & model.LocationID & "','" & model.DebtorAccountCode & "','" & model.DebtorOpBal & "','" & model.CreditorAccountCode & "','" & model.CreditorOpBal & "','" & CompanyID & "','" & model.NTN & "','" & model.STN & "','" & model.cityID & "','" & model.countryID & "')"

                Dim returnValue As Integer = BizSoft.DBManager.ExecuteQry(StrQry)
                If returnValue = 1 Then
                    Dim iAction As String = ""
                    iAction = "Add New Recored Supplier"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Supplier", "Supplier", NewID, Environment.MachineName)

                    Message = "success"
                Else
                    Message = "failed"
                End If
                Return Message
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Supplier", "Supplier", NewID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try

        End Function
        Public Function GetListLocation(ByVal CompanyID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),locationID) ID, Location as Name  from Locations  where CompanyID = " & CompanyID & " Order by Location"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
        Public Function GetSupplierType() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),id ) ID, SupplierType  as Name  from SupplierTypes   Order by SupplierType"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetSupplier(ByVal CompanyID As String) As System.Data.DataSet

            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),SupplierID    ) ID, SupplierName     as Name ,Company  from Supplier  where companyID " & CompanyID & "    Order by SupplierName "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
    End Class

End Namespace
