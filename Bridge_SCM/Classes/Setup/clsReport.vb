﻿Namespace BizSoft.Bridge_SCM
    Public Class Report
        Public Function FillReports(ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            'StrQry = "select ReportID as ID, ReportName as Name  from GS_Report_Setup where FormName =  'Reports' AND IsActive =1 AND FKReportCategory = " & ID & ""
            Dim UserID As String = HttpContext.Current.Session("UserID")
            StrQry = "SELECT dbo.GS_Report_Setup.ReportID as ID,dbo.GS_Report_Setup.ReportName as Name" &
" FROM     dbo.tbl_Report_Role_Detail INNER JOIN" &
                  " dbo.Users ON dbo.tbl_Report_Role_Detail.RoleID = dbo.Users.RoleID INNER JOIN" &
                  " dbo.GS_Report_Setup ON dbo.tbl_Report_Role_Detail.ReportID = dbo.GS_Report_Setup.ReportID" &
" WHERE  (dbo.Users.UserID = " & UserID & ") AND (dbo.tbl_Report_Role_Detail.isView = 1) AND (dbo.GS_Report_Setup.FormName = 'Reports') AND (dbo.GS_Report_Setup.IsActive = 1) AND " &
                  " (dbo.GS_Report_Setup.FKReportCategory = " & ID & ")"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
#Region "Report Module"
        Public Function FillReportModule() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select  Id, Name from GS_Report_Module"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetReportCredentialsByModule(ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            Dim UserID As String = HttpContext.Current.Session("UserID")
            StrQry = "select * from dbo.GS_Report_Module where dbo.GS_Report_Module.Id = " & ID & ""

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetReportPathByModule(ID As Int32) As System.Data.DataSet
            Dim StrQry As String = ""
            Dim UserID As String = HttpContext.Current.Session("UserID")
            StrQry = "select ReportPath from dbo.GS_Report_Module where dbo.GS_Report_Module.Id = " & ID & ""

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function FillCategories(ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            'StrQry = "select ReportID as ID, ReportName as Name  from GS_Report_Setup where FormName =  'Reports' AND IsActive =1 AND FKReportCategory = " & ID & ""
            Dim UserID As String = HttpContext.Current.Session("UserID")
            StrQry = "select dbo.GS_Report_Category.ID as ID, dbo.GS_Report_Category.Description as Name from dbo.GS_Report_Category, dbo.Users where (dbo.Users.UserID = " & UserID & ") AND (dbo.GS_Report_Category.ReportModuleId = " & ID & ")"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

#End Region

        Public Function FillReportsCategory() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select  ID, Description  from GS_Report_Category"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function


        Public Function GetReportData(ByVal ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = " select * from GS_Report_Setup where ReportID = '" & ID & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetReportParameter(ByVal ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = " select * from GS_ReportParameter where ReportID = '" & ID & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetReportSpParameter(ByVal ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = " select * from GS_Report_SP_Parameter where ReportID = '" & ID & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function ExecuteSp(htForSp As Hashtable, ByVal sp As String)
            BizSoft.DBManager.ExecuteSPForInsertion(sp, htForSp)

        End Function
        Public Function GetReportGLMenu(ByVal ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "  select * from GL_Menu where WP_FormName = '" & ID & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetData(ByVal ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from GS_ReportDetail where ReportID = '" & ID & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetStoreProcedure(ByVal ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from GS_Report_SP where ReportID = '" & ID & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetControlsForSp(ByVal report As String, ByVal Sp As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from GS_Report_SP_Parameter where ReportID = '" & report & "' and SP_Name = '" & Sp & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetData2(ByVal ID As String, ReportName As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from GS_ReportDetail where ObjectName = '" & ID & "' and ReportID = '" & ReportName & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetReportParameters(ByVal Report As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from GS_ReportParameter where ReportID = '" & Report & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

#Region "Selection Formula"
        Public Function GetReportSelectionFormula(ByVal ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = " select * from GS_Report_SelectionFormula where ReportID = '" & ID & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
#End Region
    End Class
End Namespace