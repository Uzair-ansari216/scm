﻿Namespace BizSoft.Bridge_SCM

    Public Class StatusApproval
        Inherits Logic
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT     0 as isSave, SalesOrder As OrderNo, convert(varchar(20), salesdate, 106) As SalesDate, CustomerName,    EmployeeName, CASE OrderType When 0 Then 'Phone Call' Else OrderRef END As Refrences, Remarks,     PK_SalesOrder, Status2, StatusRemarks FROM VWGUI_INV_SalesOrder WHERE FK_StatusID = 801 and companyID =" & HttpContext.Current.Session("CompanyID") & " UNION ALL" &
            " Select      0, TDRequest, convert(varchar(20), TDRDate, 106) As TDRDate, CustomerName,    EmployeeName, Case 1 When 0 Then 'Phone Call' Else 'OrderRef' END As Refrences, Remarks,     PK_TDRequest, Status2, StatusRemarks FROM VWGUI_INV_TDRequest WHERE FK_StatudID = 801 and companyID =" & HttpContext.Current.Session("CompanyID") & " Order by salesdate"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function Edit(ByVal RecID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from GS_Fault_Type where  ID = " & RecID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        ' New Methods
        Public Function Update(ByVal ID As String, ByVal Description As String, ByVal SafetyIns As String, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            StrQry = "Select * from GS_Fault_Type Where Description='" & Description & "' and ID <>  '" & ID & "'"
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "exist"
                Return Message
            End If
            StrQry = "Update GS_Fault_Type set Description = '" & Description & "',Safty_ins='" & SafetyIns & "' where ID = " & ID & ""
            Try
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Success = True
                    Message = "update"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function

        Public Function Delete(ByVal ID As String) As String

            Dim StrQry As String = ""

            StrQry = "Delete from tblSalesOrderStatus where FK_SalesOrder = " & ID & ""
            BizSoft.DBManager.ExecuteQry(StrQry)

            Return ""
        End Function

        Public Function Add(ByVal FKSaleOrder As String, ByVal Status As String, ByVal StatusDate As String, ByRef Remarks As String, ByRef Message As String) As String
            Dim StrQry As String = ""

            Dim UserID As String = HttpContext.Current.Session("UserID")
            StrQry = "Insert into tblSalesOrderStatus(FK_SalesOrder,FK_StatusID,StatusDate,Remarks,UpdatedBy) values('" & FKSaleOrder & "','" & Status & "','" & StatusDate & "','" & Remarks & "','" & UserID & "')"

            Dim returnValue As Integer = BizSoft.DBManager.ExecuteQry(StrQry)
            If returnValue = 1 Then
                Message = "success"
            Else
                Message = "failed"
            End If
            Return Message
        End Function
        Public Function GetStatus() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = " SELECT  StatusID, Description FROM Status WHERE StatusID BETWEEN 802 AND 809 ORDER BY StatusID"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
    End Class

End Namespace
