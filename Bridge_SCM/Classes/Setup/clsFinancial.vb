﻿Namespace BizSoft.Bridge_SCM
    Public Class clsFinancial
        Dim queryString As String = ""
        Dim ds As DataSet
        Public Function GetAllThemes() As DataSet
            queryString = "select * from Th_Financial"
            ds = BizSoft.DBManager.GetDataSet(queryString)
            Return ds
        End Function
        Public Function SaveTheme(ByVal name As String) As DataSet
            queryString = "insert into Th_Financial values('" & name & "',CONVERT(date, GETDATE())) Select @@IDENTITY"
            ds = BizSoft.DBManager.GetDataSet(queryString)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim iAction As String = ""
                iAction = "Add new Recored Financial Theme, Name = " & name
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Financial", "Th_Financial", ds.Tables(0).Rows(0)("Column1"), Environment.MachineName)

            End If
            Return ds
        End Function
        Public Function GetHeadings(ByVal theme As String, ByVal id As String, ByVal parentCode As String) As DataSet
            queryString = "select * from TD_Financial where FinancialID = " & theme & " and DetailID <> " & id & " and ParentCode <> '" & parentCode & "' order by Code"
            ds = BizSoft.DBManager.GetDataSet(queryString)
            Return ds
        End Function
        Public Function SaveDetail(model As FinancialModel, ByVal IsBold As Int32, ByVal RowColour As String) As DataSet
            If Convert.ToInt32(model.ID) > 0 Then
                queryString = "update TD_Financial set FinancialID = " & model.Financial & ",Code = '" & model.code & "',ParentCode = '" & model.ParentCode & "',Heading = '" & model.Heading & "',Impact = " & If(Convert.ToInt32(model.Impact) = 1, model.Impact, -1) & ",isActivity = " & If(Convert.ToInt32(model.Document) = 1, 0, 1) & ",IsBalanceSheetItem = " & If(Convert.ToInt32(model.Document) = 1, 0, 1) & ",isShowInReport = " & model.IsShowInReport & ",AddtoCode = '" & model.AddToCode & "',NotesCode = '" & model.NotesCode & "', fixedValue = " & If(model.FixedValue = "", 0, model.FixedValue) & ",isBold = " & IsBold & ",bg_Color = '" & RowColour & "' where DetailID = " & model.ID & ""
                ds = BizSoft.DBManager.GetDataSet(queryString)
            Else
                queryString = "insert into TD_Financial (FinancialID,Code,ParentCode,Heading,Impact,isActivity,IsBalanceSheetItem,isShowInReport,AddtoCode,NotesCode,fixedValue,isBold,bg_Color) values(" & model.Financial & ",'" & model.code & "','" & model.ParentCode & "','" & model.Heading & "'," & If(Convert.ToInt32(model.Impact) = 1, model.Impact, -1) & "," & If(Convert.ToInt32(model.Document) = 1, 0, 1) & "," & If(Convert.ToInt32(model.Document) = 1, 0, 1) & "," & model.IsShowInReport & ",'" & model.AddToCode & "','" & model.NotesCode & "'," & model.FixedValue & "," & IsBold & ",'" & RowColour & "') Select @@IDENTITY"
                ds = BizSoft.DBManager.GetDataSet(queryString)
            End If


            Dim dataSet As DataSet
            If Convert.ToInt32(model.ID) = 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim iAction As String = ""
                    iAction = "Add new Recored Financial Detail, Name = " & model.Heading
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Financial", "Td_Financial", ds.Tables(0).Rows(0)("Column1"), Environment.MachineName)
                End If
            Else
                Dim iAction As String = ""
                iAction = "Update Recored Financial Detail, Name = " & model.Heading
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Financial", "Td_Financial", model.ID, Environment.MachineName)
            End If

            dataSet = getDetails(model.Financial, model.Document)
            Return dataSet
        End Function

        Public Function getDetails(ByVal theme As String, ByVal document As String) As DataSet
            queryString = "select * from Td_Financial where FinancialID = " & theme & " and IsBalanceSheetItem = " & If(Convert.ToInt32(document) = 1, 0, 1) & " Order by Code"
            ds = BizSoft.DBManager.GetDataSet(queryString)
            Return ds
        End Function
        Public Function getCode(ByVal financial As String, ByVal code As String) As DataSet
            queryString = "spGeneratePnLBSCode '" & code & "'," & HttpContext.Current.Session("CompanyID") & "," & financial & ""
            ds = BizSoft.DBManager.GetDataSet(queryString)
            Return ds
        End Function
        Public Function getByID(ByVal id As String) As DataSet
            queryString = "select  * from Td_Financial where DetailID = " & id & ""
            ds = BizSoft.DBManager.GetDataSet(queryString)
            Return ds
        End Function
        Public Function deleteDetail(ByVal id As String) As Tuple(Of String, Boolean)
            Dim message As String = ""
            Dim status As Boolean = True
            queryString = "select  * from Td_Financial where DetailID = " & id & " and (ParentCode is NULL or ParentCode = '')"
            ds = BizSoft.DBManager.GetDataSet(queryString)
            If ds.Tables(0).Rows.Count > 0 Then
                queryString = "select  * from Td_Financial where parentcode = '" & ds.Tables(0).Rows(0)("Code") & "'"
                ds = BizSoft.DBManager.GetDataSet(queryString)
                If ds.Tables(0).Rows.Count > 0 Then
                    message = "You can not delete parent node"
                    status = False
                Else
                    queryString = "delete from Td_Financial where DetailID = " & id & ""
                    ds = BizSoft.DBManager.GetDataSet(queryString)
                    message = "Financial detail delete successfully"
                    Dim iAction As String = ""
                    iAction = "Delete Recored Financial Detail, ID = " & id
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Financial", "Td_Financial", id, Environment.MachineName)
                    status = True
                End If
            Else
                queryString = "select  * from Td_Financial where DetailID = " & id & ""
                ds = BizSoft.DBManager.GetDataSet(queryString)
                If ds.Tables(0).Rows.Count > 0 Then
                    queryString = "select  * from Td_Financial where parentcode = '" & ds.Tables(0).Rows(0)("Code") & "'"
                    ds = BizSoft.DBManager.GetDataSet(queryString)
                    If ds.Tables(0).Rows.Count > 0 Then
                        message = "You can not delete parent node"
                        status = False
                    Else
                        queryString = "delete from Td_Financial where DetailID = " & id & ""
                        ds = BizSoft.DBManager.GetDataSet(queryString)
                        message = "Financial detail delete successfully"
                        Dim iAction As String = ""
                        iAction = "Delete Recored Financial Detail, ID = " & id
                        BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Financial", "Td_Financial", id, Environment.MachineName)
                        status = True
                    End If
                End If
            End If
            Return Tuple.Create(CStr(message), CBool(status))
        End Function
        Public Function GetTrialBalance(ByVal fromDate As Date, ByVal toDate As Date) As DataSet
            Dim fDate = New DateTime(Convert.ToInt32(fromDate.ToString().Split("/")(2).Split(" ")(0)), Convert.ToInt32(fromDate.ToString().Split("/")(0)), Convert.ToInt32(fromDate.ToString().Split("/")(1))).ToString("dd-MMMM-yyyy")
            Dim tDate = New DateTime(Convert.ToInt32(toDate.ToString().Split("/")(2).Split(" ")(0)), Convert.ToInt32(toDate.ToString().Split("/")(0)), Convert.ToInt32(toDate.ToString().Split("/")(1))).ToString("dd-MMMM-yyyy")
            queryString = "GUI_GetWebTRIALBALANCE '" & fDate & "','" & tDate & "'," & HttpContext.Current.Session("CompanyID") & ",'" & HttpContext.Current.Session("LocationId") & "'," & HttpContext.Current.Session("UserID") & ""
            ds = BizSoft.DBManager.GetDataSet(queryString)
            Return ds
        End Function
        Public Function TrialBalance(ByVal financialtheme As String) As DataSet
            queryString = "select (SELECT TOP(1) dbo.TD_Financial.Heading FROM dbo.TD_Financial LEFT OUTER JOIN dbo.TD_Financial_Maping ON dbo.TD_Financial.DetailID = dbo.TD_Financial_Maping.DetailID WHERE (dbo.TD_Financial.FinancialID = " & financialtheme & ") AND (dbo.TD_Financial_Maping.Accountcode = GL_TRIALBALANCE_REPORT_WebForm.accountCode ) ) as Heading," &
" Left(AccountCode,1) AccountType, * ," &
" (CASE WHEN Len(AccountCode) = 1 THEN null" &
" WHEN Len(AccountCode) = 3 THEN Left (AccountCode,1)" &
" When Len(AccountCode) = 5 Then Left (AccountCode,3)" &
" WHEN Len(AccountCode) = 8 THEN Left (AccountCode,5)" &
" WHEN Len(AccountCode) = 12 THEN Left (AccountCode,8)" &
" ELSE 9999 END) as ParentCode" &
" From [GL_TRIALBALANCE_REPORT_WebForm] order by AccountCode"

            ds = BizSoft.DBManager.GetDataSet(queryString)
            Return ds
        End Function

        Public Function SaveMapping(mappingList As List(Of FinancialMappingModel), ByVal financialtheme As String) As String
            Dim strQry As String = ""
            For Each item As FinancialMappingModel In mappingList
                strQry = " delete TD_Financial_Maping  where Accountcode =  '" & item.AccountCode & "' and DetailID in (  select DetailID From TD_Financial where FinancialID = " & financialtheme & ")"
                Dim ds As DataSet = BizSoft.DBManager.GetDataSet(strQry)
                'If ds.Tables(0).Rows.Count = 0 Then
                If item.AccountCode.Length > 8 Then
                    strQry = "insert into TD_Financial_Maping (DetailID,AccountCode,Dr_Amount,CR_amount) values(" & item.DetailID & "," & item.AccountCode & "," & item.DebitAccount & "," & item.CreditAccount & ")"
                    BizSoft.DBManager.ExecuteQry(strQry)
                End If
                'End If
            Next

            updateFinancialDetails(financialtheme)

            Return "asds"
        End Function

        Public Function UnMapping(ByVal code As String, ByVal detailId As String, ByVal financialtheme As String) As DataSet
            Dim strQry As String = ""
            strQry = "delete from TD_Financial_Maping where DetailID = " & detailId & " and Accountcode = " & code & ""
            ds = BizSoft.DBManager.GetDataSet(strQry)
            updateFinancialDetails(financialtheme)
            Return ds
        End Function
        Public Function getHeadingDetails(ByVal id As Int32, ByVal theme As String) As DataSet
            queryString = "sp_GetPAndLHeadingDetail " & id & "," & HttpContext.Current.Session("CompanyID") & "," & theme & ""
            ds = BizSoft.DBManager.GetDataSet(queryString)
            Return ds
        End Function
        Public Function refreshFinancialDetails(ByVal financialTheme As Int32) As DataSet
            updateFinancialDetails(financialTheme)
            Return ds
        End Function
        Public Function GetUnMappedChartOfAccount(ByVal theme As String, ByVal document As String) As DataSet
            If document = 1 Then
                document = 0
            Else
                document = 1
            End If
            queryString = "SPGetMissingMapping " & document & "," & HttpContext.Current.Session("CompanyID") & "," & theme & ""
            ds = BizSoft.DBManager.GetDataSet(queryString)
            Return ds
        End Function
        Public Function GetFinancialHeadings(ByVal theme As String) As DataSet
            queryString = "select DetailID ," &
                                      " Heading + ' ~ ' +" &
                                      " isnull((select Heading   From TD_Financial TD where  TD.IsBalanceSheetItem = 0 and TD.Financialid = " & theme & " and TD.Code = TD_Financial.ParentCode),'-') as Title" &
                                      " From TD_Financial where IsBalanceSheetItem = 0 and Financialid = " & theme & " Order by Code"
            ds = BizSoft.DBManager.GetDataSet(queryString)
            Return ds
        End Function
        Public Function FinancialMapping(mappingList As List(Of FinancialMappingModel), ByVal financialtheme As String) As String
            Dim strQry As String = ""
            For Each item As FinancialMappingModel In mappingList
                If Not IsNothing(item.DetailID) Then
                    strQry = "insert into TD_Financial_Maping (DetailID,AccountCode,Dr_Amount,CR_amount) values(" & item.DetailID & "," & item.AccountCode & ",0,0)"
                    BizSoft.DBManager.ExecuteQry(strQry)
                End If
            Next

            updateFinancialDetails(financialtheme)

            Return "asds"
        End Function
        Private Function updateFinancialDetails(ByVal financialtheme As String) As Integer
            Dim strQry As String = ""
            '            strQry = "update Td_Financial" &
            '" set DrAmount = isnull((select sum(Dr_Amount) from TD_Financial_Maping where TD_Financial_Maping.DetailID = Td_Financial.DetailID),0)," &
            '" CrAmount = isnull((select sum(Cr_Amount) from TD_Financial_Maping where TD_Financial_Maping.DetailID = Td_Financial.DetailID),0)" &
            '" where FinancialID = " & financialtheme & ""
            '            BizSoft.DBManager.GetDataSet(strQry)

            strQry = "SPMakeBS_PnlTotal " & HttpContext.Current.Session("CompanyID") & "," & financialtheme & ""
            BizSoft.DBManager.GetDataSet(strQry)
            Return 0
        End Function

    End Class

End Namespace
