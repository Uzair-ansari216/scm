﻿Imports System.Reflection

Namespace BizSoft.Bridge_SCM

    Public Class clsItem
        Inherits Logic

        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT dbo.tblItems.*, dbo.tblUnits.Unit, dbo.BusinessSegment.SegmentName, dbo.INVGUI_GroupWithClass.ItemCategory" &
                      " FROM   dbo.tblItems " &
                      " INNER Join dbo.tblUnits ON dbo.tblItems.FK_Unit = dbo.tblUnits.PK_Unit INNER JOIN" &
                        " dbo.BusinessSegment ON dbo.tblItems.FK_BrandID = dbo.BusinessSegment.SegmentID INNER JOIN" &
                        " dbo.INVGUI_GroupWithClass ON dbo.tblItems.FK_ItemCategory = dbo.INVGUI_GroupWithClass.PK_ItemCategory where dbo.tblItems.CompanyID = " + HttpContext.Current.Session("CompanyID")
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetAll() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select  PK_Item,PartNumber, ModelNumber, Description FROM tblItems WHERE CompanyID = " & HttpContext.Current.Session("CompanyID") & " ORDER BY PartNumber"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetListBrand() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT SegmentID, SegmentName FROM BusinessSegment WHERE CompanyID = 1 ORDER BY SegmentName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetListUnit() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT PK_Unit, Unit FROM tblUnits ORDER BY Unit"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        'Public Function Edit(ByVal RecID As String) As System.Data.DataSet
        '    Dim StrQry As String = ""
        '    StrQry = "Select * from tblItems where  ID = " & RecID & ""
        '    Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
        '    Return ds
        'End Function
        'New Methods
        Public Function Update(ByVal ID As String, ByVal FK_ItemCategory As String, ByVal FK_BrandID As String, ByVal FK_Unit As String, ByVal PartNumber As String, ByVal ModelNumber As String, ByVal ProductName As String, ByVal Description As String, ByVal MinQuantity As String, ByVal MaxQuantity As String, ByVal ReOrderQuantity As String, ByVal DeliveryLandTime As String, ByVal SalePoint As String, ByVal ExpGST As String, ByVal WarrantyYear As String, ByVal WarrantyNote As String, ByVal isActive As String, ByVal GST As String, ByVal AvailableForSO As String, ByVal ItemToBeRepost As String, ByVal isSerialNo As String, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "Select * from tblItems Where Description='" & Description & "' and PK_Item <>  '" & ID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                If ID <> "-1" Then

                    StrQry = "Update tblItems set FK_ItemCategory = " & FK_ItemCategory & " ,FK_BrandID = '" & FK_BrandID & "', FK_Unit = '" & FK_Unit & "',PartNumber = '" & PartNumber & "', ModelNumber = '" & ModelNumber & "', ProductName = '" & ProductName & "', Description = '" & Description & "', MinQuantity = '" & MinQuantity & "', MaxQuantity = '" & MaxQuantity & "', ReOrderQuantity = '" & ReOrderQuantity & "', DeliveryLandTime = '" & DeliveryLandTime & "', SalePoint = '" & SalePoint & "', ExpGST = '" & ExpGST & "', WarrantyYear = '" & WarrantyYear & "', WarrantyNote = '" & WarrantyNote & "', isActive = '" & isActive & "', GST = '" & GST & "', AvailableForSO = '" & AvailableForSO & "', ItemToBeRepost = '" & ItemToBeRepost & "',isSerialNo = '" & isSerialNo & "' where PK_Item = " & ID & ""
                Else
                    StrQry = "Update tblItems set FK_ItemCategory = " & FK_ItemCategory & " ,FK_BrandID = '" & FK_BrandID & "', FK_Unit = '" & FK_Unit & "',PartNumber = '" & PartNumber & "', ModelNumber = '" & ModelNumber & "', ProductName = '" & ProductName & "', Description = '" & Description & "', MinQuantity = '" & MinQuantity & "', MaxQuantity = '" & MaxQuantity & "', ReOrderQuantity = '" & ReOrderQuantity & "', DeliveryLandTime = '" & DeliveryLandTime & "', SalePoint = '" & SalePoint & "', ExpGST = '" & ExpGST & "', WarrantyYear = '" & WarrantyYear & "', WarrantyNote = '" & WarrantyNote & "', isActive = '" & isActive & "', GST = '" & GST & "', AvailableForSO = '" & AvailableForSO & "', ItemToBeRepost = '" & ItemToBeRepost & "',isSerialNo = '" & isSerialNo & "' where PK_Item = " & ID & ""
                End If

                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Edit Recored Item Category"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Item Category", "FK_ItemCategory", ID, Environment.MachineName)

                    Success = True
                    Message = "update"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Item Category", "FK_ItemCategory", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try

        End Function

        Public Function Delete(ByVal ID As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            'Dim ds As DataSet
            'StrQry = "select * from tblItems where PK_Item = '" & ID & "'"
            'ds = BizSoft.DBManager.GetDataSet(StrQry)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    Success = False
            '    Message = "exist"
            '    Return Message
            'End If
            Try
                StrQry = "Delete from tblItems where PK_Item = " & ID & ""
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Delete Recored Items"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Items", "tblItems", ID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If

            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Items", "tblItems", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
            End Try

        End Function

        Public Function Add(ByVal ID As String, ByVal FK_ItemCategory As String, ByVal FK_BrandID As String, ByVal FK_Unit As String, ByVal PartNumber As String, ByVal ModelNumber As String, ByVal ProductName As String, ByVal Description As String, ByVal MinQuantity As String, ByVal MaxQuantity As String, ByVal ReOrderQuantity As String, ByVal DeliveryLandTime As String, ByVal SalePoint As String, ByVal ExpGST As String, ByVal WarrantyYear As String, ByVal WarrantyNote As String, ByVal isActive As String, ByVal GST As String, ByVal AvailableForSO As String, ByVal ItemToBeRepost As String, ByVal isSerialNo As String, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            Try
                StrQry = "Select * from tblItems Where Description = '" & Description & "' AND FK_ItemCategory = '" & FK_ItemCategory & "' "
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                If ID <> "-1" Then
                    StrQry = "Insert into tblItems(FK_ItemCategory,FK_BrandID,FK_Unit,PartNumber,ModelNumber,ProductName,Description,MinQuantity,MaxQuantity,ReOrderQuantity,DeliveryLandTime,SalePoint,ExpGST,WarrantyYear,WarrantyNote,isActive,GST,AvailableForSO,ItemToBeRepost,isSerialNo,CompanyID) values('" & FK_ItemCategory & "','" & FK_BrandID & "','" & FK_Unit & "','" & PartNumber & "','" & ModelNumber & "','" & ProductName & "','" & Description & "','" & MinQuantity & "','" & MaxQuantity & "','" & ReOrderQuantity & "','" & DeliveryLandTime & "','" & SalePoint & "','" & ExpGST & "','" & WarrantyYear & "','" & WarrantyNote & "','" & isActive & "','" & GST & "','" & AvailableForSO & "','" & ItemToBeRepost & "','" & isSerialNo & "','" & CompanyID & "')Select @@IDENTITY"
                    'Else
                    '    StrQry = "Insert into GS_Resource(Description,Fk_ResourceType) values ('" & Description & "','" & Fk_ResourceType & "')Select @@IDENTITY"
                End If

                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    NewID = ds.Tables(0).Rows(0)(0).ToString()
                    Dim iAction As String = ""
                    iAction = "Add New Recored Items"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Items", "tblItems", NewID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Items", "tblItems", NewID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function
        Public Function getById(id) As DataSet
            Dim StrQry As String = ""
            StrQry = "Select  * FROM tblItems WHERE PK_Item = " & id & " and CompanyID = " & HttpContext.Current.Session("CompanyID") & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

    End Class
End Namespace