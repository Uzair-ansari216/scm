﻿Imports System.Reflection

Namespace BizSoft.Bridge_SCM
    Public Class clsCountry
        Inherits Logic
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10), countryID) ID, countryCode,countryName from tblCountry where CompanyID = " + HttpContext.Current.Session("CompanyID") + " Order by countryName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetById(ByVal id As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from tblCountry where countryid = " & id & " and CompanyID = " + HttpContext.Current.Session("CompanyID") + ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function Update(ByVal ID As String, ByVal Countrycode As String, ByVal Countryname As String, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "Select * from tblCountry Where countryCode='" & Countrycode & "' and countryID <>  '" & ID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                StrQry = "Update tblCountry set countryCode = '" & Countrycode & "',countryName = '" & Countryname & "' where countryID = " & ID & ""
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Edit Recored Country"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Country", "tblCountry", ID, Environment.MachineName)

                    Success = True
                    Message = "update"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Country", "tblCountry", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function

        Public Function Delete(ByVal ID As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            ' Dim ds As DataSet
            'StrQry = "select * from tblCountry where countryID = '" & ID & "'"
            'ds = BizSoft.DBManager.GetDataSet(StrQry)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    Success = False
            '    Message = "exist"
            '    Return Message
            'End If
            Try
                StrQry = "Delete from tblCountry where countryID = " & ID & ""
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Delete Recored Country"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Country", "tblCountry", ID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
                Return Message = ""
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Country", "tblCountry", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
            End Try

        End Function

        Public Function Add(ByVal ID As String, ByVal Countrycode As String, ByVal Countryname As String, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "Select * from tblCountry Where countryCode ='" & Countrycode & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                Dim company = HttpContext.Current.Session("CompanyID")
                StrQry = "Insert into tblCountry(countryCode,countryName,CompanyID) values('" & Countrycode & "','" & Countryname & "'," & company & ")Select @@IDENTITY"

                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    NewID = ds.Tables(0).Rows(0)(0).ToString()
                    Dim iAction As String = ""
                    iAction = "Add New Recored Country"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Country", "tblCountry", NewID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Country", "tblCountry", NewID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try

        End Function

    End Class
End Namespace