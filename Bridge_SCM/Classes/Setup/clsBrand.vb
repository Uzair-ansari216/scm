﻿Imports System.Reflection

Namespace BizSoft.Bridge_SCM

    Public Class clsBrand
        Inherits Logic
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),SegmentID ) ID, SegmentName,SegmentCode  from BusinessSegment where CompanyID = " + HttpContext.Current.Session("CompanyID") + "  Order by SegmentName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetById(ByVal id As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from BusinessSegment where SegmentID = " & id & " and CompanyID = " + HttpContext.Current.Session("CompanyID") + ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function Update(ByVal ID As String, ByVal SegmentCode As String, ByVal SegmentName As String, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "Select * from BusinessSegment Where SegmentCode='" & SegmentCode & "' and SegmentID <>  '" & ID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                StrQry = "Update BusinessSegment set SegmentCode = '" & SegmentCode & "', SegmentName='" & SegmentName & "' where SegmentID = " & ID & ""
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Edit Recored Business Segment, Name = " & SegmentName
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Business Segment", "BusinessSegment", ID, Environment.MachineName)

                    Success = True
                    Message = "update"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Business Segment", "BusinessSegment", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try

        End Function

        Public Function Delete(ByVal ID As String, ByVal Name As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "select * from BusinessSegment where SegmentCode = '" & ID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If

                StrQry = "Delete from BusinessSegment where SegmentID = " & ID & ""
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Delete Recored Business Segment Name = " & Name
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Business Segment", "BusinessSegment", ID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If

            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Business Segment", "BusinessSegment", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
            End Try
        End Function

        Public Function Add(ByVal ID As String, ByVal SegmentCode As String, ByVal SegmentName As String, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            Try
                StrQry = "Select * from BusinessSegment Where SegmentCode ='" & SegmentCode & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                StrQry = "Insert into BusinessSegment(SegmentCode,SegmentName,CompanyID) values('" & SegmentCode & "','" & SegmentName & "'," & CompanyID & ")Select @@IDENTITY"

                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    NewID = ds.Tables(0).Rows(0)(0).ToString()
                    Success = True
                    Message = "success"
                    Dim iAction As String = ""
                    iAction = "Add new Recored Business Segment, Name = " & SegmentName
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Business Segment", "BusinessSegment", NewID, Environment.MachineName)
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Business Segment", "BusinessSegment", "", Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try
        End Function
    End Class

End Namespace