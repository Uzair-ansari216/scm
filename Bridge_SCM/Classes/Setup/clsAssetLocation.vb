﻿Imports System.Reflection

Namespace BizSoft.Bridge_SCM
    Public Class clsAssetLocation
        Dim dataSet As DataSet
        Dim queryString As String = ""
        Public Function GetAssetLocations() As DataSet
            queryString = "SELECT dbo.GS_FA_Location.ID, dbo.GS_FA_Location.Asset_Location, dbo.GS_FA_Location.Description, dbo.Locations.Location, dbo.Departments.DepartmentName" &
                          " FROM dbo.Locations INNER JOIN" &
                         " dbo.GS_FA_Location ON dbo.Locations.LocationID = dbo.GS_FA_Location.LocationID INNER JOIN" &
                         " dbo.Departments On dbo.GS_FA_Location.DepartmentID = dbo.Departments.DepartmentID where GS_FA_Location.CompanyID = " & HttpContext.Current.Session("CompanyID") & " ORDER BY Asset_Location "
            dataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return dataSet
        End Function
        Public Function GetById(Optional ByVal id As String = "0") As DataSet
            queryString = "Select * from GS_FA_Location where ID = " & id & ""
            dataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return dataSet
        End Function
        Public Function Add(ByVal assetLocationVm As AssetLocationModel, ByRef status As Boolean, ByRef message As String, ByRef newId As Long) As String
            Try
                queryString = "Insert into GS_FA_Location(Asset_Location,Description,CompanyID,LocationID,DepartmentID,Creation_Date,Created_By,) values('" & assetLocationVm.AssetLocation & "','" & assetLocationVm.Description & "'," & HttpContext.Current.Session("CompanyID") & "," & assetLocationVm.Location & "," & assetLocationVm.Department & ",convert(date,getdate())," & HttpContext.Current.Session("UserID") & ")Select @@IDENTITY"
                dataSet = BizSoft.DBManager.GetDataSet(queryString)
                If dataSet.Tables(0).Rows.Count > 0 Then
                    newId = dataSet.Tables(0).Rows(0)(0).ToString()
                    status = True
                    message = "Asset Location save successfully"
                    Dim iAction As String = ""
                    iAction = "Add new Recored Asset Location, Location = " & assetLocationVm.AssetLocation
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Asset Location", "GS_FA_Location", newId, Environment.MachineName)
                    Return message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, queryString, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Asset Location", "GS_FA_Location", "", Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                status = False
                message = ex.Message
                Return message
            End Try
        End Function

        Public Function Update(ByVal assetLocationVm As AssetLocationModel, ByRef status As Boolean, ByRef Message As String) As String
            Try

                queryString = "Update GS_FA_Location set Asset_Location = '" & assetLocationVm.AssetLocation & "', Description='" & assetLocationVm.Description & "',LocationID = " & assetLocationVm.Location & ", DepartmentID = " & assetLocationVm.Department & ", Updated_By = " & HttpContext.Current.Session("UserID") & ", Updation_date = convert(date,getdate()) where ID = " & assetLocationVm.Id & ""
                If BizSoft.DBManager.ExecuteQry(queryString) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Edit Recored Asset Location, Asset location = " & assetLocationVm.AssetLocation
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Asset Location", "GS_FA_Location", assetLocationVm.Id, Environment.MachineName)

                    status = True
                    Message = "Asset Location save successfully"
                    Return Message
                Else
                    status = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, queryString, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Asset Location", "GS_FA_Location", assetLocationVm.Id, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                status = False
                Message = ex.Message
                Return Message
            End Try

        End Function
        Public Function Delete(ByVal id As Int32, ByVal title As String, ByRef status As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            Dim ds As DataSet
            Try

                StrQry = "Delete from GS_FA_Location where ID = " & id & ""
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Delete Recored Asset Location Asset Location = " & title
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Asset Location", "GS_FA_Location", id, Environment.MachineName)

                    status = True
                    Message = "Asset Location delete successfully"
                    Return Message
                Else
                    status = False
                    Message = "failed"
                    Return Message
                End If

            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Asset Location", "GS_FA_Location", id, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                status = False
                Message = ex.Message
            End Try
        End Function
    End Class

End Namespace
