﻿Imports System.Data.SqlClient
Imports System.Reflection

Namespace BizSoft.Bridge_SCM
    Public Class Voucher

        Inherits Logic
        Dim objAdb As New Data.SqlClient.SqlDataAdapter()
        Dim objDs As New DataSet()
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim VoucherDate As String = HttpContext.Current.Session("WorkingDate")
            StrQry = "select * from VoucherMaster where VoucherDate ='" & VoucherDate & "' and vouchertypeid = 'jv'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetLocation() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "  SELECT Location, cityName, LocationID FROM VWGUI_Location_CityCountry WHERE CompanyID = " & CompanyID & "  ORDER BY Location"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetLocation(ID As String) As String
            Dim StrQry As String = ""
            Dim Name As String
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            If ID <> "" Then
                StrQry = "  SELECT Location FROM VWGUI_Location_CityCountry WHERE LocationID = " & ID & "  "
                Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Name = ds.Tables(0).Rows(0)(0).ToString
                    Return Name
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Function
        Public Function GetDepartment(ID As String) As String
            Dim StrQry As String = ""
            Dim Name As String
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            If ID <> "" Then
                StrQry = " SELECT DepartmentName FROM Departments WHERE DepartmentID ='" & ID & "'"
                Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Name = ds.Tables(0).Rows(0)(0).ToString
                    Return Name
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Function
        Public Function GetProjects(ID As String) As String
            Dim StrQry As String = ""
            Dim Name As String
            If ID <> "" Then
                StrQry = " SELECT ProjectTitle FROM Projects WHERE ProjectID = '" & ID & "'"
                Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Name = ds.Tables(0).Rows(0)(0).ToString
                    Return Name
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Function
        Public Function GetEmployees(ID As String) As String
            Dim StrQry As String = ""
            Dim Name As String
            If ID <> "" Then
                StrQry = "SELECT (LTrim(FirstName) + ' ' + LTrim(MiddleName) + LTrim(LastName)) AS EmployeeName FROM Employees WHERE EmployeeID = '" & ID & "' "
                Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Name = ds.Tables(0).Rows(0)(0).ToString
                    Return Name
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Function
        Public Function GetBrand(ID As String) As String
            Dim StrQry As String = ""
            Dim Name As String
            If ID <> "" Then
                StrQry = "SELECT SegmentName FROM BusinessSegment WHERE SegmentID = '" & ID & "'"
                Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Name = ds.Tables(0).Rows(0)(0).ToString
                    Return Name
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Function
        Public Function GetProjects() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "  SELECT ProjectTitle,ProjectID FROM Projects WHERE CompanyID = " & CompanyID & " ORDER BY ProjectTitle"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetEmployees() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "SELECT (LTrim(FirstName) + ' ' + LTrim(MiddleName) + LTrim(LastName)) AS EmployeeName,EmployeeID FROM Employees WHERE CompanyID = " & CompanyID & " AND StatusID <> 109 ORDER BY EmployeeName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function FindData(ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT        dbo.VoucherMaster.VoucherID, dbo.VoucherMaster.VoucherTypeID, dbo.Supplier.SupplierID, dbo.Customer.CustomerID, dbo.Customer.StrCustomerID, " &
                        " dbo.Customer.Company AS CustomerCompany, dbo.Supplier.Company AS SupplierCompany" &
" FROM            dbo.Supplier RIGHT OUTER JOIN" &
                        " dbo.VoucherMaster ON dbo.Supplier.SupplierID = dbo.VoucherMaster.PayToOrReceivedFrom LEFT OUTER JOIN" &
                        " dbo.Customer ON dbo.VoucherMaster.PayToOrReceivedFrom = dbo.Customer.StrCustomerID" &
" WHERE        (dbo.VoucherMaster.VoucherID = " & ID & ")"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetCustomerDetail(ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = " select Address from Customer where CustomerID = " & ID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetDepartment() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "   SELECT DepartmentName,DepartmentID FROM Departments WHERE CompanyID = " & CompanyID & " ORDER BY DepartmentName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetVoucherType() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from VoucherTypes where IsPublic = 1 Order By Sorton"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function AccountCode() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "  exec WgetCharofAccount " & CompanyID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetBrand() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "   SELECT SegmentName, SegmentID FROM BusinessSegment WHERE CompanyID = " & CompanyID & " ORDER BY SegmentName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetDebtorList() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "SELECT * FROM vGetCustomers WHERE StatusID <> 205  AND CompanyID = " & CompanyID & " order by Company"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetCreditorList() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "SELECT * FROM vGetSuppliers WHERE StatusID <> 155 AND CompanyID = " & CompanyID & " ORDER BY Company"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        ' New Methods
        Public Function Update(model As VoucherModel, ByRef Success As Boolean, ByRef Message As String, ByRef command As SqlCommand) As Tuple(Of String, Integer)
            Dim StrQry As String = ""
            Dim StrQry2 As String = ""
            'Try
            StrQry = "update VoucherMaster set VoucherTypeID = '" & model.VoucherTypeID & "',VoucherDate ='" & model.VoucherDate & "' ,DueDate = '" & model.DueDate & "',PayToOrReceivedFrom = '" & Replace(model.PayToOrReceivedFrom, "'", "''") & "',Description = '" & Replace(model.Description, "'", "''") & "',ReferenceNo = '" & model.ReferenceNo & "',ReferenceDate = '" & model.ReferenceDate & "' where VoucherID = " & model.ID & ""
            command.CommandType = CommandType.Text
            command.CommandText = StrQry

            Dim returnValue As Integer = command.ExecuteNonQuery() 'BizSoft.DBManager.ExecuteQry(StrQry)
            If returnValue = 1 Then
                Message = "update"
                Dim iAction As String = ""
                iAction = "Edit Recored General Voucher"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "General Voucher", "VoucherMaster", model.ID, Environment.MachineName)
            Else
                Message = "failed"
            End If
            Return Tuple.Create(CStr(Message), CInt(0))
            'Catch ex As Exception
            '    BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "General Voucher", "VoucherMaster", model.ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
            '    Message = ex.Message
            '    Return Tuple.Create(CStr(Message), CInt(0))
            'End Try
        End Function
        Public Function createVoucherLog(ByVal id As String, ByRef command As SqlCommand) As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "spCreateVoucherHistory " & CompanyID & "," & id & ""
            command.CommandText = StrQry
            objAdb.SelectCommand = command
            objAdb.Fill(objDs)
            Dim ds As DataSet = objDs 'BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function Add2(model As VoucherModel, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long, ByRef command As SqlCommand) As Tuple(Of String, Integer)
            Dim StrQry As String = ""
            Dim StrQry2 As String = ""
            Dim voucherNo As String = ""
            Dim ds As New DataSet
            'Try
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            Dim UserID As String = HttpContext.Current.Session("UserID")
            voucherNo = Val(GetLastVoucherNo(model.VoucherTypeID, FormatDateTime(model.VoucherDate, DateFormat.ShortDate))) + 1

            StrQry = "Insert into VoucherMaster(VoucherTypeID,VoucherNo,VoucherDate,DueDate,PayToOrReceivedFrom,Description,DateCreated,StatusID,UserID,CompanyID,ReferenceNo,ReferenceDate) values('" & model.VoucherTypeID & "','" & voucherNo & "','" & model.VoucherDate & "','" & model.DueDate & "','" & Replace(model.PayToOrReceivedFrom, "'", "''") & "','" & Replace(model.Description, "'", "''") & "',getDate(),'2000','" & UserID & "'," & CompanyID & ",'" & model.ReferenceNo & "','" & model.ReferenceDate & "') Select @@IDENTITY"

            'updated work
            command.CommandType = CommandType.Text
            command.CommandText = StrQry
            objAdb.SelectCommand = command
            objAdb.Fill(objDs)

            ds = objDs 'ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                NewID = ds.Tables(0).Rows(0)(0).ToString
                'Return Insert
            End If

            StrQry = "[SpSetDefultCurrencyRate] " & NewID & ", 0"
            command.CommandText = StrQry
            objAdb.SelectCommand = command
            objAdb.Fill(objDs) 'ds = BizSoft.DBManager.GetDataSet(StrQry)

            If NewID > 0 Then
                Dim iAction As String = ""
                iAction = "Add new Recored General Voucher"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "General Voucher", "VoucherMaster", NewID, Environment.MachineName)
                Message = "success"
            Else
                Message = "failed"
            End If
            Return Tuple.Create(CStr(Message), CInt(voucherNo))
            'Catch ex As Exception
            '    BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "General Voucher", "VoucherMaster", "", Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
            '    Message = ex.Message
            '    Return Tuple.Create(CStr(ex.Message), CInt(voucherNo))
            'End Try
        End Function
        Public Function GetLastVoucherNo(VoucherTypeID As String, VoucherDate As Date) As Double
            'On Error GoTo ErrorCode
            ' Dim dt As Date = Convert.ToDateTime(VoucherDate)
            Try
                Dim ds As New DataSet
                Dim LastVoucher As Integer
                Dim parameters(3) As SqlParameter
                parameters(0) = New SqlParameter("@VoucherTypeID", SqlDbType.VarChar, 5) With {.Value = VoucherTypeID}
                parameters(1) = New SqlParameter("@VoucherDate", SqlDbType.DateTime) With {.Value = VoucherDate}
                parameters(2) = New SqlParameter("@CompanyID", SqlDbType.Int) With {.Value = HttpContext.Current.Session("CompanyID")}
                'parameters(3) = New SqlParameter("@LocationID", SqlDbType.Int) With {.Value = HttpContext.Current.Session("LocationID")}
                parameters(3) = New SqlParameter("@LastVNo", SqlDbType.Int) With {.Direction = ParameterDirection.Output}
                BizSoft.DBManager.ExecuteSP("spReturnLastVoucherNo", parameters)
                LastVoucher = parameters(3).Value
                Return LastVoucher
                Exit Function
            Catch ex As Exception
                Return ex.ToString
            End Try
            'ErrorCode:
            '  MessageBox.Show("Error in Getting Voucher No")
            'Resume
        End Function
        Public Function GetMembers() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select ID,Fname from Gs_Member Order by ID"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetMemberType() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select ID,Description from Gs_MemberType Order by ID"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetDealerRecord() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "SELECT CustomerID as ID, Company as Name FROM Customer WHERE companyID = " & CompanyID & " And StatusID <> 205 ORDER BY Company"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function AddDetail2(VoucherID As String, AccountCode As String, Particulars As String, LocationID As String, DepartmentID As String, ProjectID As String, EmployeeID As String, Debit As String, Credit As String, SegmentID As String, ByRef command As SqlCommand) As String
            Dim StrQry As String = ""
            Dim Message As String
            Dim NewID As Integer
            'Try
            StrQry = "select Isnull(Max(GeneralID),0) + 1 from VoucherDetail where VoucherID = " & VoucherID & ""
            Dim ds As New DataSet
            'updated word

            command.CommandText = StrQry
            objAdb.SelectCommand = command
            objAdb.Fill(objDs)
            ds = objDs
            'ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                NewID = ds.Tables(0).Rows(0)(0).ToString
                'Return Insert
            End If

            StrQry = "Insert into VoucherDetail(GeneralID,VoucherID,AccountCode,Particulars,LocationID,DepartmentID,ProjectID,EmployeeID,Debit,Credit,SegmentID) values(" & NewID & ",'" & VoucherID & "','" & AccountCode & "','" & Replace(Particulars, "'", "''") & "','" & LocationID & "','" & DepartmentID & "','" & ProjectID & "','" & EmployeeID & "','" & Debit & "','" & Credit & "','" & SegmentID & "')"
            command.CommandText = StrQry
            objAdb.SelectCommand = command

            Dim returnValue As Integer = command.ExecuteNonQuery() 'BizSoft.DBManager.ExecuteQry(StrQry)
            If returnValue = 1 Then
                Dim iAction As String = ""
                iAction = "Add new Recored Voucher Detail"
                BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Voucher Detail", "VoucherDetail", NewID, Environment.MachineName)
                Message = "success"
            Else
                Message = "failed"
            End If
            Return Message
            'Catch ex As Exception
            '    BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Voucher Detail", "VoucherDetail", "", Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
            '    Message = ex.Message
            '    Return Message
            'End Try
        End Function

        Public Function ItemList() As System.Data.DataSet
            Dim StrQry As String = ""

            StrQry = "  SELECT * FROM INVGUI_ProductInformation where companyID = " & HttpContext.Current.Session("CompanyID") & " and  AvailableForSO = 1 ORDER BY ModelNumber, PartNumber"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetItemDetail(ID As String) As System.Data.DataSet
            Dim StrQry As String = ""

            StrQry = "  select *,'' as LocationName,'' as AccountName,'' as DepartmentName,'' as ProjectName,'' as EmployeeName,'' as SegmentName  from VoucherDetail where VoucherID = " & ID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function DeleteDetail2(ByVal ID As String, ByRef command As SqlCommand) As String

            Dim StrQry As String = ""
            'Try
            'Dim ds As DataSet
            'StrQry = "select * from GS_ProjectTaskType where FK_ProjectTypeID = '" & ID & "'"
            'ds = BizSoft.DBManager.GetDataSet(StrQry)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    Success = False
            '    Message = "exist"
            '    Return Message
            'End If

            StrQry = "Delete from VoucherDetail where VoucherID = " & ID & ""
            command.CommandText = StrQry
            objAdb.SelectCommand = command
            objAdb.Fill(objDs)
            'BizSoft.DBManager.ExecuteQry(StrQry)
            'Dim iAction As String = ""
            'iAction = "Delete Voucher Detail"
            'BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Voucher Detail", "VoucherDetail", ID, Environment.MachineName)
            Return ""
            'Catch ex As Exception
            '    BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Voucher Detail", "VoucherDetail", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
            '    Return ex.Message
            'End Try

        End Function
        Public Function GetStkQtyLoc(ByVal item_id As Integer, ByVal ToDate As Object, ByVal LocationID As Object, ByVal CurrentTransID As Object) As Integer
            Dim Result As New DataSet
            Dim nReceived
            Dim nIssued
            Dim strSQL As String
            Dim Stock As Integer
            If Not ToDate = "" Then
                'strSQL = "select isnull(sum(quantity),0) as quantity from Ps_stock where stock_type = 1 and item_id = " & item_id & " and DOC_DATE <= '" & FormatDateTime(ToDate, vbShortDate) & "' and  (Docname = '1 - Supplier - Purchases' OR Docname = 'OPENING' OR Docname = '2 - Dealer - Sales Return' ) "
                strSQL = "select isnull(sum(quantity),0) as quantity from Ps_stock where stock_type = 1 and item_id = " & item_id & " and DOC_DATE <= '" & FormatDateTime(ToDate, vbShortDate) & "' and Wh_id = " & LocationID
            Else
                strSQL = "select isnull(sum(quantity),0) as quantity from Ps_stock where stock_type = 1  and item_id = " & item_id & " And Wh_id = " & LocationID
            End If

            'rs.CursorLocation = adUseClient
            Result = BizSoft.DBManager.GetDataSet(strSQL)   'cnDataBase, adOpenForwardOnly, adLockReadOnly
            If Result.Tables(0).Rows(0)("quantity") > 0 Then
                nReceived = Result.Tables(0).Rows(0)("quantity")
            Else
                nReceived = 0
            End If




            If Not ToDate = "" Then
                'strSQL = "select isnull(sum(quantity),0) as quantity from Ps_stock where stock_type = 2 and item_id = " & item_id & " and DOC_DATE <= '" & FormatDateTime(ToDate, vbShortDate) & "' and DocName = '2 - Dealer - Sales' "
                strSQL = "select isnull(sum(quantity),0) as quantity from Ps_stock where stock_type = 2 and item_id = " & item_id & " and DOC_DATE <= '" & FormatDateTime(ToDate, vbShortDate) & "' and Wh_id = " & LocationID & " and Source_doc_number <> " & CurrentTransID
            Else
                strSQL = "select isnull(sum(quantity),0) as quantity from Ps_stock where stock_type = 2 and item_id = " & item_id & " And Wh_id = " & LocationID & " and Source_doc_number <> " & CurrentTransID
            End If

            'strsql = "select isnull(sum(quantity),0) as quantity from stock where stock_type = 0 and item_id = " & item_id
            'strsql = "select isnull(sum(quantity),0) as quantity from issuenote_detail, issuenote_header where issuenote_header.id=issuenote_detail.id and item_id=" & item_id

            'rs.CursorLocation = adUseClient
            'rs.Open strSQL, cnDataBase, adOpenForwardOnly, adLockReadOnly
            Result = BizSoft.DBManager.GetDataSet(strSQL)
            If Result.Tables(0).Rows(0)("quantity") > 0 Then
                nIssued = Result.Tables(0).Rows(0)("quantity")
            Else
                nIssued = 0
            End If
            'GetStkQty = IIf((nReceived - nIssued) > 0, (nReceived - nIssued), 0)
            Stock = nReceived - nIssued
            Return Stock
        End Function
        Public Function GetAccountName(ID As String) As String
            Dim StrQry As String = ""
            Dim Name As String
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            If ID <> "" Then
                StrQry = "Select AccountName,AccountCode  FROM vChartOfAccount  " &
" WHERE levels = 5 and CompanyID = " & CompanyID & " AND AccountCode NOT IN (SELECT DISTINCT ParentAccountCode FROM vChartOfAccount WHERE CompanyID =  " & CompanyID & " )" &
" and AccountCode = " & ID & ""
                Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Name = ds.Tables(0).Rows(0)(0).ToString
                    Return Name
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Function
        Public Function GetList2(model As VoucherModel) As System.Data.DataSet
            Dim StrQry As String = ""

            If model.VoucherNo = "" Then
                StrQry = " select * from VoucherMaster  where VoucherDate = '" & model.VoucherDate & "' and vouchertypeid = 'jv'"
            Else
                StrQry = "  select * from VoucherMaster  where VoucherDate ='" & model.VoucherDate & "' and VoucherNo = " & model.VoucherNo & " and vouchertypeid = 'jv'"
            End If

            'StrQry = "select * from VoucherMaster where VoucherTypeID = '" & AccountTypeID & "' and VoucherDate = '" & VoucherDate & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetAccountName2(AccountCode As String) As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")

            StrQry = "SELECT TOP 1 AccountCode        , AccountName    FROM  vchartOfAccount" &
            " WHERE" &
                    " CompanyID = " & CompanyID & "" &
            " AND AccountCode NOT IN (SELECT ParentAccountCode FROM vchartOfAccount WHERE CompanyID = " & CompanyID & ") " &
            " and AccountCode Like '%" & AccountCode & "%'       " &
          " ORDER BY AccountName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetAccountCode2(AccountName As String) As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")

            StrQry = " SELECT TOP 1 AccountCode  , AccountName FROM" &
            " vchartOfAccount" &
            " WHERE" &
            " CompanyID = " & CompanyID & "" &
    " AND AccountCode NOT IN (SELECT ParentAccountCode FROM vchartOfAccount WHERE CompanyID = " & CompanyID & ") " &
    " and AccountName Like '%" & AccountName & "%'      " &
   " ORDER BY AccountName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetVoucherSupportById(ByVal id As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from VoucherDetailSupport where voucherId = " & id & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetCurrency() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select PK_CurrencyID as ID, Currency +(' ')+Symbol as Currency from tblcurrency"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function SaveVoucherSupport(model As VoucherSupportModel, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As Tuple(Of String, Integer)
            Dim StrQry As String = ""
            Dim StrQry2 As String = ""
            Dim voucherNo As String = ""
            Dim ds As New DataSet
            Try

                StrQry = "select * from VoucherDetailSupport where VoucherID = " & model.VoucherId & " and CurrencyID = " & model.Currency & ""
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    StrQry = "update VoucherDetailSupport set VoucherID = " & model.VoucherId & ",AccountCode = '" & model.AccountCode & "',CurrencyID =  " & model.Currency & ",CCRate =" & model.Rate & ",OrderID = " & model.OrderId & ",OrderQty = " & model.OrderQuantity & ",OrderRate = " & model.OrderRate & ",ExpenseDate = '" & model.ExpenseDate & "',InvoiceReceived = '" & model.InvoiceReceived & "',ExpCurrencyID = " & model.ExpenseCurrency & " where ID = " & model.Id & ""
                Else
                    StrQry = "insert into VoucherDetailSupport(VoucherID,AccountCode,CurrencyID,CCRate,OrderID,OrderQty,OrderRate,ExpenseDate,InvoiceReceived,ExpCurrencyID)" &
                    " values(" & model.VoucherId & ",'" & model.AccountCode & "'," & model.Currency & "," & model.Rate & "," & model.OrderId & "," & model.OrderQuantity & "," & model.OrderRate & ",'" & model.ExpenseDate & "','" & model.InvoiceReceived & "'," & model.ExpenseCurrency & ")Select @@IDENTITY"
                End If

                ds = BizSoft.DBManager.GetDataSet(StrQry)
                NewID = If(model.Id <> "", model.Id, ds.Tables(0).Rows(0)(0).ToString)

                If NewID > 0 Then
                    Dim iAction As String = ""
                    iAction = "Add new Recored General Voucher Support"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "General Voucher Support", "VoucherDetailSupport", NewID, Environment.MachineName)
                    Message = "success"
                Else
                    Message = "failed"
                End If
                Return Tuple.Create(CStr(Message), CInt(NewID))
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "General Voucher", "VoucherMaster", "", Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Message = ex.Message
                Return Tuple.Create(CStr(ex.Message), CInt(NewID))
            End Try
        End Function

        Public Function DeleteVoucher(ByVal id As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "spDeleteTheVoucher " & HttpContext.Current.Session("CompanyID") & "," & id & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
    End Class
End Namespace