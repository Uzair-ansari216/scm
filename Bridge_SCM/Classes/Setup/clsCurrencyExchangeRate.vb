﻿Imports System.Reflection

Namespace BizSoft.Bridge_SCM
    Public Class clsCurrencyExchangeRate
        Dim dataSet As DataSet
        Dim queryString As String = ""
        Public Function GetCurrencies() As DataSet
            queryString = "SELECT PK_CurrencyID, Currency + '(' + Symbol + ')' As Currency FROM tblCurrency ORDER BY DefaultCurrency desc"
            dataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return dataSet
        End Function
        Public Function GetById(Optional ByVal id As String = "0") As DataSet
            queryString = "select * from tblCurrency_Rate where FK_CurrencyID = " & id & ""
            dataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return dataSet
        End Function
        Public Function GetMaxDate() As DataSet
            queryString = "select FORMAT( max(dated + 1),'MM/dd/yyyy') as Date from tblCurrency_Rate"
            dataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return dataSet
        End Function

        Public Function Save(model As CurrencyExchangeRateModel, ByRef Message As String, ByRef Success As Boolean, ByRef NewID As Long) As String
            Try
                Dim StartDate As DateTime = Convert.ToDateTime(model.FromDate)
                Dim EndDate As DateTime = Convert.ToDateTime(model.ToDate)

                Dim difference As Int32 = (EndDate - StartDate).TotalDays.ToString()

                For index = 1 To (difference + 1)
                    queryString = "select * From tblCurrency_Rate where Fk_CurrencyID = " & model.Currency & " and  Dated  ='" & model.FromDate & "'"
                    dataSet = BizSoft.DBManager.GetDataSet(queryString)
                    If dataSet.Tables(0).Rows.Count > 0 Then
                        NewID = dataSet.Tables(0).Rows(0)("ID")
                        queryString = "update tblCurrency_Rate set Dated = '" & dataSet.Tables(0).Rows(0)("Dated") & "',CurrencyRate = " & model.ExchangeRate & ",CreateOn = '" & dataSet.Tables(0).Rows(0)("CreateOn") & "', UpdatedOn = CONVERT(date, getdate()) where ID = " & dataSet.Tables(0).Rows(0)("ID") & ""
                        dataSet = BizSoft.DBManager.GetDataSet(queryString)
                        If NewID > 0 Then
                            Dim iAction As String = ""
                            iAction = "Update Recored Exchange Rate = " & model.ExchangeRate
                            BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Currency Exchange Rate", "tblCurrency_Rate", NewID, Environment.MachineName)
                        End If
                    Else
                        queryString = "insert into tblCurrency_Rate(Dated,CurrencyRate,FK_CurrencyID,CreateOn) values('" & model.FromDate & "'," & model.ExchangeRate & "," & model.Currency & ",CONVERT(date, getdate()))select @@IDENTITY"
                        dataSet = BizSoft.DBManager.GetDataSet(queryString)
                        If dataSet.Tables(0).Rows.Count > 0 Then
                            NewID = dataSet.Tables(0).Rows(0)("Column1")
                            Dim iAction As String = ""
                            iAction = "Add New Recored Exchange Rate = " & model.ExchangeRate
                            BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Currency Exchange Rate", "tblCurrency_Rate", NewID, Environment.MachineName)
                        End If
                    End If
                    StartDate = StartDate.AddDays(1)
                    model.FromDate = StartDate
                Next

                If NewID > 0 Then
                    Message = "success"
                Else
                    Message = "failed"
                End If
                Return Message
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, queryString, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Currency Exchange Rate", "tblCurrency_Rate", NewID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try
        End Function
        Public Function GetRatesByDate(ByVal fromDate As String, ByVal toDate As String, ByVal currency As String) As DataSet
            queryString = "select tblCurrency_Rate.ID, FORMAT(tblCurrency_Rate.dated,'dd-MMMM-yyyy') as date, Currency + '(' + Symbol + ')' As Currency,tblCurrency_Rate.CurrencyRate from tblCurrency_Rate inner join tblCurrency on tblCurrency_Rate.fk_currencyid = tblCurrency.PK_CurrencyID where dated between '" & fromDate & "' and '" & toDate & "' and tblCurrency_Rate.fk_currencyid = " & currency & " order by tblCurrency_Rate.dated"
            dataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return dataSet
        End Function
        Public Function UpdateRate(ByVal id As String, ByVal rateDate As String, ByVal rate As String) As DataSet
            queryString = "update tblCurrency_Rate set CurrencyRate = " & rate & " where ID = " & id & " and Dated = '" & rateDate & "'"
            dataSet = BizSoft.DBManager.GetDataSet(queryString)
            Return dataSet
        End Function



    End Class

End Namespace
