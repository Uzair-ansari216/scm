﻿Imports System.Reflection

Namespace BizSoft.Bridge_SCM

    Public Class clsCurrency
        Inherits Logic
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),PK_CurrencyID ) ID, Currency,Symbol,Unit,Description,DefaultCurrency  from tblCurrency where CompanyID = " + HttpContext.Current.Session("CompanyID") + " Order by Currency"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetById(ByVal id As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from tblcurrency where PK_CurrencyID = '" & id & "' and CompanyID = " + HttpContext.Current.Session("CompanyID") + ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function Update(ByVal ID As String, ByVal Currency As String, ByVal Symbol As String, ByVal Unit As String, ByVal Description As String, ByVal DefaultCurrency As String, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "Select * from tblcurrency Where Currency = '" & Currency & "' and PK_CurrencyID <>  '" & ID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                If ID <> "-1" Then

                    StrQry = "Update tblcurrency set Currency = '" & Currency & "', Symbol = '" & Symbol & "', Unit = '" & Unit & "',Description = '" & Description & "',DefaultCurrency = '" & DefaultCurrency & "' where PK_CurrencyID = " & ID & ""
                Else
                    'StrQry = "Update tblcurrency set Fk_ItemClass = " & Fk_ItemClass & ", ItemCategory = '" & ItemCategory & "', Description = '" & Description & "',isActive = '" & isActive & "' where Pk_ItemCategory = " & ID & ""
                End If

                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Edit Recored tblcurrency"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Currency", "tblcurrency", ID, Environment.MachineName)

                    Success = True
                    Message = "update"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Currency", "tblcurrency", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function

        Public Function Delete(ByVal ID As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "select * from tblcurrency where Currency = '" & ID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If

                StrQry = "Delete from tblcurrency where PK_CurrencyID = " & ID & ""
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Delete Recored Currency"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Currency", "tblcurrency", ID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If

            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Currency", "tblcurrency", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
            End Try

        End Function

        Public Function Add(ByVal ID As String, ByVal Currency As String, ByVal Symbol As String, ByVal Unit As String, ByVal Description As String, ByVal DefaultCurrency As String, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "Select * from tblcurrency Where Currency = '" & Currency & "' AND  PK_CurrencyID = '" & ID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                If ID <> "-1" Then
                    Dim company = HttpContext.Current.Session("CompanyID")
                    StrQry = "Insert into tblcurrency(Currency,Symbol,Unit,Description,DefaultCurrency,CompanyID) values('" & Currency & "','" & Symbol & "','" & Unit & "','" & Description & "','" & DefaultCurrency & "'," & company & ")Select @@IDENTITY"
                    'Else
                    '    StrQry = "Insert into GS_Resource(Description,Fk_ResourceType) values ('" & Description & "','" & Fk_ResourceType & "')Select @@IDENTITY"
                End If

                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    NewID = ds.Tables(0).Rows(0)(0).ToString()
                    Dim iAction As String = ""
                    iAction = "Add New Recored Currency"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Currency", "tblcurrency", NewID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Currency", "tblcurrency", NewID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try

        End Function
    End Class

End Namespace