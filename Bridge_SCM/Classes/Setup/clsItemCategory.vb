﻿Imports System.Reflection

Namespace BizSoft.Bridge_SCM
    Public Class clsItemCategory
        Inherits Logic
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT  dbo.tblItemCategory.*, dbo.tblItemClass.ItemClass" &
                     " FROM   dbo.tblItemClass " &
                     "INNER JOIN dbo.tblItemCategory ON dbo.tblItemClass.PK_ItemClass = dbo.tblItemCategory.FK_ItemClass where dbo.tblItemCategory.CompanyID = " + HttpContext.Current.Session("CompanyID")
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetLists2() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT PK_ItemCategory, ItemCategory FROM INVGUI_GroupWithClass WHERE CompanyID = 1 AND PK_ItemCategory IS NOT NULL ORDER BY ItemCategory"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function


        'Public Function Edit(ByVal ID As String) As System.Data.DataSet
        '    Dim StrQry As String = ""
        '    StrQry = "Select * from tblItemClass where  ID = " & ID & ""
        '    Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
        '    Return ds
        'End Function
        'New Methods
        Public Function Update(ByVal ID As String, ByVal Fk_ItemClass As String, ByVal ItemCategory As String, ByVal Description As String, ByVal isActive As String, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "Select * from tblItemCategory Where ItemCategory = '" & ItemCategory & "' and Pk_ItemCategory <>  '" & ID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                If ID <> "-1" Then

                    StrQry = "Update tblItemCategory set Fk_ItemClass = " & Fk_ItemClass & ", ItemCategory = '" & ItemCategory & "', Description = '" & Description & "',isActive = '" & isActive & "' where Pk_ItemCategory = " & ID & ""
                Else
                    StrQry = "Update tblItemCategory set Fk_ItemClass = " & Fk_ItemClass & ", ItemCategory = '" & ItemCategory & "', Description = '" & Description & "',isActive = '" & isActive & "' where Pk_ItemCategory = " & ID & ""
                End If

                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Edit Recored Item Category"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Item Category", "tblItemCategory", ID, Environment.MachineName)

                    Success = True
                    Message = "update"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Item Category", "tblItemCategory", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function

        Public Function Delete(ByVal ID As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "select * from tblItemCategory where Fk_ItemClass = '" & ID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If

                StrQry = "Delete from tblItemCategory where Pk_ItemCategory = " & ID & ""
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Delete Recored Item Category"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Item Category", "tblItemCategory", ID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If

            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Item Category", "tblItemCategory", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
            End Try

        End Function

        Public Function Add(ByVal ID As String, ByVal Fk_ItemClass As String, ByVal ItemCategory As String, ByVal Description As String, ByVal isActive As String, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            Try
                StrQry = "Select * from tblItemCategory Where ItemCategory = '" & ItemCategory & "' AND Description = '" & Description & "' AND Fk_ItemClass = '" & Fk_ItemClass & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                If ID <> "-1" Then
                    StrQry = "Insert into tblItemCategory(Fk_ItemClass,ItemCategory,Description,isActive,CompanyID) values('" & Fk_ItemClass & "','" & ItemCategory & "','" & Description & "','" & isActive & "','" & CompanyID & "')Select @@IDENTITY"
                    'Else
                    '    StrQry = "Insert into GS_Resource(Description,Fk_ResourceType) values ('" & Description & "','" & Fk_ResourceType & "')Select @@IDENTITY"
                End If

                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    NewID = ds.Tables(0).Rows(0)(0).ToString()
                    Dim iAction As String = ""
                    iAction = "Add New Recored Item Category"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Item Category", "tblItemCategory", NewID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Item Category", "tblItemCategory", NewID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function
    End Class
End Namespace