﻿Namespace BizSoft.Bridge_SCM

    Public Class ReportRoleDefine
        Inherits Logic
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT dbo.GS_Report_Module.Name AS Module, dbo.GS_Report_Category.Description AS Category, dbo.GS_Report_Setup.ReportName AS Report, dbo.GS_Report_Setup.ReportID AS ID FROM dbo.GS_Report_Setup INNER JOIN dbo.GS_Report_Category ON dbo.GS_Report_Setup.FKReportCategory = dbo.GS_Report_Category.ID INNER JOIN dbo.GS_Report_Module ON dbo.GS_Report_Setup.FKReportModule = dbo.GS_Report_Module.Id" '"select * from [dbo].[GS_Report_Setup]"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetListView() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from GL_ROLE_MASTER"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Sub DeleteDetail(ByVal ID As String)
            Dim StrQry As String = ""
            Dim Success As String = ""
            Dim Message As String = ""

            StrQry = "Delete from tbl_Report_Role_Detail where RoleID = '" & ID & "'"

            Try
                BizSoft.DBManager.GetDataSet(StrQry)
                Success = True
                Message = "Role Detail record deleted successfully "
            Catch ex As Exception
                Success = False
                Message = ex.Message
            End Try

        End Sub
        Public Sub AddDetail(ID As String, ReportID As String, isView As Boolean)
            Dim StrQry As String = ""
            Dim Success As String = ""
            Dim Message As String = ""

            StrQry = "Insert into tbl_Report_Role_Detail(RoleID,ReportID,isView) values('" & ID & "','" & ReportID & "','" & isView & "')"

            Try
                BizSoft.DBManager.GetDataSet(StrQry)
                Success = True
                Message = "Role Detail record added successfully "
            Catch ex As Exception
                Success = False
                Message = ex.Message
            End Try
        End Sub
        Public Function GetRoleDetail(ByVal RecID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "  select * from tbl_Report_Role_Detail where RoleID = " & RecID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
    End Class
End Namespace