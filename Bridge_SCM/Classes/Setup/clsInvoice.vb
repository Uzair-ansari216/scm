﻿Namespace BizSoft.Bridge_SCM
    Public Class Invoice

        Inherits Logic

        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT        dbo.Customer.FirstName, dbo.Status.StatusName, dbo.tblSalesOrder.*, dbo.tblSalesOrderStatus.*, dbo.Employees.FirstName AS SalesPerson, " &
                         " dbo.tblSalesOrderStatus.Remarks AS StatusRemarks" &
" FROM            dbo.tblSalesOrder INNER JOIN" &
                         " dbo.Customer ON dbo.tblSalesOrder.FK_CustomerID = dbo.Customer.CustomerID INNER JOIN" &
                         " dbo.tblSalesOrderStatus ON dbo.tblSalesOrder.PK_SalesOrder = dbo.tblSalesOrderStatus.FK_SalesOrder INNER JOIN" &
                         " dbo.Status ON dbo.tblSalesOrderStatus.FK_StatusID = dbo.Status.StatusID INNER JOIN" &
                         " dbo.Employees ON dbo.tblSalesOrder.FK_EmployeeID = dbo.Employees.EmployeeID"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetLocation() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            Dim LocationID As String = HttpContext.Current.Session("LocationId")
            Dim isPowerUser As Boolean = HttpContext.Current.Session("isPowerUser")
            If isPowerUser = True Then

                StrQry = "SELECT LocationID, Location + ' - ' + CityName LocationName  FROM VWGUI_Location_CityCountry where CompanyID =  " & CompanyID & " ORDER BY CityName, Location"
            Else

                StrQry = "SELECT LocationID, Location + ' - ' + CityName LocationName  FROM VWGUI_Location_CityCountry where Locationid = " & LocationID & " and CompanyID =  " & CompanyID & " ORDER BY CityName, Location"

                'CboLocation.Enabled = False
            End If

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function FindData(ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT * FROM tblinvoice where pk_invoice = " & ID & " and CompanyID = " & HttpContext.Current.Session("CompanyID") & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetCustomerDetail(ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = " select Address from Customer where CustomerID = " & ID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetSalesPerson() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "SELECT FirstName, EmployeeID FROM Employees WHERE companyID = " & CompanyID & " and  StatusID <> 109 AND LastName IN (Select Designation From Designations Where Category = 1) ORDER BY FirstName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetStatus() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "  SELECT  StatusID, StatusName FROM Status WHERE StatusID BETWEEN 800 AND 803 ORDER BY StatusID"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetCurrency() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),PK_CurrencyID ) ID, Currency  as Name, Symbol, Unit, Description, DefaultCurrency  from tblCurrency   Order by Currency"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
        ' New Methods
        Public Function Update(model As SalesOrderModel, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim StrQry2 As String = ""
            StrQry = "update tblSalesOrder set SalesDate = '" & model.SalesDate + " " + Now.ToShortTimeString & "',FK_CustomerID ='" & model.FK_CustomerID & "' ,BillTo = '" & model.BillTo & "',ShipTo = '" & model.ShipTo & "',OrderType = '" & model.OrderType & "',FK_EmployeeID = '" & model.FK_EmployeeID & "',FK_LocationID = '" & model.FK_LocationID & "',Remarks = '" & model.Remarks & "',Billing = '" & model.Billing & "',FK_Terms = '" & model.FK_Terms & "',FK_CurrencyID = '" & model.FK_CurrencyID & "',ExchangeRate = '" & model.ExchangeRate & "',LastUpdatedBy = " & HttpContext.Current.Session("CompanyID") & ",LastUpdatedOn= getDate(),Tax = " & model.Tax & ",Charges = " & model.Charges & " where PK_SalesOrder = " & model.ID & ""

            StrQry2 = "update tblSalesOrderStatus set FK_StatusID = '" & model.FK_StatusID & "',StatusDate = '" & model.StatusDate & "',Remarks = '" & model.StatusRemarks & "', UpdatedBy = " & HttpContext.Current.Session("CompanyID") & " where FK_SalesOrder = " & model.ID & " "

            Dim returnValue As Integer = BizSoft.DBManager.ExecuteQry(StrQry)
            Dim returnValue2 As Integer = BizSoft.DBManager.ExecuteQry(StrQry2)
            If returnValue = 1 And returnValue2 = 1 Then
                Message = "update"
            Else
                Message = "failed"
            End If
            Return Message
        End Function

        Public Function Delete(ByVal ID As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            StrQry = "Delete from Gs_Committee where ID = " & ID & ""
            If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                Success = True
                Message = "success"
                Return Message
            Else
                Success = False
                Message = "failed"
                Return Message
            End If
            Return Message
        End Function

        Public Function AutoGeneratedKEY(ByVal l_location As String, ByVal dtSODate As String, ByVal companyID As Integer, ByVal LocationID As Integer) As String

            'KHI-0001/06


            Dim DateF As Date
            Dim DateT As Date
            Dim tempRs As New DataSet
            Dim strSQL As String = ""
            Dim gl_query As String = ""


            strSQL = "select * from GL_FINANCIAL_YEAR where DateFrom <  '" & dtSODate & "' and DateTo > '" & dtSODate & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(strSQL)

            DateF = ds.Tables(0).Rows(0)("DateFrom")
            DateT = ds.Tables(0).Rows(0)("DateTo")
            'DateF = Format(tempRs("DateFrom").value, "dd-MMM-YYYY")
            'DateT = Format(tempRs("DateTo").value, "dd-MMM-YYYY")


            gl_query = "SELECT isNull(max(subString(SalesOrder, 12, 4)), 0) + 1 as maxNO FROM tblSalesOrder WHERE SalesDate BETWEEN '" & FormatDateTime(DateF, DateFormat.ShortDate) & "' AND '" & FormatDateTime(DateT, DateFormat.ShortDate) & " 11:59:59 PM ' and Companyid = " & companyID & " and FK_LocationID = " & LocationID & ""

            'If GL_FinancialID > 3 Then
            '    gl_query = gl_query & " and Companyid = " & GL_companyID & " and FK_LocationID = " & cboLocation.ItemData(cboLocation.ListIndex)
            'End If
            Dim rs As DataSet = BizSoft.DBManager.GetDataSet(gl_query)
            'rs.Open(gl_query, objConn)


            gl_query = " SELECT     dbo.tblCity.cityCode + '-' + dbo.Locations.LocationCode AS LocCode FROM         dbo.Locations INNER JOIN   dbo.tblCity ON dbo.Locations.FK_CityID = dbo.tblCity.cityID where Locations.LocationID =  " & LocationID & ""
            Dim DsLocCode As DataSet = BizSoft.DBManager.GetDataSet(gl_query)

            l_location = l_location & Convert.ToString(DsLocCode.Tables(0).Rows(0)("LocCode"))

            AutoGeneratedKEY = l_location & "-" & Format(rs.Tables(0).Rows(0)("maxNO"), "0000") & "/" & Year(DateF) & "-" & Right(Year(DateT), 2)
            Exit Function

            Resume Next
        End Function
        Public Function Add(model As InvoiceModel, productInformationList As List(Of InvoiceModel), ByRef Message As String) As ResultSetModel
            Dim StrQry As String = ""
            Dim INVId As Int32 = 0
            Dim outPutParam As Int16 = 0
            Dim INVNum As String = ""
            Dim ds As New DataSet
            ' Dim NewID As Integer
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            Dim sLocationCode As String = ""
            'Dim ds2 As DataSet = GetLocationCity(HttpContext.Current.Session("LocationId"))
            'If ds2.Tables(0).Rows.Count > 0 Then
            'sCityCode = ds.Tables(0).Rows(0).Item("cityCode").ToString
            'sLocationCode = ds2.Tables(0).Rows(0)(0).ToString
            'End If

            If model.InvoiceNumber.Trim = "" Then
                StrQry = "exec AutoGeneratedKEY 'SI','" & model.InvoiceDate & "'," & HttpContext.Current.Session("CompanyID") & "," & model.FK_LocationID & ""
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                INVNum = ds.Tables(0).Rows(0)(0).ToString()
            Else
                INVNum = model.InvoiceNumber
            End If
            If model.ID.Trim = "" Then
                INVId = 0
                outPutParam = 0
            Else
                INVId = model.ID
                outPutParam = 1
            End If
            StrQry = "exec Transaction_Invoice " & INVId & ",'" & INVNum & "','" & model.InvoiceDate & "','" & model.InvoiceDueDate & "'," & model.FK_Terms & "," & 1 & "," & model.SalesOrder & "," & model.FK_CustomerID & ",'" & model.BillTo & "','" & model.SalesPerson & "'," & 0 & "," & model.isgst & "," & model.Charges & "," & model.Discount & ",'" & model.Remarks & "','" & String.Empty & "'," & 0 & "," & CompanyID & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & outPutParam & ""
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                model.ID = ds.Tables(0).Rows(0)(0).ToString
            End If

            StrQry = "DELETE FROM tblInvoiceItems WHERE FK_Invoice = " & model.ID
            Dim returnValue As Integer = BizSoft.DBManager.ExecuteQry(StrQry)

            For index = 0 To productInformationList.Count - 1
                StrQry = "exec Transaction_InvoiceItems " & index + 1 & "," & model.ID & "," & productInformationList(index).FK_Item & ",'" & productInformationList(index).DefaultDescription & "'," & productInformationList(index).Quantity & "," & productInformationList(index).UnitPrice & ",'" & productInformationList(index).Remarks & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
            Next

            If model.ID > 0 Then
                Message = "success"
            Else
                Message = "failed"
            End If

            Return New ResultSetModel() With {.ID = model.ID, .Number = INVNum, .Message = Message}

        End Function
        Public Function GetMembers() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select ID,Fname from Gs_Member Order by ID"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetMemberType() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select ID,Description from Gs_MemberType Order by ID"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetDealerRecord() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "SELECT CustomerID, Company FROM Customer WHERE  CompanyID = " & CompanyID & " and StatusID <> 205 ORDER BY Company"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
        Public Function GetTerms() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = " SELECT  PK_Terms, TermsNo + ' ~ ' + TermsDescription As Terms FROM tblTerms ORDER BY PK_Terms"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
        Public Function GetSOType() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "   select * from GS_SOType"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function AddDetail(model As SalesOrderModel, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            StrQry = "	  select Isnull(Max(ItemSeq),0) + 1 from tblSalesOrderItems where FK_SalesOrder = " & model.FK_SalesOrder & ""
            Dim ds As New DataSet
            Dim NewID As Integer
            ds = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                NewID = ds.Tables(0).Rows(0)(0).ToString
                'Return Insert
            End If

            StrQry = "Insert into tblSalesOrderItems(ItemSeq,FK_SalesOrder,FK_Item,DefaultDesc,CurrentDesc,Quantity,UnitPrice,Remarks,BD_PRICE,DisCount_per) values(" & NewID & ",'" & model.FK_SalesOrder & "','" & model.FK_Item & "','" & model.DefaultDesc & "','" & model.CurrentDesc & "','" & model.Quantity & "','" & model.UnitPrice & "','" & model.Remarks & "','" & model.BD_PRICE & "','" & model.DisCount_per & "')"

            Dim returnValue As Integer = BizSoft.DBManager.ExecuteQry(StrQry)
            If returnValue = 1 Then
                Message = "success"
            Else
                Message = "failed"
            End If
            Return Message
        End Function

        Public Function UpdateDetail(model As SalesOrderModel, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""

            StrQry = "update tblSalesOrderItems set FK_Item = '" & model.FK_Item & "',DefaultDesc = '" & model.DefaultDesc & "',CurrentDesc = '" & model.CurrentDesc & "',Quantity = '" & model.Quantity & "',UnitPrice = '" & model.UnitPrice & "',Remarks = '" & model.Remarks & "',BD_PRICE = '" & model.BD_PRICE & "',DisCount_per = '" & model.DisCount_per & "' where ID = " & model.ID & ""

            Dim returnValue As Integer = BizSoft.DBManager.ExecuteQry(StrQry)
            If returnValue = 1 Then
                Message = "update"
            Else
                Message = "failed"
            End If
            Return Message
        End Function
        Public Function ItemList() As System.Data.DataSet
            Dim StrQry As String = ""

            StrQry = "exec SPLookupInvoice '" & HttpContext.Current.Session("Datefrom") & "','" & HttpContext.Current.Session("dateTo") & "'," & HttpContext.Current.Session("UserID") & "," & HttpContext.Current.Session("CompanyID") & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetReference(ID As String) As System.Data.DataSet
            Dim StrQry As String = ""

            StrQry = "SELECT " &
           "      PK_SalesOrder, SalesOrder + ' ~ ' + CustomerName As SO " &
           "FROM  VWGUI_INV_SalesOrder " &
           " where isSaleReturn = 0 and islmr = 0 And  FK_StatusID = 803  And FK_Locationid = " & ID & "  And PK_SalesOrder not in ( Select FK_SalesOrder from tblinvoice) " &
           "ORDER BY SalesOrder"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function getSaleOrderById(ID As String) As System.Data.DataSet
            Dim StrQry As String = ""

            StrQry = "SELECT * FROM VWGUI_INV_SalesOrder WHERE PK_SalesOrder = " & ID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function getSaleOrderDetailById(ID As String) As System.Data.DataSet
            Dim StrQry As String = ""

            StrQry = "SELECT ModelNumber, PartNumber, CASE Amendments When 1 Then CurrentDesc Else DefaultDesc END As Description, Quantity, Unit, UnitPrice, Amount, Remarks, FK_Item FROM VWGUI_INV_SalesOrderItems WHERE FK_SalesOrder = " & ID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function DeleteDetail(ByVal ID As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            'Dim ds As DataSet
            'StrQry = "select * from GS_ProjectTaskType where FK_ProjectTypeID = '" & ID & "'"
            'ds = BizSoft.DBManager.GetDataSet(StrQry)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    Success = False
            '    Message = "exist"
            '    Return Message
            'End If

            StrQry = "Delete from tblSalesOrderItems where ID = " & ID & ""
            If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                Success = True
                Message = "success"
                Return Message
            Else
                Success = False
                Message = "failed"
                Return Message
            End If
            Return Message
        End Function
        Public Function GetStkQtyLoc(ByVal item_id As Integer, ByVal ToDate As Object, ByVal LocationID As Object, ByVal CurrentTransID As Object) As Integer
            Dim Result As New DataSet
            Dim nReceived
            Dim nIssued
            Dim strSQL As String
            Dim Stock As Integer
            If Not ToDate = "" Then
                'strSQL = "select isnull(sum(quantity),0) as quantity from Ps_stock where stock_type = 1 and item_id = " & item_id & " and DOC_DATE <= '" & FormatDateTime(ToDate, vbShortDate) & "' and  (Docname = '1 - Supplier - Purchases' OR Docname = 'OPENING' OR Docname = '2 - Dealer - Sales Return' ) "
                strSQL = "select isnull(sum(quantity),0) as quantity from Ps_stock where stock_type = 1 and item_id = " & item_id & " and DOC_DATE <= '" & FormatDateTime(ToDate, vbShortDate) & "' and Wh_id = " & LocationID
            Else
                strSQL = "select isnull(sum(quantity),0) as quantity from Ps_stock where stock_type = 1  and item_id = " & item_id & " And Wh_id = " & LocationID
            End If

            'rs.CursorLocation = adUseClient
            Result = BizSoft.DBManager.GetDataSet(strSQL)   'cnDataBase, adOpenForwardOnly, adLockReadOnly
            If Result.Tables(0).Rows(0)("quantity") > 0 Then
                nReceived = Result.Tables(0).Rows(0)("quantity")
            Else
                nReceived = 0
            End If




            If Not ToDate = "" Then
                'strSQL = "select isnull(sum(quantity),0) as quantity from Ps_stock where stock_type = 2 and item_id = " & item_id & " and DOC_DATE <= '" & FormatDateTime(ToDate, vbShortDate) & "' and DocName = '2 - Dealer - Sales' "
                strSQL = "select isnull(sum(quantity),0) as quantity from Ps_stock where stock_type = 2 and item_id = " & item_id & " and DOC_DATE <= '" & FormatDateTime(ToDate, vbShortDate) & "' and Wh_id = " & LocationID & " and Source_doc_number <> " & CurrentTransID
            Else
                strSQL = "select isnull(sum(quantity),0) as quantity from Ps_stock where stock_type = 2 and item_id = " & item_id & " And Wh_id = " & LocationID & " and Source_doc_number <> " & CurrentTransID
            End If

            'strsql = "select isnull(sum(quantity),0) as quantity from stock where stock_type = 0 and item_id = " & item_id
            'strsql = "select isnull(sum(quantity),0) as quantity from issuenote_detail, issuenote_header where issuenote_header.id=issuenote_detail.id and item_id=" & item_id

            'rs.CursorLocation = adUseClient
            'rs.Open strSQL, cnDataBase, adOpenForwardOnly, adLockReadOnly
            Result = BizSoft.DBManager.GetDataSet(strSQL)
            If Result.Tables(0).Rows(0)("quantity") > 0 Then
                nIssued = Result.Tables(0).Rows(0)("quantity")
            Else
                nIssued = 0
            End If
            'GetStkQty = IIf((nReceived - nIssued) > 0, (nReceived - nIssued), 0)
            Stock = nReceived - nIssued
            Return Stock
        End Function


    End Class


End Namespace