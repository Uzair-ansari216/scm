﻿Imports System.Reflection

Namespace BizSoft.Bridge_SCM

    Public Class Dealer
        Inherits Logic
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from Customer where CompanyID = " + HttpContext.Current.Session("CompanyID")
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetById(ByVal id As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from Customer where CustomerID = " & id & " and CompanyID = " + HttpContext.Current.Session("CompanyID") + ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetCompanyList(company As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select CustomerId As ID , FirstName As Name from customer where companyid = " & company & " order by firstname"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function Edit(ByVal RecID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from GS_ProjectType where  ID = " & RecID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        ' New Methods
        Public Function Update(model As DealerModel, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Try
                StrQry = "Update Customer Set FirstName = '" & model.FirstName & "',Company = '" & model.Company & "',Address = '" & model.Address & "',Phone = '" & model.Phone & "',Fax = '" & model.Fax & "',Email = '" & model.Email & "',CreditLimit = '" & model.CreditLimit & "',LocationID = '" & model.LocationID & "',DebtorAccountCode = '" & model.DebtorAccountCode & "',DebtorOpBal = '" & model.DebtorOpBal & "',CreditorAccountCode = '" & model.CreditorAccountCode & "',CreditorOpBal = '" & model.CreditorOpBal & "',NTN = '" & model.NTN & "',STN = '" & model.STN & "',CellNo1 = '" & model.CellNo1 & "',CellNo2 = '" & model.CellNo2 & "',Email1 = '" & model.Email1 & "',Email2 = '" & model.Email2 & "',cityID = '" & model.cityID & "',countryID = '" & model.countryID & "',CustomerLogin = '" & model.CustomerLogin & "',CBillingName = '" & model.CBillingName & "',CustomerGroupID = '" & model.CustomerGroupID & "',CustomerTypeID = '" & model.CustomerTypeID & "',EmployeeID = '" & model.EmployeeID & "',FK_RateMaster = '" & model.FK_RateMaster & "',LastName = '" & model.LastName & "',Terms = '" & model.Terms.Trim() & "' where CustomerID = " & model.ID & ""
                Dim returnValue As Integer = BizSoft.DBManager.ExecuteQry(StrQry)
                If returnValue = 1 Then
                    Dim iAction As String = ""
                    iAction = "Edit Recored Customer"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Customer", "Customer", model.ID, Environment.MachineName)

                    Message = "update"
                Else
                    Message = "failed"
                End If
                Return Message
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Customer", "Customer", model.ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try

        End Function

        Public Function Delete(ByVal ID As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            'Dim ds As DataSet
            'StrQry = "select * from GS_ProjectTaskType where FK_ProjectTypeID = '" & ID & "'"
            'ds = BizSoft.DBManager.GetDataSet(StrQry)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    Success = False
            '    Message = "exist"
            '    Return Message
            'End If
            Try
                StrQry = "Delete from Customer where CustomerID = " & ID & ""
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Delete Recored Customer"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Customer", "Customer", ID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
                Return Message
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Customer", "Customer", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
            End Try

        End Function

        Public Function Add(model As DealerModel, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            Try
                StrQry = "Insert into Customer(FirstName,Company,Address,Phone,Fax,Email,CreditLimit,LocationID,DebtorAccountCode,DebtorOpBal,CreditorAccountCode,CreditorOpBal,NTN,STN,CellNo1,CellNo2,Email1,Email2,cityID,countryID,CustomerLogin,CBillingName,CustomerGroupID,CustomerTypeID,EmployeeID,FK_RateMaster,LastName,Terms,CompanyID,StatusID) values('" & model.FirstName & "','" & model.Company & "','" & model.Address & "','" & model.Phone & "','" & model.Fax & "','" & model.Email & "','" & model.CreditLimit & "','" & model.LocationID & "','" & model.DebtorAccountCode & "','" & model.DebtorOpBal & "','" & model.CreditorAccountCode & "','" & model.CreditorOpBal & "','" & model.NTN & "','" & model.STN & "','" & model.CellNo1 & "','" & model.CellNo2 & "','" & model.Email1 & "','" & model.Email2 & "','" & model.cityID & "','" & model.countryID & "','" & model.CustomerLogin & "','" & model.CBillingName & "','" & model.CustomerGroupID & "','" & model.CustomerTypeID & "','" & model.EmployeeID & "','" & model.FK_RateMaster & "','" & model.LastName & "','" & model.Terms.Trim() & "'," & CompanyID & ",200)"

                Dim returnValue As Integer = BizSoft.DBManager.ExecuteQry(StrQry)

                If returnValue = 1 Then
                    Dim iAction As String = ""
                    iAction = "Add New Recored Customer"
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Customer", "Customer", returnValue, Environment.MachineName)

                    Message = "success"
                Else
                    Message = "failed"
                End If
                Return Message
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), DateTime.Parse(DateTime.Now).ToString("hh:mm:ss tt"), "Customer", "Customer", 0, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try

        End Function
        Public Function GetLocation() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = " select * from Locations"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetCity() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = " select * from tblCity"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetSalesPerson() As System.Data.DataSet
            Dim StrQry As String = ""
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            StrQry = "SELECT FirstName, EmployeeID FROM Employees WHERE  companyID = " & CompanyID & " And StatusID <> 109 AND LastName IN (Select Designation From Designations Where Category = 1) ORDER BY FirstName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetPricing() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT ID , Description from TblItem_RateMaster ORDER BY Description"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetCategory() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from CustomerCategory"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetCustomerType() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT * FROM CustomerTypes ORDER BY CustomerTypeID"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetCountry() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select * from tblCountry"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetCustomerGroup() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT * FROM TblCustomerGroup "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetDetail() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "  select * from Customer"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function FindData(ID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "  select * from Customer where CustomerID = " & ID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
    End Class

End Namespace
