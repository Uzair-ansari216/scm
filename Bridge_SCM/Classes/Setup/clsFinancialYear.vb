﻿Imports System.Reflection

Namespace BizSoft.Bridge_SCM

    Public Class clsFinancialYear
        Inherits Logic
        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select convert(nvarchar(10),yearID ) ID, yearTitle,dateFrom,dateTo,style  from GL_FINANCIAL_YEAR where CompanyID = " + HttpContext.Current.Session("CompanyID") + "Order by yearTitle"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetById(ByVal id As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from GL_FINANCIAL_YEAR where yearID = " & id & " and CompanyID = " + HttpContext.Current.Session("CompanyID") + ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function Update(ByVal ID As String, ByVal yearTitle As String, ByVal dateFrom As String, ByVal dateTo As String, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "Select * from GL_FINANCIAL_YEAR Where yearTitle='" & yearTitle & "' and yearID <>  '" & ID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                StrQry = "Update GL_FINANCIAL_YEAR set yearTitle = '" & yearTitle & "', dateFrom='" & dateFrom & "', dateTo='" & dateTo & "' where yearID = " & ID & ""
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Edit Recored Financial Year Name = " & yearTitle
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Financial Year", "GL_FINANCIAL_YEAR", ID, Environment.MachineName)

                    Success = True
                    Message = "update"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Financial Year", "GL_FINANCIAL_YEAR", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try

        End Function

        Public Function Delete(ByVal ID As String, ByVal Name As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            Try
                StrQry = "Delete from GL_FINANCIAL_YEAR where yearID = " & ID & " "
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Delete Recored Financial Year Name = " & Name
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Financial Year", "GL_FINANCIAL_YEAR", ID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If

            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Financial Year", "GL_FINANCIAL_YEAR", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
            End Try

        End Function

        Public Function Add(ByVal ID As String, ByVal yearTitle As String, ByVal dateFrom As String, ByVal dateTo As String, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "Select * from GL_FINANCIAL_YEAR Where yearTitle ='" & yearTitle & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                Dim company = HttpContext.Current.Session("CompanyID")
                StrQry = "Insert into GL_FINANCIAL_YEAR(yearTitle,dateFrom,dateTo,style,CompanyID) values('" & yearTitle & "','" & dateFrom & "','" & dateTo & "',1," & company & ")Select @@IDENTITY"

                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    NewID = ds.Tables(0).Rows(0)(0).ToString()
                    Dim iAction As String = ""
                    iAction = "Add New Recored Financial Year Name = " & yearTitle
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Financial Year", "GL_FINANCIAL_YEAR", NewID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Financial Year", "GL_FINANCIAL_YEAR", NewID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Return Message = ""
        End Function

    End Class

End Namespace