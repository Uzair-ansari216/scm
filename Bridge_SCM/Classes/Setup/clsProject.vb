﻿Imports System.Reflection

Namespace BizSoft.Bridge_SCM
    Public Class clsProject
        Inherits Logic

        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT  dbo.Projects.*, dbo.vGetCustomers.Company" &
              " FROM    dbo.Projects" &
              " INNER Join  dbo.vGetCustomers ON dbo.Projects.ClientID = dbo.vGetCustomers.CustomerID where dbo.Projects.CompanyID = " + HttpContext.Current.Session("CompanyID")
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function


        Public Function GetById(ByVal id As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "Select * from Projects where ProjectID = '" & id & "' and CompanyID = " + HttpContext.Current.Session("CompanyID") + ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function
        Public Function GetListClient() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT * FROM vGetCustomers WHERE StatusID <> 205  AND CompanyID = 1 order by Company"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetListProjecType() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select projecttype from projects group by projecttype"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        Public Function GetListsDepartmentCode() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = " SELECT DepartmentName,DepartmentID FROM Departments WHERE CompanyID = " & HttpContext.Current.Session("CompanyID") & " ORDER BY DepartmentName"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds
        End Function

        'Public Function Edit(ByVal RecID As String) As System.Data.DataSet
        '    Dim StrQry As String = ""
        '    StrQry = "Select * from tblItems where  ID = " & RecID & ""
        '    Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
        '    Return ds
        'End Function
        'New Methods
        Public Function Update(ByVal ID As String, ByVal ProjectTitle As String, ByVal ProjectType As String, ByVal ClientID As String, ByVal StartDate As String, ByVal EndDate As String, ByVal DepartmentCode As String, ByVal EstimatedDuration As String, ByVal EstimatedCost As String, ByVal DebtorAccountCode As String, ByVal DebtorOpBal As String, ByVal CreditorAccountCode As String, ByVal CreditorOpBal As String, ByVal Description As String, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Try
                StrQry = "Select * from Projects Where Description='" & Description & "' and ProjectID <>  '" & ID & "'"
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                If ID <> "-1" Then

                    StrQry = "Update Projects set ProjectTitle = '" & ProjectTitle & "' ,ProjectType = '" & ProjectType & "', ClientID = '" & ClientID & "',StartDate = '" & StartDate & "', EndDate = '" & EndDate & "', DepartmentCode = '" & DepartmentCode & "', EstimatedDuration = '" & EstimatedDuration & "', EstimatedCost = '" & EstimatedCost & "', DebtorAccountCode = '" & DebtorAccountCode & "', DebtorOpBal = '" & DebtorOpBal & "', CreditorAccountCode = '" & CreditorAccountCode & "', CreditorOpBal = '" & CreditorOpBal & "', Description = '" & Description & "' where ProjectID = '" & ID & "'"
                Else
                    ' StrQry = "Update tblItems set FK_ItemCategory = " & FK_ItemCategory & " ,FK_BrandID = '" & FK_BrandID & "', FK_Unit = '" & FK_Unit & "',PartNumber = '" & PartNumber & "', ModelNumber = '" & ModelNumber & "', ProductName = '" & ProductName & "', Description = '" & Description & "', MinQuantity = '" & MinQuantity & "', MaxQuantity = '" & MaxQuantity & "', ReOrderQuantity = '" & ReOrderQuantity & "', DeliveryLandTime = '" & DeliveryLandTime & "', SalePoint = '" & SalePoint & "', ExpGST = '" & ExpGST & "', WarrantyYear = '" & WarrantyYear & "', WarrantyNote = '" & WarrantyNote & "', isActive = '" & isActive & "', GST = '" & GST & "', AvailableForSO = '" & AvailableForSO & "', ItemToBeRepost = '" & ItemToBeRepost & "',isSerialNo = '" & isSerialNo & "' where PK_Item = " & ID & ""
                End If

                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Edit Recored Project Name = " & ProjectTitle
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Project", "Projects", 0, Environment.MachineName)

                    Success = True
                    Message = "update"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Location", "Locations", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try
        End Function

        Public Function Delete(ByVal ID As String, ByVal Name As String, ByRef Success As Boolean, ByRef Message As String) As String

            Dim StrQry As String = ""
            ' Dim ds As DataSet
            'StrQry = "select * from Projects where ProjectID = '" & ID & "'"
            'ds = BizSoft.DBManager.GetDataSet(StrQry)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    Success = False
            '    Message = "exist"
            '    Return Message
            'End If
            Try
                StrQry = "Delete from Projects where ProjectID = '" & ID & "'"
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Delete Recored Project Name = " & Name
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Project", "Projects", ID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Project", "Projects", ID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
            End Try
        End Function

        Public Function Add(ByVal ID As String, ByVal ProjectTitle As String, ByVal ClientID As String, ByVal ProjectType As String, ByVal StartDate As String, ByVal EndDate As String, ByVal DepartmentCode As String, ByVal EstimatedDuration As String, ByVal EstimatedCost As String, ByVal DebtorAccountCode As String, ByVal DebtorOpBal As String, ByVal CreditorAccountCode As String, ByVal CreditorOpBal As String, ByVal Description As String, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As String
            Dim StrQry As String = ""
            Dim ds As DataSet
            Dim CompanyID As String = HttpContext.Current.Session("CompanyID")
            Try
                StrQry = "Select * from Projects Where ProjectTitle = '" & ProjectTitle & "' "
                ds = BizSoft.DBManager.GetDataSet(StrQry)
                If ds.Tables(0).Rows.Count > 0 Then
                    Success = False
                    Message = "exist"
                    Return Message
                End If
                If ID <> "-1" Then
                Dim LocationID As String = HttpContext.Current.Session("LocationId")
                Dim NewKey As String = AutoGeneratedKEY(LocationID)
                StrQry = "Insert into Projects(ProjectID,ProjectTitle,ClientID,ProjectType,StartDate,EndDate,DepartmentCode,EstimatedDuration,EstimatedCost,DebtorAccountCode,DebtorOpBal,CreditorAccountCode,CreditorOpBal,Description,CompanyID) values('" & NewKey & "','" & ProjectTitle & "','" & ClientID & "','" & ProjectType & "','" & StartDate & "','" & EndDate & "','" & DepartmentCode & "','" & EstimatedDuration & "','" & EstimatedCost & "','" & DebtorAccountCode & "','" & DebtorOpBal & "','" & CreditorAccountCode & "','" & CreditorOpBal & "','" & Description & "','" & CompanyID & "')Select @@IDENTITY"
                'Else
                '    StrQry = "Insert into GS_Resource(Description,Fk_ResourceType) values ('" & Description & "','" & Fk_ResourceType & "')Select @@IDENTITY"
            End If

                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Dim iAction As String = ""
                    iAction = "Add New Recored Project Name = " & ProjectTitle
                    BizSoft.Utilities.InsertLogs(HttpContext.Current.Session("UserID"), Date.Now, iAction, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("CompanyID"), Date.Now, "Project", "Projects", NewID, Environment.MachineName)

                    Success = True
                    Message = "success"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            Catch ex As Exception
                BizSoft.Utilities.InsertErrorLogs(HttpContext.Current.Session("UserID"), Date.Now, StrQry, ex.Message, HttpContext.Current.Session("CompanyID"), Date.Now, "Project", "Projects", NewID, Environment.MachineName, Assembly.GetExecutingAssembly().GetName().Version.ToString())
                Success = False
                Message = ex.Message
                Return Message
            End Try

        End Function


        Private Function AutoGeneratedKEY(l_location As String) As String
            '  On Error GoTo e
            Dim gl_query As String
            Dim NewKey As String
            'KHI-0001/06
            Dim rs As New DataSet
            gl_query = "SELECT isNull(max(Cast(subString(ProjectID, 5, 5) as numeric(18, 0))), 0) + 1 FROM Projects"
            ' rs.Open(gl_query, objConn)

            'Exit Function
            rs = BizSoft.DBManager.GetDataSet(gl_query)
            NewKey = l_location & Format(rs.Tables(0).Rows(0)(0), "00000") '& "/" & year(GL_FinancialDateF) & "-" & Right(GL_FinancialDateT, 2)
            ' MessageBox(Err.Description, 1)
            Return NewKey
            'Resume Next
        End Function


    End Class
End Namespace