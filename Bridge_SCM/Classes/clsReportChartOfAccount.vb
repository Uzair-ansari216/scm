﻿Namespace BizSoft.Bridge_SCM

    Public Class ReportChartOFAccount
        Inherits Logic

        Public Sub Add(ByVal p_accountCode As String, ByVal p_accountName As String, ByVal p_description As String, ByVal p_parentCode As String, ByVal p_openBalance As String, ByVal p_accountLevel As String, ByVal p_userID As String, ByVal p_dateCreated As String, ByVal p_accountTypeID As String, ByVal p_companyID As String, ByVal p_parentLevel As String, ByVal p_accountCode_old As String, ByVal p_Location_id As String, ByVal BalanceSheetAccountHead As String, ByVal ISSubLedger As String, ByVal FK_BSID As String, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long)
            Dim StrQry As String = ""

            StrQry = "Select * from ChartOfAccount Where AccountName='" & p_accountName & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "Sorry Account Name is already exists , Please try any other"
                Exit Sub
            End If
            StrQry = "GUI_chartofaccount_add_edit '" & p_accountCode & "','" & p_accountName & "', '" & p_description & "', '" & p_parentCode & "', " & Val(p_openBalance) & ",'" & p_accountLevel & "','" & p_userID & "','" & p_dateCreated & "','" & p_accountTypeID & "'," & p_companyID & ", '" & p_parentLevel & "' ,'" & p_accountCode_old & "'," & p_Location_id & ",'" & BalanceSheetAccountHead & "','" & ISSubLedger & "'," & FK_BSID & ""

            Try
                BizSoft.DBManager.GetDataSet(StrQry)
                Success = True
                Message = "Chart Of Account record added successfully "
            Catch ex As Exception
                Success = False
                Message = ex.Message
            End Try

        End Sub

        Public Sub Update(ByVal p_accountCode As String, ByVal p_accountName As String, ByVal p_description As String, ByVal p_parentCode As String, ByVal p_openBalance As String, ByVal p_accountLevel As String, ByVal p_userID As String, ByVal p_dateCreated As String, ByVal p_accountTypeID As String, ByVal p_companyID As String, ByVal p_parentLevel As String, ByVal p_accountCode_old As String, ByVal p_Location_id As String, ByVal BalanceSheetAccountHead As String, ByVal ISSubLedger As String, ByVal FK_BSID As String, ByRef Success As Boolean, ByRef Message As String)
            Dim StrQry As String = ""
            StrQry = "Select * from ChartOfAccount Where AccountName='" & p_accountName & "' and AccountCode <>  '" & p_accountCode & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "Sorry Chart Of Account is already exists , Please try any other"
                Exit Sub
            End If
            StrQry = "GUI_chartofaccount_add_edit '" & p_accountCode & "','" & p_accountName & "', '" & p_description & "', '" & p_parentCode & "', " & Val(p_openBalance) & ",'" & p_accountLevel & "','" & p_userID & "','" & p_dateCreated & "','" & p_accountTypeID & "'," & p_companyID & ", '" & p_parentLevel & "' ,'" & p_accountCode_old & "'," & p_Location_id & ",'" & BalanceSheetAccountHead & "','" & ISSubLedger & "'," & FK_BSID & ""

            Try
                If BizSoft.DBManager.GetDataSet(StrQry).Tables(0).Rows.Count > 0 Then

                    Success = True
                    Message = "Chart Of Accoun record modified successfully "
                Else
                    Success = False
                    Message = "There is some problem in saving new record "
                End If
            Catch ex As Exception
                Success = False
                Message = ex.Message
            End Try


        End Sub

        Public Function StripID(ByVal varString As String) As String
            Dim i As Integer
            If Trim(varString) <> "" Then
                i = InStr(1, Trim(varString), "~")
                If i > 1 Then
                    Return Trim(Left(Trim(varString), i - 1))
                ElseIf i = 1 Then
                    Return ""
                Else
                    Return Trim(varString)
                End If
            Else
                Return Trim(varString)
            End If
        End Function

        Public Overrides Function GetList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select convert(nvarchar(15),Accountcode )ID,AccountName   as Name  from ChartOfAccount "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        '   Public Function GetLevel5AccountList(ByVal CompanyID As String) As System.Data.DataSet
        ' Dim StrQry As String = ""
        '
        '            StrQry = "select AccountName as Name,convert(nvarchar(15),Accountcode )ID  from ChartOfAccount  where CompanyID = " & CompanyID & " and AccountLevel = 5 "
        '        Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
        '           Return ds
        '
        '        End Function
        Public Function GetLevel5AccountList(ByVal CompanyID As String) As System.Data.DataSet
            Dim StrQry As String = ""

            StrQry = "select AccountName as Name,convert(nvarchar(15),Accountcode )ID  from ChartOfAccount  where AccountLevel = 5 AND  CompanyID = " & CompanyID & ""
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetLevel5AccountList2(ID As String, ByVal CompanyID As String) As System.Data.DataSet
            Dim StrQry As String = ""
            If ID <> "" Then
                StrQry = "select AccountName as Name,convert(nvarchar(15),Accountcode )ID  from ChartOfAccount  where companyID  = " & CompanyID & " and  AccountLevel = 5 and Accountcode = '" & ID & "'"
                Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
                Return ds
            Else

            End If



        End Function
        Public Function GetAccountGroupsList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select convert(nvarchar(10),AccountGroupID )ID,AccountGroup   as Name  from Accountgroups "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function


        Public Function GetTrailBalanceData(ByVal GrossNet As String, ByVal CompanyID As String, ByVal DateF As String, ByVal DateT As String) As System.Data.DataSet
            Dim StrQry As String = ""
            If GrossNet = 1 Then
                StrQry = " select AccountCode ," &
                    " (select isnull( sum(vngl.debit)  - SUM (vngl.Credit),0)  from vngl where companyID = " & CompanyID & " and  VoucherDate < '" & DateF & "' and  vngl.AccountCode = Mvngl.AccountCode ) as op , " &
                    " (select isnull(sum(debit) - SUM (Credit),0)   from vngl where companyID =  " & CompanyID & " and  VoucherDate between  '" & DateF & "' and '" & DateT & "' and vngl.AccountCode = Mvngl.AccountCode ) as Activity " &
                    " , (select isnull(sum(debit),0 )   from vngl where companyID =  " & CompanyID & " and  VoucherDate between  '" & DateF & "' and '" & DateT & "' and vngl.AccountCode = Mvngl.AccountCode ) as debit " &
                    " , (select isnull( SUM (Credit),0)   from vngl where companyID =  " & CompanyID & " and  VoucherDate between  '" & DateF & "' and '" & DateT & "' and vngl.AccountCode = Mvngl.AccountCode ) as Credit " &
                    " from vngl Mvngl where Mvngl.companyID =  " & CompanyID & "   Group By AccountCode "
            Else
                StrQry = " select AccountCode ," &
                    " (select isnull( sum(vngl.debit)  - SUM (vngl.Credit),0)  from vngl where companyID = " & CompanyID & " and  VoucherDate < '" & DateF & "' and  vngl.AccountCode = Mvngl.AccountCode ) as op , " &
                    " (select isnull(sum(debit) - SUM (Credit),0)   from vngl where companyID =  " & CompanyID & " and  VoucherDate between  '" & DateF & "' and '" & DateT & "' and vngl.AccountCode = Mvngl.AccountCode ) as Activity " &
                    " from vngl Mvngl where Mvngl.companyID =  " & CompanyID & "   Group By AccountCode "
            End If
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function


        Public Sub DeleteTrailBalance()
            Dim StrQry As String = ""

            StrQry = " Delete from GL_TRIALBALANCE_REPORt"
            BizSoft.DBManager.ExecuteQry(StrQry)

        End Sub

        Public Sub InsertTrailBalance(ByVal UserID As String, ByVal companyID As String, ByVal AccountCode As String, ByVal AccountName As String, ByVal ODebit As String, ByVal OCredit As String, ByVal Debit As String, ByVal Credit As String, ByVal CDebit As String, ByVal CCredit As String, ByVal SAccount As String)
            Dim StrQry As String = ""

            StrQry = " Insert Into GL_TRIALBALANCE_REPORt values(" & UserID & "," & companyID & " ,'" & AccountCode & "','" & AccountName & "', " & ODebit & ", " & OCredit & ", " & Debit & ", " & Credit & ", " & CDebit & ", " & CCredit & ", " & SAccount & ")"
            BizSoft.DBManager.ExecuteQry(StrQry)

        End Sub


        Public Function GetAccountACCountPrandCode(ByVal LevelNumber As String, ByVal CompanyID As String) As System.Data.DataSet
            Dim StrQry As String = ""

            If Len(LevelNumber) < 5 Then

                StrQry = "select * from Vchartofaccount where parentaccountCode = '" & LevelNumber & "' order by accountcode desc"
            Else
                StrQry = "select * from Vchartofaccount where CompanyID = " & CompanyID & " and  parentaccountCode = '" & LevelNumber & "' order by accountcode desc"
            End If
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function


        Public Function GetISUnique(ByVal GLV_accountCode As String, ByVal CompanyID As String) As System.Data.DataSet
            Dim StrQry As String = ""

            If Len(GLV_accountCode) <= 5 Then
                StrQry = " select * from Vchartofaccount where accountCode = '" & GLV_accountCode & "'"
            Else
                StrQry = " select * from Vchartofaccount where CompanyID = " & CompanyID & " and accountCode = '" & GLV_accountCode & "'"
            End If

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetAcountLevels(ByVal GLV_parentCode As String) As System.Data.DataSet
            Dim StrQry As String = ""
            '    If Len(GLV_parentCode) < 5 Then
            StrQry = " select * from vChartofAccount where AccountCode = '" & GLV_parentCode & "'"
            '    Else
            '    StrQry = " select * from vChartofAccount where companyID = " & CompanyID & " and AccountCode = '" & GLV_parentCode & "'"
            '    End If


            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function Getds(ByVal mQuery As String) As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = mQuery
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetAccountTypeList() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select convert(nvarchar(10),AccountTypeID )ID,AccountType   as Name  from AccountTypes "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function


        Public Function GetListAccountCode() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "SELECT convert(nvarchar(15),AccountCode  ) ID,AccountName as Name FROM ChartofAccount "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function


        Public Function GetTopAccountLevelCode() As System.Data.DataSet
            Dim StrQry As String = ""
            'StrQry = "SELECT convert(nvarchar(10),AccountCode  ) ID,Description as Name FROM GL_GCA_MASTER where ParentAccountCode = 0 "

            StrQry = "SELECT AccountCode As ID,Description as Name FROM GL_GCA_MASTER where ParentAccountCode is null "
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetCharOfAccount(ByVal TopLevelID As Integer, ByVal CompanyID As String) As System.Data.DataSet

            Dim StrQry As String

            '' StrQry = " select *  from vChartofAccount where AccountGroupID  =" & TopLevelID
            StrQry = "EXECUTE GUI_GrandChartOfAccount @p_companyID = " & CompanyID & " , @AccountGroupID = " & TopLevelID
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetListAccountType() As System.Data.DataSet
            Dim StrQry As String = ""
            StrQry = "select convert(nvarchar(10),AccountTypeID )ID,AccountType  as Name  from AccountTypes order by AccountType"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function
        Public Function Edit(ByVal RecID As String) As System.Data.DataSet

            Dim StrQry As String = ""
            StrQry = "Select * from ChartOfAccount where  AccountCode = '" & RecID & "'"

            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function

        Public Function GetAccountListByAccType(ByVal AccountTypeID As String, ByVal CompanyID As String) As System.Data.DataSet

            Dim StrQry As String = ""
            StrQry = "Select AccountName, AccountCode  FROM vChartOfAccount " _
                    & " WHERE CompanyID = " & CompanyID & " AND AccountCode NOT IN (SELECT DISTINCT ParentAccountCode FROM vChartOfAccount WHERE CompanyID = " & CompanyID & ") " _
                    & " AND AccountType = " & AccountTypeID & ""


            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            Return ds

        End Function


        Public Function Delete(ByVal AccountCode As String, ByVal StatusID As String, ByVal CompanyID As String) As String
            Dim Message As String = ""
            Dim Success As Boolean
            Dim StrQry As String = ""
            StrQry = "select * from VoucherDetail where AccountCode = '" & AccountCode & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "exist"
                Return Message
            End If

            Dim StrQryNew As String = ""
            StrQryNew = "select * from ChartOfAccount where ParentAccountCode = '" & AccountCode & "'"
            Dim dsNew As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            If dsNew.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "exist"
                Return Message
            End If

            StrQry = "spDeleteAccount '" & AccountCode & "',0"
            If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                Success = True
                Message = "success"
                Return Message
            Else
                StrQry = "spDeleteAccount2 '" & AccountCode & "',0"
                If BizSoft.DBManager.ExecuteQry(StrQry) > 0 Then
                    Success = True
                    Message = "success"
                    Return Message
                Else
                    Success = False
                    Message = "failed"
                    Return Message
                End If
            End If


        End Function



        Public Function IsCorectChrtOfAccount(ByVal AccountCode As String) As Boolean

            Dim StrQry As String = ""
            StrQry = "Select * from ChartOfAccount where  AccountCode = '" & AccountCode & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If

        End Function


        Public Function GetChrtOfAccountName(ByVal AccountCode As String) As String

            Dim StrQry As String = ""
            Dim ChrtOfDesc As String = "0"
            StrQry = "Select * from ChartOfAccount where  AccountCode = '" & AccountCode & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                ChrtOfDesc = ds.Tables(0).Rows(0).Item("AccountName").value.ToString()
                Return ChrtOfDesc
            Else
                Return ChrtOfDesc
            End If

        End Function



        Public Function AddNew(ByVal p_accountCode As String, ByVal p_accountName As String, ByVal p_description As String, ByVal p_parentCode As String, ByVal p_openBalance As String, ByVal p_accountLevel As String, ByVal p_userID As String, ByVal p_dateCreated As String, ByVal p_accountTypeID As String, ByVal p_companyID As String, ByVal p_parentLevel As String, ByVal p_accountCode_old As String, ByVal p_Location_id As String, ByVal BalanceSheetAccountHead As String, ByVal ISSubLedger As String, ByVal FK_BSID As String, ByRef Success As Boolean, ByRef Message As String, ByRef NewID As Long) As String
            Dim StrQry As String = ""

            StrQry = "Select * from ChartOfAccount Where AccountName='" & p_accountName & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "exist"
                Return Message
            End If
            StrQry = "GUI_chartofaccount_add_edit '" & p_accountCode & "','" & p_accountName & "', '" & p_description & "', '" & p_parentCode & "', " & Val(p_openBalance) & ",'" & p_accountLevel & "','" & p_userID & "','" & p_dateCreated & "','" & p_accountTypeID & "'," & p_companyID & ", '" & p_parentLevel & "' ,'" & p_accountCode_old & "'," & p_Location_id & ",'" & BalanceSheetAccountHead & "','" & ISSubLedger & "'," & FK_BSID & ""

            Try
                BizSoft.DBManager.GetDataSet(StrQry)
                Success = True
                Message = "success"
                Return Message
            Catch ex As Exception
                Success = False
                Message = ex.Message
                Return Message
            End Try
            Message = ""
            Return Message
        End Function

        Public Function UpdateRecords(ByVal p_accountCode As String, ByVal p_accountName As String, ByVal p_description As String, ByVal p_parentCode As String, ByVal p_openBalance As String, ByVal p_accountLevel As String, ByVal p_userID As String, ByVal p_dateCreated As String, ByVal p_accountTypeID As String, ByVal p_companyID As String, ByVal p_parentLevel As String, ByVal p_accountCode_old As String, ByVal p_Location_id As String, ByVal BalanceSheetAccountHead As String, ByVal ISSubLedger As String, ByVal FK_BSID As String, ByRef Success As Boolean, ByRef Message As String) As String
            Dim StrQry As String = ""
            StrQry = "Select * from ChartOfAccount Where AccountName='" & p_accountName & "' and AccountCode <>  '" & p_accountCode & "'"
            Dim ds As DataSet = BizSoft.DBManager.GetDataSet(StrQry)
            If ds.Tables(0).Rows.Count > 0 Then
                Success = False
                Message = "exist"
                Return Message

            End If
            StrQry = "GUI_chartofaccount_add_edit '" & p_accountCode & "','" & p_accountName & "', '" & p_description & "', '" & p_parentCode & "', " & Val(p_openBalance) & ",'" & p_accountLevel & "','" & p_userID & "','" & p_dateCreated & "','" & p_accountTypeID & "'," & p_companyID & ", '" & p_parentLevel & "' ,'" & p_accountCode_old & "'," & p_Location_id & ",'" & BalanceSheetAccountHead & "','" & ISSubLedger & "'," & FK_BSID & ""

            Try
                Dim ReturnVal As Integer
                ReturnVal = BizSoft.DBManager.ExecuteQry(StrQry)
                Message = "update"
                Return Message
                'If ReturnVal = 0 Then

                '    Success = True
                '    Message = "update"
                '    Return Message
                'Else
                '    Success = False
                '    Message = "failed"
                '    Return Message
                'End If
            Catch ex As Exception
                Success = False
                Message = ex.Message
            End Try

            Return Message
        End Function






    End Class

End Namespace