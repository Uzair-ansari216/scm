﻿Public Class CommonConstant

    Public Const purchaseOrderReport As String = "rptPO.rpt"
    Public Const goodReceivingNoteReport As String = "rptGRN.rpt"
    Public Const goodDeliveryNoteReport As String = "rptGDN.rpt"
    Public Const salesOrderReport As String = "rptSO.rpt"
    Public Const invoiceReport As String = "rptInvoice.rpt"
End Class
