﻿const commonServiceUrl = "../../appServices/CommonService.asmx"

$(function () {
    // Wizard style
    //Add blue animated border and remove with condition when focus and blur
    if ($('.fg-line')[0]) {
        $('body').on('focus', '.form-control', function () {
            $(this).closest('.fg-line').addClass('fg-toggled');
        })

        $('body').on('blur', '.form-control', function () {
            var p = $(this).closest('.form-group');
            var i = p.find('.form-control').val();

            if (p.hasClass('fg-float')) {
                if (i.length == 0) {
                    $(this).closest('.fg-line').removeClass('fg-toggled');
                }
            }
            else {
                $(this).closest('.fg-line').removeClass('fg-toggled');
            }
        });
    }

    //Add blue border for pre-valued fg-flot text feilds
    if ($('.fg-float')[0]) {
        $('.fg-float .form-control').each(function () {
            var i = $(this).val();

            if (!i.length == 0) {
                $(this).closest('.fg-line').addClass('fg-toggled');
            }

        });
    }


    /*   Form Wizard Functions  */
    /*--------------------------*/
    _handleTabShow = function (tab, navigation, index, wizard) {
        var total = navigation.find('li').length;
        var current = index + 0;
        var percent = (current / (total - 1)) * 100;
        var percentWidth = 100 - (100 / total) + '%';

        navigation.find('li').removeClass('done');
        navigation.find('li.active').prevAll().addClass('done');

        wizard.find('.progress-bar').css({ width: percent + '%' });
        $('.form-wizard-horizontal').find('.progress').css({ 'width': percentWidth });
    };

    _updateHorizontalProgressBar = function (tab, navigation, index, wizard) {
        var total = navigation.find('li').length;
        var current = index + 0;
        var percent = (current / (total - 1)) * 100;
        var percentWidth = 100 - (100 / total) + '%';

        wizard.find('.progress-bar').css({ width: percent + '%' });
        wizard.find('.progress').css({ 'width': percentWidth });
    };

    /* Form Wizard - Example 1  */
    /*--------------------------*/
    $('#formwizard_simple').bootstrapWizard({
        onTabShow: function (tab, navigation, index) {
            _updateHorizontalProgressBar(tab, navigation, index, $('#formwizard_simple'));
        }
    });

    /* Form Wizard - Example 2  */
    /*--------------------------*/

    $('#formwizard_validation').bootstrapWizard({
        onNext: function (tab, navigation, index) {
            var form = $('#formwizard_validation').find("form");
            var valid = true;

            if (index == 1) {
                var fname = form.find('#firstname');
                var lastname = form.find('#lastname');

                if (!fname.val()) {
                    swal("You must enter your first name!");
                    fname.focus();
                    return false;
                }

                if (!lastname.val()) {
                    swal("You must enter your last name!");
                    lastname.focus();
                    return false;
                }
            }

            if (!valid) {
                return false;
            }
        },
        onTabShow: function (tab, navigation, index) {
            _updateHorizontalProgressBar(tab, navigation, index, $('#formwizard_validation'));
        }
    });
    $(".form-wizard-nav ul").removeClass("nav-pills")
    // Wizard style end


    $("body").on("change", "#developer-mode, #tool-tip, #training, #voicemessage", function () {
        var currrentElementId = $(this).attr('id')
        var hiddenFiledValue = $(this).parent().find("input[type='hidden']").val()

        switch (currrentElementId) {
            case "developer-mode":
                if (hiddenFiledValue == 'on') {
                    $(this).parent().find("input[type='hidden']").val('off')
                } else {
                    $(this).parent().find("input[type='hidden']").val('on')
                }
                break;
            case "tool-tip":
                if (hiddenFiledValue == 'on') {
                    $(this).parent().find("input[type='hidden']").val('off')
                } else {
                    $(this).parent().find("input[type='hidden']").val('on')
                }
                break;
            case "training":
                if (hiddenFiledValue == 'on') {
                    $(this).parent().find("input[type='hidden']").val('off')
                } else {
                    $(this).parent().find("input[type='hidden']").val('on')
                }
                break;
            case "voicemessage":
                if (hiddenFiledValue == 'on') {
                    $(this).parent().find("input[type='hidden']").val('off')
                } else {
                    $(this).parent().find("input[type='hidden']").val('on')
                }
                break;
            default:
        }
        $("#dropdown a").attr({ "aria-expanded": true })


        $.ajax({
            url: "" + commonServiceUrl + "/UpdateAppServices",
            Type: "POST",
            ContentType: "application/json; charset=utf-8",
            data: { developerMode: $("#hdnDevMode").val(), toolTip: $("#hdnToolTip").val(), training: $("#hdnTraining").val(), voiceMessage: $("#hdnVoiceMessage").val() },
            dataType: "json",
            success: function (responce) {
                //alert(responce)
            }, error: function (responce) {
                //alert(responce)
            }
        })
    })

    $("body").on("click", "#btntoggle", function (event) {
        event.preventDefault();
        $(this).parent().toggleClass('open')
    })


})

var addFormFields = function (formName, formId) {
    debugger
    var controlsArray = new Array()
    $("#" + formId + " input").each(function (index, row) {
        if ($(row).attr('type') != 'hidden') {
            var formDetail = {
                MenuName: formName,
                ObjectType: $(row).attr('type'),
                ObjectName: $(row).attr('id'),
                Label: $(row).parent().find('label').text(),
                IsMendatory: false
            }
            controlsArray.push(formDetail)
        }
    })
    $("#" + formId + " select").each(function (index, secrow) {
        var formDetail = {
            MenuName: formName,
            ObjectType: "Select",
            ObjectName: $(secrow).attr('id'),
            Label: $(secrow).parent().find('label').text(),
            IsMendatory: false
        }
        controlsArray.push(formDetail)
    })


    $.ajax({
        url: "" + commonServiceUrl + "/AddFormFields",  //?control=" + JSON.stringify(controlsArray),
        type: "POST",
        data: JSON.stringify({ model: controlsArray }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (responce) {
            //alert(responce)
        }, error: function (responce) {
            //alert(responce)
        }
    })
}

var setToolTip = function (formName, formId, developerMode) {
    debugger
    $.ajax({
        url: "" + commonServiceUrl + "/getRequiredToolTip?formName=" + formName,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (responce) {
            $(JSON.parse(responce.d)).each(function (index, row) {
                $("#" + formId + " input").each(function (index, formRow) {
                    if ($(formRow).attr('type') != 'hidden' && $(formRow).attr('type') != 'checkbox' && $(formRow).attr('id') == row.ObjectName) {
                        $(formRow).attr("_fieldType", row.FieldType)
                        if (row.MaximumLength > 0) {
                            $(formRow).attr("maxLength", row.MaximumLength)
                        } else {
                            $(formRow).attr("maxLength", "")
                        }

                        $(formRow).parent().find('span:first').text(row.ToolTip)
                        $(formRow).parent().find('span').eq(1).text(row.FieldTypeValidationMessage)
                        $(formRow).attr("_isMandatory", row.IsMendatory)
                        $(formRow).parent().find('span').eq(2).text(row.MendatoryMessage)
                        if (row.IsMendatory) {
                            var mendatory = "<span color='red'>*</span>" +
                            $(formRow).parent().find('label').text(row.Label + "*")
                        } else {
                            $(formRow).parent().find('label').text(row.Label)
                        }

                        if (developerMode == 'on') {
                            $(formRow).attr("title", row.ObjectName)
                        }
                    }
                })
            })

            $(JSON.parse(responce.d)).each(function (index, row) {
                $("#" + formId + " select").each(function (index, formRow) {
                    if ($(formRow).attr('type') != 'hidden' && $(formRow).attr('id') == row.ObjectName) {
                        if (!$(formRow).hasClass('select2')) {
                            $(formRow).parent().find('span:first').text(row.ToolTip)
                            $(formRow).parent().find('span').eq(2).text(row.MendatoryMessage)
                        }
                        $(formRow).attr("_isMandatory", row.IsMendatory)
                        if (row.IsMendatory) {
                            var mendatory = "<span color='red'>*</span>" +
                            $(formRow).parent().find('label').text(row.Label + "*")
                        } else {
                            $(formRow).parent().find('label').text(row.Label)
                        }
                        if (developerMode == 'on') {
                            $(formRow).attr("title", row.ObjectName)
                        }
                    }
                })
            })

        }, error: function (responce) {
            alert(responce)
        }
    })
}

var showCurrentToopTip = function (currentElement) {
    $(currentElement).parent().find('span:first').removeClass('hide')
}

var hideToopTipExceptCurrent = function (currentElement) {
    if ($(currentElement).parent().find('span:first').hasClass('hide')) {

    }
    debugger
    if ($(currentElement).attr('type') != "checkbox") {
        $(currentElement).parent().find('span:first').addClass('hide')
        if ($(currentElement).val() != "") {
            $(currentElement).parent().find('span').eq(2).addClass('hide')
        }

    }
}

var hideValidationMessage = function (currentElement) {
    if ($(currentElement).attr('type') != "checkbox") {
        $(currentElement).parent().find('span:first').addClass('hide')
        if ($(currentElement).val() != "") {
            $(currentElement).parent().find('span').eq(2).addClass('hide')
        }

    }
}

var showHideFieldValidation = function (key, currentElement, type, toolTip) {
    switch (type) {
        case "number" || "Number":
            //select integers only
            //var intRegex = /[0-9 -()+]+$/; intRegex.test($(currentElement).val()) == false
            if (key.charCode < 48 || key.charCode > 57) {

                $(currentElement).parent().find('span:first').addClass("hide")
                $(currentElement).parent().find('span').eq(1).removeClass("hide").css("color", "red")
                $(currentElement).parent().find('label').addClass('warning')
                if (key.charCode == 46) {
                    return true;
                } else {
                    return false;
                }
                //$(currentElement).val(" ")
            } else {
                $(currentElement).parent().find('label').removeClass('warning')
                $(currentElement).parent().find('span').eq(1).addClass("hide")
                if (toolTip == 'on') {
                    $(currentElement).parent().find('span:first').removeClass("hide")
                }
            }
            break;
        case "email" || "email":
            //match email address
            var intRegex = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$';
            if (intRegex.test($(currentElement).val()) == false) {
                $(currentElement).parent().find('span:first').addClass("hide")
                $(currentElement).parent().find('span').eq(1).removeClass("hide").css("color", "red")
                $(currentElement).parent().find('label').addClass('warning')
                //$(currentElement).css({"border-bottom-color":"red"})
            } else {
                $(currentElement).parent().find('label').removeClass('warning')
                $(currentElement).parent().find('span').eq(1).addClass("hide")
                $(currentElement).parent().find('span:first').removeClass("hide")
            }
            break;
    }
}

var clearAllMessages = function (formId) {
    debugger
    $("#" + formId + " input").each(function (index, row) {
        if ($(row).attr('type') != 'hidden' && $(row).attr('type') != 'checkbox') {
            if (!$(row).parent().find('span').eq(1).hasClass('hide')) {
                $(row).parent().find('span').eq(1).addClass('hide')
            }
            if (!$(row).parent().find('span').eq(2).hasClass('hide')) {
                $(row).parent().find('span').eq(2).addClass('hide')
            }
        }
    })
}



var getFormatedDate = function (dateToFormat) {
    debugger
    var datett = new Date(parseInt(dateToFormat.substr(6)));  //without -1 it gives tommorow's date
    var DateToformatted = ((datett.getMonth()) + 1) + "/" +
        (datett.getDate()) + "/" +
           datett.getFullYear()
    var hours = datett.getHours();
    var minutes = datett.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    var uio = DateToformatted;  //+ " " + strTime;                      when  time also required with date 
    return uio;
}

let isValueExist = function (id, value) {
    if (value != "") {
        $(id).parent().removeClass('is-empty')
    }
}


let showAlert = function () {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
        }
    })
}



var BlockUI = function () {
    $.blockUI({ baseZ: 90000000, message: '<h5><img src="/Layout/assets/img/spinner.gif" width="50" height="50" /> Please Wait...</h5>' });
}
var UnblockUI = function () {
    $.unblockUI();
}